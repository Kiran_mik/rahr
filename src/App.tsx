import React, { useEffect } from "react";
import { Provider } from "react-redux";
import { ConfigProvider } from "antd";
import { PersistGate } from "redux-persist/integration/react";

import Routes from "./configs/routes";
import store, { persistor } from "@store/index";
import { addKeysBeforeLoading } from "@utils/helpers";

//@ts-ignore
ConfigProvider.config({
  //@ts-ignore
  theme: {
    primaryColor: "#4E1C95",
  },
  prefixCls: "ant",
});

function App() {
  return (
    <ConfigProvider>
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor} />
        <LoadRoutes />
      </Provider>
    </ConfigProvider>
  );
}

export const LoadRoutes = () => {
  useEffect(() => {
    addKeysBeforeLoading();
  }, []);

  return <Routes />;
};

export default App;
