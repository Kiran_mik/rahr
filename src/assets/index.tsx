import NotificationIcon from "./icon-notifications.svg";
import AlertSettingIcon from "./alerts-settings.svg";
import SearchIcon from "./search.svg";
import TableUser from "./Table-User.svg";
import UserImage from "./UserImage.png";
import ThreeDots from "./three-dots-vertical.svg";
import DashboardIcon from "./icons/dashboard.svg";
import DealProcessingIcon from "./icons/processing.svg";
import AgentMangement from "./icons/agent-management.svg";
import Academy from "./icons/academy.svg";
import Teams from "./icons/teams.svg";
import GlobalSetting from "./icons/global-setting.svg";
import FormManagement from "./icons/form-management.svg";
import UserManagement from "./icons/user-management.svg";
import Notification from "./icons/notification.svg";
import Setting from "./icons/setting.svg";
import UserAvatar2 from "./UserAvatar2.svg";
import User3 from "./User3.png";
import BackArrow from "./icons/Back Arrow.svg";
import EditIcon from "./icons/edit-icon.svg";
import EditPencilIcon from "./icons/edit-pencil.svg";
import EditIconWhite from "./icons/edit-icon-white.svg";
import Cross from "./icons/cross.svg";
import Right from "./icons/right.svg";
import ReportIcon from "./icons/report-icon.png";
import DrawerIcon from "./photo-bg.png";
import DemoImage from "./demoImage.png";
import ExclamationCircle from "./icons/exclamation-circle.svg";
import SideBarIconLeft from "./Menuarrow-left.svg";
import SideBarIconRight from "./Menuarrow-right.svg";
import DatepickerIcon from "./datepicker.svg";
import DropdownIcon from "./drodown-icon.svg";
import RoundedCircle from "./rounded-circle.svg";
import StatusIcon from "./icon-status.svg";
import FillEditIcon from "./editIcon.svg";
import BagIcon from "./Bagicon.svg";
import WhiteArrowLeft from "./arrowleft-white.svg";
import EmptyDivImg from "./emptydivimg.svg";
import Missisauga from "./Missisauga.png";
import HHToronto from "./HHToronto.png";
import HHOttawa from "./HHOttawa.png";
import HollandBloorview from "./HollandBloorview.png";
import BlueNotification from "./BlueNotification.svg";
import Vector from "./icons/vector.svg";
import AgentConfirmation from "./illustratoin-confirmation.png";
import ApplicationParked from "./AccountParked.png";
import ApplicationApproved from "./ApplicationApproved.png";
import ApplicationReview from "./ApplicationReview.png";
import ApplicationTermination from "./ApplicationTermination.png";

export {
  NotificationIcon,
  AlertSettingIcon,
  SearchIcon,
  DashboardIcon,
  DealProcessingIcon,
  AgentMangement,
  Academy,
  Teams,
  GlobalSetting,
  FormManagement,
  UserManagement,
  Notification,
  Setting,
  TableUser,
  UserImage,
  ThreeDots,
  UserAvatar2,
  User3,
  BackArrow,
  EditIcon,
  Cross,
  Right,
  ReportIcon,
  DrawerIcon,
  ExclamationCircle,
  DemoImage,
  SideBarIconLeft,
  SideBarIconRight,
  DatepickerIcon,
  DropdownIcon,
  RoundedCircle,
  StatusIcon,
  FillEditIcon,
  BagIcon,
  WhiteArrowLeft,
  EmptyDivImg,
  Missisauga,
  HHToronto,
  HHOttawa,
  HollandBloorview,
  BlueNotification,
  Vector,
  EditPencilIcon,
  EditIconWhite,
  AgentConfirmation,
  ApplicationParked,
  ApplicationApproved,
  ApplicationReview,
  ApplicationTermination,
};
