import React, { useEffect } from "react";
import { Match, Router } from "@reach/router";
import UserManagementPages from "@pages/UserManagementPages";

import NewStaffMember from "@pages/UserManagementPages/StaffMembers/NewStaff";
import StaffDetail from "@pages/UserManagementPages/StaffMembers/StaffDetails";
import EditStaffProfile from "@pages/UserManagementPages/StaffMembers/EditStaff/EditStaffProfile";
import RAHStaffListPage from "@pages/UserManagementPages/RAHStaffListPage";
import { BranchesList } from "@pages/UserManagementPages/BranchesPage";
import DepartmentPage from "@pages/UserManagementPages/DepartmentPage/index";
import BranchDetail from "@pages/UserManagementPages/BranchesPage/BranchDetails";
import DepartmentDetail from "@pages/UserManagementPages/DepartmentPages/DepartmentDetails";
import DepartmentList from "@pages/UserManagementPages/DepartmentPages/DepartmentList";
import AddbranchPage from "@pages/UserManagementPages/BranchesPage/NewBranch";
import GoogleMap from "@pages/UserManagementPages/GoogleMaps";

import AgentManagmentPages from "@pages/AgentManagmentPages";
import AgentListPage from "@pages/AgentManagmentPages/AgentListPage";
import NewAgentPage from "@pages/AgentManagmentPages/NewAgentPage";
import GlobalSettingsPages from "@pages/GlobalSettingsPages";
import BoardManagmentPage from "@pages/GlobalSettingsPages/BoardManagmentPage";
import OnboardingPage from "@pages/GlobalSettingsPages/OnboardingPage";

import PageMiddleWare from "../middlewares/PageMiddleWare";
import AuthPages from "@pages/AuthPages";
import AgentOnboardingPages from "@pages/AgentOnboardingPages";
import ContactDetailsPage from "@pages/AgentOnboardingPages/ContactDetailsPage";
import ProfileInformationPage from "@pages/AgentOnboardingPages/ProfileInformationPage";
import CurrentEmploymentStatusPages from "@pages/AgentOnboardingPages/CurrentEmploymentStatusPages";
import PRECDetailsPage from "@pages/AgentOnboardingPages/PrecDetails";
import ReviewAgreementsPage from "@pages/AgentOnboardingPages/ReviewAgreements";
import ConfirmationPage from "@pages/AgentOnboardingPages/ConfirmationPage";
import ReviewPage from "@pages/AgentOnboardingPages/ReviewPage";
import BillingCommission from "@pages/AgentOnboardingPages/BillingCommission";
import CharityEnrollment from "@pages/AgentOnboardingPages/CharityEnrollment";
import AgentReviewPage from "@pages/AgentManagmentPages/AgentReviewPage";
import OffboardingPage from "@pages/GlobalSettingsPages/OffboardingPage";
import ChangeBranchModal from "@pages/AgentOnboardingPages/ChangeBranchModalPage/";
import PageNotFound from "@pages/PageNotFound";
import Dashboard from "@pages/Dashboard";
import TeminationPage from "@pages/AgentManagmentPages/Termination/TerminationPage";

const Routes = () => {
  return (
    <>
      <Router primary={false}>
        <PageMiddleWare path={"/"}>
          <Dashboard path={"/"} />
          <AgentManagmentPages path={"/agent/"}>
            <PageNotFound default />
            <AgentListPage path={"/agent-list"} />
            <NewAgentPage path={"/new-agent"} />
            <AgentReviewPage path={"/agent-review/:agentId/:form"} />
            <AgentReviewPage isTerminating={true} path={"/agent-review/terminating/:agentId/:form"} />
            <AgentReviewPage isParking={true} path={"/agent-review/parking/:agentId/:form"} />
          </AgentManagmentPages>
          <TeminationPage path={"/offboarding/temination"} />
          <GlobalSettingsPages path={"/global-settings"}>
            <PageNotFound default />

            <BoardManagmentPage path={"/board-list"} />
            <OnboardingPage path={"/onboard"} />
            <OffboardingPage path={"/offboard"} />
          </GlobalSettingsPages>
          <UserManagementPages path={"/usermanagement/"}>
            <PageNotFound default />
            <GoogleMap path={"/google-map"} />
            <RAHStaffListPage path={"/staff-list"} />
            <NewStaffMember path={"/new-staff"} />
            <EditStaffProfile path={"/edit-staff/:empId"} />
            <StaffDetail path={"/staff-detail"} />
            <StaffDetail path={"/staff-detail/:empId"} />
            <BranchesList path={"/branch-list"} />
            <AddbranchPage path={"/new-branch"} />
            <AddbranchPage path={"/edit-branch/:branchId"} />
            <BranchDetail path={"/branch-detail/:branchId"} />
            <DepartmentList path={"/department-list"} />
            <DepartmentPage path={"/new-department"} />
            <DepartmentDetail path={"/department-detail/:departmentId"} />
          </UserManagementPages>
          <AgentOnboardingPages path={"/agent-onboard/"}>
            <PageNotFound default />
            <ContactDetailsPage path={"/contact-details"} />
            <CurrentEmploymentStatusPages path={"/employment-status"} />
            <ProfileInformationPage path={"/profile-info"} />
            <ChangeBranchModal
              path={"/profile-info/branches"}
              backPath={"/agent-onboard/profile-info/"}
            />
            <CurrentEmploymentStatusPages path={"/employment-info"} />
            <PRECDetailsPage path={"/prec-details"} />
            <BillingCommission path={"/billing-commission"} />
            <ReviewAgreementsPage path={"/review-agreements"} />

            <CharityEnrollment path={"/charity-enrollments"} />
            <ConfirmationPage path={"/confirmation"} />
            <ReviewPage path={"/review"} />
          </AgentOnboardingPages>
        </PageMiddleWare>
      </Router>
    </>
  );
};

export default Routes;
