import Facebook from "@assets/social-media/facebook.svg";
import Instagram from "@assets/social-media/instagram.svg";
import LinkIdn from "@assets/social-media/linkIdn.svg";
import Twitter from "@assets/social-media/twitter.svg";

export const BASE_URL = process.env.REACT_APP_BASE_URL_original;
export const phoneRegExp = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
export const BASE_URL_MAP = {
    'usermanagement': process.env.REACT_APP_BASE_URL_original,
    'global-settings': process.env.REACT_APP_BASE_URL_GLOBAL_SETTINGS,
    'agent-onboard': process.env.REACT_APP_BASE_URL_AGENT,
    'agent': process.env.REACT_APP_BASE_URL_AGENT
}
export const getBaseURL = () => {
    const pathName = window.location.pathname.split("/")[1]
    //@ts-ignore
    return BASE_URL_MAP[pathName] || process.env.REACT_APP_BASE_URL_original



}
export const SOCIAL_MEDIA_OPTIONS = {
    facebook: { img: Facebook, id: 1, url: 'https://www.facebook.com/RightatHomeCAN/' },
    linkedIn: { img: LinkIdn, id: 2, url: 'https://www.linkedin.com/company/right-at-home-canada/' },
    twitter: { img: Twitter, id: 3, url: 'https://twitter.com/RightatHome_CAN' },
    instagram: { img: Instagram, id: 4, url: 'https://www.instagram.com/rightathomewinnipeg/?hl=en' }
}


