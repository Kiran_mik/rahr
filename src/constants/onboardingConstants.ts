import { ObjectStr } from '@utils/helpers';

export const STATUS_COLOR_MAP: ObjectStr = {
	SUCCESS: '#31AF91',
	ERROR: '#FB3307',
	DISABLED: "#AAAABC"
}

export const TextLableEnum: { [key: number]: string } = {
	1: "New registrant",
	2: "Transfer",
	3: "Reinstated with RECO"
}


export const BrokerOrSalesRepresentative: { [key: number]: string } = {
	1: "Broker",
	2: "Sales representative",
}

export const AgentNotificationEnum: { [key: number]: string } = {
	1: "Text",
	2: "Email",
	3: "Both",
}