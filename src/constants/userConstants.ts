export const USER_STATUS_ENUM = {
    ACTIVE: 1,
    INACTIVE: 2,
    INVITE_SENT: 3,
    PENDING_REVIEW: 4,
    AWAITING_DOC: 5,
    PARKED: 6,
    OFFBOARDING_REQUEST: 7,
    TERMINATED: 8
}

export const USER_STATUS_TEXT_ENUM : { [key: number]: string } = {
    1: 'Active',
    2: 'Inactive',
    3: 'Invite sent',
    4: 'Pending Review',
    5: 'Awaiting doc',
    6: 'Parked',
    7: 'OffBoarding Request',
    8: 'Terminated'
}

export const AGENT_STATUS_TEXT : { [key: number]: string } = {
    1: 'Active agent',
    3: 'New Agent',
    4: 'New Agent',
    6: 'Parked',
    8: 'Terminated'
}

export const ROLE_MAPING  = {
    AGENT: 2,
    ADMIN: 1,
  };