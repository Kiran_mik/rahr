import { agentOnboardingActions, AgentOnBoardingSteps } from '@store/agentOnboardingReducer'
import { useDispatch, useSelector } from 'react-redux'


export default function () {
    const dispatch = useDispatch()
    const form = useSelector((state: { agentOnboarding: AgentOnBoardingSteps }) => state.agentOnboarding)
    const setForm = (formType: string, details: {}) => {
        dispatch(agentOnboardingActions.updateForm(formType, details))
    }
    const setStep = (step: number) => {
        dispatch(agentOnboardingActions.setStep(step))
    }
    return [setForm, setStep, form] as [(formType: string, details: {}) => void, (step: number) => void, any]

}