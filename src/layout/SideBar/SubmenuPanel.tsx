import React, { useEffect, useState } from "react";
import { Menu } from "antd";
import { WhiteArrowLeft } from "@assets/index";
import {
  SubmenuPanel,
  MenuTitleWrapper,
  SubMenuWrapper,
  SubTitle
} from "./style";
function SubMenuPanel() {
  return (
    <>
      <SubmenuPanel>
        <MenuTitleWrapper>
          <img src={WhiteArrowLeft} alt="Back Arrow" />
          List of agents
        </MenuTitleWrapper>
        <hr />
        <SubMenuWrapper>
          <SubTitle>Agents by status</SubTitle>
          <Menu theme="dark" mode="inline">
            <Menu.Item className="active-child">All agents</Menu.Item>
            <Menu.Item>Active agents</Menu.Item>
            <Menu.Item>Inactive agents</Menu.Item>
            <Menu.Item>New agents</Menu.Item>
            <Menu.Item>Parked agents</Menu.Item>
          </Menu>
        </SubMenuWrapper>
      </SubmenuPanel>
    </>
  );
}

export default SubMenuPanel;
