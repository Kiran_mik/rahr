interface ObjectKey {
	[key: string]: any
}


export const MENU_KEYS_MAP: ObjectKey = {
	"sub8": (url: string) => url.includes("global-settings"),
	"sub6": (url: string) => url.includes("usermanagement"),
	"sub3": (url: string) => url.includes("agent"),

}

export const PATH_NAV_ITEMS_MAP: ObjectKey = {
	"16": (url: string) => url.includes("branch"),
	"17": (url: string) => url.includes("department"),
	"15": (url: string) => url.includes("staff"),
	"21": (url: string) => url.includes("board-list"),
	"22": (url: string) => url.includes("onboard"),
	"5": (url: string) => url.includes("agent-list") || url.includes("new-agent"),
}


export const GLOBAL_SETTINGS_LINKS = [{
	title: 'Board management',
	key: 21,
	to: '/global-settings/board-list',
},
{
	title: 'Onboarding',
	key: 22,
	to: '/global-settings/onboard'
},
{
	title: 'Offboarding',
	key: 23,
	to: '/global-settings/offboard'
},
{
	title: 'Fees',
	key: 24
}
]

export const USER_MANAGMENT_LINKS = [{
	key: 15,
	title: 'Staff'
},
{
	key: 16,
	title: 'Branch'
},
{
	key: 17,
	title: 'Department'
},

]


export const AGENT_MANAGMENT_LINKS = [{
	title: 'Dashboard',
	key: 4
},
{
	title: 'Agents',
	key: 5,
	to: '/agent/agent-list'
},

{
	title: 'Onboarding & Offboarding',
	key: 6
},

]