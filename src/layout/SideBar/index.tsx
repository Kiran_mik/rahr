import React, { useEffect, useState } from "react";
import { Layout, Menu, Breadcrumb } from "antd";
import { Avatar } from "antd";
import { Flex } from "@components/Flex";
import { useDispatch, useSelector } from "react-redux";
import {
  DoubleLeftOutlined,
  DoubleRightOutlined,
  BarChartOutlined,
} from "@ant-design/icons";
import {
  DashboardIcon,
  DealProcessingIcon,
  AgentMangement,
  Academy,
  Teams,
  GlobalSetting,
  FormManagement,
  UserManagement,
  Notification,
  Setting,
} from "@assets/index";
import {
  LogoBox,
  NavButton,
  SidebarPanel,
  MenuWrapper,
  MenuTitle,
  SidebarFooter,
  AccountUser,
  UserName,
  UserEmail,
  InnerMenu,
  AccountDetails,
} from "./style";
import logo from "@assets/logo.png";
import { Link, navigate, useLocation } from "@reach/router";
import {
  AGENT_MANAGMENT_LINKS,
  GLOBAL_SETTINGS_LINKS,
  MENU_KEYS_MAP,
  PATH_NAV_ITEMS_MAP,
} from "./helper";
import SubMenuPanel from "./SubmenuPanel";
const { Header, Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;
const noNavigate = (routeUrl: any) => {
  navigate(routeUrl);
};

const rootSubmenuKeys = [
  "sub1",
  "sub2",
  "sub3",
  "sub4",
  "sub5",
  "sub6",
  "sub7",
  "sub8",
];
const SideBar = (props: any) => {
  const [collapsed, setCollapsed] = useState<boolean>(false);
  const location = useLocation();
  const dispatch = useDispatch();
  const user = useSelector(
    (state: { user: { username: string; email: string } }) => state.user
  );
  const { username, email } = user;

  const [openKeys, setOpenKeys] = React.useState(["sub6"]);

  useEffect(() => {
    let scrollElement = document.getElementById("scrolltobottom");
    scrollElement && scrollElement.scrollIntoView();

    const activeMenu = Object.keys(MENU_KEYS_MAP).find((i) =>
    MENU_KEYS_MAP[i](location.pathname)
    );
    setOpenKeys([activeMenu || "sub6"]);
  }, []);

  const nameSymbol = username && username[0] + username[1];
  /*open*/
  const onCollapse = () => {
    setCollapsed(!collapsed);
  };

  const onOpenChange = (keys: Array<any>) => {
    const latestOpenKey = keys.find((key: any) => openKeys.indexOf(key) === -1);
    if (rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
      setOpenKeys(keys);
    } else {
      setOpenKeys(latestOpenKey ? [latestOpenKey] : []);
    }
  };
  /*end*/

  const activeKey = Object.keys(PATH_NAV_ITEMS_MAP).find((i) =>
    PATH_NAV_ITEMS_MAP[i](location.pathname)
  );
  return (
    <SidebarPanel id={"sidePanel"}>
      <Layout style={{ minHeight: "100vh" }}>
        <Sider
          style={{
            minWidth: "100%",
            maxWidth: "100%",
          }}
          collapsible
          collapsed={collapsed}
          onCollapse={onCollapse}
        >
          <LogoBox className="collapsiblelogo">
            <img src={logo} alt="logo" className="logoimg" />
          </LogoBox>
          <InnerMenu className="heightoncollapse">
            <MenuWrapper className="childmenu">
              <Menu
                theme="dark"
                mode="inline"
                openKeys={openKeys}
                onOpenChange={onOpenChange}
                //@ts-ignore
                selectedKeys={[activeKey]}
                defaultOpenKeys={["sub6"]}
              >
                <MenuTitle className="hideoncollapse">Operations </MenuTitle>
                <Menu.Item key="1">
                  <span>
                    <img src={DashboardIcon} alt="Dashboard" />{" "}
                  </span>
                  <span className="hideoncollapse">Dashboard</span>
                </Menu.Item>
                <SubMenu
                  key="sub2"
                  title={
                    <span>
                      <img src={DealProcessingIcon} alt="Deal processing" />
                      <span className="hideoncollapse">Deal processing</span>
                    </span>
                  }
                >
                  <Menu.Item key="2">Dashboard </Menu.Item>
                  <Menu.Item key="3">Deals </Menu.Item>{" "}
                </SubMenu>
                <SubMenu
                  key="sub3"
                  title={
                    <span>
                      <img src={AgentMangement} alt="Agent Management" />
                      <span className="hideoncollapse">Agent Management</span>
                    </span>
                  }
                >
                  {AGENT_MANAGMENT_LINKS.map((i: any) => {
                    return (
                      <Menu.Item
                        key={i.key.toString()}
                        onClick={() => {
                          if (i.to) {
                            noNavigate(i.to);
                          }
                        }}
                      >
                        {i.title}
                      </Menu.Item>
                    );
                  })}
                </SubMenu>
                <SubMenu
                  key="sub4"
                  title={
                    <span>
                      <img src={Academy} alt="Academy" />
                      <span className="hideoncollapse">Academy </span>
                    </span>
                  }
                >
                  <Menu.Item key="7">Dashboard </Menu.Item>
                  <Menu.Item key="8">Courses </Menu.Item>
                  <Menu.Item key="9">Resources </Menu.Item>
                  <Menu.Item key="10">Events </Menu.Item>{" "}
                </SubMenu>
                <SubMenu
                  key="sub5"
                  title={
                    <span>
                      <img src={Teams} alt="Teams & Mentors" />
                      <span className="hideoncollapse">Teams & Mentors</span>
                    </span>
                  }
                >
                  <Menu.Item key="11">Dashboard </Menu.Item>
                  <Menu.Item key="12">Teams </Menu.Item>
                  <Menu.Item key="13">Mentors </Menu.Item>{" "}
                </SubMenu>
                <Menu.Item key="14" icon={<BarChartOutlined />}>
                  <span className="hideoncollapse">Reports</span>
                </Menu.Item>
                <MenuTitle className="hideoncollapse">App Management</MenuTitle>
                <SubMenu
                  key="sub6"
                  title={
                    <span id="scrolltobottom">
                      <img src={UserManagement} alt="User management" />
                      <span className="hideoncollapse">User management</span>
                    </span>
                  }
                >
                  <Menu.Item
                    key="15"
                    onClick={() => {
                      noNavigate("/usermanagement/staff-list");
                    }}
                  >
                    Staff{" "}
                  </Menu.Item>
                  <Menu.Item
                    key="16"
                    onClick={() => {
                      noNavigate("/usermanagement/branch-list");
                    }}
                  >
                    Branch{" "}
                  </Menu.Item>
                  <Menu.Item
                    key="17"
                    onClick={() => {
                      noNavigate("/usermanagement/department-list");
                    }}
                  >
                    Department
                  </Menu.Item>
                </SubMenu>
                <SubMenu
                  key="sub7"
                  title={
                    <span>
                      <img src={FormManagement} alt="Form management" />
                      <span className="hideoncollapse">Form management</span>
                    </span>
                  }
                >
                  <Menu.Item key="18">Deal Templates </Menu.Item>
                  <Menu.Item key="19">Form Templates </Menu.Item>
                  <Menu.Item key="20">Clauses</Menu.Item>{" "}
                </SubMenu>

                <SubMenu
                  key="sub8"
                  title={
                    <span>
                      <img src={GlobalSetting} alt="Global settings" />
                      <span className="hideoncollapse">Global settings</span>
                    </span>
                  }
                >
                  {GLOBAL_SETTINGS_LINKS.map((i: any) => {
                    return (
                      <Menu.Item
                        key={i.key.toString()}
                        onClick={() => {
                          if (i.to) {
                            noNavigate(i.to);
                          }
                        }}
                      >
                        {i.title}
                      </Menu.Item>
                    );
                  })}
                </SubMenu>
              </Menu>
            </MenuWrapper>
          </InnerMenu>
          <SidebarFooter>
            <AccountDetails>
              <hr />
              <MenuWrapper className="childmenu2">
                <Menu theme="dark" mode="inline">
                  <MenuTitle className="hideoncollapse">Account </MenuTitle>
                  <Menu.Item key="25">
                    <span>
                      <img src={Notification} alt="Notification" />{" "}
                    </span>
                    <span className="hideoncollapse">Notifications</span>
                  </Menu.Item>
                  <Menu.Item key="26">
                    {" "}
                    <span>
                      {" "}
                      <img src={Setting} alt="Setting" />{" "}
                    </span>
                    <span className="hideoncollapse">Profile settings</span>
                  </Menu.Item>
                </Menu>
              </MenuWrapper>
            </AccountDetails>
            <AccountUser className="paddingoncollapse">
              <Flex
                style={{
                  alignSelf: "",
                }}
              >
                {" "}
                <Avatar
                  className="userimage"
                  style={{
                    backgroundColor: "white",
                    color: "black",
                    marginRight: "10px",
                    alignSelf: "center",
                  }}
                  size={36}
                >
                  {nameSymbol}
                </Avatar>{" "}
              </Flex>
              {collapsed === false ? (
                <Flex
                  justifyContent={"center"}
                  style={{
                    flexDirection: "column",
                  }}
                >
                  <UserName>{username}</UserName>
                  <UserEmail>{email}</UserEmail>
                </Flex>
              ) : (
                ""
              )}
            </AccountUser>
          </SidebarFooter>
        </Sider>
        {/* <SubMenuPanel /> */}
      </Layout>
    </SidebarPanel>
  );
};
export default SideBar;
