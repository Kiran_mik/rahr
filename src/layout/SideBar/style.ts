

import styled from 'styled-components'
import { Button } from 'antd';
import {
  SideBarIconLeft,
  SideBarIconRight
} from "@assets/index";
export const SidebarPanel = styled.div`
  
.ant-menu-inline.ant-menu-sub {
  background:rgba(55, 61, 95, 1) !important;
        border-bottom-left-radius: 4px;
    border-bottom-right-radius: 4px;
    padding: 8px 0px;
}
.anticon.anticon-left{
    background:url(${SideBarIconLeft}) no-repeat;
    display: block;
    width: 32px;
    height: 32px;
}
.anticon.anticon-right{
    background:url(${SideBarIconRight}) no-repeat;
    display: block;
    width: 32px;
    height: 32px;
}
.anticon.anticon-left svg{
display:none;
}
.anticon.anticon-right svg{
display:none;
}
.ant-menu-sub.ant-menu-inline > .ant-menu-item{
  height:auto;
  padding:8px 16px;
}
.ant-menu-inline.ant-menu-sub .ant-menu-title-content {
font-size:12px;
}
.ant-menu-item-selected {
    background-color: rgba(20, 25, 57, 1) !important;
}
.ant-layout-sider{
width:290px !important;
min-width:290px !important;
max-width:290px !important;
background:rgb(34, 41, 79);
// border-radius:4px;
}

.ant-layout-sider.ant-layout-sider-collapsed{
  width:80px  !important;
min-width:80px  !important;
max-width:80px  !important;
}
.ant-layout-sider.ant-layout-sider-collapsed .logoimg{
    width: 38px !important;
    margin: 0 auto;
    display: block;
}
.ant-layout-sider-has-trigger{
  padding-bottom:0px
}
.ant-layout-sider-collapsed .hideoncollapse{
    display: none;
}

.ant-menu.ant-menu-dark {
 background:rgb(34, 41, 79);
}
#components-layout-demo-side .logo {
  height: 32px;
  margin: 16px;
  background: rgba(255, 255, 255, 0.3);
}

.site-layout .site-layout-background {
  background: #fff;
}
.ant-layout-sider-trigger {
    position: absolute;
    top: 40px;
    z-index: 1;
    height: 32px;
    text-align: center;
    cursor: pointer;
    transition: all 0.2s;
    right: 32px;
    width: 32px !important;
    background:transparent;
}
.ant-layout-sider-collapsed .ant-layout-sider-trigger{
    right: -14px;
        top: 34px;
}
 .ant-menu.ant-menu-inline-collapsed > .ant-menu-submenu > .ant-menu-submenu-title {
    margin: 4px 0px;
    padding-left:0px !important;
    padding-right:0px !important;
}
.ant-menu-inline-collapsed .ant-menu-item{
    margin: 8px 0px !important;
      padding-left:0px !important;
    padding-right:0px !important;
}
.ant-layout-sider-collapsed .heightoncollapse{
      max-height: calc(100vh - 305px) !important;
}
.ant-layout-sider-collapsed .childmenu{
  padding:17px 17px !important;
}
.ant-layout-sider-collapsed .childmenu2{
  padding:17px 17px 2px 17px !important;
}
.ant-layout-sider-collapsed .paddingoncollapse{
  padding:16px 16px !important;
      justify-content: center;

}
.ant-layout-sider-collapsed .userimage{
  margin-right:0px !important;
}
.ant-layout-sider-collapsed .ant-menu-title-content img {
    background: rgba(55,61,95,1);
    padding: 10px;
    margin-right: 0px;
    border-radius: 10px;
    margin: 0 auto;
    display: block;
}
.ant-layout-sider-collapsed .collapsiblelogo{
padding:32px 16px 27px 16px;
}
.ant-layout-sider-collapsed .ant-menu-item-icon{
      background: rgba(55,61,95,1);
  
    border-radius: 10px;
    margin: 0 auto;
    display: block;
    width:36px;
    height:36px;
    margin:0px;
}
`;
export const SideBarMenu = styled.div`
.site-layout-background{
  background: #22294F;
}
`;

export const LogoBox = styled.div`
display: flex;
    justify-content: space-between;
        padding: 32px 32px 27px 32px;
    border-bottom: 1px solid #a2a2ba87;
img{
  width:62px;
  height:auto;
}
`

export const NavButton = styled(Button)`
background: #373D5F;
border:none;
align-self:center;
padding: 8px 11px;
    font-size: 10px;
`
export const MenuWrapper = styled.div`
   padding: 0px 28px 0px 32px;

 .ant-menu-submenu-title{
     height: auto;
    margin-top: 0px;
    margin-bottom: 0px;
    padding: 0 0px;
    overflow: hidden;
    line-height: 1.5;
    padding: 12px 0px;
    text-overflow: ellipsis;
}
.ant-menu-submenu-title{
  padding-left:12px !important;
}
.ant-menu-title-content{
      color: rgba(245, 245, 255, 1);
    font-size: 14px;
    line-height: 14px;
    font-family: Poppins;
    font-weight:400;
        overflow: unset !important;
        margin-left:0px !important;
}
.ant-menu-item .ant-menu-title-content img{
  margin-right:13px;
}
.ant-menu-submenu-title .ant-menu-title-content img{
  margin-right:16px;
}

.ant-menu-item{
   padding-left:12px !important;
   margin-top:0px !important;
   margin-bottom:0px !important;
}
.ant-menu-inline.ant-menu-root .ant-menu-item svg{
  width:16px;
  height:16px;
  margin-right:16px;
  color:#F5F5FF;
}

`
export const MenuTitle = styled.p`
color:rgba(248, 248, 252, 0.7);
font-size:12px;
line-height:12px;
text-transform: uppercase;
font-family: Poppins !important;
font-weight:600;
margin-bottom:12px;
  padding-top:27px;
`

export const InnerMenu = styled.div`
  // max-height: calc(100vh - 182px);
   max-height: calc(100vh - 337px);
    overflow: hidden;
    padding-bottom: 27px;
    overflow-y: auto;

&::-webkit-scrollbar {
  width: 7px;
}


&::-webkit-scrollbar-track {
  box-shadow: inset 0 0 5px grey;
  border-radius: 10px;
}


&::-webkit-scrollbar-thumb {
  background: #ffffff8f;
  border-radius: 10px;
}


&::-webkit-scrollbar-thumb:hover {
  background: #ffffff8f;
}
`
export const SidebarFooter = styled.div`
  position:absolute;
  bottom:0px;
  width:100%;
`

export const AccountDetails = styled.div`
    padding-bottom: 15px;
    @media(min-width:1900px){
       margin-top: 80px;
    }
    hr{
          border: none;
    border-top: 1px solid #a2a2ba87;
   
    }
`

export const AccountUser = styled.div`
display:flex;
    background: rgba(55, 61, 95, 1);
    padding: 16px 32px;
   .userimage{
     font-size:14px !important;
     line-height:22px;
     font-weight:600;
     font-family:Poppins; 
   }
    
`
export const UserName = styled.p`
font-size:14px;
line-height:22px;
color:#FFFFFF;
font-family:Poppins;
font-weight:600;
margin-bottom:0px;
`
export const UserEmail = styled.p`
    font-size: 12px;
    line-height: 20px;
    color: rgba(255, 255, 255, 1);
    margin-bottom:0px
`

//subpanel

export const SubmenuPanel = styled.div`
  width:222px;
  background:
rgba(34, 41, 79, 0.9);
padding: 32px 28px;
hr{
  border: none;
    border-bottom: 1px solid rgba(226, 226, 238, 0.5);
    margin: 0px -28px;
}
 .ant-menu.ant-menu-dark {
    background: transparent;
}
.ant-menu-item{
    height: auto !important;
    padding: 5px 10px !important;
    line-height: 22px;
    font-size: 14px;
    color: #FFFFFF;
    font-weight: 400;
    margin:0px;
    margin-bottom:5px !important;
}
.ant-menu-item.active-child{
      background: rgba(23, 8, 45, 1);
    border-radius: 5px;
}
`
export const SubMenuWrapper = styled.div`
padding:45px 0px;
`
export const SubTitle = styled.div`
font-size:12px;
color:rgba(248, 248, 252, 0.7);
line-height:12px;
font-weight:600;
margin-bottom:20px;
    text-transform: uppercase;
`

export const MenuTitleWrapper = styled.div`
    color: #fff;
    font-size: 20px;
    line-height: 24px;
    font-weight: 500;
    padding-bottom: 26px;
    margin-bottom: 0px;
    img{
      padding-right:12px;
    }
    
  `