import React, { FC } from "react";
import { Heading, Icons, LogoImage } from "./style";
import { NotificationIcon, AlertSettingIcon } from "@assets/index";
import { Flex } from "@components/Flex";
import { Avatar } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { LogoutOutlined } from "@ant-design/icons";
import { authActions } from "@store/authReducer";
import Cookies from "js-cookie";
import { Menu, Dropdown } from "antd";
import logo from "@assets/logo.png";

import { H3Typography } from "@components/Typography/Typography";
import { userActions } from "@store/userProfileReducer";
import { navigate } from "@reach/router";
import NavHeader from "@components/NavHeader";
interface StaffHeaderProps {
  user: any;
}

const Header: FC<StaffHeaderProps> = ({ children, user }) => {
  return (
    <>
      <NavHeader>
        <Flex flex={1} alignItems={"center"}>
          <LogoImage src={logo} />
          <H3Typography style={{ color: "white", marginLeft: 20 }}>
            Right at Home, Right for me.
          </H3Typography>
        </Flex>
      </NavHeader>
    </>
  );
};

export default Header;
