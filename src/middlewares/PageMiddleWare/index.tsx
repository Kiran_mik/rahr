import Header from "@layout/header";
import SideBar from "@layout/SideBar";
import AuthPages from "@pages/AuthPages";
import CreateNewPassword from "@pages/AuthPages/PasswordRecoveryPage/CreateNewPassword";
import PasswordReset from "@pages/AuthPages/PasswordRecoveryPage/PasswordReset";
import RecoveryLink from "@pages/AuthPages/PasswordRecoveryPage/RecoveryLink";
import RecoveryLogin from "@pages/AuthPages/PasswordRecoveryPage/RecoveryLogin";
import SignInPage from "@pages/AuthPages/SignInPage";
import SignUpPage from "@pages/AuthPages/SignUpPage";
import PageNotFound from "@pages/PageNotFound";
import SetAgentPasswordPages from "@pages/SetAgentPasswordPages";
import { navigate, Router } from "@reach/router";
import { updateUserStore } from "@services/UserService";
import { ROLE_ROUTE_MAP } from "@utils/helpers";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Container } from "./style";


function PageMiddleWare(props: {
  path: string;
  location?: any;
  children: any;
}) {
  const auth = useSelector(
    (state: { auth: { token: string | null } }) => state.auth
  );
  const [loading, setLoading] = useState(false);
  const user = useSelector((state: any) => {
    return state.user;
  });
  const dispatch = useDispatch();

  useEffect(() => {
    if (auth.token) {
      storeUserInfo();
    }
  }, [auth.token, user.role]);

  const storeUserInfo = async () => {
    setLoading(true);
    await updateUserStore();
    //@ts-ignore
    if (props.location.pathname.split("/").length <= 2) {
      const path = ROLE_ROUTE_MAP[user.role || 0];
      navigate("/");
    }

    setLoading(false);
  };

  const showHeader = user.role !== 1 && auth.token;
  const isAuthValid = user && user.role && auth.token;
  const showDashboard = isAuthValid && user.role === 1;
  const showAgentDashboard = isAuthValid && user.role === 2;
  return (
    <>
      {!loading && (
        <>
          {showHeader && <Header user={user} />}

          <Container style={{ display: "flex" }}>
            {showDashboard && (
              <>
                <div className={"sidePanel"}>
                  <SideBar />
                </div>

                {props.children}
              </>
            )}
          </Container>
          {showAgentDashboard && props.children}
          {!auth.token && (
            <>
              <Header user={user} />
              <Router>
                <AuthPages path="/">
                  <PageNotFound default />
                  <SignInPage path={"/"} />
                  <SignUpPage path={"/sign-up"} />
                  <RecoveryLogin path={"/password-recovery"} />
                  <RecoveryLink path={"/recovery-link"} />
                  <PasswordReset path={"/password-reset-send"} />
                  <CreateNewPassword path={"login/password-reset/:token"} />
                  <SetAgentPasswordPages path={"/set-password/:token"} />
                </AuthPages>
              </Router>
            </>
          )}
        </>
      )}
    </>
  );
}

export default PageMiddleWare;
