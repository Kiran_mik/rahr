import React, { useEffect, useState, useCallback } from "react";
import { TableWrapper } from "@pages/UserManagementPages/RAHStaffListPage/style";
import { getColumns } from "./Columns";
import DataTable from "@components/DataTable/DataTable";
import { getAgentList, rowSelection } from "./helper";
import { message } from "antd";
import { navigate } from "@reach/router";
import { debounce } from 'lodash';


const AgentListTable = (props: any) => {
  const { inputKey, filterVal } = props
  const [selectionType, setSelectionType] = useState<"checkbox">("checkbox");
  const [page, setPage] = useState<number>(1);
  const [dataSource, setDataSource] = useState<Array<any>>([]);
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    getPositionsList(inputKey, filterVal);
  }, [inputKey, filterVal]);

  const getPositionsList = useCallback(
    debounce(async (inputKey?: string, filterVal?: string) => {
      setLoading(true);
      const response = (await getAgentList(inputKey || '', filterVal || '')) as { data: Array<any>; error: any };
      if (!response.error) {
        setDataSource(response.data);
      } else {
        message.error("Something went wrong!");
      }
      setLoading(false);
    }, 300),
    [],
  );


  const onRowSelect = (row: any, type: string) => {
    if (type === "redirect") {
      navigate(`/agent/agent-review/${row.id}/1`);
    }
  };
  const cols = getColumns(onRowSelect);
  return (
    <>
      <TableWrapper>
        {/* <DataTable */}
        <DataTable
          loading={loading}
          rowSelection={{
            type: selectionType,
            ...rowSelection,
          }}
          columns={cols}
          dataSource={dataSource}
          pagination={false}
        />
      </TableWrapper>
    </>
  );
};
export default AgentListTable;
