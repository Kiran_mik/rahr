import { ThreeDots } from "@assets/index";
import { DropdownStyle } from "@pages/UserManagementPages/RAHStaffListPage/style";
import { Dropdown, Menu } from "antd";
import React from "react";

export interface DataType {
  board: string;
  abbreviation: string;
  email: string;
  address: string;
}

export const menu = (id: number) => (
  <Menu className="MenuItems">
    <Menu.Item key="0">
      {/* <Link
          style={{ color: "#17082d" }}
          to={`/usermanagement/department-detail/`}
        > */}
      Edit
      {/* </Link> */}
    </Menu.Item>
    <Menu.Item key="1">Delete</Menu.Item>
  </Menu>
);

export const BoardTableColumns = [
  {
    title: "Board",
    dataIndex: "board",
    key: "board",
    sorter: (a: any, b: any) => a.board.localeCompare(b.board),
    render: (board: string) => {
      return <div style={{ color: "#17082D" }}>{board}</div>;
    }
  },
  {
    title: "Abbreviation",
    dataIndex: "abbreviation",
    key: "abbreviation",
    sorter: (a: any, b: any) => a.abbreviation.localeCompare(b.abbreviation)
  },
  {
    title: "Email",
    dataIndex: "email",
    key: "email",
    render: (email: string) => {
      return <div style={{ color: "#531DAB" }}>{email}</div>;
    }
  },
  {
    title: "Address",
    dataIndex: "address",
    key: "address"
  },
  {
    title: "",
    dataIndex: "board",
    key: "board",
    render: (row: { id: number; index: number }) => {
      return (
        <DropdownStyle key={row.id + row.index} style={{ cursor: "pointer" }}>
          <Dropdown overlay={menu(row.id)} trigger={["click"]}>
            <img
              src={ThreeDots}
              alt={ThreeDots}
              onClick={e => e.preventDefault()}
            />
          </Dropdown>
        </DropdownStyle>
      );
    }
  }
];
export const dataSource = [
  {
    board: "Barrie & District Association of Realtors",
    abbreviation: "BDAR",
    email: "info@bdar.ca",
    address: "676 Veterans Dr, Barrie, ON L9J 0H6"
  },
  {
    board: "Brampton Real Estate Board Registration",
    abbreviation: "BREB",
    email: "info@breb@org",
    address: "60 Gillingham Drive, Suite 401 Brampton, ON, L6X 0Z9"
  }
];
