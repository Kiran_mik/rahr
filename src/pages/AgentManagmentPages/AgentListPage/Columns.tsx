import React from "react";
import { Dropdown, Menu } from "antd";
import { DropdownStyle } from "./style";
import { Link } from "@reach/router";
import { ThreeDots } from "@assets/index";

export const menu = (id: number) => (
  <Menu className="MenuItems">
    <Menu.Item key="0">
      <Link style={{ color: "#17082d" }} to={``}>
        Update
      </Link>
    </Menu.Item>
    <Menu.Item key="1">Deactivate</Menu.Item>
  </Menu>
);

interface ObjectNumberKey {
  [key: number]: any;
}
//1= Active, 2=Inactive, 3=Invite sent,  4=Pending Review, 5=Awaiting doc, 6=Parked, 7 = OffBoarding Request
const STATUS_MAP: ObjectNumberKey = {
  1: { title: "Active", color: "#219653"  },
    2: { title: "Inactive" , color: "#A2A2BA" },
    3: { title: "Invite Sent", color: "#0711F7" },
    4: { title: "Pending Review", color: "#F5222D"  },
    5: { title: "Awaiting documents", color: "#07A0F7"  },
    6: { title: "Parked", color: "#FBA63B"  },
    7: { title: "Offboarding", color: "#DC29EC"  },
    8: { title: "Terminated", color: "red"  },
    
  };
export const getColumns = (onRowSelect: (row: any, type: string) => void) => [
  {
    title: "Agent Name",

    dataIndex: "name",
    key: "name",
    sorter: (a: any, b: any) => a.name.localeCompare(b.name),
    render: (name: string, agent: any) => {
      return (
        <div
          onClick={() => {
            onRowSelect(agent.row, "redirect");
          }}
          style={{ fontWeight: 500, cursor: "pointer" }}
        >
          {name}
        </div>
      );
    },
  },
  {
    title: "Start Date",

    dataIndex: "startDate",
    key: "startDate",
  },
  {
    title: "BrokerWolf ID",

    dataIndex: "brokerWolfId",
    key: "brokerWolfId",
    // sorter: (a: any, b: any) => a.brokerWolfId.localeCompare(b.brokerWolfId),
  },
  {
    title: "Status",

    dataIndex: "status",
    key: "status",
    render: (status: number) => {
      return (
        !!STATUS_MAP[status] && (
          <span style={{ color: STATUS_MAP[status].color }}>
            {STATUS_MAP[status].title}
          </span>
        )
      );
    },
    // sorter: (a: any, b: any) => a.status.localeCompare(b.status),
  },
  {
    title: "",
    dataIndex: "row",
    key: "row",
    render: (row: { id: number; index: number }) => {
      return (
        <DropdownStyle key={row.id + row.index} style={{ cursor: "pointer" }}>
          <Dropdown overlay={menu(row.id)} trigger={["click"]}>
            <img
              src={ThreeDots}
              alt={ThreeDots}
              onClick={(e) => e.preventDefault()}
            />
          </Dropdown>
        </DropdownStyle>
      );
    },
  },
];
