
import services from "@services/index";
export interface DataType {
  key: React.Key;
  id?: number;
  name?: string;
  startDate?: string;
  brokerWolfId?: string;
  status?: string;
  row: { id: string }
  // userObject?: any;
  // departmentNameAndId?: any;
}

export const rowSelection = {
  onChange: (selectedRowKeys: React.Key[], selectedRows: DataType[]) => { },
  getCheckboxProps: (record: DataType) => ({
    disabled: record.row.id === "Disabled User", // Column configuration not to be checked
    name: record.row.id
  })
};


export const getAgentList = async (input: string, type: string) => {
  try {
    const res = await services.get(`v1/agents?status=${type}&search=${input}`, process.env.REACT_APP_BASE_URL) as { data: { data: any } }
    return { data: transformAgentList(res.data.data), error: false }
  } catch (err) {
    return { data: [], error: err }
  }
};

export const transformAgentList = (agents: Array<any>) => {
  return agents.map((agent: any, index: number) => {
    return {
      key: agent.id,
      name: agent.profile_detail.full_name,
      startDate: agent.profile_info && agent.profile_info.start_date,
      status: agent.status,
      brokerWolfId: agent.broker_wolf_id !== "null" ? agent.brokerWolfId : '',
      row: {
        ...agent
      }
    }
  })
}