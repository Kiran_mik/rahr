import React, { useState } from "react";
import { navigate } from "@reach/router";
import { Secondary } from "@components/Buttons";
import StaffHeader from "@components/StaffHeader";
import { H3 } from "@components/Typography/Typography";
import {
  InputAndButton,
  NavigateButton
} from "@pages/UserManagementPages/RAHStaffListPage/style";
import DashboardLayout from "@pages/UserManagementPages/StaffMembers/DashboardLayout";
import { SearchBar } from "@components/SerachBar";
import AgentListTable from "./AgentListTable";
import FilterComponent from "@components/FilterComponent";

function AgentListPage(props: { path: string }) {
  const [inputVal, setInputVal] = useState('')
  const [filterVal, setFilterval] = useState('')
  const redirect = () => {
    navigate("/agent/new-agent");
  };
  return (
    <DashboardLayout>
      <StaffHeader>
        <H3 text="Agents" />
      </StaffHeader>
      <InputAndButton>
        <SearchBar
          className="SearchInput"
          placeholder="Search by agent name, branch or status"
          onChange={(e: any) => {
            setInputVal(e.target.value)
          }}
        />
        <FilterComponent setFilterval={(val: string) => setFilterval(val)} />
        <NavigateButton onClick={redirect}>
          <Secondary text="+ Add new Agent" />
        </NavigateButton>
      </InputAndButton>
      <AgentListTable inputKey={inputVal} filterVal={filterVal} />
    </DashboardLayout>
  );
}

export default AgentListPage;
