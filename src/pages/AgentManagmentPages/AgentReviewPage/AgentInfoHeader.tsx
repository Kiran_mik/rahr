import React, { useState } from "react";
import { Avatar, Progress } from "antd";
import { UserAddOutlined } from "@ant-design/icons";

import { Flex } from "@components/Flex";
import { H4 } from "@components/Typography/Typography";
import { Primary } from "@components/Buttons/index";
import StatusItem from "./StatusItem";
import { AgentInfoContainer } from "./style";
import { exportOptions, NEW_OR_TERMINATING_ITEMS, TERMINATED_OR_ACTIVE_ITEMS } from "./helper";
import { AGENT_STATUS_TEXT, USER_STATUS_ENUM, USER_STATUS_TEXT_ENUM } from "@constants/userConstants";
import { ThreeDots } from "@assets/index";
import OptionsGroup from "@components/OptionGroup";
import { Li, Ul } from "@components/GoogleAutoComplete/style";

function AgentInfoHeader(props: any) {
  const { progressStatus, buttonText, onChange, userData, showProgress, progress } = props
  const [showlist, setShowList] = useState(false)
  const [showOptions, setShowOptions] = useState(false)

  //@ts-ignore
  let items: any[] = [];
  let displayColor = ''
  const DisplayItems = (value: any) => {
    displayColor = value
    switch (value) {
      case "Terminated":
        items = TERMINATED_OR_ACTIVE_ITEMS(userData.branch_name, userData.broker_wolf_id, userData.terminate_date)
        break;
      case "Parked":
        items = TERMINATED_OR_ACTIVE_ITEMS(userData.branch_name, userData.broker_wolf_id, userData.terminate_date)
        break;
      case "Active":
        items = TERMINATED_OR_ACTIVE_ITEMS(userData.branch_name, userData.broker_wolf_id, userData.start_date)
        break;
      case "Invite sent":
        items = NEW_OR_TERMINATING_ITEMS(userData.branch_name, userData.start_date, userData.ticket_no, userData.broker_wolf_id)
        break;
      case "Pending Review":
        items = NEW_OR_TERMINATING_ITEMS(userData.branch_name, userData.start_date, userData.ticket_no, userData.broker_wolf_id)
        break;
      case "Awaiting doc":
        items = NEW_OR_TERMINATING_ITEMS(userData.branch_name, userData.start_date, userData.ticket_no, userData.broker_wolf_id)
        break;
      default:
        items = TERMINATED_OR_ACTIVE_ITEMS('-', '-', '-')
    }
  }
  DisplayItems(USER_STATUS_TEXT_ENUM[userData.status])
  const handleChange = (value: any) => {
    setShowList(false)
  }

  return (
    <AgentInfoContainer>
      <Flex alignItems={"flex-start"} justifyContent={"space-between"}>
        <Flex>
          <Avatar
            style={{ width: 60, height: 60 }}
            icon={<img src={userData.profile_photo} alt='user avatar' />}
          />
          <Flex direction="column" left={10}>
            <H4 text={userData.full_name} />
            <span style={{ color: (displayColor === "Parked" || displayColor === "Terminated") ? "#FF4D4F" : "#9792E3", fontWeight: "bold" }}>
              {AGENT_STATUS_TEXT[userData.status]}
            </span>
          </Flex>
        </Flex>
        {items.map((item: any) => {
          return <StatusItem key={item.label} {...item} />;
        })}
        {userData.status === USER_STATUS_ENUM.PENDING_REVIEW &&
          <Flex left={60} top={10} onClick={() => setShowList(true)}>
            <Avatar icon={<UserAddOutlined />} />
          </Flex>
        }
        {showlist &&
          <OptionsGroup onChange={handleChange} />
        }
      </Flex>
      {userData.status === USER_STATUS_ENUM.PENDING_REVIEW && showProgress &&
        <Flex alignItems={"flex-start"} top={40}>
          <Flex direction="column" flex={1}>
            <span>{progressStatus}</span>
            <Progress
              strokeColor={{
                "0%": "#31AF91",
                "100%": "#31AF91",
              }}
              percent={progress}
            />
          </Flex>
          <Flex left={50}>
            <Primary text={buttonText} onClick={() => onChange('value updated')} isDisable={true} />
          </Flex>
          <Flex left={50} style={{ width: 46, height: 46, border: "1px solid #BDBDD3", borderRadius: "8px", textAlign: "center", cursor: "pointer" }} onClick={() => setShowOptions(true)}>
            <img src={ThreeDots} alt="" width="20" style={{ margin: "0 auto" }} />
          </Flex>
          {showOptions &&
            <Flex left={50}>
              <Ul>
                {exportOptions.map((place) => {
                  return (
                    <Li
                      onClick={(e: any) => {
                        console.log('place')
                        setShowOptions(false)
                      }}
                    >
                      {place.description}
                    </Li>
                  );
                })}
              </Ul>
            </Flex>
          }
        </Flex>
      }
    </AgentInfoContainer>
  );
}

export default AgentInfoHeader;
