import React, { ReactNode, useEffect, useState } from "react";
import { Tabs } from "antd";
import { TabLayoutContainer } from "./style";
import { ObjectStr } from "@utils/helpers";
import ContactDetailsTabSection from "./ContactDetailsTabSection";
import ProfileInformationTabSection from "./ProfileInformationTabSection";
import EmploymentStatusTabSection from "./EmploymentStatusTabSection";
import BillingAndCommisionTabSection from "./BillingAndCommisionTabSection";
import CharityAndEnrollmentTabSection from "./CharityAndEnrollmentTabSection";
import FormsTabSection from "./FormsTabSection";
import { navigate, useParams, useLocation } from "@reach/router";
import { getTabsData } from "./helper";
import Loader from "@components/Loader";
import { Absolute } from "@components/Flex/style";
import { updateRawData } from "@utils/helpers";
import Termination from "../TerminationDetails/Termination";
import Parking from "../ParkingDetails/Parking";
import CloseIcon from "@assets/close-circle.svg";
import ApproveIcon from "@assets/RightIcon.svg";
import PendingIcon from "@assets/PendingIcon.svg";
const { TabPane } = Tabs;
const TAB_STATUS_ICON: { [key: number]: ReactNode } = {
  0: <img src={PendingIcon} style={{ marginLeft: "4px" }} />,
  1: <img src={ApproveIcon} style={{ marginLeft: "4px" }} />,
  2: <img src={CloseIcon} style={{ marginLeft: "4px" }} />,
};
function AgentReviewTabLayout(props: any) {
  const {
    isTerminating,
    isParking,
    tabStatuses,
    isInReview,
    updateTabStatuses,
    finalButtonState,
    finalFormSubmit,
    finalSubmitButtonLoadingStatus,
  } = props;
  let INITIAL_TABS_COMPONENT_MAP: ObjectStr = {
    "Termination": {
      component: (props: any) => <Termination {...props} />,
      key: 8,
      getUrl: "temination",
      agentStatus: props.agentStatus
    },
    "Contact Details": {
      component: (props: any) => <ContactDetailsTabSection {...props} />,
      key: 1,
      getUrl: "contact-details",
    },
    "Profile Information": {
      component: (props: any) => <ProfileInformationTabSection {...props} />,
      key: 2,
      getUrl: "profile-details",
    },
    "Employment Status": {
      component: (props: any) => <EmploymentStatusTabSection {...props} />,
      key: 3,
      getUrl: "employment-status",
    },
    "Billing & Commision": {
      component: (props: any) => <BillingAndCommisionTabSection {...props} />,
      key: 5,
      getUrl: "billing-commissions",
    },
    "Charity Enrollment": {
      component: (props: any) => <CharityAndEnrollmentTabSection {...props} />,
      key: 6,
      getUrl: "charity-details",
    },
    Forms: {
      component: (props: any) => <FormsTabSection {...props}
        finalButtonState={finalButtonState}
        finalFormSubmit={finalFormSubmit}
        finalSubmitButtonLoadingStatus={finalSubmitButtonLoadingStatus} />,
      key: 7,
      getUrl: "agreement-details",
    },
  };
  const [isLoading, setIsLoading] = useState(false);
  const [data, setData] = useState({} as any);
  const params = useParams();
  const location = useLocation();
  const formType = params.form;
  const agentId = params.agentId;
  const [TABS_COMPONENT_MAP, SET_TABS_COMPONENT_MAP] = useState(INITIAL_TABS_COMPONENT_MAP)
  // console.log(params, location)


  useEffect(() => {
    if (isTerminating) {
      SET_TABS_COMPONENT_MAP({
        ...{
          "Termination": {
            component: (props: any) => <Termination {...props} />,
            key: 8,
            getUrl: "temination",
            agentStatus: props.agentStatus
          },
        }, ...TABS_COMPONENT_MAP
      })
    }
    if (isParking) {
      SET_TABS_COMPONENT_MAP({
        ...{
          "Parking": {
            component: (props: any) => <Parking {...props} />,
            key: 9,
            getUrl: "parking",
            agentStatus: props.agentStatus
          },
        }, ...TABS_COMPONENT_MAP
      })
    }
  }, [isTerminating, isParking])
  useEffect(() => {
    fetchData();
  }, [location.pathname]);

  const fetchData = async (loaderType?: string) => {
    if (!loaderType) {
      setIsLoading(true);
    } else {
      updateRawData({ [loaderType]: true });
    }

    let endPoint = "";
    const urlKey = formType;

    const currentTab: any = Object.keys(TABS_COMPONENT_MAP).find((key) => {
      return TABS_COMPONENT_MAP[key].key == urlKey;
    });
    if (currentTab) {
      endPoint = TABS_COMPONENT_MAP[currentTab]["getUrl"];
    }
    const res: any = await getTabsData(endPoint, agentId);
    updateTabStatuses();
    setData(res);
    setIsLoading(false);
    if (loaderType) {
      updateRawData({ [loaderType]: false });
    }

  };
  const gotoNextStep = (currentStep: number) => {
    const nextStep = currentStep == 3 ? 5 : currentStep + 1;
    navigate(`/agent/agent-review/${agentId}/${nextStep}`);
  };

  const gotoPrevStep = (currentStep: number) => {
    const nextStep = currentStep == 5 ? 3 : currentStep - 1;
    navigate(`/agent/agent-review/${agentId}/${nextStep}`);
  };
  return (
    <TabLayoutContainer>
      <div className="card-container">
        <Tabs
          type="card"
          defaultActiveKey={formType}
          onTabClick={(tab: string) => {
            let tabBaseUrl = `/agent/agent-review/`;
            if (isTerminating) tabBaseUrl += 'terminating/'
            if (isParking) tabBaseUrl += 'parking/'
            navigate(`${tabBaseUrl}${agentId}/${tab}`);
          }}
          activeKey={formType}
        >
          {Object.keys(TABS_COMPONENT_MAP).map((i: string) => {
            const tabObject = TABS_COMPONENT_MAP[i];
            tabObject.data = data;
            tabObject.loading = isLoading;
            tabObject.fetchApiCallback = fetchData;
            tabObject.isInReview = isInReview;
            tabObject.gotoNextStep = gotoNextStep;
            tabObject.gotoPrevStep = gotoPrevStep;
            tabObject.currentStep = tabObject.key;
            tabObject.updateTabStatuses = updateTabStatuses;
            tabObject.tabStatuses = tabStatuses;
            tabObject.userData = props.userData;
            tabObject.headerRefreshCallback = props.headerRefreshCallback;
            let statusIcon = null;
            if (isInReview) {
              const currentTabStatus = tabStatuses[tabObject.key];
              statusIcon = TAB_STATUS_ICON[currentTabStatus];
            }
            tabObject.agentId = agentId;
            return (
              <TabPane
                tab={(<>
                  <div className="tabtitle"> {i}</div>
                  {statusIcon}
                </>)}
                key={tabObject.key.toString()}
              >
                {tabObject.component(tabObject)}
              </TabPane>
            );
          })}
        </Tabs>
      </div>
      <Absolute top={"40%"} left={"50%"}>
        <Loader loading={isLoading} />
      </Absolute>
    </TabLayoutContainer>
  );
}

export default AgentReviewTabLayout;
