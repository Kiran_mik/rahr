import React from "react";
import { Flex } from "@components/Flex";
import { H4 } from "@components/Typography/Typography";
import { TabSectionFieldLabel, TabSectionFieldValue } from "../style";

function BankInformation(props: {
    accountNumber:string;
    institutionNumber:string;
    transitionNumber:string;
}) {
    const { accountNumber, institutionNumber, transitionNumber } = props;
    return (
        <Flex direction="column" flex={1}>
            <Flex alignItems={"baseline"}>
                <Flex flex={1}>
                    <H4 text="Bank information" />
                </Flex>
            </Flex>
            <Flex top={20} direction="column" alignItems="baseline">
                <Flex
                    flex={1}
                    justifyContent="space-between"
                    style={{ width: "100%" }}
                >
                    <Flex direction="column" flex={1}>
                        <TabSectionFieldLabel>Account number</TabSectionFieldLabel>
                        <TabSectionFieldValue>{accountNumber}</TabSectionFieldValue>
                    </Flex>
                    <Flex direction="column" flex={1}>
                        <TabSectionFieldLabel>Institution number</TabSectionFieldLabel>
                        <TabSectionFieldValue>{institutionNumber}</TabSectionFieldValue>
                    </Flex>
                    <Flex direction="column" flex={1}>
                        <TabSectionFieldLabel>Transition number</TabSectionFieldLabel>
                        <TabSectionFieldValue>{transitionNumber}</TabSectionFieldValue>
                    </Flex>
                    <Flex direction="column" flex={1} />
                </Flex>
            </Flex>
        </Flex>
    );
}

export default BankInformation;
