import React from "react";
import { Flex } from "@components/Flex";
import {
  H4,
} from "@components/Typography/Typography";
import {
  TabSectionFieldLabel,
  TabSectionFieldValue,
} from "../style";
import DocumentImg from "@assets/icon-form.svg";
import OnboardingFormButton from "@components/OnboardingFormButton";
import { BILLIN_MODE_ENUM } from "@pages/AgentOnboardingPages/BillingCommission/helper";
import CreditCardSection from "./CreditCardSection";
import BankInformation from "./BankInformation";
import SubmitReviewSection from "../SubmitReviewSection";

function BillingSection(props: any) {
  const {
    billingAndCommisionInfo,
    getUrl,
    fetchApiCallback,
    review,
    isInReview
  } = props;
  const formDetails = { id: "", docusign_id: "", form_link: billingAndCommisionInfo.quarterly_fees_autodebit_authorization_signed_document, form_name: "" };
  return (
    <SubmitReviewSection
      title="Billing"
      review={review || []}
      endPoint={getUrl}
      block="billing"
      fetchApiCallback={fetchApiCallback}
      isInReview={isInReview}
    >

      <Flex flex={1} top={20}>
        <H4 text="Billing method" />
      </Flex>
      <Flex direction="column" top={10}>
        <TabSectionFieldLabel>Selected method</TabSectionFieldLabel>
        <TabSectionFieldValue>{billingAndCommisionInfo.billing_method === BILLIN_MODE_ENUM.PRE_AUTHORIZE_DEBIT ? "Pre-athorized debit" : "Credit card"}</TabSectionFieldValue>
      </Flex>
      {billingAndCommisionInfo.billing_method === BILLIN_MODE_ENUM.PRE_AUTHORIZE_DEBIT && (<Flex top={20}>
        <BankInformation
          accountNumber={billingAndCommisionInfo.billing_account_no}
          institutionNumber={billingAndCommisionInfo.billing_institution_no}
          transitionNumber={billingAndCommisionInfo.billing_transit_number}
        />
      </Flex>)}
      {billingAndCommisionInfo.billing_method === BILLIN_MODE_ENUM.CREDIT_CARD && (
        <CreditCardSection
          title="Credit card details"
          creditCardNumber={billingAndCommisionInfo.billing_credit_card_no}
          expiryDate={billingAndCommisionInfo.billing_expire_date}
          cvc={billingAndCommisionInfo.billing_cvc}
        />)}

      <Flex top={20} direction="column">
        <Flex flex={1}>
          <H4 text="Authorization agreement" />
        </Flex>
        <Flex top={10}>
          <OnboardingFormButton
            buttonText="View & Sign"
            image={DocumentImg}
            text="Quarterly fees auto-debit authorization	"
            formDetails={formDetails}
          />
        </Flex>
      </Flex>
      <CreditCardSection
        title="Alternative billing method"
        creditCardNumber={billingAndCommisionInfo.credit_card_no}
        expiryDate={billingAndCommisionInfo.expire_date}
        cvc={billingAndCommisionInfo.cvc}
      />
    </SubmitReviewSection>
  );
}

export default BillingSection;
