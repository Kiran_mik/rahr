import React from "react";
import { Flex } from "@components/Flex";
import { H4 } from "@components/Typography/Typography";
import { TabSectionFieldLabel, TabSectionFieldValue } from "../style";

function CommisionPayoutAddressSection(props:any) {
  const {billingAndCommisionInfo} = props;
  if(!billingAndCommisionInfo.commission_mailing_address_data) return null;
  return (
    <>
      <Flex alignItems="baseline" top={20}>
        <Flex flex={1}>
          <H4 text="Address details" />
        </Flex>
      </Flex>
      <Flex top={20} direction="column" alignItems="baseline">
        <Flex
          flex={1}
          justifyContent="space-between"
          style={{ width: "100%" }}
        >
          <Flex direction="column" flex={1}>
            <TabSectionFieldLabel>Address #1</TabSectionFieldLabel>
            <TabSectionFieldValue>{billingAndCommisionInfo.commission_mailing_address_data.address_1}</TabSectionFieldValue>
          </Flex>
          <Flex direction="column" flex={1}>
            <TabSectionFieldLabel>Address #2</TabSectionFieldLabel>
            <TabSectionFieldValue>{billingAndCommisionInfo.commission_mailing_address_data.address_2}</TabSectionFieldValue>
          </Flex>
          <Flex direction="column" flex={1}>
            <TabSectionFieldLabel>Province</TabSectionFieldLabel>
            <TabSectionFieldValue>{billingAndCommisionInfo.commission_mailing_address_data.province}</TabSectionFieldValue>
          </Flex>
          <Flex direction="column" flex={1} />
        </Flex>

        <Flex
          flex={1}
          justifyContent="space-between"
          style={{ width: "100%" }}
          top={10}
        >
          <Flex direction="column" flex={1}>
            <TabSectionFieldLabel>City</TabSectionFieldLabel>
            <TabSectionFieldValue>{billingAndCommisionInfo.commission_mailing_address_data.city}</TabSectionFieldValue>
          </Flex>
          <Flex direction="column" flex={1}>
            <TabSectionFieldLabel>Postal code</TabSectionFieldLabel>
            <TabSectionFieldValue>{billingAndCommisionInfo.commission_mailing_address_data.postal_code}</TabSectionFieldValue>
          </Flex>
          <Flex direction="column" flex={1} />
          <Flex direction="column" flex={1} />
        </Flex>
      </Flex>
    </>
  );
}

export default CommisionPayoutAddressSection;
