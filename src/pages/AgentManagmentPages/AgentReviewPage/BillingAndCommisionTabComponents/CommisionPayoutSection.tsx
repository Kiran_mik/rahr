import React from "react";
import { Flex } from "@components/Flex";
import {
  H4,
} from "@components/Typography/Typography";
import {
  TabSectionFieldLabel,
  TabSectionFieldValue,
  CheckedIcon,
} from "../style";
import DocumentImg from "@assets/icon-form.svg";
import OnboardingFormButton from "@components/OnboardingFormButton";

import CommisionPayoutAddressSection from "./CommisionPayoutAddressSection";
import { PAYMENT_MODE_ENUM } from "@pages/AgentOnboardingPages/BillingCommission/helper";
import BankInformation from "./BankInformation";
import SubmitReviewSection from "../SubmitReviewSection";

function CommisionPayoutSection(props: any) {
  const {
    billingAndCommisionInfo,
    getUrl,
    fetchApiCallback,
    review,
    isInReview
  } = props;
  const formDetails = { id: "", form_name: "", form_link: billingAndCommisionInfo.commission_void_cheque_doc };
  return (
    <SubmitReviewSection
      title="Commission payout"
      review={review || []}
      endPoint={getUrl}
      block="commision_payout"
      fetchApiCallback={fetchApiCallback}
      isInReview={isInReview}
    >
      <Flex flex={1} top={20}>
        <H4 text="Preferred payout mode" />
      </Flex>
      <Flex direction="column" top={10}>
        <TabSectionFieldLabel>Selected method</TabSectionFieldLabel>
        <TabSectionFieldValue>{billingAndCommisionInfo.payout_mode == PAYMENT_MODE_ENUM.ELECTRONIC_FUND_TRANSFER ? "Electronic Fund Transfer" : "Cheque"}</TabSectionFieldValue>
      </Flex>
      {billingAndCommisionInfo.payout_mode == PAYMENT_MODE_ENUM.ELECTRONIC_FUND_TRANSFER && (
        <Flex top={20}>
          <BankInformation
            accountNumber={billingAndCommisionInfo.commission_account_no}
            institutionNumber={billingAndCommisionInfo.commission_institution_no}
            transitionNumber={billingAndCommisionInfo.commission_transit_number}
          />
        </Flex>
      )}

      <Flex top={20} direction="column">
        <Flex flex={1}>
          <H4 text="Attached VOID Cheque" />
        </Flex>
        <Flex top={10}>
          <OnboardingFormButton
            buttonText="View & Sign"
            image={DocumentImg}
            text="VOID Cheque"
            formDetails={formDetails}
          />
        </Flex>
      </Flex>
      {billingAndCommisionInfo.payout_mode == PAYMENT_MODE_ENUM.CHEQUE && (<CommisionPayoutAddressSection billingAndCommisionInfo={billingAndCommisionInfo} />)}

      <Flex flex={1} top={20}>
        <H4 text="Authorization agreement" />
      </Flex>
      <Flex top={10} direction="row" alignItems="center">
        {billingAndCommisionInfo.is_authorized_agreement && <CheckedIcon />}
        <TabSectionFieldValue>
          Agent gave authorization Right at Home Realty Inc. to pay out his/her
          commissions by cheque.
        </TabSectionFieldValue>
      </Flex>
    </SubmitReviewSection>
  );
}

export default CommisionPayoutSection;
