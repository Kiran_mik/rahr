import React, { useState } from "react";
import { Flex } from "@components/Flex";
import { H4 } from "@components/Typography/Typography";
import { CheckedIcon, TabSectionFieldLabel, TabSectionFieldValue } from "../style";
import { EditIconButton } from "@components/Buttons/index";
import FolderIcon from "@assets/icons/folder.svg";
import moment from "moment";

const CreditCardNumber = ({ cardNumber, mask }: {
    cardNumber: string;
    mask: boolean;
}) => {
    let lastFourDigits = cardNumber && cardNumber.slice(-4);
    if (mask)
        return <>**** **** **** {lastFourDigits}</>
    else {
        let cardArray = cardNumber.match(/.{1,4}/g);
        return <>
            {cardArray
                && cardArray.map((item, index) => (
                    <React.Fragment key={index}>{item} </React.Fragment>
                ))}</>
    }
}


const CreditCardCVC = ({ cvc, mask }: {
    cvc: string;
    mask: boolean;
}) => {
    if (mask)
        return <>***</>
    else
        return <>{cvc}</>
}


function CreditCardSection(props: any) {
    const { title, expiryDate, cvc, creditCardNumber } = props;
    const [maskDetails, setMaskDetails] = useState<boolean>(true);
    return (
        <>
            <Flex top={20}>
                <Flex direction="column" flex={1}>
                    <Flex alignItems={"baseline"}>
                        <Flex>
                            <H4 text={"Alternative billing method"} />
                        </Flex>
                        <EditIconButton
                            icon={FolderIcon}
                            onClick={() => setMaskDetails(!maskDetails)}
                            style={{
                                marginLeft: "10px",
                                borderColor: "#4E1C95",
                                padding: "3px 10px",
                            }}
                            text={<span style={{ color: "#4E1C95" }}>View details</span>}
                        />
                    </Flex>
                    <Flex top={20} direction="column" alignItems={"baseline"}>
                        <Flex
                            flex={1}
                            justifyContent={"space-between"}
                            style={{ width: "100%" }}
                        >
                            <Flex direction="column" flex={1}>
                                <TabSectionFieldLabel>{title}</TabSectionFieldLabel>
                                <TabSectionFieldValue>
                                    <CreditCardNumber
                                        cardNumber={creditCardNumber}
                                        mask={maskDetails}
                                    />
                                </TabSectionFieldValue>
                            </Flex>
                            <Flex direction="column" flex={1}>
                                <TabSectionFieldLabel>Expiration date</TabSectionFieldLabel>
                                <TabSectionFieldValue>{moment(expiryDate).format("MMMM, YYYY")}</TabSectionFieldValue>
                            </Flex>
                            <Flex direction="column" flex={1}>
                                <TabSectionFieldLabel>CVC</TabSectionFieldLabel>
                                <TabSectionFieldValue>
                                    <CreditCardCVC
                                        cvc={cvc}
                                        mask={maskDetails}
                                    />
                                </TabSectionFieldValue>
                            </Flex>
                            <Flex direction="column" flex={1} />
                        </Flex>
                    </Flex>
                </Flex>
            </Flex>
            <Flex top={20} direction="row" alignItems={"center"}>
                <CheckedIcon />
                <TabSectionFieldValue>Credit card authorized</TabSectionFieldValue>
            </Flex>
        </>
    );
}

export default CreditCardSection;
