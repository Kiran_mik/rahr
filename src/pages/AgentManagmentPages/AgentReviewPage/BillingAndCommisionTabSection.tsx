import React from "react";
import { H2 } from "@components/Typography/Typography";
import { TabItemLayout } from "./style";
import { Flex } from "@components/Flex";
import ExportIcon from "@assets/icons/export.png";
import {
  EditIconButton,
  Primary,
  Transparent,
} from "@components/Buttons/index";

//components
import CommisionPayoutSection from "./BillingAndCommisionTabComponents/CommisionPayoutSection";
import BillingSection from "./BillingAndCommisionTabComponents/BillingSection";

function BillingAndCommisionTabSection(props: any) {
  const { data: billingAndCommisionInfo, getUrl, fetchApiCallback, isInReview,gotoNextStep,currentStep,gotoPrevStep } = props;

  if (!billingAndCommisionInfo.payout_mode) return null;
  const otherProps = {
    review: billingAndCommisionInfo.review,
    getUrl,
    fetchApiCallback,
    isInReview
  };
  return (
    <TabItemLayout>
      <Flex>
        <Flex flex={1}>
          <H2 text="Billing & Comission" className="font-600" />
        </Flex>
        <EditIconButton
          icon={ExportIcon}
          text={
            <span style={{ fontWeight: 600, color: "#4E1C95", fontSize: 14 }}>
              Export for Broker Wolf
            </span>
          }
        />
      </Flex>
      <Flex top={20}>
        <CommisionPayoutSection
          billingAndCommisionInfo={billingAndCommisionInfo}
          {...otherProps}
        />
      </Flex>
      <Flex top={20}>
        <BillingSection
          billingAndCommisionInfo={billingAndCommisionInfo}
          {...otherProps}
        />
      </Flex>
      {isInReview && (
        <Flex className="footerbtn">
          <Primary
            text="Next step"
            className="submitbtn"
            style={{ marginRight: "2px" }}
            onClick={()=>{
              gotoNextStep(currentStep);
            }}
          />
          <Transparent text="Prev step" onClick={()=>{
              gotoPrevStep(currentStep);
            }}/>
        </Flex>
      )}
    </TabItemLayout>
  );
}

export default BillingAndCommisionTabSection;
