import { CheckCircleFilled } from "@ant-design/icons";
import { Flex } from "@components/Flex";
import { H3, H4 } from "@components/Typography/Typography";
import React from "react";
import {
  TabSectionComponentLayout,
  TabSectionFieldValue,
  TabSectionFieldLabel,
} from "../style";
const dashedBorderLayout = {
  border: "none",
  paddingBottom: 30,
  borderBottom: "1px dashed",
  borderRadius: 0,
  marginTop: 10,
  width: "88%",
};
function CharityContributionTabSection(props: any) {
  const { is_want_to_join_charity } = props;
  return (
    <Flex direction="column" style={{width:'100%'}}>
      <TabSectionComponentLayout
        style={{
          ...dashedBorderLayout,
          borderBottom: is_want_to_join_charity ? "1px dashed" : "none",
        }}
      >
        <Flex flex={1}>
          <H3 text={"Charity contributions"} />
        </Flex>
        <Flex direction="column" top={20}>
          <TabSectionFieldLabel style={{width:'100%'}}>
            Is the agent interested in contributing to the following charities?
          </TabSectionFieldLabel>
          <TabSectionFieldValue>
            {is_want_to_join_charity ? "Yes" : "No"}
          </TabSectionFieldValue>
        </Flex>

        {!!is_want_to_join_charity && (
          <Flex direction="column" top={20}>
            <Flex>
              <H4 text="Authorization agreement" />
            </Flex>
            <Flex top={20} alignItems={"center"}>
              <CheckCircleFilled style={{ color: "#31AF91" }} />
              <span
                style={{
                  color: "black",
                  fontWeight: 400,
                  fontSize: 14,
                  marginLeft: 10,
                }}
              >
                Agent agree to enroll in the Habitat for Humanity Program and/or
                Holland Bloorview Program and authorize Right at Home Realty
                Inc. to deduct the amount indicated below.
              </span>
            </Flex>
            <Flex top={10} alignItems={"center"}>
              <CheckCircleFilled style={{ color: "#31AF91" }} />
              <span
                style={{
                  color: "black",
                  fontWeight: 400,
                  fontSize: 14,
                  marginLeft: 10,
                }}
              >
                Agent understands that my contributions will be reflected in my
                T4A as a sale expense, and are fully deductible.
              </span>
            </Flex>
          </Flex>
        )}
      </TabSectionComponentLayout>
      {!!is_want_to_join_charity && (
        <>
          <TabSectionComponentLayout style={dashedBorderLayout}>
            <Flex direction="column" top={20}>
              <Flex>
                <H4 text="Programs agent enrolled" />
              </Flex>
            </Flex>
            <Flex direction="column" top={10}>
              <TabSectionFieldLabel>Selected programs</TabSectionFieldLabel>
              <TabSectionFieldValue>
                Habitat for Humanity program, Holland Bloorview Kids
                Rehabilitation Hospital program
              </TabSectionFieldValue>
            </Flex>
          </TabSectionComponentLayout>
          <TabSectionComponentLayout
            style={{
              ...dashedBorderLayout,
              borderBottom: "none",
              paddingBottom: 0,
            }}
          >
            <Flex direction="column" top={20}>
              <Flex>
                <H4 text="Contribution per deal" />
              </Flex>
            </Flex>
            <Flex direction="column" top={10}>
              <TabSectionFieldLabel>Selected contribution</TabSectionFieldLabel>
              <TabSectionFieldValue>$5 per program</TabSectionFieldValue>
            </Flex>
          </TabSectionComponentLayout>
        </>
      )}
    </Flex>
  );
}

export default CharityContributionTabSection;
