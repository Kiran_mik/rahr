import React, { useState } from "react";
import { EditIconButton, Primary, Transparent } from "@components/Buttons";
import { Flex } from "@components/Flex";
import { H2 } from "@components/Typography/Typography";
import ExportIcon from "@assets/icons/export.png";

import { TabItemLayout } from "./style";
import CharityContributionTabSection from "./CharityAndEnrollmentTabComponents/CharityContributionTabSection";
import { useParams } from "@reach/router";
import { changeReviewStatus } from "./helper";

function CharityAndEnrollmentTabSection(props: any) {
  const params = useParams();
  const { agentId } = params;
  const [isLoading, setLoading] = useState(false);
  const { getUrl, data: { charityDetailsInfo }, isInReview, gotoNextStep, tabStatuses, currentStep, updateTabStatuses, gotoPrevStep } = props;

  const submitCharitySectionReview = async () => {
    const body: any = {
      agent_id: agentId,
      review: {
        block: "charity_details",
        review: 1,
      },
    };
    return await changeReviewStatus(getUrl, body);
  }

  const handleSubmit = async () => {
    setLoading(true);
    // if( not already submitted)
    if (!tabStatuses[currentStep]) {
      const res: any = await submitCharitySectionReview();
      if (res.success) {
        updateTabStatuses();
      }
    }
    setLoading(false);
    gotoNextStep(currentStep);


  }
  return (
    <TabItemLayout>
      <Flex>
        <Flex flex={1}>
          <H2 text="Charity enrollment" className="font-600" />
        </Flex>
        <EditIconButton
          icon={ExportIcon}
          text={
            <span style={{ fontWeight: 600, color: "#4E1C95", fontSize: 14 }}>
              Export for Broker Wolf
            </span>
          }
        />
      </Flex>
      <Flex top={20}>
        <CharityContributionTabSection {...charityDetailsInfo} />
      </Flex>
      {isInReview && (
        <Flex className="footerbtn">
          <Primary
            text="Next step"
            className="submitbtn"
            style={{ marginRight: "2px" }}
            onClick={handleSubmit}
            isLoading={isLoading}
          />
          <Transparent text="Prev step" onClick={() => {
            gotoPrevStep(currentStep);
          }} />
        </Flex>
      )}

    </TabItemLayout>
  );
}

export default CharityAndEnrollmentTabSection;
