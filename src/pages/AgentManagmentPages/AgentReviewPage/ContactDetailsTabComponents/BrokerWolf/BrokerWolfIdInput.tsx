import React, { useEffect, useState } from "react";
import { Flex } from "@components/Flex";
import { CancelButton } from "./style";
import { Primary } from "@components/Buttons/index";
import { InputField } from "@components/Input/Input";
import { FlexBox } from "@pages/UserManagementPages/StaffMembers/NewStaff/style";

function BrokerWolfIdInputSection(props: any) {
  const { formik, onCancelPress, isUpdate } = props;
  let { values, touched, errors, handleChange, handleSubmit } = formik;
  return (
    <Flex flex={0.8} direction="row" alignItems={"center"}>
      <InputField
        style={{ backgroundColor: "#fff" }}
        placeholder="Enter BrowerWolfID"
        id="broker_wolf_id"
        name="broker_wolf_id"
        onChange={handleChange}
        value={values.broker_wolf_id}
        error={touched.broker_wolf_id && errors.broker_wolf_id}
      />
      <FlexBox className="review-btn">
        <Primary
          text={isUpdate ? "Update" : "Add ID"}
          className="submitbtn"
          onClick={handleSubmit}
        />
        <CancelButton
          onClick={() => {
            onCancelPress(false);
          }}
        >
          Cancel
        </CancelButton>
      </FlexBox>
    </Flex>
  );
}

export default BrokerWolfIdInputSection;
