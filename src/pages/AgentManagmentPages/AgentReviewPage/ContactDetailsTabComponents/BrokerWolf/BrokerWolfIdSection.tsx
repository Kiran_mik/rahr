import React, { useEffect, useState } from "react";
import { Flex } from "@components/Flex";
import { Alert } from "antd";
import { ContentWrapper, CancelButton } from "./style";
import { H4 } from "@components/Typography/Typography";
import { EditIconButton, Primary } from "@components/Buttons/index";
import { EditIconWhite, EditPencilIcon } from "@assets/index";
import { Formik } from "formik";
import {
  validationSchema,
  formInitialValues,
  saveBrokerWolfId,
} from "./helper";
import { InputField } from "@components/Input/Input";
import { FlexBox } from "@pages/UserManagementPages/StaffMembers/NewStaff/style";
import BrokerWolfIdInputSection from "./BrokerWolfIdInput";

function BrokerWolfIdSection(props: any) {
  const [viewType, setViewType] = useState<any>(null);
  const [data, setData] = useState(formInitialValues);

  useEffect(() => {
    let { broker_wolf_id } = props.userData;
    if (broker_wolf_id) {
      setData({
        broker_wolf_id: broker_wolf_id,
      });
      setViewType("idExist");
    }
  }, [props && props.userData]);

  const addButton = () => {
    return (
      <EditIconButton
        icon={EditIconWhite}
        onClick={() => {
          setViewType("add");
        }}
        style={{ backgroundColor: "#07A0F7" }}
        text={
          <span style={{ color: "#fff", fontSize: 14 }}>Add BrokerWolf ID</span>
        }
      />
    );
  };

  const editButton = () => {
    return (
      <EditIconButton
        icon={EditPencilIcon}
        onClick={() => {
          setViewType("edit");
        }}
        style={{ backgroundColor: "transparent", border: "none" }}
        text={<span style={{ color: "#000", fontSize: 14 }}>Edit</span>}
      />
    );
  };

  const inputSection = (isUpdate = false) => {
    return (
      <Formik
        initialValues={data}
        onSubmit={(values) => {
          let body = {
            agent_id: props.userData.user_id,
            broker_wolf_id: values.broker_wolf_id,
          };
          handleSubmitData(body);
        }}
        enableReinitialize={true}
        validationSchema={validationSchema}
      >
        {(formikProps) => {
          return (
            <BrokerWolfIdInputSection
              isUpdate={isUpdate}
              formik={formikProps}
              onCancelPress={() => {
                if (viewType == "edit") {
                  setViewType("idExist");
                } else {
                  setViewType(null);
                }
              }}
            />
          );
        }}
      </Formik>
    );
  };

  const handleSubmitData = async (data: any) => {
    let res: any = await saveBrokerWolfId(data);
    if (res) {
      if (props.headerRefreshCallback) {
        let headerCallback = props.headerRefreshCallback;
        headerCallback();
        setViewType("idExist");
        setData({
          broker_wolf_id: data.broker_wolf_id,
        });
      }
    }
  };

  return (
    <Flex direction="column" top={20}>
      <Flex
        direction="row"
        justifyContent={"space-between"}
        alignItems={"center"}
      >
        <H4 text={"BrokerWolf ID"} />
        {viewType == "idExist" ? editButton() : null}
      </Flex>
      <ContentWrapper>
        {viewType == "idExist" ? (
          <span>{data.broker_wolf_id}</span>
        ) : (
          <>
            <Flex
              direction="row"
              justifyContent={"space-between"}
              alignItems={"center"}
              className="container"
            >
              <Alert
                message={
                  viewType == "edit"
                    ? "update the BrokerWolf ID"
                    : "This agent does not have a BrokerWolf ID"
                }
                type="info"
                showIcon
              />
              {viewType == "add"
                ? inputSection()
                : viewType == "edit"
                ? inputSection(true)
                : addButton()}
            </Flex>
          </>
        )}
      </ContentWrapper>
    </Flex>
  );
}

export default BrokerWolfIdSection;
