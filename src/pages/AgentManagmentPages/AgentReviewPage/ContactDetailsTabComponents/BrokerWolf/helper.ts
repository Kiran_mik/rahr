import services from "@services/index";
import * as Yup from "yup";

export const validationSchema = Yup.object({
  //Contact information
  broker_wolf_id: Yup.string()
    .required("Please enter BrowerWolfID")
    .nullable(),
});

export const formInitialValues = {
  broker_wolf_id: "",
};

export const saveBrokerWolfId = async (body: any) => {
  try {
    const res = await services.post(
      `v1/agents/onboardings/broker-wolf`,
      body,
      process.env.REACT_APP_BASE_URL_AGENT
    );
    return true;
  } catch (error) {
    return false;
  }
};
