import styled from "styled-components";

export const ContentWrapper = styled.div`
  .container {
    margin-top: 10px;
    background-color: #e6f7ff;
    border: 1px solid #8fd4ff;
    border-radius: 8px;
    padding: 8px;
  }
  .ant-alert {
    padding: 0px 5px;
  }
  .ant-alert-info {
    background-color: #e6f7ff;
    border: none;
    border-radius: 0px;
    margin-bottom: 0px;
  }
  .ant-alert-info .ant-alert-icon {
    color: #1a90ff;
  }

  .review-btn {
    padding: 0px;
    margin-left: 10px;
    .submitbtn {
      background: #4e1c95;
      color: #fff;
      font-family: Poppins;
      font-size: 14px;
      line-height: 22px;
      border-radius: 8px;
      width: max-content;
      cursor: pointer;
    }
  }

  .lhIKys {
    margin-bottom: 0px;
  }
`;

export const CancelButton = styled.button`
  background-color: transparent;
  background: transparent;
  border-color: transparent;
  height: auto;
  font-size: 13px;
  font-weight: bold;
  color: #4e1c95;
  font-family: Poppins;
  cursor: pointer;
  position: relative;
  margin-left: 10px;
`;
