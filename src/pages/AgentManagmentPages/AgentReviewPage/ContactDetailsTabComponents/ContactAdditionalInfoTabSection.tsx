import { Flex } from "@components/Flex";
import SubmitReviewSection from "@pages/AgentManagmentPages/AgentReviewPage/SubmitReviewSection";
import React from "react";
import {
  TabSectionFieldLabel,
  TabSectionFieldValue
} from "../style";

function ContactAdditionalInfoTabSection(props: any) {
  const { getUrl, fetchApiCallback, review, how_did_you_here,isInReview } = props;
  return (
     
      <SubmitReviewSection
        title={"Additional information"}
        review={review || []}
        endPoint={getUrl}
        block={"contact_additional_details"}
        fetchApiCallback={fetchApiCallback}
        isInReview={isInReview}
      >
        <Flex top={20} direction="column" alignItems={"baseline"}>
          <Flex
            flex={1}
            justifyContent={"space-between"}
            style={{ width: "100%" }}
          >
            <Flex direction="column" flex={1}>
              <TabSectionFieldLabel>
                How did agent abour RAHR
              </TabSectionFieldLabel>
              <TabSectionFieldValue>
                {how_did_you_here && how_did_you_here.status_name}
              </TabSectionFieldValue>
            </Flex>
            <Flex direction="column" flex={1}>
              <TabSectionFieldLabel>Additonal details</TabSectionFieldLabel>
              <TabSectionFieldValue>-</TabSectionFieldValue>
            </Flex>

            <Flex direction="column" flex={1} />
            <Flex direction="column" flex={1} />
          </Flex>
        </Flex>
      </SubmitReviewSection>
   );
}

export default ContactAdditionalInfoTabSection;
