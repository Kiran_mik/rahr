import { Flex } from "@components/Flex";
import SubmitReviewSection from "@pages/AgentManagmentPages/AgentReviewPage/SubmitReviewSection";
import React from "react";
import {
  TabSectionFieldLabel,
  TabSectionFieldValue
} from "../style";


function ContactAddressDetailsTabSection(props: any) {
  const {
    getUrl,
    fetchApiCallback,
    review,
    address1,
    address2,
    province,
    city,
    postal_code,
    isInReview
  } = props;
  return (
       <SubmitReviewSection
        title="Address details"
        review={review || []}
        endPoint={getUrl}
        block={"contact_address_details"}
        fetchApiCallback={fetchApiCallback}
        isInReview={isInReview}
      >
        <>
          <Flex top={20} direction="column" alignItems={"baseline"}>
            <Flex
              flex={1}
              justifyContent={"space-between"}
              style={{ width: "100%" }}
            >
              <Flex direction="column" flex={1}>
                <TabSectionFieldLabel>Address #1</TabSectionFieldLabel>
                <TabSectionFieldValue>{address1}</TabSectionFieldValue>
              </Flex>
              <Flex direction="column" flex={1}>
                <TabSectionFieldLabel>Address #2</TabSectionFieldLabel>
                <TabSectionFieldValue>{address2}</TabSectionFieldValue>
              </Flex>
              <Flex direction="column" flex={1}>
                <TabSectionFieldLabel>Province</TabSectionFieldLabel>
                <TabSectionFieldValue>{province}</TabSectionFieldValue>
              </Flex>
              <Flex direction="column" flex={1} />
            </Flex>

            <Flex
              flex={1}
              justifyContent={"space-between"}
              style={{ width: "100%" }}
              top={10}
            >
              <Flex direction="column" flex={1}>
                <TabSectionFieldLabel>City</TabSectionFieldLabel>
                <TabSectionFieldValue>{city}</TabSectionFieldValue>
              </Flex>
              <Flex direction="column" flex={1}>
                <TabSectionFieldLabel>Postal code</TabSectionFieldLabel>
                <TabSectionFieldValue>{postal_code}</TabSectionFieldValue>
              </Flex>
              <Flex direction="column" flex={1} />
              <Flex direction="column" flex={1} />
            </Flex>
          </Flex>
        </>
      </SubmitReviewSection>
   );
}

export default ContactAddressDetailsTabSection;
