import { Flex } from "@components/Flex";
import SubmitReviewSection from "@pages/AgentManagmentPages/AgentReviewPage/SubmitReviewSection";
import { Avatar } from "antd";
import React from "react";
import {
  TabSectionFieldLabel,
  TabSectionFieldValue
} from "../style";

function ContactInformationTabSection(props: any) {
  const {
    email,
    profile_photo,
    first_name,
    cell_phone_provider,
    last_name,
    middle_name,
    cell_phone_number,
    getUrl,
    fetchApiCallback,
    review,
    isInReview
  } = props;
  return (
       <SubmitReviewSection
        title="Contact information"
        review={review || []}
        endPoint={getUrl}
        block={"contact_details"}
        fetchApiCallback={fetchApiCallback}
        isInReview={isInReview}
      >
        <Flex top={40}>
          <Flex direction="column">
            <TabSectionFieldLabel style={{ color: "#7B7B97", fontSize: 12 }}>
              Profile photo{" "}
              <TabSectionFieldValue style={{ color: "red" }}>
                *
              </TabSectionFieldValue>
            </TabSectionFieldLabel>
            <Avatar
              style={{ width: 200, height: 200 }}
              icon={<img src={profile_photo} />}
            />
          </Flex>
          <Flex flex={0.8} direction="column" alignItems={"baseline"} left={40}>
            <Flex
              flex={1}
              justifyContent={"space-between"}
              style={{ width: "100%" }}
            >
              <Flex direction="column" flex={1}>
                <TabSectionFieldLabel>First Name</TabSectionFieldLabel>
                <TabSectionFieldValue>{first_name}</TabSectionFieldValue>
              </Flex>

              <Flex direction="column" flex={1}>
                <TabSectionFieldLabel>Middle Name</TabSectionFieldLabel>
                <TabSectionFieldValue>
                  {middle_name || "-"}
                </TabSectionFieldValue>
              </Flex>

              <Flex direction="column" flex={1}>
                <TabSectionFieldLabel>Last Name</TabSectionFieldLabel>
                <TabSectionFieldValue>{last_name}</TabSectionFieldValue>
              </Flex>
            </Flex>

            <Flex
              flex={1}
              justifyContent={"space-between"}
              style={{ width: "100%" }}
            >
              <Flex direction="column" flex={1}>
                <TabSectionFieldLabel>Email</TabSectionFieldLabel>
                <TabSectionFieldValue>{email}</TabSectionFieldValue>
              </Flex>

              <Flex direction="column" flex={1}>
                <TabSectionFieldLabel>
                  Primary phone number
                </TabSectionFieldLabel>
                <TabSectionFieldValue>{cell_phone_number}</TabSectionFieldValue>
              </Flex>

              <Flex direction="column" flex={1}>
                <TabSectionFieldLabel>
                  Secondary phone number
                </TabSectionFieldLabel>
                <TabSectionFieldValue>-</TabSectionFieldValue>
              </Flex>
            </Flex>

            <Flex
              flex={1}
              justifyContent={"space-between"}
              style={{ width: "100%" }}
            >
              <Flex direction="column">
                <TabSectionFieldLabel>Cell phone provider</TabSectionFieldLabel>
                <TabSectionFieldValue>
                  {cell_phone_provider ? cell_phone_provider.status_name : "-"}
                </TabSectionFieldValue>
              </Flex>
            </Flex>
          </Flex>
        </Flex>
      </SubmitReviewSection>
   );
}

export default ContactInformationTabSection;
