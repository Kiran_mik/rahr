import Date from "@components/Date";
import { Flex } from "@components/Flex";
import SubmitReviewSection from "@pages/AgentManagmentPages/AgentReviewPage/SubmitReviewSection";
import React from "react";
import {
  TabSectionFieldLabel,
  TabSectionFieldValue
} from "../style";

function ContactPersonalDetailsTabSection(props: any) {
  const {
    getUrl,
    fetchApiCallback,
    review,
    dob,
    SIN_number,
    website_url,
    languages,
    gender,
    isInReview
  } = props;

  return (
       <SubmitReviewSection
        title={"Personal details"}
        review={review || []}
        endPoint={getUrl}
        block={"contact_personal_details"}
        fetchApiCallback={fetchApiCallback}
        isInReview={isInReview}
      >
        <Flex top={20} direction="column" alignItems={"baseline"}>
          <Flex
            flex={1}
            justifyContent={"space-between"}
            style={{ width: "100%" }}
          >
            <Flex direction="column" flex={1}>
              <TabSectionFieldLabel>Date of birth</TabSectionFieldLabel>
              <TabSectionFieldValue>
                <Date date={dob} format={"DD. MMMM, YYYY "} />
              </TabSectionFieldValue>
            </Flex>
            <Flex direction="column" flex={1}>
              <TabSectionFieldLabel>Sin number</TabSectionFieldLabel>
              <TabSectionFieldValue>{SIN_number}</TabSectionFieldValue>
            </Flex>
            <Flex direction="column" flex={1}>
              <TabSectionFieldLabel>Language spoken</TabSectionFieldLabel>
              <TabSectionFieldValue>{languages}</TabSectionFieldValue>
            </Flex>
            <Flex direction="column" flex={1}>
              <TabSectionFieldLabel>Website</TabSectionFieldLabel>
              <TabSectionFieldValue>{website_url}</TabSectionFieldValue>
            </Flex>
          </Flex>

          <Flex
            flex={1}
            justifyContent={"space-between"}
            style={{ width: "100%" }}
            top={10}
          >
            <Flex direction="column" flex={1}>
              <TabSectionFieldLabel>Gender</TabSectionFieldLabel>
              <TabSectionFieldValue>
                {gender ? gender.status_name : "-"}
              </TabSectionFieldValue>
            </Flex>
          </Flex>
        </Flex>
      </SubmitReviewSection>
   );
}

export default ContactPersonalDetailsTabSection;
