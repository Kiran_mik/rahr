import { Flex } from "@components/Flex";
import SubmitReviewSection from "@pages/AgentManagmentPages/AgentReviewPage/SubmitReviewSection";
import React from "react";
import {
  TabSectionFieldLabel,
  TabSectionFieldValue
} from "../style";

function EmergencyContactTabSection(props: any) {
  const { getUrl, fetchApiCallback, review, emergency_contact = [],isInReview } = props;
  const primaryEmergency = (emergency_contact && emergency_contact[0]) || {};
  const secondaryEmergency = (emergency_contact && emergency_contact[1]) || {};

  return (
       <SubmitReviewSection
        title={"Emergency contact"}
        review={review || []}
        endPoint={getUrl}
        block={"contact_emergency_details"}
        fetchApiCallback={fetchApiCallback}
        isInReview={isInReview}
      >
        <Flex top={20} direction="column" alignItems={"baseline"}>
          <Flex
            flex={1}
            justifyContent={"space-between"}
            style={{ width: "100%" }}
          >
            <Flex direction="column" flex={1}>
              <TabSectionFieldLabel>
                First emergency contact name
              </TabSectionFieldLabel>
              <TabSectionFieldValue>
                {primaryEmergency.contact_name}
              </TabSectionFieldValue>
            </Flex>
            <Flex direction="column" flex={1}>
              <TabSectionFieldLabel>
                Emergency contact number
              </TabSectionFieldLabel>
              <TabSectionFieldValue>
                {primaryEmergency.contact_number}
              </TabSectionFieldValue>
            </Flex>
            <Flex direction="column" flex={1}>
              <TabSectionFieldLabel>Relationship </TabSectionFieldLabel>
              <TabSectionFieldValue>Family member</TabSectionFieldValue>
            </Flex>
            <Flex direction="column" flex={1} />
          </Flex>

          {secondaryEmergency && secondaryEmergency.contact_name && (
            <Flex
              flex={1}
              justifyContent={"space-between"}
              style={{ width: "100%" }}
              top={10}
            >
              <Flex direction="column" flex={1}>
                <TabSectionFieldLabel>
                  Second emergency contact name
                </TabSectionFieldLabel>
                <TabSectionFieldValue>
                  {secondaryEmergency.contact_name}
                </TabSectionFieldValue>
              </Flex>
              <Flex direction="column" flex={1}>
                <TabSectionFieldLabel>
                  Emergency contact number
                </TabSectionFieldLabel>
                <TabSectionFieldValue>
                  {secondaryEmergency.contact_number}
                </TabSectionFieldValue>
              </Flex>
              <Flex direction="column" flex={1}>
                <TabSectionFieldLabel>Relationship </TabSectionFieldLabel>
                <TabSectionFieldValue>Family member</TabSectionFieldValue>
              </Flex>
              <Flex direction="column" flex={1} />
            </Flex>
          )}
        </Flex>
      </SubmitReviewSection>
   );
}

export default EmergencyContactTabSection;
