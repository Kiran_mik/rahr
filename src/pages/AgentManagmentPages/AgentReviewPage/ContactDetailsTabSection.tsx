import React from "react";
import { H2 } from "@components/Typography/Typography";
import { TabItemLayout } from "./style";
import { Flex } from "@components/Flex";
import { EditIconButton, Primary } from "@components/Buttons/index";
import ExportIcon from "@assets/icons/export.png";
import ContactInformationTabSection from "./ContactDetailsTabComponents/ContactInformationTabSection";
import ContactAddressDetailsTabSection from "./ContactDetailsTabComponents/ContactAddressDetailsTabSection";
import ContactPersonalDetailsTabSection from "./ContactDetailsTabComponents/ContactPersonalDetailsTabSection";
import EmergencyContactTabSection from "./ContactDetailsTabComponents/EmergencyContactTabSection";
import ContactAdditionalInfoTabSection from "./ContactDetailsTabComponents/ContactAdditionalInfoTabSection";
import BrokerWolfIdSection from "./ContactDetailsTabComponents/BrokerWolf/BrokerWolfIdSection";

function ContactDetailsTabSection(props: any) {
  const {
    data: contactDetailsInfo,
    getUrl,
    fetchApiCallback,
    isInReview,
    gotoNextStep,
    currentStep,
    userData,
    headerRefreshCallback,
  } = props;

  const otherProps = {
    review: contactDetailsInfo.review,
    getUrl,
    fetchApiCallback,
    isInReview,
  };
  return (
    <TabItemLayout>
      <Flex>
        <Flex flex={1}>
          <H2 text="Profile contact details" className="font-600" />
        </Flex>
        <EditIconButton
          icon={ExportIcon}
          text={
            <span style={{ fontWeight: 600, color: "#4E1C95", fontSize: 14 }}>
              Export for Broker Wolf
            </span>
          }
        />
      </Flex>
      <BrokerWolfIdSection
        userData={userData}
        headerRefreshCallback={headerRefreshCallback}
      />
      <Flex top={10}>
        <ContactInformationTabSection
          {...{
            email: contactDetailsInfo.email,
            ...contactDetailsInfo.profile_detail,
            ...otherProps,
          }}
        />
      </Flex>
      <Flex top={20}>
        <ContactAddressDetailsTabSection
          {...{ ...contactDetailsInfo.profile_detail, ...otherProps }}
        />
      </Flex>
      <Flex top={20}>
        <ContactPersonalDetailsTabSection
          {...{
            ...contactDetailsInfo.profile_detail,
            ...otherProps,
            languages: !contactDetailsInfo.user_language
              ? ""
              : contactDetailsInfo.user_language
                  .map((i: any) => i.languages.name)
                  .join(","),
          }}
        />
      </Flex>
      <Flex top={20}>
        <EmergencyContactTabSection
          {...{ ...contactDetailsInfo.profile_detail, ...otherProps }}
        />
      </Flex>
      <Flex top={20}>
        <ContactAdditionalInfoTabSection
          {...{ ...contactDetailsInfo.profile_detail, ...otherProps }}
        />
      </Flex>
      {isInReview && (
        <Flex className="footerbtn">
          <Primary
            text="Next step"
            className="submitbtn"
            style={{ marginRight: "2px" }}
            onClick={() => {
              gotoNextStep(currentStep);
            }}
          />
        </Flex>
      )}
    </TabItemLayout>
  );
}

export default ContactDetailsTabSection;
