import React from "react";
import { Flex } from "@components/Flex";
import { H5 } from "@components/Typography/Typography";
import { TabSectionFieldLabel, TabSectionFieldValue } from "../style";
import moment from "moment";
import { numberToOrdinalWord } from "../../../AgentOnboardingPages/CurrentEmploymentStatusPages/helper";

function AgentCurrentJobInformation(props: any) {
  let { index, item } = props;

  const getWordFromNumber = () => {
    const makeOrdinalTitle =
      index == 0
        ? "Agent current job information"
        : "Agent " + numberToOrdinalWord(index + 1).toLowerCase() + " job information";
    return makeOrdinalTitle;
  };

  return (
    <Flex flex={1} direction="column" top={20}>
      <Flex flex={1}>
        <H5 text={getWordFromNumber()} className="font-500" />
      </Flex>
      <Flex direction="column" alignItems={"baseline"} top={10}>
        <Flex
          flex={1}
          justifyContent={"space-between"}
          style={{ width: "100%" }}
        >
          <Flex direction="column" flex={1}>
            <TabSectionFieldLabel>Company name</TabSectionFieldLabel>
            <TabSectionFieldValue>
              {item && item.company_name ? item.company_name : "-"}
            </TabSectionFieldValue>
          </Flex>
          <Flex direction="column" flex={1}>
            <TabSectionFieldLabel>Company address</TabSectionFieldLabel>
            <TabSectionFieldValue>
              {item && item.company_address ? item.company_address : "-"}
            </TabSectionFieldValue>
          </Flex>
          <Flex direction="column" flex={1}>
            <TabSectionFieldLabel>Position title</TabSectionFieldLabel>
            <TabSectionFieldValue>
              {item && item.position ? item.position : "-"}
            </TabSectionFieldValue>
          </Flex>
          <Flex direction="column" flex={1}>
            <TabSectionFieldLabel>
              Employment commencement date
            </TabSectionFieldLabel>
            <TabSectionFieldValue>
              {item && item.employment_commencement_date
                ? moment(item.employment_commencement_date).format(
                    "DD MMMM YYYY"
                  )
                : "-"}
            </TabSectionFieldValue>
          </Flex>
        </Flex>
      </Flex>
      <Flex direction="column" flex={1} top={10}>
        <TabSectionFieldLabel>Job description</TabSectionFieldLabel>
        <TabSectionFieldValue>
          {item && item.description ? item.description : "-"}
        </TabSectionFieldValue>
      </Flex>
    </Flex>
  );
}

export default AgentCurrentJobInformation;
