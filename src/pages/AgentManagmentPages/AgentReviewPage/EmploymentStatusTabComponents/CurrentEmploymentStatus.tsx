import { Flex } from "@components/Flex";
import SubmitReviewSection from "@pages/AgentManagmentPages/AgentReviewPage/SubmitReviewSection";
import {
  H5
} from "@components/Typography/Typography";
import React from "react";
import {
  TabSectionFieldLabel,
  TabSectionFieldValue
} from "../style";
import AgentCurrentJobInformation from "./AgentCurrentJobInformation";

function CurrentEmploymentStatus(props: any) {
  let { data, getUrl, fetchApiCallback, isInReview } = props;
  let { current_employment_status } = data;

  return (
    <SubmitReviewSection
      title={"Current Employment status"}
      review={
        current_employment_status && current_employment_status.review
          ? current_employment_status.review
          : []
      }
      endPoint={getUrl}
      block={"current_employment_status"}
      fetchApiCallback={fetchApiCallback}
      isInReview={isInReview}
    >
      <Flex top={20}>
        <H5
          text="Is agent going to hold any other job while working as a realtor at
          Right at Home Realty Inc?"
          className="font-500"
        />
      </Flex>
      <Flex direction="column" flex={1} top={10}>
        <TabSectionFieldLabel>Agent answer</TabSectionFieldLabel>
        <TabSectionFieldValue>
          {current_employment_status &&
            current_employment_status.full_time_to_rahr_or_not &&
            current_employment_status.full_time_to_rahr_or_not == 1
            ? "Yes"
            : "No"}
        </TabSectionFieldValue>
      </Flex>
      <Flex direction="column">
        {current_employment_status &&
          current_employment_status.current_job_information &&
          current_employment_status.current_job_information.length > 0 &&
          current_employment_status.current_job_information.map(
            (item: any, index: number) => {
              return (
                <AgentCurrentJobInformation
                  key={index}
                  item={item}
                  index={index}
                />
              );
            }
          )}
      </Flex>
    </SubmitReviewSection>
  );
}

export default CurrentEmploymentStatus;
