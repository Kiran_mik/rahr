import { Flex } from "@components/Flex";
import SubmitReviewSection from "@pages/AgentManagmentPages/AgentReviewPage/SubmitReviewSection";
import { H5 } from "@components/Typography/Typography";
import React from "react";
import { TabSectionFieldLabel, TabSectionFieldValue } from "../style";
import DocumentImg from "@assets/icon-form.svg";
import OnboardingFormButton from "@components/OnboardingFormButton";

function PrecFromStatus(props: any) {
  let { data, getUrl, fetchApiCallback, isInReview } = props;
  let { prec_details } = data;
  const formDetails = { id: "", docusign_id: "", form_link: "", form_name: "" };

  const checkHst = () => {
    if (prec_details && prec_details.personal_hst) {
      return prec_details.personal_hst;
    } else if (prec_details && prec_details.corporate_hst) {
      return prec_details.corporate_hst;
    } else {
      return "-";
    }
  };

  return (
    <SubmitReviewSection
      title={"PREC"}
      review={prec_details && prec_details.review ? prec_details.review : []}
      endPoint={getUrl}
      block={"prec_details"}
      fetchApiCallback={fetchApiCallback}
      isInReview={isInReview}
    >
      <Flex top={20}>
        <H5
          text={"Is agent joining as a “Personal Real Estate Corporation”?"}
          className="font-500"
        />
      </Flex>
      <Flex direction="column" top={10}>
        <TabSectionFieldValue>
          {prec_details &&
          prec_details.joining_as_prec &&
          prec_details.joining_as_prec == 1
            ? "Yes"
            : "No"}
        </TabSectionFieldValue>
      </Flex>
      <Flex direction="column" flex={1} top={20}>
        <TabSectionFieldLabel>
          {prec_details && prec_details.personal_hst
            ? "Personal HST"
            : "Corporate HST"}
          Corporate HST
        </TabSectionFieldLabel>
        <TabSectionFieldValue>{checkHst()}</TabSectionFieldValue>
      </Flex>
      <Flex top={20}>
        <H5 text={"Attached forms"} className="font-500" />
      </Flex>
      <Flex top={10}>
        <OnboardingFormButton
          buttonText="View"
          image={DocumentImg}
          text="PREC form"
          formDetails={formDetails}
        />
      </Flex>
    </SubmitReviewSection>
  );
}

export default PrecFromStatus;
