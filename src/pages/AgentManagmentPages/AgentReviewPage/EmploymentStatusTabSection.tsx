import React from "react";
import { H2 } from "@components/Typography/Typography";
import { TabItemLayout } from "./style";
import { Flex } from "@components/Flex";
import ExportIcon from "@assets/icons/export.png";
import {
  EditIconButton,
  Primary,
  Transparent,
} from "@components/Buttons/index";
import EmptyDiv from "@components/EmptyState";
import EmptyTableImg from "@assets/EmptyTablePic.png";

//components
import CurrentEmploymentStatus from "./EmploymentStatusTabComponents/CurrentEmploymentStatus";
import PrecFromStatus from "./EmploymentStatusTabComponents/PrecFromStatus";

function EmploymentStatusTabSection(props: any) {
  const { isInReview,gotoNextStep,currentStep,gotoPrevStep } = props;
  return (
    <TabItemLayout>
      <Flex>
        <Flex flex={1}>
          <H2 text="Emplyment status & PREC" className="font-600" />
        </Flex>
        <EditIconButton
          icon={ExportIcon}
          text={
            <span style={{ fontWeight: 600, color: "#4E1C95", fontSize: 14 }}>
              Export for Broker Wolf
            </span>
          }
        />
      </Flex>
      {Object.keys(props.data).length > 1 ? (
        <>
          <Flex top={20}>
            <CurrentEmploymentStatus {...props} />
          </Flex>
          <Flex top={20}>
            <PrecFromStatus {...props} />
          </Flex>
          {isInReview && (
            <Flex className="footerbtn">
              <Primary
                text="Next step"
                className="submitbtn"
                style={{ marginRight: "2px" }}
              // isLoading={isLoading}
              onClick={()=>{
                gotoNextStep(currentStep);
              }}
              />
              <Transparent text="Prev step" onClick={()=>{
                gotoPrevStep(currentStep);
              }}/>
            </Flex>
          )}
        </>
      ) : (
        !props.loading && (
          <Flex top={70} justifyContent={"center"}>
            <EmptyDiv
              image={EmptyTableImg}
              text={<>There is no data available right now.</>}
            />
          </Flex>
        )
      )}
    </TabItemLayout>
  );
}

export default EmploymentStatusTabSection;
