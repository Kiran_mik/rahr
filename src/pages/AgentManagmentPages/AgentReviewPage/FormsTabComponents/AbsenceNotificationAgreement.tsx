import { CheckCircleOutlined } from '@ant-design/icons';
import AccoridanPanel from '@components/AccordianComponent';
import React from 'react';
import { NonCollasibleWrapper } from '../style';

function AbsenceNotificationAgreement() {
    return  ( <NonCollasibleWrapper>
      <div className='title'>
      Absence notification agreement
      </div>
      <div className='icon'>
      <CheckCircleOutlined />Agreed
      </div>
    </NonCollasibleWrapper>);
}

export default AbsenceNotificationAgreement;
