import { CheckCircleOutlined } from '@ant-design/icons';
import React from 'react';
import { NonCollasibleWrapper } from '../style';

function BrokerLoadingAuthority() {
  return ( <NonCollasibleWrapper>
  <div className='title'>
  Broker loading authority
  </div>
  <div className='icon'>
  <CheckCircleOutlined />Agreed
  </div>
</NonCollasibleWrapper>);
}

export default BrokerLoadingAuthority;
