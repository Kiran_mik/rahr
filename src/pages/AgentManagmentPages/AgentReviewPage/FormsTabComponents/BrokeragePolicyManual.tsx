import React from 'react';
import { NonCollasibleBody } from '../style';
import DocuSign from '@components/DocuSign';
import DocumentImage from '@assets/document-form.svg'
import { FormLableBoldTypography } from '@components/Typography/Typography';
import { CheckCircleOutlined } from '@ant-design/icons';
import { CheckedIcon } from '@components/AccordianComponent/style'
import { Flex } from '@components/Flex';
function BrokeragePolicyManual(props: any) {
  return (
    <NonCollasibleBody>
      <Flex justifyContent="space-between" bottom={16}>
        <div className='title'>
          <CheckedIcon style={{ marginRight: "18px", color: "#31AF91" }} /> Brokerage policy manual
        </div>
        <div className='icon'>
          <CheckCircleOutlined style={{ marginRight: "3px" }} />Signed
        </div>

      </Flex>
      <FormLableBoldTypography>Attached forms</FormLableBoldTypography>
      <DocuSign text="Brokerage policy manual" image={DocumentImage}
        link={props.data && props.data.brokerage_policy_manual_signed_document}
        linktext="View Form" />
    </NonCollasibleBody>
  );
}
export default BrokeragePolicyManual;
