import { CheckCircleOutlined } from '@ant-design/icons';
import AccoridanPanel from '@components/AccordianComponent';
import React from 'react';
import { NonCollasibleWrapper } from '../style';

function BrokerageTelemarketingPolicy() {
    /**
   * Return Component according to their Id
   */
 
  return (
  <NonCollasibleWrapper>
    <div className='title'>
    Brokerage telemarketing policy
    </div>
    <div className='icon'>
    <CheckCircleOutlined />Agreed
    </div>
  </NonCollasibleWrapper>
  );
}

export default BrokerageTelemarketingPolicy;
