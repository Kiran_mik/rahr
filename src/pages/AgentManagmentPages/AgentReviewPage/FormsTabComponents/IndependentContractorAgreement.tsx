import React from 'react';
import { NonCollasibleBody } from '../style';
import DocuSign from '@components/DocuSign';
import DocumentImage from '@assets/document-form.svg'
import { FormLableBoldTypography } from '@components/Typography/Typography';
import { Flex } from '@components/Flex';
import { CheckedIcon } from '@components/AccordianComponent/style'
import { CheckCircleOutlined } from '@ant-design/icons';

function IndependentContractorAgreement(props: any) {
  return (
    <NonCollasibleBody>
      <Flex justifyContent="space-between" bottom={16}>
        <div className='title'>
          <CheckedIcon style={{ marginRight: "18px", color: "#31AF91" }} /> Independent contractor agreement
        </div>
        <div className='icon'>
          <CheckCircleOutlined style={{ marginRight: "3px" }} />Signed
        </div>

      </Flex>
      <FormLableBoldTypography>Attached forms</FormLableBoldTypography>
      <DocuSign text="Independent contractor agreement"
        link={props.data && props.data.independent_contract_agreement_signed_document}
        image={DocumentImage} linktext="View Form" />
    </NonCollasibleBody>
  );
}

export default IndependentContractorAgreement;
