import { CheckCircleOutlined } from '@ant-design/icons';
import React from 'react';
import { NonCollasibleWrapper } from '../style';

function JoiningRAHRAsTeamMember() {
  return ( <NonCollasibleWrapper>
    <div className='title'>
    Joining RAHR as a team member
    </div>
    <div className='icon'>
    <CheckCircleOutlined />Agreed
    </div>
  </NonCollasibleWrapper>);;
}

export default JoiningRAHRAsTeamMember;
