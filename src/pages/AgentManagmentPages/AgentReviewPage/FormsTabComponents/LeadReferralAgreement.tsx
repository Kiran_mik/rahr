import { CheckCircleOutlined } from '@ant-design/icons';
import React from 'react';
import { NonCollasibleWrapper } from '../style';

function LeadReferralAgreement() {
  return  ( <NonCollasibleWrapper>
    <div className='title'>
    Lead referral agreement
    </div>
    <div className='icon'>
    <CheckCircleOutlined />Agreed
    </div>
  </NonCollasibleWrapper>);
}

export default LeadReferralAgreement;
