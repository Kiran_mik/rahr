import { CheckCircleOutlined } from '@ant-design/icons';
import React from 'react';
import { NonCollasibleWrapper } from '../style';

function ProfessionalDevelopmentProgramAgreement() {
  return ( <NonCollasibleWrapper>
    <div className='title'>
    Professional development program agreement
    </div>
    <div className='icon'>
    <CheckCircleOutlined />Agreed
    </div>
  </NonCollasibleWrapper>);
}

export default ProfessionalDevelopmentProgramAgreement;
