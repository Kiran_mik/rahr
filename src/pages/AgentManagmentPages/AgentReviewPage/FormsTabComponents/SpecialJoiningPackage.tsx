import { CheckCircleOutlined } from '@ant-design/icons';
import { Flex } from '@components/Flex';
import { FormLableBoldTypography, FormLableTypography } from '@components/Typography/Typography';
import React from 'react';
import { NonCollasibleBody } from '../style';
import { Col, Row } from 'antd';
import { InputField } from '@components/Input/Input';
import CommentBox from '@components/Comment';
import { CheckedIcon } from '@components/AccordianComponent/style'
import { isObjectEmpty } from '@utils/helpers';
function SpecialJoiningPackage(props: any) {
    if (!(props.data && !isObjectEmpty(props.data))) return null;
    const {
        data:
        {
            inviterDetails: {
                special_joining_package,
                additional_special_package_info,
                first_name,
                last_name,
                profile_photo
            },
            trade_name,
            cellphone_no,
            email,
            website
        } } = props;
    return <NonCollasibleBody>
        <Flex justifyContent="space-between" bottom={16}>
            <div className='title'>
                <CheckedIcon style={{ marginRight: "18px", color: "#31AF91" }} />Special joining package
            </div>
            <div className='icon'>
                <CheckCircleOutlined style={{ marginRight: "3px" }} />Agreed
            </div>

        </Flex>
        <Flex bottom={15}>
            <FormLableBoldTypography>Information for “Special Joining Package</FormLableBoldTypography>
        </Flex>
        <Row gutter={16}>
            <Col span={8}>
                <FormLableTypography>Trade name <span>*</span></FormLableTypography>
                <InputField name="" value={trade_name} disabled={true}></InputField>
            </Col>
            <Col span={8}>
                <FormLableTypography>Cell phone number <span>*</span></FormLableTypography>
                <InputField name="" value={cellphone_no} disabled={true}></InputField>
            </Col>
        </Row>
        <Row gutter={16}>
            <Col span={8}>
                <FormLableTypography>Email <span>*</span></FormLableTypography>
                <InputField name="" value={email} disabled={true}></InputField>
            </Col>
            {website && website.length > 0 && (
                <Col span={8}>
                    <FormLableTypography>Website </FormLableTypography>
                    <InputField name="" value={website} disabled={true}></InputField>
                </Col>
            )}

        </Row>
        {special_joining_package == 0 && additional_special_package_info && additional_special_package_info.length > 0 && (
            <Flex>
                <CommentBox
                    image={profile_photo}
                    name={first_name + " " + last_name}
                    description={additional_special_package_info}
                ></CommentBox>
            </Flex>
        )}

    </NonCollasibleBody>
}

export default React.memo(SpecialJoiningPackage);
