import { EditIconButton, Primary, Transparent } from '@components/Buttons'
import { Flex } from '@components/Flex'
import { H2, H3, H4 } from '@components/Typography/Typography'
import React, { useEffect, useMemo, useState } from 'react'
import { NonCollasibleWrapper, TabItemLayout, TabSectionFieldValue } from './style'
import ExportIcon from "@assets/icons/export.png";
import IndependentContractorAgreement from './FormsTabComponents/IndependentContractorAgreement'
import SpecialJoiningPackage from './FormsTabComponents/SpecialJoiningPackage'
import BrokeragePolicyManual from './FormsTabComponents/BrokeragePolicyManual'
import { isObjectEmpty } from '@utils/helpers'
import { getAgreementList } from '@pages/AgentOnboardingPages/ReviewAgreements/helper'
import { BASE_URL_MAP } from '@constants/index'
import Loader from '@components/Loader'
import { CheckCircleOutlined } from '@ant-design/icons'
import { CheckedIcon } from '@components/AccordianComponent/style'
import { useParams } from '@reach/router'
import { changeReviewStatus } from './helper'
import { notification } from 'antd'

const mapFormsData = (data: any) => {
  const mappedData: any[] = [];
  Object.keys(data).forEach(element => {
    if (!isNaN(Number(element))) {
      const agreementId = data[element].agreement_id
      mappedData[agreementId] = data[element];
      if (agreementId == 9) {
        mappedData[agreementId].inviterDetails = data.inviterDetails;
      }
    }
  });
  return mappedData;
}
function FormsTabSection(props: any) {
  const {
    getUrl,
    isInReview,
    gotoPrevStep,
    currentStep,
    tabStatuses,
    updateTabStatuses,
    finalButtonState,
    finalFormSubmit,
    finalSubmitButtonLoadingStatus
  } = props;
  const params = useParams();
  const { agentId } = params;
  const [isLoading, setLoading] = useState(false);
  const [reviewList, setReviewList] = useState<Array<any>>([]);
  const fetchAgreementList = async () => {
    let agreementList = await getAgreementList(BASE_URL_MAP['agent-onboard']);
    console.log(reviewList);
    setReviewList(agreementList);
  }
  useEffect(() => {
    if (!isObjectEmpty(props.data)) {
      fetchAgreementList();
    }
  }, [props.data])

  const data = mapFormsData(props.data);
  if (isObjectEmpty(data)) return null;
  if (!reviewList.length) return <Loader loading={true} />


  const checkIfStepCompleted = (stepId: number) => {
    if (data[stepId]) {
      return data[stepId].agreed;
    }
    return false
  }


  const submitAgreementSectionReview = async () => {
    const body: any = {
      agent_id: agentId,
      review: {
        block: "agreement_details",
        review: 1,
      },
    };
    return await changeReviewStatus(getUrl, body);
  }

  const openNotification = () => {
    notification.open({
      message: 'Agent profile approved',
      description:
        'The agent will be notified about the status change of their profile.',
      icon: <CheckedIcon style={{ color: '#5FCFA9' }} />,
    });
  };
  const handleButtonClick = async () => {
    setLoading(true);
    // if( not already submitted)
    if (!tabStatuses[currentStep]) {
      const res: any = await submitAgreementSectionReview();
      if (res.success) {
        updateTabStatuses();
        finalFormSubmit();
      }
    }
    if (finalButtonState) {
      finalFormSubmit();
    }

    // set final steps as completed
    // if all steps are completd trigger below action
    // if review comment in any section then submit feedback api call
    // else approve agent
    setLoading(false);
  }

  return (
    <TabItemLayout>
      <Flex bottom={16}>
        <Flex flex={1}>
          <H2 text="Review agreements" className="font-600" />
        </Flex>
        <EditIconButton
          icon={ExportIcon}
          text={
            <span style={{ fontWeight: 600, color: "#4E1C95", fontSize: 14 }}>
              Export for Broker Wolf
            </span>
          }
        />
      </Flex>
      <Flex bottom={15}>
        <H3 text="Form name" className='font-500' />
      </Flex>
      <Flex direction='column'>
        {reviewList.map(reviewItem => {
          if (!reviewItem.status) return null;
          const stepId = reviewItem.id;
          const isStepCompleted = checkIfStepCompleted(stepId);
          if (stepId == 7) //IndependentContractorAgreement
            return <IndependentContractorAgreement key={stepId} data={data[stepId]} />
          if (stepId == 8) //BrokeragePolicyManual
            return <BrokeragePolicyManual key={stepId} data={data[8]} />
          if (stepId == 9) //SpecialJoiningPackage
            return <SpecialJoiningPackage key={stepId} data={data[stepId]} />
          if (stepId == 10) return null;
          return (
            <NonCollasibleWrapper key={stepId}>
              <div className='title'>
                {reviewItem.title}
              </div>
              {isStepCompleted && (<div className='icon'>
                <CheckCircleOutlined style={{ marginRight: "3px" }} />Agreed
              </div>)}

            </NonCollasibleWrapper>
          )
        })}
        <Flex top={20} bottom={16}>
          <H4 text="Agent agreement" />
        </Flex>
        <Flex direction="row" alignItems={"center"}>
          <CheckedIcon style={{ marginRight: "10px" }} />{" "}
          <TabSectionFieldValue>{" Agent agrees with the terms and conditions and confirms that all the provided details are correct"}</TabSectionFieldValue>
        </Flex>
      </Flex>
      {isInReview && (
        <Flex className="footerbtn">
          <Primary
            text={finalButtonState == 0 ? "Submit" : (finalButtonState == 1 ? "Approve application" : "Submit feedback")}
            className="submitbtn"
            style={{ marginRight: "2px" }}
            isLoading={isLoading || finalSubmitButtonLoadingStatus}
            onClick={handleButtonClick}
            isDisable={finalButtonState == 0 && tabStatuses[currentStep]}
          />
          <Transparent
            text="Prev step"
            onClick={() => {
              gotoPrevStep(currentStep);
            }} />
        </Flex>
      )}

    </TabItemLayout>
  )
}
function formPropsAreEqual(prevProps: any, nextProps: any) {
  return prevProps.data === nextProps.data;
}

export default React.memo(FormsTabSection, formPropsAreEqual);
