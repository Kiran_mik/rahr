import { H5 } from '@components/Typography/Typography'
import { Col, Row } from 'antd'
import React from 'react'
import { ContentWrapper, TabSectionFieldLabel, TabSectionFieldValue } from '../style'

function AgentToJoinRAHR(props: any) {
    const { profileInfo } = props;
    return (
        <ContentWrapper>
            <H5 text="What attracted agent to join RAHR?" className="font-500" />
            <Row gutter={16}>
                <Col span={24}>
                    <TabSectionFieldLabel>
                        Agent answer
                    </TabSectionFieldLabel>
                    <TabSectionFieldValue>{profileInfo.attracted_to_join_rahr_id ? profileInfo.attracted_to_join_rahr_id.status_name : "-"}</TabSectionFieldValue>
                </Col>

            </Row>
        </ContentWrapper>
    )
}

export default AgentToJoinRAHR
