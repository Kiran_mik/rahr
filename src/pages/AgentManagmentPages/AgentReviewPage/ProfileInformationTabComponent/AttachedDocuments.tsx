import OnboardingFormButton from '@components/OnboardingFormButton';
import { Col, Row } from 'antd'
import React from 'react'
import { TabSectionFieldLabel, TabSectionFieldValue } from '../style'
import DocumentImg from "@assets/icon-form.svg";
import SubmitReviewSection from '../SubmitReviewSection';

function AttachedDocuments(props: any) {
    const { profileInfo,
        getUrl,
        fetchApiCallback,
        review, isInReview } = props;
    const formDetailsTranscript = { id: "", form_name: "", form_link: profileInfo.official_transcript_doc };
    const formDetailsPolice = { id: "", form_name: "", form_link: profileInfo.police_record_doc };

    return (
        <SubmitReviewSection
            title="Attached documents"
            review={review || []}
            endPoint={getUrl}
            block="attached_document"
            fetchApiCallback={fetchApiCallback}
            isInReview={isInReview}
        >

            <Row gutter={16}>
                <Col span={8}>
                    <TabSectionFieldLabel>
                        Official transcript
                    </TabSectionFieldLabel>
                    <TabSectionFieldValue>
                        {profileInfo.official_transcript_doc
                            ? (<OnboardingFormButton
                                buttonText="View"
                                image={DocumentImg}
                                text="Official transcript"
                                formDetails={formDetailsTranscript}
                            />)
                            : "-"
                        }
                    </TabSectionFieldValue>
                </Col>
                <Col span={8}>
                    <TabSectionFieldLabel>
                        Police record
                    </TabSectionFieldLabel>
                    <TabSectionFieldValue>
                        {profileInfo.police_record_doc
                            ? (<OnboardingFormButton
                                buttonText="View"
                                image={DocumentImg}
                                text="Police record"
                                formDetails={formDetailsPolice}
                            />)
                            : "-"
                        }
                    </TabSectionFieldValue>
                </Col>
            </Row>
        </SubmitReviewSection>
    )
}

export default AttachedDocuments;
