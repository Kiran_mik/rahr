import { Col, Row } from 'antd'
import React, { useEffect, useState } from 'react'
import { TabSectionFieldLabel, TabSectionFieldValue } from '../style'
import SubmitReviewSection from '../SubmitReviewSection';

function BoardMembershipNumber(props: any) {
    const { profileInfo,
        getUrl,
        fetchApiCallback,
        review, isInReview
    } = props;
    const [boards, setBoards] = useState([]);
    useEffect(() => {
        if (profileInfo.board_id_form_to_sign) {
            try {
                let boards = JSON.parse(profileInfo.board_id_form_to_sign);
                if (typeof (boards) == 'object') {
                    setBoards(boards);
                }

            } catch (ex) { }
        }
    }, [profileInfo.board_id_form_to_sign])
    if (!(boards && boards.length)) return null;
    return (
        <SubmitReviewSection
            title="Real estate board IDS agent belongs to"
            review={review || []}
            endPoint={getUrl}
            block="real_estate_board_ids"
            fetchApiCallback={fetchApiCallback}
            isInReview={isInReview}
        >

            <Row gutter={16}>
                {boards.map((board: any) => (
                    <Col span={8} key={board.abbreviation}>
                        <TabSectionFieldLabel>
                            {board.abbreviation} Number
                        </TabSectionFieldLabel>
                        <TabSectionFieldValue>
                            {board.number
                                ? board.number
                                : "-"
                            }
                        </TabSectionFieldValue>
                    </Col>
                ))}

            </Row>
        </SubmitReviewSection>
    )
}

export default BoardMembershipNumber;
