import { H5 } from '@components/Typography/Typography'
import { Col, Row } from 'antd'
import React from 'react'
import { ContentWrapper, TabSectionFieldLabel, TabSectionFieldValue } from '../style'

function BranchAgent(props: any) {
    const { user_branch } = props;
    if (!(user_branch && user_branch.branch)) return null;
    return (
        <ContentWrapper>
            <H5 text="Branch agent wants to join" className="font-500" />
            <Row gutter={16}>
                <Col span={8}>
                    <TabSectionFieldLabel>
                        Branch
                    </TabSectionFieldLabel>
                    <TabSectionFieldValue>{user_branch.branch.branch_name}</TabSectionFieldValue>
                </Col>
                <Col span={8}>
                    <TabSectionFieldLabel>
                        Branch broker code
                    </TabSectionFieldLabel>
                    <TabSectionFieldValue>{user_branch.branch.broker_code || "-"}</TabSectionFieldValue>
                </Col>
            </Row>
        </ContentWrapper>
    )
}

export default BranchAgent
