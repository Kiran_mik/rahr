import OnboardingFormButton from '@components/OnboardingFormButton';
import { H5 } from '@components/Typography/Typography'
import { BrokerOrSalesRepresentative } from '@constants/onboardingConstants';
import { AGENT_STATUS } from '@pages/AgentOnboardingPages/ProfileInformationPage/helper';
import { Col, Row } from 'antd'
import React from 'react'
import { ContentWrapper, TabSectionFieldLabel, TabSectionFieldValue } from '../style'
import DocumentImg from "@assets/icon-form.svg";
import { formatNumberWithComma } from '@utils/format';

function BrokerSalesRepresentative(props: any) {
	const { profileInfo } = props;
	const formDetails = { id: "", form_name: "", form_link: profileInfo.previous_year_payroll_history_report };

	return (
		<ContentWrapper>
			<H5 text="Is the agent joining as a broker or sales representative?" className="font-500" />
			<Row gutter={16}>
				<Col span={8}>
					<TabSectionFieldLabel>
						Broker / Sales representative
					</TabSectionFieldLabel>
					<TabSectionFieldValue>{profileInfo.broker_or_sales_representative ? BrokerOrSalesRepresentative[profileInfo.broker_or_sales_representative] : " - "}</TabSectionFieldValue>
				</Col>
				<Col span={8}>
					<TabSectionFieldLabel>
						Area agent specialize in
					</TabSectionFieldLabel>
					<TabSectionFieldValue>{profileInfo.primary_area_of_specialization_id ? profileInfo.primary_area_of_specialization_id.status_name : " - "}</TabSectionFieldValue>
				</Col>
				{(profileInfo.agent_status == AGENT_STATUS.TRANSFER
					|| profileInfo.agent_status == AGENT_STATUS.REINSTATED_WITH_RECO)
					&& (
						<>
							<Col span={8}>
								<TabSectionFieldLabel>
									Previous Year Transactions
								</TabSectionFieldLabel>
								<TabSectionFieldValue>
									{
										profileInfo.previous_year_transactions
											? formatNumberWithComma(profileInfo.previous_year_transactions)
											: "-"
									}
								</TabSectionFieldValue>
							</Col>
							<Col span={8}>
								<TabSectionFieldLabel>
									Last year gross income
								</TabSectionFieldLabel>
								<TabSectionFieldValue>
									{
										profileInfo.last_year_gross_income
											? formatNumberWithComma(profileInfo.last_year_gross_income)
											: "-"
									}
								</TabSectionFieldValue>
							</Col>
							<Col span={8}>
								<TabSectionFieldLabel>
									Previous year payroll history
								</TabSectionFieldLabel>
								{profileInfo.previous_year_payroll_history_report
									? (<OnboardingFormButton
										buttonText="View"
										image={DocumentImg}
										text="Previous year payroll history"
										formDetails={formDetails}
									/>)
									: "-"}
							</Col>
						</>
					)}
			</Row>

		</ContentWrapper>
	)
}

export default BrokerSalesRepresentative
