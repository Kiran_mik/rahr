import { H5 } from '@components/Typography/Typography'
import { Col, Row } from 'antd'
import React from 'react'
import { ContentWrapper, TabSectionFieldLabel, TabSectionFieldValue } from '../style'

function ComplaintRECOorTRREB(props: any) {
	const { profileInfo } = props;
	let complains = [];
	try {
		complains = JSON.parse(profileInfo.complaint_file_opened_with_reco_or_trreb_data);
	} catch (ex) { }
	return (
		<ContentWrapper>
			<H5 text="Complaint file opened with RECO or TRREB" className="font-500" />
			<Row gutter={16}>
				<Col span={24}>
					<TabSectionFieldLabel>
						Has the agent ever had a complaint file open with RECO or TRREB?					</TabSectionFieldLabel>
					<TabSectionFieldValue>{profileInfo.complaint_file_opened_with_reco_or_trreb ? "Yes" : "No"}</TabSectionFieldValue>
				</Col>
				{profileInfo.complaint_file_opened_with_reco_or_trreb == 1 && complains.map((item:any, key:number) => (
					<React.Fragment key={key}>
						<Col span={24}>
							<TabSectionFieldLabel>
								Details of complain
							</TabSectionFieldLabel>
							<TabSectionFieldValue>{item.complaint}</TabSectionFieldValue>
						</Col>
						<Col span={24}>
							<TabSectionFieldLabel>
								Details of outcome
							</TabSectionFieldLabel>
							<TabSectionFieldValue>{item.outcome}</TabSectionFieldValue>
						</Col>
					</React.Fragment>
				))}
			</Row>
		</ContentWrapper>
	)
}

export default ComplaintRECOorTRREB
