import { H5 } from '@components/Typography/Typography'
import { formatNumberWithComma } from '@utils/format';
import { Col, Row } from 'antd'
import React from 'react'
import { ContentWrapper, TabSectionFieldLabel, TabSectionFieldValue } from '../style'

function GarnishmentOrder(props: any) {
    const { profileInfo } = props;
    let garnishData = [];
    try {
        garnishData = JSON.parse(profileInfo.subject_to_a_garnishment_order_data);
    } catch (ex) { }
    return (
        <ContentWrapper>
            <H5 text="Subject to a garnishment order" className="font-500" />
            <Row gutter={16}>
                <Col span={24}>
                    <TabSectionFieldLabel>
                        Is the agent a subject to a garnishment order?
                    </TabSectionFieldLabel>
                    <TabSectionFieldValue>{profileInfo.subject_to_a_garnishment_order ? "Yes" : "No"}</TabSectionFieldValue>
                </Col>
                {profileInfo.subject_to_a_garnishment_order == 1 && garnishData.map((item: any, key: number) => (
                    <React.Fragment key={key}>
                        <Col span={24}>
                            <TabSectionFieldLabel>
                                Amount of garhishment
                            </TabSectionFieldLabel>
                            <TabSectionFieldValue>$ {formatNumberWithComma(item.amount_garnishment)}</TabSectionFieldValue>
                        </Col>
                        <Col span={24}>
                            <TabSectionFieldLabel>
                                Name of garnishor
                            </TabSectionFieldLabel>
                            <TabSectionFieldValue>{item.name_garnishor}</TabSectionFieldValue>
                        </Col>
                    </React.Fragment>
                ))}
            </Row>
        </ContentWrapper>
    )
}

export default GarnishmentOrder
