import { H5 } from '@components/Typography/Typography'
import { AgentNotificationEnum } from '@constants/onboardingConstants';
import { Col, Row } from 'antd'
import React from 'react'
import { ContentWrapper, TabSectionFieldLabel, TabSectionFieldValue } from '../style'

function Notification(props: any) {
    const { profileInfo } = props;
    return (
        <ContentWrapper >
            <H5 text="How does the agent want to be notified about their onboarding appointments?" className="font-500" />
            <Row gutter={16}>
                <Col span={24}>
                    <TabSectionFieldLabel>
                        Notification method
                    </TabSectionFieldLabel>
                    <TabSectionFieldValue>
                        {profileInfo.notification_type
                            ? AgentNotificationEnum[profileInfo.notification_type]
                            : "-"
                        }</TabSectionFieldValue>
                </Col>
            </Row>
        </ContentWrapper>
    )
}

export default Notification
