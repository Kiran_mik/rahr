import { H5 } from '@components/Typography/Typography'
import { Col, Row } from 'antd'
import React from 'react'
import { ContentWrapper, TabSectionFieldLabel, TabSectionFieldValue } from '../style'

function OnBoardingAppointment() {
    return (
        <ContentWrapper>
            <H5 text="How agents wants to be notified about the onboarding appointment?" className="font-500" />
            <Row gutter={16}>
                <Col span={24}>
                    <TabSectionFieldLabel>
                        Notification method
                    </TabSectionFieldLabel>
                    <TabSectionFieldValue>Text</TabSectionFieldValue>
                </Col>

            </Row>
        </ContentWrapper>
    )
}

export default OnBoardingAppointment
