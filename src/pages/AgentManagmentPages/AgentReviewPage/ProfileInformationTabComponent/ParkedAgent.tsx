import React from 'react'
import OnboardingFormButton from '@components/OnboardingFormButton';
import { H5 } from '@components/Typography/Typography'
import { Col, Row } from 'antd'
import { ContentWrapper, TabSectionFieldLabel, TabSectionFieldValue } from '../style'
import DocumentImg from "@assets/icon-form.svg";
import { Flex } from '@components/Flex';

function ParkedAgent(props: any) {
    const { profileInfo } = props;
    const formDetails = { id: "", form_name: "", form_link: profileInfo.join_as_a_parked_agent_signed_document };
    return (
        <ContentWrapper>
            <H5 text="Is the agent joining as a parked agent?" className="font-500" />
            <Row gutter={16}>
                <Col span={24}>
                    <TabSectionFieldLabel>
                        Agent answer
                    </TabSectionFieldLabel>
                    <TabSectionFieldValue>{profileInfo.join_as_a_parked_agent ? "Yes" : "Keep agent status active"}</TabSectionFieldValue>
                    {profileInfo.join_as_a_parked_agent == 1
                        && profileInfo.join_as_a_parked_agent_signed_document != ""
                        && profileInfo.join_as_a_parked_agent_signed_document != null
                        && (
                            <Flex top={10}>
                                <OnboardingFormButton
                                    buttonText="View"
                                    image={DocumentImg}
                                    text="Move to park form"
                                    formDetails={formDetails}
                                />
                            </Flex>
                        )}
                </Col>

            </Row>
        </ContentWrapper>
    )
}

export default ParkedAgent
