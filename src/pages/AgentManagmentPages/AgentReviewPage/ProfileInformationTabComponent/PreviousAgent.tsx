import React from 'react'
import { H5 } from '@components/Typography/Typography'
import { Col, Row } from 'antd'
import { ContentWrapper, TabSectionFieldValue } from '../style'

function PreviousAgent(props: any) {
    const { profileInfo } = props;
    return (
        <ContentWrapper>
            <H5 text="Was ever agent joining RAHR before ?" className="font-500" />
            <Row gutter={16}>
                <Col span={24}>
                    <TabSectionFieldValue>{profileInfo.ever_joined_rahr_as_an_agent
                        ? "Yes, I was previously an agent with RAHR"
                        : "No, this is my first time joining"
                    }</TabSectionFieldValue>
                </Col>

            </Row>
        </ContentWrapper>
    )
}

export default PreviousAgent
