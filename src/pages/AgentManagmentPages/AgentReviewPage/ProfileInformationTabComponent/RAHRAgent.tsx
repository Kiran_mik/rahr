import { Flex } from '@components/Flex';
import OnboardingFormButton from '@components/OnboardingFormButton';
import { H5 } from '@components/Typography/Typography'
import { AGENT_STATUS } from '@pages/AgentOnboardingPages/ProfileInformationPage/helper';
import { Col, Row } from 'antd'
import React from 'react'
import { ContentWrapper, TabSectionFieldLabel, TabSectionFieldValue } from '../style'
import DocumentImg from "@assets/icon-form.svg";

function RAHRAgent(props: any) {
    const { profileInfo } = props;
    if (profileInfo.agent_status == AGENT_STATUS.TRANSFER && profileInfo.ever_joined_rahr_as_an_agent === 1) return null;
    const formDetails = { id: "", form_name: "", form_link: profileInfo.join_as_a_parked_agent_signed_document };

    return (
        <ContentWrapper>
            <H5 text="Has agent been referred by RAHR Agent? " className="font-500" />
            <Row gutter={16}>
                <Col span={24}>
                    <TabSectionFieldLabel>
                        Agent answer
                    </TabSectionFieldLabel>
                    <TabSectionFieldValue>{profileInfo.referred_by_agent ? "Yes" : "No"}</TabSectionFieldValue>
                </Col>
                {profileInfo.referred_by_agent == 1 && (
                    <>
                        {profileInfo.agent_status == AGENT_STATUS.NEW_REGISTRATION && (<Flex direction="column">
                            <Row gutter={16}>
                                <Col span={12}>
                                    <TabSectionFieldLabel>
                                        RAH agent name
                                    </TabSectionFieldLabel>
                                    <TabSectionFieldValue>{profileInfo.referred_rah_agent_name}</TabSectionFieldValue>
                                </Col>
                                <Col span={12}>
                                    <TabSectionFieldLabel>
                                        RAH agent email address
                                    </TabSectionFieldLabel>
                                    <TabSectionFieldValue>{profileInfo.referred_rah_agent_email}</TabSectionFieldValue>
                                </Col>
                            </Row>
                            <Row gutter={16}>
                                <Col span={12}>
                                    <TabSectionFieldLabel>Branch office</TabSectionFieldLabel>
                                    <TabSectionFieldValue>{profileInfo.referred_agent_branch_office_id ? profileInfo.referred_agent_branch_office_id : "-"}</TabSectionFieldValue>
                                </Col>
                                <Col span={12}>
                                    <TabSectionFieldLabel>RAH agent cell number</TabSectionFieldLabel>
                                    <TabSectionFieldValue>{profileInfo.referred_rah_agent_cell_no ? profileInfo.referred_rah_agent_cell_no : "-"}</TabSectionFieldValue>
                                </Col>
                            </Row>
                        </Flex>
                        )}
                        {profileInfo.agent_status == AGENT_STATUS.TRANSFER && (
                            <Col span={8}>
                                <TabSectionFieldLabel>
                                    Agent referral program
                                </TabSectionFieldLabel>
                                <OnboardingFormButton
                                    buttonText="View"
                                    image={DocumentImg}
                                    text="Agent referral program"
                                    formDetails={formDetails}
                                />
                            </Col>
                        )}
                    </>
                )}
            </Row>
        </ContentWrapper>
    )
}

export default RAHRAgent
