import React, { useEffect, useState } from "react";
import { Flex } from "@components/Flex";
import {
	TabSectionFieldLabel,
	TabSectionFieldValue,
} from "../style";
import { Col, Row } from "antd";
import { Checkbox } from "@components/Checkbox";
import SubmitReviewSection from "../SubmitReviewSection";
import moment from "moment";
const isSectionApproved = (review: any, sectionName: string) => {
	const currentReviewSection = review.find(
		(reviewSection: { block: string; }) => reviewSection.block == sectionName
	);
	if (currentReviewSection)
		return currentReviewSection.review
	return false;
}
function RECOLicense(props: any) {
	const {
		profileInfo,
		getUrl,
		fetchApiCallback,
		review,
		isInReview
	} = props;
	const [recoCheckboxes, setRecoCheckBoxes] = useState<any>({
		is_agent_registered_with_reco: false,
		is_trade_legal_name_reflected: false
	})
	const [boards, setBoards] = useState([]);

	useEffect(() => {
		if (profileInfo.board_id_form_to_sign) {
			try {
				let boards = JSON.parse(profileInfo.board_id_form_to_sign);
				if (typeof (boards) == 'object') {
					setBoards(boards);
				}
			} catch (ex) {

			}
		}
	}, [profileInfo.board_id_form_to_sign]);
	useEffect(() => {
		if (boards && boards.length) {
			let recoCheckboxesLocal = { ...recoCheckboxes };
			boards.map((board: any) => {
				recoCheckboxesLocal[`is_agent_registered_with_${board.abbreviation.toLowerCase()}`] = false;
			})
			setRecoCheckBoxes({ ...recoCheckboxesLocal })
		}
	}, [boards]);

	const handleRecoChange = (e: any) => {
		let recoCheckboxesLocal = { ...recoCheckboxes };
		recoCheckboxesLocal[e.target.name] = e.target.checked;
		setRecoCheckBoxes({ ...recoCheckboxesLocal })
	}
	return (
		<SubmitReviewSection
			title="RECO license Information"
			review={review || []}
			endPoint={getUrl}
			block="reco_information"
			fetchApiCallback={fetchApiCallback}
			isInReview={isInReview}
			isApproveDisabled={(Boolean(Object.keys(recoCheckboxes).find((key) => !recoCheckboxes[key])))}
		>
			<Row gutter={16}>
				<Col span={8}>
					<TabSectionFieldLabel>
						Trade name registered under RECO
					</TabSectionFieldLabel>
					<TabSectionFieldValue>
						{
							profileInfo.reco_registration_name
								? profileInfo.reco_registration_name
								: "-"
						}
					</TabSectionFieldValue>
				</Col>
				<Col span={8}>
					<TabSectionFieldLabel>
						Reco registration number
					</TabSectionFieldLabel>
					<TabSectionFieldValue>
						{
							profileInfo.reco_registration_number
								? profileInfo.reco_registration_number
								: "-"
						}
					</TabSectionFieldValue>
				</Col>
				<Col span={8}>
					<TabSectionFieldLabel>
						Legal name
					</TabSectionFieldLabel>
					<TabSectionFieldValue>
						{
							profileInfo.reco_legal_name
								? profileInfo.reco_legal_name
								: "-"
						}
					</TabSectionFieldValue>
				</Col>
				<Col span={8}>
					<TabSectionFieldLabel>
						Year licensed
					</TabSectionFieldLabel>
					<TabSectionFieldValue>
						{
							profileInfo.reco_year_licensed
								? moment(profileInfo.reco_year_licensed).format('YYYY')
								: "-"
						}
					</TabSectionFieldValue>
				</Col>
				<Col span={8}>
					<TabSectionFieldLabel>
						Expiry date
					</TabSectionFieldLabel>
					<TabSectionFieldValue>
						{
							profileInfo.reco_expiry_date
								? moment(profileInfo.reco_expiry_date).format("DD MMMM, YYYY")
								: "-"
						}
					</TabSectionFieldValue>
				</Col>
			</Row>
			{isInReview && !(isSectionApproved(review, "reco_information")) && (
				<Flex direction="column" top={16}>
					<Checkbox
						onChange={handleRecoChange}
						id="is_agent_registered_with_reco"
						name="I can confirm that agent registered with RECO"
						checked={recoCheckboxes["is_agent_registered_with_reco"]}
						style={{ marginBottom: "8px" }}
					/>
					<Checkbox
						onChange={handleRecoChange}
						id="is_trade_legal_name_reflected"
						checked={recoCheckboxes["is_trade_legal_name_reflected"]}
						name="I confirmed that the agent's Trade name & Legal Name is being reflected on the RECO's Website"
						style={{ marginBottom: "8px" }}
					/>
					{boards.map((board: any) => {
						return <Checkbox
							key={board.board_id}
							onChange={handleRecoChange}
							id={`is_agent_registered_with_${board.abbreviation.toLowerCase()}`}
							checked={recoCheckboxes[`is_agent_registered_with_${board.abbreviation.toLowerCase()}`]}
							name={`I can confirm that agent registered with ${board.abbreviation} Regional board`}
							style={{ marginBottom: "8px" }} />
					})}
				</Flex>
			)}

		</SubmitReviewSection>
	);
}

export default RECOLicense;
