import React from 'react'
import { ContentWrapper } from '../style'
import { Description, H5 } from "@components/Typography/Typography";
import { TextLableEnum } from '@constants/onboardingConstants';

function Status(props:any) {
    const {profileInfo} = props;
    const textLable = TextLableEnum[profileInfo.agent_status];

    return (
        <ContentWrapper>
            <H5 text="Agent status" className="font-500" />
            <Description text={textLable} />
        </ContentWrapper>
    )
}

export default Status
