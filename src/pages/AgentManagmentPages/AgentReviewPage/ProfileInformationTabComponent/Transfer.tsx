import OnboardingFormButton from '@components/OnboardingFormButton';
import { H5 } from '@components/Typography/Typography'
import { Col, Row } from 'antd'
import React from 'react'
import { ContentWrapper, TabSectionFieldLabel, TabSectionFieldValue } from '../style'
import DocumentImg from "@assets/icon-form.svg";
import moment from 'moment';

function Transfer(props: any) {
    const { profileInfo } = props;
    const formDetails = { id: "", form_name: "", form_link: profileInfo.transferring_resignation_letter };

    return (
        <ContentWrapper>
            <H5 text="Where is agent transfering from?" className="font-500" />
            <Row gutter={16}>
                <Col span={8}>
                    <TabSectionFieldLabel>
                        Brokerage name
                    </TabSectionFieldLabel>
                    <TabSectionFieldValue>
                        {profileInfo.transferring_from_brokerage_name
                            ? profileInfo.transferring_from_brokerage_name
                            : "-"}
                    </TabSectionFieldValue>
                </Col>
                <Col span={8}>
                    <TabSectionFieldLabel>
                        Resignation date
                    </TabSectionFieldLabel>
                    <TabSectionFieldValue>
                        {profileInfo.transferring_from_resignation_date
                            ? moment(profileInfo.transferring_from_resignation_date).format("DD MMMM, YYYY")
                            : "-"}
                    </TabSectionFieldValue>
                </Col>
            </Row>
            <Row gutter={16}>
                <Col span={8}>
                    <TabSectionFieldLabel>
                        Resignation later
                    </TabSectionFieldLabel>
                    {profileInfo.transferring_resignation_letter
                        ? <OnboardingFormButton
                            buttonText="View"
                            image={DocumentImg}
                            text="Resignation later"
                            formDetails={formDetails}
                        />
                        : "-"}

                </Col>
            </Row>

        </ContentWrapper>
    )
}

export default Transfer
