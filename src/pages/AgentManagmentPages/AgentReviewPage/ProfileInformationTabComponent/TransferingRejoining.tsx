import React from 'react'
import { ContentWrapper } from '../style'
import { Description, H5 } from "@components/Typography/Typography";

function TransferingRejoining(props: any) {
    const { profileInfo } = props;

    return (
        <ContentWrapper>
            <H5 text="Transferring / Rejoining RAHR" className="font-500" />
            <Description text={profileInfo.reinstated_with_reco_options
                ? "Transfer from another agency"
                : "Rejoining RAHR"
            } />
        </ContentWrapper>
    )
}

export default TransferingRejoining
