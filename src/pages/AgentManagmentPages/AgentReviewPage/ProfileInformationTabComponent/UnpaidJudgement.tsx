import { H5 } from '@components/Typography/Typography'
import { formatNumberWithComma } from '@utils/format';
import { Col, Row } from 'antd'
import React from 'react'
import { ContentWrapper, TabSectionFieldLabel, TabSectionFieldValue } from '../style'

function UnpaidJudgement(props: any) {
    const { profileInfo } = props;
    let judgements = [];
    try {
        judgements = JSON.parse(profileInfo.unpaid_judgements_against_you_data);
    } catch (ex) { }
    return (
        <ContentWrapper>
            <H5 text="Unpaid judgements against agent" className="font-500" />
            <Row gutter={16}>
                <Col span={24}>
                    <TabSectionFieldLabel>
                        Are there any unpaid judgements against agent?
                    </TabSectionFieldLabel>
                    <TabSectionFieldValue>{profileInfo.unpaid_judgements_against_you ? "Yes" : "No"}</TabSectionFieldValue>
                </Col>
                {profileInfo.unpaid_judgements_against_you == 1 && judgements.map((item: any, key: number) => (
                    <React.Fragment key={key}>
                        <Col span={24}>
                            <TabSectionFieldLabel>
                                Amount of judgement
                            </TabSectionFieldLabel>
                            <TabSectionFieldValue>$ {formatNumberWithComma(item.judgment_amount)}</TabSectionFieldValue>
                        </Col>
                        <Col span={24}>
                            <TabSectionFieldLabel>
                                Name of judgement creditor
                            </TabSectionFieldLabel>
                            <TabSectionFieldValue>{item.name_judgment_creditor}</TabSectionFieldValue>
                        </Col>
                    </React.Fragment>
                ))}
            </Row>
        </ContentWrapper>
    )
}

export default UnpaidJudgement
