import { H5 } from '@components/Typography/Typography'
import { Col, Row } from 'antd'
import React from 'react'
import { ContentWrapper, TabSectionFieldValue } from '../style'

function WorkingAtRAHR(props: any) {
    const { profileInfo } = props;
    return (
        <ContentWrapper className="bordernone">
            <H5 text="The agent’s goals for 6 months / 1 year / 2 years working at RAHR" className="font-500" />
            <Row gutter={16}>
                <Col span={24}>
                    <TabSectionFieldValue>
                        {
                            profileInfo.your_goal
                                ? profileInfo.your_goal
                                : "-"
                        }
                    </TabSectionFieldValue>
                </Col>

            </Row>
        </ContentWrapper>
    )
}

export default WorkingAtRAHR
