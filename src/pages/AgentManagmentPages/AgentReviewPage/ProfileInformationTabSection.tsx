import { EditIconButton, Primary, Transparent } from '@components/Buttons'
import { Flex } from '@components/Flex'
import React from 'react'
import { TabItemLayout } from './style'
import ExportIcon from "@assets/icons/export.png";
import { H2 } from '@components/Typography/Typography';
import Status from './ProfileInformationTabComponent/Status';
import BrokerSalesRepresentative from './ProfileInformationTabComponent/BrokerSalesRepresentative';
import AgentToJoinRAHR from './ProfileInformationTabComponent/AgentToJoinRAHR';
import BranchAgent from './ProfileInformationTabComponent/BranchAgent';
import ParkedAgent from './ProfileInformationTabComponent/ParkedAgent';
import RAHRAgent from './ProfileInformationTabComponent/RAHRAgent';
import RECOLicense from './ProfileInformationTabComponent/RECOLicense';
import ComplaintRECOorTRREB from './ProfileInformationTabComponent/ComplaintRECOorTRREB';
import UnpaidJudgement from './ProfileInformationTabComponent/UnpaidJudgement';
import GarnishmentOrder from './ProfileInformationTabComponent/GarnishmentOrder';
import WorkingAtRAHR from './ProfileInformationTabComponent/WorkingAtRAHR';
import Notification from './ProfileInformationTabComponent/Notification';
import AttachedDocuments from './ProfileInformationTabComponent/AttachedDocuments';
import { AGENT_STATUS } from '@pages/AgentOnboardingPages/ProfileInformationPage/helper';
import Transfer from './ProfileInformationTabComponent/Transfer';
import PreviousAgent from './ProfileInformationTabComponent/PreviousAgent';
import TransferingRejoining from './ProfileInformationTabComponent/TransferingRejoining';
import BoardMembershipNumber from './ProfileInformationTabComponent/BoardMembershipNumber';



function ProfileInformationTabSection(props: any) {
    const { data, getUrl, fetchApiCallback, isInReview, gotoNextStep, currentStep, gotoPrevStep } = props;
    const { profile_info: profileInfo, review, user_branch } = data;
    const otherProps = {
        review: review && review.review,
        getUrl,
        fetchApiCallback,
        isInReview
    };
    if (!profileInfo) return null;
    return (
        <TabItemLayout>
            <Flex>
                <Flex flex={1}>
                    <H2 text="Profile contact details" className="font-600" />
                </Flex>

                <EditIconButton
                    icon={ExportIcon}
                    text={
                        <span style={{ fontWeight: 600, color: "#4E1C95", fontSize: 14 }}>
                            Export for Broker Wolf
                        </span>
                    }
                />
            </Flex>
            <Flex >
                <Status profileInfo={profileInfo} />
            </Flex>
            {profileInfo.agent_status == AGENT_STATUS.REINSTATED_WITH_RECO && (
                <Flex >
                    <TransferingRejoining profileInfo={profileInfo} />
                </Flex>
            )}
            {(profileInfo.agent_status == AGENT_STATUS.REINSTATED_WITH_RECO
                || profileInfo.agent_status == AGENT_STATUS.TRANSFER)
                && (
                    <Flex >
                        <Transfer profileInfo={profileInfo} />
                    </Flex>
                )}
            <Flex >
                <BrokerSalesRepresentative profileInfo={profileInfo} />
            </Flex>
            <Flex >
                <AgentToJoinRAHR profileInfo={profileInfo} />
            </Flex>
            <Flex>
                <BranchAgent user_branch={user_branch} />
            </Flex>
            <Flex>
                <ParkedAgent profileInfo={profileInfo} />
            </Flex>
            {profileInfo.agent_status == AGENT_STATUS.TRANSFER && (
                <Flex >
                    <PreviousAgent profileInfo={profileInfo} />
                </Flex>
            )}
            {(profileInfo.agent_status == AGENT_STATUS.TRANSFER
                || profileInfo.agent_status == AGENT_STATUS.NEW_REGISTRATION)
                && (
                    <Flex>
                        <RAHRAgent profileInfo={profileInfo} />
                    </Flex>
                )}
            <Flex top={20}>
                <RECOLicense profileInfo={profileInfo} {...otherProps} />
            </Flex>
            {profileInfo.agent_status == AGENT_STATUS.NEW_REGISTRATION
                && (
                    <Flex top={20}>
                        <AttachedDocuments profileInfo={profileInfo} {...otherProps} />
                    </Flex>
                )}

            {profileInfo.agent_status == AGENT_STATUS.TRANSFER
                && (
                    <Flex top={20}>
                        <BoardMembershipNumber profileInfo={profileInfo} {...otherProps} />
                    </Flex>
                )}

            <Flex>
                <ComplaintRECOorTRREB profileInfo={profileInfo} />
            </Flex>
            <Flex>
                <UnpaidJudgement profileInfo={profileInfo} />
            </Flex>
            <Flex>
                <GarnishmentOrder profileInfo={profileInfo} />
            </Flex>
            <Flex>
                <Notification profileInfo={profileInfo} />
            </Flex>
            <Flex>
                <WorkingAtRAHR profileInfo={profileInfo} />
            </Flex>

            {isInReview && (<Flex className="footerbtn">
                <Primary

                    text="Next step"
                    className="submitbtn"
                    style={{ marginRight: "2px" }}
                    onClick={() => {
                        gotoNextStep(currentStep);
                    }}
                />

                <Transparent
                    onClick={() => {
                        gotoPrevStep(currentStep);
                    }}
                    text="Prev step"
                />
            </Flex>)}


        </TabItemLayout>
    )
}

export default ProfileInformationTabSection
