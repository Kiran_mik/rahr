import React from "react";
import { HomeOutlined } from "@ant-design/icons";
import { Flex } from "@components/Flex";
import { H5TITLE } from "./style";
interface StatusItemProps {
  label: string;
  value?: string;
  Icon: any;
  color: string;
}
function StatusItem(props: StatusItemProps) {
  const { label, value, Icon, color } = props;
  return (
    <Flex left={10} alignItems={"center"}>
      <div style={{ backgroundColor: color, padding: "12px 13px", borderRadius: 5, lineHeight: "0px", width: "40px" , height: "40px" }}>
        <Icon style={{ fontSize: 14 }} />
      </div>
      <Flex direction="column" left={12}>
        <span style={{ color: "#AFADC8", fontWeight: 500, fontSize: 12 }}>
          {label}
        </span>
        <H5TITLE text={value || '-'} />
      </Flex>
    </Flex>
  );
}

export default StatusItem;
