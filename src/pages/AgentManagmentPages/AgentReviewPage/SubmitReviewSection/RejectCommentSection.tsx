import { Primary } from "@components/Buttons/index";
import { Flex } from "@components/Flex";
import TextAreaInput from "@components/Input/TextArea";
import { FormLableBoldTypography } from "@components/Typography/Typography";
import { FlexBox } from "@pages/UserManagementPages/StaffMembers/NewStaff/style";
import { Avatar } from "antd";
import React from "react";
import { RejectCommentEditButton, RejectCommentSectionWrapper } from "./style";

function RejectCommentSection(props: any) {
  let { editable, onAction } = props;
  const { values, errors, setFieldValue, touched, handleSubmit } = props.formik;
  return (
    <RejectCommentSectionWrapper>
      <Flex direction="row">
        <Flex flex={0.1}>
          <Avatar
            style={{ width: 35, height: 35 }}
            icon={<img src={"https://joeschmoe.io/api/v1/random"} />}
          />
        </Flex>
        <Flex flex={1.5}>
          <Flex flex={1} direction="column">
            {editable ? (
              <>
                <TextAreaInput
                  name="comment"
                  id="comment"
                  placeholder="Please provide rejection reason"
                  onChange={(e: any) => {
                    setFieldValue(`comment`, e.target.value);
                  }}
                  value={values.comment}
                  error={touched && touched.comment && errors.comment}
                />
                <FlexBox className="add-comment-btn">
                  <Primary
                    text="Add Comment"
                    className="submitbtn"
                    onClick={handleSubmit}
                  />
                </FlexBox>
              </>
            ) : (
              <>
                <FormLableBoldTypography>
                  {values.comment}
                </FormLableBoldTypography>
                <FlexBox className="add-comment-btn">
                  <RejectCommentEditButton onClick={() => onAction("edit")}>
                    Edit
                  </RejectCommentEditButton>
                  {/* <RejectCommentEditButton onClick={() => onAction("delete")}>
                    Delete
                  </RejectCommentEditButton> */}
                </FlexBox>
              </>
            )}
          </Flex>
        </Flex>
      </Flex>
    </RejectCommentSectionWrapper>
  );
}

export default RejectCommentSection;
