import services from "@services/index";
import * as Yup from "yup";

export const reviewValidationSchema = Yup.object({
    comment: Yup.string()
      .required("Please enter rejection reason")
      .nullable(),
  });
  
  export const reviewInitialValues = {
    comment: "",
  };
  
  export const changeReviewStatus = async (endPoint: string, body: any) => {
    try {
      const res = await services.post(
        `v1/agents/onboarding/reviews/` + endPoint,
        body,
        process.env.REACT_APP_BASE_URL_AGENT
      );
      return res;
    } catch (error) {}
  };
  