import { CheckOutlined, CloseOutlined } from "@ant-design/icons";
import { Flex } from "@components/Flex";
import { H4 } from "@components/Typography/Typography";
import { useParams } from "@reach/router";
import { updateRawData } from "@utils/helpers";
import { Formik } from "formik";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import {
  changeReviewStatus,
  reviewInitialValues,
  reviewValidationSchema,
} from "./helper";
import RejectCommentSection from "./RejectCommentSection";
import { CheckFormSectionButton, TabSectionComponentLayout } from "./style";

interface ReviewBoxComponentProps {
  title: string;
  review: any;
  endPoint: any;
  block: string;
  fetchApiCallback: any;
  children: any;
  isInReview?: boolean;
  isApproveDisabled?: boolean
}

function SubmitReviewSection(props: ReviewBoxComponentProps) {
  const rawData = useSelector((state: any) => state.rawData);
  const { title, review, endPoint, block, fetchApiCallback, isInReview } = props;
  const params = useParams();
  const { agentId } = params;

  const [reviewStatus, setReviewStatus] = useState(-1);
  const [editable, setEditable] = useState(false);
  const [reviewCommentValue, setReviewCommentValue] = useState(
    reviewInitialValues
  );

  useEffect(() => {
    if (isInReview && review && review.length) {
      const reviewObj = review.find((i: any) => i.block === block) || {};
      const reviewStatusValue =
        reviewObj.review == 1 ? 1 : reviewObj.review == 0 ? 0 : -1;
      setReviewStatus(reviewStatusValue);
      setReviewCommentValue({
        comment: reviewObj.comment,
      });
    }
  }, [review, isInReview]);


  const rejectSubmitHanlder = async (values: any) => {
    const body: any = {
      agent_id: agentId,
      review: {
        block: block,
        review: 0,
        comment: values.comment,
      },
    };
    changeStatusApiCall(body);
  };

  const onConfirmReview = async () => {
    updateRawData({ [block]: true });
    const body: any = {
      agent_id: agentId,
      review: {
        block: block,
        review: 1,
      },
    };
    changeStatusApiCall(body);
  };

  const changeStatusApiCall = async (body: any) => {
    try {
      const res: any = await changeReviewStatus(endPoint, body);

      setEditable(false);
      if (res) {
        await fetchApiCallback(body.review.review ? block : false);
        setReviewStatus(body.review.review);
      }
    } catch (error) { }
  };

  const onAction = (type: string) => {
    setEditable(true);
    // if (type == "delete") {
    //   setReviewCommentValue({
    //     comment: "",
    //   });
    // }
  };
  const checkBoxBorderColor = () => {
    return isInReview ? (reviewStatus == 1
      ? "#31AF91"
      : reviewStatus == 0
        ? "#CF1322"
        : "rgba(123, 123, 151, 1)") : "transparent";
  };


  return (
    <TabSectionComponentLayout style={{ borderColor: checkBoxBorderColor() }}>
      <Flex alignItems={"baseline"}>
        <Flex flex={1}>
          <H4 text={title} />
        </Flex>
        {isInReview && (<Flex>
          <CheckFormSectionButton
            onClick={() => {
              if (props.isApproveDisabled) return;
              onConfirmReview()
            }}
            isLoading={rawData[block]}
            icon={
              <CheckOutlined
                size={10}
                style={{ color: reviewStatus == 1 ? "#FFFFFF" : "#000" }}
              />
            }
            bgColor={reviewStatus == 1 ? "#31AF91" : "#FFFFFF"}
          />
          <CheckFormSectionButton
            onClick={() => {
              setReviewStatus(0);
              setEditable(true);
            }}
            icon={
              <CloseOutlined
                size={10}
                style={{ color: reviewStatus == 0 ? "#FFFFFF" : "#000" }}
              />
            }
            bgColor={reviewStatus == 0 ? "#CF1322" : "#FFFFFF"}
          />
        </Flex>
        )}
      </Flex>
      <Formik
        initialValues={reviewCommentValue}
        onSubmit={(values: any) => {
          rejectSubmitHanlder(values);
        }}
        validationSchema={reviewValidationSchema}
        enableReinitialize={true}
      >
        {(formikProps: any) => {
          return (
            <>
              {reviewStatus == 0 ? (
                <RejectCommentSection
                  formik={formikProps}
                  editable={editable}
                  onAction={onAction}
                />
              ) : null}
            </>
          );
        }}
      </Formik>
      {props.children}
    </TabSectionComponentLayout>
  );
}

export default SubmitReviewSection;
