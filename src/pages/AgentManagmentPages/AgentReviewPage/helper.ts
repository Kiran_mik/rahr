import services from "@services/index";
import * as Yup from "yup";
import { dateFormat } from '@utils/format';


import {
  CalendarOutlined,
  CarryOutOutlined,
  FormOutlined,
  HomeOutlined,
} from "@ant-design/icons";
import { BASE_URL_MAP } from "@constants/index";

export const STATUS_ITEMS = [
  {
    label: "Branch",
    color: "#FFC19E",
    value: "Don Mills",
    Icon: HomeOutlined,
  },
  {
    label: "Start Date",
    color: "#8FD4FF",
    value: "21 Mar, 2021",
    Icon: CalendarOutlined,
  },
  {
    label: "Ticket #",
    color: "#E8D1F9",
    value: "5482",
    Icon: CarryOutOutlined,
  },
  {
    label: "Broker Wolf ID",
    color: "#87E8DE",
    value: "-",
    Icon: FormOutlined,
  },
];
export const NEW_OR_TERMINATING_ITEMS = (branch?: string, date?: any, ticketId?: any, id?: any) => [
  {
    label: "Branch",
    color: "#FFC19E",
    value: branch,
    Icon: HomeOutlined,
  },
  {
    label: "Start Date",
    color: "#8FD4FF",
    value: date && dateFormat(date, 'DD MMM, YYYY'),
    Icon: CalendarOutlined,
  },
  {
    label: "Ticket #",
    color: "#E8D1F9",
    value: ticketId || "-",
    Icon: CarryOutOutlined,
  },
  {
    label: "Broker Wolf ID",
    color: "#87E8DE",
    value: id || "-",
    Icon: FormOutlined,
  },
];

export const TERMINATED_OR_ACTIVE_ITEMS = (branch: string, id: any, date: any) => {
  return ([
    {
      label: "Branch",
      color: "#FFC19E",
      value: branch,
      Icon: HomeOutlined,
    },
    {
      label: "Broker Wolf ID",
      color: "#87E8DE",
      value: id || "-",
      Icon: FormOutlined,
    },
    {
      label: "Termination date",
      color: "#8FD4FF",
      value: date && dateFormat(date, 'DD MMM, YYYY'),
      Icon: CalendarOutlined,
    }
  ]);
}

/**
 * Api for getting the tab content data
 */
export const getTabsData = async (endPoint: any, agentId: any) => {
  try {
    const res = await services.get(
      `v1/agents/onboarding/reviews/` + endPoint + "/" + agentId,
      process.env.REACT_APP_BASE_URL_AGENT
    );
    //@ts-ignore
    return res.data.data;
  } catch (err) {
    return [];
  }
};

export const reviewValidationSchema = Yup.object({
  comment: Yup.string()
    .required("Please enter rejection reason")
    .nullable(),
});

export const reviewInitialValues = {
  comment: "",
};

export const changeReviewStatus = async (endPoint: string, body: any) => {
  try {
    const res = await services.post(
      `v1/agents/onboarding/reviews/` + endPoint,
      body,
      process.env.REACT_APP_BASE_URL_AGENT
    );
    return res;
  } catch (error) { }
};


/**
 * Api for getting the tab content data
 */
export const getHeaderInfo = async (agentId: any) => {
  try {
    const res = await services.get(
      `v1/agents/onboardings/header-info/` + agentId,
      process.env.REACT_APP_BASE_URL_AGENT
    );
    //@ts-ignore
    return res.data.data;
  } catch (err) {
    return [];
  }
};


export const getTabInfo = async (agentId: any) => {
  try {
    const res = await services.get(
      `v1/agents/onboardings/tab-info/` + agentId,
      process.env.REACT_APP_BASE_URL_AGENT
    );
    //@ts-ignore
    return res.data.data;
  } catch (err) {
    return [];
  }
};


export const submitReview = async (agentId: number, isApprove: boolean) => {
  try {
    const res = await services.post(
      `v1/agents/onboarding/reviews/submit`,
      {
        agent_id: agentId,
        type: isApprove ? "approve" : "feedback"
      },
      BASE_URL_MAP["agent-onboard"]
    );
    return res;
  } catch (error) { }
}


export const exportOptions = [
  { description: 'Transfer agent' },
  { description: 'Export to Broker Wolf' }
]