import { UserBreadWrap } from "@components/CommonLayouts/style";
import { Flex } from "@components/Flex";
import StaffHeader from "@components/StaffHeader";
import { H3 } from "@components/Typography/Typography";
import { USER_STATUS_ENUM } from "@constants/userConstants";
import DashboardLayout from "@pages/UserManagementPages/StaffMembers/DashboardLayout";
import React, { useState, useEffect } from "react";
import AgentInfoHeader from "./AgentInfoHeader";
import AgentReviewTabLayout from "./AgentReviewTabLayout";
import { getHeaderInfo, getTabInfo, submitReview } from "./helper";
import { useParams } from "@reach/router";
import Loader from "@components/Loader";

function AgentReviewPage(props: { path?: string, agentId?: number, isTerminating?: boolean, isParking?: boolean }) {
  const params = useParams()
  const [userData, setUserData] = useState(null)
  const [progress, setProgress] = useState<null | number>(null)
  const [statusBar, setStatusBar] = useState(false)
  const [showProgress, setShowProgress] = useState(false);
  const [tabStatuses, setTabStatus] = useState({});
  const [agentStatus, setAgentStatus] = useState(0);
  const [finalButtonState, setFinalButtonState] = useState(0);
  const [finalButtonHeaderText, setFinalButtonHeaderText] = useState("");
  const [finalSubmitButtonLoadingStatus, setFinalSubmitButtonLoadingStatus] = useState(false);
  useEffect(() => {
    const tabValues = Object.values(tabStatuses);
    const firstActiveStep = tabValues.indexOf(0);
    if (firstActiveStep == 7 || firstActiveStep == -1) {
      if (tabValues.indexOf(2) != -1) {
        setFinalButtonState(2);
      } else {
        setFinalButtonState(1);
      }
    } else {
      setFinalButtonState(0);
    }

    setFinalButtonHeaderText(tabValues.indexOf(2) == -1 ? "Approve applicatiom" : "Submit feedback")

  }, [tabStatuses])

  const finalFormSubmit = async () => {
    setFinalSubmitButtonLoadingStatus(true);
    const isApprove = finalButtonState == 1 ? true : false;
    const agentId = params.agentId;

    const res: any = await submitReview(agentId, isApprove)
    if (res.status && isApprove) {
      setAgentStatus(USER_STATUS_ENUM.ACTIVE);
    }
    setFinalSubmitButtonLoadingStatus(false);
  }

  const updateTabStatuses = async () => {
    const tabInfo: { tabs_statuses: any, progress: number } = await getTabInfo(props.agentId);
    setProgress(tabInfo.progress);
    setTabStatus(tabInfo.tabs_statuses)
  }
  const getData = async () => {
    const agentDetails: any = await getHeaderInfo(props.agentId);
    setAgentStatus(agentDetails.status);
    setShowProgress(true)
    setUserData(agentDetails)
    setStatusBar(true)
    if (agentDetails.status === USER_STATUS_ENUM.PENDING_REVIEW) {
      updateTabStatuses();
    }
  }
  useEffect(() => {
    getData()
  }, [])

  return (
    <DashboardLayout>
      <StaffHeader>
        <UserBreadWrap>
          <H3 text="Agent onboarding application" className="font-500" />
        </UserBreadWrap>
      </StaffHeader>
      {
        statusBar ?
          <React.Fragment>
            <Flex direction="column" top={10}>
              <AgentInfoHeader
                showProgress={showProgress}
                userData={userData}
                progress={Number(progress)}
                progressStatus="Reviewing agent onboarding application"
                buttonText={finalButtonHeaderText}
                onChange={finalFormSubmit}
                isTerminating={props.isTerminating}
                isParking={props.isParking}
              />
            </Flex>
            <Flex top={40}>
              <AgentReviewTabLayout
                userData={userData}
                headerRefreshCallback={getData}
                tabStatuses={tabStatuses}
                updateTabStatuses={updateTabStatuses}
                isInReview={agentStatus == USER_STATUS_ENUM.PENDING_REVIEW}
                finalButtonState={finalButtonState}
                finalFormSubmit={finalFormSubmit}
                finalSubmitButtonLoadingStatus={finalSubmitButtonLoadingStatus}
                isTerminating={props.isTerminating}
                isParking={props.isParking}
              />
            </Flex>
          </React.Fragment>
          :
          <Loader loading={true} />
      }
    </DashboardLayout >
  );
}

export default AgentReviewPage;
