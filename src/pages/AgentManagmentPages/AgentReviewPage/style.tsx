import React from "react";

import { FormLableTypography } from "../../../shared/components/Typography/Typography";
import { BorderBtn } from "@components/Buttons";
import { Flex } from "@components/Flex";
import styled from "styled-components";
import { CheckCircleFilled } from "@ant-design/icons";
import { H5 } from "@components/Typography/Typography";


export const AgentInfoContainer = styled(Flex)`
  display: flex;
  flex-direction: column;
  padding: 20px 32px;

  position: static;
  width: 100%;
  min-height: auto;
  left: 0px;

  background: #ffffff;
  border: 1px solid #f5f5ff;
  box-sizing: border-box;
  box-shadow: 0px 4px 12px rgba(30, 28, 108, 0.1);
  border-radius: 15px;
`;

export const H5TITLE = styled(H5)`
  margin:0;
`;

export const TabLayoutContainer = styled.div`
  flex: 1;
  .ant-tabs-nav-list {
    width: 100%;
  }
  .card-container p {
    margin: 0;
  }
  .card-container > .ant-tabs-card .ant-tabs-content {
    height: auto;
    margin-top: -16px;
  }
  .card-container > .ant-tabs-card .ant-tabs-content > .ant-tabs-tabpane {
    padding: 16px;
    background: #fff;
  }
  .card-container > .ant-tabs-card > .ant-tabs-nav::before {
    display: none;
  }
  .card-container > .ant-tabs-card .ant-tabs-tab,
  [data-theme="compact"] .card-container > .ant-tabs-card .ant-tabs-tab {
    background: rgba(255, 255, 255, 0.5);
    border-color: transparent;
    border-radius: 10px 10px 0px 0px;
    flex: 1;
  }
  .card-container > .ant-tabs-card .ant-tabs-tab-active,
  [data-theme="compact"] .card-container > .ant-tabs-card .ant-tabs-tab-active {
    background: #fff;
    border-color: #fff;
  }
  .ant-tabs-tab.ant-tabs-tab-active .ant-tabs-tab-btn {
    color: rgba(80, 81, 79, 1) !important;
  }
  #components-tabs-demo-card-top .code-box-demo {
    padding: 24px;
    overflow: hidden;
    background: #f5f5f5;
  }
  [data-theme="compact"] .card-container > .ant-tabs-card .ant-tabs-content {
    height: 120px;
    margin-top: -8px;
  }
  .ant-tabs-tab-btn{
    display:flex;
  }
  .ant-tabs-tab-btn .tabtitle{
    font-size:14px;
    white-space: nowrap;
    width: 88px;
    overflow: hidden;
    text-overflow: ellipsis;
  }
  .ant-notification {
    background:#000
  }
`;

export const TabItemLayout = styled.div`
  padding: 20px;
  .footerbtn {
    margin: 24px 0px;
    border-top: 1px solid #a2a2ba;
    padding-top: 24px;
    .submitbtn {
      padding: 12px 24px;
      background: #4e1c95;
      color: #fff;
      font-family: Poppins;
      font-size: 14px;
      line-height: 22px;
      border-radius: 8px;
      width: max-content;
      cursor: pointer;
    }
    .borderbtn {
      border: 1px solid #bdbdd3;
      color: #17082d;
      padding: 13px 24px;
      font-weight: 600;
    }
  }
`;

export const TabSectionComponentLayout = styled.div`
  border: 1px solid rgba(123, 123, 151, 1);
  padding: 10px 8px;
  width: 100%;
  border-radius: 8px;
`;

export const CheckFormSectionButton = (props: any) => {
  return (
    <BorderBtn
      {...props}
      style={{
        width: 32,
        height: 32,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        marginLeft: 10,
        padding: 0,
        backgroundColor: props.bgColor ? props.bgColor : "#FFFFFF",
      }}
    />
  );
};

export const TabSectionFieldLabel = styled(FormLableTypography)`
  font-size: 14px;
  color: #7b7b97;
  font-weight: 600;
  line-height: 32px;
  margin-bottom: 4px;
`;
export const TabSectionFieldValue = styled.span`
  font-size: 14px;
  font-weight: 400;
  white-space: pre-line;
`;

export const ContentWrapper = styled.div`
  border-bottom: 1px dashed #a2a2ba;
  padding: 33px 0px;
  width: 100%;
  &.bordernone {
    border-bottom: none;
  }
  &.space-bottom-0 {
    padding-bottom: 0px;
  }
  .borderbtn {
    border: 1px solid rgb(78, 28, 149);
    border-color: #4e1c95;
    padding: 3px 10px;
    font-size: 14px;
    border-radius: 8px;
  }
`;
export const CheckedIcon = styled(CheckCircleFilled)`
  font-size: 15px !important;
  color: #31af91;
  margin-right: 5px;
`;

export const RejectCommentSectionWrapper = styled.div`
  margin-top: 20px;
  padding: 20px;
  width: 100%;
  background-color: #f5f5ff;
  border-radius: 16px;

  .add-comment-btn {
    padding: 0px;
    .submitbtn {
      padding: 12px 24px;
      background: #4e1c95;
      color: #fff;
      font-family: Poppins;
      font-size: 14px;
      line-height: 22px;
      border-radius: 8px;
      width: max-content;
      cursor: pointer;
    }
  }

  textarea.ant-input {
    height: 88px;
    border-radius: 8px;
    border-color: #bdbdd3 !important;
  }
  .ant-input-textarea-show-count::after {
    visibility: hidden;
  }
`;

export const RejectCommentEditButton = styled.button`
  background-color: transparent;
  background: transparent;
  border-color: transparent;
  height: auto;
  font-size: 13px;
  line-height: 32px;
  font-weight: 500;
  color: #4e1c95;
  font-family: Poppins;
  cursor: pointer;
  position: relative;
  margin-right: 10px;
  padding: 0px;
`;


export const NonCollasibleWrapper = styled.div`
display: flex;
width: 100%;
justify-content: space-between;
padding: 7px 16px;
background: #FCFCFF;
border: 1px solid #DBDBED;
border-radius: 10px;
margin:7px 0px;
align-items: center;
.title{
  font-weight: 500;
font-size: 14px;
line-height: 22px;
color: #17082D;
}
.icon{
  color: #31AF91;
  background: #F2FFF7;
  padding: 5px 8px;
  font-size: 12px;
  line-height: 20px;
  display:flex;
  align-items:center;
  .anticon {
    margin-right:4px
  }
}
`;
export const NonCollasibleBody = styled.div`
width: 100%;
padding: 24px;
background: #fff;
border: 1px solid #DBDBED;
border-radius: 10px;
margin:7px 0px;
align-items: center;
.title{
  font-weight: 500;
font-size: 14px;
line-height: 22px;
color: #17082D;
}
.icon{
  color: #31AF91;
  background: #F2FFF7;
  padding: 5px 8px;
  font-size: 12px;
  line-height: 20px;
  display:flex;
  align-items:center;
  .anticon {
    margin-right:4px
  }
}
`;

export const CollapsibleWrapper = styled.div`

margin:7px 0px;
.ant-collapse{

padding: 11px 16px;
background: #FCFCFF;
border: 1px solid #DBDBED;
border-radius: 10px;

align-items: center;
}
.ant-collapse > .ant-collapse-item {
  border-bottom: none;
}
.ant-collapse-header{
  padding: 0px !important;
  font-weight: 500;
  font-size: 14px;
  line-height: 22px  !important;
  color: #17082D;
}
.ant-collapse-content{
  border-top:none;
}
.ant-collapse-content > .ant-collapse-content-box {
  background: #FCFCFF;
  padding: 16px 0px;
}
.ant-collapse > .ant-collapse-item > .ant-collapse-header .ant-collapse-arrow{
  float: right;
  margin-right: 0px;
}
`;

