import TextAreaInput from "@components/Input/TextArea";
import { FormLableTypography } from "@components/Typography/Typography";
import { Col, Row } from "antd";
import React from "react";
import { SectionProps } from "./helper";

function AdditionalSJPSection(props: SectionProps) {
  const { onChange,  id, name, formik } = props;
  return (
    <>
      <FormLableTypography>
        Anything specific you want add to SJP <span>*</span>
      </FormLableTypography>
      <Row>
        <Col md={12}>
          <TextAreaInput
            id={id}
            name={name}
            onChange={onChange}
            placeholder="Enter notes"
            className="hidecounter"
            error={formik && (formik.touched.addtional_sjp && formik.errors.addtional_sjp)}
          />
        </Col>
      </Row>
    </>
  );
}

export default AdditionalSJPSection;
