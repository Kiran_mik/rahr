import SelectInput from "@components/Select";
import {
  FormLableBoldTypography,
  FormLableTypography
} from "@components/Typography/Typography";
import { Col, Row } from "antd";
import { Select } from "antd";

import React from "react";
import { SectionProps } from "./helper";
const { Option } = Select;

interface Props extends SectionProps {
  branchList: Array<any>;
  error?: string;
}
function AgentAdditionalInfo(props: Props) {
  const { branchList, id, onChange, name, value, error } = props;
  return (
    <>
      <Row>
        <Col span={6}>
          <FormLableTypography>
            Branch
            <span>*</span>
          </FormLableTypography>

          <SelectInput
            className="bottom-0"
            placeholdertitle="Start typing or select from list"
            onChange={(e: any) => {
              onChange({ target: { value: e, id: id, name: name } }, e);
            }}
            name={name}
            id={id}
            value={value}
            error={error}
          >
            {branchList.map((item: any) => {
              return (
                <>
                  <Option key={item.id} value={item.id}>{item.branch_name}</Option>
                </>
              );
            })}
          </SelectInput>
        </Col>
      </Row>
    </>
  );
}

export default AgentAdditionalInfo;
