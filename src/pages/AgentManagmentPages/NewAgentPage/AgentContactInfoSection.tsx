import React from "react";

import { Row, Col } from "antd";
import {
  FormLableBoldTypography,
  FormLableTypography
} from "@components/Typography/Typography";
import { InputField } from "@components/Input/Input";
import { FormikValues } from "formik";

function AgentContactInfoSection(props: { formik: FormikValues,onChange:(e:any)=>void }) {
  const { formik,onChange } = props;
  const {  values, errors, touched } = formik;
   
  return (
    <>
      <FormLableBoldTypography className="spaceBottom-16">
        Contact information
      </FormLableBoldTypography>
      <Row gutter={16}>
        <Col span={6}>
          <FormLableTypography>
            First name<span>*</span>
          </FormLableTypography>
          <InputField
            value={values.first_name}
            error={touched.first_name && errors.first_name}
            onChange={onChange}
            id="first_name"
            name="first_name"
            placeholder="First name"
          />
        </Col>
        <Col span={6}>
          <FormLableTypography>
            Last name<span>*</span>
          </FormLableTypography>
          <InputField
            value={values.last_name}
            error={touched.last_name && errors.last_name}
            onChange={onChange}
            id="last_name"
            name="last_name"
            placeholder="Last name"
          />
        </Col>
        <Col span={6}>
          <FormLableTypography>
            Email<span>*</span>
          </FormLableTypography>
          <InputField
            value={values.email}
            error={touched.email && errors.email}
            onChange={onChange}
            id="email"
            name="email"
            placeholder="Email"
          />
        </Col>
        <Col span={6}>
          <FormLableTypography>
            Cell phone number<span>*</span>
          </FormLableTypography>
          <InputField
            value={values.phone_number}
            error={touched.phone_number && errors.phone_number}
            onChange={onChange}
            id="phone_number"
            name="phone_number"
            placeholder="+1 (647) 223 - 3215"
          />
        </Col>
      </Row>
    </>
  );
}

export default AgentContactInfoSection;
