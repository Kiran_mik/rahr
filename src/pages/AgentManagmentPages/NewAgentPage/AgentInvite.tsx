import React, { useEffect, useState } from "react";

import { Formik } from "formik";
import {  H2 } from "@components/Typography/Typography";
import { FormWrapper } from "@components/CommonLayouts/style";
import { Flex } from "@components/Flex";
import { Primary, Transparent } from "@components/Buttons";

import AgentStatusSection from "./AgentStatusSection";
import AgentContactInfoSection from "./AgentContactInfoSection";
import AgentParkedStatusSection from "./AgentParkedStatusSection";
import AgentJoiningSpecialPackageStatusSection from "./AgentJoiningSpecialPackageStatusSection";
import AdditionalSJPSection from "./AdditionalSJPSection";
import AgentAdditionalInfo from "./AgentAdditionalInfo";
import {
  AGENT_INITIAL_VALUES,
  getBranchList,
  transformAndInviteAgent,
  validationSchema,
} from "./helper";
import { navigate } from "@reach/router";
import { FlexBox } from "@pages/UserManagementPages/StaffMembers/NewStaff/style";

import AgentRECOStatus from "./AgentRECOStatus";
import SuccessModal from "./SuccessModal";

const AgentInvite = () => {
  const [branchList, setBranchList] = useState([]);
  const [modalValue, setModalValue] = useState<boolean | null>(null);
  useEffect(() => {
    (async () => {
      const data = await getBranchList();
      //@ts-ignore
      setBranchList(data);
    })();
  }, []);
  const onSubmit = async (values: any) => {
    const res = await transformAndInviteAgent(values);
    if (res) {
      setModalValue(true);
    }
  };
  return (
    <FormWrapper>
      <H2 text="Invite a new agent to RAH" className="spaceBottom-24" />
      <Formik
        onSubmit={onSubmit}
        validationSchema={validationSchema}
        initialValues={AGENT_INITIAL_VALUES}
      >
        {(props) => {
          const formik = props;
          const { values, setTouched, touched } = formik;

          const onImDone = () => {
            navigate("/agent/agent-list");
          };
          const inviteAnother = () => {
            formik.resetForm();
            setModalValue(null);
          };
          const setFieldsTouched = (field: string) => {
            const fieldIndex = Object.keys(values).indexOf(field);
            const touchedObject: any = { ...touched };
            Object.keys(values).forEach((key: string, index: number) => {
              if (index <= fieldIndex) {
                touchedObject[key] = true;
              }
            });
            setTouched(touchedObject);
          };
          return (
            <>
              <Flex direction={"column"} top={20}>
                <AgentContactInfoSection
                  onChange={(e: any) => {
                    setFieldsTouched(e.target.id);
                    formik.handleChange(e);
                  }}
                  formik={{ ...formik }}
                />
              </Flex>
              <Flex direction={"column"}>
                <AgentStatusSection
                  value={values.agent_status}
                  onChange={formik.handleChange}
                  id={"agent_status"}
                  name={"agent_status"}
                />
              </Flex>
              <Flex direction={"column"} top={20}>
                <AgentParkedStatusSection
                  value={values.agent_parked_status}
                  onChange={(e, value) => {
                    setFieldsTouched("agent_parked_status");
                    formik.handleChange(e);
                  }}
                  id={"agent_parked_status"}
                  name={"agent_parked_status"}
                />
              </Flex>
              <Flex direction={"column"} top={20}>
                <AgentRECOStatus
                  value={values.is_agnet_submited_application_with_reco}
                  onChange={(e, value) => {
                    setFieldsTouched(
                      "is_agnet_submited_application_with_reco"
                    );
                    formik.handleChange(e);
                  }}
                  id={"is_agnet_submited_application_with_reco"}
                  name={"is_agnet_submited_application_with_reco"}
                />
              </Flex>
              <Flex direction={"column"} top={20}>
                <AgentJoiningSpecialPackageStatusSection
                  value={values.agent_special_joining_status}
                  onChange={(e, value) => {
                    setFieldsTouched("agent_special_joining_status");
                    formik.handleChange(e);
                  }}
                  id={"agent_special_joining_status"}
                  name={"agent_special_joining_status"}
                />
              </Flex>
              <Flex direction={"column"} top={20}>
                {values.agent_special_joining_status === 1 && (
                  <AdditionalSJPSection
                    value={values.addtional_sjp}
                    onChange={(e, value) => {
                      setFieldsTouched("addtional_sjp");
                      formik.handleChange(e);
                    }}
                    id={"addtional_sjp"}
                    name={"addtional_sjp"}
                    formik={{ ...formik }}
                  />
                )}
              </Flex>
              <Flex direction={"column"} top={20}>
                <AgentAdditionalInfo
                  value={values.branch_id}
                  onChange={(e, value) => {
                    setFieldsTouched("branch_id");
                    formik.handleChange(e);
                  }}
                  id={"branch_id"}
                  name={"branch_id"}
                  branchList={branchList}
                  error={formik.errors.branch_id}
                />
              </Flex>
              <FlexBox className="footerbtn">
                <Primary
                  onClick={formik.handleSubmit}
                  text="Send invitation"
                  className="submitbtn"
                  style={{ marginRight: "2px" }}
                />

                <Transparent
                  onClick={() => {
                    navigate("/agent/agent-list");
                  }}
                  text="Cancel"
                />
              </FlexBox>
              {modalValue && (
                <SuccessModal
                  onImDone={onImDone}
                  inviteAnother={inviteAnother}
                  modalValue={modalValue}
                  setModalValue={setModalValue}
                />
              )}
            </>
          );
        }}
      </Formik>
    </FormWrapper>
  );
};

export default AgentInvite;
