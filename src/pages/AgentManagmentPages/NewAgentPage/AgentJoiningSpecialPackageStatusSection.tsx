import { FormLableBoldTypography, FormLableTypography } from "@components/Typography/Typography";
import { Col, Radio, Row } from "antd";
import React from "react";
import { SectionProps } from "./helper";

function AgentJoiningSpecialPackageStatusSection(props: SectionProps) {
  const { onChange, value, id, name } = props;

  return (
    <>
      <FormLableBoldTypography className="spaceBottom-16">
        Additional information
      </FormLableBoldTypography>

      <Row>
        <Col>
          <FormLableTypography>
            Is the agent eligible for “Special Joining Package”?
            <span>*</span>
          </FormLableTypography>
          <Radio.Group
            value={value}
            name={name}
            id={id}
            onChange={(e: any) => {
              onChange(e, e.target.value);
            }}
          >
            <Radio value={0}>No</Radio>
            <Radio value={1}>Yes</Radio>
          </Radio.Group>
        </Col>
      </Row>
    </>
  );
}

export default AgentJoiningSpecialPackageStatusSection;
