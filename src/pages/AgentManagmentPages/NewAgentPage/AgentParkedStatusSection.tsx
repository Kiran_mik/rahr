import { FormLableBoldTypography } from "@components/Typography/Typography";
import { Radio, Row } from "antd";
import React from "react";
import { SectionProps } from "./helper";

function AgentParkedStatusSection(props: SectionProps) {
  const { onChange, value, id, name } = props;

  return (
    <>
      <FormLableBoldTypography className="spaceBottom-16">
        Does the agent want to join as a parked agent?  <span>*</span>
      </FormLableBoldTypography>
      <Row>
        <Radio.Group
          value={value}
          name={name}
          id={id}
          onChange={(e: any) => {
            onChange(e, e.target.value);
          }}
        >
          <Radio value={0}>No</Radio>
          <Radio value={1}>Yes</Radio>
        </Radio.Group>
      </Row>
    </>
  );
}

export default AgentParkedStatusSection;
