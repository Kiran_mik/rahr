import { FormLableBoldTypography } from "@components/Typography/Typography";
import { Radio, Row } from "antd";
import React from "react";
import { SectionProps } from "./helper";

function AgentRECOStatus(props: SectionProps) {
  const { onChange, value, id, name } = props;

  return (
    <>
      <FormLableBoldTypography className="spaceBottom-16">
        Has the agent submitted their Application to RECO? <span>*</span>
      </FormLableBoldTypography>
      <Row>
        <Radio.Group
          value={value}
          name={name}
          id={id}
          onChange={(e: any) => {
            onChange(e, e.target.value);
          }}
        >
          <Radio value={1}>Yes</Radio>

          <Radio value={0}>No</Radio>
        </Radio.Group>
      </Row>
    </>
  );
}

export default AgentRECOStatus;
