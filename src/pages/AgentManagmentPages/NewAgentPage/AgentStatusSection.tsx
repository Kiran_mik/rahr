import React from "react";

import { Row, Radio } from "antd";
import { FormLableBoldTypography } from "@components/Typography/Typography";
import { SectionProps } from "./helper";

function AgentStatusSection(props: SectionProps) {
  const { onChange, value, id, name } = props;
  return (
    <>
      <FormLableBoldTypography className="spaceBottom-16">
        Agent Status
      </FormLableBoldTypography>
      <Row className="squareRadioBtn">
        <Radio.Group
          value={value}
          name={name}
          id={id}
          onChange={(e: any) => {
            onChange(e, e.target.value);
          }}
        >
          <Radio value={1}>New registrant</Radio>
          <Radio value={2}>Transfer</Radio>
          <Radio value={3}>Reinstated with RECO</Radio>
        </Radio.Group>
      </Row>
    </>
  );
}

export default AgentStatusSection;
