import React from "react";
import modalimg from "@assets/new_agent.png";
import ModalComponent from "@components/Modal/Modal";
import { H1, H4 } from "@components/Typography/Typography";
import { Flex } from "@components/Flex";
import { DrawerPrimaryBtn } from "@components/Buttons";

interface Props {
  setModalValue: (value: any) => void;
  modalValue: any;
  inviteAnother: () => void;
  onImDone: () => void;
}
function SuccessModal(props: Props) {
  const { setModalValue, modalValue, inviteAnother, onImDone } = props;
  return (
    <ModalComponent
      visible={modalValue}
      onOK={() => setModalValue(null)}
      onCancel={() => () => setModalValue(null)}
      //@ts-ignore
      modalimage={modalimg}
      title=""
      content=""
      modalWidth={700}
    >
      <div style={{ textAlign: "center", padding: "24px" }}>
        <H1
          style={{ fontWeight: "bold" }}
          text={`New agent onboarding request 
  is on the way!`}
        />
        <H4
          style={{ fontWeight: 400 }}
          text={`We have sent onboarding request to agent email`}
        />
        <img
          src={modalimg}
          alt="backgroundimg"
          style={{ marginBottom: "40px" }}
        />
      </div>
      <Flex justifyContent="center">
        <DrawerPrimaryBtn text="Invite another agent" onClick={inviteAnother} />

        <div style={{ marginLeft: 10 }}>
          <DrawerPrimaryBtn text="I'm done" onClick={onImDone} />
        </div>
      </Flex>
    </ModalComponent>
  );
}

export default SuccessModal;
