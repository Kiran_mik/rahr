import { message } from "antd"

import services from "@services/index"

import { phoneRegExp } from "@constants/index"

import * as Yup from "yup"
import { FormikValues } from "formik"

export const transformAndInviteAgent = async (values: any) => {
	try {
		const payload = {
			agent_status: values.agent_status,
			first_name: values.first_name,
			last_name: values.last_name,
			email: values.email,
			cell_phone_number: values.phone_number,
			is_agent_want_join_as_parked_agent: values.agent_parked_status,
			is_agent_joined_as_SJP: values.agent_special_joining_status,
			additional_special_package_info: values.addtional_sjp,
			is_agent_submitted_application_with_reco: values.is_agnet_submited_application_with_reco,
			branch_id: values.branch_id,
		}
		const res = await inviteAgent(payload)
		return res
	} catch (err) {
		return false
	}
}

export const inviteAgent = async (data: any) => {
	try {
		const res = await services.post("v1/agents", data)
		//@ts-ignore
		return res.status === 200
	} catch (err) {
		//@ts-ignore
		if (err.error.error) {
			//@ts-ignore

			const values = Object.values(err.error.error)
			if (values[0]) {
				//@ts-ignore
				message.error(values[0])
			}
		}
		return false
	}
}

export const getBranchList = async (basePath?:string) => {
	try {
		const res = await services.getTest("v1/staffs/support-data",basePath)
		//@ts-ignore
		return res.data.data.branch_list
	} catch (err) {}
}

export const validationSchema = Yup.object({
	agent_status: Yup.number().required("Field is required"),
	first_name: Yup.string()
		.required("Please enter your first name")
		.max(30, ""),
	last_name: Yup.string().required("Please enter your last name"),
	email: Yup.string()
		.email("Please enter a valid email")
		.nullable()
		.required("Please enter an email address"),
	phone_number: Yup.string()
		.required("Phone number cannot be empty")
		.matches(phoneRegExp, "Phone number is not valid")
		.min(10, "Phone number should be of 10 digits")
		.max(10, "Please enter a valid phone number"),
	agent_parked_status: Yup.number().required("Field is required"),
	agent_special_joining_status: Yup.boolean().required("Field is required"),
	
	addtional_sjp: Yup
    .string()
    .nullable()
    .when("agent_special_joining_status", {
      is: (agent_special_joining_status: boolean) => agent_special_joining_status,
      then: Yup.string().nullable().required("Please enter addition SJP information"),
      otherwise: Yup.string().nullable(),
    }),
	is_agnet_submited_application_with_reco: Yup.number().required("Field is required"),
	branch_id: Yup.number().required("Please select branch"),
})

export const AGENT_INITIAL_VALUES = {
	agent_status: 1,
	agent_parked_status: 0,
	agent_special_joining_status: 0,
	is_agnet_submited_application_with_reco: 1,
	email: "",
	first_name: "",
	last_name: "",
	phone_number: "",
	branch_id: "",
	addtional_sjp: "",
}

export interface SectionProps {
	onChange: (e: any, value: any) => void
	id: string
	name: string
	value: any,
	formik?: FormikValues
}
