import React from "react";

import { H3 } from "@components/Typography/Typography";
import DashboardLayout from "@pages/UserManagementPages/StaffMembers/DashboardLayout";
import StaffHeader from "@components/StaffHeader";
import { UserBreadWrap } from "@components/CommonLayouts/style";
import AgentInvite from "./AgentInvite";

function NewAgentPage(props: { path: string }) {
  return (
    <DashboardLayout>
      <StaffHeader>
        <UserBreadWrap>
          <H3 text="Add new agent" className="font-500" />
        </UserBreadWrap>
      </StaffHeader>
      <AgentInvite />
    </DashboardLayout>
  );
}

export default NewAgentPage;
