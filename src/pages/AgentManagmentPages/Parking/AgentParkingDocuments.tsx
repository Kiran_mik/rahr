import React, { FC } from 'react';
import { Flex } from '@components/Flex';
import { H5 } from '@components/Typography/Typography';
import { Row } from 'antd';
import { FormLableTypography } from '@components/Typography/Typography';
import { FileUploadBox } from '@components/FileUploader';
import { FormikHandlers, FormikValues } from 'formik';

const AgentParkingDocuments: FC<FormikHandlers & FormikValues> = ({ values, setFieldValue, errors }) => {

    return (
        <Flex top={25} direction="column">
            <Row>
                <H5 text="Agent parking documents" />
            </Row>
            <Flex top={10} direction="column">
                <FormLableTypography>Parking agreement</FormLableTypography>

                <FileUploadBox
                    name="parking_agreement"
                    id="parking_agreement"
                    error={errors['parking_agreement']}
                    value={values.parking_agreement}
                    onChange={(value: any) => setFieldValue('parking_agreement', value)}
                    templateUrl={'https://google.com'}
                />
            </Flex>
        </Flex>
    )
}

export default AgentParkingDocuments;