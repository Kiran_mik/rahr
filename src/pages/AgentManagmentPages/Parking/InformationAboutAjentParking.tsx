import React, { FC, useState } from 'react';
import { Flex } from '@components/Flex';
import { H5, LabelTypography } from '@components/Typography/Typography';
import { Row, Select } from 'antd';
import { FormLableTypography } from '@components/Typography/Typography';
import SelectInput from '@components/Select';
import { Col } from 'antd';
import TextAreaInput from '@components/Input/TextArea';
import { FormikHandlers, FormikValues } from 'formik';

const InformationAboutAgentParking: FC<FormikHandlers & FormikValues> = ({ values, errors, setFieldValue }) => {



    return (
        <Flex top={25} direction="column">
            <Row>
                <H5 text="Information about agent parking" />
            </Row>
            <Flex top={10} direction="column">
                <LabelTypography>Number of transaction (Previous 12 months)</LabelTypography>
                <LabelTypography>25</LabelTypography>
            </Flex>
            <Flex top={10} direction="column">
                <Row gutter={16}>
                    <Col className='gutter-row' span={24}>
                        <FormLableTypography>
                            Comments/Reason
                        </FormLableTypography>

                        <TextAreaInput
                            name="comments_reason"
                            id="comments_reason"
                            placeholder=""
                            onChange={(e: any) => {
                                setFieldValue('comments_reason', e.target.value)
                            }}
                            value={values.comments_reason}
                            error={errors['comments_reason']}
                        />
                    </Col>
                </Row>
            </Flex>
        </Flex>
    )
}

export default InformationAboutAgentParking;