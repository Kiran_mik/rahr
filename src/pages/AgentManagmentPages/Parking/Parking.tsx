import React, { FC } from 'react';
import { TabItemLayout } from '../AgentReviewPage/style';
import { Flex } from '@components/Flex';
import { H2 } from '@components/Typography/Typography';
import TerminationForms from '../Termination/TerminationForms';
import BoardTerminationDate from '../Termination/BoardsTerminationDate';
import ParkingChecklist from './ParkingChecklist';
import InformationAboutAgentParking from './InformationAboutAjentParking';
import AgentParkingDocuments from './AgentParkingDocuments'
import { Formik, Form } from 'formik';
import { submitParking, parkingValidationSchema, initialValues } from './helper'

const Parking: FC = (props: any) => {

    const onSubmitForm = (values: any) => {
        submitParking({ agent_id: props.agentId, ...values })
    }
    return (
        <TabItemLayout>
            <Flex>
                <Flex>
                    <H2 text="License termination" className="font-600" />
                </Flex>

            </Flex>
            <Formik
                enableReinitialize={true}
                initialValues={initialValues}
                validationSchema={parkingValidationSchema}
                onSubmit={(values, actions) => {
                    onSubmitForm(values);
                }}
            >
                {(props) => {
                    return (
                        <Form>
                            <ParkingChecklist {...props} />
                            <InformationAboutAgentParking {...props} />
                            <AgentParkingDocuments {...props} />
                            <TerminationForms {...props} />
                            <BoardTerminationDate {...props} />
                            <button type="submit">Submit parking</button>
                        </Form>)
                }}
            </Formik>
        </TabItemLayout>
    )
}

export default Parking;