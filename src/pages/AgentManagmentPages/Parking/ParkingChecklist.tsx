import React, { FC } from 'react';
import { FormikHandlers, FormikValues } from 'formik';
import { Flex } from '@components/Flex';
import { H5 } from '@components/Typography/Typography';
import { Checkbox } from '@components/Checkbox';
import { checklist } from './helper';


const ParkingChecklist: FC<FormikHandlers & FormikValues> = ({ values, setFieldValue }) => {

    return (
        <Flex top={20} flex={1} direction="column">
            <H5 text="Parking checklist" />

            {checklist.map((item: any) => (
                <Flex top={5} key={item.id}>
                    <Checkbox
                        name={item.label}
                        id={item.id}
                        checked={values[item.id]}
                        onChange={(e: any) => setFieldValue(item.id, e.target.checked)}
                    />
                </Flex>
            ))}


        </Flex>
    )
}

export default ParkingChecklist;