import React from "react";
import * as Yup from "yup";
import services from "@services/index";


export const checklist = [
    {
        id: "is_agent_removed_from_regional_board",
        label: "Has agent been removed from regional boards?"
    },
    {
        id: "is_agent_removed_from_showing_time",
        label: "Has agent been removed from ShowingTime?"
    },
]
export const initialValues = {
    is_approved_agent_parked_this_year_again: 0,
    reason_of_approval: "",
    is_agent_removed_from_regional_board: 0,
    is_agent_removed_from_showing_time: 0,
    comments_reason: "",
    parking_agreement: null,
    trreb_form: null,
    mreb_form: null,
    trreb_termination_date: new Date(),
    mreb_termination_date: new Date()
}


export const parkingValidationSchema = Yup.object({
    parking_agreement: Yup.string().nullable().required("Please upload Parking agreement"),
    trreb_form: Yup.string().nullable().required("Please upload TRREB form"),
    mreb_form: Yup.string().nullable().required("Please upload MREB form"),
    trreb_termination_date: Yup.date().nullable().required("Please select TRREB termination date "),
    mreb_termination_date: Yup.date().nullable().required("Please select MREB termination date"),
});

export const submitParking = async (values: any) => {
    console.log("submitParking", values);
    try {
        const url = "v1/agents/park";
        const res = await services.post(url, { ...values });
        //@ts-ignore
        return res;
    } catch (err) {
        return err;
    }
};