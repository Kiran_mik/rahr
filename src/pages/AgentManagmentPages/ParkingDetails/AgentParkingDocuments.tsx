import React, { FC } from 'react';
import { Flex } from '@components/Flex';
import { H5 } from '@components/Typography/Typography';
import { Row } from 'antd';
import { FormLableTypography } from '@components/Typography/Typography';
import { FileUploadBox } from '@components/FileUploader';
import OnboardingFormButton from '@components/OnboardingFormButton';
import DocumentImg from "@assets/icon-form.svg";


type AgentParkingDocumentsProps = {
    test?: Text;
    parkingDetails?: any
}

const AgentParkingDocuments: FC<AgentParkingDocumentsProps> = ({ test, parkingDetails }) => {
    const { parking_agreement } = parkingDetails
    const formDetail = { id: "", form_name: "", form_link: parking_agreement };
    return (
        <Flex top={25} direction="column">
            <Row>
                <H5 text="Agent parking documents" />
            </Row>
            <Flex top={10} direction="column">
                <FormLableTypography>Parking agreement</FormLableTypography>
                {parking_agreement
                    ? <OnboardingFormButton
                        buttonText="View Form"
                        image={DocumentImg}
                        text="Parking agreeement.pdf"
                        formDetails={formDetail}
                    />
                    : "-"}
            </Flex>
        </Flex>
    )
}

export default AgentParkingDocuments;


