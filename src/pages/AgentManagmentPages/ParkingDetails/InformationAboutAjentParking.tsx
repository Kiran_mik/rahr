import React, { FC, useState } from 'react';
import { Flex } from '@components/Flex';
import { H5, LabelTypography } from '@components/Typography/Typography';
import { Row, Select } from 'antd';
import { FormLableTypography } from '@components/Typography/Typography';
import SelectInput from '@components/Select';
import { Col } from 'antd';
import TextAreaInput from '@components/Input/TextArea';
import { dateFormat } from '@utils/format';

type InformationAboutAgentParkingProps = {
    test?: Text;
    parkingDetails?: any
}

const InformationAboutAgentParking: FC<InformationAboutAgentParkingProps> = ({ test, parkingDetails }) => {
    const { start_date, comments_reason } = parkingDetails
    return (
        <Flex top={25} direction="column">
            <Row>
                <H5 text="Information about agent parking" />
            </Row>
            <Flex top={10} direction="column">
                <Row>
                    <Col className="gutter-row" span={8}>
                        <FormLableTypography>Start date</FormLableTypography>
                        <LabelTypography>{(start_date && dateFormat(start_date, 'DD MMMM YYYY')) || '-'}</LabelTypography>
                    </Col>

                    <Col className="gutter-row" span={8}>
                        <FormLableTypography>Parking date</FormLableTypography>
                        <LabelTypography>{'-'}</LabelTypography>
                    </Col>
                </Row>
            </Flex>
            <Flex top={10} direction="column">
                <Row>
                    <Col className="gutter-row" span={12}>
                        <FormLableTypography>Comments</FormLableTypography>
                        <LabelTypography>{comments_reason || ''}</LabelTypography>
                    </Col>
                </Row>
            </Flex>
        </Flex>
    )
}

export default InformationAboutAgentParking;