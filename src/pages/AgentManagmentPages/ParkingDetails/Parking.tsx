import React, { FC, useEffect, useState } from 'react';
import { TabItemLayout } from '../AgentReviewPage/style';
import { Flex } from '@components/Flex';
import { H2 } from '@components/Typography/Typography';
import TerminationForms from '../TerminationDetails/TerminationForms';
import BoardTerminationDate from '../TerminationDetails/BoardsTerminationDate';
import InformationAboutAgentParking from './InformationAboutAjentParking';
import AgentParkingDocuments from './AgentParkingDocuments'
import { useParams } from "@reach/router";
import { agentParkingDetails } from './helper';

type ParkingProps = {
    test: Text;
}

const Parking: FC<ParkingProps> = ({ test }) => {
    const params = useParams();
    const [parkingDetails, setParkingDetails] = useState({
        MREB_termination_date: '',
        TRREB_termination_date: '',
        MREB_form: '',
        TRREB_form: ''
    })
    useEffect(() => {
        const getData = async () => {
            const res = await agentParkingDetails(params.agentId)
            setParkingDetails(res)
        }
        getData()
    }, [])
    console.log(parkingDetails);

    return (
        <TabItemLayout>
            <Flex>
                <Flex>
                    <H2 text="Agent profile information" className="font-600" />
                </Flex>

            </Flex>
            <InformationAboutAgentParking parkingDetails={parkingDetails} />
            <AgentParkingDocuments parkingDetails={parkingDetails}/>
            <TerminationForms heading='Parking forms' formsList={{ mreb_form: parkingDetails.MREB_form, trreb_form: parkingDetails.TRREB_form }} />
            <BoardTerminationDate boardinfo={{ trreb_termination_date: parkingDetails.TRREB_termination_date, mreb_termination_date: parkingDetails.MREB_termination_date }} />
        </TabItemLayout>
    )
}

export default Parking;