import React, { FC } from 'react';
import { Flex } from '@components/Flex';
import { H5 } from '@components/Typography/Typography';
import { Checkbox } from '@components/Checkbox';

type ParkingProps = {
    test?: Text;
}

const checklist = [
    {
        id: 1,
        label: "Has agent been removed from regional boards?"
    },
    {
        id: 2,
        label: "Has agent been removed from ShowingTime?"
    },
]
const ParkingChecklist: FC<ParkingProps> = ({ test }) => {

    return (
        <Flex top={20} flex={1} direction="column">
            <H5 text="Parking checklist" />

            {checklist.map((item: any) => (
                <Flex top={5} key={item.id}>
                    <Checkbox
                        name={item.label}
                        id={item.id}
                    />
                </Flex>
            ))}


        </Flex>
    )
}

export default ParkingChecklist;