import services from "@services/index";
import { BASE_URL_MAP } from "@constants/index";


export const agentParkingDetails = async (agentId: any) => {
  try {
    const res = await services.get(
      `v1/agents/park/` + agentId,
      process.env.REACT_APP_BASE_URL_AGENT
    );
    //@ts-ignore
    return res.data.data;
  } catch (err) {
    return [];
  }
};
