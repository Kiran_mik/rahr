import React, { FC, useState, useEffect } from 'react';
import { Flex } from '@components/Flex';
import { H5 } from '@components/Typography/Typography';
import { Row, Radio } from 'antd';
import { SquareRadioWrapper } from '@pages/AgentOnboardingPages/style';
import { FormikHandlers, FormikValues } from 'formik';




const AgentCurrentStatus: FC<FormikValues & FormikHandlers> = ({ values, setFieldValue }) => {


    return (
        <Flex top={20} direction="column">
            <Row>
                <H5 text="Agent's Current Status with RECO as a RAHR Agent" />
            </Row>
            <SquareRadioWrapper>
                <Row className="squareRadioBtn">
                    <Radio.Group
                        value={values.current_status_with_reco}
                        onChange={(e) => {
                            setFieldValue('current_status_with_reco', e.target.value)
                        }}
                    >
                        <Radio value={1}>Active with RAHR</Radio>
                        <Radio value={2}>Terminated and is no longer registered with RAHR</Radio>
                    </Radio.Group>
                </Row>
            </SquareRadioWrapper>
        </Flex>
    );
}

export default React.memo(AgentCurrentStatus);