import React, { FC } from 'react';
import { Flex } from '@components/Flex';
import { H5 } from '@components/Typography/Typography';
import { Row } from 'antd';
import { FormLableTypography } from '@components/Typography/Typography';
import { FileUploadBox } from '@components/FileUploader';
import { FormikHandlers, FormikValues } from 'formik';

type AgentTerminationDocumentsProps = {
    test?: Text;
}

const AgentTerminationDocuments: FC<FormikHandlers & FormikValues> = ({ values, setFieldValue }) => {

    return (
        <Flex top={25} direction="column">
            <Row>
                <H5 text="Agent termination documents" />
            </Row>
            <Flex top={10} direction="column">
                <FormLableTypography>Agent resignation letter</FormLableTypography>

                <FileUploadBox
                    name="agent_resignation_letter"
                    id="agent_resignation_letter"
                    onChange={(value: any) => setFieldValue("agent_resignation_letter", value)}
                    error={false}
                    value={values.agent_resignation_letter}
                    templateUrl={'https://google.com'}
                />
            </Flex>

            <Flex top={10} direction="column">
                <FormLableTypography>Termination exit survey</FormLableTypography>
                <FileUploadBox
                    name="termination_exit_survey"
                    id="termination_exit_survey"
                    onChange={(value: any) => setFieldValue("termination_exit_survey", value)}
                    error={false}
                    value={''}
                />
            </Flex>
        </Flex>
    )
}

export default AgentTerminationDocuments;