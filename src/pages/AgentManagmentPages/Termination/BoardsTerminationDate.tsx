import React, { FC } from 'react';
import { Flex } from '@components/Flex';
import { H5, FormLableTypography } from '@components/Typography/Typography';
import { Row, Col } from 'antd';
import DatePickerComponent from '@components/DatePicker';
import { FormikValues, FormikHandlers } from 'formik';

type BoardTerminationDateProps = {
    test?: Text;
}

const BoardTerminationDate: FC<FormikValues & FormikHandlers> = ({ values, setFieldValue, errors }) => {

    return (
        <Flex top={25} direction="column">
            <Row>
                <H5 text="Boards termination date" />
            </Row>
            <Flex top={10} direction="column">
                <Row gutter={16}>
                    <Col className="gutter-row" span={8}>
                        <FormLableTypography>TRREB termination date <span>*</span></FormLableTypography>
                        <DatePickerComponent
                            name="trreb_termination_date"
                            id="trreb_termination_date"
                            error={errors['trreb_termination_date']}
                            onChange={(value: any) => {
                                setFieldValue("trreb_termination_date", value)
                            }}
                            value={values.trreb_termination_date}
                        />
                    </Col>

                    <Col className="gutter-row" span={8}>
                        <FormLableTypography>MREB termination date <span>*</span></FormLableTypography>
                        <DatePickerComponent
                            name="mreb_termination_date"
                            id="mreb_termination_date"
                            error={errors['mreb_termination_date']}
                            onChange={(value: any) => {
                                setFieldValue("mreb_termination_date", value)
                            }}
                            value={values.mreb_termination_date}
                        />
                    </Col>
                </Row>
            </Flex>
        </Flex>
    )
}

export default BoardTerminationDate;
