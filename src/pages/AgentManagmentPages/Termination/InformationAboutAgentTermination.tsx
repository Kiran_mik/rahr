import React, { FC } from 'react';
import { Flex } from '@components/Flex';
import { H5, LabelTypography } from '@components/Typography/Typography';
import { Row, Select } from 'antd';
import { FormLableTypography } from '@components/Typography/Typography';
import SelectInput from '@components/Select';
import { Col } from 'antd';
import TextAreaInput from '@components/Input/TextArea';
import { FormikHandlers, FormikValues } from 'formik';
import { reasonForLeavingOptions, agentRejoinOptions } from './helper';


const InformationAboutAgentTermination: FC<FormikHandlers & FormikValues> = ({ values, setFieldValue, errors }) => {

    return (
        <Flex top={25} direction="column">
            <Row>
                <H5 text="Information about agent termination" />
            </Row>
            <Flex top={10} direction="column">
                <LabelTypography>Number of transaction (Previous 12 months)</LabelTypography>
                <LabelTypography>{values.number_of_transaction}</LabelTypography>
            </Flex>
            <Flex top={10} direction="column">
                <Row gutter={16}>
                    <Col className="gutter-row" span={8}>
                        <FormLableTypography>
                            Reason for agent leaving RAH
                            <span> *</span>
                        </FormLableTypography>

                        <SelectInput
                            value={values.reason_for_agent_leaving_rahr}
                            placeholdertitle="Transfer to"
                            onChange={(e: any) => {
                                setFieldValue("reason_for_agent_leaving_rahr", e)
                            }}
                            name={"reason_for_agent_leaving_rahr"}
                            id={"reason_for_agent_leaving_rahr"}
                            error={errors['reason_for_agent_leaving_rahr']}
                        >
                            {reasonForLeavingOptions.map((option: any) => {
                                return (
                                    <Select.Option key={option.id} value={option.id}>
                                        {option.name}
                                    </Select.Option>
                                );
                            })}
                        </SelectInput>
                    </Col>
                    <Col className="gutter-row" span={8}>
                        <FormLableTypography>
                            Which Brokerage is the agent transfer....?
                            <span> *</span>
                        </FormLableTypography>

                        <SelectInput
                            value={values.which_brokerage_is_the_agent_transfer}
                            placeholdertitle="Select from a list"
                            onChange={(e: any) => {
                                setFieldValue("which_brokerage_is_the_agent_transfer", e)
                            }}
                            name={"which_brokerage_is_the_agent_transfer"}
                            id={"which_brokerage_is_the_agent_transfer"}
                            error={errors['which_brokerage_is_the_agent_transfer']}
                        >
                            {reasonForLeavingOptions.map((option: any) => {
                                return (
                                    <Select.Option key={option.id} value={option.id}>
                                        {option.name}
                                    </Select.Option>
                                );
                            })}
                        </SelectInput>
                    </Col>
                    <Col className="gutter-row" span={8}>
                        <FormLableTypography>
                            Would agent consider rejoin RAHR?
                            <span> *</span>
                        </FormLableTypography>

                        <SelectInput
                            value={values.would_agent_consider_rejoin_rahr}
                            placeholdertitle="Select from a list"
                            onChange={(e: any) => {
                                setFieldValue("would_agent_consider_rejoin_rahr", e)
                            }}
                            name={"would_agent_consider_rejoin_rahr"}
                            id={"would_agent_consider_rejoin_rahr"}
                            error={errors['would_agent_consider_rejoin_rahr']}
                        >
                            {agentRejoinOptions.map((option: any) => {
                                return (
                                    <Select.Option key={option.id} value={option.id}>
                                        {option.name}
                                    </Select.Option>
                                );
                            })}
                        </SelectInput>
                    </Col>
                    <Col className='gutter-row' span={24}>
                        <FormLableTypography>
                            Additional comments
                        </FormLableTypography>

                        <TextAreaInput
                            name="additional_comments"
                            id="additional_comments"
                            placeholder=""
                            onChange={(e: any) => {
                                setFieldValue("additional_comments", e.target.value)
                            }}
                            value={values.additional_comments}
                            error={false}
                        />
                    </Col>
                </Row>
            </Flex>
        </Flex>
    )
}

export default InformationAboutAgentTermination;