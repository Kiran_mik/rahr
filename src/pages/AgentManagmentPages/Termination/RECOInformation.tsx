import React, { FC } from 'react';
import { Flex } from '@components/Flex';
import { H5, LabelTypography, FormLableTypography } from '@components/Typography/Typography';
import { Row, Col } from 'antd';
import DatePickerComponent from '@components/DatePicker';
import { FormikHandlers, FormikValues } from 'formik';
import { dateFormat } from '@utils/format';


const RECOInformation: FC<FormikHandlers & FormikValues> = ({ values, setFieldValue, errors }) => {

    return (
        <Flex top={25} direction="column">
            <Row>
                <H5 text="RECO information" />
            </Row>
            <Flex top={10} direction="column">
                <Row>
                    <Col className="gutter-row" span={8}>
                        <FormLableTypography>Agent’s RECO number</FormLableTypography>
                        <LabelTypography>{values.agent_reco_number}</LabelTypography>
                    </Col>

                    <Col className="gutter-row" span={12}>
                        <FormLableTypography>RECO Start date</FormLableTypography>
                        <LabelTypography>{dateFormat(values.reco_start_date, "D MMMM, YYYY")}</LabelTypography>
                    </Col>
                </Row>
            </Flex>
            <Flex top={10} direction="column">
                <Row>
                    <Col className="gutter-row" span={8}>
                        <FormLableTypography>RECO termination date <span>*</span></FormLableTypography>
                        <DatePickerComponent
                            name="reco_termination_date"
                            id="reco_termination_date"
                            error={errors['reco_termination_date']}
                            onChange={(value: any) => {
                                setFieldValue("reco_termination_date", value)
                            }}
                            value={values.reco_termination_date}
                        />
                    </Col>
                </Row>
            </Flex>
        </Flex>
    )
}

export default RECOInformation;