import React, { FC } from 'react';
import { TabItemLayout } from '../AgentReviewPage/style';
import { Flex } from '@components/Flex';
import { H2 } from '@components/Typography/Typography';
import AgentCurrentStatus from './AgentCurrentStatus';
import TerminationChecklist from './TerminationChecklist';
import InformationAboutAgentTermination from './InformationAboutAgentTermination';
import AgentTerminationDocuments from './AgentTerminationDocuments';
import TerminationForms from './TerminationForms';
import RECOInformation from './RECOInformation';
import BoardTerminationDate from './BoardsTerminationDate';
import { Formik, Form } from 'formik';
import { initialValues, terminarionValidationSchema, seubmitTermination } from './helper'




const Termination: FC = (props: any) => {
    const onSubmitForm = (values: any) => {
        seubmitTermination({ agent_id: props.agentId, ...values })
    }

    return (
        <TabItemLayout>
            <Flex>
                <Flex>
                    <H2 text="License termination" className="font-600" />
                </Flex>

            </Flex>
            <Formik
                enableReinitialize={true}
                initialValues={initialValues}
                validationSchema={terminarionValidationSchema}
                onSubmit={(values, actions) => {
                    onSubmitForm(values);
                }}
            >
                {(props) => {
                    return (
                        <Form>
                            <AgentCurrentStatus {...props} />
                            <TerminationChecklist {...props} />
                            <InformationAboutAgentTermination {...props} />
                            <AgentTerminationDocuments {...props} />
                            <TerminationForms {...props} />
                            <RECOInformation {...props} />
                            <BoardTerminationDate {...props} />
                            <button type="submit">Submit termination</button>
                        </Form>
                    )
                }}
            </Formik>
        </TabItemLayout >
    )
}

export default Termination;