import React, { FC } from 'react';
import { Flex } from '@components/Flex';
import { H5 } from '@components/Typography/Typography';
import { Checkbox } from '@components/Checkbox';
import { FormikHandlers, FormikValues } from 'formik';


const checklist = [
    {
        id: "did_agent_return_all_items",
        label: "Did Agent return all items that was borrowed/belongs to RAHR?"
    },
    {
        id: "has_agent_been_removed_from_reco_website",
        label: "Has agent been removed from RECO website?"
    },
    {
        id: "has_agent_been_removed_from_regional_board",
        label: "Has Agent been removed from the regional Board(s)?"
    },
    {
        id: "has_agent_been_terminated_from_brokerwolf",
        label: "Has Agent been terminated from Brokerwolf?"
    },
    {
        id: "has_agent_been_terminated_from_wolf_connect",
        label: "Has Agent been terminated from WolfConnect?"
    },
    {
        id: "has_agent_access_been_removed_from_showing_time",
        label: "Has Agent's access been removed from ShowingTime?"
    },
    {
        id: "has_agent_security_card_been_deactivated",
        label: "Has Agent's Security Card been deactivated?"
    },
    {
        id: "has_agent_been_removed_from_mail_chimp",
        label: "Has Agent been removed from MailChimp?"
    },
    {
        id: "has_agent_been_removed_from_the_all_RAHRs_website",
        label: "Has Agent been removed from the RAHR's Website/all RAHR's Social Media Outlets?"
    },
    {
        id: "has_agents_mail_folder_been_removed",
        label: "Has Agent's mail folder been removed?"
    },
]
const TerminationChecklist: FC<FormikHandlers & FormikValues> = ({ values, setFieldValue }) => {

    return (
        <Flex top={20} flex={1} direction="column">
            <H5 text="Termination checklist" />

            {checklist.map((item: any) => (
                <Flex top={5} key={item.id}>
                    <Checkbox
                        name={item.label}
                        id={item.id}
                        checked={values[item.id]}
                        onChange={(e: any) => setFieldValue(item.id, e.target.checked)}
                    />
                </Flex>
            ))}


        </Flex>
    )
}

export default TerminationChecklist;