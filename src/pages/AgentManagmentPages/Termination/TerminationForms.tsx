import React, { FC } from 'react';
import { Flex } from '@components/Flex';
import { H5, LabelTypography } from '@components/Typography/Typography';
import { Row } from 'antd';
import { FormLableTypography } from '@components/Typography/Typography';
import { FileInput } from "@components/FileInput";
import { FileUploadBox } from '@components/FileUploader';
import { FormikHandlers, FormikValues } from 'formik';


const TerminationForms: FC<FormikHandlers & FormikValues> = ({ values, setFieldValue, errors }) => {

    return (
        <Flex top={25} direction="column">
            <Row>
                <H5 text="Termination forms" />
            </Row>
            <Flex top={10} direction="column">
                <FormLableTypography>TRREB <span> *</span></FormLableTypography>

                <FileUploadBox
                    name="trreb_form"
                    id="trreb_form"
                    error={errors['trreb_form']}
                    onChange={(value: any) => setFieldValue("trreb_form", value)}
                    value={values.trreb_form}
                />
            </Flex>

            <Flex top={10} direction="column">
                <FormLableTypography>MREB <span>*</span></FormLableTypography>
                <FileUploadBox
                    name="mreb_form"
                    id="mreb_form"
                    error={errors['mreb_form']}
                    onChange={(value: any) => setFieldValue("mreb_form", value)}
                    value={values.mreb_form}
                />
            </Flex>
        </Flex>
    )
}

export default TerminationForms;