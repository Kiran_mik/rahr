import { UserBreadWrap } from "@components/CommonLayouts/style";
import { Flex } from "@components/Flex";
import StaffHeader from "@components/StaffHeader";
import { H3 } from "@components/Typography/Typography";
import DashboardLayout from "@pages/UserManagementPages/StaffMembers/DashboardLayout";
import React from "react";
import AgentInfoHeader from "../AgentReviewPage/AgentInfoHeader";
import AgentReviewTabLayout from "../AgentReviewPage/AgentReviewTabLayout";

function TeminationPage(props: { path?: string }) {
    return (
        <DashboardLayout>
            <StaffHeader>
                <UserBreadWrap>
                    <H3 text="Agent onboarding application" className="font-500" />
                </UserBreadWrap>
            </StaffHeader>
            <Flex direction="column" top={10}>
                <AgentInfoHeader />
            </Flex>
            <Flex top={40}>
                <AgentReviewTabLayout />
            </Flex>
        </DashboardLayout>
    );
}

export default TeminationPage;
