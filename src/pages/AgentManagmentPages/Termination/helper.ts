import React from "react";
import * as Yup from "yup";
import services from "@services/index";

export const reasonForLeavingOptions = [
    {
        id: 1,
        name: "Transfer To"
    },
    {
        id: 2,
        name: "Leaving Real Estate"
    },
    {
        id: 3,
        name: "Non-RECO Renewal"
    },
    {
        id: 4,
        name: "Terminated by Manager"
    },
    {
        id: 5,
        name: "Terminated by Board"
    },
    {
        id: 6,
        name: "Others"
    },
    {
        id: 7,
        name: "Unknown"
    },
]

export const agentRejoinOptions = [
    {
        id: 1,
        name: "Yes"
    },
    {
        id: 2,
        name: "No"
    },
    {
        id: 3,
        name: "May be"
    }
]

export const initialValues = {
    current_status_with_reco: 1,
    did_agent_return_all_items: 0,
    has_agent_been_removed_from_reco_website: 0,
    has_agent_been_removed_from_regional_board: 0,
    has_agent_been_terminated_from_brokerwolf: 0,
    has_agent_been_terminated_from_wolf_connect: 0,
    has_agent_access_been_removed_from_showing_time: 0,
    has_agent_security_card_been_deactivated: 0,
    has_agent_been_removed_from_mail_chimp: 0,
    has_agent_been_removed_from_the_all_RAHRs_website: 0,
    has_agents_mail_folder_been_removed: 0,
    number_of_transaction: 25,
    reason_for_agent_leaving_rahr: null,
    which_brokerage_is_the_agent_transfer: null,
    would_agent_consider_rejoin_rahr: null,
    additional_comments: "",
    agent_resignation_letter: "https://image-mgmt-bucket.s3.ca-central-1.amazonaws.com/files/1643100371-new6.jpeg",
    termination_exit_survey: "https://image-mgmt-bucket.s3.ca-central-1.amazonaws.com/files/1643100371-new6.jpeg",
    trreb_form: "https://image-mgmt-bucket.s3.ca-central-1.amazonaws.com/files/1643100371-new6.jpeg",
    mreb_form: "https://image-mgmt-bucket.s3.ca-central-1.amazonaws.com/files/1643100371-new6.jpeg",
    agent_reco_number: 236581,
    reco_start_date: "2022-12-12",
    reco_termination_date: new Date(),
    trreb_termination_date: new Date(),
    mreb_termination_date: new Date()
}

export const parkingInitialValues = {
    is_approved_agent_parked_this_year_again: 0,
    reason_of_approval: "",
    is_agent_removed_from_regional_board: 1,
    is_agent_removed_from_showing_time: 1,
    comments_reason: "",
    parking_agreement: null,
    trreb_form: null,
    mreb_form: null,
    trreb_termination_date: new Date(),
    mreb_termination_date: new Date()
}

export const terminarionValidationSchema = Yup.object({
    reason_for_agent_leaving_rahr: Yup.number().nullable().required("Please select reason for agent leaving RAH "),
    which_brokerage_is_the_agent_transfer: Yup.number().nullable().required("Please select Brokerage"),
    would_agent_consider_rejoin_rahr: Yup.number().nullable().required("Please select option"),
    trreb_form: Yup.string().nullable().required("Please upload TRREB form"),
    mreb_form: Yup.string().nullable().required("Please upload MREB form"),
    reco_termination_date: Yup.date().nullable().required("Please select RECO termination date"),
    trreb_termination_date: Yup.date().nullable().required("Please select TRREB termination date "),
    mreb_termination_date: Yup.date().nullable().required("Please select MREB termination date"),
});


export const parkingValidationSchema = Yup.object({
    trreb_form: Yup.string().nullable().required("Please upload TRREB form"),
    mreb_form: Yup.string().nullable().required("Please upload MREB form"),
    reco_termination_date: Yup.date().nullable().required("Please select RECO termination date"),
    trreb_termination_date: Yup.date().nullable().required("Please select TRREB termination date "),
    mreb_termination_date: Yup.date().nullable().required("Please select MREB termination date"),
});

export const seubmitTermination = async (values: any) => {
    try {
        const url = "v1/agents/onboardings/terminate";
        const res = await services.post(url, { ...values });
        //@ts-ignore
        return res;
    } catch (err) {
        return err;
    }
};