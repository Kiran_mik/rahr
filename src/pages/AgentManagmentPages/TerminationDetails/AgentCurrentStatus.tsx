import React, { FC, useState } from 'react';
import { Flex } from '@components/Flex';
import { H5 } from '@components/Typography/Typography';
import { ContentWrapper } from '../../AgentOnboardingPages/WelcomeScreen/style';
import { Row, Radio } from 'antd';
import { SquareRadioWrapper } from '@pages/AgentOnboardingPages/style';

type StatusProps = {
    test?: Text;
}

const AgentCurrentStatus: FC<StatusProps> = ({ test }) => {

    const [currentStatus, setCurrentStatus] = useState<number>(1);

    return (
        <Flex top={20} direction="column">
            <Row>
                <H5 text="Agent's Current Status with RECO as a RAHR Agent" />
            </Row>
            <SquareRadioWrapper>
                <Row className="squareRadioBtn">
                    <Radio.Group
                        value={currentStatus}
                        onChange={(e) => {
                            setCurrentStatus(e.target.value)
                        }}
                    >
                        <Radio value={1}>Active with RAHR</Radio>
                        <Radio value={2}>Terminated and is no longer registered with RAHR</Radio>
                    </Radio.Group>
                </Row>
            </SquareRadioWrapper>
        </Flex>
    )
}

export default AgentCurrentStatus;