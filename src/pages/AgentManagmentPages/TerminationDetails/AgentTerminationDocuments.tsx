import React, { FC } from 'react';
import { Flex } from '@components/Flex';
import { H5 } from '@components/Typography/Typography';
import { Row } from 'antd';
import { TabSectionFieldLabel } from '../AgentReviewPage/style';
import OnboardingFormButton from '@components/OnboardingFormButton';
import DocumentImg from "@assets/icon-form.svg";

type AgentTerminationDocumentsProps = {
    test?: Text;
    docsList?: any
}

const AgentTerminationDocuments: FC<AgentTerminationDocumentsProps> = ({ test, docsList }) => {
    const { termination_exit_survey,agent_resignation_letter } = docsList
    const formDetails = { id: "", form_name: "", form_link: termination_exit_survey };
    const formDetail = { id: "", form_name: "", form_link: agent_resignation_letter };
    return (
        <Flex top={25} direction="column">
            <Row>
                <H5 text="Agent termination documents" />
            </Row>
            <Flex top={10} direction="column">
                <TabSectionFieldLabel>
                    Agent Resignation later
                </TabSectionFieldLabel>
                {agent_resignation_letter
                    ? <OnboardingFormButton
                        buttonText="View Form"
                        image={DocumentImg}
                        text="Resignation.pdf"
                        formDetails={formDetail}
                    />
                    : "-"}
            </Flex>

            <Flex top={12} direction="column">
                <TabSectionFieldLabel>Termination exit survey</TabSectionFieldLabel>
                {termination_exit_survey
                    ? <OnboardingFormButton
                        buttonText="View Form"
                        image={DocumentImg}
                        text={termination_exit_survey}
                        formDetails={formDetails}
                    />
                    : "-"}
            </Flex>
        </Flex>
    )
}

export default AgentTerminationDocuments;


