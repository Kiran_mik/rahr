import React, { FC } from 'react';
import { Flex } from '@components/Flex';
import { H5, FormLableTypography, LabelTypography } from '@components/Typography/Typography';
import { Row, Col } from 'antd';
import { dateFormat } from '@utils/format';


type BoardTerminationDateProps = {
    test?: Text;
    boardinfo?: any
}

const BoardTerminationDate: FC<BoardTerminationDateProps> = ({ test, boardinfo }) => {
    const { trreb_termination_date, mreb_termination_date } = boardinfo

    return (
        <Flex top={25} direction="column">
            <Row>
                <H5 text="Board termination date" />
            </Row>
            <Flex top={10} direction="column">
                <Row gutter={16}>
                    <Col className="gutter-row" span={8}>
                        <FormLableTypography>TRREB termination date </FormLableTypography>
                        <LabelTypography>{(trreb_termination_date && dateFormat(trreb_termination_date, 'DD MMMM YYYY')) || '-'}</LabelTypography>
                    </Col>

                    <Col className="gutter-row" span={8}>
                        <FormLableTypography>MREB termination date </FormLableTypography>
                        <LabelTypography>{(mreb_termination_date && dateFormat(mreb_termination_date, 'DD MMMM YYYY')) || '-'}</LabelTypography>
                    </Col>
                </Row>
            </Flex>
        </Flex>
    )
}

export default BoardTerminationDate;


