import React, { FC, useState } from 'react';
import { Flex } from '@components/Flex';
import { H5, LabelTypography } from '@components/Typography/Typography';
import { Row } from 'antd';
import { FormLableTypography } from '@components/Typography/Typography';
import { Col } from 'antd';
import { reasonForLeavingOptions } from '../Termination/helper';

type InformationAboutAgentTerminationProps = {
    test?: Text;
    terminationDetails?: any
}

const InformationAboutAgentTermination: FC<InformationAboutAgentTerminationProps> = ({ test, terminationDetails }) => {
    const { reason_for_agent_leaving_rahr, additional_comments, number_of_transaction, agent_status_with_rah,
        agent_termination_fee, which_agency_is_the_agent_joining, would_agent_join_rahr_again,
        how_long_was_the_agent_with_rahr, how_is_the_agent_terminated } = terminationDetails
    return (
        <Flex top={25} direction="column">
            <Row>
                <H5 text="Information about agent termination" />
            </Row>
            <Flex top={10} direction="column">
                <Row>
                    <Col className="gutter-row" span={8}>
                        <FormLableTypography>How is the agent terminated</FormLableTypography>
                        <LabelTypography>{how_is_the_agent_terminated || '-'}</LabelTypography>
                    </Col>
                    <Col className="gutter-row" span={8}>
                        <FormLableTypography>Agent termination fee</FormLableTypography>
                        <LabelTypography>{agent_termination_fee || '-'}</LabelTypography>
                    </Col>
                    <Col className="gutter-row" span={8}>
                        <FormLableTypography>Number of transactions last year</FormLableTypography>
                        <LabelTypography>{number_of_transaction || '-'}</LabelTypography>
                    </Col>
                </Row>
            </Flex>
            <Flex top={10} direction="column">
                <Row>
                    <Col className="gutter-row" span={8}>
                        <FormLableTypography>Reason for agent leaving RAH</FormLableTypography>
                        <LabelTypography>{(reason_for_agent_leaving_rahr && reasonForLeavingOptions[reason_for_agent_leaving_rahr].name) || '-'}</LabelTypography>
                    </Col>

                    <Col className="gutter-row" span={8}>
                        <FormLableTypography>Which agency is the agent joining</FormLableTypography>
                        <LabelTypography>{which_agency_is_the_agent_joining || '-'}</LabelTypography>
                    </Col>
                    <Col className="gutter-row" span={8}>
                        <FormLableTypography>Would agent join RAHR again</FormLableTypography>
                        <LabelTypography>{would_agent_join_rahr_again || '-'}</LabelTypography>
                    </Col>
                </Row>
            </Flex>
            <Flex top={10} direction="column">
                <Row>
                    <Col className="gutter-row" span={8}>
                        <FormLableTypography>How long was the agent with RAHR</FormLableTypography>
                        <LabelTypography>{how_long_was_the_agent_with_rahr || '-'}</LabelTypography>
                    </Col>

                    <Col className="gutter-row" span={8}>
                        <FormLableTypography>Agent status with RAH</FormLableTypography>
                        <LabelTypography>{agent_status_with_rah || '-'}</LabelTypography>
                    </Col>
                </Row>
            </Flex>
            <Flex top={10} direction="column">
                <Row>
                    <Col className="gutter-row" span={12}>
                        <FormLableTypography>Comments</FormLableTypography>
                        <LabelTypography>{additional_comments || '-'}</LabelTypography>
                    </Col>
                </Row>
            </Flex>
        </Flex>
    )
}

export default InformationAboutAgentTermination;