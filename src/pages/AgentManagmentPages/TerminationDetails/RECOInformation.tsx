import React, { FC } from 'react';
import { Flex } from '@components/Flex';
import { H5, LabelTypography, FormLableTypography } from '@components/Typography/Typography';
import { Row, Col } from 'antd';
import { dateFormat } from '@utils/format';

type RECOInformationProps = {
    test?: Text;
    recoinfo?: any
}

const RECOInformation: FC<RECOInformationProps> = ({ test, recoinfo }) => {
    const { agent_reco_number, reco_termination_date, reco_start_date } = recoinfo

    return (
        <Flex top={25} direction="column">
            <Row>
                <H5 text="RECO information" />
            </Row>
            <Flex top={10} direction="column">
                <Row>
                    <Col className="gutter-row" span={8}>
                        <FormLableTypography>Agent’s RECO number</FormLableTypography>
                        <LabelTypography>{agent_reco_number || '-'}</LabelTypography>
                    </Col>

                    <Col className="gutter-row" span={8}>
                        <FormLableTypography>RECO Start date</FormLableTypography>
                        <LabelTypography>{(reco_start_date && dateFormat(reco_start_date, 'DD MMMM, YYYY')) || '-'}</LabelTypography>
                    </Col>
                    <Col className="gutter-row" span={8}>
                        <FormLableTypography>RECO termination date </FormLableTypography>
                        <LabelTypography>{(reco_termination_date && dateFormat(reco_termination_date, 'DD MMMM YYYY')) || '-'}</LabelTypography>
                    </Col>
                </Row>
            </Flex>
            {/* <Flex top={10} direction="column">
                <Row>
                    <Col className="gutter-row" span={4}>
                        <FormLableTypography>RECO termination date <span>*</span></FormLableTypography>
                        <LabelTypography>21 November, 2020</LabelTypography>
                    </Col>
                </Row>
            </Flex> */}
        </Flex>
    )
}

export default RECOInformation;


