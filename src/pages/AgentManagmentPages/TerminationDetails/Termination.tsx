import React, { FC, useEffect, useState } from 'react';
import { TabItemLayout } from '../AgentReviewPage/style';
import { Flex } from '@components/Flex';
import { H2 } from '@components/Typography/Typography';
import InformationAboutAgentTermination from './InformationAboutAgentTermination';
import AgentTerminationDocuments from './AgentTerminationDocuments';
import TerminationForms from './TerminationForms';
import RECOInformation from './RECOInformation';
import BoardTerminationDate from './BoardsTerminationDate';
import { agentTermination } from './helper';
import { useParams } from "@reach/router";
import { EditIconButton } from '@components/Buttons';
import ExportIcon from "@assets/icons/export.png";

type TerminationProps = {
    test: Text;
}

const TerminationClone: FC<TerminationProps> = ({ test }) => {
    const params = useParams();
    const [terminationDetails, setTerminationDetails] = useState({})
    useEffect(() => {
        const getData = async () => {
            const res = await agentTermination(params.agentId)
            setTerminationDetails(res)
        }
        getData()
    }, [])

    return (
        <TabItemLayout>
            <Flex>
                <Flex flex={1}>
                    <H2 text="Agent profile information" className="font-600" />
                </Flex>

                <EditIconButton
                    icon={ExportIcon}
                    text={
                        <span style={{ fontWeight: 600, color: "#4E1C95", fontSize: 14 }}>
                            Export as CSV
                        </span>
                    }
                />
            </Flex>
            <InformationAboutAgentTermination terminationDetails={terminationDetails} />
            <RECOInformation recoinfo={terminationDetails} />
            <BoardTerminationDate boardinfo={terminationDetails}/>
            <AgentTerminationDocuments docsList={terminationDetails}/>
            <TerminationForms heading='Termination forms' formsList={terminationDetails}/>
        </TabItemLayout>
    )
}

export default TerminationClone;