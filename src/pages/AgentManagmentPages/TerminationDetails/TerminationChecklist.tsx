import React, { FC } from 'react';
import { Flex } from '@components/Flex';
import { H5 } from '@components/Typography/Typography';
import { ContentWrapper } from '../../AgentOnboardingPages/WelcomeScreen/style';
import { Row } from 'antd';
import { Checkbox } from '@components/Checkbox';

type StatusProps = {
    test?: Text;
}

const checklist = [
    {
        id: 1,
        label: "Did Agent return all items that was borrowed/belongs to RAHR?"
    },
    {
        id: 2,
        label: "Has agent been removed from RECO website?"
    },
    {
        id: 3,
        label: "Has Agent been removed from the regional Board(s)?"
    },
    {
        id: 4,
        label: "Has Agent been terminated from Brokerwolf?"
    },
    {
        id: 5,
        label: "Has Agent been terminated from WolfConnect?"
    },
    {
        id: 6,
        label: "Has Agent's access been removed from ShowingTime?"
    },
    {
        id: 7,
        label: "Has Agent's Security Card been deactivated?"
    },
    {
        id: 8,
        label: "Has Agent been removed from MailChimp?"
    },
    {
        id: 9,
        label: "Has Agent been removed from the RAHR's Website/all RAHR's Social Media Outlets?"
    },
    {
        id: 10,
        label: "Has Agent's mail folder been removed?"
    },
]
const TerminationChecklist: FC<StatusProps> = ({ test }) => {

    return (
        <Flex top={20} flex={1} direction="column">
            <H5 text="Termination checklist" />

            {checklist.map((item: any) => (
                <Flex top={5} key={item.id}>
                    <Checkbox
                        name={item.label}
                        id={item.id}
                    />
                </Flex>
            ))}


        </Flex>
    )
}

export default TerminationChecklist;