import React, { FC } from 'react';
import { Flex } from '@components/Flex';
import { H5 } from '@components/Typography/Typography';
import { Row } from 'antd';
import { TabSectionFieldLabel } from '../AgentReviewPage/style';
import OnboardingFormButton from '@components/OnboardingFormButton';
import DocumentImg from "@assets/icon-form.svg";

type TerminationFormsProps = {
    test?: Text;
    heading?: string;
    formsList?: any
}

const TerminationForms: FC<TerminationFormsProps> = ({ test, heading, formsList }) => {
    const { mreb_form, trreb_form } = formsList
    const formDetails = { id: "", form_name: "", form_link: trreb_form };
    const formDetail = { id: "", form_name: "", form_link: mreb_form };
    return (
        <Flex top={25} direction="column">
            <Row>
                <H5 text={heading} />
            </Row>
            <Flex top={10} direction="column">
                <TabSectionFieldLabel>
                    TRREB
                </TabSectionFieldLabel>
                {trreb_form
                    ? <OnboardingFormButton
                        buttonText="View Form"
                        image={DocumentImg}
                        text="trreb-form.pdf"
                        formDetails={formDetails}
                    />
                    : "-"}
            </Flex>

            <Flex top={10} direction="column">
                <TabSectionFieldLabel>
                    MREB
                </TabSectionFieldLabel>
                {trreb_form
                    ? <OnboardingFormButton
                        buttonText="View Form"
                        image={DocumentImg}
                        text="mreb-form.pdf"
                        formDetails={formDetail}
                    />
                    : "-"}
            </Flex>
        </Flex>
    )
}

export default TerminationForms;


