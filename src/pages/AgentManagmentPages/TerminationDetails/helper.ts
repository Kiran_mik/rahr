import services from "@services/index";
import { BASE_URL_MAP } from "@constants/index";


export const agentTermination = async (agentId: any) => {
  try {
    const res = await services.get(
      `v1/agents/onboardings/terminate/` + agentId,
      process.env.REACT_APP_BASE_URL_AGENT
    );
    //@ts-ignore
    return res.data.data;
  } catch (err) {
    return [];
  }
};


// export const submitReview = async (agentId: number, isApprove: boolean) => {
//   try {
//     const res = await services.post(
//       `v1/agents/onboarding/reviews/submit`,
//       {
//         agent_id: agentId,
//         type: isApprove ? "approve" : "feedback"
//       },
//       BASE_URL_MAP["agent-onboard"]
//     );
//     return res;
//   } catch (error) { }
// }
