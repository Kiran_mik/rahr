import { Flex } from '@components/Flex';
import { Absolute } from '@components/Flex/style';
import Loader from '@components/Loader';
import { H1 } from '@components/Typography/Typography';
import useAgentSteps from '@hooks/useAgentSteps';
import { navigate, useLocation } from '@reach/router';
import { AgentOnBoardingSteps } from '@store/agentOnboardingReducer';
import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { STEPS, STEPS_ENUM } from './constants';
import ContactDetails from './ContactDetails';
import { getFormDetails } from './helper';
import SideSteperComponent from './SideSteperComponent';
import {
  AccountRegistrationContainer, AgentOnboardingPagesContainer, AgentOnboardingStepContainer,
  SideSteperWrapper
} from './style';



function AgentOnboardingLayout(props: { children: React.ReactChildren }) {
  const [isLoading, setIsLoading] = useState(false);
  const [setForm, setStep, form] = useAgentSteps();
  const onboardingForm = form as AgentOnBoardingSteps;
  const location = useLocation();
  const user = useSelector((state: { user: any }) => state.user);
  const { userId, status } = user;
  useEffect(() => {
    window.addEventListener("popstate", (e: any) => {
      loadFormDetails();
    });
  }, []);

  useEffect(() => {
    loadFormDetails();
  }, [location.pathname, status]);

  useEffect(() => {
    const { currentStep } = form;
    const stepObject = STEPS[currentStep];
    if (stepObject) {
      const searchedPrams = window.location.search;
      navigate(`/agent-onboard/${stepObject.key}/${searchedPrams}`, {
        replace: false,
      });
    }
  }, [form.currentStep]);

  const loadFormDetails = async () => {
    const { currentStep } = onboardingForm;
    const stepObject = STEPS[currentStep];

    const currentFormKey = stepObject && stepObject.storeKey;

    if (currentFormKey && stepObject.isGet) {
      setIsLoading(true);
      const formDetails = await getFormDetails(currentStep);
      if(!formDetails){
        setIsLoading(false);
        return;
      }
      if (formDetails.id) {
        formDetails.stepId = formDetails.id;
      }
      setForm(currentFormKey, {
        ...formDetails,
      });
      setIsLoading(false);
    }
  };
  return (
    <AgentOnboardingPagesContainer id={"agent-onboarding-pages-container"}>
      <Flex
        justifyContent={"center"}
        id={"agent-onboarding-pages"}
        style={{
          overflowY: "auto",
          maxHeight: "calc(100vh - 80px)",
          minHeight: "calc(100vh - 80px)",
          padding: "24px",
        }}
      >
        <AccountRegistrationContainer>
          <Flex
            direction="row"
            justifyContent={"space-between"}
            alignItems={"center"}
          >
            <H1 text="Account Registration" className="font-600" />
            <div>{"Ticket#" + userId}</div>
          </Flex>
          <AgentOnboardingStepContainer top={24}>
            <Flex style={{ borderRight: "1px solid #E2E2EE" }}>
              <SideSteperWrapper>
                <SideSteperComponent />
                {!(
                  form.currentStep == STEPS_ENUM.CONFIRMATION ||
                  form.currentStep == STEPS_ENUM.REVIEW
                ) && <ContactDetails />}
              </SideSteperWrapper>
            </Flex>
            <Absolute top={"40%"} left={"50%"}>
              <Loader loading={isLoading} />
            </Absolute>
            <Flex flex={1} style={{ width: "100%", display: "block" }}>
              {props.children}
            </Flex>
          </AgentOnboardingStepContainer>
        </AccountRegistrationContainer>
      </Flex>
    </AgentOnboardingPagesContainer>
  );
}

export default AgentOnboardingLayout;
