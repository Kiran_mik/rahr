import React from "react";
import OnboardingFormButton from "@components/OnboardingFormButton";

export interface PropContent {
    text?: React.ReactNode;
    image?: any;
    buttonText?: string;
    formDetails: { id: string, docusign_id?: string, form_link?: string, form_name: string }
}


export const RequiredFormUpload = (props: PropContent): JSX.Element => {
    return <OnboardingFormButton {...props} />;
};
export default RequiredFormUpload;
