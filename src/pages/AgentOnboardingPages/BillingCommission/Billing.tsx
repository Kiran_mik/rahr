import DocumentUpload from "@components/DocumentUploader";

import useSWR from "swr";

import { Flex } from "@components/Flex";
import { InputField } from "@components/Input/Input";
import { Note } from "@components/Note";
import {
  Description,
  FormLableTypography,
  H3,
  H5Typography,
} from "@components/Typography/Typography";
import { Col, Radio, Row } from "antd";
import React, { useEffect, useState } from "react";
import { ContentWrapper } from "../style";
import DocumentImg from "@assets/icon-form.svg";
import DatePickerComponent from "@components/DatePicker";
import { BILLIN_MODE_ENUM, checkBillingDocuSignEnvolpe } from "./helper";
import DocuSign from "@components/DocuSign";
import { getQueryParams } from "@utils/helpers";
import { getDocuSignLink } from "@services/UserService";

const Billing: React.FC<{
  formik: any;
  joinAsPrec: boolean;
}> = (props: any) => {
  let { formik, joinAsPrec } = props;
  const { errors, values, handleChange, touched, setFieldValue } = formik;
  const billingEnvId = getQueryParams("envelope_id");
  const [showLoader, setShowLoader] = useState(false);

  const url = "v1/agents/onboardings/billing-commission/docusign";
  const { data, error } = useSWR(
    [
      url,
      billingEnvId ||
      values.quarterly_fees_autodebit_authorization_signed_document,
    ],
    getDocuSignLink,
    {
      revalidateOnFocus: false,
    }
  );

  const docuSignPlatformURL = data;

  useEffect(() => {
    if (
      billingEnvId &&
      !values.quarterly_fees_autodebit_authorization_signed_document
    ) {
      checkDocuSignEnvolpe(billingEnvId);
    }
  }, [
    billingEnvId,
    values.quarterly_fees_autodebit_authorization_envelope_id,
    values.quarterly_fees_autodebit_authorization_signed_document,
  ]);

  const checkDocuSignEnvolpe = async (envId: string) => {
    setShowLoader(true);
    const res = await checkBillingDocuSignEnvolpe(envId, values);
    if (res.success) {
      formik.setFieldValue(
        "quarterly_fees_autodebit_authorization_signed_document",
        res.data
      );
    }
    setShowLoader(false);
  };
  const isValidating = !data;

  const isLoading =
    (isValidating &&
      !values.quarterly_fees_autodebit_authorization_sign_url &&
      !values.quarterly_fees_autodebit_authorization_signed_document) ||
    showLoader;

  return (
    <ContentWrapper className="bordernone">
      <Flex direction="column">
        <H3
          text={`${joinAsPrec ? "PREC " : ""}Billing`}
          className="space-bottom-16 font-500"
        />
        <Description text="With our Auto-Debit program, your office payment is made automatically on the payment due date – and you don’t even have to sign the cheque. Quarterly fees will be Auto-Debited from your bank account on the first business day of January, April, July and October. Please note that in the event that at any time you are leaving Right at Home Realty Inc, you must provide a resignation letter at least 3 business days prior to the next quarterly billing period. If you fail to follow this policy, you will be charged by the accounting department with the quarterly fee." />
        <Note
          message="**Note:"
          description= {<span style={{marginLeft:"5px"}}>**NOTE: RAH is NOT responsible for any NSF charges incurred by the account holder<br></br>
           <span style={{color:"red"}}> ***</span>There is a $75 processing adminstration fee for any late payments</span>}
        />
      </Flex>
      <Flex direction="column" bottom={16}>
        <H5Typography className="spaceBottom-16 font-500">
          Please select following billing method
        </H5Typography>

        <Radio.Group
          value={values.billing_method}
          name={"billing_method"}
          id={"billing_method"}
          onChange={handleChange}
        >
          <Radio value={1}>Pre-authorized debit</Radio>
          <Radio value={2}>Credit card</Radio>
        </Radio.Group>
      </Flex>
      {values.billing_method == BILLIN_MODE_ENUM.PRE_AUTHORIZE_DEBIT ? (
        <Row gutter={16}>
          <Col span={8}>
            <FormLableTypography>
              Transit number <span>*</span>
            </FormLableTypography>
            <InputField
              id="billing_transit_number"
              name="billing_transit_number"
              onChange={handleChange}
              value={values.billing_transit_number}
              error={
                touched.billing_transit_number && errors.billing_transit_number
              }
            />
          </Col>
          <Col span={8}>
            <FormLableTypography>
              Institution number <span>*</span>
            </FormLableTypography>
            <InputField
              id="billing_institution_no"
              name="billing_institution_no"
              onChange={handleChange}
              value={values.billing_institution_no}
              error={
                touched.billing_institution_no && errors.billing_institution_no
              }
            />
          </Col>
          <Col span={8}>
            <FormLableTypography>
              Account number <span>*</span>
            </FormLableTypography>
            <InputField
              id="billing_account_no"
              name="billing_account_no"
              onChange={handleChange}
              value={values.billing_account_no}
              error={touched.billing_account_no && errors.billing_account_no}
            />
          </Col>
        </Row>
      ) : (
        <Row gutter={16}>
          <Col span={10}>
            <FormLableTypography>
              Credit card number <span>*</span>
            </FormLableTypography>
            <InputField
              id="billing_credit_card_no"
              name="billing_credit_card_no"
              onChange={handleChange}
              value={values.billing_credit_card_no}
              error={
                touched.billing_credit_card_no && errors.billing_credit_card_no
              }
              placeholder="_ _ _ _  _ _ _ _  _ _ _ _  _ _ _ _ "
            />
          </Col>
          <Col span={7}>
            <FormLableTypography>
              Expiration date <span>*</span>
            </FormLableTypography>
            <DatePickerComponent
              id="billing_expire_date"
              name="billing_expire_date"
              onChange={(value: any) => {
                setFieldValue("billing_expire_date", value);
              }}
              value={values.billing_expire_date}
              error={touched.billing_expire_date && errors.billing_expire_date}
              placeholder="mm/yy"
              picker="month"
              format="MM/YY"
            />
          </Col>
          <Col span={7}>
            <FormLableTypography>
              CVC <span>*</span>
            </FormLableTypography>
            <InputField
              id="billing_cvc"
              name="billing_cvc"
              onChange={handleChange}
              value={values.billing_cvc}
              error={touched.billing_cvc && errors.billing_cvc}
              placeholder="_ _ _"
            />
          </Col>
        </Row>
      )}

      <Flex direction="column">
        <H5Typography className="spaceBottom-16 font-500">
          Attach a VOID cheque <span>*</span>
        </H5Typography>
        <DocumentUpload
          id="billing_void_cheque_doc"
          name="billing_void_cheque_doc"
          error={
            touched.billing_void_cheque_doc && errors.billing_void_cheque_doc
          }
          type="file"
          onChange={(value: any) => {
            setFieldValue("billing_void_cheque_doc", value);
          }}
          label="Billing Cheque"
          docURL={values.commission_void_cheque_doc}
        />
      </Flex>

      <Flex top={10} direction="column">
        <FormLableTypography>
          Personal Real Estate Corporation form<span>*</span>
        </FormLableTypography>

        <DocuSign
          text="Quarterly fees auto-debit authorization	"
          linktext="View and sign form"
          image={DocumentImg}
          link={docuSignPlatformURL}
          isLoading={isLoading}
          docURL={values.quarterly_fees_autodebit_authorization_signed_document}
        />

      </Flex>
      <Flex top={32} bottom={16}>
        <FormLableTypography>
          {" "}
          In case of late payment to avoid $75 administration fee - please enter
          your credit card as back up option
        </FormLableTypography>
      </Flex>
      <Row gutter={16}>
        <Col span={10}>
          <FormLableTypography>
            Credit card number <span>*</span>
          </FormLableTypography>
          <InputField
            id="credit_card_no"
            name="credit_card_no"
            onChange={handleChange}
            value={values.credit_card_no}
            error={touched.credit_card_no && errors.credit_card_no}
            placeholder="_ _ _ _  _ _ _ _  _ _ _ _  _ _ _ _ "
          />
        </Col>
        <Col span={7}>
          <FormLableTypography>
            Expiration date <span>*</span>
          </FormLableTypography>
          <DatePickerComponent
            id="expire_date"
            name="expire_date"
            onChange={(value: any) => {
              setFieldValue("expire_date", value);
            }}
            value={values.expire_date}
            error={touched.expire_date && errors.expire_date}
            placeholder="mm/yy"
            picker="month"
            format="MM/YY"
          />
        </Col>
        <Col span={7}>
          <FormLableTypography>
            CVC <span>*</span>
          </FormLableTypography>
          <InputField
            id="cvc"
            name="cvc"
            onChange={handleChange}
            value={values.cvc}
            error={touched.cvc && errors.cvc}
            placeholder="_ _ _"
          />
        </Col>
      </Row>
    </ContentWrapper>
  );
};

export default Billing;
