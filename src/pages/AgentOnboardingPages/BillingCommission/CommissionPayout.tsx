import BankDetail from "@assets/bank-details.svg";
import QuestionMark from "@assets/question-mark.svg";
import { Checkbox } from "@components/Checkbox";
import DocumentUpload from "@components/DocumentUploader";
import { Flex } from "@components/Flex";
import { InputField } from "@components/Input/Input";
import { Note } from "@components/Note";
import { TooltipText } from "@components/Tooltip";
import {
  FormLableTypography,
  H3,
  H5Typography,
} from "@components/Typography/Typography";
import { Col, Radio, Row, Tooltip } from "antd";
import React from "react";
import { ContentWrapper, SquareRadioWrapper } from "../style";
import { PAYMENT_MODE_ENUM } from "./helper";

const CommissionPayout: React.FC<{
  formik: any;
  joinAsPrec: boolean;
}> = (props: any) => {
  let { formik, joinAsPrec } = props;
  const { errors, values, handleChange, touched, setFieldValue } = formik;
   return (
    <ContentWrapper>
      <Flex direction="column">
        <H3
          text={`${joinAsPrec ? "PREC " : ""} Commission Payout`}
          className="space-bottom-16 font-500"
        />
      </Flex>
      <Flex>
        <H5Typography className="spaceBottom-16 font-500">
          Select preferred payout mode <span>*</span>
        </H5Typography>
        <TooltipText
          text={
            <div>
              Can apply via <br />
              1-800-959-5525 or
              <br />
              <a
                href="http://www.cra-arc.gc.ca"
                target="_blank"
                style={{ color: "rgba(78, 28, 149, 1)" }}
              >
                http://www.cra-arc.gc.ca
              </a>{" "}
            </div>
          }
        />
      </Flex>
      <SquareRadioWrapper>
        <Row className="squareRadioBtn">
          <Radio.Group
            value={values.payout_mode}
            name={"payout_mode"}
            id={"payout_mode"}
            onChange={handleChange}
          >
            <Radio value={1}>Electronic Fund Transfer</Radio>
            <Radio value={2}>Cheque</Radio>
          </Radio.Group>
        </Row>
      </SquareRadioWrapper>
      {values.payout_mode == PAYMENT_MODE_ENUM.ELECTRONIC_FUND_TRANSFER && (
        <>
          <Note
            message="**NOTE:"
            description=" Please remember to provide us with your HST number. This will designate the bank and the account you wish your commissions to be deposited."
          />
          <Flex>
            <H5Typography className="spaceBottom-16 font-500">
              Enter your bank details for secure pre-authorized debit
            </H5Typography>
            &nbsp;
            <Tooltip
              placement="top"
              title={<img src={BankDetail} alt="Bank Details" width="100%" />}
            >
              {" "}
              <img
                src={QuestionMark}
                alt="Tooltip Icon"
                style={{ maxWidth: "max-content" }}
              />
            </Tooltip>
          </Flex>
          <Row gutter={16}>
            <Col span={8}>
              <FormLableTypography>
                Transit number <span>*</span>
              </FormLableTypography>
              <InputField
                id="commission_transit_number"
                name="commission_transit_number"
                onChange={handleChange}
                value={values.commission_transit_number}
                error={
                  touched.commission_transit_number &&
                  errors.commission_transit_number
                }
              />
            </Col>
            <Col span={8}>
              <FormLableTypography>
                Institution number <span>*</span>
              </FormLableTypography>
              <InputField
                id="commission_institution_no"
                name="commission_institution_no"
                onChange={handleChange}
                value={values.commission_institution_no}
                error={
                  touched.commission_institution_no &&
                  errors.commission_institution_no
                }
              />
            </Col>
            <Col span={8}>
              <FormLableTypography>
                Account number <span>*</span>
              </FormLableTypography>
              <InputField
                id="commission_account_no"
                name="commission_account_no"
                onChange={handleChange}
                value={values.commission_account_no}
                error={
                  touched.commission_account_no && errors.commission_account_no
                }
              />
            </Col>
          </Row>
        </>
      )}
      {values.payout_mode == PAYMENT_MODE_ENUM.CHEQUE && (
        <>
          {/* cheque */}
          <Flex top={16}>
            <Note
              message="**IMPORTANT:"
              description=" With this option there will be an additional $25 admin fee each time cheque is issued. Instead of transferring the rest to your account,  this amount is going to be deducted from their pay and the rest will be issued to them in a cheque format"
            />
          </Flex>
          <H5Typography className="spaceBottom-16 font-500">
            Mailing address
          </H5Typography>
          <Flex bottom={16}>
            <Checkbox
              name={"Same as personal address"}
              id="commission_is_same_as_personal_address"
              onChange={(e: any) => {
                handleChange(e);
                if (e.target.checked) {
                  setFieldValue(
                    "commission_mailing_address_data_address_1",
                    values.address_1
                  );
                  setFieldValue(
                    "commission_mailing_address_data_address_2",
                    values.address_2
                  );
                  setFieldValue(
                    "commission_mailing_address_data_city",
                    values.city
                  );
                  setFieldValue(
                    "commission_mailing_address_data_province",
                    values.province
                  );
                  setFieldValue(
                    "commission_mailing_address_data_postal_code",
                    values.postal_code
                  );
                }
              }}
              checked={values.commission_is_same_as_personal_address}
              error={errors.commission_is_same_as_personal_address}
            />
          </Flex>
          <Row gutter={16}>
            <Col span={16}>
              <FormLableTypography>
                Address #1 <span>*</span>
              </FormLableTypography>
              <InputField
                id="commission_mailing_address_data_address_1"
                name="commission_mailing_address_data_address_1"
                onChange={handleChange}
                value={values.commission_mailing_address_data_address_1}
                error={
                  touched.commission_mailing_address_data_address_1 &&
                  errors.commission_mailing_address_data_address_1
                }
                placeholder="17 Random Street"
                disabled={values.commission_is_same_as_personal_address}
              />
            </Col>
            <Col span={8}>
              <FormLableTypography>Address #2</FormLableTypography>
              <InputField
                id="commission_mailing_address_data_address_2"
                name="commission_mailing_address_data_address_2"
                onChange={handleChange}
                value={values.commission_mailing_address_data_address_2}
                error={
                  touched.commission_mailing_address_data_address_2 &&
                  errors.commission_mailing_address_data_address_2
                }
                placeholder="Unit 2B"
                disabled={values.commission_is_same_as_personal_address}
              />
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={8}>
              <FormLableTypography>
                City <span>*</span>
              </FormLableTypography>
              <InputField
                id="commission_mailing_address_data_city"
                name="commission_mailing_address_data_city"
                onChange={handleChange}
                value={values.commission_mailing_address_data_city}
                error={
                  touched.commission_mailing_address_data_city &&
                  errors.commission_mailing_address_data_city
                }
                placeholder="Vaughan"
                disabled={values.commission_is_same_as_personal_address}
              />
            </Col>
            <Col span={8}>
              <FormLableTypography>
                Province <span>*</span>
              </FormLableTypography>
              <InputField
                id="commission_mailing_address_data_province"
                name="commission_mailing_address_data_province"
                onChange={handleChange}
                value={values.commission_mailing_address_data_province}
                error={
                  touched.commission_mailing_address_data_province &&
                  errors.commission_mailing_address_data_province
                }
                placeholder="Ontario"
                disabled={values.commission_is_same_as_personal_address}
              />
            </Col>
            <Col span={8}>
              <FormLableTypography>
                Postal code <span>*</span>
              </FormLableTypography>
              <InputField
                id="commission_mailing_address_data_postal_code"
                name="commission_mailing_address_data_postal_code"
                onChange={handleChange}
                value={values.commission_mailing_address_data_postal_code}
                error={
                  touched.commission_mailing_address_data_postal_code &&
                  errors.commission_mailing_address_data_postal_code
                }
                placeholder="M2M W1W"
                disabled={values.commission_is_same_as_personal_address}
              />
            </Col>
          </Row>
          {/* end */}
        </>
      )}

      <Flex direction="column">
        <H5Typography className="spaceBottom-16 font-500">
          Attach a VOID cheque <span>*</span>
        </H5Typography>
        <DocumentUpload
          id="commission_void_cheque_doc"
          name="commission_void_cheque_doc"
          error={
            touched.commission_void_cheque_doc &&
            errors.commission_void_cheque_doc
          }
          type="file"
          onChange={(value: any) => {
            setFieldValue("commission_void_cheque_doc", value);
          }}
          label="Commission Cheque"
          docURL={values.commission_void_cheque_doc}
        />
      </Flex>
      <Flex direction="column">
        <H5Typography className="spaceBottom-16 font-500">
          Authorization agreement
        </H5Typography>
        <Checkbox
          name="I give authorization to Right at Home Realty Inc. for Electronic Transfer (Direct Deposit) for my
commission cheques."
          id="is_authorized_agreement"
          onChange={handleChange}
          checked={values.is_authorized_agreement}
          error={
            touched.is_authorized_agreement && errors.is_authorized_agreement
          }
        />
      </Flex>
    </ContentWrapper>
  );
};

export default CommissionPayout;
