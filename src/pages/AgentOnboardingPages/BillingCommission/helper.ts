import services from "@services/index";
import { uploadFile } from "@services/UserService";
import valid from "card-validator";
import { updateUserStore } from "@services/UserService";

import * as Yup from "yup";
import { AGENT_ONBOARDING_BASE_URL } from "../constants";
import { checkDocuSign } from './../../../services/UserService';


export const PAYMENT_MODE_ENUM = {
    ELECTRONIC_FUND_TRANSFER: 1,
    CHEQUE: 2
}


export const BILLIN_MODE_ENUM = {
    PRE_AUTHORIZE_DEBIT: 1,
    CREDIT_CARD: 2
}

export const validationSchema = Yup.object({

    //Commission
    payout_mode: Yup.number(),
    commission_transit_number: Yup
        .string()
        .nullable()
        .when("payout_mode", {
            is: (payout_mode: number) => payout_mode == PAYMENT_MODE_ENUM.ELECTRONIC_FUND_TRANSFER,
            then: Yup.string().required("Please enter transit number").matches(/^[0-9]+$/, "Must be only digits").nullable(),
            otherwise: Yup.string().nullable(),
        }),
    commission_institution_no: Yup
        .string()
        .nullable()
        .when("payout_mode", {
            is: (payout_mode: number) => payout_mode == PAYMENT_MODE_ENUM.ELECTRONIC_FUND_TRANSFER,
            then: Yup.string().required("Please enter institution number").matches(/^[0-9]+$/, "Must be only digits").nullable(),
            otherwise: Yup.string().nullable(),
        }),
    commission_account_no: Yup
        .string()
        .nullable()
        .when("payout_mode", {
            is: (payout_mode: number) => payout_mode == PAYMENT_MODE_ENUM.ELECTRONIC_FUND_TRANSFER,
            then: Yup.string().required("Please enter account number").matches(/^[0-9]+$/, "Must be only digits").nullable(),
            otherwise: Yup.string().nullable(),
        }),
    //-------end---------

    //Commission mailing address
    commission_is_same_as_personal_address: Yup.boolean().nullable(),

    commission_mailing_address_data_address_1: Yup
        .string()
        .nullable()
        .when(["payout_mode", "commission_is_same_as_personal_address"], {
            is: ((payout_mode: number, commission_is_same_as_personal_address: boolean) =>
                (!commission_is_same_as_personal_address && payout_mode == PAYMENT_MODE_ENUM.CHEQUE)),
            then: Yup
                .string()
                .nullable()
                .required("Please enter address"),
            otherwise: Yup.string().nullable(),
        }),

    commission_mailing_address_data_address_2: Yup
        .string()
        .nullable()
        .when(["payout_mode", "commission_is_same_as_personal_address"], {
            is: ((payout_mode: number, commission_is_same_as_personal_address: boolean) =>
                (!commission_is_same_as_personal_address && payout_mode == PAYMENT_MODE_ENUM.CHEQUE)),
            then: Yup
                .string()
                .nullable(),
            otherwise: Yup.string().nullable(),
        }),

    commission_mailing_address_data_city: Yup
        .string()
        .nullable()
        .when(["payout_mode", "commission_is_same_as_personal_address"], {
            is: ((payout_mode: number, commission_is_same_as_personal_address: boolean) =>
                (!commission_is_same_as_personal_address && payout_mode == PAYMENT_MODE_ENUM.CHEQUE)),
            then: Yup
                .string()
                .nullable()
                .required("Please enter city"),
            otherwise: Yup.string().nullable(),
        }),

    commission_mailing_address_data_province: Yup
        .string()
        .nullable()
        .when(["payout_mode", "commission_is_same_as_personal_address"], {
            is: ((payout_mode: number, commission_is_same_as_personal_address: boolean) =>
                (!commission_is_same_as_personal_address && payout_mode == PAYMENT_MODE_ENUM.CHEQUE)),
            then: Yup
                .string()
                .nullable()
                .required("Please enter province"),
            otherwise: Yup.string().nullable(),
        }),

    commission_mailing_address_data_postal_code: Yup
        .string()
        .nullable()
        .when(["payout_mode", "commission_is_same_as_personal_address"], {
            is: ((payout_mode: number, commission_is_same_as_personal_address: boolean) =>
                (!commission_is_same_as_personal_address && payout_mode == PAYMENT_MODE_ENUM.CHEQUE)),
            then: Yup
                .string()
                .nullable()
                .required("Please enter postal code"),
            otherwise: Yup.string().nullable(),
        }),
    //-------end---------

    // void cheque and authorize
    commission_void_cheque_doc: Yup.
        mixed()
        .required('Please upload a void cheque')
        .nullable(),

    is_authorized_agreement: Yup.boolean().oneOf([true], "Please select the checkbox"),
    //-------end---------

    //Billing
    billing_method: Yup.number(),

    billing_transit_number: Yup
        .string()
        .nullable()
        .when("billing_method", {
            is: (billing_method: number) => billing_method == BILLIN_MODE_ENUM.PRE_AUTHORIZE_DEBIT,
            then: Yup.string()
                .required("Please enter transit number").matches(/^[0-9]+$/, "Must be only digits").nullable(),
            otherwise: Yup.string().nullable(),
        }),
    billing_institution_no: Yup
        .string()
        .nullable()
        .when("billing_method", {
            is: (billing_method: number) => billing_method == BILLIN_MODE_ENUM.PRE_AUTHORIZE_DEBIT,
            then: Yup.string().required("Please enter institution number").matches(/^[0-9]+$/, "Must be only digits").nullable(),
            otherwise: Yup.string().nullable(),
        }),
    billing_account_no: Yup
        .string()
        .nullable()
        .when("billing_method", {
            is: (billing_method: number) => billing_method == BILLIN_MODE_ENUM.PRE_AUTHORIZE_DEBIT,
            then: Yup.string().required("Please enter account number").matches(/^[0-9]+$/, "Must be only digits").nullable(),
            otherwise: Yup.string().nullable(),
        }),
    //-------end---------

    //Billing credit card
    billing_credit_card_no: Yup
        .string()
        .nullable()
        .when("billing_method", {
            is: (billing_method: number) => billing_method == BILLIN_MODE_ENUM.CREDIT_CARD,
            then: Yup.string().required("Please enter credit card number").nullable().test(
                "test-number",
                "Credit Card number is invalid",
                value => valid.number(value).isValid
            ),
            otherwise: Yup.string().nullable(),
        }),

    billing_expire_date: Yup
        .date()
        .nullable()
        .when("billing_method", {
            is: (billing_method: number) => billing_method == BILLIN_MODE_ENUM.CREDIT_CARD,
            then: Yup
                .date()
                .required("Please enter expiry date")
                .nullable()
                .min(new Date(), "Credit card is expired")
            ,
            otherwise: Yup.date().nullable(),
        }),

    billing_cvc: Yup
        .string()
        .nullable()
        .when("billing_method", {
            is: (billing_method: number) => billing_method == BILLIN_MODE_ENUM.CREDIT_CARD,
            then: Yup
                .string()
                .required("Please enter cvc")
                .test(
                    "test-number",
                    "Invalid cvv",
                    value => valid.cvv(value).isValid
                )
                .nullable()
            ,
            otherwise: Yup.string().nullable(),
        }),
    //-------end---------

    // billing void cheque and authorization agreement
    billing_void_cheque_doc: Yup.
        mixed()
        .required('Please upload a void cheque')
        .nullable(),
    //-------end---------


    //Credit card
    credit_card_no: Yup.string()
        .required("Please enter credit card number").test(
            "test-number",
            "Credit Card number is invalid",
            value => valid.number(value).isValid
        ).nullable(),

    expire_date: Yup
        .date()
        .required("Please enter expiry date")
        .nullable()
        .min(new Date(), "Credit card is expired"),

    cvc: Yup
        .string()
        .required("Please enter cvc")
        .test(
            "test-number",
            "Invalid cvc",
            value => valid.cvv(value).isValid
        )
        .nullable()
    ,
    //-------end---------

    /*quarterly_fees_autodebit_authorization_signed_document:Yup
    .string()
    .nullable()
    .required("Please sign the document")*/
});

export const formInitialValues = {
    // Commission
    payout_mode: 1,

    commission_transit_number: null,
    commission_institution_no: null,
    commission_account_no: null,
    commission_void_cheque_doc: "",

    commission_is_same_as_personal_address: 1,
    commission_mailing_address_data_address_1: "",
    commission_mailing_address_data_address_2: "",
    commission_mailing_address_data_city: "",
    commission_mailing_address_data_province: "",
    commission_mailing_address_data_postal_code: "",

    is_authorized_agreement: 0,
    // ---- end ---

    //  Billing
    billing_method: 1,

    billing_transit_number: null,
    billing_institution_no: null,
    billing_account_no: null,

    billing_void_cheque_doc: "",

    billing_credit_card_no: null,
    billing_expire_date: null,
    billing_cvc: null,

    authorized_agreement_doc: "",

    quarterly_fees_autodebit_authorization_signed_document: "",

    credit_card_no: null,
    expire_date: null,
    cvc: null,
};


export const mapBillingData = (billingCommissionInfo: any) => {
    let sameAsPersonalAddress = billingCommissionInfo.commission_is_same_as_personal_address;
    let addressDetails = {
        commission_mailing_address_data_address_1: sameAsPersonalAddress
            ? billingCommissionInfo.address_1
            : (billingCommissionInfo.commission_mailing_address_data ? billingCommissionInfo.commission_mailing_address_data.address_1 : ""),
        commission_mailing_address_data_address_2: sameAsPersonalAddress
            ? billingCommissionInfo.address_2
            : (billingCommissionInfo.commission_mailing_address_data ? billingCommissionInfo.commission_mailing_address_data.address_2 : ""),
        commission_mailing_address_data_city: sameAsPersonalAddress
            ? billingCommissionInfo.city
            : (billingCommissionInfo.commission_mailing_address_data ? billingCommissionInfo.commission_mailing_address_data.city : ""),
        commission_mailing_address_data_province: sameAsPersonalAddress
            ? billingCommissionInfo.province
            : (billingCommissionInfo.commission_mailing_address_data ? billingCommissionInfo.commission_mailing_address_data.province : ""),
        commission_mailing_address_data_postal_code: sameAsPersonalAddress
            ? billingCommissionInfo.postal_code
            : (billingCommissionInfo.commission_mailing_address_data ? billingCommissionInfo.commission_mailing_address_data.postal_code : ""),
    }
    return {
        ...formInitialValues,
        ...billingCommissionInfo,
        ...addressDetails,
        is_authorized_agreement: parseInt(billingCommissionInfo.is_authorized_agreement || '0'),
        billing_method: billingCommissionInfo.billing_method === null ? 1 : billingCommissionInfo.billing_method
    }

}


export const generatePayload = async (values: any) => {
    let formValues = { ...values };
    if (formValues.payout_mode == PAYMENT_MODE_ENUM.ELECTRONIC_FUND_TRANSFER) {
        delete formValues.commission_is_same_as_personal_address;
        delete formValues.commission_mailing_address_data_address_1;
        delete formValues.commission_mailing_address_data_address_2;
        delete formValues.commission_mailing_address_data_city;
        delete formValues.commission_mailing_address_data_province;
        delete formValues.commission_mailing_address_data_postal_code;
    } else if (!formValues.commission_is_same_as_personal_address) {
        formValues.commission_mailing_address_data = {
            address_1: formValues.commission_mailing_address_data_address_1,
            address_2: formValues.commission_mailing_address_data_address_2,
            city: formValues.commission_mailing_address_data_city,
            province: formValues.commission_mailing_address_data_province,
            postal_code: formValues.commission_mailing_address_data_postal_code,
        }
        delete formValues.commission_mailing_address_data_address_1;
        delete formValues.commission_mailing_address_data_address_2;
        delete formValues.commission_mailing_address_data_city;
        delete formValues.commission_mailing_address_data_province;
        delete formValues.commission_mailing_address_data_postal_code;
    }

    delete formValues.address_1;
    delete formValues.address_2;
    delete formValues.city;
    delete formValues.postal_code;
    delete formValues.province;

    let commission_void_cheque_doc = await uploadFile(values.commission_void_cheque_doc, "cheque");
    let billing_void_cheque_doc = await uploadFile(values.billing_void_cheque_doc, "cheque");
    return {
        ...formValues,
        commission_void_cheque_doc: commission_void_cheque_doc,
        billing_void_cheque_doc: billing_void_cheque_doc,
        commission_is_same_as_personal_address: Number(values.commission_is_same_as_personal_address)
    }
}
/**
 * Post Api for saving the contact details
 * @param values - payload of contact details form
 * @param currentFormKey - current form key
 */
export const submitBillingDetailsForm = async (
    values: any,
    currentFormKey: string
) => {
    try {
        const url = currentFormKey;
        const res = await services.post(
            `${AGENT_ONBOARDING_BASE_URL}/${url}`,
            values
        );
        await updateUserStore()
        return res;
    } catch (error) {
        return error;
    }
};


export const checkBillingDocuSignEnvolpe = async (envId: string, values: any) => {

    const res = (await checkDocuSign(
        {
            envelope_id:
                values.quarterly_fees_autodebit_authorization_envelope_id || envId,
            type: "agent-agent-agreement_billing-commission",
            event: "signing_complete",
        },
        "/billing-commission/docusign"
    )) as { success: boolean; data: string };
    return res;

};
