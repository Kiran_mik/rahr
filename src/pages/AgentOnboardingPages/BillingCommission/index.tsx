import { BorderBtn, Primary } from "@components/Buttons";
import { Flex } from "@components/Flex";
import { H2Typography } from "@components/Typography/Typography";
import useAgentSteps from "@hooks/useAgentSteps";
import { getErrors } from "@utils/helpers";
import { Formik } from "formik";
import React, { useEffect, useState } from "react";
import { STEPS } from "../constants";
import { FormHeader, InnerContainer } from "../style";
import Billing from "./Billing";
import CommissionPayout from "./CommissionPayout";
import {
  formInitialValues,
  generatePayload,
  mapBillingData,
  submitBillingDetailsForm,
  validationSchema,
} from "./helper";

function BillingCommission(props: { path: string }) {
  const [setForm, setStep, form] = useAgentSteps();
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const [billingAndCommissionFormInitialValues, setIntialFormValues] = useState<
    any
  >(formInitialValues);

  const currentStepObject = STEPS[form.currentStep];
  const currentFormKey = currentStepObject && currentStepObject.storeKey;
  const billingCommissionInfo: any =
    (currentStepObject && form[currentStepObject.storeKey]) || {};
  let formik: any = null;

  useEffect(() => {
    return () => {
      setForm(currentFormKey, { ...billingCommissionInfo, stepId: null });
    };
  }, []);

  useEffect(() => {
    if (billingCommissionInfo.stepId) {
      setIntialFormValues(mapBillingData(billingCommissionInfo));
    }
  }, [billingCommissionInfo.stepId]);

  const onSubmitHandler = async (values: any) => {
    try {
      setIsLoading(true);
      let payload = await generatePayload(values);
      const res = (await submitBillingDetailsForm(
        {
          ...payload,
          is_authorized_agreement: payload.is_authorized_agreement ? 1 : 0,
        },
        currentStepObject.key
      )) as any;
      setIsLoading(false);
      if (res.error && res.error.error) {
        const errors = getErrors(res.error.error);

        formik.setErrors(errors);
        return;
      }
      setStep(form.currentStep + 1);
    } catch (err) {
         //@ts-ignore
		if (err.response.data.error) {
         setIsLoading(false);
      }
    }
  };

  const joinAsPrec = billingAndCommissionFormInitialValues.join_as_prec;
  return (
    <>
      <FormHeader>
        <Flex>
          <H2Typography>
            {joinAsPrec ? `PREC ` : ""}Billing & Commission
          </H2Typography>
        </Flex>
      </FormHeader>
      <InnerContainer>
        <Formik
          initialValues={billingAndCommissionFormInitialValues}
          onSubmit={(values) => {
            onSubmitHandler(values);
          }}
          enableReinitialize={true}
          validationSchema={validationSchema}
        >
          {(formikProps) => {
            const { handleSubmit, errors } = formikProps;
            formik = formikProps;
             return (
              <>
                <CommissionPayout formik={formik} joinAsPrec={joinAsPrec} />
                <Billing formik={formik} joinAsPrec={joinAsPrec} />
                <Flex className="footerbtn">
                  <Primary
                    text="Proceed to the next step"
                    className="submitbtn"
                    style={{ marginRight: "10px" }}
                    onClick={handleSubmit}
                    isLoading={isLoading}
                  />
                  <BorderBtn
                    onClick={() => {
                      setStep(form.currentStep - 1);
                    }}
                    className="borderbtn"
                    text="Previous step"
                  />
                </Flex>
              </>
            );
          }}
        </Formik>
      </InnerContainer>
    </>
  );
}

export default BillingCommission;
