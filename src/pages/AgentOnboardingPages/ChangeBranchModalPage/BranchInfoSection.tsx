import { ArrowLeftOutlined } from "@ant-design/icons";
import { BorderBtn, Primary } from "@components/Buttons";
import { Flex } from "@components/Flex";
import { H3, H4 } from "@components/Typography/Typography";
import { navigate } from "@reach/router";
import { Divider, Spin } from "antd";
import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import useSWR from "swr";
import { loadSelectedBranchDetails } from "./helper";

function BranchInfoSection(props: any) {
  const selectedBranch = useSelector(
    (state: any) => state.rawData.selectedBranchByAgent || {}
  );

  const { data, error } = useSWR(
    selectedBranch.id,
    loadSelectedBranchDetails,

    {
      revalidateOnFocus: false,
    }
  );
  const isValidating = !data;
  const branchInfoRes = data as any;
   const {profile_photo,first_name,last_name,short_note}=branchInfoRes || {};

  return (
    <Flex direction="column" style={{ width: 600 }}>
      <Flex alignItems={"center"} right={20}>
        <BorderBtn
          icon={<ArrowLeftOutlined />}
          onClick={() => {
            navigate(props.backPath);
          }}
        />
        <Flex flex={1} left={10}>
          <H3 text={"Branch informations"} />
        </Flex>
        <Primary
          style={{ width: 200, fontSize: 12 }}
          isDisable={!selectedBranch.id}
          onClick={()=>{
            navigate(props.backPath);
          }}
          text={"This is your home branch"}
        />
      </Flex>
      <Divider />
      {isValidating ? (
        <Spin />
      ) : (
        <>
          <Flex top={20}>
            <H3 text={"Branch manager"} />
          </Flex>
          <Flex top={10} right={20}>
            <Flex>
              <img
                src={profile_photo}
                style={{ height: 300, width: 190 }}
              />
            </Flex>
            <Flex direction="column" left={30}>
              <H4 text={`Manager: ${first_name} ${last_name}`} />
              <div style={{ width: 340 }}>
                <p>
                {short_note}
                </p>
                 
              </div>
            </Flex>
          </Flex>
        </>
      )}
    </Flex>
  );
}

export default BranchInfoSection;
