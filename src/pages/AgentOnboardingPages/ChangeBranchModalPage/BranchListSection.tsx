import React, { useEffect } from "react";
import { Spin } from "antd";
import useSWR from "swr";
import { Flex } from "@components/Flex";
import { SearchBarOuline } from "@components/SerachBar";
import { H3 } from "@components/Typography/Typography";
import { getBranchList } from "@pages/AgentManagmentPages/NewAgentPage/helper";
import { useSelector } from "react-redux";
import { updateRawData } from "@utils/helpers";
import { useLocation } from "@reach/router";

function BranchListSection() {
  const location = useLocation();
  const selectedBranch = useSelector(
    (state: any) => state.rawData.selectedBranchByAgent || {}
  );

  useEffect(() => {
    const { state } = location as { state: any };
    updateRawData({ selectedBranchByAgent: state.branch });
  }, []);

  const { data, error } = useSWR(
    process.env.REACT_APP_BASE_URL_original,
    getBranchList,
    {
      revalidateOnFocus: false,
    }
  );
  const branches = data || [];
  const isLoading = !data;
  return (
    <>
      <Flex
        direction="column"
        style={{ backgroundColor: "white", padding: 20, width: 332 }}
      >
        <H3 text={"RAHR Branchs"} />
        <span style={{ fontSize: 12, fontWeight: "bold", marginTop: 10 }}>
          Find you local branch
        </span>

        <Flex top={5}>
          <SearchBarOuline
            placeholder="Type your address"
            style={{ width: "100%" }}
          />
        </Flex>
        <Flex direction="column" top={20}>
          <H3 text={"List of branches"} />
          {isLoading ? (
            <Spin />
          ) : (
            <Flex direction="column" top={10}>
              {branches.map((i: any) => {
                const isSelectedBranch = i.id === selectedBranch.id;
                return (
                  <Flex
                    style={{
                      padding: 5,
                      backgroundColor: isSelectedBranch ? "#9792E3" : "#F0F0F6",
                      borderRadius: 5,
                      cursor: "pointer",
                    }}
                    onClick={() => {
                      updateRawData({ selectedBranchByAgent: i });
                    }}
                    top={10}
                    justifyContent={"center"}
                  >
                    <span
                      style={{ color: isSelectedBranch ? "white" : "black" }}
                    >
                      {i.branch_name}
                    </span>
                  </Flex>
                );
              })}
            </Flex>
          )}
        </Flex>
      </Flex>
    </>
  );
}

export default BranchListSection;
