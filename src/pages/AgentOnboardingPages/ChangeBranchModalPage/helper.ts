import services from '@services/index';
import { message } from 'antd';

export const loadSelectedBranchDetails = async (branchId: string) => {
    try {
        if (branchId) {
            const res = await services.get(`v1/branches/${branchId}`, process.env.REACT_APP_BASE_URL) as { data: { data: any } }
            const branchInfo = res.data.data;
            const managerId = branchInfo.branch_manager_users && branchInfo.branch_manager_users[0] && branchInfo.branch_manager_users[0].user_id
            if (managerId) {
                const managerInfoRes = await services.get(`v1/staffs/${managerId}`, process.env.REACT_APP_BASE_URL) as { data: { data: any } }
                const managerInfo = managerInfoRes.data.data
                return { ...managerInfo, ...branchInfo, status: 200 }
            }

            return null
        }
    } catch (error) {
        message.error('Something went wrong')
        return error
    }
}