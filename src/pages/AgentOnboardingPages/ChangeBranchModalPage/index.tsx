import { ArrowLeftOutlined } from "@ant-design/icons";
import { BorderBtn, Primary } from "@components/Buttons";
import { Flex } from "@components/Flex";
import FullScreenModal from "@components/FullScreenModal";
import { SearchBarOuline } from "@components/SerachBar";
import { H3 } from "@components/Typography/Typography";
import { navigate } from "@reach/router";
import { Divider } from "antd";
import React from "react";
import BranchInfoSection from "./BranchInfoSection";
import BranchListSection from "./BranchListSection";

function ChangeBranchModal(props: any) {
  return (
    <>
      <FullScreenModal style={{ backgroundColor: "#F8F8FC" }}>
        <Flex flex={1} justifyContent={"center"}>
          <BranchInfoSection {...props} />
          <BranchListSection />
        </Flex>
      </FullScreenModal>
    </>
  );
}

export default ChangeBranchModal;
