import React from "react";
import { Checkbox } from "@components/Checkbox";
import { Block, Flex } from "@components/Flex";
import {
  H2Typography,
  H3Typography,
  H4Typography,
} from "@components/Typography/Typography";
import { Radio } from "antd";
import { CharityProps } from "./helper";
import Error from "@components/Error";
import { ContentWrapper } from "../style";

function AuthorizationAgreementSection(props: CharityProps) {
  const { values, errors, touched } = props.formik;
  const { onChange } = props;
  return (
    <ContentWrapper className="bordernone space-bottom-0">

      <H2Typography>Authorization agreement</H2Typography>
      <Flex direction="column" top={16}>
        <Checkbox
          id={"authorization_agreement_1"}
          name="I hereby agree to enroll in the Habitat for Humanity Program and/or Holland Bloorview 
        Program and authorize Right at Home Realty Inc. to deduct the amount indicated below."
          onChange={onChange}
          error={
            touched.authorization_agreement_1 &&
            errors.authorization_agreement_1
          }
          checked={values.authorization_agreement_1}
        />

      </Flex>
      <Flex direction="column" top={16}>
        <Checkbox
          id={"authorization_agreement_2"}
          name="I understand that my contributions will be reflected in my T4A as a sale expense, 
        and are fully deductible."
          onChange={onChange}
          error={
            touched.authorization_agreement_2 &&
            errors.authorization_agreement_2
          }
          checked={values.authorization_agreement_2}
        />

      </Flex>
      <Flex bottom={10} top={16} direction="column">
        <H4Typography>
          Please select which Program(s) you would like to enroll in:
        </H4Typography>
        <Flex top={10}>
          <Checkbox
            id={"enroll_program_1"}
            onChange={onChange}
            name="Habitat for Humanity program"
            error={touched.enroll_program_1 && errors.enroll_program_1}
            checked={values.enroll_program_1}
          />
        </Flex>
        <Flex top={16}>
          <Checkbox
            id={"enroll_program_2"}
            onChange={onChange}
            name="Holland Bloorview Kids Rehabilitation Hospital program"
            error={touched.enroll_program_2 && errors.enroll_program_2}
            checked={values.enroll_program_2}
          />
        </Flex>
        {touched.enroll_program_2 &&
          errors.enroll_program_2 &&
          (touched.enroll_program_1 && errors.enroll_program_1) && (
            <Flex top={10}>
              <Error
                error={
                  "Please check  Habitat for Humanity program or Holland Bloorview Kids Rehabilitation Hospital program"
                }
              />
            </Flex>
          )}
      </Flex>
      <Flex top={16} direction="column">
        <H4Typography>Please select contribution per deal</H4Typography>
        <Flex top={10}>
          <Radio.Group
            name="deal_contribution_type"
            id={"deal_contribution_type"}
            onChange={onChange}
            value={values["deal_contribution_type"]}
          >
            <Radio value={1}>$5 per selected program</Radio>
            <Radio value={2}>Custom amount</Radio>
          </Radio.Group>
        </Flex>
      </Flex>
    </ContentWrapper>
  );
}

export default AuthorizationAgreementSection;
