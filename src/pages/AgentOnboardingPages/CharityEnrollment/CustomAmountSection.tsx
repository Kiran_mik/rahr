import { Block, Flex } from "@components/Flex";
import { InputField } from "@components/Input/Input";
import { FormLableTypography } from "@components/Typography/Typography";
import React from "react";
import { CharityProps } from "./helper";

function CustomAmountSection(props: CharityProps) {
  const { values, errors, touched } = props.formik;
  const { onChange } = props;
  console.log(errors, values);
  return (
    <Flex top={20}>
      <Block style={{ marginRight: 20, flex: 1 }}>
        <FormLableTypography>
          Amount for Habitat for Humanity<span>*</span>
        </FormLableTypography>
        <InputField
          prefix={"$"}
          id={"custom_amount_habitat_for_humanity"}
          name={"custom_amount_habitat_for_humanity"}
          type="number"
          onChange={(e) => {
            let valu = e.target.value;

            if (!Number(valu) && valu.length) {
              return;
            }
            onChange && onChange(e);
          }}
          value={values["custom_amount_habitat_for_humanity"]}
          error={
            touched.custom_amount_habitat_for_humanity &&
            errors.custom_amount_habitat_for_humanity
          }
        />
      </Block>
      <Block style={{ marginRight: 20, flex: 1 }}>
        <FormLableTypography>
          Amount for Ottawa Network for Education<span>*</span>
        </FormLableTypography>
        <InputField
          prefix={"$"}
          type="number"
          id={"custom_amount_for_ottawa_network_for_education"}
          name={"custom_amount_for_ottawa_network_for_education"}
          onChange={(e) => {
            let valu = e.target.value;

            if (!Number(valu) && valu.length) {
              return;
            }
            onChange && onChange(e);
          }}
          value={values["custom_amount_for_ottawa_network_for_education"]}
          error={
            touched.custom_amount_for_ottawa_network_for_education &&
            errors.custom_amount_for_ottawa_network_for_education
          }
        />
      </Block>
    </Flex>
  );
}

export default CustomAmountSection;
