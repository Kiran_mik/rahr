import React from "react";
import {
  HHToronto,
  Missisauga,
  HHOttawa,
  HollandBloorview,
  BlueNotification,
} from "@assets/index";
import { ImageContainer, NoteBlock } from "./style";
import { H5Typography } from "@components/Typography/Typography";
import { Transparent } from "@components/Buttons/index";
import { ContentWrapper, SquareRadioWrapper } from "../style";
import { Radio, Row } from "antd";

import { CharityProps } from "./helper";

function HabitantSection(props: CharityProps) {
  const { handleChange, values, errors } = props.formik;
  return (
    <>
      <ContentWrapper>
        <ImageContainer>
          <img src={HHToronto} />
          <img src={Missisauga} />
          <img src={HHOttawa} />
          <img src={HollandBloorview} />
        </ImageContainer>
        <H5Typography style={{ fontWeight: 400, fontSize: 14 }}>
          These deductions will commence as of this date as my contribution to
          Habitat for Humanity, Toronto, Habitat for Humanity, Halton-Missisauga
          and/or Holland Bloorview Kids Rehabilitation Hospital.
        </H5Typography>
        <Transparent
          text="Learn more"
          className="borderbtn"
          style={{ marginTop: "13px" }}
        />
      </ContentWrapper>
      <ContentWrapper>
        <H5Typography>
          Do you want to make a contribution to the following charities?
        </H5Typography>
        <SquareRadioWrapper>
          <Row className="squareRadioBtn">
            <Radio.Group
              id={"is_want_to_join_charity"}
              name={"is_want_to_join_charity"}
              value={values["is_want_to_join_charity"]}
              onChange={handleChange}
            >
              <Radio value={1}>Yes</Radio>
              <Radio value={0}>No</Radio>
            </Radio.Group>
          </Row>
        </SquareRadioWrapper>
        <NoteBlock>
          <img src={BlueNotification} />
          <span className="redNote">**NOTE: </span>You can withdraw from any of
          these charity program(s) anytime you want by contacting your branch
          office.
        </NoteBlock>
      </ContentWrapper>
    </>
  );
}

export default HabitantSection;
