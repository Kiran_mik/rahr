import * as Yup from "yup";

import services from "@services/index";
import { STEPS, AGENT_ONBOARDING_BASE_URL } from "../constants";
import { updateUserStore } from "@services/UserService";

export interface CharityProps {
  formik: any;
  onChange?: (e: any) => void;
}
export const validationSchema = Yup.object({
  is_want_to_join_charity: Yup.number(),
  deal_contribution_type: Yup.number(),
  authorization_agreement_1: Yup.boolean().notRequired(),
  authorization_agreement_2: Yup.boolean().notRequired(),
  enroll_program_1: Yup.boolean(),
  enroll_program_2: Yup.boolean(),
  custom_amount_habitat_for_humanity: Yup.string().when(
    ["deal_contribution_type"],
    {
      is: (deal_contribution_type: number) => deal_contribution_type === 2,
      then: Yup.string().required("Please enter amount"),
      otherwise: Yup.string().notRequired(),
    }
  ),
  custom_amount_for_ottawa_network_for_education: Yup.string().when(
    ["deal_contribution_type"],
    {
      is: (deal_contribution_type: number) => deal_contribution_type === 2,
      then: Yup.string().required("Please enter amount"),
      otherwise: Yup.string().notRequired(),
    }
  ),
});
export const formInitialValues = {
  is_want_to_join_charity: 1,
  deal_contribution_type: 1,
  authorization_agreement_1: "",
  authorization_agreement_2: "",
  enroll_program_1: 0,
  enroll_program_2: 0,
  custom_amount_habitat_for_humanity: "",
  custom_amount_for_ottawa_network_for_education: "",
};

export const submitCharityForm = async (
  values: any,
  currentFormKey: string
) => {
  try {
    const url = currentFormKey;
    const payload = {
      ...values,
      is_want_to_join_charity: values.is_want_to_join_charity ? 1 : 0,
      deal_contribution_type: values.deal_contribution_type,
      authorization_agreement_1: values.authorization_agreement_1 ? 1 : 0,
      authorization_agreement_2: values.authorization_agreement_1 ? 1 : 0,
      enroll_program_1: values.enroll_program_1 ? 1 : 0,
      enroll_program_2: values.enroll_program_2 ? 1 : 0,
      draft:0
    };
    const res = await services.post(
      `${AGENT_ONBOARDING_BASE_URL}/${url}`,
      payload
    );
    await updateUserStore()
    return res;
  } catch (error) {
    return false;
  }
};
