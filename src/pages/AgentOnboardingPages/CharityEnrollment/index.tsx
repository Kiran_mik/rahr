import React, { useEffect, useState } from "react";
import { ContentWrapper, FormHeader, InnerContainer } from "../style";
import { Flex } from "@components/Flex";
import { H3Typography } from "@components/Typography/Typography";
import { FlexBox } from "@pages/UserManagementPages/StaffMembers/NewStaff/style";
import HabitantSection from "./HabitantSection";
import AuthorizationAgreementSection from "./AuthorizationAgreementSection";
import { BorderBtn, Primary } from "@components/Buttons";
import { Formik } from "formik";
import {
  validationSchema,
  formInitialValues,
  submitCharityForm,
} from "./helper";
import CustomAmountSection from "./CustomAmountSection";
import useAgentSteps from "@hooks/useAgentSteps";
import { STEPS } from "../constants";

function CharityEnrollment(props: { path: string }) {
  const [setForm, setStep, form] = useAgentSteps();
  const currentStepObject = STEPS[form.currentStep];
  const currentFormKey = STEPS[form.currentStep] && STEPS[form.currentStep].storeKey;
  const charityInfo: any = form[currentFormKey] || {};

  const [isLoading, setIsLoading] = useState(false);
  const [charityDetailsFormInitialValues, setIntialFormValues] = useState(
    formInitialValues
  );

  useEffect(() => {
    return () => {
      setForm(currentFormKey, { ...charityInfo, stepId: null });
    };
  }, []);

  useEffect(() => {
    if (charityInfo.stepId) {
      setIntialFormValues({
        ...charityInfo,
        custom_amount_for_ottawa_network_for_education:
          charityInfo.custom_amount_for_ottawa_network_for_education || "",

        custom_amount_habitat_for_humanity:
          charityInfo.custom_amount_habitat_for_humanity || "",
        deal_contribution_type: charityInfo.deal_contribution_type || 1,
      });
    }
  }, [charityInfo.stepId]);

  const onSubmit = async (values: any) => {
    setIsLoading(true);
    if (values.stepId) {
      delete values.stepId;
    }
    const res = await submitCharityForm(values, currentStepObject.key);
    setIsLoading(false);
    if (res) {
      setForm(currentFormKey, values);
      setStep(form.currentStep + 1);
    }
  };

  return (
    <>
      <FormHeader>
        <Flex>
          <H3Typography>Charity Enrollment</H3Typography>
        </Flex>
      </FormHeader>
      <InnerContainer>
        <Formik
          initialValues={charityDetailsFormInitialValues}
          onSubmit={onSubmit}
          validationSchema={validationSchema}
          enableReinitialize={true}
        >
          {(props) => {
            const {
              values,
              handleSubmit,
              setTouched,
              touched,
              handleChange,
              errors,
            } = props;
            const setFieldsTouched = (field: string) => {
              const fieldIndex = Object.keys(values).indexOf(field);
              const touchedObject: any = { ...touched };
              Object.keys(values).forEach((key: string, index: number) => {
                if (index <= fieldIndex) {
                  touchedObject[key] = true;
                }
              });
              setTouched(touchedObject);
            };
            return (
              <>
                <HabitantSection formik={props} />

                {values.is_want_to_join_charity === 1 && (
                  <>
                    {" "}
                    <AuthorizationAgreementSection
                      formik={props}
                      onChange={(e: any) => {
                        setFieldsTouched(e.target.id);
                        handleChange(e);
                      }}
                    />
                    {values.deal_contribution_type === 2 && (
                      <CustomAmountSection
                        formik={props}
                        onChange={(e: any) => {
                          setFieldsTouched(e.target.id);
                          handleChange(e);
                        }}
                      />
                    )}{" "}
                  </>
                )}

                <FlexBox className="footerbtn">
                  <Primary
                    text="Proceed to the next step"
                    className="submitbtn"
                    style={{ marginRight: "10px" }}
                    onClick={handleSubmit}
                    isLoading={isLoading}
                  />
                  <BorderBtn
                    onClick={() => {
                      setStep(form.currentStep - 1);
                    }}
                    className="borderbtn"
                    text="Previous step"
                  />
                </FlexBox>
              </>
            );
          }}
        </Formik>
      </InnerContainer>
    </>
  );
}

export default CharityEnrollment;
