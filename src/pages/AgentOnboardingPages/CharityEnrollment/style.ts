import styled from "styled-components";

export const ImageContainer = styled.div`
  display: inline-block;
  img{
    margin-right:20px;
  }
`;

export const NoteBlock = styled.div`
  background-color: #e6f7ff;
  border: 1px solid #8fd4ff;
  padding: 9px 17px 9px 17px;
  border-radius: 8px;
  .redNote {
    color: #f5222d;
  }
`;
