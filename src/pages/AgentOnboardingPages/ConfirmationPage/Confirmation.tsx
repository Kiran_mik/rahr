import React from "react";
import { AgentConfirmation } from "@assets/index";
import ConfirmationScreen from "@components/AgentConfirmationScreen";
import { Description } from "@components/Typography/Typography";

function Confirmation() {
  return (
    <>
      <ConfirmationScreen
        subHeader="Thank you!"
        rightImage={AgentConfirmation}
      >
        <Description
          className="bottom-16"
          text="Your request to become an agent for “Right at Home Realty, Inc” has been submitted. We will review your request in __  - __ hours and will get back to you. "
        />
        <Description 
          className="bottom-16"
          text="If you have any questions or concerns, please contact our branch at: (406) 555-0120"
        />
        <Description
          className="bottom-16"
          text="Meanwhile, you can visit our social media to be connected with us!"
        />
      </ConfirmationScreen>
    </>
  );
}

export default Confirmation;
