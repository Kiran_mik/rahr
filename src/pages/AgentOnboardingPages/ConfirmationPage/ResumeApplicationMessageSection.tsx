import { AgentConfirmation } from "@assets/index";
import ConfirmationScreen from "@components/AgentConfirmationScreen";
import { Flex } from "@components/Flex";
import { Description } from "@components/Typography/Typography";
import AccountApproved from "@pages/Dashboard/Status/AccountApproved";
import AccountParked from "@pages/Dashboard/Status/AccountParked";
import AccountRegistration from "@pages/Dashboard/Status/AccountRegistration";
import AccountTermination from "@pages/Dashboard/Status/AccountTermination";
import { Link } from "@reach/router";
import React from "react";
import { STEPS } from "../constants";

function ResumeApplicationMessageSection(props: {
  stpesNotCompleted: Array<number>;
}) {
  const { stpesNotCompleted } = props;
  return (
    <>
      <ConfirmationScreen
        subHeader="Please finish your application"
        rightImage={AgentConfirmation}
      >
        <Description
          className="bottom-16"
          style={{ color: "black" }}
          text="We have carefully reviewed your application and found some information that needs to be updated on the screen(s) indicated below: "
        />
        <Flex direction="column" bottom={10}>
          {stpesNotCompleted.map((i: number) => {
            return (
              <Link to={`/agent-onboard/${STEPS[i].key}/`}>
                .
                <span style={{ textDecoration: "underline", marginLeft: 5 }}>
                  {STEPS[i].title}
                </span>
              </Link>
            );
          })}
        </Flex>
        <Description
          className="bottom-16"
          style={{ color: "black" }}
          text="Please carefully review the Branch Manager’s comments on the indicated screen(s) and contact your local branch if you have any questions."
        />
      </ConfirmationScreen>
      <AccountRegistration />
      <AccountApproved />
      <AccountTermination />
      <AccountParked />
    </>
  );
}

export default ResumeApplicationMessageSection;
