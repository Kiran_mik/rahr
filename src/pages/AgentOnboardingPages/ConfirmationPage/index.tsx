import React, { useState } from "react";
import { Flex } from "@components/Flex";
import { H3Typography } from "@components/Typography/Typography";
import { FormHeader, InnerContainer } from "../style";
import Confirmation from "./Confirmation";
import { Primary } from "@components/Buttons";
import services from "@services/index";
import { AGENT_ONBOARDING_BASE_URL, STEPS, STEPS_ENUM } from "../constants";
import { useDispatch, useSelector } from "react-redux";
import { userActions } from "@store/userProfileReducer";
import { USER_STATUS_ENUM } from "@constants/userConstants";
import ResumeApplicationMessageSection from "./ResumeApplicationMessageSection";
import { navigate } from "@reach/router";

function ConfirmationPage(props: { path: string }) {
  const [isLoading, setIsLoading] = useState(false);
  const user = useSelector((state: { user: any }) => state.user);
  const { onBoardingStepsStatus } = user;
  const stpesNotCompleted = [1, 2, 3, 4, 5, 6, 7].filter(
    (i: any) => !onBoardingStepsStatus[i]
  );

  const dispatch = useDispatch();

  const submitApplication = async () => {
    if (isActionRequired) {
      navigate(`/agent-onboard/${STEPS[stpesNotCompleted[0]].key}/`);
      return;
    }
    setIsLoading(true);
    try {
      const payload = {};
      const res: any = await services.post(
        `${AGENT_ONBOARDING_BASE_URL}/confirmation`,
        payload
      );
      if (res && res.success) {
        dispatch(
          userActions.setUser({
            ...user,
            ...{
              status: USER_STATUS_ENUM.PENDING_REVIEW,
              currentOnboardingStep: STEPS_ENUM.REVIEW,
            },
          })
        );
      }
      setIsLoading(false);
      return res;
    } catch (error) {
      console.log(error);
      setIsLoading(false);
      return false;
    }
  };
  const isActionRequired = stpesNotCompleted.length;
  return (
    <>
      <FormHeader>
        <Flex>
          <H3Typography>
            {isActionRequired ? "Actions required" : "Confirmation"}
          </H3Typography>
        </Flex>
      </FormHeader>
      <InnerContainer>
        {isActionRequired ? (
          <ResumeApplicationMessageSection
            stpesNotCompleted={stpesNotCompleted}
          />
        ) : (
          <Confirmation />
        )}
        <Flex className="footerbtn">
          <Primary
            onClick={submitApplication}
            text={
              isActionRequired
                ? "Resume your application"
                : "Finish application"
            }
            isLoading={isLoading}
            className="submitbtn"
            style={{ backgroundColor: isActionRequired ? "#F58A07" : "" }}
          />
        </Flex>
      </InnerContainer>
    </>
  );
}

export default ConfirmationPage;
