import { H5 } from '@components/Typography/Typography';
import React from 'react'
import { ContactWrapper, ContactText } from "./style";
import ContactImage from "@assets/contactus.svg"
import { Flex } from '@components/Flex';
import EmailIcon from '@assets/email.svg';
import PhoneIcon from '@assets/phone.svg'
export default function ContactDetails() {
    return (
        <ContactWrapper>
            <H5 text="Have questions? " />
            <span>Contract our branch!</span>
            <img src={ContactImage} alt="" className="margin-16" style={{ margin: "15px auto", width: "67px" }} />
            <Flex justifyContent="space-between">
                <ContactText><img src={PhoneIcon} alt=""/>905-565-9200</ContactText>
                <ContactText><img src={EmailIcon} alt=""/>Email</ContactText>
            </Flex>
        </ContactWrapper>
    )
}
