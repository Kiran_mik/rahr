import React from "react";
import { Row, Col } from "antd";
import { H3, FormLableTypography } from "@components/Typography/Typography";
import { InputField } from "@components/Input/Input";
import SelectInput from "@components/Select";
import { ContentWrapper } from "../style";
import { Flex } from "@components/Flex";
import { Select } from "antd";
const { Option } = Select;

const AdditionalInformationSection: React.FC<{
  formik: any;
  hearAboutList: any;
}> = (props: any) => {
  let { hearAboutList, formik } = props;
  const { errors, values, handleChange, touched, setFieldValue } = formik;

  return (
    <ContentWrapper className="bordernone">
      <Flex direction="column">
        <H3
          text="Additional information"
          className="space-bottom-16 font-500"
        />
      </Flex>
      <Row gutter={16}>
        <Col span={12}>
          <FormLableTypography>
            How did you hear about RAHR<span>*</span>
          </FormLableTypography>
          <SelectInput
            id="how_did_you_hear_about_rahr"
            name="how_did_you_hear_about_rahr"
            placeholdertitle="Select from a list"
            error={errors.how_did_you_hear_about_rahr}
            value={values.how_did_you_hear_about_rahr}
            onChange={(e: any) => {
              setFieldValue("how_did_you_hear_about_rahr", e);
            }}
          >
            {hearAboutList.map((item: any) => {
              return (
                <>
                  <Option value={item.id}>{item.status}</Option>
                </>
              );
            })}
          </SelectInput>
        </Col>
        {values.how_did_you_hear_about_rahr &&
          values.how_did_you_hear_about_rahr == 10 && (
            <Col span={12}>
              <FormLableTypography>
                Please provide details<span>*</span>
              </FormLableTypography>
              <InputField
                id="detail_hear_about_rahr"
                name="detail_hear_about_rahr"
                onChange={handleChange}
                value={values.detail_hear_about_rahr}
                error={
                  touched.detail_hear_about_rahr &&
                  errors.detail_hear_about_rahr
                }
              />
            </Col>
          )}
      </Row>
    </ContentWrapper>
  );
};

export default AdditionalInformationSection;
