import React, { useEffect, useState, useRef } from "react";
import { Row, Col } from "antd";
import { FormLableTypography, H3 } from "@components/Typography/Typography";
import { InputField } from "@components/Input/Input";
import { GoogleAutoComplete } from "@components/GoogleAutoComplete";

import { Flex } from "@components/Flex";
import { ContentWrapper } from "../style";
import MapContainer from "@components/MapContainer/MapContainer";
import SelectInput from "@components/Select";
import { provinceList } from "./helper";
import { Select } from "antd";
import { getAddressComps, getLocationByPlaceId } from "@utils/googlemaphealper";
const { Option } = Select;
interface LatLongType {
  [index: number]: {
    lat: number;
    lng: number
  }
}
const AddressDetailsSection: React.FC<{ formik: any }> = (props: any) => {
  const { errors, values, handleChange, setFieldValue, touched } = props.formik;
  const ref: any = useRef(null);
  const [mapWidth, setMapWidth] = useState(0);
  const [coord, setCoord] = useState<LatLongType>([])
  const [center, setCenter] = useState({
    lat: 43.5931073,
    lng: -79.6422539
  })

  useEffect(() => {
    setMapWidth(ref.current.offsetWidth);
  }, []);
  const onPlaceSelect = async (placeId: string) => {
    const place:any = await getLocationByPlaceId(placeId);
    if (place) {
      const locObj = getAddressComps(place);
      if (locObj.city.length > 0) {
        const coordinates = ({ lat: place.geometry.location.lat(), lng: place.geometry.location.lng() });
        // console.log("coordinates", coordinates);
        // console.log("address", locObj);
        setFieldValue("address1", locObj.formattedAddress || '')
        setFieldValue("address2", locObj.addressLine_2 || '')
        setFieldValue("province", locObj.state || '')
        setFieldValue("city", locObj.city || '')
        setFieldValue("postal_code", locObj.postalCode || '')
        setCoord([coordinates])
        setCenter({ lat: place.geometry.location.lat(), lng: place.geometry.location.lng() })
      }
    }
  };

  return (
    <ContentWrapper>
      <Flex direction="column">
        <H3 text="Home address details" className="space-bottom-16 font-500" />
      </Flex>
      <Row gutter={16}>
      <div id="map"></div>
        <Col span={16}>
          <FormLableTypography>
            Address line 1<span>*</span>
          </FormLableTypography>
          <GoogleAutoComplete
            apiKey={process.env.REACT_APP_GOOGLEAPI_KEY}
            defaultVal={''}
            id="address1"
            name="address1"
            value={values.address1}
            placeholder='Input your location'
            onPlaceSelect={(place: any) => {
              // console.log("api resp", place);
              if (place) onPlaceSelect(place.place_id);
            }}
            error={touched.address1 && errors.address1}
          />
        </Col>
        <Col span={8}>
          <FormLableTypography>Address line 2</FormLableTypography>
          <InputField
            id="address2"
            name="address2"
            onChange={handleChange}
            value={values.address2}
            error={touched.address2 && errors.address2}
          />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col span={8}>
          <FormLableTypography>
            Province<span>*</span>
          </FormLableTypography>
          <SelectInput
            id="province"
            name="province"
            placeholdertitle="Select from a list"
            onChange={(e: any) => {
              setFieldValue("province", e);
            }}
            error={errors.province}
            value={values.province}
          >
            {provinceList.map((item: any) => {
              return (
                <>
                  <Option value={item.id}>{item.status}</Option>
                </>
              );
            })}
          </SelectInput>
        </Col>
        <Col span={8}>
          <FormLableTypography>
            City<span>*</span>
          </FormLableTypography>
          <InputField
            id="city"
            name="city"
            onChange={handleChange}
            value={values.city}
            error={touched.city && errors.city}
          />
        </Col>
        <Col span={8}>
          <FormLableTypography>
            Postal code<span>*</span>
          </FormLableTypography>
          <InputField
            id="postal_code"
            name="postal_code"
            placeholder="--- ---"
            onChange={handleChange}
            value={values.postal_code}
            error={touched.postal_code && errors.postal_code}
          />
        </Col>
      </Row>
      <Row
        ref={ref}
        style={{
          width: "100%",
        }}
      >
        <div>
          <MapContainer width={mapWidth} coord={coord} center={center} />
        </div>
      </Row>
    </ContentWrapper>
  );
};

export default AddressDetailsSection;
