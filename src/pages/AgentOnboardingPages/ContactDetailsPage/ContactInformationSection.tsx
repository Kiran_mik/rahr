import React, { useState } from "react";
import { Row, Col } from "antd";
import { FormLableTypography, H3 } from "@components/Typography/Typography";
import { InputField } from "@components/Input/Input";
import ProfilePhotoSection from "./ProfilePhotoInformation";
import { ContentWrapper } from "../style";
import { Flex } from "@components/Flex";
import { FileInput } from "@components/FileInput";
import SelectInput from "@components/Select";
import { Select } from "antd";
const { Option } = Select;

const ContactInformationSection: React.FC<{
  formik: any;
  cellPhoneProvidersList: any;
}> = (props: any) => {
  const { cellPhoneProvidersList, formik } = props;
  const { errors, values, handleChange, setFieldValue, touched } = formik;
  const [imagename, setImagename] = useState("");
  return (
    <ContentWrapper>
      <Flex direction="column">
        <H3 text="Contact information" className="space-bottom-16 font-500" />
      </Flex>
      <Row gutter={16}>
        <Col span={8}>
          <FormLableTypography>
            First name<span>*</span>
          </FormLableTypography>
          <InputField
            id="first_name"
            name="first_name"
            onChange={handleChange}
            value={values.first_name}
            error={touched.first_name && errors.first_name}
          />
        </Col>
        <Col span={8}>
          <FormLableTypography>Middle name</FormLableTypography>
          <InputField
            id="middle_name"
            name="middle_name"
            onChange={handleChange}
            value={values.middle_name}
            error={touched.middle_name && errors.middle_name}
          />
        </Col>
        <Col span={8}>
          <FormLableTypography>
            Last name<span>*</span>
          </FormLableTypography>
          <InputField
            id="last_name"
            name="last_name"
            onChange={handleChange}
            value={values.last_name}
            error={touched.last_name && errors.last_name}
          />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col span={8}>
          <FormLableTypography>
            Email<span>*</span>
          </FormLableTypography>
          <InputField
            id="email"
            name="email"
            onChange={handleChange}
            value={values.email}
            error={touched.email && errors.email}
          />
        </Col>
        <Col span={8}>
          <FormLableTypography>
            Primary phone number<span>*</span>
          </FormLableTypography>
          <InputField
            id="cell_phone_number"
            name="cell_phone_number"
            onChange={handleChange}
            value={values.cell_phone_number}
            error={touched.cell_phone_number && errors.cell_phone_number}
          />
        </Col>
        <Col span={8}>
          <FormLableTypography>Secondary phone number</FormLableTypography>
          <InputField
            id="phone_number"
            name="phone_number"
            onChange={handleChange}
            value={values.phone_number}
            error={touched.phone_number && errors.phone_number}
          />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col span={8}>
          <FormLableTypography>
            Cell phone provider<span>*</span>
          </FormLableTypography>
          <SelectInput
            id="cell_phone_provider"
            name="cell_phone_provider"
            placeholdertitle="Select from a list"
            onChange={(e: any) => {
              setFieldValue("cell_phone_provider", e);
            }}
            error={errors.cell_phone_provider}
            value={values.cell_phone_provider}
          >
            {cellPhoneProvidersList.map((item: any) => {
              return (
                <>
                  <Option value={item.id}>{item.status}</Option>
                </>
              );
            })}
          </SelectInput>
        </Col>
        <Col span={16}>
          <FormLableTypography>
            Driver licence / ID<span>*</span>
          </FormLableTypography>
          <FileInput
            type="file"
            name="driver_license"
            id="driver_license"
            onChange={(value: any) => {
              let formData = new FormData();
              formData.append("imageFile", value.target.files[0]);
              setImagename(value.target.files[0].name);
              setFieldValue("driver_license", value.target.files[0]);
            }}
            error={errors.driver_license}
            imgname={imagename}
            imgURL={values.driver_license}
            label={'Driver licence'}
          />
        </Col>
      </Row>
      <ProfilePhotoSection {...props} />
    </ContentWrapper>
  );
};

export default ContactInformationSection;
