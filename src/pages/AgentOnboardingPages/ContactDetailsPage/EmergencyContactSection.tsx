import React from "react";
import { Row, Col } from "antd";
import {
  H3,
  FormLableTypography,
  FormLableBoldTypography,
} from "@components/Typography/Typography";
import { InputField } from "@components/Input/Input";
import SelectInput from "@components/Select";
import { ContentWrapper } from "../style";
import { Flex } from "@components/Flex";
import { Select } from "antd";
const { Option } = Select;

const EmergencyContactSection: React.FC<{
  formik: any;
  contactRelationshipList: any;
}> = (props: any) => {
  let { contactRelationshipList, formik } = props;
  const { errors, values, handleChange, setFieldValue, touched } = formik;

  return (
    <ContentWrapper>
      <Flex direction="column">
        <H3 text="Emergency contact" className="space-bottom-16 font-500" />
      </Flex>
      <Row gutter={16}>
        <Col span={8}>
          <FormLableTypography>
            Emergency contact name<span>*</span>
          </FormLableTypography>
          <InputField
            name={`first_contact_name`}
            id={`first_contact_name`}
            onChange={handleChange}
            value={values.first_contact_name}
            error={touched.first_contact_name && errors.first_contact_name}
          />
        </Col>
        <Col span={8}>
          <FormLableTypography>
            Emergency contact number<span>*</span>
          </FormLableTypography>
          <InputField
            name={`first_contact_number`}
            id={`first_contact_number`}
            onChange={handleChange}
            value={values.first_contact_number}
            error={touched.first_contact_number && errors.first_contact_number}
          />
        </Col>
        <Col span={8}>
          <FormLableTypography>
            Relationship<span>*</span>
          </FormLableTypography>
          <SelectInput
            id="first_contact_relation"
            name="first_contact_relation"
            placeholdertitle="Select from a list"
            error={errors.first_contact_relation}
            value={values.first_contact_relation}
            onChange={(e: any) => {
              setFieldValue("first_contact_relation", e);
            }}
          >
            {contactRelationshipList.map((item: any) => {
              return (
                <>
                  <Option value={item.id}>{item.status}</Option>
                </>
              );
            })}
          </SelectInput>
        </Col>
      </Row>
      <FormLableBoldTypography style={{ marginBottom: "10px" }}>
        Second emergency contact information
      </FormLableBoldTypography>
      <Row gutter={16}>
        <Col span={8}>
          <FormLableTypography>Emergency contact name</FormLableTypography>
          <InputField
            name={`second_contact_name`}
            id={`second_contact_name`}
            onChange={handleChange}
            value={values.second_contact_name}
            error={errors.second_contact_name}
          />
        </Col>
        <Col span={8}>
          <FormLableTypography>Emergency contact number</FormLableTypography>
          <InputField
            name={`second_contact_number`}
            id={`second_contact_number`}
            onChange={handleChange}
            value={values.second_contact_number}
            error={errors.second_contact_number}
          />
        </Col>
        <Col span={8}>
          <FormLableTypography>Relationship</FormLableTypography>
          <SelectInput
            id="second_contact_relation"
            name="second_contact_relation"
            placeholdertitle="Select from a list"
            error={errors.second_contact_relation}
            value={values.second_contact_relation}
            onChange={(e: any) => {
              setFieldValue("second_contact_relation", e);
            }}
          >
            {contactRelationshipList.map((item: any) => {
              return (
                <>
                  <Option value={item.id}>{item.status}</Option>
                </>
              );
            })}
          </SelectInput>
        </Col>
      </Row>
    </ContentWrapper>
  );
};

export default EmergencyContactSection;
