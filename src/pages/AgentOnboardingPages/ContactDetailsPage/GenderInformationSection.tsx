import React from "react";
import { Row, Col } from "antd";
import { H3, FormLableTypography } from "@components/Typography/Typography";
import { InputField } from "@components/Input/Input";
import { Flex } from "@components/Flex";
import GenderStatusSection from "./GenderStatusSection";

const GenderInformationSection: React.FC<{ formik: any }> = (props: any) => {
  const { values, handleChange, errors, touched } = props.formik;

  return (
    <>
      <Flex direction="column">
        <H3 text="Gender information" className="space-bottom-16 font-500" />
      </Flex>
      <GenderStatusSection
        value={Number(values.gender)}
        onChange={handleChange}
        id={"gender"}
        name={"gender"}
        error={touched.gender && errors.gender}
      />
      {values && values.gender == 4 && (
        <Row gutter={16}>
          <Col span={24}>
            <FormLableTypography>
              Please provide details<span>*</span>
            </FormLableTypography>
            <InputField
              id="other_gender_detail"
              name="other_gender_detail"
              value={values.other_gender_detail}
              error={touched.other_gender_detail && errors.other_gender_detail}
              onChange={handleChange}
            />
          </Col>
        </Row>
      )}
    </>
  );
};

export default GenderInformationSection;
