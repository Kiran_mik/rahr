import React from "react";
import { Row, Radio } from "antd";
import { genderSection } from "./helper";
import { SquareRadioWrapper } from "../style";
import { FormLableBoldTypography } from "@components/Typography/Typography";

function GenderStatusSection(props: genderSection) {
  const { onChange, value, id, name,error } = props;
  return (
    <>
      <FormLableBoldTypography className="spaceBottom-16">
        Select your gender<span>*</span>{" "}
      </FormLableBoldTypography>
      <SquareRadioWrapper>
        <Row className="squareRadioBtn">
          <Radio.Group
            value={value}
            name={id}
            id={name}
             onChange={(e: any) => {
              onChange(e, e.target.value);
            }}
          >
            <Radio value={1}>Male</Radio>
            <Radio value={2}>Female</Radio>
            <Radio value={3}>Prefer not to say</Radio>
            <Radio value={4}>Other</Radio>
          </Radio.Group>
        </Row>
        <span>
        {error ? (
          <span style={{ color: "red", fontFamily: "Poppins" }}>{error}</span>
        ) : (
          ""
        )}
      </span>
      </SquareRadioWrapper>
    </>
  );
}

export default GenderStatusSection;
