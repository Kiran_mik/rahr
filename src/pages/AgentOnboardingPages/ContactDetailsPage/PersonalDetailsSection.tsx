import React from "react";
import { Row, Col } from "antd";
import { H3, FormLableTypography } from "@components/Typography/Typography";
import { Select } from "antd";
import { InputField } from "@components/Input/Input";
import DatePicker from "@components/DatePicker";
import SelectInput, { SelectInputWithCheckBox } from "@components/Select";
import { ContentWrapper } from "../style";
import { Flex } from "@components/Flex";
import GenderInformationSection from "./GenderInformationSection";
import { Checkbox } from "@components/Checkbox";
const { Option } = Select;

const PersonalDetailsSection: React.FC<{
  formik: any;
  languages: Array<any>;
}> = (props: any) => {
  const { errors, values, handleChange, setFieldValue, touched } = props.formik;
  const { languages } = props;

  return (
    <ContentWrapper>
      <Flex direction="column">
        <H3 text="Personal details" className="space-bottom-16 font-500" />
      </Flex>
      <Row gutter={16}>
        <Col span={8}>
          <FormLableTypography>
            Date of birth<span>*</span>
          </FormLableTypography>
          <DatePicker
            placeholder={"dd/mm/yyyy"}
            id="dob"
            name="dob"
            value={values.dob}
            error={touched.dob && errors.dob}
            onChange={(date) => {
              setFieldValue(`dob`, date);
            }}
          />
        </Col>
        <Col span={8}>
          <FormLableTypography>
            SIN<span>*</span>
          </FormLableTypography>
          <InputField
            id="SIN_number"
            name="SIN_number"
            placeholder="---------"
            value={values.SIN_number}
            error={touched.SIN_number && errors.SIN_number}
            onChange={handleChange}
          />
        </Col>
        <Col span={8}>
          <FormLableTypography>
            Language spoken<span>*</span>
          </FormLableTypography>
          <SelectInputWithCheckBox
            mode="multiple"
            id="language_spoken"
            name="language_spoken"
            placeholdertitle="Select from a list"
            options={languages.map((i: any) => {
              i.title = i.name;
              i.key = i.id;
              i.value=i.id;
              return i;

            })}
            error={errors.language_spoken}
            value={values.language_spoken || []}
            onChange={(e: any) => {
              setFieldValue("language_spoken", e);
            }}
          />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col span={24}>
          <FormLableTypography>Your website (if any)</FormLableTypography>
          <InputField
            id="website_url"
            name="website_url"
            value={values.website_url}
            error={touched.website_url && errors.website_url}
            onChange={handleChange}
          />
        </Col>
      </Row>
      <GenderInformationSection {...props} />
    </ContentWrapper>
  );
};

export default PersonalDetailsSection;
