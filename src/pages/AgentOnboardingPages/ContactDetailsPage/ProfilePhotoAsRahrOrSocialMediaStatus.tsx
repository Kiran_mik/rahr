import { Radio, Row } from "antd";
import React from "react";
import { usePhotoAsRahrWebStatus } from "./helper";
import {
  FormLableTypography,
} from "@components/Typography/Typography";

function ProfilePhotoAsRahrOrSocialMediaStatus(props: usePhotoAsRahrWebStatus) {
  const { onChange, value, id, name } = props;

  return (
    <>
      <FormLableTypography style={{ marginBottom: "13px" }}>
        Do you want to use this photo for the RAHR Website/Social Media?
        <span>*</span>
      </FormLableTypography>
      <Row>
        <Radio.Group
          value={value}
          name={name}
          id={id}
          onChange={(e: any) => {
            onChange(e, e.target.value);
          }}
        >
          <Radio value={1}>Yes</Radio>
          <Radio value={0}> No, I will update my photo in future</Radio>
        </Radio.Group>
      </Row>
    </>
  );
}

export default ProfilePhotoAsRahrOrSocialMediaStatus;
