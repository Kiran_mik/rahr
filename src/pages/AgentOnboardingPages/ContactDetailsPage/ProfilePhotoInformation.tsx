import React, { useState } from "react";
import { Row, Col } from "antd";
import {
  FormLableBoldTypography,
  FormLableTypography,
} from "@components/Typography/Typography";
import { FileInput } from "@components/FileInput";
import ProfilePhotoAsRahrOrSocialMediaStatus from "./ProfilePhotoAsRahrOrSocialMediaStatus";
import { TooltipText } from "@components/Tooltip";

const ProfilePhotoSection: React.FC<{ formik: any }> = (props: any) => {
  const { errors, values, handleChange, setFieldValue } = props.formik;
  const [imagename, setImagename] = useState("");
 
  return (
    <>
      <FormLableBoldTypography className="spaceBottom-16">
        Your profile photo information
      </FormLableBoldTypography>
      <Row gutter={16}>
        <Col span={24}>
          <FormLableTypography>
            Your profile picture<span>*</span>{" "}
            <TooltipText
              text={<div>We use your photo for internal recognition. And you can update it later after onboarding<br></br>
              Please use following rules for photo:

            
              <ul style={{paddingLeft:"25px"}}>
                <li>clear, sharp and in focus</li>
                <li>with smile or neutral facial expression</li>
                <li>eyes open and clearly visible</li>
                <li>use plain or not distracted background </li>
                <li>an image of your current appearance, taken within the last 12 months</li>
                </ul></div>}
            />
          </FormLableTypography>
          <FileInput
            type="file"
            name="profile_photo"
            id="profile_photo"
            onChange={(value: any) => {
              let formData = new FormData();
              formData.append("imageFile", value.target.files[0]);
              setImagename(value.target.files[0].name);
              setFieldValue("profile_photo", value.target.files[0]);
            }}
            error={errors.profile_photo}
            imgname={imagename}
            imgURL={values.profile_photo}
            label={"Profile picture"}
          />
        </Col>
      </Row>
      <Row gutter={16} style={{ marginTop: "10px" }}>
        <Col span={24}>
          <ProfilePhotoAsRahrOrSocialMediaStatus
            value={values.is_photo_upload_rahr_social_media}
            onChange={handleChange}
            id={"is_photo_upload_rahr_social_media"}
            name={"is_photo_upload_rahr_social_media"}
          />
        </Col>
      </Row>
    </>
  );
};

export default ProfilePhotoSection;
