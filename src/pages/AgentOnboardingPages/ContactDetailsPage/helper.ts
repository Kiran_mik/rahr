import services from "@services/index";
import { updateUserStore } from "@services/UserService";
import * as Yup from "yup";
import { STEPS, AGENT_ONBOARDING_BASE_URL } from "../constants";

export const validationSchema = Yup.object({
  //Contact information
  first_name: Yup.string()
    .required("Please enter first name")
    .nullable(),
  middle_name: Yup.string().nullable(),
  last_name: Yup.string()
    .required("Please enter last name")
    .nullable(),
  cell_phone_number: Yup.string()
    .required("Primary phone number cannot be empty")
    .min(10, "Phone number should be of 10 digits")
    .max(10, "Please enter a valid phone number")
    .nullable(),
  phone_number: Yup.string()
    .min(10, "Phone number should be of 10 digits")
    .max(10, "Please enter a valid phone number")
    .nullable(),
  email: Yup.string()
    .email("Please enter valid email")
    .required("Please enter email")
    .nullable(),
  cell_phone_provider: Yup.string()
    .required("Please select cell phone provider")
    .nullable(),
  driver_license: Yup.mixed().required('Please select driver  licence').nullable(),
  profile_photo: Yup.mixed().required('Please select profile picture').nullable(),
  is_photo_upload_rahr_social_media: Yup.mixed().nullable(),
  //-------end---------

  //Home address details
  address1: Yup.string()
    .required("Please enter address")
    .nullable(),
  address2: Yup.string().nullable(),
  province: Yup.string()
    .required("Please enter province")
    .nullable(),
  city: Yup.string()
    .required("Please enter city")
    .nullable(),
  postal_code: Yup.string()
    .required("Please enter postal code")
    .nullable(),

  //-------end---------

  //Personal details
  dob: Yup.mixed()
    .required("Please select date of birth")
    .nullable(),
  SIN_number:
    Yup.string()
      .matches(/^[0-9]+$/, "Must be only digits")
      .min(9, 'SIN must be 9 digits')
      .max(9, 'SIN must be 9 digits')
      .required("Please enter SIN")
      .nullable(),
  language_spoken: Yup.mixed()
    .required("Please select language")
    .nullable(),
  website_url: Yup.string()
    .url("Please enter a valid URL")
    .nullable(),
  //-------end---------

  //Gender information
  gender: Yup.string()
    .required("Please select gender")
    .nullable(),
  other_gender_detail: Yup.string()
    .nullable()
    .when(["gender"], {
      is: (gender: number) => gender == 4,
      then: Yup.string().required("Please enter other gender detail").nullable(),
      otherwise: Yup.string().nullable(),
    }),
  //-------end---------

  //Emergency contact
  first_contact_name: Yup.string()
    .required("Please enter emergency contact name")
    .nullable(),
  first_contact_number: Yup.string()
    .required("Please enter emergency contact number")
    .min(10, "Phone number should be of 10 digits")
    .max(10, "Please enter a valid phone number")
    .nullable(),
  first_contact_relation: Yup.string()
    .required("Please select relationship")
    .nullable(),
  second_contact_name: Yup.string().nullable(),
  second_contact_number: Yup.string().nullable(),
  second_contact_relation: Yup.string().nullable(),
  //-------end---------

  //Additional information
  how_did_you_hear_about_rahr: Yup.string()
    .required("Select answer")
    .nullable(),
  detail_hear_about_rahr: Yup.string()
    .nullable()
    .when(["how_did_you_hear_about_rahr"], {
      is: (how_did_you_hear_about_rahr: number) =>
        how_did_you_hear_about_rahr == 10,
      then: Yup.string().nullable().required("Please provide other details"),
      otherwise: Yup.string().nullable(),
    }),

  //-------end---------
});

export const formInitialValues = {
  first_name: "",
  middle_name: "",
  last_name: "",
  email: "",
  cell_phone_number: "",
  phone_number: "",
  cell_phone_provider: "",
  driver_license: "",
  profile_photo: "",
  is_photo_upload_rahr_social_media: 1,
  address1: "",
  address2: "",
  province: "",
  city: "",
  postal_code: "",
  dob: "",
  SIN_number: "",
  language_spoken: [],
  website_url: "",
  gender: 1,
  other_gender_detail: "",
  first_contact_name: "",
  first_contact_number: "",
  first_contact_relation: "",
  second_contact_name: "",
  second_contact_number: "",
  second_contact_relation: "",
  how_did_you_hear_about_rahr: "",
  detail_hear_about_rahr: "",
};

export interface usePhotoAsRahrWebStatus {
  onChange: (e: any, value: any) => void;
  id: string;
  name: string;
  value: any;
}

export interface genderSection {
  onChange: (e: any, value: any) => void;
  id: string;
  name: string;
  value: any;
  error?: string;
}

export const generatePayload = (values: any) => {
  let {
    first_contact_name,
    first_contact_number,
    first_contact_relation,
    second_contact_name,
    second_contact_number,
    second_contact_relation,
    ...restData
  } = values;
  let payload = restData;
  let emergency_contact = [
    {
      contact_name: first_contact_name,
      contact_number: first_contact_number,
      relation: first_contact_relation,
    },
    {
      contact_name: second_contact_name,
      contact_number: second_contact_number,
      relation: second_contact_relation,
    },
  ];
  payload.emergency_contact = emergency_contact;

  return payload;
};

/**
 * Post Api for saving the contact details
 * @param values - payload of contact details form
 * @param currentFormKey - current form key
 */
export const submitContactDetailsForm = async (
  values: any,
  currentFormKey: string
) => {
  delete values.how_did_you_here;
  try {
    const url = currentFormKey;
    const res = await services.post(
      `${AGENT_ONBOARDING_BASE_URL}/${url}`,
      { ...values, draft: 0 }
    );
    await updateUserStore()
    return res;
  } catch (error) {
    return error;
  }
};

/**
 * Api for getting the Cell Phone Providers
 */
export const getCellPhoneProviders = async () => {
  try {
    const res = await services.get(
      `v1/master-statuses?type=CELL_PHONE_PROVIDERS`,
      process.env.REACT_APP_BASE_URL_GLOBAL_SETTINGS
    );
    //@ts-ignore
    return res.data.data;
  } catch (err) {
    return [];
  }
};

export const getLanguages = async () => {
  try {
    const res = await services.get(`v1/languages`);

    //@ts-ignore
    return res.data.data;
  } catch (err) {
    return [];
  }
};

/**
 * Api for getting the relationship list
 */
export const getRelationshipList = async () => {
  try {
    const res = await services.get(
      `v1/master-statuses?type=RELATIONSHIP`,
      process.env.REACT_APP_BASE_URL_GLOBAL_SETTINGS
    );
    //@ts-ignore
    return res.data.data;
  } catch (err) {
    return [];
  }
};

/**
 * Api for getting the HOW_DID_YOU_HEAR_ABOUT_RAHR
 */
export const getHearAboutRahrList = async () => {
  try {
    const res = await services.get(
      `v1/master-statuses?type=HOW_DID_YOU_HEAR_ABOUT_RAHR`,
      process.env.REACT_APP_BASE_URL_GLOBAL_SETTINGS
    );
    //@ts-ignore
    return res.data.data;
  } catch (err) {
    return [];
  }
};

export const provinceList = [
  {
    status: "Alberta",
    id: "Alberta",
  },
  {
    status: "Ontario",
    id: "Ontario",
  },
  {
    status: "Manitoba",
    id: "Manitoba",
  },
  {
    status: "New Brunswick",
    id: "New Brunswick",
  },
  {
    status: "Newfoundland and Labrador.",
    id: "Newfoundland and Labrador.",
  },
];
