import { Flex } from "@components/Flex";
import { H2Typography } from "@components/Typography/Typography";
import React, { useEffect, useState } from "react";
import ContactInformationSection from "./ContactInformationSection";
import AddressDetailsSection from "./AddressDetailsSection";
import PersonalDetailsSection from "./PersonalDetailsSection";
import EmergencyContactSection from "./EmergencyContactSection";
import AdditionalInformationSection from "./AdditionalInformationSection";
import { Formik } from "formik";
import {
  formInitialValues,
  validationSchema,
  submitContactDetailsForm,
  generatePayload,
  getCellPhoneProviders,
  getLanguages,
  getRelationshipList,
  getHearAboutRahrList,
} from "./helper";
import { FormHeader, InnerContainer } from "../style";
import { FlexBox } from "@pages/UserManagementPages/StaffMembers/NewStaff/style";
import { Primary } from "@components/Buttons";
import useAgentSteps from "@hooks/useAgentSteps";
import { STEPS } from "../constants";
import { uploadImage } from "@services/UserService";
import { message } from "antd";
import { getErrors } from "@utils/helpers";

function ContactDetailsPage(props: { path: string }) {
  const [setForm, setStep, form] = useAgentSteps();
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [contactDetailsFormInitialValues, setIntialFormValues] = useState(
    formInitialValues
  );
  const [cellPhoneProvidersList, setCellPhoneProvidersList] = useState<
    { status: string; id: string }[]
  >([]);
  const [contactRelationshipList, setContactRelationshipList] = useState<
    { status: string; id: string }[]
  >([]);
  const [hearAboutList, setHearAboutList] = useState<
    { status: string; id: string }[]
  >([]);
  const [languages, setLanguages] = useState([]);
  const currentStepObject = STEPS[form.currentStep];
  const currentFormKey = currentStepObject && currentStepObject.storeKey;
  const contactDeatailsInfo: any =
    (currentStepObject && form[currentStepObject.storeKey]) || {};
  let formik: any = null;

  useEffect(() => {
    getMenuData();
    getLanguagesData();
    return () => {
      setForm(currentFormKey, { ...contactDeatailsInfo, stepId: null });
    };
  }, []);

  useEffect(() => {
    if (contactDeatailsInfo.stepId && contactDeatailsInfo.profile_detail) {
      const profileDetailsInfo = contactDeatailsInfo.profile_detail;
      let { emergency_contact, ...restData } = profileDetailsInfo;
      let emergencyContactArray = setEmergencyContactValues(emergency_contact);
      const formValues = {
        ...formInitialValues,
        ...restData,
        ...profileDetailsInfo,
        language_spoken: restData.language_spoken
          ? JSON.parse(restData.language_spoken)
          : null,
        cell_phone_provider: restData.cell_phone_provider
          ? Number(restData.cell_phone_provider.status_id)
          : "",
        how_did_you_hear_about_rahr: restData.cell_phone_provider
          ? Number(restData.how_did_you_hear_about_rahr)
          : "",
        gender: restData.gender ? restData.gender.status_id : "",
        ...emergencyContactArray,
        email: contactDeatailsInfo.email,
      };
      delete formValues["how_did_you_here"];
      setIntialFormValues(formValues);
    }
  }, [contactDeatailsInfo.stepId]);

  const setEmergencyContactValues = (emergency_contact: any) => {
    let eContact = emergency_contact;
    if (eContact && eContact.length) {
      return {
        first_contact_name:
          (eContact && eContact[0] && eContact[0]["contact_name"]) || "",
        first_contact_number:
          (eContact && eContact[0] && eContact[0]["contact_number"]) || "",
        first_contact_relation:
          (eContact && eContact[0] && eContact[0]["relation"]) || "",
        second_contact_name:
          (eContact && eContact[1] && eContact[1]["contact_name"]) || "",
        second_contact_number:
          (eContact && eContact[1] && eContact[1]["contact_number"]) || "",
        second_contact_relation:
          (eContact && eContact[1] && eContact[1]["relation"]) || "",
      };
    }
  };

  const onSubmitHandler = async (values: any) => {
    try {
      setIsLoading(true);
      const drivingLicense = await uploadImage(values.driver_license);
      const profilePhoto = await uploadImage(values.profile_photo);
      const res = (await submitContactDetailsForm(
        generatePayload({
          ...values,
          driver_license: drivingLicense,
          profile_photo: profilePhoto,
          draft: 0
        }),
        currentStepObject.key
      )) as any;
      setIsLoading(false);
      if (!res.success) {
        if (res.error && res.error.error) {
          const errors = getErrors(res.error.error);

          formik.setErrors(errors);
        }
        return;
      }
      setStep(form.currentStep + 1);
    } catch (err) {
      if (err.response.data.error) {
        //@ts-ignore
        const isImageFile = err.response.data.error.imageFile;
        message.error(isImageFile);
        setIsLoading(false);
      }
    }
  };

  const getMenuData = async () => {
    const cellPhoneResponse: any = await getCellPhoneProviders();
    setCellPhoneProvidersList(cellPhoneResponse);

    const relationshipResponse: any = await getRelationshipList();
    setContactRelationshipList(relationshipResponse);

    const getHearAboutResponse: any = await getHearAboutRahrList();
    setHearAboutList(getHearAboutResponse);
  };

  const getLanguagesData = async () => {
    const res: any = await getLanguages();
    setLanguages(res || []);
  };
  return (
    <>
      <FormHeader>
        <Flex>
          <H2Typography>Contact Details</H2Typography>
        </Flex>
      </FormHeader>
      <InnerContainer>
        <Formik
          initialValues={contactDetailsFormInitialValues}
          onSubmit={(values) => {
            onSubmitHandler(values);
          }}
          enableReinitialize={true}
          validationSchema={validationSchema}
        >
          {(formikProps) => {
            const { handleSubmit } = formikProps;
            formik = formikProps;
            return (
              <>
                <ContactInformationSection
                  formik={formikProps}
                  cellPhoneProvidersList={cellPhoneProvidersList}
                />
                <AddressDetailsSection formik={formikProps} />
                <PersonalDetailsSection
                  formik={formikProps}
                  languages={languages}
                />
                <EmergencyContactSection
                  contactRelationshipList={contactRelationshipList}
                  formik={formikProps}
                />
                <AdditionalInformationSection
                  formik={formikProps}
                  hearAboutList={hearAboutList}
                />
                <FlexBox className="footerbtn">
                  <Primary
                    text="Proceed to the next step"
                    className="submitbtn"
                    style={{ marginRight: "10px" }}
                    onClick={handleSubmit}
                    isLoading={isLoading}
                  />
                </FlexBox>
              </>
            );
          }}
        </Formik>
      </InnerContainer>
    </>
  );
}

export default ContactDetailsPage;
