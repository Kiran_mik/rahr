import React from "react";
import { Link } from "@reach/router";

interface AddAdditionalEmploymentProps {
  onAdd: () => void;
}

function AddAdditionalEmployment(props: AddAdditionalEmploymentProps) {
  return (
    <div style={{ marginBottom: "33px" }}>
      <Link onClick={props.onAdd} to="">
        + Add additional employment
      </Link>
    </div>
  );
}

export default AddAdditionalEmployment;
