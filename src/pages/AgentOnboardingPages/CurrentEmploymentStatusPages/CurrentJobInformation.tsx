import DatePicker from "@components/DatePicker";
import { Flex } from "@components/Flex";
import { InputField } from "@components/Input/Input";
import TextAreaInput from "@components/Input/TextArea";
import {
  FormLableTypography,
  H3,
  Description,
} from "@components/Typography/Typography";
import { Col, Row } from "antd";
import React, { useRef, useEffect } from "react";
import RemoveIcon from "@assets/icons/delete-bin.svg";
import { RemoveButton } from "../style";
import { TooltipText } from "@components/Tooltip";
import { numberToOrdinalWord } from "./helper";

const CurrentJobInformation: React.FC<{
  formik: any;
  index: any;
  item: any;
  arrayHelper: any;
}> = (props: any) => {
  let { index, item, arrayHelper } = props;
  const companyNameInputRef: any = React.createRef();
  const { values, errors, handleChange, setFieldValue, touched } = props.formik;

  const makeOrdinalTitle =
    index == 0
      ? "Your current job information"
      : numberToOrdinalWord(index + 1) + " job information";

  const getError = (index: any, keyName: any) => {
    let { job_info } = errors;
    let errorText =
      job_info && job_info[index] && job_info[index][keyName]
        ? job_info[index][keyName]
        : "";
    return errorText;
  };

  const getTouchedError = (index: any, keyName: any) => {
    let { job_info } = touched;
    return job_info && job_info[index] && job_info[index][keyName]
      ? job_info[index][keyName]
      : false;
  };

  useEffect(() => {
    focusFirstFeildCheck();
  }, []);

  const focusFirstFeildCheck = () => {
    let { job_info } = values;
    if (companyNameInputRef.current && job_info[0]["company_name"] == "") {
      companyNameInputRef.current.focus();
    }
  };

  return (
    <>
      <Flex direction="column" top={20}>
        <Flex direction="row" justifyContent={"space-between"}>
          <H3 text={makeOrdinalTitle} className="font-500" />
          {index == 0 ? null : (
            <Flex
              direction="row"
              alignItems={"center"}
              onClick={() => {
                arrayHelper.remove(index);
              }}
            >
              <img src={RemoveIcon} width={15} />
              <RemoveButton>Remove</RemoveButton>
            </Flex>
          )}
        </Flex>
        <Flex top={12}>
          <Description
            className="bottom-16"
            text="I hereby declare that while working as a realtor at Right at Home Realty Inc,  I will continue my employment at:"
          />
        </Flex>
        <Row style={{ justifyContent: "space-between" }}>
          <Col span={11}>
            <FormLableTypography className="spaceBottom-16">
              Company name <span>*</span>
              <TooltipText text={""} />
            </FormLableTypography>
            <InputField
              refProps={companyNameInputRef}
              name={`job_info[${index}].company_name`}
              id={`job_info[${index}].company_name`}
              onChange={(e: any) => {
                setFieldValue(
                  `job_info[${index}].company_name`,
                  e.target.value
                );
              }}
              value={item.company_name}
              error={
                getTouchedError(index, "company_name") &&
                getError(index, "company_name")
              }
            />
          </Col>
          <Col span={11}>
            <FormLableTypography>
              Company address <span>*</span>
            </FormLableTypography>
            <InputField
              name={`job_info[${index}].company_address`}
              id={`job_info[${index}].company_address`}
              onChange={(e: any) => {
                setFieldValue(
                  `job_info[${index}].company_address`,
                  e.target.value
                );
              }}
              value={item.company_address}
              error={
                getTouchedError(index, "company_address") &&
                getError(index, "company_address")
              }
            />
          </Col>
        </Row>
        <Row style={{ justifyContent: "space-between" }}>
          <Col span={11}>
            <FormLableTypography>
              Position / title <span>*</span>
            </FormLableTypography>
            <InputField
              name={`job_info[${index}].position`}
              id={`job_info[${index}].position`}
              onChange={(e: any) => {
                setFieldValue(`job_info[${index}].position`, e.target.value);
              }}
              value={item.position}
              error={
                getTouchedError(index, "position") &&
                getError(index, "position")
              }
            />
          </Col>
          <Col span={11}>
            <FormLableTypography>
              Employment commencement date <span>*</span>
            </FormLableTypography>
            <DatePicker
              placeholder={"dd/mm/yyyy"}
              name={`job_info[${index}].employment_commencement_date`}
              id={`job_info[${index}].employment_commencement_date`}
              value={item.employment_commencement_date}
              error={
                getTouchedError(index, "employment_commencement_date") &&
                getError(index, "employment_commencement_date")
              }
              onChange={(date) => {
                setFieldValue(
                  `job_info[${index}].employment_commencement_date`,
                  date
                );
              }}
            />
          </Col>
        </Row>
        <Flex direction="column">
          <FormLableTypography>
            Description of job <span>*</span>
          </FormLableTypography>
          <TextAreaInput
            name={`job_info[${index}].description`}
            id={`job_info[${index}].description`}
            onChange={(e: any) => {
              setFieldValue(`job_info[${index}].description`, e.target.value);
            }}
            value={item.description}
            error={
              getTouchedError(index, "description") &&
              getError(index, "description")
            }
          />
        </Flex>
      </Flex>
    </>
  );
};

export default CurrentJobInformation;
