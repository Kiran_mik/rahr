import React from "react";
import { Row, Radio } from "antd";
import { SquareRadioWrapper } from "../style";
import { FormLableBoldTypography } from "@components/Typography/Typography";
import { TooltipText } from "@components/Tooltip";

interface JobStatusRAHRProps {
  value: any;
  id: string;
  name: string;
  onChange: any;
}

function JobStatusRAHR(props: JobStatusRAHRProps) {
  const { onChange, value, id, name } = props;
  return (
    <>
      <FormLableBoldTypography className="spaceBottom-16">
        Are you going to hold any other job while working at Right at Home
        Realty, Inc?
        <TooltipText
          text={
            "It is a RECO requirement to disclose this information. Explanation about why agent need it"
          }
        />
      </FormLableBoldTypography>
      <SquareRadioWrapper>
        <Row className="squareRadioBtn">
          <Radio.Group
            value={value}
            name={id}
            id={name}
            onChange={(e: any) => {
              onChange(e, e.target.value);
            }}
          >
            <Radio value={1}>I will continue working at my current job</Radio>
            <Radio value={0}>I will work only at RAHR</Radio>
          </Radio.Group>
        </Row>
      </SquareRadioWrapper>
    </>
  );
}

export default JobStatusRAHR;
