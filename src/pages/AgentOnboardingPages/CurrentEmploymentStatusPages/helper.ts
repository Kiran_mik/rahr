import * as Yup from "yup";
import services from "@services/index";
import { STEPS, AGENT_ONBOARDING_BASE_URL } from "../constants";
import { updateUserStore } from "@services/UserService";

export const validationSchema = Yup.object({
  job_info: Yup.array().of(
    Yup.object().shape({
      company_name: Yup.string().required("Please enter company name"),
      company_address: Yup.string().required("Please enter company address"),
      position: Yup.string().required("Please enter position / title"),
      employment_commencement_date: Yup.date().required("Please select date"),
      description: Yup.string().required("Please enter job description"),
    })
  ),
});

export const formInitialValues = {
  full_time_to_rahr_or_not: 0,
  job_info: [
    {
      job_status: 1,
      company_name: "",
      company_address: "",
      position: "",
      employment_commencement_date: "",
      description: "",
    },
  ],
};

export const submitEmploymentStatusDetailsForm = async (
  values: any,
  currentFormKey: string
) => {
  try {
    const url = currentFormKey;
    const res = await services.post(
      `${AGENT_ONBOARDING_BASE_URL}/${url}`,
      {...values,      draft:0
      }
    );
    await updateUserStore()
    return res;
  } catch (error) {
    console.log("error", error);
  }
};

const capitalizeFirstLetter = (string: any) => {
  return string.charAt(0).toUpperCase() + string.slice(1);
};

export const numberToOrdinalWord = (cardinal: any) => {
  var ordinals = [
    "zeroth",
    "first",
    "second",
    "third",
    "fourth",
    "fifth",
    "sixth",
    "seventh",
    "eighth",
    "ninth",
    "tenth",
    "eleventh",
    "twelfth",
    "thirteenth",
    "fourteenth",
    "fifteenth",
    "sixteenth",
    "seventeenth",
    "eighteenth",
    "nineteenth",
    "twentieth",
  ];
  var tens: any = {
    20: "twenty",
    30: "thirty",
    40: "forty" /* and so on up to 90 */,
  };
  var ordinalTens: any = {
    30: "thirtieth",
    40: "fortieth",
    50: "fiftieth" /* and so on */,
  };

  if (cardinal <= 20) {
    return capitalizeFirstLetter(ordinals[cardinal]);
  }

  if (cardinal % 10 === 0) {
    return capitalizeFirstLetter(ordinalTens[cardinal]);
  }

  return capitalizeFirstLetter(
    tens[cardinal - (cardinal % 10)] + ordinals[cardinal % 10]
  );
};
