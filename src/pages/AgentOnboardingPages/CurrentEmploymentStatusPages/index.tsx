import { Flex } from "@components/Flex";
import {
  H2Typography,
  FormLableBoldTypography,
} from "@components/Typography/Typography";
import React, { useState, useEffect } from "react";
import AddAdditionalEmployment from "./AddAdditionalEmployment";
import { ContentWrapper } from "../style";
import { FieldArray, Formik } from "formik";
import { formInitialValues, validationSchema } from "./helper";
import { FormHeader, InnerContainer } from "../style";
import { BorderBtn, Primary } from "@components/Buttons";
import JobStatusRAHR from "./JobStatusRAHR";
import CurrentJobInformation from "./CurrentJobInformation";
import useAgentSteps from "@hooks/useAgentSteps";
import { STEPS } from "../constants";
import { submitEmploymentStatusDetailsForm } from "./helper";
import { navigate } from "@reach/router";

function CurrentEmploymentStatusPages(props: { path: string }) {
  const [setForm, setStep, form] = useAgentSteps();
  const currentStepObject = STEPS[form.currentStep];
  const currentFormKey = STEPS[form.currentStep] && STEPS[form.currentStep].storeKey;
  const [isLoading, setIsLoading] = useState(false);
  const currentEmploymentInfo: any = form[currentFormKey] || {};
  const [currentEmploymentFormInitialValues, setIntialFormValues] = useState(
    formInitialValues
  );

  useEffect(() => {
    return () => {
      setForm(currentFormKey, { ...currentEmploymentInfo, stepId: null });
    };
  }, []);

  useEffect(() => {
    if (currentEmploymentInfo.stepId && currentEmploymentInfo.id) {
      let {
        company_details_data,
        full_time_to_rahr_or_not,
      } = currentEmploymentInfo;
      let job_info =
        company_details_data && company_details_data.length
          ? company_details_data
          : formInitialValues.job_info;
      setIntialFormValues({
        full_time_to_rahr_or_not: full_time_to_rahr_or_not,
        job_info: job_info,
      });
    }
  }, [currentEmploymentInfo.stepId]);

  const onSubmitHandler = async (values: any) => {
    let payload = values;
    if (payload.full_time_to_rahr_or_not == 0) {
      payload.job_info = [];
    }

    setIsLoading(true);
    const res = await submitEmploymentStatusDetailsForm(
      payload,
      currentStepObject.key
    );
    setIsLoading(false);
    setForm("currentFormKey", payload);
    setStep(form.currentStep + 1);
  };

  const onSubmitCheck = (formikProps: any) => {
    const { values, handleSubmit } = formikProps;
    if (values.full_time_to_rahr_or_not == 0) {
      onSubmitHandler(values);
    } else {
      handleSubmit();
    }
  };

  return (
    <>
      <FormHeader>
        <Flex>
          <H2Typography>Current Employment Status</H2Typography>
        </Flex>
      </FormHeader>
      <InnerContainer>
        <Formik
          onSubmit={(values) => {
            console.log("values", values);
            onSubmitHandler(values);
          }}
          initialValues={currentEmploymentFormInitialValues}
          validationSchema={validationSchema}
          enableReinitialize={true}
        >
          {(formikProps) => {
            const {
              values,
              setFieldValue,
              handleChange,
              handleSubmit,
            } = formikProps;
            return (
              <>
                <Flex direction="column" top={20} style={{minHeight:"calc(100vh - 400px)"}}>
                  <JobStatusRAHR
                    id={"full_time_to_rahr_or_not"}
                    name={"full_time_to_rahr_or_not"}
                    value={values.full_time_to_rahr_or_not}
                    onChange={handleChange}
                  />
                </Flex>
                {values.full_time_to_rahr_or_not == 0 ? null : (
                  <>
                    <FieldArray
                      name={"job_info"}
                      render={(arrayHelper: any) => {
                        return formikProps.values.job_info.map(
                          (item: any, index: any) => {
                            return (
                              <CurrentJobInformation
                                formik={formikProps}
                                item={item}
                                index={index}
                                arrayHelper={arrayHelper}
                              />
                            );
                          }
                        );
                      }}
                    />
                    <AddAdditionalEmployment
                      onAdd={() => {
                        const originalArray = values.job_info;
                        let length = originalArray.length;
                        originalArray.push({
                          job_status: length + 1,
                          company_name: "",
                          company_address: "",
                          position: "",
                          employment_commencement_date: "",
                          description: "",
                        });
                        setFieldValue("job_info", originalArray);
                      }}
                    />
                  </>
                )}
                <Flex className="footerbtn">
                  <Primary
                    text="Proceed to the next step"
                    className="submitbtn"
                    style={{ marginRight: "10px" }}
                    onClick={() => onSubmitCheck(formikProps)}
                    isLoading={isLoading}
                  />

                  <BorderBtn
                    onClick={() => navigate("/agent-onboard/profile-info")}
                    className="borderbtn"
                    text="Previous step"
                  />
                </Flex>
              </>
            );
          }}
        </Formik>
      </InnerContainer>
    </>
  );
}

export default CurrentEmploymentStatusPages;
