import React from "react";
import { Row, Col } from "antd";
import { FormLableTypography } from "@components/Typography/Typography";
import { InputField } from "@components/Input/Input";
import { PERCProps } from "./helper";
import { TooltipText } from "@components/Tooltip";

const CorporateHSTInput = (props: PERCProps) => {
  const { formik } = props;
  const { values } = formik;
  const { joining_as_prec } = values;
  const currentHST = joining_as_prec === 1 ? "corporate_hst" : "personal_hst";
  const tootTipTitle =
    joining_as_prec === 0
      ? "If agent doesnt join as PREC - need to fill out personal HST#"
      : "If agent joins as a PREC - he/she needs to fill out corporate HST number and fill out PREC form.";
  return (
    <Row style={{ marginTop: "10px" }}>
      <Col span={16}>
        <FormLableTypography>
          {joining_as_prec === 1 ? "Corporate" : "Personal"} HST #<span>*</span>{" "}
          <TooltipText text={tootTipTitle} />
        </FormLableTypography>
        <InputField
          id={currentHST}
          value={formik.values[currentHST]}
          name={currentHST}
          onChange={formik.handleChange}
          placeholder={"_ _ _ _ _ _ _ _ _  RT _ _ _ _ "}
          error={formik.touched[currentHST] && formik.errors[currentHST]}
        />
      </Col>
    </Row>
  );
};

export default CorporateHSTInput;
