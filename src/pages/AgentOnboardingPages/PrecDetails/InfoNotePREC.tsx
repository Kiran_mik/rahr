import React from "react";
import { Flex } from "@components/Flex";
import { ContentWrapper } from "../style";
import TextAreaInput from "@components/Input/TextArea";
import { Row, Col } from "antd";
import { DescriptionItem } from "./style";
import { REGISTER_YOUR_BUSSINESS_URL } from "../constants";

export default function InformationPREC() {
  return (
    <>
      <Flex direction="column" top={10}>
        <DescriptionItem text="A PREC is a corporation that a salesperson or broker may establish that is permitted to directly receive from a brokerage remuneration that is earned by the registrant. Find more information in the PREC form." />
        <DescriptionItem
          text={
            <>
              For more information please refer to{" "}
              <a href={REGISTER_YOUR_BUSSINESS_URL} target={"_blank"}>
                this link
              </a>
              .
            </>
          }
        />
        <DescriptionItem text="To finish your onboarding you don't need to fill all your PREC  information at the moment. But you will have to complete it after you will become RAHR agent if you selected PREC option." />
      </Flex>
    </>
  );
}
