import React from "react";
import { Row, Radio } from "antd";
import { joiningPREC } from "./helper";
import { SquareRadioWrapper } from "../style";
import { FormLableBoldTypography } from "@components/Typography/Typography";
import { TooltipText } from "@components/Tooltip";

function JoiningPrecStatus(props: joiningPREC) {
  const { formik } = props;
  const { values } = formik;

  return (
    <>
      <FormLableBoldTypography className="spaceBottom-16">
        Are your joining as a “PREC”?
        <TooltipText
          text={"Can apply via 1-800-959-5525 or http://www.cra-arc.gc.ca  "}
        />
      </FormLableBoldTypography>
      <SquareRadioWrapper>
        <Row className="squareRadioBtn">
          <Radio.Group
            onChange={formik.handleChange}
            id="joining_as_prec"
            name="joining_as_prec"
            value={values.joining_as_prec}
          >
            <Radio value={1}>Yes</Radio>
            <Radio value={0}>No</Radio>
          </Radio.Group>
        </Row>
      </SquareRadioWrapper>
    </>
  );
}

export default JoiningPrecStatus;
