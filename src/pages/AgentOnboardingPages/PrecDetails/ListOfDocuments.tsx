import React from "react";
import { Flex } from "@components/Flex";
import {
  FormLableBoldTypography,
  Description,
} from "@components/Typography/Typography";
import { DocumentList, DescriptionItem } from "./style";

export default function ListOfDocuments() {
  return (
    <>
      <Flex direction="column" top={20}>
        <FormLableBoldTypography className="spaceBottom-16">
          List of documents you need to register as PREC:
        </FormLableBoldTypography>
        <DocumentList>
          <li>Articles of Incorporation</li>
          <li>Void cheque for Corporation</li>
          <li>
            Agreement between RAHR & Agent to use PREC for payment of
            commissions
          </li>
        </DocumentList>
      </Flex>
    </>
  );
}
