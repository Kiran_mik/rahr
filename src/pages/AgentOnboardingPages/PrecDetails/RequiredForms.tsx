import React, { useEffect, useState } from "react";
import RequiredFormUpload from "./RequiredFormUpload/index";
import { ContentWrapper } from "../style";
import { Flex } from "@components/Flex";
import {
  H3Typography,
  FormLableTypography,
} from "@components/Typography/Typography";
import DocumentImg from "@assets/icon-form.svg";
import { PERCProps } from "./helper";
import DocuSign from "@components/DocuSign";
import {
  getDocuSignURL,
  getQueryParams,
  removeQueryParams,
} from "@utils/helpers";
import useSWR from "swr";
import { checkDocuSign, getDocuSignLink } from "@services/UserService";
import { Spin } from "antd";

export default function RequiredForm(props: PERCProps) {
  const { values } = props.formik;
  const [showLoader, setShowLoader] = useState(false);
  const precEnvId = getQueryParams("envelope_id");
  const url = getDocuSignURL("", "v1/agents/onboardings/prec-details/docusign");
  const { data, error } = useSWR(
    [url, precEnvId || values.prec_form_signed_document],
    getDocuSignLink,
    {
      revalidateOnFocus: false,
    }
  );
  const docuSignURLPREC = data;
  const isValidating = !data;

  useEffect(() => {
    if (precEnvId && !values.prec_form_signed_document) {
      checkDocuSignEnvolpe(precEnvId);
    }
  }, [
    precEnvId,
    values.prec_form_envelope_id,
    values.prec_form_signed_document,
  ]);

  const checkDocuSignEnvolpe = async (envId: string) => {
    setShowLoader(true);

    const res = (await checkDocuSign({
      envelope_id: values.prec_form_envelope_id || envId,
      type: "agent-agreement_PREC-form",
      event: "signing_complete",
    })) as { success: boolean; data: string };
    if (res.success) {
      props.formik.setFieldValue("prec_form_signed_document", res.data);
      removeQueryParams();
    }

    setShowLoader(false);
  };

  const isLoading =
    (isValidating &&
      !values.prec_form_sign_url &&
      !values.prec_form_signed_document) ||
    showLoader;

  console.log({ docuSignURLPREC });
  return (
    <ContentWrapper className="bordernone">
      {isLoading && <Spin />}
      {!isLoading && (
        <>
          <Flex bottom={20}>
            <H3Typography>Required forms</H3Typography>
          </Flex>
          <FormLableTypography>
            Personal Real Estate Corporation form<span>*</span>
          </FormLableTypography>
          <Flex top={10}>
            <DocuSign
              text="PREC Form"
              linktext="View and sign form"
              image={DocumentImg}
              link={docuSignURLPREC}
              docURL={values.prec_form_signed_document}
            />
          </Flex>
        </>
      )}
    </ContentWrapper>
  );
}
