
import services from "@services/index";
import { updateUserStore } from "@services/UserService";
import * as Yup from "yup";
import { STEPS, AGENT_ONBOARDING_BASE_URL } from "../constants";



export interface PERCProps {
  formik: any

}

export interface joiningPREC extends PERCProps {
  formik: any

}


export const validationSchema = Yup.object({
  joining_as_prec: Yup.boolean().required('Please select this field'),
  corporate_hst: Yup.string().nullable().when(['joining_as_prec'], {
    is: (joining_as_prec: boolean) => joining_as_prec,
    then: Yup.string().required('Please enter corporate HST number').matches(/^[0-9]+$/, "Must be only digits").min(9, 'Please enter nine digits of your HST number').nullable(),
    otherwise: Yup.string().nullable()

  }),
  personal_hst: Yup.string().nullable()
    .when(['joining_as_prec'], {
      is: (joining_as_prec: boolean) => !joining_as_prec,
      then: Yup.string().required('Please enter personal HST number').matches(/^[0-9]+$/, "Must be only digits").min(9, 'Please enter nine digits of your HST number').nullable(),
      otherwise: Yup.string().nullable()
    }),
  onboardingForm: Yup.object()

})

export const formInitialValues = {
  joining_as_prec: 1,
  corporate_hst: '',
  personal_hst: '',
  onboardingForm: {}
}


export const submitPRECForm = async (values: any, currentFormKey: string) => {
  try {
    const url = currentFormKey;
    const res = await services.post(`${AGENT_ONBOARDING_BASE_URL}/${url}`, {
      ...values, draft: 0
    })
    await updateUserStore()
    return res;
  } catch (error) {

  }
}