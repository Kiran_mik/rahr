import { Flex } from "@components/Flex";
import { H3Typography } from "@components/Typography/Typography";
import React, { useEffect, useState } from "react";
import { FormHeader, InnerContainer, ContentWrapper } from "../style";
import JoiningPrecStatus from "./JoiningPrecStatus";
import CorporateHSTInput from "./CorporateHSTInput";
import InformationPREC from "./InfoNotePREC";
import ListOfDocuments from "./ListOfDocuments";
import RequiredForm from "./RequiredForms";
import { FlexBox } from "@pages/UserManagementPages/StaffMembers/NewStaff/style";
import { Primary, BorderBtn } from "@components/Buttons";
import { useFormik, validateYupSchema } from "formik";
import { formInitialValues, submitPRECForm, validationSchema } from "./helper";
import { STEPS } from "../constants";
import useAgentSteps from "@hooks/useAgentSteps";

function PRECDetailsPage(props: { path: string }) {
  const [setForm, setStep, form] = useAgentSteps();
  const currentStepObject = STEPS[form.currentStep];

  const [isLoading, setIsLoading] = useState<boolean>(false);
  const currentFormKey = STEPS[form.currentStep] && STEPS[form.currentStep].storeKey;
  const precDetailsInfo: any = form[currentFormKey] || {};
  const [precDetailsFormInitialValues, setIntialFormValues] = useState(
    formInitialValues
  );

  const formik = useFormik({
    onSubmit: async (values: any) => {
      const formValues =
        values.joining_as_prec === 1
          ? { ...values, personal_hst: "" }
          : { ...values, corporate_hst: "" };
      setIsLoading(true);
      const res = await submitPRECForm(formValues, currentStepObject.key);
      setIsLoading(false);
      setForm(currentFormKey, formValues);
      setStep(form.currentStep + 1);
    },
    initialValues: precDetailsFormInitialValues,
    validationSchema: validationSchema,
    enableReinitialize: true,
  });

  useEffect(() => {
    return () => {
      setForm(currentFormKey, { ...precDetailsInfo, stepId: null });
    };
  }, []);

  useEffect(() => {
       setIntialFormValues({...formInitialValues,...precDetailsInfo});
    
  }, [precDetailsInfo.stepId]);

  const { values } = formik;
   return (
    <>
      <FormHeader>
        <Flex>
          <H3Typography>Personal Real Estate Corporation</H3Typography>
        </Flex>
      </FormHeader>
      <InnerContainer>
        <ContentWrapper>
          <JoiningPrecStatus formik={formik} />
          <CorporateHSTInput formik={formik} />
          <InformationPREC />
          {values.joining_as_prec === 1 && <ListOfDocuments />}
        </ContentWrapper>
        {!!values.joining_as_prec  && values.joining_as_prec === 1 && <RequiredForm formik={formik} />}
        <FlexBox className="footerbtn">
          <Primary
            text="Proceed to the next step"
            className="submitbtn"
            style={{ marginRight: "10px" }}
            onClick={formik.handleSubmit}
            isLoading={isLoading}
          />

          <BorderBtn
            className="borderbtn"
            text="Previous step"
            onClick={() => {
              setStep(form.currentStep - 1);
            }}
          />
        </FlexBox>
      </InnerContainer>
    </>
  );
}

export default PRECDetailsPage;
