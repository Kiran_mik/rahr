import styled from "styled-components";
import { Tooltip, Button } from "antd";
import { H3, Description } from "@components/Typography/Typography";

export const DescriptionItem = styled(Description)`
  margin-bottom: 16px;
  color: #000;
`;

export const DocumentList = styled.ul`
  padding: 0px 25px;
  margin-bottom: 0px;
  & > li:after {
    font-size: 12px;
    padding: 0 8px;
  }
`;

export const PrimaryButtonWrapper = styled.div`
  margin-top: 20px !important;
`;
