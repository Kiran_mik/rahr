import { Link } from "@reach/router";
import React from "react";

interface AddNewGarnishmentSubjectProps {
  onAdd: () => void;
}

function AddNewGarnishmentSubject(props: AddNewGarnishmentSubjectProps) {
  return (
    <div style={{ borderBottom: "1px dashed #a2a2ba", paddingBottom: "33px" }}>
      <Link onClick={props.onAdd} to="" style={{ color: "#4E1C95" }}>
        + Add additional subject
      </Link>
    </div>
  );
}

export default AddNewGarnishmentSubject;
