import { Link } from "@reach/router";
import React from "react";

interface AddNewComplaintProps {
  onAdd: () => void;
}

function AddNewUnpaidJudgement(props: AddNewComplaintProps) {
  return (
    <div style={{ borderBottom: "1px dashed #a2a2ba", paddingBottom: "33px" }}>
      <Link onClick={props.onAdd} to="" style={{ color: "#4E1C95" }}>
        + Add additional unpaid judgment
      </Link>
    </div>
  );
}

export default AddNewUnpaidJudgement;
