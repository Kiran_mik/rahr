import React from "react";
import {  FormLableTypography, H3, H5, H5Typography } from "@components/Typography/Typography";
import { Flex } from "@components/Flex";
import { ContentWrapper } from "../style";
import { Radio, Row, Col } from "antd";
import { InputField } from "@components/Input/Input";
import DatePicker from "@components/DatePicker";
import { TooltipText } from "@components/Tooltip";
import { Note } from "@components/Note";

const ApplicationRECO: React.FC<{
  formik: any;
}> = (props: any) => {
  const { errors, values, handleChange, touched, setFieldValue } = props.formik;
   return (
    <ContentWrapper>
      <Flex direction="column">
        <H3
          text="Have you submitted your Application to RECO?"
          className="space-bottom-16 font-500"
          mandatoryIcon="*"
        />
      </Flex>
      <Flex style={{ marginBottom: "16px" }}>
        <Radio.Group
          value={Number(values.submit_application_to_reco_to_join_rahr)}
          onChange={handleChange}
          id={"submit_application_to_reco_to_join_rahr"}
          name={"submit_application_to_reco_to_join_rahr"}
        >
          <Radio value={1}>Yes</Radio>
          <Radio value={0}>No</Radio>
        </Radio.Group>
      </Flex>
      {values.submit_application_to_reco_to_join_rahr ? (
        <>
          <Row gutter={16}>
            <Col span={24}>
              <H5 text="RECO license Information" className="font-500" />
            </Col>
            <Col span={12}>
              <FormLableTypography>
                Trade name registered under RECO <span>*</span>
                <TooltipText text={"Trade name is what is approved by RECO"} />

              </FormLableTypography>
              <InputField
                id="reco_registration_name"
                name="reco_registration_name"
                onChange={handleChange}
                value={values.reco_registration_name}
                error={
                  touched.reco_registration_name && errors.reco_registration_name
                }
              />
            </Col>
            <Col span={12}>
              <FormLableTypography>RECO Registration number <span>*</span> </FormLableTypography>
              <InputField
                id="reco_registration_number"
                name="reco_registration_number"
                onChange={handleChange}
                value={values.reco_registration_number}
                error={
                  touched.reco_registration_number &&
                  errors.reco_registration_number
                }
              />
            </Col>
            <Col span={12}>
              <FormLableTypography>RECO Legal name <span>*</span></FormLableTypography>
              <InputField
                id="reco_legal_name"
                name="reco_legal_name"
                onChange={handleChange}
                value={values.reco_legal_name}
                error={touched.reco_legal_name && errors.reco_legal_name}
              />
            </Col>
            <Col span={12}>
              <FormLableTypography>Expiry date <span>*</span></FormLableTypography>
              <DatePicker
                placeholder={"dd/mm/yyyy"}
                id="reco_expiry_date"
                name="reco_expiry_date"
                value={values.reco_expiry_date}
                error={touched.reco_expiry_date && errors.reco_expiry_date}
                onChange={(date) => {
                  setFieldValue(`reco_expiry_date`, date);
                }}
              />
            </Col>
            <Col span={12}>
              <FormLableTypography>Year licensed </FormLableTypography>
              <DatePicker
                placeholder={"yyyy"}
                id="reco_year_licensed"
                name="reco_year_licensed"
                picker="year"
                value={values.reco_year_licensed}
                error={touched.reco_year_licensed && errors.reco_year_licensed}
                onChange={(date) => {
                  setFieldValue(`reco_year_licensed`, date);
                }}
              />
            </Col>
          </Row>
        </>
      ) :
        <Note
          // title="You need to register your RECO license"
          description={
            (<>
              Please proceed to the RECO website www.reco.on.ca and submit your RECO application with RAHR's. Our RECO Brokerage #4721253 RECO will proceed your application within their guidelines of 8-10 business days. You will need to make your insurance payment to finalize the registration. Please, find full instructions on how to set up RECO account using <a href="#">this link</a>
            </>)} />

      }


    </ContentWrapper>
  );
};

export default ApplicationRECO;
