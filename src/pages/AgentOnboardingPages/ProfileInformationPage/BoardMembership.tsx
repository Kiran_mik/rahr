import React, { useEffect, useState } from "react";
import { H5, H3 } from "@components/Typography/Typography";
import SignupFormLink from "@components/SignupFormLink";
import DocumentImg from "@assets/icon-form.svg";
import { Flex } from "@components/Flex";
import { ContentWrapper } from "../style";
import { getDocuSignURL, getQueryParams } from "@utils/helpers";
import useSWR from "swr";
import { checkDocuSign, getDocuSignLink } from "@services/UserService";
import DocuSign from "@components/DocuSign";
import { Spin } from "antd";
import { getBranchBoards } from "./helper";

export default function BoardMembership(props: { formik: any }) {
  const { values, errors } = props.formik;
  const envolopeId = getQueryParams("envelope_id");
  const [showLoader, setShowLoader] = useState(false);
  const [isFetching, SetIsFetching] = useState(true);
  const [branchBoards, setBranchBoards] = useState([]);
  useEffect(() => {
    const fetchBranchBoards = async () => {
      SetIsFetching(true)
      if (values.user_branch) {
        const res = await getBranchBoards(values.user_branch.id);
        setBranchBoards(res);
      }
      SetIsFetching(false);
    }
    fetchBranchBoards();
  }, [values.user_branch]);

  const url = getDocuSignURL(
    "Board ID Forms&number=2",
    "v1/agents/onboardings/profile-info/docusign"
  );

  const { data, error } = useSWR(
    [url, values.board_id_form_to_sign_sign_url],
    getDocuSignLink
  );
  const docuSignPlatformURL = data;

  useEffect(() => {
    if (envolopeId) {
      checkDocuSignEnvolpe(envolopeId);
    }
  }, [envolopeId]);

  const checkDocuSignEnvolpe = async (envId?: string) => {
    if (!values.board_id_form_to_sign_signed_document) {
      const res = (await checkDocuSign(
        {
          envelope_id: values.board_id_form_to_sign_envelope_id || envId,
          type: "agent-profile-info_2",
          event: "signing_complete",
        },
        "/profile-info/docusign"
      )) as { success: boolean; data: string };
      if (res.success) {
        props.formik.setFieldValue(
          "board_id_form_to_sign_signed_document",
          res.data
        );
      }
    }
  };
  const isValidating = !data;
  const isLoading =
    (isValidating &&
      !values.board_id_form_to_sign_sign_url &&
      !values.board_id_form_to_sign_signed_document) ||
    showLoader || isFetching;
  if (!values.user_branch) return null;
  return (
    <ContentWrapper>
      <Flex direction="column">
        <H3
          text="Your board membership(s)"
          className="space-bottom-16 font-500"
          mandatoryIcon="*"
        />
      </Flex>

      <Flex direction="column">
        {isLoading && <Spin />}
        {!isLoading && (
          <>
            <H5 text="Board ID forms to sign" className="font-500" />
            <DocuSign
              text="TRREB form"
              linktext="Sign form"
              error={errors.board_id_form_to_sign_signed_document}
              image={DocumentImg}
              link={
                values.board_id_form_to_sign_sign_url || docuSignPlatformURL
              }
              docURL={values.board_id_form_to_sign_signed_document}
            />
          </>
        )}
      </Flex>
    </ContentWrapper>
  );
}
