import React, { useState } from "react";
import { FormLableTypography, H3 } from "@components/Typography/Typography";
import { Flex } from "@components/Flex";
import { ContentWrapper } from "../style";
import SelectInput from "@components/Select";
import { Col, Radio, Row, Select } from "antd";
import { FileInput } from "@components/FileInput";
import { InputField } from "@components/Input/Input";
import { AGENT_STATUS } from "./helper";
import { BrokerOrSalesRepresentative } from "@constants/onboardingConstants";

const { Option } = Select;

export default function BrokerSalesRepresentative(props: any) {
  let { specializeAreasLists, formik } = props;
  let { values, handleChange, setFieldValue, errors, touched } = formik;
  const { agentStatus } = props;
  const [payrollReportImageName, setPayrollReportImageName] = useState("");

  return (
    <ContentWrapper>
      <Flex direction="column">
        <H3
          text="Are you a broker or sales representative?"
          className="space-bottom-16 font-500"
          mandatoryIcon="*"
        />
      </Flex>
      <Flex style={{ marginBottom: "16px" }}>
        <Radio.Group
          value={Number(values.broker_or_sales_representative)}
          onChange={handleChange}
          id="broker_or_sales_representative"
          name="broker_or_sales_representative"
        >
          <Radio value={1}>{BrokerOrSalesRepresentative[1]}</Radio>
          <Radio value={2}>{BrokerOrSalesRepresentative[2]}</Radio>
        </Radio.Group>
      </Flex>
      <Flex direction="column">
        <FormLableTypography>Areas you specialize in</FormLableTypography>
        <SelectInput
          placeholdertitle="Select from a list"
          className="bottom-0"
          id="primary_area_of_specialization_id"
          name="primary_area_of_specialization_id"
          onChange={(e: any) => {
            setFieldValue("primary_area_of_specialization_id", e);
          }}
          value={values.primary_area_of_specialization_id && Number(values.primary_area_of_specialization_id)}
          error={
            touched.primary_area_of_specialization_id &&
            errors.primary_area_of_specialization_id
          }
        >
          {specializeAreasLists.map((item: any) => <Option key={item.id} value={item.id}>{item.status}</Option>)}
        </SelectInput>
      </Flex>
      {(agentStatus == AGENT_STATUS.TRANSFER || agentStatus == AGENT_STATUS.REINSTATED_WITH_RECO) && (<><Row gutter={16} style={{ marginTop: "16px" }}>
        <Col span={12}>
          <FormLableTypography>Previous Year Transactions <span>*</span></FormLableTypography>
          <InputField
            id="previous_year_transactions"
            name="previous_year_transactions"
            onChange={handleChange}
            value={values.previous_year_transactions}
            error={
              touched.previous_year_transactions
              && errors.previous_year_transactions
            }
          />
        </Col>
        <Col span={12}>
          <FormLableTypography>Last year gross income<span>*</span></FormLableTypography>
          <InputField
            id="last_year_gross_income"
            name="last_year_gross_income"
            onChange={handleChange}
            value={values.last_year_gross_income}
            error={
              touched.last_year_gross_income
              && errors.last_year_gross_income
            }
          />
        </Col>
      </Row>
        <Flex direction="column">
          <FormLableTypography>Previous year payroll history report<span>*</span></FormLableTypography>
          <FileInput
            type="file"
            name="previous_year_payroll_history_report"
            id="previous_year_payroll_history_report"
            onChange={(value: any) => {
              let formData = new FormData();
              formData.append("imageFile", value.target.files[0]);
              setPayrollReportImageName(value.target.files[0].name);
              setFieldValue("previous_year_payroll_history_report", value.target.files[0]);
            }}
            error={errors.previous_year_payroll_history_report}
            imgname={payrollReportImageName}
            imgURL={values.previous_year_payroll_history_report}
            label={'Payroll report'}
          />
        </Flex>
      </>
      )}
    </ContentWrapper>
  );
}
