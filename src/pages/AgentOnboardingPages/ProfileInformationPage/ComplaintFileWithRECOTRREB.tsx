import React from "react";
import { FormLableTypography } from "@components/Typography/Typography";
import { Flex } from "@components/Flex";
import { Row, Col } from "antd";
import { InputField } from "@components/Input/Input";
import RemoveIcon from "@assets/icons/delete-bin.svg";
import { RemoveButton } from "../style";

const ComplaintFileWithRECOTRREB: React.FC<{
  formik: any;
  index: any;
  arrayHelper: any;
  item: any;
}> = (props: any) => {
  let { index, formik, arrayHelper } = props;
  const { errors, touched, values, handleChange } = formik;

  return (
    <>
      <Flex direction="row" justifyContent={"flex-end"} top={20}>
        {index == 0 ? null : (
          <Flex
            direction="row"
            alignItems={"center"}
            onClick={() => {
              arrayHelper.remove(index);
            }}
          >
            <img src={RemoveIcon} width={15} />
            <RemoveButton>Remove</RemoveButton>
          </Flex>
        )}
      </Flex>
      <Row>
        <Col span={24}>
          <FormLableTypography>
            Provide details of the complaint <span>*</span>
          </FormLableTypography>
          <InputField
            placeholder=""
            name={`complaint_file_opened_with_reco_or_trreb_data.${index}.complaint`}
            id="complaint"
            onChange={handleChange}
            value={values.complaint_file_opened_with_reco_or_trreb_data[index].complaint}
            error={
              touched 
              && touched.complaint_file_opened_with_reco_or_trreb_data 
              && touched.complaint_file_opened_with_reco_or_trreb_data[index]
              && touched.complaint_file_opened_with_reco_or_trreb_data[index].complaint
              && errors 
              && errors.complaint_file_opened_with_reco_or_trreb_data 
              && errors.complaint_file_opened_with_reco_or_trreb_data[index]
              && errors.complaint_file_opened_with_reco_or_trreb_data[index].complaint
            }
          />
        </Col>
        <Col span={24}>
          <FormLableTypography>
            Provide details of the outcome<span>*</span>
          </FormLableTypography>
          <InputField
            placeholder=""
            name={`complaint_file_opened_with_reco_or_trreb_data.${index}.outcome`}
            id="outcome"
            onChange={handleChange}
            value={values.complaint_file_opened_with_reco_or_trreb_data[index].outcome}
            error={
              touched 
              && touched.complaint_file_opened_with_reco_or_trreb_data 
              && touched.complaint_file_opened_with_reco_or_trreb_data[index]
              && touched.complaint_file_opened_with_reco_or_trreb_data[index].outcome
              && errors 
              && errors.complaint_file_opened_with_reco_or_trreb_data 
              && errors.complaint_file_opened_with_reco_or_trreb_data[index]
              && errors.complaint_file_opened_with_reco_or_trreb_data[index].outcome
            }
          />
        </Col>
      </Row>
    </>
  );
};

export default ComplaintFileWithRECOTRREB;
