import { FieldArray } from "formik";
import React from "react";
import AddNewComplaint from "./AddNewComplaint";
import ComplaintFileWithRECOTRREB from "./ComplaintFileWithRECOTRREB";
import { ContentWrapper } from "../style";
import { Radio } from "antd";
import { H3 } from "@components/Typography/Typography";
import { Flex } from "@components/Flex";
import { NEW_COMPLAINT_SECTION } from "./helper";

function ComplaintSection(props: { formik: any }) {
  const { formik } = props;
  const { values, setFieldValue, handleChange } = formik;
  return (
    <ContentWrapper className="space-bottom-0 bordernone">
      <Flex direction="column">
        <H3
          text="Have you ever had a complaint file opened with RECO or TRREB? "
          className="space-bottom-16 font-500"
          mandatoryIcon="*"
        />
      </Flex>
      <Flex style={{ marginBottom: "16px" }}>
        <Radio.Group
          value={Number(values.complaint_file_opened_with_reco_or_trreb)}
          onChange={handleChange}
          id={"complaint_file_opened_with_reco_or_trreb"}
          name={"complaint_file_opened_with_reco_or_trreb"}
        >
          <Radio value={1}>Yes</Radio>
          <Radio value={0}>No</Radio>
        </Radio.Group>
      </Flex>
      {values.complaint_file_opened_with_reco_or_trreb == 1 && (
        <>
          {values.complaint_file_opened_with_reco_or_trreb_data
            && values.complaint_file_opened_with_reco_or_trreb_data.length > 0
            && (<FieldArray
              name={"complaint_file_opened_with_reco_or_trreb_data"}
              render={(arrayHelper: any) => {
                return values.complaint_file_opened_with_reco_or_trreb_data.map(
                  (item: any, index: any) => {
                    return (
                      <ComplaintFileWithRECOTRREB
                        key={index}
                        item={item}
                        index={index}
                        formik={formik}
                        arrayHelper={arrayHelper}
                      />
                    );
                  }
                );
              }}
            />)}

          <AddNewComplaint
            onAdd={() => {
              const originalArray =
                values.complaint_file_opened_with_reco_or_trreb_data;
              originalArray.push(NEW_COMPLAINT_SECTION);
              setFieldValue(
                "complaint_file_opened_with_reco_or_trreb_data",
                originalArray
              );
            }}
          />
        </>
      )}

    </ContentWrapper>
  );
}

export default ComplaintSection;
