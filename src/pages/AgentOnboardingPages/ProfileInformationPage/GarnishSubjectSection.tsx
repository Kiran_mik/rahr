import { Flex } from "@components/Flex";
import { H3 } from "@components/Typography/Typography";
import { Radio } from "antd";
import { FieldArray } from "formik";
import React from "react";
import { ContentWrapper } from "../style";
import AddNewGarnishmentSubject from "./AddNewGarnishmentSubject";
import GarnishmentOrder from "./GarnishmentOrder";
import { NEW_GARNISHMENT_SECTION } from "./helper";

function GarnishSubjectSection(props: { formik: any }) {
  const { formik } = props;
  const { values, handleChange, setFieldValue } = formik;
  return (
    <ContentWrapper className="space-bottom-0 bordernone">
      <Flex direction="column">
        <H3
          text="Are you subject to a garnishment order?"
          className="space-bottom-16 font-500"
          mandatoryIcon="*"
        />
      </Flex>
      <Flex style={{ marginBottom: "16px" }}>
        <Radio.Group
          value={Number(values.subject_to_a_garnishment_order)}
          onChange={handleChange}
          id={"subject_to_a_garnishment_order"}
          name={"subject_to_a_garnishment_order"}
        >
          <Radio value={1}>Yes</Radio>
          <Radio value={0}>No</Radio>
        </Radio.Group>
      </Flex>
      {values.subject_to_a_garnishment_order == 1 && (
        <>
          {values.subject_to_a_garnishment_order_data
            && values.subject_to_a_garnishment_order_data.length > 0
            && (<FieldArray
              name={"subject_to_a_garnishment_order_data"}
              render={(arrayHelper: any) => {
                return values.subject_to_a_garnishment_order_data.map(
                  (item: any, index: any) => {
                    return <GarnishmentOrder
                      key={index}
                      item={item}
                      index={index}
                      formik={formik}
                      arrayHelper={arrayHelper} />;
                  });
              }}
            />)}
          <AddNewGarnishmentSubject
            onAdd={() => {
              const originalArray = values.subject_to_a_garnishment_order_data;
              originalArray.push({...NEW_GARNISHMENT_SECTION});
              setFieldValue("subject_to_a_garnishment_order_data", originalArray);
            }}
          />
        </>
      )}
    </ContentWrapper>
  );
}

export default GarnishSubjectSection;
