import React from "react";
import { FormLableTypography } from "@components/Typography/Typography";
import { Flex } from "@components/Flex";
import {  Row, Col } from "antd";
import { InputField } from "@components/Input/Input";
import { RemoveButton } from "../style";
import RemoveIcon from "@assets/icons/delete-bin.svg";

const GarnishmentOrder: React.FC<{
  formik: any;
  index: any;
  arrayHelper: any;
  item: any;
}> = (props: any) => {
  let { index, formik, arrayHelper } = props;
  const { errors, values, handleChange, touched } = formik;

  return (
    <>
      <Flex direction="row" justifyContent={"flex-end"} top={10} bottom={10}>
        {index == 0 ? null : (
          <Flex
            direction="row"
            alignItems={"center"}
            onClick={() => {
              arrayHelper.remove(index);
            }}
          >
            <img src={RemoveIcon} width={15} />
            <RemoveButton>Remove</RemoveButton>
          </Flex>
        )}
      </Flex>
      <Row gutter={16}>
        <Col span={12}>
          <FormLableTypography>
            Name of garnishor<span>*</span>
          </FormLableTypography>
          <InputField
            placeholder=""
            name={`subject_to_a_garnishment_order_data.${index}.name_garnishor`}
            id="name_garnishor"
            onChange={handleChange}
            value={values.subject_to_a_garnishment_order_data[index].name_garnishor}
            error={
              touched
              && touched.subject_to_a_garnishment_order_data 
              && touched.subject_to_a_garnishment_order_data[index]
              && touched.subject_to_a_garnishment_order_data[index].name_garnishor
              && errors
              && errors.subject_to_a_garnishment_order_data 
              && errors.subject_to_a_garnishment_order_data[index]
              && errors.subject_to_a_garnishment_order_data[index].name_garnishor
            }
          />
        </Col>
        <Col span={12}>
          <FormLableTypography>
            Amount of garnishment<span>*</span>
          </FormLableTypography>
          <InputField
            placeholder=""
            name={`subject_to_a_garnishment_order_data.${index}.amount_garnishment`}
            id="amount_garnishment"
            onChange={handleChange}
            value={values.subject_to_a_garnishment_order_data[index].amount_garnishment}
            error={
              touched
              && touched.subject_to_a_garnishment_order_data 
              && touched.subject_to_a_garnishment_order_data[index]
              && touched.subject_to_a_garnishment_order_data[index].amount_garnishment
              && errors
              && errors.subject_to_a_garnishment_order_data 
              && errors.subject_to_a_garnishment_order_data[index]
              && errors.subject_to_a_garnishment_order_data[index].amount_garnishment
            }
          />
        </Col>
      </Row>
    </>
  );
};
export default GarnishmentOrder;
