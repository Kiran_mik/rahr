import React from 'react'
import { Description, H3 } from "@components/Typography/Typography";
import { Flex } from "@components/Flex";
import { ContentWrapper } from "../style";
import TextAreaInput from "@components/Input/TextArea";
import { Row, Col } from 'antd';
const GoalsAtRAHR: React.FC<{
	formik: any;
}> = (props: any) => {
	let { formik } = props;
	const { errors, values, handleChange, touched } = formik;
	return (
		<ContentWrapper className="bordernone">
			<Flex direction="column">
				<H3 text="Please provide your goals for your first 6 months / 1 year / 2 years working at RAHR" className="space-bottom-16 font-500" />
				<Description className="bottom-16" text="**It will help us identify how we can assist you in realizing your goals/achieving future success and improve overall retention." />
			</Flex>
			<Row >
				<Col span={24}>
					<TextAreaInput
						name="your_goal"
						id="your_goal"
						onChange={handleChange}
						value={values.your_goal}
						error={
							touched.your_goal
							&& errors.your_goal
						}
						className="rounded" />
				</Col>
			</Row>

		</ContentWrapper>
	)
}

export default GoalsAtRAHR;