import React from "react";
import { H3 } from "@components/Typography/Typography";
import { Flex } from "@components/Flex";
import { ContentWrapper } from "../style";
import SelectInput from "@components/Select";
import { Select } from "antd";
const { Option } = Select;

export default function JoinRAHR(props: any) {
  let { rahrAttractionLists, formik } = props;
  let { values, setFieldValue, errors, touched } = formik;
  return (
    <ContentWrapper>
      <Flex direction="column">
        <H3
          text="What attracted you to join RAHR?"
          className="space-bottom-16 font-500"
          mandatoryIcon="*"
        />
      </Flex>
      <Flex direction="column">
        <SelectInput
          placeholdertitle="Select from a list"
          className="bottom-0"
          id="attracted_to_join_rahr_id"
          name="attracted_to_join_rahr_id"
          onChange={(e: any) => {
            setFieldValue("attracted_to_join_rahr_id", e);
          }}
          error={
            touched.attracted_to_join_rahr_id &&
            errors.attracted_to_join_rahr_id
          }
          value={
            values.attracted_to_join_rahr_id
              ? Number(values.attracted_to_join_rahr_id)
              : undefined
          }
        >
          {rahrAttractionLists.map((item: any) => <Option key={item.id} value={item.id}>{item.status}</Option>)}
        </SelectInput>
      </Flex>
    </ContentWrapper>
  );
}
