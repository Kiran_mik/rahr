import React from "react";
import {  H3, } from "@components/Typography/Typography";
import { Flex } from "@components/Flex";
import { ContentWrapper } from "../style";

import { Radio, Row } from "antd";


const JoinRAHRAsAgent=(props: any)=> {
	let {  formik } = props;
	let { values, handleChange } = formik;
  
	return (
		<ContentWrapper>
			<Flex direction="column">

				<H3 text="Have you ever joined RAHR as an agent?" className="space-bottom-16 font-500" />
				<Row >
					<Radio.Group
						value={Number(values.ever_joined_rahr_as_an_agent)}
						onChange={handleChange}
						id="ever_joined_rahr_as_an_agent"
						name="ever_joined_rahr_as_an_agent"

					>
						<Radio value={0}>No, this is my first time joining</Radio>
						<Radio value={1}>Yes, I was previously an agent with RAHR</Radio>
					</Radio.Group>
				</Row>


			</Flex>
		</ContentWrapper>

	);
};

export default JoinRAHRAsAgent;
