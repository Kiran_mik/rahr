import { FieldArray } from "formik";
import React from "react";
import AddNewUnpaidJudgement from "./AddNewUnpaidJudgement";
import UnpaidJudgement from "./UnpaidJudgement";
import { ContentWrapper } from "../style";
import { Radio } from "antd";
import { H3 } from "@components/Typography/Typography";
import { Flex } from "@components/Flex";

function JudgementSection(props: { formik: any }) {
  const { formik } = props;
  const { values, setFieldValue, handleChange } = formik;
  return (
    <ContentWrapper className="space-bottom-0 bordernone">
      <Flex direction="column">
        <H3
          text="Are there any unpaid judgments against you?"
          className="space-bottom-16 font-500"
          mandatoryIcon="*"
        />
      </Flex>
      <Flex style={{ marginBottom: "16px" }}>
        <Radio.Group
          value={Number(values.unpaid_judgements_against_you)}
          onChange={handleChange}
          id={"unpaid_judgements_against_you"}
          name={"unpaid_judgements_against_you"}
        >
          <Radio value={1}>Yes</Radio>
          <Radio value={0}>No</Radio>
        </Radio.Group>
      </Flex>
      {values.unpaid_judgements_against_you == 1 && (
        <>
          {values.unpaid_judgements_against_you_data
            && values.unpaid_judgements_against_you_data.length > 0
            && (<FieldArray
            name={"unpaid_judgements_against_you_data"}
            render={(arrayHelper: any) => {
              return values.unpaid_judgements_against_you_data.map(
                (item: any, index: any) => {
                  return (
                    <UnpaidJudgement
                      item={item}
                      key={index}
                      index={index}
                      formik={formik}
                      arrayHelper={arrayHelper}
                    />
                  );
                }
              );
            }}
          />)}
          <AddNewUnpaidJudgement
            onAdd={() => {
              const originalArray = values.unpaid_judgements_against_you_data;
              originalArray.push({
                name_judgment_creditor: "",
                judgment_amount: "",
              });
              setFieldValue("unpaid_judgements_against_you_data", originalArray);
            }}
          />
        </>
      )}

    </ContentWrapper>
  );
}
export default JudgementSection;
