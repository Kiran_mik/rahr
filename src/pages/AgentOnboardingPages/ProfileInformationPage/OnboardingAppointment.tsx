import React from "react";
import { Flex } from "@components/Flex";
import { ContentWrapper } from "../style";
import { Radio } from "antd";
import { H3 } from "@components/Typography/Typography";
import { AgentNotificationEnum } from "@constants/onboardingConstants";

const OnboardingAppointment: React.FC<{
  formik: any;
}> = (props: any) => {
  const { values, handleChange } = props.formik;
  return (
    <ContentWrapper>
      <Flex direction="column">
        <H3
          text="How do you want to be notified about onboarding appointment?"
          className="space-bottom-16 font-500"
          mandatoryIcon="*"
        />
      </Flex>
      <Flex>
        <Radio.Group
          defaultValue={1}
          value={Number(values.notification_type)}
          onChange={handleChange}
          id={"notification_type"}
          name={"notification_type"}
        >
          <Radio value={1}>{AgentNotificationEnum[1]}</Radio>
          <Radio value={2}>{AgentNotificationEnum[2]}</Radio>
          <Radio value={3}>{AgentNotificationEnum[3]}</Radio>
        </Radio.Group>
      </Flex>
    </ContentWrapper>
  );
}

export default OnboardingAppointment;