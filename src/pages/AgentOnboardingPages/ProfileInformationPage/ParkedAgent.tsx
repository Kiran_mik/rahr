import React, { useState } from "react";
import { Radio } from "antd";
import useSWR from "swr";
import SignupFormLink from "@components/SignupFormLink";
import DocumentImg from "@assets/icon-form.svg";
import { H3 } from "@components/Typography/Typography";
import { Flex } from "@components/Flex";
import { ContentWrapper } from "../style";

import { getDocuSignURL, getQueryParams } from "@utils/helpers";
import { checkDocuSign, getDocuSignLink } from "@services/UserService";
import DocuSign from "@components/DocuSign";

const ParkedDocuSign = (props: any) => {
  const { formik } = props;
  const { values,errors, handleChange } = formik;
  const [showLoader, setShowLoader] = useState(false);
  const moveToParkEnvId = getQueryParams("envelope_id");
  const docuSignURL = getDocuSignURL("Parked Agent Form&number=1");
  const { data, error } = useSWR(
    [
      docuSignURL,
      // moveToParkEnvId || values.join_as_a_parked_agent_signed_document,
      true
    ],
    getDocuSignLink,
    {
      revalidateOnFocus: false,
    }
  );
  const docuSignPlatoformURL = data;

  const checkDocuSignEnvolpe = async (envId: string) => {
    setShowLoader(true);

    const res = (await checkDocuSign(
      {
        envelope_id: values.board_id_form_to_sign_envelope_id || envId,
        type: "agent-profile-info_1",
        event: "signing_complete",
      },
      "/profile-info/docusign"
    )) as { success: boolean; data: string };
    if (res.success) {
      formik.setFieldValue("join_as_a_parked_agent_signed_document", res.data);
    }
    setShowLoader(false);
  };
  const isValidating = !data;

  const isLoading =
    (isValidating &&
      !values.join_as_a_parked_agent_sign_url &&
      !values.join_as_a_parked_agent_signed_document) ||
    showLoader;

  return (
    <Flex direction="column">
      <DocuSign
        text="Move to park form"
        linktext="Sign form"
        // isLoading={isLoading}
        image={DocumentImg}
        error={errors.join_as_a_parked_agent_signed_document}
        link={docuSignPlatoformURL}
        docURL={values.join_as_a_parked_agent_signed_document}
      />
    </Flex>
  );
};

export default function ParkedAgent(props: any) {
  const { formik } = props;
  const { values, handleChange } = formik;

  return (
    <ContentWrapper>
      <Flex direction="column">
        <H3
          text="Do you want to join as a parked agent? "
          className="space-bottom-16 font-500"
          mandatoryIcon="*"
        />
      </Flex>
      <Flex style={{ marginBottom: "16px" }}>
        <Radio.Group
          value={Number(values.join_as_a_parked_agent)}
          onChange={handleChange}
          id={"join_as_a_parked_agent"}
          name={"join_as_a_parked_agent"}
        >
          <Radio value={1}>Yes, please move me to parked status</Radio>
          <Radio value={0}>No</Radio>
        </Radio.Group>
      </Flex>
      {values.join_as_a_parked_agent == 1 && <ParkedDocuSign formik={formik} />}
    </ContentWrapper>
  );
}
