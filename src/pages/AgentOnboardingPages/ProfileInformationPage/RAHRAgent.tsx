import React from "react";
import { FormLableTypography, H3 } from "@components/Typography/Typography";
import { Flex } from "@components/Flex";
import { ContentWrapper } from "../style";
import { Radio, Row, Col } from "antd";
import SelectInput from "@components/Select";
import { InputField } from "@components/Input/Input";
import SignupFormLink from "@components/SignupFormLink";
import DocumentImg from "@assets/icon-form.svg";
import { AGENT_STATUS } from "./helper";
const RAHRAgent: React.FC<{
  formik: any;
  agentStatus: number
}> = (props: any) => {
  const { agentStatus, formik: { errors, values, handleChange, touched } } = props;

  if(agentStatus == AGENT_STATUS.TRANSFER && values.ever_joined_rahr_as_an_agent === 1) return null;
  return (
    <ContentWrapper>
      <Flex direction="column">
        <H3
          text="Have you been referred by RAHR Agent? "
          className="space-bottom-16 font-500"
        />
      </Flex>
      <Flex style={{ marginBottom: "16px" }}>
        <Radio.Group
          value={Number(values.referred_by_agent)}
          onChange={handleChange}
          id={"referred_by_agent"}
          name={"referred_by_agent"}
        >
          <Radio value={1}>Yes</Radio>
          <Radio value={0}>No</Radio>
        </Radio.Group>
      </Flex>

      {values.referred_by_agent == 1 && (
        <>
          {agentStatus == AGENT_STATUS.NEW_REGISTRATION && (
            <>
              <Row gutter={16}>
                <Col span={12}>
                  <FormLableTypography>
                    RAH agent name <span>*</span>
                  </FormLableTypography>
                  <InputField
                    id="referred_rah_agent_name"
                    name="referred_rah_agent_name"
                    onChange={handleChange}
                    value={values.referred_rah_agent_name}
                    error={
                      touched.referred_rah_agent_name && errors.referred_rah_agent_name
                    }
                  />
                </Col>
                <Col span={12}>
                  <FormLableTypography>
                    RAH agent email address <span>*</span>
                  </FormLableTypography>
                  <InputField
                    id="referred_rah_agent_email"
                    name="referred_rah_agent_email"
                    onChange={handleChange}
                    value={values.referred_rah_agent_email}
                    error={
                      touched.referred_rah_agent_email &&
                      errors.referred_rah_agent_email
                    }
                  />
                </Col>
              </Row>
              <Row gutter={16}>
                <Col span={12}>
                  <FormLableTypography>Branch office</FormLableTypography>
                  <SelectInput
                    className="bottom-0"
                    placeholdertitle="Select from a list"
                    name="referred_agent_branch_office_id"
                    id="referred_agent_branch_office_id"
                    value={values.referred_agent_branch_office_id}
                    error={
                      touched.referred_agent_branch_office_id &&
                      errors.referred_agent_branch_office_id
                    }
                  />
                </Col>
                <Col span={12}>
                  <FormLableTypography>RAH agent cell number</FormLableTypography>
                  <InputField
                    className="bottom-0"
                    placeholder="+1 ( _ _ _ ) _ _ _ - _ _ _ _ "
                    name="referred_rah_agent_cell_no"
                    id="referred_rah_agent_cell_no"
                    value={values.referred_rah_agent_cell_no && values.referred_rah_agent_cell_no}
                    onChange={handleChange}
                    error={
                      touched.referred_rah_agent_cell_no &&
                      errors.referred_rah_agent_cell_no
                    }
                  />
                </Col>
              </Row>
            </>
          )}
          {agentStatus == AGENT_STATUS.TRANSFER  && (
            <SignupFormLink
              text="Agent referral program"
              linktext="Sign form"
              image={DocumentImg}
              redirects="/sign-up"
            />
          )}
        </>
      )}

    </ContentWrapper>
  );
};

export default RAHRAgent;
