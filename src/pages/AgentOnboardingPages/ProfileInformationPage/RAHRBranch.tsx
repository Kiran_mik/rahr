import { UpdateBtn } from '@components/Buttons';
import { Flex } from '@components/Flex';
import MapContainer from '@components/MapContainer/MapContainer';
import { FormValue, H3, SmallText } from '@components/Typography/Typography';
import { navigate } from '@reach/router';
import React, { useEffect, useRef, useState } from 'react';
import { useSelector } from 'react-redux';
import { ContentWrapper } from '../style';



const RAHRBranch: React.FC<{
  formik: any;
}> = (props: any) => {
  const { values: { user_branch } } = props.formik;
  const selectedBranch = useSelector(
    (state: any) => state.rawData.selectedBranchByAgent || {}
  );
  const ref: any = useRef(null);
  const [mapWidth, setMapWidth] = useState(0);


  useEffect(() => {
    if (user_branch)
      setMapWidth(ref.current.offsetWidth);
  }, [user_branch]);

  if (!user_branch) return null;
  return (
    <ContentWrapper>
      {/* <ChangeBranchModal/> */}
      <Flex direction="column">
        <H3
          text="Which RAHR branch did you want to join?"
          className="space-bottom-16 font-500"
          mandatoryIcon="*"
        />
      </Flex>
      <Flex>
        <Flex direction="column" flex={1}>
          <SmallText text="Selected branch" />
          <FormValue
            className="font-400"
            text={selectedBranch.branch_name || user_branch.branch_name}
          />
        </Flex>
        <Flex direction="column" flex={1}>
          <SmallText text="Branch broker code" />
          <FormValue className="font-400" text={user_branch.broker_code || "-"} />
        </Flex>
        <Flex direction="column">
          <UpdateBtn
            text="Change the branch"
            onClick={() => {
              navigate("/agent-onboard/profile-info/branches/", {
                state: {
                  branch: selectedBranch || user_branch
                }
              });

            }}
          />
        </Flex>
      </Flex>
      <Flex
        direction="column"
        top={20}
        ref={ref}
        style={{
          width: "100%",
        }}
      >
        <div>
          <MapContainer
            width={mapWidth}
            coord={[{ lat: (user_branch && user_branch.latitude) || null, lng: (user_branch && user_branch.longitude) || null }]}
          />
          {/* <MapContainer width={mapWidth} /> */}
        </div>
      </Flex>
    </ContentWrapper>
  );
};

export default RAHRBranch;
