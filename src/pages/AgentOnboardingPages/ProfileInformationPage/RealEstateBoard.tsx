import React, { useEffect, useState } from "react";
import { FormLableTypography, H3 } from "@components/Typography/Typography";
import { Flex } from "@components/Flex";
import { ContentWrapper } from "../style";
import { Col, Row, Spin } from "antd";
import { TooltipText } from "@components/Tooltip";
import { InputField } from "@components/Input/Input";
import { getBranchBoards } from "./helper";
import RoundedCheckbox from "@components/RoundedCheckbox";


const RealEstateBoard: React.FC<{
	formik: any;
}> = (props: any) => {
	const { errors, values, handleChange, touched, setFieldValue } = props.formik;
	const [isLoading, setIsLoading] = useState(true);
	const [branchBoards, setBranchBoards] = useState([]);
	useEffect(() => {
		const fetchBranchBoards = async () => {
			setIsLoading(true)
			if (values.user_branch) {
				const res = await getBranchBoards(values.user_branch.id);
				setBranchBoards(res);
				setFieldValue("has_boards", true);
			}
			setIsLoading(false)

		}
		fetchBranchBoards();
	}, [values.user_branch]);
	if (!values.user_branch) return null;

	const handleBoardToggle = (board: any) => {
		let selectedBoard = [...values.board_id_form_to_sign];
		const selectedBoardIndex = selectedBoard.findIndex((boardItem: { board_id: any; }) => boardItem && boardItem.board_id == board.board_id)
		console.log(selectedBoardIndex)
		if (selectedBoardIndex == -1)
			selectedBoard.push({ board_id: board.board_id, number: "", abbreviation: board.abbreviation });
		else
			selectedBoard.splice(selectedBoardIndex)
		setFieldValue("board_id_form_to_sign", [...selectedBoard]);
	}
	return (
		<ContentWrapper>
			<Flex>
				<H3 text="What real estate board do you belong to?" className="space-bottom-16 font-500" mandatoryIcon="*" />
				<TooltipText text="If the agent is moving to parked, they won't need to see this section since they will not be registered with the board" />
			</Flex>
			{isLoading ? <Spin /> : (
				<>
					<Flex>
						{branchBoards.map((board: any) => {
							const selectedBoardIndex = values.board_id_form_to_sign.findIndex((boardItem: { board_id: any; }) => boardItem && boardItem.board_id == board.board_id)
							return (<RoundedCheckbox
								name={board.abbreviation}
								id={board.board_id}
								checked={selectedBoardIndex != -1}
								onChange={() => handleBoardToggle(board)}
								key={board.board_id}
							/>)
						})}
					</Flex>
					<Flex>
						{/* to be updated as error by sweta */}
						{touched.board_id_form_to_sign && typeof (errors.board_id_form_to_sign) === "string" && errors.board_id_form_to_sign}
					</Flex>
					<Row gutter={16}>

						{values.board_id_form_to_sign && values.board_id_form_to_sign.map((board: any, index: number) => {
							return (
								<Col span={12} key={board.board_id}>
									<FormLableTypography>
										{board.abbreviation} number <span>*</span>
									</FormLableTypography>
									<InputField
										placeholder=""
										id="number"
										name={`board_id_form_to_sign.${index}.number`}
										onChange={handleChange}
										value={values.board_id_form_to_sign[index].number}
										error={
											touched
											&& touched.board_id_form_to_sign
											&& touched.board_id_form_to_sign[index]
											&& touched.board_id_form_to_sign[index].number
											&& errors
											&& errors.board_id_form_to_sign
											&& errors.board_id_form_to_sign[index]
											&& errors.board_id_form_to_sign[index].number
										}

									/>
								</Col>
							)
						}
						)}
					</Row>
				</>)}

		</ContentWrapper>

	);
};

export default RealEstateBoard;
