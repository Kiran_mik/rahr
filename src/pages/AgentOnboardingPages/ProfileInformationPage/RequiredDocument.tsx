import React, { useState } from "react";
import { FormLableTypography, H3 } from "@components/Typography/Typography";
import { Flex } from "@components/Flex";
import { ContentWrapper } from "../style";

import { Row, Col } from "antd";
import { FileInput } from "@components/FileInput/index";

const RequiredDocument: React.FC<{
  formik: any;
}> = (props: any) => {
  const { errors, values,  setFieldValue } = props.formik;
  const [policeRecordImageName, setPoliceRecordImageName] = useState("");
  const [officialTranscriptImageName, setOfficialTranscriptImageName] = useState("");
  return (
    <ContentWrapper>
      <Flex direction="column">
        <H3
          text="Required documents to submit"
          className="space-bottom-16 font-500"
          mandatoryIcon="*"
        />
      </Flex>
      <Row gutter={16}>
        <Col span={12}>
          <FormLableTypography>
            Police record <span>*</span>
          </FormLableTypography>
          <FileInput
            type="file"
            name="police_record_doc"
            id="police_record_doc"
            onChange={(value: any) => {
              let formData = new FormData();
              formData.append("imageFile", value.target.files[0]);
              setPoliceRecordImageName(value.target.files[0].name);
              setFieldValue("police_record_doc", value.target.files[0]);
            }}
            error={errors.police_record_doc}
            imgname={policeRecordImageName}
            imgURL={values.police_record_doc}
            label={'Police record'}
          />
        </Col>
        <Col span={12}>
          <FormLableTypography>
            Official transcript <span>*</span>
          </FormLableTypography>
          <FileInput
            type="file"
            name="official_transcript_doc"
            id="official_transcript_doc"
            onChange={(value: any) => {
              let formData = new FormData();
              formData.append("imageFile", value.target.files[0]);
              setOfficialTranscriptImageName(value.target.files[0].name);
              setFieldValue("official_transcript_doc", value.target.files[0]);
            }}
            error={errors.official_transcript_doc}
            imgname={officialTranscriptImageName}
            imgURL={values.official_transcript_doc}
            label={'Official transcript'}
          />
        </Col>
      </Row>
    </ContentWrapper>
  );
}

export default RequiredDocument;