import React from "react";
import { Description, FormLableTypography, H3 } from "@components/Typography/Typography";
import { Flex } from "@components/Flex";
import { ContentWrapper, SquareRadioWrapper } from "../style";
import { Radio, Row } from "antd";
import { AGENT_STATUS } from "./helper";
import { TextLableEnum } from "@constants/onboardingConstants";

export const Status = (props: any): JSX.Element => {
	const { agentStatus, formik } = props;
	let { values, handleChange } = formik;
	const textLable = TextLableEnum[agentStatus];
	return (
		<ContentWrapper>
			<Flex direction="column">
				<H3 text="Your status" className="space-bottom-16 font-500" />
				<Description text={textLable} className="bottom-0" />
			</Flex>
			<Flex direction="column">
				{agentStatus == AGENT_STATUS.REINSTATED_WITH_RECO && (<SquareRadioWrapper style={{ marginTop: "16px" }}>
					<FormLableTypography style={{ fontWeight: 500 }}>
						Please select the following <span>*</span>
					</FormLableTypography>
					<Row className="squareRadioBtn">
						<Radio.Group
							id="reinstated_with_reco_options"
							name="reinstated_with_reco_options"
							value={Number(values.reinstated_with_reco_options)}
							onChange={handleChange}>
							<Radio value={1}>Transfer from another agency</Radio>
							<Radio value={0}>Rejoining RAHR</Radio>
						</Radio.Group>
					</Row>
				</SquareRadioWrapper>
				)}
			</Flex>
		</ContentWrapper>

	);
};

export default Status;
