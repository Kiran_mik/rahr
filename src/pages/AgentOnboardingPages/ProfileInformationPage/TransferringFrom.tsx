
import React, { useState } from "react";
import { FormLableTypography, H3 } from "@components/Typography/Typography";
import { Flex } from "@components/Flex";
import { ContentWrapper } from "../style";
import { Row, Col, } from "antd";
import { InputField } from "@components/Input/Input";
import DatePickerComponent from "@components/DatePicker";
import { FileInput } from "@components/FileInput";

const TransferringFrom: React.FC<{
	formik: any;
}> = (props: any) => {
	const { errors, values, handleChange, setFieldValue } = props.formik;
	const [resignationLetterImageName, setResignationLetterImageName] = useState("");
	
	return (
		<ContentWrapper>
			<Flex direction="column">
				<H3
					text="Where are you transferring from? "
					className="space-bottom-16 font-500"
					mandatoryIcon="*"
				/>
			</Flex>

			<Row gutter={16}>
				<Col span={12}>
					<FormLableTypography>
						Brokerage name <span>*</span>
					</FormLableTypography>
					<InputField
						placeholder=""
						id="transferring_from_brokerage_name"
						name="transferring_from_brokerage_name"
						onChange={handleChange}
						value={values.transferring_from_brokerage_name}
						error={errors.transferring_from_brokerage_name}
					/>
				</Col>
				<Col span={12}>
					<FormLableTypography>
						Resignation date <span>*</span>
					</FormLableTypography>
					<DatePickerComponent
						name="transferring_from_resignation_date"
						id="transferring_from_resignation_date"
						error={errors.transferring_from_resignation_date}
						onChange={(value: any) => {
							setFieldValue("transferring_from_resignation_date", value);
						}}
						value={values.transferring_from_resignation_date}
					/>
				</Col>
			</Row>
			<Row gutter={16}>
				<Col span={24}>
					<FormLableTypography>
						Upload resignation letter <span>*</span>
					</FormLableTypography>
					<FileInput
						type="file"
						name="transferring_resignation_letter"
						id="transferring_resignation_letter"
						onChange={(value: any) => {
							let formData = new FormData();
							formData.append("imageFile", value.target.files[0]);
							setResignationLetterImageName(value.target.files[0].name);
							setFieldValue("transferring_resignation_letter", value.target.files[0]);
						  }}
						  error={errors.transferring_resignation_letter}
						  imgname={resignationLetterImageName}
						  imgURL={values.transferring_resignation_letter}
						  label={'Official transcript'}						
					/>
				</Col>
			</Row>
		</ContentWrapper>
	);
};

export default TransferringFrom;
