import React from "react";
import { FormLableTypography, H3 } from "@components/Typography/Typography";
import { Flex } from "@components/Flex";
import { Row, Col } from "antd";
import { InputField } from "@components/Input/Input";
import RemoveIcon from "@assets/icons/delete-bin.svg";
import { RemoveButton } from "../style";

const UnpaidJudgement: React.FC<{
  formik: any;
  index: any;
  arrayHelper: any;
  item: any;
}> = (props: any) => {
  let { index, formik, arrayHelper } = props;
  const { errors, values, handleChange,touched } = formik;
  return (
    <>
      <Flex direction="row" justifyContent={"flex-end"} top={10} bottom={10}>
        {index == 0 ? null : (
          <Flex
            direction="row"
            alignItems={"center"}
            onClick={() => {
              arrayHelper.remove(index);
            }}
          >
            <img src={RemoveIcon} width={15} />
            <RemoveButton>Remove</RemoveButton>
          </Flex>
        )}
      </Flex>
      <Row gutter={16}>
        <Col span={12}>
          <FormLableTypography>
            Name of judgment creditor<span>*</span>
          </FormLableTypography>
          <InputField
            placeholder=""
            name={`unpaid_judgements_against_you_data.${index}.name_judgment_creditor`}
            id="name_judgment_creditor"
            onChange={handleChange}
            value={values.unpaid_judgements_against_you_data[index].name_judgment_creditor}
            error={
              touched 
              && touched.unpaid_judgements_against_you_data 
              && touched.unpaid_judgements_against_you_data[index] 
              && touched.unpaid_judgements_against_you_data[index].name_judgment_creditor
              && errors 
              && errors.unpaid_judgements_against_you_data 
              && errors.unpaid_judgements_against_you_data[index] 
              && errors.unpaid_judgements_against_you_data[index].name_judgment_creditor
            }
          />
        </Col>
        <Col span={12}>
          <FormLableTypography>
            Amount of judgment<span>*</span>
          </FormLableTypography>
          <InputField
            placeholder=""
            name={`unpaid_judgements_against_you_data.${index}.judgment_amount`}
            id="judgment_amount"
            onChange={handleChange}
            value={values.unpaid_judgements_against_you_data[index].judgment_amount}
            error={
              touched
              && touched.unpaid_judgements_against_you_data 
              &&  touched.unpaid_judgements_against_you_data[index] 
              && touched.unpaid_judgements_against_you_data[index].judgment_amount
            && errors 
              &&  errors.unpaid_judgements_against_you_data 
              && errors.unpaid_judgements_against_you_data[index] 
              && errors.unpaid_judgements_against_you_data[index].judgment_amount}
          />
        </Col>
      </Row>
    </>
  );
};
export default UnpaidJudgement;
