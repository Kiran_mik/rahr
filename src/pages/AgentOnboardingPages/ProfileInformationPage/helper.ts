import services from "@services/index";
import { getMasterStatuses } from "@services/masterStatuses";
import { AGENT_ONBOARDING_BASE_URL } from "../constants";
import * as Yup from "yup";
import moment from "moment";
import { updateUserStore } from "@services/UserService";
import { BASE_URL_MAP } from "@constants/index";

export const validationSchema = Yup.object({
  //Agent status
  agent_status: Yup.number(),

  //Where are you transferring from?
  transferring_from_brokerage_name: Yup
    .string()
    .nullable()
    .when("agent_status", {
      is: (agent_status: number) => agent_status == AGENT_STATUS.TRANSFER,
      then: Yup.string().nullable().required("Please enter previous brokerage name"),
      otherwise: Yup.string().nullable(),
    }),

  transferring_from_resignation_date: Yup
    .string()
    .nullable()
    .when("agent_status", {
      is: (agent_status: number) => agent_status == AGENT_STATUS.TRANSFER,
      then: Yup.string().nullable().required("Please enter date "),
      otherwise: Yup.string().nullable(),
    }),

  transferring_resignation_letter: Yup
    .mixed()
    .nullable()
    .when("agent_status", {
      is: (agent_status: number) => agent_status == AGENT_STATUS.TRANSFER,
      then: Yup.mixed().nullable().required("Please upload resignation letter"),
      otherwise: Yup.mixed().nullable(),
    }),
  //-------end---------

  //Are you a broker or sales representative?
  broker_or_sales_representative: Yup.number(),
  primary_area_of_specialization_id: Yup
    .string()
    .nullable()
    .required(
      "Please `select areas you specialize in` from menu"
    ),
  board_id_form_to_sign_signed_document: Yup
    .string()
    // .required(
    //   "Please fill this form"
    // )
    .nullable(),

  join_as_a_parked_agent_signed_document: Yup
    .string().when(['join_as_a_parked_agent'], {
      is: (join_as_a_parked_agent: any) => join_as_a_parked_agent,
      then: Yup
        .string().required(
          "Please fill this form"
        ).nullable(),
      otherwise: Yup.string().notRequired().nullable()

    })
  ,



  previous_year_transactions: Yup
    .number()
    .typeError('you must specify a number')
    .nullable()
    .when("agent_status", {
      is: (agent_status: number) => agent_status == AGENT_STATUS.TRANSFER,
      then: Yup
        .number()
        .typeError('you must specify a number')
        .nullable()
        .required("Please enter previous year transactions"),
      otherwise: Yup
        .number().typeError('you must specify a number')
        .nullable(),
    }),

  last_year_gross_income: Yup
    .number()
    .typeError('you must specify a number')
    .nullable()
    .when("agent_status", {
      is: (agent_status: number) => agent_status == AGENT_STATUS.TRANSFER,
      then: Yup
        .number()
        .typeError('you must specify a number')
        .nullable().required("Please enter last year gross income "),
      otherwise: Yup.number()
        .typeError('you must specify a number').nullable(),
    }),

  previous_year_payroll_history_report: Yup
    .mixed()
    .nullable()
    .when("agent_status", {
      is: (agent_status: number) => agent_status == AGENT_STATUS.TRANSFER,
      then: Yup.mixed().nullable().required("Please upload previous year payroll history report"),
      otherwise: Yup.mixed().nullable(),
    }),
  //-------end---------

  //What attracted you to join RAHR?
  attracted_to_join_rahr_id:
    Yup
      .number()
      .nullable()
      .when("agent_status", {
        is: (agent_status: number) => (agent_status == AGENT_STATUS.NEW_REGISTRATION
          || agent_status == AGENT_STATUS.REINSTATED_WITH_RECO),
        then: Yup.number().nullable().required("Please `select What attracted you to join RAHR` from menu"),
        otherwise: Yup.number().nullable(),
      }),
  //-------end---------

  //Do you want to join as a parked agent?
  join_as_a_parked_agent: Yup.boolean(),
  // join_as_a_parked_agent_envelope_id: Yup
  //   .string()
  //   .nullable()
  //   .when("join_as_a_parked_agent", {
  //     is: (join_as_a_parked_agent: boolean) => join_as_a_parked_agent,
  //     then: Yup
  //       .string()
  //       .required("Please sign move to park form"),
  //     otherwise: Yup.string().nullable(),
  //   }),

  //-------end---------

  //Have you been referred by RAHR Agent?
  referred_by_agent: Yup.boolean(),
  referred_rah_agent_name: Yup
    .string()
    .nullable()
    .when(["agent_status", "referred_by_agent"], {
      is: ((agent_status: number, referred_by_agent: boolean) =>
        (referred_by_agent && agent_status == AGENT_STATUS.NEW_REGISTRATION)),
      then: Yup.string().nullable().required("Please enter agent name"),
      otherwise: Yup.string().nullable(),
    }),
  referred_rah_agent_email: Yup
    .string()
    .nullable()
    .when(["agent_status", "referred_by_agent"], {
      is: ((agent_status: number, referred_by_agent: boolean) =>
        (referred_by_agent && agent_status == AGENT_STATUS.NEW_REGISTRATION)),
      then: Yup
        .string()
        .nullable()
        .email("Please enter valid email")
        .required("Please enter agent email"),
      otherwise: Yup.string().nullable(),
    }),
  referred_agent_branch_office_id: Yup.string().nullable(),
  referred_rah_agent_cell_no: Yup.number().nullable(),

  // referred_by_agent_envelope_id: Yup
  //   .string()
  //   .nullable()
  //   .when(["referred_by_agent", "agentStatus"], {
  //     is: ((agent_status: number, referred_by_agent: boolean) =>
  //       (referred_by_agent && agent_status == AGENT_STATUS.TRANSFER)),
  //     then: Yup
  //       .string()
  //       .required("Please sign Agent referal program"),
  //     otherwise: Yup.string().nullable(),
  //   }),
  //--------end--------

  //Have you submitted your Application to RECO?
  submit_application_to_reco_to_join_as_a_parked_agentjoin_rahr: Yup.boolean(),
  reco_registration_name: Yup
    .string()
    .nullable()
    .when("submit_application_to_reco_to_join_rahr", {
      is: (submit_application_to_reco_to_join_rahr: boolean) => submit_application_to_reco_to_join_rahr,
      then: Yup.string().nullable().required("Please enter trade name registered under RECO"),
      otherwise: Yup.string().nullable(),
    }),
  reco_registration_number: Yup
    .number()
    .nullable()
    .when("submit_application_to_reco_to_join_rahr", {
      is: (submit_application_to_reco_to_join_rahr: boolean) => submit_application_to_reco_to_join_rahr,
      then: Yup.number().typeError('you must specify a number').nullable().required("Please enter RECO Registration number"),
      otherwise: Yup.number().nullable()
    }),
  reco_legal_name: Yup
    .string()
    .nullable()
    .when("submit_application_to_reco_to_join_rahr", {
      is: (submit_application_to_reco_to_join_rahr: boolean) => submit_application_to_reco_to_join_rahr,
      then: Yup.string().nullable().required("Please enter date "),
      otherwise: Yup.string().nullable(),
    }),
  reco_expiry_date: Yup
    .string()
    .nullable()
    .when("submit_application_to_reco_to_join_rahr", {
      is: (submit_application_to_reco_to_join_rahr: boolean) => submit_application_to_reco_to_join_rahr,
      then: Yup.string().nullable().required("Please enter Year licensed"),
      otherwise: Yup.string().nullable(),
    }),
  reco_year_licensed: Yup
    .string()
    .nullable(),
  //--------end--------

  //Required documents to submit
  //  police_record_doc: Yup.string(),
  //  official_transcript_doc: Yup.string(),
  //--------end--------

  //Your board membership(s)
  //  board_id_form_to_sign: Yup.string(),
  //--------end--------


  // Real estate board numbers
  has_boards: Yup.boolean(),
  board_id_form_to_sign: Yup.array().nullable()
    .when("has_boards", {
      is: true,
      then: Yup.array()
        .of(
          Yup.object().shape({
            abbreviation: Yup.string().required(),
            board_id: Yup.number().required(),
            number: Yup.number().required("Please ented board registration number"),
          })
        )
        .min(1, "Please select board")
    }),
  //--------end--------

  //Have you ever had a complaint file opened with RECO or TRREB?
  complaint_file_opened_with_reco_or_trreb: Yup.boolean(),
  complaint_file_opened_with_reco_or_trreb_data: Yup.array()
    .when("complaint_file_opened_with_reco_or_trreb",
      {
        is: (join_as_a_parked_agent: boolean) => join_as_a_parked_agent,
        then: Yup.array().of(
          Yup.object().shape({
            complaint: Yup.string().required(
              "Please enter  details of the complaint "
            ),
            outcome: Yup.string().required(
              "Please enter  details of the outcome "
            ),
          })
        ),
        otherwise: Yup.array().notRequired().nullable(),
      }
    ),
  //--------end--------

  //Are there any unpaid judgments against you?
  unpaid_judgements_against_you: Yup.boolean(),
  unpaid_judgements_against_you_data: Yup.array().when(
    "unpaid_judgements_against_you",
    {
      is: true,
      then: Yup.array().of(
        Yup.object().shape({
          name_judgment_creditor: Yup.string().nullable().required(
            "Please enter name of judgment creditor"
          ),
          judgment_amount: Yup.number().nullable().typeError('you must specify a number').required(
            "Please enter amount of judgment "
          ),
        })
      ),
      otherwise: Yup.array().notRequired().nullable(),
    }
  ),
  //--------end--------

  //Are you subject to a garnishment order?
  subject_to_a_garnishment_order: Yup.boolean(),
  subject_to_a_garnishment_order_data: Yup.array().when(
    "subject_to_a_garnishment_order",
    {
      is: true,
      then: Yup.array().of(
        Yup.object().shape({
          name_garnishor: Yup.string().required(
            "Please enter name of garnishor"
          ),
          amount_garnishment: Yup.number().nullable().typeError('you must specify a number').required(
            "Please enter amount of garnishment "
          ),
        })
      ),
      otherwise: Yup.array().notRequired().nullable(),
    }
  ),
  //--------end--------

  //How do you want to be notified about onboarding appointment?
  notification_type: Yup.number(),
  //-------end-------

  //Please provide your goals for your first 6 months / 1 year / 2 years working at RAHR
  //end
});
export const NEW_COMPLAINT_SECTION = {
  complaint: "",
  outcome: "",
};
export const NEW_JUDGEMENT_SECTION = {
  name_judgment_creditor: "",
  judgment_amount: "",
};
export const NEW_GARNISHMENT_SECTION = {
  name_garnishor: "",
  amount_garnishment: "",
};

export const AGENT_STATUS = {
  NEW_REGISTRATION: 1,
  TRANSFER: 2,
  REINSTATED_WITH_RECO: 3
}

export const formInitialValues = {
  agent_status: 1,
  broker_or_sales_representative: 1,
  primary_area_of_specialization_id: "",
  attracted_to_join_rahr_id: "",
  join_as_a_parked_agent: 1,
  referred_by_agent: 1,
  referred_rah_agent_name: "",
  referred_agent_branch_office_id: undefined,
  referred_rah_agent_cell_no: undefined,
  referred_rah_agent_email: "",
  submit_application_to_reco_to_join_rahr: 1,
  reco_registration_name: "",
  reco_registration_number: undefined,
  reco_legal_name: "",
  reco_expiry_date: "",
  reco_year_licensed: null,
  police_record_doc: "",
  official_transcript_doc: "",
  complaint_file_opened_with_reco_or_trreb: 1,
  complaint_file_opened_with_reco_or_trreb_data: [NEW_COMPLAINT_SECTION],
  unpaid_judgements_against_you: 1,
  unpaid_judgements_against_you_data: [NEW_JUDGEMENT_SECTION],
  subject_to_a_garnishment_order: 1,
  subject_to_a_garnishment_order_data: [NEW_GARNISHMENT_SECTION],
  notification_type: 1,
  ever_joined_rahr_as_an_agent: 0,
  //not integrted
  attached_real_estate_board_you_belong: "",
  transferring_from_brokerage_name: "",
  previous_year_transactions: null,
  last_year_gross_income: null,
  previous_year_payroll_history_report: "",
  transferring_resignation_letter: "",

  // Board Membership
  has_boards: false,
  board_id_form_to_sign: [],
};


export const getAreasSpecializations = async () => {
  return await getMasterStatuses("AGENT_SPECIALITIES");
};

export const getAttractYouToJoinRAHR = async () => {
  return await getMasterStatuses("AGENT_ATTRACTED_QUESTIONS");
};

export const submitProfileDetailsForm = async (
  values: any,
  currentFormKey: string
) => {
  try {
    const url = currentFormKey;
    const res = await services.post(
      `${AGENT_ONBOARDING_BASE_URL}/${url}`,
      {
        ...values,
        draft: 0

      }
    );
    await updateUserStore()
    return res;
  } catch (error) {
    console.log("error", error);
  }
};



export const mapProfileData = (profileDetailsInfo: any) => {
  let newProfileDetailsInfo = { ...profileDetailsInfo.profile_info }

  //area of specialization
  newProfileDetailsInfo.primary_area_of_specialization_id = newProfileDetailsInfo.primary_area_of_specialization_id && newProfileDetailsInfo.primary_area_of_specialization_id.status_id;

  //attracted to join RAHR
  newProfileDetailsInfo.attracted_to_join_rahr_id = newProfileDetailsInfo.attracted_to_join_rahr_id && newProfileDetailsInfo.attracted_to_join_rahr_id.status_id;

  // branch information
  newProfileDetailsInfo.user_branch = (profileDetailsInfo.user_branch && { ...profileDetailsInfo.user_branch.branch }) || null;

  // complaint section
  let complaint_file_opened_with_reco_or_trreb_data: any = [NEW_COMPLAINT_SECTION];
  if (newProfileDetailsInfo.complaint_file_opened_with_reco_or_trreb
    && newProfileDetailsInfo.complaint_file_opened_with_reco_or_trreb != "null"
    && newProfileDetailsInfo.complaint_file_opened_with_reco_or_trreb != "") {
    complaint_file_opened_with_reco_or_trreb_data
      = JSON.parse(newProfileDetailsInfo.complaint_file_opened_with_reco_or_trreb_data);
  }

  // judgement section
  let unpaid_judgements_against_you_data: any = [NEW_JUDGEMENT_SECTION];
  if (newProfileDetailsInfo.unpaid_judgements_against_you_data
    && newProfileDetailsInfo.unpaid_judgements_against_you_data != "null"
    && newProfileDetailsInfo.complaint_file_opened_with_reco_or_trreb != "") {
    unpaid_judgements_against_you_data
      = JSON.parse(newProfileDetailsInfo.unpaid_judgements_against_you_data);
  }

  // garnishment section
  let subject_to_a_garnishment_order_data: any = [NEW_GARNISHMENT_SECTION];
  if (newProfileDetailsInfo.subject_to_a_garnishment_order_data
    && newProfileDetailsInfo.subject_to_a_garnishment_order_data != "null"
    && newProfileDetailsInfo.complaint_file_opened_with_reco_or_trreb != "") {
    subject_to_a_garnishment_order_data
      = JSON.parse(newProfileDetailsInfo.subject_to_a_garnishment_order_data);
  }
  let board_id_form_to_sign: any = [];
  if (newProfileDetailsInfo.board_id_form_to_sign
    && newProfileDetailsInfo.board_id_form_to_sign != "null"
    && newProfileDetailsInfo.board_id_form_to_sign != "") {
    try {
      board_id_form_to_sign = JSON.parse(newProfileDetailsInfo.board_id_form_to_sign)
    } catch (ex) { }
  }

  return {
    ...newProfileDetailsInfo,
    complaint_file_opened_with_reco_or_trreb_data: complaint_file_opened_with_reco_or_trreb_data,
    unpaid_judgements_against_you_data: unpaid_judgements_against_you_data,
    subject_to_a_garnishment_order_data: subject_to_a_garnishment_order_data,
    referred_rah_agent_name: newProfileDetailsInfo.referred_rah_agent_name || "",
    referred_rah_agent_email: newProfileDetailsInfo.referred_rah_agent_email || "",
    broker_or_sales_representative: newProfileDetailsInfo.broker_or_sales_representative || 1,
    notification_type: newProfileDetailsInfo.notification_type || 1,
    reco_expiry_date: newProfileDetailsInfo.reco_expiry_date ? moment(newProfileDetailsInfo.reco_expiry_date).format("DD-MM-YYYY") : null,
    reco_year_licensed: newProfileDetailsInfo.reco_year_licensed,
    board_id_form_to_sign: typeof (board_id_form_to_sign) != "object" ? [] : board_id_form_to_sign
  }
}



export const generatePayload = (values: any) => {
  let newValues = { ...values };
  if (!newValues.complaint_file_opened_with_reco_or_trreb) {
    newValues.complaint_file_opened_with_reco_or_trreb_data = "";
  }
  if (!newValues.unpaid_judgements_against_you) {
    newValues.unpaid_judgements_against_you_data = "";
  }
  if (!newValues.subject_to_a_garnishment_order) {
    newValues.subject_to_a_garnishment_order_data = "";
  }
  delete newValues.has_boards;
  if (!newValues.referred_by_agent) {
    newValues.referred_rah_agent_name = "";
    newValues.referred_agent_branch_office_id = null;
    newValues.referred_rah_agent_cell_no = null;
    newValues.referred_rah_agent_email = null;
    newValues.referred_by_agent_envelope_id = null;
    newValues.referred_by_agent_template_id = "";
    newValues.referred_by_agent_sign_url = "";
    newValues.referred_by_agent_signed_document = "";
  }
  if (!newValues.submit_application_to_reco_to_join_rahr) {
    newValues.reco_registration_name = "";
    newValues.reco_registration_number = null;
    newValues.reco_legal_name = "";
    newValues.reco_expiry_date = null;
    newValues.reco_year_licensed = "";
  }
  return {
    ...newValues,
    ...{
      attached_real_estate_board_you_belong: "12346",
      board_id_form_to_sign_signed_document: "test.pdf",
      join_as_a_parked_agent_signed_document: "test.pdf",
      referred_by_agent_signed_document: "test.pd"
    }
  }
}


export const getBranchBoards = async (branchId: number) => {
  try {
    const res = await services.get(
      `v1/agents/onboardings/profile-info/boards/${branchId}`,
      BASE_URL_MAP['agent-onboard']
    );
    //@ts-ignore
    return res.data.data;
  } catch (err) {
    return [];
  }
}