/* eslint-disable eqeqeq */
import { BorderBtn, Primary } from "@components/Buttons";
import { Flex } from "@components/Flex";
import { H2Typography } from "@components/Typography/Typography";
import useAgentSteps from "@hooks/useAgentSteps";
import { FlexBox } from "@pages/UserManagementPages/StaffMembers/NewStaff/style";
import { uploadImage } from "@services/UserService";
import { Formik } from "formik";
import React, { useEffect, useState } from "react";
import { STEPS } from "../constants";
import { FormHeader, InnerContainer } from "../style";
import ApplicationRECO from "./ApplicationRECO";
import BoardMembership from "./BoardMembership";
import BrokerSalesRepresentative from "./BrokerSalesRepresentative";
import ComplaintSection from "./ComplaintSection";
import GarnishSubjectSection from "./GarnishSubjectSection";
import GoalsAtRAHR from "./GoalsAtRAHR";
import {
  AGENT_STATUS, formInitialValues, generatePayload, getAreasSpecializations,
  getAttractYouToJoinRAHR, mapProfileData, submitProfileDetailsForm, validationSchema
} from "./helper";
import JoinRAHR from "./JoinRAHR";
import JoinRAHRAsAgent from "./JoinRAHRAsAgent";
import JudgementSection from "./JudgementSection";
import OnboardingAppointment from "./OnboardingAppointment";
import ParkedAgent from "./ParkedAgent";
import RAHRAgent from "./RAHRAgent";
import RAHRBranch from "./RAHRBranch";
import RealEstateBoard from "./RealEstateBoard";
import RequiredDocument from "./RequiredDocument";
import Status from "./Status";
import TransferringFrom from "./TransferringFrom";

function ProfileInformationPage(props: { path: string }) {
  const [setForm, setStep, form] = useAgentSteps();
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const currentStepObject = STEPS[form.currentStep];
  const currentFormKey = currentStepObject && currentStepObject.storeKey;
  const profileDetailsInfo: any =
    (currentFormKey && form[currentFormKey]) || {};

  const [profileDetailsFormInitialValues, setIntialFormValues] = useState(
    formInitialValues
  );

  const [specializeAreasLists, setSpecializeAreasLists] = useState<
    { value: string; key: string }[]
  >([]);
  const [rahrAttractionLists, setrahrAttractionLists] = useState<
    { value: string; key: string }[]
  >([]);

  useEffect(() => {
    fetchData();
    return () => {
      setForm(currentFormKey, { ...profileDetailsInfo, stepId: null });
    };
  }, []);

  useEffect(() => {
    setIntialFormValues({
      ...profileDetailsFormInitialValues,
      ...mapProfileData(profileDetailsInfo),
    });
  }, [profileDetailsInfo.stepId]);

  const fetchData = async () => {
    const areaListRes: any = await getAreasSpecializations();
    const attractListRes: any = await getAttractYouToJoinRAHR();
    setSpecializeAreasLists(areaListRes);
    setrahrAttractionLists(attractListRes);
  };
  const onSubmitHandler = async (values: any) => {
    try {
      setIsLoading(true);
      let previousYearPayrollHistoryReport = values.previous_year_payroll_history_report
        ? await uploadImage(values.previous_year_payroll_history_report)
        : "";

      let transferringResignationLetter = values.transferring_resignation_letter
        ? await uploadImage(values.transferring_resignation_letter)
        : "";

      let police_record_doc = values.police_record_doc
        ? await uploadImage(values.police_record_doc)
        : "";

      let official_transcript_doc = values.official_transcript_doc
        ? await uploadImage(values.official_transcript_doc)
        : "";

      const res: any = await submitProfileDetailsForm(
        generatePayload({
          ...values,
          previous_year_payroll_history_report: previousYearPayrollHistoryReport,
          transferring_resignation_letter: transferringResignationLetter,
          police_record_doc: police_record_doc,
          official_transcript_doc: official_transcript_doc,
        }),
        currentStepObject.key
      );
      setIsLoading(false);
      if (res.status == 200) setStep(form.currentStep + 1);
    } catch (err) {
     }
  };

  if (!currentStepObject) return null;
  return (
    <>
      <FormHeader>
        <Flex>
          <H2Typography>Profile Information</H2Typography>
        </Flex>
      </FormHeader>
      <InnerContainer>
        <Formik
          initialValues={profileDetailsFormInitialValues}
          onSubmit={(values) => {
            onSubmitHandler(values);
          }}
          enableReinitialize={true}
          validationSchema={validationSchema}
        >
          {(formikProps) => {
            const { values, handleSubmit } = formikProps;
            const agentStatus = values.agent_status;
            return (
              <>
                <Status agentStatus={agentStatus} formik={formikProps} />

                {(agentStatus == AGENT_STATUS.TRANSFER ||
                  agentStatus == AGENT_STATUS.REINSTATED_WITH_RECO) && (
                  <TransferringFrom formik={formikProps} />
                )}

                <BrokerSalesRepresentative
                  formik={formikProps}
                  specializeAreasLists={specializeAreasLists}
                  agentStatus={agentStatus}
                />

                {(agentStatus == AGENT_STATUS.NEW_REGISTRATION ||
                  agentStatus == AGENT_STATUS.REINSTATED_WITH_RECO) && (
                  <JoinRAHR
                    formik={formikProps}
                    rahrAttractionLists={rahrAttractionLists}
                  />
                )}

                <RAHRBranch formik={formikProps} />

                <ParkedAgent formik={formikProps} />

                {agentStatus == AGENT_STATUS.TRANSFER && (
                  <JoinRAHRAsAgent formik={formikProps} />
                )}

                {(agentStatus == AGENT_STATUS.NEW_REGISTRATION ||
                  agentStatus == AGENT_STATUS.TRANSFER) && (
                  <RAHRAgent agentStatus={agentStatus} formik={formikProps} />
                )}

                <ApplicationRECO formik={formikProps} />

                {agentStatus == AGENT_STATUS.TRANSFER && (
                  <RealEstateBoard formik={formikProps} />
                )}

                {agentStatus == AGENT_STATUS.NEW_REGISTRATION && (
                  <>
                    <RequiredDocument formik={formikProps} />
                    <BoardMembership formik={formikProps} />
                  </>
                )}

                <ComplaintSection formik={formikProps} />

                <JudgementSection formik={formikProps} />

                <GarnishSubjectSection formik={formikProps} />

                <OnboardingAppointment formik={formikProps} />

                <GoalsAtRAHR formik={formikProps} />

                <FlexBox className="footerbtn">
                  <Primary
                    text="Proceed to the next step"
                    className="submitbtn"
                    style={{ marginRight: "10px" }}
                    onClick={handleSubmit}
                    isLoading={isLoading}
                  />

                  <BorderBtn className="borderbtn" text="Previous step" />
                </FlexBox>
              </>
            );
          }}
        </Formik>
      </InnerContainer>
    </>
  );
}

export default ProfileInformationPage;
