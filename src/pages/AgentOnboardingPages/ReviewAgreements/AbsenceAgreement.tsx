import React, { useEffect, useState } from "react";
import { Flex } from "@components/Flex";
import { Description } from "@components/Typography/Typography";
import { ReviewContentWrapper } from "./style";
import { Checkbox } from "@components/Checkbox";
import { Primary } from "@components/Buttons";
import { FlexBox } from "@pages/UserManagementPages/StaffMembers/NewStaff/style";
import { termValidationSchema, initialValuesForAgreement } from "./helper";
import { useFormik } from "formik";
import { useSelector } from "react-redux";

export default function AbsenceAgreement(props: any) {
  let { contentText, data, onSubmitReview, id } = props;

  const showLoader = useSelector((state: any) => state.rawData.showLoader);
  const [agreemntValues, setAgreemntValues] = useState(
    initialValuesForAgreement
  );

  useEffect(() => {
    if (Object.keys(data).length !== 0) {
      data.agreed = data.agreed == 1 ? true : false;
      setAgreemntValues(data);
    }
  }, [data]);

  const formik = useFormik({
    onSubmit: async (values: any) => {
      onSubmitReview(values, id);
    },
    initialValues: agreemntValues,
    validationSchema: termValidationSchema,
    enableReinitialize: true,
  });

  const { values, errors, handleChange, handleSubmit } = formik;

  return (
    <>
      <ReviewContentWrapper>
        <Description className="bottom-16" text={contentText} />
        <Checkbox
          name={
            "I have read and agree with the above mentioned terms & conditions for  “Absence notification agreement”"
          }
          id={"agreed"}
          onChange={handleChange}
          checked={values.agreed}
          error={errors.agreed}
        />
        <FlexBox className="review-btn">
          <Primary
            text="Confirm agreement"
isDisable={!values.agreed}
            className="submitbtn"
            onClick={handleSubmit}
            isLoading={showLoader}
          />
        </FlexBox>
      </ReviewContentWrapper>
    </>
  );
}
