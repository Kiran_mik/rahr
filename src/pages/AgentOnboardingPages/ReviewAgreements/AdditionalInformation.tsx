import React, { useEffect, useState } from "react";
import { Checkbox } from "@components/Checkbox";
import { Primary } from "@components/Buttons";
import { FlexBox } from "@pages/UserManagementPages/StaffMembers/NewStaff/style";
import SignupFormLink from "@components/SignupFormLink";
import DocumentImg from "@assets/icon-form.svg";
import { H3 } from "@components/Typography/Typography";
import { Flex } from "@components/Flex";
import {
  additionalInfoValidationSchema,
  initialValuesForAgreement,
} from "./helper";
import { useFormik } from "formik";
import { Description } from "@components/Typography/Typography";
import TextAreaInput from "@components/Input/TextArea";
import FormItem from "antd/lib/form/FormItem";
import { ReviewContentWrapper, SkipButton } from "./style";
import { useSelector } from "react-redux";

export default function AdditionalInformation(props: any) {
  let { contentText, data, onSubmitReview, id } = props;
  const showLoader=useSelector((state:any)=>state.rawData.showLoader)

  
  const [agreemntValues, setAgreemntValues] = useState(
    initialValuesForAgreement
  );

  useEffect(() => {
    if (Object.keys(data).length !== 0) {
      setAgreemntValues(data);
    }
  }, [data]);

  const formik = useFormik({
    onSubmit: async (values: any) => {
      values.agreed = 1;
      onSubmitReview(values, id);
    },
    initialValues: agreemntValues,
    validationSchema: additionalInfoValidationSchema,
    enableReinitialize: true,
  });

  const { values, errors, handleChange, handleSubmit, touched } = formik;

  return (
    <>
      <ReviewContentWrapper>
        <Description className="bottom-16" text={props.contentText} />
        <FormItem name="short_note" label="">
          <TextAreaInput
            id="aditional_info"
            name="aditional_info"
            onChange={handleChange}
            value={values.aditional_info}
            error={touched.aditional_info && errors.aditional_info}
          />
        </FormItem>
        <FlexBox className="review-btn">
          <Primary text="Submit" className="submitbtn"
          isLoading={showLoader}
          onClick={handleSubmit} />
          <SkipButton onClick={()=>{handleSubmit()}}>Skip</SkipButton>
        </FlexBox>
      </ReviewContentWrapper>
    </>
  );
}
