import React, { useEffect, useState } from "react";
import { Description } from "@components/Typography/Typography";
import { ReviewContentWrapper } from "./style";
import { Checkbox } from "@components/Checkbox";
import { Primary } from "@components/Buttons";
import { FlexBox } from "@pages/UserManagementPages/StaffMembers/NewStaff/style";
import { termValidationSchema, initialValuesForAgreement } from "./helper";
import { useFormik } from "formik";

import DocumentImg from "@assets/icon-form.svg";
import { H3 } from "@components/Typography/Typography";
import { Flex } from "@components/Flex";
import {
  getDocuSignURL,
  getQueryParams,
  removeQueryParams,
} from "@utils/helpers";
import { checkDocuSign, getDocuSignLink } from "@services/UserService";
import useAgentSteps from "@hooks/useAgentSteps";
import { Spin } from "antd";
import useSWR from "swr";
import DocuSign from "@components/DocuSign";
import { useSelector } from "react-redux";

export default function BrokeragePolicyManual(props: any) {
  let { contentText, onSubmitReview, id } = props;
  const showLoaderState = useSelector((state: any) => state.rawData.showLoader);

  const [showLoader, setShowLoader] = useState(false);

  let contentData = props.data;
  const [agreemntValues, setAgreemntValues] = useState(
    initialValuesForAgreement
  );
  const brokrage_policy_manual_envelopeId = getQueryParams("envelope_id");
  const agreementType = getQueryParams("type");
  const docusignFormTypeFlag =
    agreementType && agreementType.includes("agent-agreement_")
      ? agreementType.split("_")[1]
      : "";

  const url = getDocuSignURL("Brokerage Policy Manual&number=2");
  const [setForm, setStep, form] = useAgentSteps();
  const { data, error } = useSWR(
    [
      url,
      brokrage_policy_manual_envelopeId ||
        contentData.brokerage_policy_manual_signed_document,
    ],
    getDocuSignLink,
    {
      revalidateOnFocus: false,
    }
  );
  const docuSignURLForBrokeragePolicy = data;

  useEffect(() => {
    if (Object.keys(contentData).length !== 0) {
      contentData.agreed = contentData.agreed == 1 ? true : false;
      setAgreemntValues(contentData);
    }

    if (
      brokrage_policy_manual_envelopeId &&
      !contentData.brokerage_policy_manual_signed_document &&
      docusignFormTypeFlag === "2"
    ) {
      checkDocuSignEnvolpe(brokrage_policy_manual_envelopeId);
    }
  }, [
    brokrage_policy_manual_envelopeId,
    contentData.brokerage_policy_manual_envelope_id,
    contentData.brokerage_policy_manual_signed_document,
  ]);

  const formik = useFormik({
    onSubmit: async (values: any) => {
      onSubmitReview(values, id);
    },
    initialValues: agreemntValues,
    validationSchema: termValidationSchema,
    enableReinitialize: true,
  });

  const checkDocuSignEnvolpe = async (envId: string) => {
    setShowLoader(true);

    const res = (await checkDocuSign({
      envelope_id: contentData.brokerage_policy_manual_envelope_id || envId,
      type: "agent-agreement_2",
      event: "signing_complete",
    })) as { success: boolean; data: string };
    if (res.success) {
      formik.setFieldValue("brokerage_policy_manual_signed_document", res.data);
      removeQueryParams();
    }

    setShowLoader(false);
  };

  const { values, errors, handleChange, handleSubmit } = formik;
  const isValidating = !data;

  const isLoading =
    (isValidating &&
      !contentData.brokerage_policy_manual_sign_url &&
      !values.brokerage_policy_manual_signed_document) ||
    showLoader;
  return (
    <>
      <ReviewContentWrapper>
        <Description className="bottom-16" text={contentText} />
        <Flex direction="column">
          {isLoading && <Spin />}
          {!isLoading &&
            (docuSignURLForBrokeragePolicy ||
              contentData.brokerage_policy_manual_sign_url ||
              values.brokerage_policy_manual_signed_document) && (
              <Flex direction="column">
                <H3
                  text="Attached forms"
                  className="space-bottom-16 font-500"
                />
                <DocuSign
                  text="Brokerage policy manual"
                  linktext="View and sign form"
                  image={DocumentImg}
                  link={docuSignURLForBrokeragePolicy}
                  docURL={values.brokerage_policy_manual_signed_document}
                />
              </Flex>
            )}
        </Flex>
        <Flex top={20}>
          <Checkbox
            name={
              "I have read and agree with the above mentioned terms & conditions for  “Brokerage policy manual”"
            }
            id={"agreed"}
            onChange={handleChange}
            checked={values.agreed}
            error={errors.agreed}
          />
        </Flex>
        <FlexBox className="review-btn">
          <Primary
            text="Confirm agreement"
            isDisable={
              !values.agreed || !values.brokerage_policy_manual_signed_document
            }
            className="submitbtn"
            onClick={handleSubmit}
            isLoading={showLoaderState}
          />
        </FlexBox>
      </ReviewContentWrapper>
    </>
  );
}
