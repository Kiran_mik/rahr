import React, { useEffect, useState } from "react";
import { Description } from "@components/Typography/Typography";
import { ReviewContentWrapper } from "./style";
import { Checkbox } from "@components/Checkbox";
import { Primary } from "@components/Buttons";
import { FlexBox } from "@pages/UserManagementPages/StaffMembers/NewStaff/style";
import SignupFormLink from "@components/SignupFormLink";
import DocumentImg from "@assets/icon-form.svg";
import { H3 } from "@components/Typography/Typography";
import { Flex } from "@components/Flex";
import { termValidationSchema, initialValuesForAgreement } from "./helper";
import { useFormik } from "formik";
import DocuSign from "@components/DocuSign";
import {
  getDocuSignURL,
  getQueryParams,
  removeQueryParams,
} from "@utils/helpers";
import useSWR from "swr";
import { checkDocuSign, getDocuSignLink } from "@services/UserService";
import { Spin } from "antd";
import { useSelector } from "react-redux";

export default function IndependentContractor(props: any) {
  const showLoaderState = useSelector((state: any) => state.rawData.showLoader);

  const [showLoader, setShowLoader] = useState(false);
  let { contentText, onSubmitReview, id } = props;
  const contentData = props.data;
  const icaEnvId = getQueryParams("envelope_id");
  const agreementType = getQueryParams("type");
  const docusignFormTypeFlag =
    agreementType && agreementType.includes("agent-agreement_")
      ? agreementType.split("_")[1]
      : "";
  const [agreemntValues, setAgreemntValues] = useState(
    initialValuesForAgreement
  );
  const url = getDocuSignURL("Independent contractor agreement&number=1");
  const { data, error } = useSWR(
    [
      url,
      icaEnvId || contentData.independent_contract_agreement_signed_document,
    ],
    getDocuSignLink,
    {
      revalidateOnFocus: false,
    }
  );
  const docuSignURLForICA = data;

  useEffect(() => {
    if (Object.keys(contentData).length !== 0) {
      contentData.agreed = contentData.agreed == 1 ? true : false;
      setAgreemntValues(contentData);
    }
    if (contentData.independent_contract_agreement_signed_document) {
      formik.setFieldValue(
        "independent_contract_agreement_signed_document",
        contentData.independent_contract_agreement_signed_document
      );
    }
    if (
      icaEnvId &&
      !contentData.independent_contract_agreement_signed_document &&
      docusignFormTypeFlag === "1"
    ) {
      checkDocuSignEnvolpe(icaEnvId);
    }
  }, [
    icaEnvId,
    contentData.independent_contract_agreement_signed_document,
    contentData.independent_contract_agreement_envelope_id,
  ]);

  const formik = useFormik({
    onSubmit: async (values: any) => {
      onSubmitReview(values, id);
    },
    initialValues: agreemntValues,
    validationSchema: termValidationSchema,
    enableReinitialize: true,
  });

  const checkDocuSignEnvolpe = async (envId: string) => {
    setShowLoader(true);
    const res = (await checkDocuSign({
      envelope_id:
        contentData.independent_contract_agreement_envelope_id || envId,
      type: "agent-agreement",
      event: "signing_complete",
    })) as { success: boolean; data: string };
    if (res.success) {
      formik.setFieldValue(
        "independent_contract_agreement_signed_document",
        res.data
      );
      removeQueryParams();
    }
    setShowLoader(false);
  };

  const { values, errors, handleChange, handleSubmit } = formik;
  const isValidating = !data;
  const isLoading =
    (isValidating &&
      !contentData.independent_contract_agreement_sign_url &&
      !values.independent_contract_agreement_signed_document) ||
    showLoader;

  return (
    <>
      <ReviewContentWrapper>
        <Description className="bottom-16" text={props.contentText} />
        {isLoading && <Spin />}

        {!isLoading &&
          (docuSignURLForICA ||
            contentData.independent_contract_agreement_sign_url ||
            values.independent_contract_agreement_signed_document) && (
            <Flex direction="column">
              <H3 text="Attached forms" className="space-bottom-16 font-500" />

              <DocuSign
                text="Independent contractor agreement"
                linktext="ICA"
                image={DocumentImg}
                docURL={values.independent_contract_agreement_signed_document}
                link={docuSignURLForICA}
              />
            </Flex>
          )}
        <Flex top={20}>
          <Checkbox
            name={
              "I have read and agree with the above mentioned terms & conditions for “Brokerage telemarketing policy”"
            }
            id={"agreed"}
            onChange={handleChange}
            checked={values.agreed}
            error={errors.agreed}
          />
        </Flex>
        <FlexBox className="review-btn">
          <Primary
            text="Confirm agreement"
            isDisable={
              !values.agreed ||
              !values.independent_contract_agreement_signed_document
            }
            className="submitbtn"
            onClick={handleSubmit}
            isLoading={showLoaderState}
          />
        </FlexBox>
      </ReviewContentWrapper>
    </>
  );
}
