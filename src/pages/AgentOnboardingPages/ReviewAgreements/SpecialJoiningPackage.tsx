import React, { useEffect, useState } from "react";
import { Flex } from "@components/Flex";
import { Checkbox } from "@components/Checkbox";
import {
  specialJoiningValidationSchema,
  initialValuesForAgreement,
  getContactDetailsPageInfo,
} from "./helper";
import { useFormik } from "formik";
import {
  Description,
  H3,
  FormLableTypography,
} from "@components/Typography/Typography";
import { ReviewContentWrapper } from "./style";
import CheckboxComponent from "../../../shared/components/Checkbox/Checkbox";
import { Primary } from "@components/Buttons";
import { FlexBox } from "@pages/UserManagementPages/StaffMembers/NewStaff/style";
import { Radio, Row, Col } from "antd";
import { InputField } from "@components/Input/Input";
import SpecialJoiningImageOne from "@assets/special_joning_one.svg";
import SpecialJoiningImageTwo from "@assets/special_joning_two.svg";
import useAgentSteps from "@hooks/useAgentSteps";
import { STEPS } from "../constants";
import { useSelector } from "react-redux";

export default function SpecialJoiningPackage(props: any) {
  const [setForm, setStep, form] = useAgentSteps();
  const showLoader = useSelector((state: any) => state.rawData.showLoader);

  const currentFormKey = STEPS[1].storeKey;
  const [fillContactDetailsFlag, setFillContactDetailsFlag] = useState(false);
  const contactDetailsData: any = form[currentFormKey] || {};
  let { contentText, data, onSubmitReview, id } = props;
  const [agreemntValues, setAgreemntValues] = useState(
    initialValuesForAgreement
  );

  useEffect(() => {
    if (Object.keys(data).length !== 0) {
      data.agreed = data.agreed == 1 ? true : false;
      setAgreemntValues(data);
    }
  }, [data]);

  const formik = useFormik({
    onSubmit: async (values: any) => {
      onSubmitReview(values, id);
    },
    initialValues: agreemntValues,
    validationSchema: specialJoiningValidationSchema,
    enableReinitialize: true,
  });

  const { values, errors, handleChange, handleSubmit, touched } = formik;

  const onChangeHandler = async () => {
    let dataObject = agreemntValues;
    if (!fillContactDetailsFlag) {
      if (contactDetailsData && contactDetailsData.profile_detail) {
        const profileData = contactDetailsData.profile_detail;
        dataObject.trade_name = profileData.first_name;
        dataObject.email = contactDetailsData.email;
        dataObject.cellphone_no = profileData.cell_phone_number;
        dataObject.website = profileData.website_url;
      } else {
        const res = (await getContactDetailsPageInfo()) as any;
        const profileData = res.profile_detail;
        dataObject.trade_name = profileData.first_name;
        dataObject.email = res.email;
        dataObject.cellphone_no = profileData.cell_phone_number;
        dataObject.website = profileData.website_url;
      }
    } else {
      dataObject.trade_name = "";
      dataObject.email = "";
      dataObject.cellphone_no = "";
      dataObject.website = "";
    }
    setFillContactDetailsFlag(!fillContactDetailsFlag);
    setAgreemntValues(dataObject);
  };

  return (
    <>
      <ReviewContentWrapper>
        <Flex
          top={20}
          bottom={20}
          direction="row"
          justifyContent={"flex-start"}
        >
          <img src={SpecialJoiningImageOne} alt="" />
          <img
            src={SpecialJoiningImageTwo}
            alt=""
            style={{ marginLeft: "20px" }}
          />
        </Flex>
        <Description className="bottom-16" text={contentText} />
        <H3 text="Attached forms" className="space-bottom-16 font-500" />
        <Flex top={15} bottom={15}>
          <CheckboxComponent
            name={"Same as in “contact information” section"}
            onChange={() => onChangeHandler()}
            checked={fillContactDetailsFlag}
          />
        </Flex>
        <Row gutter={16}>
          <Col span={12}>
            <FormLableTypography>
              Trade name <span>*</span>
            </FormLableTypography>
            <InputField
              id="trade_name"
              name="trade_name"
              onChange={handleChange}
              value={values.trade_name}
              error={touched.trade_name && errors.trade_name}
            />
          </Col>
          <Col span={12}>
            <FormLableTypography>
              Cell phone number <span>*</span>
            </FormLableTypography>
            <InputField
              id="cellphone_no"
              name="cellphone_no"
              onChange={handleChange}
              value={values.cellphone_no}
              error={touched.cellphone_no && errors.cellphone_no}
            />
          </Col>
        </Row>
        <Row gutter={16}>
          <Col span={12}>
            <FormLableTypography>
              Email <span>*</span>
            </FormLableTypography>
            <InputField
              id="email"
              name="email"
              onChange={handleChange}
              value={values.email}
              error={touched.email && errors.email}
            />
          </Col>
          <Col span={12}>
            <FormLableTypography>
              Website
            </FormLableTypography>
            <InputField
              id="website"
              name="website"
              onChange={handleChange}
              value={values.website}
              error={touched.website && errors.website}
            />
          </Col>
        </Row>
        <Checkbox
          name={
            "I have read and agree with the above mentioned terms & conditions for “Brokerage telemarketing policy”"
          }
          id={"agreed"}
          onChange={handleChange}
          checked={values.agreed}
          error={errors.agreed}
        />
        <FlexBox className="review-btn">
          <Primary
            text="Confirm agreement"
isDisable={!values.agreed}
            className="submitbtn"
            onClick={handleSubmit}
            isLoading={showLoader}
          />
        </FlexBox>
      </ReviewContentWrapper>
    </>
  );
}
