import { ObjectStr } from '@utils/helpers';
import services from "@services/index";
import * as Yup from "yup";
import { AGENT_ONBOARDING_BASE_URL } from "../constants";
import { updateUserStore } from '@services/UserService';


export const DOCU_SIGN_FORM_TABS_MAP: ObjectStr = {
  "1": 7,
  "2": 8
}

export const getAgreementList = async (basePath?: string | null, isParked?: number) => {
  try {
    const res = await services.get(`v1/agents/onboardings/review-agreements/list`, basePath || '') as { data: { data: [] } };
    const agreements = res.data.data.map((i: any) => {
      i.show = true;
      if (i.id === 6 && isParked === 1) {
        i.show = false
      }
      return i

    })
    return agreements;
  } catch (err) {
    return [];
  }
};

export const initialValuesForAgreement = {
  agreement_id: 0,
  agent_id: null,
  trade_name: "",
  email: "",
  cellphone_no: "",
  website: "",
  aditional_info: "",
  independent_contract_agreement_envelope_id: null,
  independent_contract_agreement_sign_url: null,
  independent_contract_agreement_signed_document: null,
  brokerage_policy_manual_envelope_id: null,
  brokerage_policy_manual_sign_url: null,
  brokerage_policy_manual_signed_document: null,
  agreed: 0,
};

export const termValidationSchema = Yup.object({
  agreed: Yup.boolean(),
  // agreed: Yup.boolean()
  //   .oneOf([true], "Please accept this term")
  //   .required("Please accept this term"),
});

export const specialJoiningValidationSchema = Yup.object({
  agreed: Yup.boolean(),
  trade_name: Yup.string()
    .required("Please enter trade name")
    .nullable(),
  email: Yup.string()
    .required("Please enter email")
    .nullable(),
  cellphone_no: Yup.string()
    .required("Please enter cell phone number")
    .nullable(),
  website: Yup.string().url('Please enter valid url')
    .nullable(),
});

export const additionalInfoValidationSchema = Yup.object({
  aditional_info: Yup.string().nullable(),
});

/**
 * Post Api for saving the review agreement details
 * @param values - payload of contact details form
 * @param currentFormKey - current form key
 */
export const submitReviewForm = async (values: any, currentFormKey: string) => {
  try {
    const url = currentFormKey;
    const res = await services.post(
      `${AGENT_ONBOARDING_BASE_URL}/${url}`,
      {
        ...values, draft: 0
      }
    );
    await updateUserStore()
    return res;
  } catch (error) {
    console.log("error", error);
  }
};


export const getContactDetailsPageInfo = async () => {
  try {
    const res = await services.get(`${AGENT_ONBOARDING_BASE_URL}/contact-details`) as { data: { data: any } };
    return res.data.data;
  } catch (error) {

  }
}

export const submitReviewAgreemnts = async () => {
  try {
    const res = await services.get(`${AGENT_ONBOARDING_BASE_URL}/review-agreements/submit`) as { data: { data: any } };
    await updateUserStore()
    return res
  } catch (error) {
    return error
  }
}