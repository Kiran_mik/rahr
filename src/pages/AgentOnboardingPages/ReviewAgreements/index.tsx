import { Flex } from "@components/Flex";
import { H3Typography } from "@components/Typography/Typography";
import React, { useEffect, useState } from "react";
import { FormHeader, InnerContainer, ContentWrapper } from "../style";
import AccoridanPanel from "../../../shared/components/AccordianComponent/index";
import { H3 } from "@components/Typography/Typography";
import { Checkbox } from "@components/Checkbox";
import { FlexBox } from "@pages/UserManagementPages/StaffMembers/NewStaff/style";
import { Primary, BorderBtn } from "@components/Buttons";
import {
  DOCU_SIGN_FORM_TABS_MAP,
  getAgreementList,
  submitReviewAgreemnts,
  submitReviewForm,
} from "./helper";
import useAgentSteps from "@hooks/useAgentSteps";
import { STEPS } from "../constants";
import { useSelector } from "react-redux";
import { message } from "antd";

//components
import BrokerageTelemarketingPolicy from "./BrokerageTelemarketingPolicy";
import BrokerageLoadingAuthority from "./BrokerageLoadingAuthority";
import LeadReferralAgreement from "./LeadReferralAgreement";
import AbsenceAgreement from "./AbsenceAgreement";
import JoiningRAHRAgreement from "./JoiningRAHRAgreement";
import ProfessionalDevelopment from "./ProfessionalDevelopment";
import IndependentContractor from "./IndependentContractor";
import BrokeragePolicyManual from "./BrokeragePolicyManual";
import SpecialJoiningPackage from "./SpecialJoiningPackage";
import AdditionalInformation from "./AdditionalInformation";
import { navigate } from "@reach/router";
import { getQueryParams, updateRawData } from "@utils/helpers";
import { getUpdatedReviewAgreements } from "../helper";

function ReviewAgreementsPage(props: { path: string }) {
  const [showSubmitLoader, setShowSubmitLoader] = useState(false);
  const [reviewContentList, setReviewContentList] = useState([{}] as any);
  const [docusignFormType, setDocuSignFormType] = useState<string>("");
  const [setForm, setStep, form] = useAgentSteps();
  const currentStepObject = STEPS[form.currentStep];
  const currentFormKey =
    STEPS[form.currentStep] &&
    STEPS[form.currentStep] &&
    STEPS[form.currentStep].storeKey;
  const reviewAgreementInfo: any = form[currentFormKey];
  const [reviewInfoInitialValues, setIntialFormValues] = useState([{}] as any);
  const [allAgreementStatus, setAllAgreementStatus] = useState(false);

  const user = useSelector((state: { user: any }) => state.user);
   const {   onBoardingStepsStatus } = user;

  const { userId } = user;
  useEffect(() => {
    return () => {
      setForm(currentFormKey, { ...(reviewAgreementInfo || {}), stepId: null });
    };
  }, []);

  useEffect(() => {
    if (reviewAgreementInfo) {
      makeArrayList(reviewAgreementInfo);
    }

    const agreementType = getQueryParams("type");
    const docusignFormTypeFlag =
      agreementType &&
      agreementType.includes("agent-agreement_") &&
      agreementType.split("_")[1];

    if (reviewAgreementInfo) {
      if (docusignFormTypeFlag)
        setDocuSignFormType(
          DOCU_SIGN_FORM_TABS_MAP[docusignFormTypeFlag || 0]
        ); else {

        let firstDocumentNotAgreed = "1";
        Object.keys(reviewAgreementInfo).forEach((key) => {
          if (reviewAgreementInfo[key] && !reviewAgreementInfo[key].agreed) {
            firstDocumentNotAgreed = reviewAgreementInfo[key].agreement_id;
            return;
          }
        });
        setDocuSignFormType(firstDocumentNotAgreed);
      }
    }
  }, [reviewAgreementInfo]);

 useEffect(()=>{
   setAllAgreementStatus( onBoardingStepsStatus && onBoardingStepsStatus[7])
 },[ onBoardingStepsStatus])


  /**
   * seperate step id and make array of remiaining review agreements objects
   */
  const makeArrayList = (reviewAgreementInfo: any) => {
    let data = JSON.parse(JSON.stringify(reviewAgreementInfo));
    if (Object.keys(data).length > 1) {
      let makeArray: any = [];
      Object.keys(data).forEach((key) => {
        if (typeof data[key] === "object") {
          makeArray.push(data[key]);
        }
      });
      setIntialFormValues(makeArray);
    }
  };

  /**
   * Return Component according to their Id
   */
  const getComponentById = (type: any) => {
    switch (type) {
      case 1:
        return BrokerageTelemarketingPolicy;
      case 2:
        return BrokerageLoadingAuthority;
      case 3:
        return LeadReferralAgreement;
      case 4:
        return AbsenceAgreement;
      case 5:
        return JoiningRAHRAgreement;
      case 6:
        return ProfessionalDevelopment;
      case 7:
        return IndependentContractor;
      case 8:
        return BrokeragePolicyManual;
      case 9:
        return SpecialJoiningPackage;
      case 10:
        return AdditionalInformation;
      default:
        return blankView;
    }
  };

  const blankView = () => {
    return <div />;
  };

  useEffect(() => {
    getReviewAgreementListData();
  }, []);

  const getReviewAgreementListData = async () => {
    let res: any = await getAgreementList(null, user.join_as_a_parked_agent);
    setReviewContentList(res);
  };

  const getAgreementDataById = (id: any) => {
    if (reviewInfoInitialValues && reviewInfoInitialValues.length) {
      let findIndex: any = reviewInfoInitialValues.findIndex(
        (item: any) => item && item.agreement_id == id
      );
      return findIndex == -1 ? [] : reviewInfoInitialValues[findIndex];
    } else {
      return [];
    }
  };

  const submitHandler = async (values: any, id: any) => {
    values.agreed = 1;
    values.agreement_id = id;
    values.agent_id = userId;
    updateRawData({ showLoader: true });

    const res: any = await submitReviewForm(
      {
        ...values,
        brokerage_policy_manual_template_id:
          values.brokerage_policy_manual_envelope_id,
      },
      currentStepObject.key
    );
    const updatedReviewAgreements = (await getUpdatedReviewAgreements()) as any;
    setForm(currentFormKey, {
      ...updatedReviewAgreements,
      stepId:
        (updatedReviewAgreements && updatedReviewAgreements.id) ||
        Math.random(),
    });
    updateRawData({ showLoader: false });
    const nextForm = parseInt(docusignFormType || "0") + 1;
    setDocuSignFormType(nextForm.toString());
  };

  const submitApplication = async () => {
    const agreements = { ...reviewAgreementInfo };
    if (agreements.stepId) {
      delete agreements.stepId;
    }
    if(agreements.inviterDetails){
      delete agreements.inviterDetails;
    }
    const isAllAgrrementsAgreed = Object.values(agreements).every(
      (i: any) =>  i.agreed === 1 || (i.id === 6 && user.join_as_a_parked_agent)
    );
    if (isAllAgrrementsAgreed && allAgreementStatus) {
      setShowSubmitLoader(true);
      const res = (await submitReviewAgreemnts()) as { status: number };
      setShowSubmitLoader(false);
      if (res.status === 200) {
        navigate("/agent-onboard/confirmation");
      }
    } else {
      message.error("Please accept all agreements above");
    }
  };
  return (
    <>
      <FormHeader>
        <Flex>
          <H3Typography>Review Agreements</H3Typography>
        </Flex>
      </FormHeader>
      <InnerContainer>
        <ContentWrapper className="bordernone">
          {reviewContentList.length > 1 &&
            reviewAgreementInfo &&
            reviewContentList.map((item: any, index: any) => {
              const ComponentTag: any = getComponentById(item.id);
              const componentData = getAgreementDataById(item.id);
              return (
                <>
                  {item.show && (
                    <AccoridanPanel
                      accordianKey={item.id}
                      title={item.title}
                      defaultActiveKey={docusignFormType}
                      status={componentData.agreed}
                      onClick={() => {
                        setDocuSignFormType(
                          parseInt(docusignFormType) === item.id ? 11 : item.id
                        );
                      }}
                    >
                      <ComponentTag
                        id={item.id}
                        contentText={item.contents}
                        data={componentData}
                        onSubmitReview={submitHandler}
                      />
                    </AccoridanPanel>
                  )}
                </>
              );
            })}
          {reviewContentList.length > 1 ? (
            <>
              <Flex top={20} direction="column">
                <H3 text="Agreement" className="space-bottom-16 font-500" />
                <Checkbox
                  name={
                    "I agree with the terms and conditions and confirm that all the provided details are correct."
                  }
                  checked={allAgreementStatus}
                  onChange={() => {
                    setAllAgreementStatus(!allAgreementStatus);
                  }}
                />
              </Flex>
              <FlexBox className="footerbtn">
                <Primary
                  text="Submit application"
                  className="submitbtn"
                  style={{ marginRight: "10px" }}
                  onClick={submitApplication}
                  isLoading={showSubmitLoader}
                />

                <BorderBtn
                  className="borderbtn"
                  text="Previous step"
                  onClick={() => {
                    setStep(form.currentStep - 1);
                  }}
                />
              </FlexBox>
            </>
          ) : null}
        </ContentWrapper>
      </InnerContainer>
    </>
  );
}

export default ReviewAgreementsPage;
