import styled from "styled-components";

export const ReviewContentWrapper = styled.div`
  padding: 0px 25px;
  text-align: justify;

  .review-btn {
    padding: 0px;
    margin: 24px 0px;
    .submitbtn {
      padding: 12px 24px;
      background: #4e1c95;
      color: #fff;
      font-family: Poppins;
      font-size: 14px;
      line-height: 22px;
      border-radius: 8px;
      width: max-content;
      cursor: pointer;
    }
  }
`;

export const SkipButton = styled.button`
  background-color: transparent;
  background: transparent;
  border-color: transparent;
  height: auto;
  font-size: 13px;
  line-height: 32px;
  font-weight: 400;
  color: #4E1C95;
  font-family: Poppins;
  cursor: pointer;
  position: relative;
  margin-left: 20px;
`;
