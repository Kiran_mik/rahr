import React from "react";
import { Description } from "@components/Typography/Typography";
import ReviewImage from "@assets/review.svg";
import { useSelector } from "react-redux";
import ConfirmationScreen from "@components/AgentConfirmationScreen";
function Review() {
  const user = useSelector((state: any) => state.user);
  return (
    <>
      <ConfirmationScreen
        subHeader={`${user.firstName} ${user.lastName},`}
        rightImage={ReviewImage}
      >
        <Description
          className="bottom-16"
          text="Thank you for submitting your application to become an agent for  “Right at Home Realty, Inc”. We are reviewing your application and you will receive email notification as soon as we confirm all information or if there will be any changes. Usually it takes up to __ hours to "
        />
      </ConfirmationScreen>
    </>
  );
}

export default Review;
