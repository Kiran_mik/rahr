import React, { useEffect } from "react";
import { Flex } from '@components/Flex'
import { H3Typography } from '@components/Typography/Typography'
import { FormHeader, InnerContainer } from '../style'
import Review from "./Review";


function ReviewPage(props: { path: string }) {
    useEffect(() => { }, []);
    return (
        <>
            <FormHeader>
                <Flex>
                    <H3Typography>Your application is under review</H3Typography>
                </Flex>
            </FormHeader>
            <InnerContainer>
                <Review />
            </InnerContainer>

        </>
    )
}

export default ReviewPage
