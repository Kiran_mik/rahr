import React, { useEffect } from "react";
import { Steps } from "antd";

import { STEPS } from "./constants";
import StepIcon from "./StepIcon";
import { navigate, useLocation } from "@reach/router";
import useAgentSteps from "@hooks/useAgentSteps";
import { useSelector } from "react-redux";
import { AgentOnBoardingSteps } from "@store/agentOnboardingReducer";
import { USER_STATUS_ENUM } from "@constants/userConstants";

const { Step } = Steps;

function SideSteperComponent() {
  const location = useLocation();
  const key = location.pathname.split("/")[2];
  const [setForm, setStep, form] = useAgentSteps();
  const user = useSelector((state: { user: any }) => state.user);
  const { status, onBoardingStepsStatus } = user;

  useEffect(() => {
    const currentStep = Object.values(STEPS).find((i: any) => i.key === key);
    if (currentStep) {
      setStep(currentStep.index);
    }
  }, [location.pathname]);

  const CurrentStep =
    useSelector(
      (state: { agentOnboarding: AgentOnBoardingSteps }) =>
        state.agentOnboarding
    ).currentStep - 1;

  const updatedSteps = Object.values(STEPS).map((i: any) => {
    i.status =
      onBoardingStepsStatus && onBoardingStepsStatus[i.index]
        ? "finish"
        : "process";
    return i;
  });
  return (
    <>
      <Steps
        current={CurrentStep}
        size={"small"}
        onChange={() => { }}
        direction="vertical"
        style={{ height: 500, paddingLeft: "8px" }}
      >
        {updatedSteps.map((i: any) => {
          const isEnabled =
            (i.status === "process" || i.status === "finish") &&
            status != USER_STATUS_ENUM.PENDING_REVIEW;
          return (
            !i.hide && (
              <Step
                key={i.index}
                onClick={() => {
                  if (isEnabled) {
                    setStep(i.index);
                    navigate(`/agent-onboard/${i.key}`);
                  }
                }}
                icon={<StepIcon step={i} />}
                disabled={i.status === "wait" || !isEnabled}
                status={i.status}
                title={i.title}
                style={{ cursor: isEnabled ? "pointer" : "no-drop" }}
              />
            )
          );
        })}
      </Steps>
    </>
  );
}

export default SideSteperComponent;
