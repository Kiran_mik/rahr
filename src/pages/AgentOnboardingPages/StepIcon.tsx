import React from "react";

import { Flex } from "@components/Flex";
import { STATUS_COLOR_MAP } from "@constants/onboardingConstants";

const StepIcon = (props: { step: any }) => {
  const { step } = props;
  const isFinished = step.status === "finish";
  const showRounded = step.status === "process" || step.status === "error";
  const isError = step.status === "error";
  const backgroundColor = showRounded
    ? isError
      ? STATUS_COLOR_MAP.ERROR
      : "#4E1C95"
    : isFinished
    ? STATUS_COLOR_MAP.SUCCESS
    : STATUS_COLOR_MAP.DISABLED;
  return (
    <>
      <Flex
        style={{
          height: 25,
          width: 25,
          borderRadius: 50,
          justifyContent: "center",
          padding: 4,
          alignItems: "center",
          borderColor: "#382561",
          borderWidth: 1,
          marginLeft: 0,
          marginTop: 0,
        }}
        className="currentborder"
      >
        <Flex
          direction={"column"}
          justifyContent={"center"}
          style={{
            height: 20,
            width: 20,
            borderRadius: 50,
            alignItems: "center",
            display: "flex",
            backgroundColor: backgroundColor,
            color: "white",
            fontSize: 12,
          }}
        >
          {props.step.index}
        </Flex>
      </Flex>
      {step.status === "process" && (
        <Flex
          style={{
            height: 30,
            width: 1,
            backgroundColor: "#382561",
            marginLeft: 12,
          }}
        />
      )}
    </>
  );
};

export default StepIcon;
