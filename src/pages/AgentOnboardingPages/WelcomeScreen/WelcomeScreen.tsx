import React from "react";
import {
  Container,
  LeftContent,
  RightContent,
  ContentWrapper,
  TextContent,
  Heading,
  PrimaryButtonHalf
} from "./style";
import Welcome from "../../../assets/welcome.png";

const WelcomeScreen = (props: { path: string }) => {
  return (
    <Container>
      <LeftContent>
        <ContentWrapper>
          <Heading>Welcome to the Right at Home Realty, George</Heading>
          <TextContent>
            We are the <b>#1</b> brokerage in the GTA and remain the largest
            independent brokerage in Canada. Our network has grown to 12 offices
            and over 5400 members serving Ontario with recent acquisitions in
            Barrie and Ottawa further extending our reach.
          </TextContent>
          <TextContent>
            Before joining us, you have to complete the onboarding process and
            submit all required documents to set up your account with us.
          </TextContent>
          <PrimaryButtonHalf text="Complete your profile" />
        </ContentWrapper>
      </LeftContent>
      <RightContent>
        <img src={Welcome} alt="group" />
      </RightContent>
    </Container>
  );
};

export default WelcomeScreen;
