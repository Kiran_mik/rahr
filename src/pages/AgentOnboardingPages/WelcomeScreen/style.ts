import styled from "styled-components";
import { Primary } from "../../../shared/components/Buttons";

export const Container = styled.div`
  display: flex;
  min-height:calc(100vh - 80px);
  background:#ebeff4;
`;
export const PrimaryButtonHalf = styled(Primary)`
width:50%;
backgroind:white
`;
export const LeftContent = styled.div`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 20px 0;
  flex-direction: column;
  flex:50%;
 
`;

export const RightContent = styled.div`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 20px 0;
  flex-direction: column;
  flex:50%;
`;

export const ContentWrapper = styled.div`
 width:501px;
`;

export const TextContent = styled.p`
  text-align: left;
    line-height: 22px;
    font-size: 14px;
    font-family: Poppins;
    color: #777777;
    b{
      color:#382561;
      font-weight:600
    }
`;

export const Heading = styled.h1`
font-size:32px;
line-height:40px;
  font-family:Poppins;
  color:#17082D;
  margin-bottom:32px
`;

