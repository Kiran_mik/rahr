import { USER_STATUS_ENUM } from "@constants/userConstants";

interface ObjectKey {
  [key: number]: any;
}
export const AGENT_ONBOARDING_BASE_URL = "v1/agents/onboardings";
export const REGISTER_YOUR_BUSSINESS_URL =
  "https://www.canada.ca/en/revenue-agency/services/tax/businesses/topics/registering-your-business/register.html";
export const STEPS: ObjectKey = {
  1: {
    title: "Contact Details",
    key: "contact-details",
    status: "finish",
    index: 1,
    storeKey: "contactDetails",
    isGet: true,
    visible: USER_STATUS_ENUM.INVITE_SENT
  },
  2: {
    title: "Profile Information",
    key: "profile-info",
    status: "finish",
    index: 2,
    storeKey: "personalInfo",
    isGet: true,
    visible: USER_STATUS_ENUM.INVITE_SENT
  },
  3: {
    title: "Employment Status",
    key: "employment-info",
    status: "finish",
    index: 3,
    storeKey: "employmentInfo",
    isGet: true,
    visible: USER_STATUS_ENUM.INVITE_SENT
  },
  4: {
    title: "PREC Details",
    key: "prec-details",
    status: "finish",
    index: 4,
    storeKey: "precDetails",
    isGet: true,
    visible: USER_STATUS_ENUM.INVITE_SENT

  },
  5: {
    title: "Billing & Commission",
    key: "billing-commission",
    status: "finish",
    index: 5,
    storeKey: "BillingCommission",
    isGet: true,
    visible: USER_STATUS_ENUM.INVITE_SENT

  },
  6: {
    title: "Charity Enrollment",
    key: "charity-enrollments",
    status: "finish",
    index: 6,
    storeKey: "charityEnrollments",
    isGet: true,
    visible: USER_STATUS_ENUM.INVITE_SENT

  },
  7: {
    title: "Review Agreements",
    key: "review-agreements",
    status: "finish",
    index: 7,
    storeKey: "review-agreements",
    isGet: true,
    visible: USER_STATUS_ENUM.INVITE_SENT

  },
  8: {
    title: "Confirmation",
    key: "confirmation",
    status: "finish",
    index: 8,
    storeKey: "contactDetails",
    hide: true,
    visible: USER_STATUS_ENUM.INVITE_SENT
  },
  9: {
    title: "Application is under review",
    key: "review",
    status: "finish",
    index: 9,
    storeKey: "contactDetails",
    hide: true,
    visible:USER_STATUS_ENUM.PENDING_REVIEW

  },
};


export const STEPS_ENUM = {
  CONTACT_DETAILS:1,
  PROFILE_DETAILS:2,
  EMPLOYMENT_STATUS:3,
  PREC_DETAILS:4,
  BILLING_COMMISION:5,
  CHARITY_ENROLLMENT:6,
  REVIEW_AGREEMENT:7,
  CONFIRMATION:8,
  REVIEW:9
}