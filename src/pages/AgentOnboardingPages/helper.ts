import services from "@services/index"
import { STEPS, AGENT_ONBOARDING_BASE_URL } from "./constants"

export const getFormDetails = async (step: number) => {
    try {
        const formKey = STEPS[step].key;

        const res = await services.get(`${AGENT_ONBOARDING_BASE_URL}/${formKey}`) as { data: { data: any } }

        return res.data.data;
    } catch (error) {

    }
}

export const getUpdatedReviewAgreements = async () => {
    try {
        const res = await services.get(`${AGENT_ONBOARDING_BASE_URL}/review-agreements`) as { data: { data: any } }
        return res.data.data;

    } catch (err) {

    }
}