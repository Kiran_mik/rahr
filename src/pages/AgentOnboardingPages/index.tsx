import React, { useEffect } from "react";
import AgentOnboardingLayout from "./AgentOnboardingLayout";
import {  navigate, useLocation } from "@reach/router";
import { useSelector } from "react-redux";
import { STEPS, STEPS_ENUM } from "./constants";
import { USER_STATUS_ENUM } from "@constants/userConstants";

function AgentOnboardingPages(props: { path: string; children: any }) {
  const user = useSelector((state: { user: any }) => state.user);
  const { status,currentOnboardingStep } = user;
  const location = useLocation();
  const key = location.pathname.split("/")[2];
  const currentStep = Object.values(STEPS).find((i: any) => i.key === key);
   useEffect(()=>{
    if(currentOnboardingStep==STEPS_ENUM.REVIEW && currentStep.key!=STEPS_ENUM.REVIEW){
      navigate("/agent-onboard/review");
    }
  },[currentOnboardingStep,currentStep])
  if(currentStep && currentStep.visible!==status){
    if(status==USER_STATUS_ENUM.INVITE_SENT){
      navigate(`/agent-onboard/${STEPS[1].key}`, { replace: true });
      return null;
    } else if (status==USER_STATUS_ENUM.PENDING_REVIEW) {
      navigate(`/agent-onboard/review`, { replace: true });
      return null;
    } else {
      return null;
    }
  }
    return (
    <>
      <AgentOnboardingLayout>{props.children}</AgentOnboardingLayout>
    </>
  );
}

export default AgentOnboardingPages;
