import { Flex } from "@components/Flex";
import styled from "styled-components";

export const SquareRadioWrapper = styled.div`
.squareRadioBtn .ant-radio-wrapper{
    border: 1px solid #AFADC8;
      padding: 8px 15px;
      margin-bottom: 18px;
      margin-right:15px;
      border-radius:4px;
  }
  .hidecounter.ant-input-textarea-show-count::after{
    display:none
  }
      }
`;

export const StepperFormContainer = styled.div`
  width: calc(100% - 401px);
  display: block;
  margin: 0 auto;
`;

export const FormHeader = styled.div`
  display: flex;
  padding: 24px 48px;
  border-bottom: 1px solid #e2e2ee;
`;
export const InnerContainer = styled.div`
  display: flex;
  flex-direction: column;
  padding: 0px 48px;
  max-height: calc(100vh - 253px);
  overflow: auto;
  &::-webkit-scrollbar {
    width: 5px;
  }

  &::-webkit-scrollbar-track {
    box-shadow: inset 0 0 5px rgba(226, 226, 238, 1);
    border-radius: 10px;
  }

  &::-webkit-scrollbar-thumb {
    background: rgba(151, 146, 227, 1);
    border-radius: 10px;
  }

  &::-webkit-scrollbar-thumb:hover {
    background: rgba(151, 146, 227, 1);
  }
  .footerbtn {
    padding: 0px 48px;
    margin: 24px -48px;
    border-top: 1px solid #e2e2ee;
    padding-top: 24px;
    .submitbtn {
      padding: 12px 24px;
      background: #4e1c95;
      color: #fff;
      font-family: Poppins;
      font-size: 14px;
      line-height: 22px;
      border-radius: 8px;
      width: max-content;
      cursor: pointer;
    }
    .borderbtn {
      border: 1px solid #bdbdd3;
      color: #17082d;
      padding: 13px 24px;
    }
  }
`;

export const ContentWrapper = styled.div`
  border-bottom: 1px dashed #a2a2ba;
  padding: 33px 0px;
  width: 100%;
  &.bordernone {
    border-bottom: none;
  }
  &.space-bottom-0 {
    padding-bottom: 0px;
  }
  .borderbtn {
    border: 1px solid rgb(78, 28, 149);
    border-color: #4e1c95;
    padding: 3px 10px;
    font-size: 14px;
    border-radius: 8px;
  }
`;

export const AgentOnboardingPagesContainer = styled.div`
  background-color: #ebeff4;
`;
export const AgentOnboardingStepContainer = styled(Flex)`
  background-color: white;
  border-radius: 10px;
  // margin-bottom:24px;
`;

export const SideSteperWrapper = styled.div`
  display: flex;
  padding: 30px 30px;
  padding-right: 30px;
  width: 273px;
  flex-direction: column;
  border-radius: 10px;
  max-height: calc(100vh - 175px);
  padding-bottom: 27px;
  overflow: auto;
  &::-webkit-scrollbar {
    width: 5px;
  }

  &::-webkit-scrollbar-track {
    box-shadow: inset 0 0 5px rgba(226, 226, 238, 1);
    border-radius: 10px;
  }

  &::-webkit-scrollbar-thumb {
    background: rgba(151, 146, 227, 1);
    border-radius: 10px;
  }

  &::-webkit-scrollbar-thumb:hover {
    background: rgba(151, 146, 227, 1);
  }
  .ant-steps-small .ant-steps-item-title {
    font-size: 10px;
    color: #17082d;
  }
  .ant-steps-vertical.ant-steps-small
    .ant-steps-item-container
    .ant-steps-item-title {
    line-height: 27px;
  }
  .ant-steps-vertical.ant-steps-small
    .ant-steps-item-container
    .ant-steps-item-tail {
      position: absolute;
      top: 0px;
      left: 15px;
      padding: 26px 0 0px;
  }
  .ant-steps-vertical
    > .ant-steps-item
    > .ant-steps-item-container
    > .ant-steps-item-tail::after {
    width: 1px;
    height: 100%;
    margin-left: -2.1px;
    background-color: transparent;
    border-right:1px solid rgb(56, 37, 97);
  }
  .ant-steps-item-active .currentborder{
    border: 1px solid rgb(56, 37, 97);
  }
`;
export const LogoImage = styled.img`
  height: 50px;
  @media (max-width: 600px) {
    height: 36px;
  }
`;

export const AccountRegistrationContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: calc(100% - 369px);
`;

export const ContactWrapper = styled.div`
  padding: 16px;
  display: flex;
  background: #f8f8fc;
  flex-direction: column;
  justify-content: center;
  text-align: center;
  border-radius: 10px;
  h5 {
    margin-bottom: 4px;
  }
  .margin-16 {
    margin: 16px 0px;
  }
  span {
    font-size: 12px;
    color: #50514f;
    font-weight: 400;
  }
`;
export const ContactText = styled.div`
  font-size: 12px;
  color: #17082d;
  line-height: 20px;
  font-weight: 400;
  display: flex;
  align-items: center;
  img {
    margin-right: 4px;
  }
`;

export const RemoveButton = styled.div`
  height: auto;
  padding: 0px 5px;
  font-size: 14px;
  font-weight: 400;
  letter-spacing: 0px;
  color: #4e1c95;
  font-family: Poppins;
  cursor: pointer;
`;
