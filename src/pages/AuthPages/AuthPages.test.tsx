import store from "@store/index";
import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import AuthPages from ".";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(
    <Provider store={store}>
      <AuthPages />
    </Provider>,
    div
  );

  ReactDOM.unmountComponentAtNode(div);
});
