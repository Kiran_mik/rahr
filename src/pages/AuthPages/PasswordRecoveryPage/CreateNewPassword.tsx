import React, { FC, useState, useEffect } from "react";
import { Primary } from "../../../shared/components/Buttons";
import { navigate, RouteComponentProps } from "@reach/router";
import SignUpWrapper from "../../../shared/components/SignUp/Layout";
import {
  ForgotLabel,
  FormItem,
  FormContents,
  Headings,
  Header,
  PrimaryButtonWrapper,
  SubHeading,
} from "./style";
import { Form, Input, message } from "antd";
import { Label } from "@components/Typography/Typography";
import { resetPassword, validateToken } from "@services/AuthService";

interface CreateNewPasswordProps extends RouteComponentProps {
  title?: string;
  token?: any;
}
const CreateNewPassword: FC<CreateNewPasswordProps> = ({ token }) => {
  const [tokenValid, setTokenValid] = useState(false);
  const [errormsg, setErrormsg] = useState("");
  const redirect = () => {
    navigate("/");
  };

  const SubmitForm = async (values: any) => {
    const res: any = await resetPassword(token, values.password);
    if (!res.success) {
      message.error(res.error.message);
    } else {
      message.success(res.message);
      navigate("/");
    }
  };
  const ValidateToken = async () => {
    const tokenRes: any = await validateToken(token);
    if (!tokenRes.sucess) {
      setTokenValid(false);
      setErrormsg(tokenRes.error.error.token[0]);
    } else {
      setTokenValid(true);
    }
  };
  useEffect(() => {
    ValidateToken();
  }, []);

  return (
    <>
      <SignUpWrapper className="create-password-outer ">
        {tokenValid ? (
          <Form onFinish={SubmitForm}>
            <Headings>
              <Header text="Create new password" />
              <SubHeading>
                In order to protect your account, make sure your password:
                <ul style={{ paddingLeft: "25px" }}>
                  <li>Is longer than 7 character</li>
                  <li>
                    Does not match or significantly contain your email id
                    address and previous password
                  </li>
                  <li>
                    Is not a member of this list of commonly used passwords
                  </li>
                </ul>
              </SubHeading>
            </Headings>
            <FormContents>
              <FormItem>
                <Label text="New Password" />
                <Form.Item
                  name="password"
                  extra="Must be at least 8 characters"
                  style={{ flexDirection: "column" }}
                  rules={[
                    {
                      required: true,
                      message: "Password field cannot be empty.",
                    },
                    {
                      min: 8,
                      message: "Must contain more than 7 characters",
                    },
                  ]}
                >
                  <Input.Password
                    data-testid="passwordTest"
                    name="password"
                    placeholder="***********"
                  />
                </Form.Item>
              </FormItem>
              <FormItem>
                <Label text="Confirm password" />
                <Form.Item
                  name="confirm"
                  extra="Both passwords must match"
                  dependencies={["password"]}
                  rules={[
                    {
                      required: true,
                      message: "Please Confirm your password.",
                    },
                    ({ getFieldValue }) => ({
                      validator(_, value) {
                        if (!value || getFieldValue("password") === value) {
                          return Promise.resolve();
                        }
                        return Promise.reject(
                          new Error(
                            "The two passwords that you entered do not match!"
                          )
                        );
                      },
                    }),
                  ]}
                  hasFeedback
                >
                  <Input.Password
                    data-testid="passwordConfirmTest"
                    placeholder="***********"
                  />
                </Form.Item>
              </FormItem>
              <FormItem>
                <PrimaryButtonWrapper>
                  <Primary testId="submitButtonTest" text="Change password" />
                </PrimaryButtonWrapper>
              </FormItem>
              <FormItem>
                <ForgotLabel
                  data-testid="returnLoginScreenTest"
                  onClick={redirect}
                >
                  Return to login screen
                </ForgotLabel>
              </FormItem>
            </FormContents>
          </Form>
        ) : (
          <>
            {errormsg ? (
              <p
                style={{ color: "red", textAlign: "center", marginTop: "10px" }}
              >
                {errormsg}
              </p>
            ) : (
              ""
            )}

            <ForgotLabel
              data-testid="returnLoginScreenTest"
              onClick={() => {
                navigate("/recovery-link");
              }}
            >
              Return to Forgot Password
            </ForgotLabel>
          </>
        )}
      </SignUpWrapper>
    </>
  );
};

export default CreateNewPassword;
