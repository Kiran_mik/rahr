import React from "react";
import {
  render,
  screen,
  fireEvent,
  waitFor,
  logRoles
} from "@testing-library/react";
import "@testing-library/jest-dom";
import "@testing-library/jest-dom/extend-expect";
import { PasswordReset, RecoveryLink } from ".";
import FormItem from "antd/lib/form/FormItem";
import CreateNewPassword from "./CreateNewPassword";

describe("PasswordRecovery  test cases", () => {
  window.matchMedia =
    window.matchMedia ||
    function() {
      return {
        matches: false,
        addListener: function() {},
        removeListener: function() {}
      };
    };
  test("Recover Password Show", async () => {
    const { debug } = render(<RecoveryLink />);

    screen.debug(debug(), 20000);
    const UserDetails = screen.queryByText(/Recover Password/i);
    const testLoginClick = screen.getByTestId("testLoginClick");
    const formItemTest = screen.getByTestId("formItemTest");
    fireEvent.click(screen.getByTestId("backButtonTest"));
    fireEvent.click(screen.getByTestId("Formtest"));
    fireEvent.click(screen.getByTestId("testRecoveryLink"));

    expect(screen.getByPlaceholderText("name@email.com")).toBeInTheDocument();
    expect(testLoginClick).toHaveAttribute("href", "/");
    expect(UserDetails).toBeInTheDocument();
    expect(formItemTest).toBeInTheDocument();
    //form test cases
    await fireEvent.change(screen.getByPlaceholderText("name@email.com"), {
      target: { value: "test@gmail.com" }
    });
    expect(screen.getByPlaceholderText("name@email.com").value).toBe(
      "test@gmail.com"
    );
    await fireEvent.submit(screen.getByText("Email me a recovery link"));
  });

  test("passwordReset test", async () => {
    render(<PasswordReset />);

    const PasswordResetHeader = screen.queryByText(/Password reset sent/i);
    fireEvent.click(screen.getByTestId("LoginButtonReset"));
    const RecoveryLinkTest = screen.getByTestId("recoveryLinkTest");

    expect(RecoveryLinkTest).toHaveAttribute("href", "/recovery-link");
    expect(PasswordResetHeader).toBeInTheDocument();
    //expect(EmailError).toBeInTheDocument();
  });

  test("Create new Password test", async () => {
    const { debug } = render(<CreateNewPassword />);
    screen.debug(debug(), 20000);
    const newPasswordHeader = screen.queryByText(/Create new password/i);
    fireEvent.click(screen.getByTestId("returnLoginScreenTest"));
    fireEvent.click(screen.getByTestId("submitButtonTest"));

    expect(newPasswordHeader).toBeInTheDocument();
    expect(
      screen.queryByText(/Is longer than 7 character/i)
    ).toBeInTheDocument();
    expect(
      screen.queryByText(/Must be at least 8 characters/i)
    ).toBeInTheDocument();

    //form test cases
    // const passwordTest = screen.getByTestId("passwordTest");
    await fireEvent.change(screen.getByTestId("passwordTest"), {
      target: { value: "12345678" }
    });
    expect(screen.getByTestId("passwordTest").value).toBe("12345678");
    await fireEvent.change(screen.getByTestId("passwordConfirmTest"), {
      target: { value: "12345678" }
    });
    expect(screen.getByTestId("passwordConfirmTest").value).toBe("12345678");
    await fireEvent.submit(screen.getByTestId("submitButtonTest"));

    //expect(screen.getByTestId("submitButtonTest")).toHaveBeenCalled();

    await fireEvent.change(screen.getByTestId("passwordTest"), {
      target: { value: "" }
    });
    await fireEvent.change(screen.getByTestId("passwordConfirmTest"), {
      target: { value: "" }
    });
    await fireEvent.submit(screen.getByTestId("submitButtonTest"));
  });
});
