import React, { FC } from "react";
import { Primary } from "../../../shared/components/Buttons";
import { navigate, RouteComponentProps } from "@reach/router";
import SignUpWrapper from "../../../shared/components/SignUp/Layout";
import {
  FormItem,
  MailPics,
  SubHeading,
  Headertxt,
  ResetLinkText,
  FormFooterText,
  LoginText,
} from "./style";
import { Form } from "antd";
import MailPic from "@assets/PasswordSent.png";

interface PasswordResetProps extends RouteComponentProps {
  title?: string;
}

const PasswordReset: FC<PasswordResetProps> = (props) => {
  const data: any = props.location;

  return (
    <>
      <SignUpWrapper>
        <Form>
          <MailPics src={MailPic} />
          <Headertxt text="Password reset sent" />

          <ResetLinkText>
            We have sent a password recovery instruction to your email{" "}
            <span>{data && data.state && data.state.email}</span>. Please check
            your inbox and click in the received link to reset a password
          </ResetLinkText>
          <FormItem />
          <Primary
            style={{ marginBottom: "16px" }}
            onClick={() => {
              navigate("/");
            }}
            testId="LoginButtonReset"
            text="Login"
          />
          <FormFooterText>
            <SubHeading>
              Didn’t receive the email? Check your spam folder or try to
              <LoginText data-testid="recoveryLinkTest" to="/recovery-link">
                {" Resend"}{" "}
              </LoginText>
            </SubHeading>
          </FormFooterText>
        </Form>
      </SignUpWrapper>
    </>
  );
};

export default PasswordReset;
