import React, { FC, useState } from "react";
import { Primary } from "../../../shared/components/Buttons";
import { navigate, RouteComponentProps } from "@reach/router";
import SignUpWrapper from "../../../shared/components/SignUp/Layout";
import {
  FormItem,
  Headings,
  ButtonAndBackLink,
  ArrowImage,
  Header,
  SubHeading,
  BackButton,
  LoginText,
  FormFooterText,
} from "./style";
import { Form, Input } from "antd";
import BackArrow from "@assets/BackArrow.svg";
import { Label } from "@components/Typography/Typography";
import { ForgotPassword } from "@services/AuthService";

interface RecoveryLinkProps extends RouteComponentProps {
  title?: string;
}

const RecoveryLink: FC<RecoveryLinkProps> = () => {
  const [form] = Form.useForm();
  const [error, setError] = useState("");
  const redirect = () => {
    navigate("/");
  };
  const SubmitForm = async (values: any) => {
    const res: any = await ForgotPassword(values);
    if (!res.success) {
      setError(res.msg);
      const ele = document.getElementById("email");
      ele && ele.focus();
    } else {
      navigate("/password-reset-send", { state: { email: values.email } });
    }
  };
  const onFinishFailed = (errorInfo: any) => {
    const ele = document.getElementById(errorInfo.errorFields[0]["name"][0]);
    ele && ele.focus();
  };
  return (
    <>
      <SignUpWrapper>
        <Form
          data-testid="Formtest"
          form={form}
          onFinish={SubmitForm}
          onFinishFailed={onFinishFailed}
          onFieldsChange={() => {
            setError("");
          }}
        >
          <Headings>
            <Header text="Recover Password" />
            <SubHeading>
              Don’t worry, happens to the best of us. We are here to help you to
              recover your password. Enter the email you used when you joined
              and we`ll send you instruction to reset your password
            </SubHeading>
          </Headings>
          <FormItem className="dynamic-error-lable" data-testId="formItemTest">
            <Label text="Email ID" />
            <Form.Item
              extra={error}
              name="email"
              style={{
                flexDirection: "column",
                marginBottom: "24px",
                color: "red",
              }}
              dependencies={["email"]}
              rules={[
                {
                  type: "email",
                  message: "Please enter valid email.",
                },
                {
                  required: true,
                  message: "Email field cannot be empty.",
                },
              ]}
            >
              <Input
                className={error ? "dynamic-error " : ""}
                id="email"
                placeholder="name@email.com"
              />
            </Form.Item>
          </FormItem>

          <ButtonAndBackLink>
            <BackButton data-testid="backButtonTest" onClick={redirect}>
              <ArrowImage src={BackArrow} />
              <label>Back</label>
            </BackButton>
            <Primary
              testId="testRecoveryLink"
              text="Email me a recovery link"
            />
          </ButtonAndBackLink>
          <FormFooterText>
            <SubHeading>Remember password?</SubHeading>
            <LoginText data-testid="testLoginClick" to="/">
              &nbsp;{"Login"}
            </LoginText>
          </FormFooterText>
        </Form>
      </SignUpWrapper>
    </>
  );
};

export default RecoveryLink;
