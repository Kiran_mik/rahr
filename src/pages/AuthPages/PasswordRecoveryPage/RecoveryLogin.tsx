import React, { FC } from "react";
import { Primary } from "../../../shared/components/Buttons";
import { RouteComponentProps } from "@reach/router";
import SignUpWrapper from "../../../shared/components/SignUp/Layout";
import {
  ForgotLabel,
  FormItem,
  FormContents,
  MainHeadings,
  Header,
  SubHeading,
  PrimaryButtonWrapper,
} from "./style";
import { Form, Input } from "antd";
import { Label } from "@components/Typography/Typography";
interface PasswordRecoveryProps extends RouteComponentProps {
  title?: string;
}

const PasswordRecovery: FC<PasswordRecoveryProps> = () => {
  return (
    <>
      <SignUpWrapper>
        <Form>
          <MainHeadings>
            <Header text="Great to see you again!" />
            <SubHeading>Log in to your Right at Home Realty account</SubHeading>
          </MainHeadings>
          <FormContents>
            <FormItem>
              <Label text="Email ID" />
              <Form.Item
                name="email"
                style={{ flexDirection: "column" }}
                rules={[
                  {
                    type: "email",
                    message: "Please enter a valid email.",
                  },
                  {
                    required: true,
                    message: "Email field cannot be empty.",
                  },
                ]}
              >
                <Input name="email" placeholder="name@email.com" />
              </Form.Item>
            </FormItem>
            <FormItem>
              <Label text="Password" />
              <Form.Item
                name="password"
                dependencies={["password"]}
                rules={[
                  {
                    required: true,
                    message: "Please enter your password.",
                  },
                ]}
                hasFeedback
              >
                <Input.Password placeholder="***********" name="password" />
              </Form.Item>

              <FormItem>
                <PrimaryButtonWrapper>
                  <Primary text="Sign In" />
                </PrimaryButtonWrapper>
              </FormItem>
              <ForgotLabel href="/recovery-link">Forgot Password?</ForgotLabel>
            </FormItem>
          </FormContents>
        </Form>
      </SignUpWrapper>
    </>
  );
};

export default PasswordRecovery;
