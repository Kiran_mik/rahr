import * as yup from "yup";

export const passwordRecoverySchema = yup.object().shape({
    email: yup.string().email().required(),
    password: yup.string().required()
})

// export const passwordRecoverySchema = yup.object().shape({

// })