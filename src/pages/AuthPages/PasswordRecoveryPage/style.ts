import styled from "styled-components";
import { Input, Form, Button } from "antd";

import { H2, DescriptionPara, H3 } from "@components/Typography/Typography";
import { Link } from "@reach/router";
import { Primary } from "@components/Buttons";
import { PrimaryButton } from "@components/Buttons/style";

export const Header = styled(H3)`
  margin-bottom:8px;
  font-size:600;
`;
export const Headertxt = styled(H3)`
  margin-bottom:10px;
`;
export const SubHeading = styled(DescriptionPara)`
margin-bottom:40px;
`;
export const FormFooterText = styled.div`
  display:flex;
  justify-content:center;
  margin-top: 24px;
`;
export const LoginText = styled(Link)`
  font-weight: 700;
  color:#4E1C95;
`;
export const ResetLinkText = styled.p`
  margin-bottom: 40px;
  line-height:24px;
  color: #4F4F4F;
  font-size:14px;
  font-family: Poppins;
  span{
    font-weight:700;
  }
`;

export const PrimaryButtonWrapper = styled.div`
  margin-top:40px !important;
`;

export const FormLayout = styled(Form)``;

export const ForgotLabel = styled.a`
  color: #4e1c95;
  justify-content: center;
  display: flex;
    margin: 20px auto auto auto;
    font-size: 14px;
    line-height: 16px;
    font-family: Poppins;
    font-weight: 400;
    width:max-content;
`;


export const FormItem = styled.div`
  display: block;
`;

export const FormContents = styled.div`
  display: block;
  margin-top: 25px;
`;

export const Headings = styled.div`
  font-family: Poppins;

  &h1 {
    font-size: 24px;
    text-align: center;
  }
  .ResetLinkText {
    label {
      font-weight: bold;
    }
  }
`;

export const MainHeadings = styled.div`
`;

export const ButtonAndBackLink = styled.div`
  display: flex;
  label {
    font-size: 14px;
    font-weight: 500;
    color: #4e1c95;
  }
`;
export const BackButton = styled.div`
display: flex;
    flex: 32%;
    align-items: center;
`;

export const ArrowImage = styled.img`
  width: 16px;
  height: 16px;
  margin-right:10px;
`;

export const MailPics = styled.img`
     /*width: 48px;*/
    height: auto;
    margin-bottom: 30px;
`;

export const BottomPara = styled.p`
  display: flex;
  justify-content: center;
  margin-top: 35px;
  
        color: #4e1c95;
    text-align: center;
    margin: 60px 0px 0px 0px;
    font-family: Poppins;
    font-size: 12px;
    line-height: 18px;
    padding: 0px 32px;
  
`;

