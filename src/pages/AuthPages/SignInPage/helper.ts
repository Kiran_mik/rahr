import services from "@services/index";
import * as Yup from "yup";

export const validationSchema = Yup.object({
  email: Yup.string()
    .email("Please enter valid email ID")
    .required("Please enter email ID"),
  password: Yup.string().required("Please enter password"),
});
