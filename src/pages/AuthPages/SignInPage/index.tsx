import React, { FC, useState } from "react";
import { RouteComponentProps } from "@reach/router";
import SignUpWrapper from "@components/SignUp/Layout";
import { Primary } from "@components/Buttons";
import { Label } from "@components/Typography/Typography";
import { InputField } from "@components/Input/Input";
import { Formik } from "formik";
import Cookies from "js-cookie";

import {
  SubHeading,
  FormItem,
  Header as AppHeader,
  PrimaryButtonWrapper,
  ForgotLabel,
} from "./style";
import { validationSchema } from "./helper";
import { useDispatch } from "react-redux";
import { signInUser } from "@services/AuthService";
import { authActions } from "@store/authReducer";
import { userActions } from "@store/userProfileReducer";
import { message } from "antd";

export interface SignupProps extends RouteComponentProps {
  token?: string;
}
export interface ChildrenProps {
  name?: string;
}

const SignInPage: FC<SignupProps> = (props) => {
  const [errormsg, setError] = useState<boolean>(false);
  const [errorState, setErrorState] = useState<{
    password: null | Array<string>;
    email: null | Array<string>;
  }>({
    password: null,
    email: null,
  });

  const dispatch = useDispatch();

  const onSubmit = async (values: any) => {
    const res = (await signInUser(values.email, values.password)) as {
      data: { access_token: string; role?: number };
      success: boolean;
      error?: { error: any; message: "" };
    };
    if (!res.success) {
      setError(true);
      if (res.error && res.error.error) {
        setErrorState(res.error.error);
      } else {
        if (res.error && res.error.message) {
          message.error(res.error.message);
        }
      }
    } else {
      dispatch(authActions.setAuth(res.data.access_token));
      Cookies.set("token", res.data.access_token, { expires: 7 });
      setError(false);
    }
  };
  return (
    <>
      <SignUpWrapper>
        <Formik
          initialValues={{
            email: "",
            password: "",
          }}
          onSubmit={onSubmit}
          validationSchema={validationSchema}
        >
          {(props) => {
            const {
              handleChange,
              values,
              handleSubmit,
              errors,
              touched,
              setTouched,
              setFieldValue,
            } = props;
            const setFieldsTouched = (field: string) => {
              const fieldIndex = Object.keys(values).indexOf(field);
              const touchedObject: any = { ...touched };
              Object.keys(values).forEach((key: string, index: number) => {
                if (index <= fieldIndex) {
                  touchedObject[key] = true;
                }
              });
              setTouched(touchedObject);
            };
            return (
              <>
                <AppHeader text="Welcome to Agent Portal" />
                <SubHeading>Create your account in just few steps </SubHeading>
                <form onSubmit={handleSubmit}>
                  <FormItem>
                    <Label text="Email ID" />
                    <InputField
                      id="email"
                      placeholder="Enter your email"
                      name="email"
                      onChange={(e: any) => {
                        setFieldsTouched("email");
                        handleChange(e);
                      }}
                      value={values.email}
                      error={
                        errorState.email
                          ? errorState.email[0]
                          : touched.email && errors.email
                      }
                    />
                  </FormItem>

                  <FormItem>
                    <Label text="Password" />
                    <InputField
                      id="password"
                      placeholder="********"
                      name={"password"}
                      type="password"
                      value={values.password}
                      error={touched.password && errors.password}
                      onChange={(e: any) => {
                        setFieldsTouched("password");
                        handleChange(e);
                      }}
                    />
                  </FormItem>

                  <FormItem>
                    <PrimaryButtonWrapper>
                      <Primary
                        text="Sign In"
                        type="submit"
                        onClick={handleSubmit}
                      />
                    </PrimaryButtonWrapper>
                  </FormItem>
                </form>
                <ForgotLabel href="/recovery-link">
                  Forgot Password?
                </ForgotLabel>
              </>
            );
          }}
        </Formik>
      </SignUpWrapper>
    </>
  );
};

export default SignInPage;
