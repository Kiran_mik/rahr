import React, { FC, useState } from "react";
import { navigate, RouteComponentProps } from "@reach/router";
import { Form } from "antd";
import SignUpWrapper from "@components/SignUp/Layout";
import { Primary } from "../../../shared/components/Buttons";
import { SubHeading, Header, PrimaryButtonWrapper } from "./style";
import { InputField } from "../../../shared/components/Input/Input";
import { PasswordField } from "../../../shared/components/InputPassword";
import PasswordStrengthBar from "react-password-strength-bar";
import { Label } from "@components/Typography/Typography";
export interface SignupProps extends RouteComponentProps {
  title?: string;
}
export interface ChildrenProps {
  name?: string;
}
type LayoutType = Parameters<typeof Form>[0]["layout"];

const SignUpPage: FC<SignupProps> = () => {
  const [form] = Form.useForm();
  const [formLayout, setFormLayout] = useState<LayoutType>("horizontal");
  const [password, setPassword] = useState("");

  const onFormLayoutChange = ({ layout }: { layout: LayoutType }) => {
    setFormLayout(layout);
  };

  const SubmitForm = () => {
    navigate("/sign-in");
  };
  return (
    <SignUpWrapper>
      <Form
        layout="vertical"
        form={form}
        initialValues={{ layout: formLayout }}
        onValuesChange={onFormLayoutChange}
        scrollToFirstError
        onFinish={SubmitForm}
      >
        <Header text="Join Right at Home Realty" />
        <SubHeading text="Create your account in just few steps" />
        <Label text="Email ID" />
        <Form.Item
          name="email"
          style={{ flexDirection: "column" }}
          rules={[
            {
              type: "email",
              message: "Please enter a valid email."
            },
            {
              required: true,
              message: "Email field cannot be empty."
            }
          ]}
        >
          <InputField
            placeholder="Enter your Email ID"
            type="text"
            name="email"
          />
        </Form.Item>
        <Label text="New Password" />
        <Form.Item
          name="password"
          style={{ flexDirection: "column", marginBottom: "6px" }}
          rules={[
            {
              required: true,
              message: "Password field cannot be empty."
            },
            {
              min: 7,
              message: "Must contain more than 7 characters"
            }
          ]}
        >
          <PasswordField placeholder="***********" />
        </Form.Item>
        <PasswordStrengthBar style={{ width: "100%" }} password={password} />
        <Label text="Confirm Password" />
        <Form.Item
          name="confirm"
          style={{ flexDirection: "column" }}
          dependencies={["password"]}
          rules={[
            {
              required: true,
              message: "This field cannot be empty."
            },
            ({ getFieldValue }) => ({
              validator(_, value) {
                if (!value || getFieldValue("password") === value) {
                  return Promise.resolve();
                }
                return Promise.reject(
                  new Error("The two passwords that you entered do not match!")
                );
              }
            })
          ]}
          hasFeedback
        >
          <PasswordField placeholder="***********" />
        </Form.Item>
        <Form.Item>
          <PrimaryButtonWrapper>
            <Primary text=" Complete Sign up" />
          </PrimaryButtonWrapper>
        </Form.Item>
      </Form>
    </SignUpWrapper>
  );
};

export default SignUpPage;
