import styled from "styled-components";
import { Tooltip, Button } from "antd";
import { H3, Description } from "@components/Typography/Typography";


export const Header = styled(H3)`
  margin-bottom:20px;
`;

export const FormItems = styled.div`
  display: block;
`;
export const FormItem = styled.div`
  display: block;
`;
const Title = styled.h1`
  font-size: 24px;
  font-weight: 400;
`;
export const SubHeading = styled(Description)`
  margin-bottom: 38px;
  color: #4F4F4F;
`;

export const PrimaryButtonWrapper = styled.div`
  margin-top:20px !important;
`;


