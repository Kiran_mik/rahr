import services from "@services/index"

export const getUserInfo = async () => {
    try {
        const res = await services.get('v1/user-info') as { data: { data: any } }
        return res.data.data;
    } catch (error) {
        return error
    }
}