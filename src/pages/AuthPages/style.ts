import { Flex } from "@components/Flex";
import styled from "styled-components";

export const Container = styled(Flex)`
  width:100%;
  overflow:hidden;
  .sidePanel + div{
   flex:1;
  }
  `;
