import React from "react";
import { Link } from "@reach/router";
import { useSelector } from "react-redux";
import { ApplicationApproved } from "@assets/index";
import ConfirmationScreen from "@components/AgentConfirmationScreen";
import { Description } from "@components/Typography/Typography";

const AccountApproved = () => {
  const user = useSelector((state: any) => state.user);
  return (
    <>
      <ConfirmationScreen
        subHeader={`Dear ${user.firstName},`}
        rightImage={ApplicationApproved}
      >
        <Description
          className="bottom-16"
          text="Your account has been approved already. Please login to the agent portal through the link below:"
        />
        <Description
          className="bottom-16"
          text={
            <>
              <Link to="" style={{ color: "#4E1C95" }}>
                {"{"}agent portal link{"}"}
              </Link>
            </>
          }
        />
        <Description
          className="bottom-16"
          text="If you have any questions or concerns, please contact our branch at: (406) 555-0120"
        />
      </ConfirmationScreen>
    </>
  );
};

export default AccountApproved;
