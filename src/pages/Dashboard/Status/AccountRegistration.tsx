import React from "react";
import { useSelector } from "react-redux";
import { ApplicationReview } from "@assets/index";
import ConfirmationScreen from "@components/AgentConfirmationScreen";
import { Description } from "@components/Typography/Typography";

const AccountRegistration = () => {
  const user = useSelector((state: any) => state.user);
  return (
    <>
      <ConfirmationScreen
        subHeader={`Dear ${user.firstName},`}
        rightImage={ApplicationReview}
      >
        <Description
          className="bottom-16"
          text="Thank you for submitting your onboarding application. Your application will be reviewed by your branch manager. Should there be any issues or missing information our system will send you an email. "
        />
        <Description
          className="bottom-16"
          text="If you have any questions or concerns, please contact our branch at: (406) 555-0120"
        />
      </ConfirmationScreen>
    </>
  );
};

export default AccountRegistration;
