import React from "react";
import { useSelector } from "react-redux";
import { ApplicationTermination } from "@assets/index";
import ConfirmationScreen from "@components/AgentConfirmationScreen";
import { Description } from "@components/Typography/Typography";

const AccountTermination = () => {
  const user = useSelector((state: any) => state.user);
  return (
    <>
      <ConfirmationScreen
        subHeader={`Dear ${user.firstName},`}
        rightImage={ApplicationTermination}
      >
        <Description
          className="bottom-16"
          text="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed erat erat. Aliquam fringilla ante eu mi ultrices tincidunt. Nulla vitae orci nec neque tincidunt rutrum. Vivamus blandit eros vitae ligula consectetur tristique. Donec consectetur hendrerit lacus non eleifend. Fusce sed ultrices dui."
        />
        <Description
          className="bottom-16"
          text="If you have any questions or concerns, please contact our branch at: (406) 555-0120"
        />
      </ConfirmationScreen>
    </>
  );
};

export default AccountTermination;
