import Loader from "@components/Loader";
import { ROLE_MAPING, USER_STATUS_ENUM } from "@constants/userConstants";
import { navigate } from "@reach/router";
import { ROLE_ROUTE_MAP } from "@utils/helpers";
import React from "react";
import { useSelector } from "react-redux";

const Dashboard = (props: { path: string }) => {
    const user = useSelector((state: any) => {
        return state.user;
    });
    if (user.status == USER_STATUS_ENUM.PARKED) {
        return <p>Parked agent</p>
    }
    if (user.status == USER_STATUS_ENUM.TERMINATED) {
        return <p>Terminated</p>
    }
    if (user.status == USER_STATUS_ENUM.ACTIVE && user.role == ROLE_MAPING.AGENT) {
        return <p>Active agent</p>
    }
    
    if (user.role == ROLE_MAPING.AGENT
        && (user.status == USER_STATUS_ENUM.PENDING_REVIEW
            || user.status == USER_STATUS_ENUM.INVITE_SENT
            || user.status == USER_STATUS_ENUM.AWAITING_DOC)) {
        navigate(ROLE_ROUTE_MAP[user.role])
        return <Loader loading={true} />
    }
    navigate(ROLE_ROUTE_MAP[ROLE_MAPING.ADMIN])
    return (
        <Loader loading={true} />
    );
}

export default Dashboard;
