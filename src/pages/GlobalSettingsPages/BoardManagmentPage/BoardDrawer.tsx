import React, { useState, useEffect } from "react";
import { Formik, Form } from "formik";
import { Transparent, DrawerPrimaryBtn } from "@components/Buttons/index";
import { InputField } from "@components/Input/Input";
import DrawerView from "@components/Drawer";
import {
  Container,
  DrawerFooter,
  DrawerInnerContent,
  DrawerLabel,
} from "./styles";
import { validationSchema, formInitValues } from "./helper";
import { Flex } from "@components/Flex";
import { isFormValid } from "@utils/helpers";
interface BoardDrawerProps {
  selectedBoard: any;
  onClose: () => void;
  onAddBoard: (board: any) => Promise<any>;
}

const BoardDrawer = (props: BoardDrawerProps) => {
  const { onClose, onAddBoard, selectedBoard } = props;
  const { id } = selectedBoard;
  const [formValues, setformValues] = useState < any > ("");
  const [errorState, setErrorState] = useState < {
    name: null | Array < string >;
    email: null | Array < string >;
  } > ({
    name: null,
    email: null,
  });

  useEffect(() => {
    setformValues(id ? selectedBoard : formInitValues);
  }, [id]);
  const onAddBoardHandler = async (values: any) => {
    const res = await onAddBoard(values);
    setErrorState(res.error);
  };
  return (
    <Container>
      <Formik
        initialValues={formValues}
        onSubmit={onAddBoardHandler}
        validationSchema={validationSchema}
        enableReinitialize={true}
      >
        {(props) => {
          const {
            values,
            setFieldValue,
            errors,
            touched,
            submitForm,
            setTouched,
          } = props;
          const isValid = isFormValid(errors, values);
          const setFieldsTouched = (field: string) => {
            const fieldIndex = Object.keys(values).indexOf(field);
            const touchedObject: any = { ...touched };
            Object.keys(values).forEach((key: string, index: number) => {
              if (index <= fieldIndex) {
                touchedObject[key] = true;
              }
            });
            setTouched(touchedObject);
          };
          return (
            <Form>
              <DrawerView
                onClose={onClose}
                visible={true}
                title={id ? `Edit board information` : `Add board information`}
              >
                <DrawerInnerContent>
                  <DrawerLabel>
                    Board name
                    <span>*</span>
                  </DrawerLabel>
                  <InputField
                    id={"name"}
                    name={"name"}
                    placeholder="Enter board name"
                    onChange={(e: any) => {
                      setFieldsTouched("name");

                      setFieldValue("name", e.target.value);
                    }}
                    error={
                      errorState.name
                        ? errorState.name[0]
                        : touched.name && errors.name
                    }
                    value={values.name}
                  />

                  <DrawerLabel>
                    Abbreviation
                    <span>*</span>
                  </DrawerLabel>
                  <InputField
                    id={"abbreviation"}
                    name={"abbreviation"}
                    placeholder="Enter board name's abbreviation"
                    onChange={(e: any) => {
                       setFieldsTouched("abbreviation");

                      setFieldValue("abbreviation", e.target.value);
                    }}
                    error={touched.abbreviation && errors.abbreviation}
                    value={values.abbreviation}
                  />

                  <DrawerLabel>
                    Email
                    <span>*</span>
                  </DrawerLabel>
                  <InputField
                    id={"email"}
                    name={"email"}
                    placeholder="Enter email"
                    onChange={(e: any) => {
                       setFieldsTouched("email");

                      setFieldValue("email", e.target.value);
                    }}
                    error={
                      errorState.email
                        ? errorState.email[0]
                        : touched.email && errors.email
                    }
                    value={values.email}
                  />

                  <DrawerLabel>
                    Address
                    <span>*</span>
                  </DrawerLabel>
                  <InputField
                    id={"address"}
                    name={"address"}
                    placeholder="Enter address"
                    onChange={(e: any) => {
                       setFieldsTouched("address");

                      setFieldValue("address", e.target.value);
                    }}
                    error={touched.address && errors.address}
                    value={values.address}
                  />
                </DrawerInnerContent>
                <DrawerFooter style={{ justifyContent: "flex-start" }}>
                  <Flex>
                    <DrawerPrimaryBtn
                      isDisable={!isValid}
                      onClick={() => {
                        submitForm();
                      }}
                      text={id ? "Save changes" : "Add Board"}
                    />
                  </Flex>
                  <Transparent text="Cancel" onClick={onClose} />
                </DrawerFooter>
              </DrawerView>
            </Form>
          );
        }}
      </Formik>
    </Container>
  );
};

export default BoardDrawer;
