import React from "react";
import DataTable from "@components/DataTable/DataTable";

import { getCols } from "./BoardTableColumns";
import { TableWrapper } from "@components/styles";
import EmptyDiv from "@components/EmptyState";
import EmptyTableImg from "@assets/EmptyTablePic.png";
import { Flex } from "@components/Flex";
interface BoardTableProps {
  onRowSelect: (row: any) => void;
  boards: Array<any>;
  loading: boolean;
  onSort: (key: string) => void;
  onAddBoard: () => void;
}

export default function BoardTable(props: BoardTableProps) {
  const { onRowSelect, onAddBoard, boards = [], loading, onSort } = props;

  return (
    <TableWrapper>
      <DataTable
        columns={getCols(onRowSelect, onSort)}
        //@ts-ignore
        dataSource={boards}
         pagination={false}
        infinity={true}
        loading={loading}
        renderEmpty={() => {
          return (
            <Flex top={70}  justifyContent={'center'}>
              <EmptyDiv
                image={EmptyTableImg}
                text={
                  <>
                    There are no regional boards listed. To add a regional board
                    please <a onClick={onAddBoard}>add a board</a>.
                  </>
                }
              />
            </Flex>
          );
        }}
      />
    </TableWrapper>
  );
}
