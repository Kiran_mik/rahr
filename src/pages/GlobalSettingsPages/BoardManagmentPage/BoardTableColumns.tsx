import { ThreeDots } from "@assets/index";
import { DropdownStyle } from "@pages/UserManagementPages/RAHStaffListPage/style";
import { Dropdown, Menu } from "antd";
import React from "react";

export interface DataType {
  board: string;
  abbreviation: string;
  email: string;
  address: string;
}

export const menu = (row: any, onRowSelect: (row: any) => void) => (
  <Menu className="MenuItems">
    <Menu.Item
      key="0"
      onClick={() => {
        onRowSelect(row);
      }}
    >
      Edit
    </Menu.Item>
  </Menu>
);

export const getCols = (onRowSelect: (row: any) => void,onSort:(key:string)=>void) => {
  return [
    {
      title: "Board",
      dataIndex: "name",
      key: "name",
      onHeaderCell: (column: any) => {
        return {
          onClick: () => {
            onSort("name");
          }
        };
      },
      sorter: (a: any, b: any) => {},
      render: (board: string) => {
        return <div style={{ color: "#17082D" }}>{board}</div>;
      },
    },
    {
      title: "Abbreviation",
      dataIndex: "abbreviation",
      key: "abbreviation",
      sorter: (a: any, b: any) => {},
      onHeaderCell: (column: any) => {
        return {
          onClick: () => {
            onSort("abbreviation");
          }
        };
      },
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
      render: (email: string) => {
        return <div style={{ color: "#531DAB" }}>{email}</div>;
      },
    },
    {
      title: "Address",
      dataIndex: "address",
      key: "address",
    },
    {
      title: "",
      dataIndex: "row",
      key: "row",
      render: (row: { id: number; index: number }) => {
        return (
          <DropdownStyle key={row.id + row.index} style={{ cursor: "pointer" }}>
            <Dropdown overlay={menu(row, onRowSelect)} trigger={["click"]}>
              <img
                src={ThreeDots}
                alt={ThreeDots}
                onClick={(e) => e.preventDefault()}
              />
            </Dropdown>
          </DropdownStyle>
        );
      },
    },
  ];
};

export const dataSource = [
  {
    board: "Barrie & District Association of Realtors",
    abbreviation: "BDAR",
    email: "info@bdar.ca",
    address: "676 Veterans Dr, Barrie, ON L9J 0H6",
    row: { id: 1 },
  },
  {
    board: "Brampton Real Estate Board Registration",
    abbreviation: "BREB",
    email: "info@breb@org",
    address: "60 Gillingham Drive, Suite 401 Brampton, ON, L6X 0Z9",
    row: { id: 2 },
  },
];
