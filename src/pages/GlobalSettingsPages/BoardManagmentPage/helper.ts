import * as Yup from "yup";
import services from "@services/index";

export const formInitValues = {
  name: "",
  abbreviation: "",
  email: "",
  address: "",
};

export const validationSchema = Yup.object({
  name: Yup.string().required("Please enter board name"),
  abbreviation: Yup.string().required("Please enter board name's abbreviation"),
  email: Yup.string()
    .email("Please enter a valid email")
    .nullable()
    .required("Please enter an email address"),
  address: Yup.string().required("Please enter address"),
});

export const addNewBoard = async (board: any) => {
  try {
    const method = board.id ? services.put : services.post;
    const url = board.id ? `v1/boards/${board.id}` : `v1/boards`;
    const res = await method(url, { ...board });
    return res;
  } catch (err) {
    return err;
  }
};
export const getBoards = async (sortObj: { key: string; order: string }) => {
  try {
    const res = await services.getTest(
      `v1/boards?sortBy=${sortObj.key}&order=${sortObj.order}`
    );
    //@ts-ignore
    return transformBoards(res.data.data || []);
  } catch (err) {
    return [];
  }
};

export const transformBoards = (boards: []) =>
  boards.map((i: any) => {
    i.row = { ...i };
    return i;
  });
