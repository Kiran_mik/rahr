import { Primary } from "@components/Buttons";
import { Flex } from "@components/Flex";
import StaffHeader from "@components/StaffHeader";
import { H3 } from "@components/Typography/Typography";
import { Breadcrumb } from "antd";
import { BreadItem, UserBreadWrap } from "@components/CommonLayouts/style";
import DashboardLayout from "@pages/UserManagementPages/StaffMembers/DashboardLayout";
import React, { useEffect, useState } from "react";
import BoardDrawer from "./BoardDrawer";
import BoardTable from "./BoardTable";
import { addNewBoard, getBoards } from "./helper";

function BoardManagmentPage(props: { path: string }) {
  const [selectedBoard, setSelectedBoard] = useState < null | {} > ();
  const [boards, setBoards] = useState < Array < any >> ([]);
  const [loading, setLoading] = useState < boolean > (false);
  const [currentSortOrder, setCurrentSortOrder] = useState({
    key: "name",
    order: "asc",
  });

  useEffect(() => {
    loadBoardData({ key: "name", order: "asc" });
  }, []);

  const loadBoardData = async (order: { key: string; order: string }) => {
    setLoading(true);
    const boardsRes = await getBoards(order);
    setBoards(boardsRes);
    setCurrentSortOrder(order);
    setLoading(false);
  };

  const onSort = async (key: string) => {
    const sortOrder =
      currentSortOrder.key === key || !currentSortOrder.key
        ? currentSortOrder.order === "asc"
          ? "desc"
          : "asc"
        : "asc";
    loadBoardData({ key, order: sortOrder });
  };

  const onAddBoard = async (board: any) => {
    const res = await addNewBoard(board);

    //@ts-ignore
    if (res.status === 422) {
      //@ts-ignore
      return res.error;
    }
    if (res) {
      setSelectedBoard(null);
      loadBoardData(currentSortOrder);
    }
  };

  return (
    <DashboardLayout>
      <StaffHeader>
        <UserBreadWrap>
          <H3 text="Board management" />
          <Breadcrumb separator="/">

            <BreadItem data-testid="breadcrumb-user" firstNav={true}>
              Global settings
            </BreadItem>

            <BreadItem firstNav={true}>
              Board management
            </BreadItem>
          </Breadcrumb>
        </UserBreadWrap>
      </StaffHeader>
      <Flex top={30} bottom={24} style={{ alignItems: "center" }}>
        <Flex flex={1}>
          <H3 text="Board Information" className="font-500" />
        </Flex>
        <Flex flex={0.2} justifyContent="flex-end">
          <Primary
            text="Add New"
            className="addbtn"
            onClick={() => {
              setSelectedBoard({});
            }}
          />
        </Flex>
      </Flex>
      <BoardTable
        onRowSelect={(row) => {
          setSelectedBoard(row);
        }}
        boards={boards}
        loading={loading}
        onSort={onSort}
        onAddBoard={() => {
          setSelectedBoard({});
        }}
      />
      {selectedBoard && (
        <BoardDrawer
          onClose={() => setSelectedBoard(null)}
          selectedBoard={selectedBoard}
          onAddBoard={onAddBoard}
        />
      )}
    </DashboardLayout>
  );
}

export default BoardManagmentPage;
