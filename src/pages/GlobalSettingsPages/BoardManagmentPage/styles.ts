import styled from "styled-components";
import { FormLableTypography } from "@components/Typography/Typography";

export const Container = styled.div``;

export const DrawerFooter = styled.div`
  display: flex;
  border-top: 1px solid #e2e2ee;
  padding: 24px 0px 18px 0px;
  justify-content: space-between;
  display: flex;
  border-top: 1px solid #e2e2ee;
  padding: 24px 0px 18px 0px;
  -webkit-box-pack: justify;
  -webkit-justify-content: space-between;
  -ms-flex-pack: justify;
  justify-content: space-between;
`;

export const DrawerInnerContent = styled.div`
  max-height: calc(100vh - 181px);
  overflow: auto;
  min-height: calc(100vh - 181px);
  padding: 24px 0px;
`;

export const DrawerLabel = styled(FormLableTypography)`
 font-size:14px;

`