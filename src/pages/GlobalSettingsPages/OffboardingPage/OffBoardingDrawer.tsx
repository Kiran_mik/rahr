import DrawerView from "@components/Drawer";
import React, { useEffect, useState } from "react";
import { Modal } from "antd";
import { Formik, Form } from "formik";
import { FormLableTypography } from "@components/Typography/Typography";
import { Transparent, DrawerPrimaryBtn } from "@components/Buttons/index";
import { InputField } from "@components/Input/Input";
import { DrwaerInnerContent, DrawerFooter } from "@components/styles";
import { Radio, Row, Select } from "antd";
import SelectInput from "@components/Select";
import { getBoards } from "../BoardManagmentPage/helper";
import {
    validationSchema,
    formInitValues,
    createForm,
    getFormDetails,
} from "./helper";
import { isFormValid } from "@utils/helpers";
import { Flex } from "@components/Flex";
import { FORM_TYPES } from "./constants";

interface OffBoardingDrawerProps {
    onClose: (revalidate?: boolean) => void;
    type: string;
    selectedForm?: any;
    onAdd?: (form: any) => void;
}
const { Option } = Select;
const OffBoardingDrawer = (props: OffBoardingDrawerProps) => {
    const { onClose, type, selectedForm, onAdd } = props;
    const [boards, setBoards] = useState<Array<any>>([]);
    const [formValues, setFormValues] = useState(formInitValues);
    const [errorState, setErrorState] = useState<{
        form_name: null | Array<string>;
    }>({
        form_name: null,
    });
    const title =
        type === "TERMINATION_FORM"
            ? "Termination Form"
            : type === "PARKING_FORM"
                ? "Parking Form"
                : "";

    useEffect(() => {
        loadBoards();
    }, []);
    useEffect(() => {
        (async () => {
            if (selectedForm && selectedForm.id) {
                const formDetails = await getFormDetails(selectedForm.id);
                setFormValues({
                    ...formDetails,
                    form_type: parseInt(formDetails.form_type),
                    board_id: parseInt(formDetails.board_id),
                });
            }
        })();
    }, [selectedForm && selectedForm.id]);

    const loadBoards = async () => {
        const boardList = await getBoards({ key: "name", order: "asc" });
        setBoards(boardList);
    };
    const onSubmit = async (values: any) => {
        submitForm(values);
    };

    const submitForm = async (values: any) => {
        const formType = FORM_TYPES[type];
        const res: any = (await createForm({
            ...values,
            offboarding_form_type: formType,
        })) as { error: { error: any } } | boolean;
        if (res !== true && res && res.error && res.error.error) {
            setErrorState(res.error.error);
            return;
        }
        onClose(true);
        if (onAdd) {
            onAdd(res.data);
        }

        return res;
    };

    return (
        <>
            <Formik
                initialValues={formValues}
                onSubmit={onSubmit}
                validationSchema={validationSchema}
                enableReinitialize={true}
            >
                {(props) => {
                    const {
                        values,
                        errors,
                        touched,
                        setTouched,
                        submitForm,
                        setFieldValue,
                    } = props;

                    const currentDocField =
                        values.form_type === 1 ? "docusign_id" : "form_link";
                    const isValid = isFormValid(errors, values, [
                        values.form_type === 1 ? "form_link" : "docusign_id",
                        "updated_at",
                    ]);

                    const setFieldsTouched = (field: string) => {
                        const fieldIndex = Object.keys(values).indexOf(field);
                        const touchedObject: any = { ...touched };
                        Object.keys(values).forEach((key: string, index: number) => {
                            if (index <= fieldIndex) {
                                touchedObject[key] = true;
                            }
                        });
                        setTouched(touchedObject);
                    };

                    return (
                        <DrawerView
                            onClose={() => onClose(false)}
                            visible={true}
                            title={`${selectedForm && selectedForm.id ? "Edit " + title : title
                                }`}
                        >
                            <DrwaerInnerContent>

                                <FormLableTypography>
                                    Select Board
                                    <span>*</span>
                                </FormLableTypography>

                                <SelectInput
                                    value={values.board_id || null}
                                    placeholdertitle="Select Board"
                                    onChange={(e: any) => {
                                        setFieldsTouched("board_id");
                                        setFieldValue("board_id", e);
                                    }}
                                    name={"board_id"}
                                    id={"board_id"}
                                    error={touched.board_id && errors.board_id}
                                >
                                    {boards.map((board: any) => {
                                        return (
                                            <Option key={board.id} value={board.id}>
                                                {board.name}
                                            </Option>
                                        );
                                    })}
                                </SelectInput>

                                <FormLableTypography>
                                    Form name
                                    <span>*</span>
                                </FormLableTypography>

                                <InputField
                                    value={values.form_name || ""}
                                    placeholder={"Enter form name"}
                                    onChange={(e: any) => {
                                        setFieldsTouched("form_name");
                                        setFieldValue("form_name", e.target.value);
                                    }}
                                    name={"form_name"}
                                    id={"form_name"}
                                    onBlur={() => {
                                        setTouched({ form_name: true });
                                    }}
                                    error={
                                        errorState.form_name
                                            ? errorState.form_name[0]
                                            : touched.form_name && errors.form_name
                                    }
                                />
                                <FormLableTypography>
                                    Select form type
                                    <span>*</span>
                                </FormLableTypography>
                                <Row className="squareRadioBtn">
                                    <Radio.Group
                                        value={values.form_type}
                                        onChange={(e) => {
                                            setFieldValue("form_type", e.target.value);
                                            if (!selectedForm) {
                                                setFieldValue(
                                                    values.form_type !== 1 ? "docusign_id" : "form_link",
                                                    ""
                                                );
                                            }
                                        }}
                                    >
                                        <Radio value={1}>DocuSign ID</Radio>
                                        <Radio value={2}>Form link</Radio>
                                    </Radio.Group>
                                </Row>
                                <FormLableTypography>
                                    {values.form_type === 1 ? "DocuSign ID" : "Link"}
                                    <span>*</span>
                                </FormLableTypography>

                                <InputField
                                    value={values[currentDocField] || ""}
                                    placeholder={values.form_type === 1 ? "DocuSign ID" : "Link"}
                                    onChange={(e: any) => {
                                        setFieldsTouched(currentDocField);
                                        setFieldValue(currentDocField, e.target.value);
                                    }}
                                    name={currentDocField}
                                    id={currentDocField}
                                    error={touched[currentDocField] && errors[currentDocField]}
                                />
                            </DrwaerInnerContent>
                            <DrawerFooter style={{ justifyContent: "flex-start" }}>
                                <Flex>
                                    <DrawerPrimaryBtn
                                        isDisable={!isValid}
                                        onClick={() => {
                                            isValid && submitForm();
                                        }}
                                        text={
                                            selectedForm && selectedForm.id
                                                ? "Save changes"
                                                : "Add Form"
                                        }
                                    />
                                </Flex>
                                <Transparent text="Cancel" onClick={() => onClose(false)} />
                            </DrawerFooter>
                        </DrawerView>
                    );
                }}
            </Formik>
        </>
    );
};

export default OffBoardingDrawer;
