import React, { useState, useEffect } from "react";

import EmptyDiv from "@components/EmptyState";
import EmptyTableImg from "@assets/RAHR_form_empty.png";

import { Primary } from "@components/Buttons";
import DataTable from "@components/DataTable/DataTable";
import { Flex } from "@components/Flex";
import { TableWrapper } from "@components/styles";
import { H3 } from "@components/Typography/Typography";

import OffBoardingDrawer from "./OffBoardingDrawer";
import { getParkingForms } from "./offBoardingFormHelpers";
import { getDocUrl, changeFormStatus } from "./helper";
import { getTerminationFormCols } from "./terminationFormCols";

interface ParkingFormrops {
    onDocView: (formDocuument: any) => void;
}
const ParkingForm = (props: ParkingFormrops) => {
    const { onDocView } = props;

    const [parkingForms, setParkingForms] = useState([]);
    const [loading, setLoading] = useState(false);
    const [selectedForm, setSelectedForm] = useState({});

    const [drawerType, setDrawerType] = useState<null | string>();

    useEffect(() => {
        formData();
    }, []);

    const onAction = async (row: any, actionType: string) => {
        if (actionType === "view") {
            if (parseInt(row.form_type) === 2) {
                // onDocView({ form_name: row.form_name, form_url: row.form_link });
                window.open(row.form_link, "_blank");
                return;
            }
            const res = (await getDocUrl(row)) as any;
            onDocView(res);
            return;
        }
        if (actionType === "status") {
            const res = await changeFormStatus(row);

            if (res) {
                formData();
            }
        }
        if (actionType === "edit") {
            setSelectedForm(row);
            setDrawerType("PARKING_FORM");
        }
    };

    const formData = async () => {
        setLoading(true);
        const res = await getParkingForms();
        //@ts-ignore
        setParkingForms(res);
        setLoading(false);
    };

    const cols = getTerminationFormCols(onAction);
    return (
        <>
            <div>
                <Flex top={30} bottom={20} style={{ alignItems: "center" }}>
                    <Flex flex={1}>
                        <H3 text="Parking" className="font-500" />
                    </Flex>
                    <Flex flex={0.2} justifyContent="flex-end">
                        <Primary
                            text="Add New"
                            onClick={() => setDrawerType("PARKING_FORM")}
                            className="addbtn"
                        />
                    </Flex>
                </Flex>
                <TableWrapper>
                    <DataTable
                        loading={loading}
                        dataSource={parkingForms || []}
                        columns={cols}
                        renderEmpty={() => {
                            return (
                                <Flex top={70} justifyContent={"center"}>
                                    <EmptyDiv
                                        image={EmptyTableImg}
                                        text={
                                            <>
                                                There are no parking forms. To add a parking form please
                                                &nbsp;
                                                <a onClick={() => setDrawerType("PARKING_FORM")}>
                                                    add a form
                                                </a>
                                                .
                                            </>
                                        }
                                    />
                                </Flex>
                            );
                        }}
                    />
                </TableWrapper>
            </div>
            {!!drawerType && (
                <OffBoardingDrawer
                    onClose={(revalidate) => {
                        setDrawerType(null);
                        setSelectedForm({})

                        if (revalidate) {
                            formData();
                        }
                    }}
                    type={drawerType}
                    selectedForm={selectedForm}
                />
            )}
        </>
    );
}

export default ParkingForm;
