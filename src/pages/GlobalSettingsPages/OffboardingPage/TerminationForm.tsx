import React, { useEffect, useState } from "react";
import { Primary } from "@components/Buttons";
import DataTable from "@components/DataTable/DataTable";
import { Flex } from "@components/Flex";
import { TableWrapper } from "@components/styles";
import { H3 } from "@components/Typography/Typography";
import OffBoardingDrawer from "./OffBoardingDrawer";
import EmptyDiv from "@components/EmptyState";
import EmptyTableImg from "@assets/EmptyTablePic.png";

import { getTerminationFormCols } from "./terminationFormCols";
import { getDocUrl, changeFormStatus } from "./helper";
import { getTerminationForms } from './offBoardingFormHelpers';

interface TerminationFormProps {
    revalidate?: boolean;
    onDocView: (formDocuument: any) => void;
}

const TerminationForm = (props: TerminationFormProps) => {
    const { onDocView } = props;
    const [terminationForms, setTerminationForms] = useState([]);
    const [drawerType, setDrawerType] = useState<null | string>();
    const [loading, setLoading] = useState(false);
    const [selectedForm, setSelectedForm] = useState({});


    const formData = async () => {
        setLoading(true);
        const res = await getTerminationForms();
        //@ts-ignore
        setTerminationForms(res);
        setLoading(false);
    };

    const onAction = async (row: any, actionType: string) => {
        if (actionType === "view") {
            if (parseInt(row.form_type) === 2) {
                // onDocView({ form_name: row.form_name, form_url: row.form_link });
                window.open(row.form_link, "_blank");
                return;
            }

            const res = (await getDocUrl(row)) as any;
            onDocView(res);
            return;
        }
        if (actionType === "status") {
            const res = await changeFormStatus(row);

            if (res) {
                formData();
            }
        }
        if (actionType === "edit") {
            setSelectedForm(row);
            setDrawerType("TERMINATION_FORM");
        }
    };

    const cols = getTerminationFormCols(onAction);

    useEffect(() => {
        formData();
    }, []);

    return (
        <>
            <div>
                <Flex top={30} bottom={20} style={{ alignItems: "center" }}>
                    <Flex flex={1}>
                        <H3 text="Termination forms" className="font-500" />
                    </Flex>
                    <Flex flex={0.2} justifyContent="flex-end">
                        <Primary
                            text="Add New"
                            onClick={() => setDrawerType("TERMINATION_FORM")}
                            className="addbtn"
                        />
                    </Flex>
                </Flex>
                <TableWrapper>
                    <DataTable
                        loading={loading}
                        dataSource={terminationForms}
                        columns={cols}
                        renderEmpty={() => {
                            return (
                                <Flex top={70} justifyContent={"center"}>
                                    <EmptyDiv
                                        image={EmptyTableImg}
                                        text={
                                            <>
                                                There are no termination forms. To add a form please add a termination form. To add a form please{" "}
                                                <a onClick={() => setDrawerType("TERMINATION_FORM")}>
                                                    add a termination form
                                                </a>
                                                .
                                            </>
                                        }
                                    />
                                </Flex>
                            );
                        }}
                    />
                </TableWrapper>
            </div>
            {!!drawerType && (
                <OffBoardingDrawer
                    onClose={(revalidate) => {
                        setDrawerType(null);
                        setSelectedForm({});
                        if (revalidate) {
                            formData();
                        }
                    }}
                    type={drawerType}
                    selectedForm={selectedForm}
                />
            )}
        </>
    );
}

export default TerminationForm;
