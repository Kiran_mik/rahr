interface ObjectString {
    [key: string]: any
}
export const FORM_TYPES: ObjectString = {
    "TERMINATION_FORM": 1,
    "PARKING_FORM": 2
}