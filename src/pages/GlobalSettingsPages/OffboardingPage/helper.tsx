import React from "react";
import * as Yup from "yup";
import services from "@services/index";
import { Menu, message } from "antd";
import { env } from "process";

export const formInitValues = {
    board_id: "",
    form_name: "",
    form_type: 1,
    docusign_id: "",
    form_link: "",
};

export const validationSchema = Yup.object({
    board_id: Yup.number().required("Please select board"),
    form_name: Yup.string().required("Please enter form name"),
    form_type: Yup.number(),
    docusign_id: Yup.string().when(["form_type"], {
        is: (field: any) => {
            return field === 1;
        },
        then: Yup.string()
            .nullable()
            .required("Please enter DocuSign Id "),
        otherwise: Yup.string().nullable(),
    }),
    form_link: Yup.string().when(["form_type"], {
        is: (field: any) => {
            return field === 2;
        },
        then: Yup.string()
            .nullable()
            .url("Please enter valid link")
            .required("Please enter  form link"),
        otherwise: Yup.string().nullable(),
    }),
});

export const precDeactivatevalidationSchema = Yup.object({
    selectedOption: Yup.string()
        .required("Please select from the list")
        .nullable(),
});

export const precDeactivateMenuValues = {
    selectedOption: null,
};

export const menu = (
    row: any,
    onAction: (row: any, actionType: string) => void
) => (
    <Menu className="MenuItems" style={{ borderRadius: "8px" }}>
        <Menu.Item
            key="1"
            disabled={row.status === 0}
            onClick={() => onAction(row, "view")}
        >
            View
        </Menu.Item>
        <Menu.Item
            key="2"
            disabled={row.status === 0}
            onClick={() => onAction(row, "edit")}
        >
            Edit
        </Menu.Item>
        <Menu.Item key="2" onClick={() => onAction(row, "status")}>
            {row.status === 1 ? "Deactivate" : "Activate"}
        </Menu.Item>
    </Menu>
);

export const createForm = async (values: any) => {
    try {
        const method = values.id ? services.put : services.post;
        const formatedValues = {
            ...values,
            docusign_id: values.form_type === 1 ? values.docusign_id : "",
            form_link: values.form_type === 2 ? values.form_link : "",
        };
        const url = values.id
            ? `v1/offboarding-forms/${values.id}`
            : "v1/offboarding-forms/";
        const res = await method(url, { ...formatedValues });
        //@ts-ignore
        return res;
    } catch (err) {
        return err;
    }
};

export const getFormDetails = async (id: string) => {
    try {
        const res = await services.getTest(`v1/offboarding-forms/${id}`);
        //@ts-ignore
        return res.data.data;
    } catch (err) {
        return false;
    }
};

export const getDocUrl = async (row: any) => {
    try {
        const res = (await services.getTest(
            `v1/offboarding-forms/documents/${row.id}`,
            process.env.REACT_APP_BASE_URL_GLOBAL_SETTINGS
        )) as {
            data: { data: any };
        };
        return res.data.data;
    } catch (err) {
        //@ts-ignore
        message.error(err.error ? err.error.message : "Something went wrong!");
    }
};


export const changeFormStatus = async (row: any) => {
    try {
        const res = await services.put(`v1/offboarding-forms/status/${row.id}`, {
            status: row.status === 1 ? 0 : 1,
        });
        //@ts-ignore
        return res.status_code === 200;
    } catch (err) {
        return false;
    }
};