import StaffHeader from "@components/StaffHeader";
import { H3 } from "@components/Typography/Typography";
import DashboardLayout from "@pages/UserManagementPages/StaffMembers/DashboardLayout";
import React, { useState } from "react";
import { Breadcrumb } from "antd";
import { BreadItem, UserBreadWrap } from "@components/CommonLayouts/style";
import TerminationForm from "./TerminationForm";
import ParkingForm from "./ParkingForm";
import PersonalRealEstateCorporationForm from "../OnboardingPage/PersonalRealEstateCorporationForm";
import PDFViewer from "@components/PDFViewer";

const OffboardingPage = (props: { path: string }) => {
    const [viewFormData, setViewFormData] = useState<null | {
        form_name: string;
        form_url: string;
        id: number;
    }>(null);

    return (
        <DashboardLayout>
            <StaffHeader>
                <UserBreadWrap>
                    <Breadcrumb separator="/">
                        <BreadItem data-testid="breadcrumb-user" firstNav={true}>
                            Global settings
                        </BreadItem>
                        <BreadItem firstNav={true}>Offboarding</BreadItem>
                    </Breadcrumb>
                    <H3 text="Offboarding" />
                </UserBreadWrap>
            </StaffHeader>

            <TerminationForm
                onDocView={(doc: any) => setViewFormData(doc)}
            />
            <ParkingForm
                onDocView={(doc: any) => setViewFormData(doc)}
            />

            {viewFormData && (
                <PDFViewer
                    title={viewFormData.form_name}
                    pdfURL={viewFormData.form_url}
                    onClose={() => setViewFormData(null)}
                />
            )}
        </DashboardLayout>
    );
}

export default OffboardingPage;
