import React from "react";
import { Dropdown, Menu } from "antd";
import { ThreeDots } from "@assets/index";
import { DropdownStyle } from "@pages/UserManagementPages/RAHStaffListPage/style";
import { H5Typography } from "@components/Typography/Typography";
import Date from "@components/Date";
import moment from "moment";

export const menu = (
    row: any,
    onAction: (row: any, actionType: string) => void
) => (
    <Menu className="MenuItems" style={{ borderRadius: "8px" }}>
        <Menu.Item
            key="1"
            disabled={row.status === 0}
            onClick={() => onAction(row, "view")}
        >
            View
        </Menu.Item>
        <Menu.Item
            key="2"
            disabled={row.status === 0}
            onClick={() => onAction(row, "edit")}
        >
            Edit
        </Menu.Item>

        <Menu.Item key="3" onClick={() => onAction(row, "status")}>
            {row.status === 1 ? "Deactivate" : "Activate"}
        </Menu.Item>
    </Menu>
);

export const getTerminationFormCols = (
    onAction: (row: any, actionType: string) => void
) => {
    return [
        {
            title: "Form Name",
            dataIndex: "form_name",
            key: "form_name",
            sorter: (a: any, b: any) => {
                return a.form_name.localeCompare(b.form_name);
            },

            render: (name: any, row: any) => {
                return (
                    <H5Typography
                        style={{
                            fontSize: 14,
                            fontWeight: 400,
                            color: row.status === 1 ? "black" : "#A2A2BA",
                        }}
                    >
                        {name}
                    </H5Typography>
                );
            },
        },
        {
            title: "DocuSign ID",
            key: "docusign_id",
            dataIndex: "docusign_id",
            sorter: (a: any, b: any) => {
                return a.docusign_id && a.docusign_id.localeCompare(b.docusign_id);
            },

            render: (docusign_id: string, row: any) => (
                <H5Typography
                    style={{
                        fontSize: 14,
                        fontWeight: 400,
                        color: row.status === 1 ? "black" : "#A2A2BA",
                    }}
                >
                    {docusign_id || "-"}
                </H5Typography>
            ),
        },
        {
            title: "Form link",
            key: "form_link",
            dataIndex: "form_link",
            sorter: (a: any, b: any) => {
                return a.form_link && a.form_link.localeCompare(b.form_link);
            },

            render: (link: string, row: any) => {
                return link ? (
                    <a
                        {...(row.status === 1 ? { href: link, target: "_blank" } : {})}
                        style={{
                            fontWeight: 400,
                            fontSize: 14,
                            color: row.status === 1 ? "#531DAB" : "#A2A2BA",
                            cursor: row.status === 1 ? "pointer" : "no-drop",
                        }}
                    >
                        Link
                    </a>
                ) : (
                    "-"
                );
            },
        },
        {
            title: "Last modified",
            key: "updated_at",
            dataIndex: "updated_at",
            sorter: (a: any, b: any, dir: any) => {
                const aDate = moment(a.updated_at);

                const bDate = moment(b.updated_at);
                //@ts-ignore
                return bDate - aDate;
            },

            render: (updated_at: string, row: any) => {
                return <Date date={updated_at} isDisabled={row.status === 1} />;
            },
        },

        {
            title: "",
            dataIndex: "formObject",
            key: "formObject",
            render: (row: { id: number; index: number }) => {
                return (
                    <DropdownStyle
                        key={row.id + "" + row.index}
                        style={{ cursor: "pointer" }}
                    >
                        <Dropdown overlay={menu(row, onAction)} trigger={["click"]}>
                            <img
                                src={ThreeDots}
                                alt={ThreeDots}
                                onClick={(e) => e.preventDefault()}
                            />
                        </Dropdown>
                    </DropdownStyle>
                );
            },
        },
    ];
};
