import React, { useEffect, useState } from "react";
import { Primary } from "@components/Buttons";
import DataTable from "@components/DataTable/DataTable";
import { Flex } from "@components/Flex";
import { TableWrapper } from "@components/styles";
import { H3 } from "@components/Typography/Typography";
import OnBoardingDrawer from "./OnBoardingDrawer";
import EmptyDiv from "@components/EmptyState";
import EmptyTableImg from "@assets/EmptyTablePic.png";

import { changeFormStatus } from "./apiCalls";
import { getRegistrationFormCols } from "./boardFormCols";
import { getRegistrationForms } from "./regirstrationFormHelpers";
import { getDocUrl } from "./helper";

interface OnboardingRegestrationFormProps {
  revalidate?: boolean;
  onDocView: (formDocuument: any) => void;
}

function OnboardingRegestrationForm(props: OnboardingRegestrationFormProps) {
  const { onDocView } = props;
  const [regirstrationForms, setRegistrationForms] = useState([]);
  const [drawerType, setDrawerType] = useState<null | string>();
  const [loading, setLoading] = useState(false);
  const [selectedForm, setSelectedForm] = useState({});
  useEffect(() => {
    formData();
  }, []);

  const formData = async () => {
    setLoading(true);
    const res = await getRegistrationForms();
    //@ts-ignore
    setRegistrationForms(res);
    setLoading(false);
  };
  const onAction = async (row: any, actionType: string) => {
    if (actionType === "view") {
      if (parseInt(row.form_type) === 2) {
        // onDocView({ form_name: row.form_name, form_url: row.form_link });
        window.open(row.form_link, "_blank");
        return;
      }

      const res = (await getDocUrl(row)) as any;
      onDocView(res);
      return;
    }
    if (actionType === "status") {
      const res = await changeFormStatus(row);

      if (res) {
        formData();
      }
    }
    if (actionType === "edit") {
      setSelectedForm(row);
      setDrawerType("BOARD_REGISTRATION");
    }
  };

  const cols = getRegistrationFormCols(onAction);

  return (
    <>
      <div>
        <Flex top={30} bottom={20} style={{ alignItems: "center" }}>
          <Flex flex={1}>
            <H3 text="Board Registration Forms" className="font-500" />
          </Flex>
          <Flex flex={0.2} justifyContent="flex-end">
            <Primary
              text="Add New"
              onClick={() => setDrawerType("BOARD_REGISTRATION")}
              className="addbtn"
            />
          </Flex>
        </Flex>
        <TableWrapper>
          <DataTable
            loading={loading}
            dataSource={regirstrationForms || []}
            columns={cols}
            renderEmpty={() => {
              return (
                <Flex top={70} justifyContent={"center"}>
                  <EmptyDiv
                    image={EmptyTableImg}
                    text={
                      <>
                        There are no board registration forms. To add a
                        registration form please{" "}
                        <a onClick={() => setDrawerType("BOARD_REGISTRATION")}>
                          add a form
                        </a>
                        .
                      </>
                    }
                  />
                </Flex>
              );
            }}
          />
        </TableWrapper>
      </div>
      {!!drawerType && (
        <OnBoardingDrawer
          onClose={(revalidate) => {
            setDrawerType(null);
            setSelectedForm({});
            if (revalidate) {
              formData();
            }
          }}
          type={drawerType}
          selectedForm={selectedForm}
        />
      )}
    </>
  );
}

export default OnboardingRegestrationForm;
