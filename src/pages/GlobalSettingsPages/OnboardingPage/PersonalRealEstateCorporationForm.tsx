import { Primary } from "@components/Buttons";
import EmptyDiv from "@components/EmptyState";
import EmptyTableImg from "@assets/Personal_form.png";
import DataTable from "@components/DataTable/DataTable";
import { Flex } from "@components/Flex";
import { TableWrapper } from "@components/styles";
import { H3 } from "@components/Typography/Typography";
import React, { useState, useEffect } from "react";
import { getDocUrl } from "./helper";
import OnBoardingDrawer from "./OnBoardingDrawer";
import { changeFormStatus } from "./apiCalls";
import { getPersonalEstateForms } from "./regirstrationFormHelpers";
import { getRegistrationFormCols } from "./boardFormCols";
import { Modal } from "antd";
import {
  precDeactivatevalidationSchema,
  precDeactivateMenuValues,
} from "./helper";
import { Formik } from "formik";
import PRECDeactivateModalView from "./precDeactivateModalView";

interface PersonalRealEstateCorporationFormprops {
  onDocView: (formDocuument: any) => void;
}

function PersonalRealEstateCorporationForm(
  props: PersonalRealEstateCorporationFormprops
) {
  const { onDocView } = props;

  const [drawerType, setDrawerType] = useState<null | string>();
  const [loading, setLoading] = useState(false);
  const [personalRealEstateForm, setPersonalRealEstateForm] = useState([]);
  const [selectedForm, setSelectedForm] = useState({});
  const [showModal, setShowModal] = useState<any>(null);
  const [modalType, setModalType] = useState<any>(null);
  const [onAddSetStatusActivate, setOnAddSetStatusActivate] = useState<boolean>(
    false
  );

  useEffect(() => {
    formData();
  }, []);

  const onAction = async (row: any, actionType: string) => {
    if (actionType === "view") {
      if (parseInt(row.form_type) === 2) {
        // onDocView({ form_name: row.form_name, form_url: row.form_link });
        window.open(row.form_link, "_blank");

        return;
      }
      const res = (await getDocUrl(row)) as any;
      onDocView(res);
      return;
    }
    if (actionType === "status") {
      if (row.status == 0) {
        setModalType("ON_ACTIVATION");
      } else {
        setModalType("ON_DEACTIVATION");
      }
      setSelectedForm(row);
      setShowModal(true);
    }
    if (actionType === "edit") {
      setSelectedForm(row);
      setDrawerType("PERSONAL_REAL_ESTATE_CORPORATION_FORM");
    }
  };

  const formData = async () => {
    setLoading(true);
    const res = await getPersonalEstateForms();
    //@ts-ignore
    setPersonalRealEstateForm(res);
    setLoading(false);
  };
  const cols = getRegistrationFormCols(onAction);

  const onAdd = () => {
    if (personalRealEstateForm.length > 0) {
      setModalType("ON_ADD");
      setShowModal(true);
    } else {
      setDrawerType("PERSONAL_REAL_ESTATE_CORPORATION_FORM");
    }
  };

  const onContinueClickHandler = async (formikProps: any) => {
    let { handleSubmit } = formikProps;
    if (modalType == "ON_ACTIVATION") {
      setShowModal(false);
      const res = await changeFormStatus(selectedForm);
      if (res) {
        clearData();
        formData();
      }
    } else if (modalType == "ON_DEACTIVATION") {
      handleSubmit();
    } else if (modalType == "ON_ADD") {
      setShowModal(false);
      clearData();
      setDrawerType("PERSONAL_REAL_ESTATE_CORPORATION_FORM");
    }
  };

  const deactivationSubmitHandler = async (values: any, resetForm: any) => {
    setShowModal(false);
    if (values.selectedOption == 0) {
      //if opt to create new form
      clearData();
      setOnAddSetStatusActivate(true);
      setDrawerType("PERSONAL_REAL_ESTATE_CORPORATION_FORM");
      resetForm();
    } else {
      let row = personalRealEstateForm.filter((i: any) => {
        return i.id == values.selectedOption;
      })[0];
      const res = await changeFormStatus(row);
      if (res) {
        formData();
        clearData();
        resetForm();
      }
    }
  };

  const clearData = () => {
    setSelectedForm({});
    setModalType(null);
  };

  /**
   * set status activate after adding first PREC form OR
   * if user has opt for create new from deactivate modal.
   */
  const onAddPrecFormCallback = async (data: any) => {
    if(personalRealEstateForm.length){
      let form: any = data;
      //execute only on post request, ignore put request
      if (typeof form === "object") {
        onAddSetStatusActivate || personalRealEstateForm.length == 0
          ? (form.status = 0)
          : (form.status = 1);
        const res = await changeFormStatus(form);
      }
      clearData();
      setOnAddSetStatusActivate(false);  
    }
    formData();
  };

  const getModalContent = (type: string, formikProps: any) => {
    switch (type) {
      case "ON_ACTIVATION":
        return (
          <p>
            Only one form may be active at a time and the currently active form
            will be automatically deactivated in place of this one.
          </p>
        );
      case "ON_DEACTIVATION":
        return (
          <PRECDeactivateModalView
            formik={formikProps}
            optionsList={personalRealEstateForm}
            rowData={selectedForm}
          />
        );
      case "ON_ADD":
        return (
          <p>If you create new PREC forms it will be deactivated by default</p>
        );
      default:
        return null;
    }
  };

  return (
    <>
      <div>
        <Flex top={30} bottom={20} style={{ alignItems: "center" }}>
          <Flex flex={1}>
            <H3
              text="Personal Real Estate Corporation Form"
              className="font-500"
            />
          </Flex>
          <Flex flex={0.2} justifyContent="flex-end">
            <Primary text="Add New" onClick={onAdd} className="addbtn" />
          </Flex>
        </Flex>
        <TableWrapper>
          <DataTable
            loading={loading}
            dataSource={personalRealEstateForm || []}
            columns={cols}
            renderEmpty={() => {
              return (
                <Flex top={70} justifyContent={"center"}>
                  <EmptyDiv
                    image={EmptyTableImg}
                    text={
                      <>
                        There are no PREC forms. To add a PREC form please
                        &nbsp;
                        <a onClick={onAdd}>add a form</a>.
                      </>
                    }
                  />
                </Flex>
              );
            }}
          />
        </TableWrapper>
      </div>
      {!!drawerType && (
        <OnBoardingDrawer
          onClose={(revalidate) => {
            setDrawerType(null);
            setSelectedForm({});
            setOnAddSetStatusActivate(false);
            if (revalidate) {
              formData();
            }
          }}
          type={drawerType}
          selectedForm={selectedForm}
          onAdd={async (form: any) => {
            onAddPrecFormCallback(form);
          }}
        />
      )}
      <Formik
        initialValues={precDeactivateMenuValues}
        onSubmit={(values, { resetForm }) => {
          deactivationSubmitHandler(values, resetForm);
        }}
        enableReinitialize={true}
        validationSchema={precDeactivatevalidationSchema}
      >
        {(formikProps) => {
          let { resetForm } = formikProps;
          return (
            <Modal
              title={"Modal"}
              visible={showModal}
              onOk={() => {
                onContinueClickHandler(formikProps);
              }}
              onCancel={() => {
                setShowModal(false);
                clearData();
                resetForm();
              }}
              okText="Continue"
              cancelText="Cancel"
            >
              {getModalContent(modalType, formikProps)}
            </Modal>
          );
        }}
      </Formik>
    </>
  );
}

export default PersonalRealEstateCorporationForm;
