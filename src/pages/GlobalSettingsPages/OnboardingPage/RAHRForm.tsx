import React, { useState, useEffect } from "react";

import EmptyDiv from "@components/EmptyState";
import EmptyTableImg from "@assets/RAHR_form_empty.png";

import { Primary } from "@components/Buttons";
import DataTable from "@components/DataTable/DataTable";
import { Flex } from "@components/Flex";
import { TableWrapper } from "@components/styles";
import { H3 } from "@components/Typography/Typography";

import OnBoardingDrawer from "./OnBoardingDrawer";
import { getHomeRealityForms } from "./regirstrationFormHelpers";
import { changeFormStatus } from "./apiCalls";
import { getDocUrl } from "./helper";
import { getRegistrationFormCols } from "./boardFormCols";

interface RAHRFormprops {
  onDocView: (formDocuument: any) => void;
}
function RAHRForm(props: RAHRFormprops) {
  const { onDocView } = props;

  const [homeRealityForms, setHomeRealityForms] = useState([]);
  const [loading, setLoading] = useState(false);
  const [selectedForm, setSelectedForm] = useState({});

  const [drawerType, setDrawerType] = useState<null | string>();

  useEffect(() => {
    formData();
  }, []);

  const onAction = async (row: any, actionType: string) => {
    if (actionType === "view") {
      if (parseInt(row.form_type) === 2) {
        // onDocView({ form_name: row.form_name, form_url: row.form_link });
        window.open(row.form_link, "_blank");

        return;
      }
      const res = (await getDocUrl(row)) as any;
      onDocView(res);
      return;
    }
    if (actionType === "status") {
      const res = await changeFormStatus(row);

      if (res) {
        formData();
      }
    }
    if (actionType === "edit") {
      setSelectedForm(row);
      setDrawerType("RAHR_FORM");
    }
  };

  const formData = async () => {
    setLoading(true);
    const res = await getHomeRealityForms();
    //@ts-ignore
    setHomeRealityForms(res);
    setLoading(false);
  };

  const cols = getRegistrationFormCols(onAction);
  return (
    <>
      <div>
        <Flex top={30} bottom={20} style={{ alignItems: "center" }}>
          <Flex flex={1}>
            <H3 text="Right At Home Realty Forms" className="font-500" />
          </Flex>
          <Flex flex={0.2} justifyContent="flex-end">
            <Primary
              text="Add New"
              onClick={() => setDrawerType("RAHR_FORM")}
              className="addbtn"
            />
          </Flex>
        </Flex>
        <TableWrapper>
          <DataTable
            loading={loading}
            dataSource={homeRealityForms || []}
            columns={cols}
            renderEmpty={() => {
              return (
                <Flex top={70} justifyContent={"center"}>
                  <EmptyDiv
                    image={EmptyTableImg}
                    text={
                      <>
                        There are no RAHR forms. To add a RAHR form please
                        &nbsp;
                        <a onClick={() => setDrawerType("RAHR_FORM")}>
                          add a form
                        </a>
                        .
                      </>
                    }
                  />
                </Flex>
              );
            }}
          />
        </TableWrapper>
      </div>
      {!!drawerType && (
        <OnBoardingDrawer
          onClose={(revalidate) => {
            setDrawerType(null);
            setSelectedForm({});

            if (revalidate) {
              formData();
            }
          }}
          type={drawerType}
          selectedForm={selectedForm}
        />
      )}
    </>
  );
}

export default RAHRForm;
