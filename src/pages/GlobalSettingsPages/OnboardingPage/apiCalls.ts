import services from "@services/index";

export const changeFormStatus = async (row: any) => {
  try {
    const res = await services.put(`v1/onboarding-forms/status/${row.id}`, {
      status: row.status === 1 ? 0 : 1,
    });
    //@ts-ignore
    return res.status_code === 200;
  } catch (err) {
    return false;
  }
};
