interface ObjectString{
    [key:string]:any
}
export const FORM_TYPES:ObjectString={
    "BOARD_REGISTRATION":1,
    "RAHR_FORM":2,
    "PERSONAL_REAL_ESTATE_CORPORATION_FORM":3
}