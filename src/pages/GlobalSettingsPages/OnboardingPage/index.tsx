import StaffHeader from "@components/StaffHeader";
import { H3 } from "@components/Typography/Typography";
import DashboardLayout from "@pages/UserManagementPages/StaffMembers/DashboardLayout";
import React, { useState } from "react";
import { Breadcrumb } from "antd";
import { BreadItem, UserBreadWrap } from "@components/CommonLayouts/style";
import OnboardingRegestrationForm from "./OnboardingRegestrationForm";
import RAHRForm from "./RAHRForm";
import PersonalRealEstateCorporationForm from "./PersonalRealEstateCorporationForm";
import PDFViewer from "@components/PDFViewer";
function OnboardingPage(props: { path: string }) {
  const [viewFormData, setViewFormData] = useState<null | {
    form_name: string;
    form_url: string;
    id: number;
  }>(null);

  return (
    <DashboardLayout>
      <StaffHeader>
        <UserBreadWrap>
          <H3 text="Onboarding" />
          <Breadcrumb separator="/">
            <BreadItem data-testid="breadcrumb-user" firstNav={true}>
              Global settings
            </BreadItem>

            <BreadItem firstNav={true}>Onboarding</BreadItem>
          </Breadcrumb>
        </UserBreadWrap>
      </StaffHeader>

      <OnboardingRegestrationForm
        onDocView={(doc: any) => setViewFormData(doc)}
      />
      <RAHRForm 
              onDocView={(doc: any) => setViewFormData(doc)}

      />
      <PersonalRealEstateCorporationForm
        onDocView={(doc: any) => setViewFormData(doc)}
      />

      {viewFormData && (
        <PDFViewer
          title={viewFormData.form_name}
          pdfURL={viewFormData.form_url}
          onClose={() => setViewFormData(null)}
        />
      )}
    </DashboardLayout>
  );
}

export default OnboardingPage;
