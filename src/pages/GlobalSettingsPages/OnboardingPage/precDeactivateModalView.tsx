import React from "react";
import { Row, Col } from "antd";
import { H3, FormLableTypography } from "@components/Typography/Typography";
import SelectInput from "@components/Select";
import { Select } from "antd";
const { Option } = Select;

const PRECDeactivateModalView: React.FC<{
  formik: any;
  optionsList: any;
  rowData: any;
}> = (props: any) => {
  let { optionsList, formik, rowData } = props;
  const { errors, values, setFieldValue } = formik;
  const dataList = optionsList.filter((i: any) => {
    return i.id != rowData.id;
  });

  return (
    <Row gutter={16}>
      <Col span={24}>
        <FormLableTypography style={{ marginBottom: "10px" }}>
          There must be an active PREC form at all times, please choose one to activate
          <span>*</span>
        </FormLableTypography>
      </Col>
      <Col span={16}>
        <SelectInput
          id="selectedOption"
          name="selectedOption"
          placeholdertitle="Select from a list"
          error={errors.selectedOption}
          value={values.selectedOption}
          onChange={(e: any) => {
            setFieldValue("selectedOption", e);
          }}
        >
          <Option value={0}>{"Create New"}</Option>
          {dataList.map((item: any) => {
            return (
              <>
                <Option value={item.id}>{item.form_name}</Option>
              </>
            );
          })}
        </SelectInput>
      </Col>
    </Row>
  );
};

export default PRECDeactivateModalView;
