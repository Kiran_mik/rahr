import services from "@services/index";

export const getRegistrationForms = async () => {
  try {
    const res = await services.getTest(`v1/onboarding-forms/type/1`);
    //@ts-ignore
    return transformFormData(res.data.data || []);
  } catch (err) {
    return [];
  }
};

export const getHomeRealityForms = async () => {
  try {
    const res = await services.getTest(`v1/onboarding-forms/type/2`);
    //@ts-ignore
    return transformFormData(res.data.data || []);
  } catch (err) {
    return [];
  }
};

export const getPersonalEstateForms = async () => {
  try {
    const res = await services.getTest(`v1/onboarding-forms/type/3`);
    //@ts-ignore
    return transformFormData(res.data.data || []);
  } catch (err) {
    return [];
  }
};

export const transformFormData = (res: Array<any>) => {
  const data = res.map(
    (row: {
      id: number;
      form_name: string;
      onboarding_form_type: number;
      form_type: number;
      form_link: string;
      docusign_id: string;
      status: number;
      updated_at: string;
    }) => ({
      ...row,
      formObject: { ...row },
    })
  );
  return data.sort((a: any, b: any) => {
    const aDate = new Date(a.updated_at)
    const bDate = new Date(b.updated_at)
    //@ts-ignore
    return bDate - aDate

  })
}