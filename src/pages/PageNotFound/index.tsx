import { Flex } from "@components/Flex";
import React from "react";

function PageNotFound(props: any) {
  return (
    <Flex
      direction="column"
      style={{ height: "100vh" }}
      justifyContent={"center"}
    >
      <Flex justifyContent={"center"}>
        <img src="https://i.stack.imgur.com/6M513.png" />
      </Flex>
    </Flex>
  );
}

export default PageNotFound;
