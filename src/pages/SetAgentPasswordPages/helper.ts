import { signInUser } from "@services/AuthService";
import services from "@services/index";
import { authActions } from "@store/authReducer";
import store from "@store/index";
import Cookies from "js-cookie";

import * as Yup from "yup";

export const validationSchema = Yup.object({
    password: Yup.string().required('Please enter password').min(8,"Password must have atleast 8 characters"),
    confPassword: Yup.string().oneOf([Yup.ref('password'), null], 'Passwords must match')


})

export const setPassword = async (email: string, password: string, token: string) => {

    try {
        const res = await services.post('v1/agents/set-password', { password, token }) as { data: { data: {} } };
        const loginRes = (await signInUser(email, password)) as {
            data: { access_token: string; role?: number };
            success: boolean;
        };
        store.dispatch(authActions.setAuth(loginRes.data.access_token));
        Cookies.set("token", loginRes.data.access_token, { expires: 7 });

        return res.data.data
    } catch (err) {
        return err;
    }
}



export const isTokenValid = async (token: string) => {
    try {
        const res = await services.getTest(`v1/agents/validate-token?token=${token}`)
        return res;
    } catch (error) {
        return error
    }
}