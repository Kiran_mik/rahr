import React, { FC, useEffect, useState } from "react";
import { navigate, RouteComponentProps } from "@reach/router";
import SignUpWrapper from "@components/SignUp/Layout";
import { Primary } from "@components/Buttons";
import {
  H4Typography,
  H5Typography,
  Label,
} from "@components/Typography/Typography";
import { InputField } from "@components/Input/Input";
import { Checkbox } from "@components/Checkbox";
import { Flex } from "@components/Flex";
import { Formik } from "formik";

import {
  FormItems,
  SubHeading,
  FormItem,
  Header as AppHeader,
  PrimaryButtonWrapper,
} from "./style";
import { isTokenValid, setPassword, validationSchema } from "./helper";
import { message } from "antd";

export interface SignupProps extends RouteComponentProps {
  token?: string;
}
export interface ChildrenProps {
  name?: string;
}

const SetAgentPasswordPages: FC<SignupProps> = (props) => {
  const { token } = props;
  const [userInfo, setUserInfo] = useState<null | { email: string }>(null);
  const [errormsg, setError] = useState<boolean>(false);

  useEffect(() => {
    validateToken();
  }, [token]);

  const validateToken = async () => {
    if (token) {
      const res = (await isTokenValid(token)) as any;

      if (res.status === 200) {
        setUserInfo(res.data.data);
      } else {
        if (res.error && res.error.error) {
          const errorValue = Object.values(res.error.error) as Array<string>;
          message.error(errorValue[0]);
          navigate("/");
        }
      }
    }
  };
  const onSubmit = async (values: any) => {
    if (token && userInfo) {
      await setPassword(userInfo.email, values.password, token);
      navigate("/");
    }
  };
  return (
    <>
      <SignUpWrapper>
        <Formik
          initialValues={{
            password: "",
            confPassword: "",
          }}
          onSubmit={onSubmit}
          validationSchema={validationSchema}
        >
          {(props) => {
            const {
              setTouched,
              handleChange,
              values,
              touched,
              handleSubmit,
              errors,
            } = props;

            const setFieldsTouched = (field: string) => {
              const fieldIndex = Object.keys(values).indexOf(field);
              const touchedObject: any = { ...touched };
              Object.keys(values).forEach((key: string, index: number) => {
                if (index <= fieldIndex) {
                  touchedObject[key] = true;
                }
              });
              setTouched(touchedObject);
            };
            return (
              <div>
                <AppHeader text="Join Right at Home Realty" />
                <SubHeading>Create your account in just few steps </SubHeading>
                <FormItems>
                  {!!userInfo && (
                    <FormItem>
                      <H5Typography
                        style={{
                          fontSize: 12,
                          fontWeight: "normal",
                          color: "#7B7B97",
                          lineHeight: 1,
                        }}
                      >
                        Email ID
                      </H5Typography>
                      <H5Typography
                        style={{
                          fontSize: 14,
                          fontWeight: "normal",
                          color: "#17082D",
                          marginBottom: 10,
                        }}
                      >
                        {userInfo.email}
                      </H5Typography>
                    </FormItem>
                  )}
                  <FormItem>
                    <Label text="New Password" />
                    <InputField
                      placeholder="Password"
                      id={"password"}
                      name={"password"}
                      type="password"
                      onChange={(e) => {
                        setFieldsTouched("password");

                        handleChange(e);
                      }}
                      value={values.password}
                      error={touched.password && errors.password}
                    />
                  </FormItem>

                  <FormItem>
                    <Label text="Confirm password" />
                    <InputField
                      placeholder="Confirm Password"
                      id={"confPassword"}
                      name={"confPassword"}
                      type="password"
                      value={values.confPassword}
                      error={touched.confPassword && errors.confPassword}
                      onChange={(e) => {
                        setFieldsTouched("confPassword");

                        handleChange(e);
                      }}
                    />
                  </FormItem>
                  <FormItem>
                    <Flex>
                      <Checkbox />
                      <H4Typography style={{ marginLeft: 10 }}>
                        Remember me for 30 days
                      </H4Typography>
                    </Flex>
                  </FormItem>

                  <FormItem>
                    <PrimaryButtonWrapper>
                      <Primary text="Update password" onClick={handleSubmit} />
                    </PrimaryButtonWrapper>
                  </FormItem>
                </FormItems>
              </div>
            );
          }}
        </Formik>
      </SignUpWrapper>
    </>
  );
};

export default SetAgentPasswordPages;
