import styled from "styled-components";
import { InputField } from "@components/Input/Input";
import { H3 } from "@components/Typography/Typography";

export const Header = styled(H3)`
margin-bottom: 8px;
    font-weight: 600;
`;

export const FormInput = styled(InputField)`
background: #FFFFFF;
box-sizing: border-box;
border:none
border-radius: 4px;
height:41px;
margin:auto;

@media (max-width: 900px) {
    width:100%;
    margin:auto;

  }
`
export const FormItems = styled.div`
display:block;

`
export const FormItem = styled.div`
display:block;
`
const Title = styled.h1`

`
export const SubHeading = styled.p`
  margin-bottom: 40px;
  line-height:22px;
  font-size:14px;
  color:#59596B;
font-family:Poppins;
`;

export const ForgotLabel = styled.a`
  color: #4e1c95;
  justify-content: center;
  display: flex;
    margin: 20px auto auto auto;
    font-size: 14px;
    line-height: 16px;
    font-family: Poppins;
    font-weight: 400;
    width:max-content;
`;
export const PrimaryButtonWrapper = styled.div`
  margin-top:40px !important;
`;