import DrawerView from "@components/Drawer";
import React from "react";
import {
  Container,
  DrawerFooter,
  DrwaerInnerContent,
  DrwaerLabel
} from "../StaffMembers/EditStaff/style";
import { Transparent, DrawerBorderBtn } from "@components/Buttons/index";
import { InputField } from "@components/Input/Input";

const BoardDrawer = () => {
  // let perms = JSON.parse(JSON.stringify(tableValues));

  return (
    <Container>
      <DrawerView onClose={() => {}} visible={true} title={`Add new Board`}>
        <DrwaerInnerContent>
          <DrwaerLabel>
            Board name
            <span>*</span>
          </DrwaerLabel>

          <InputField
            value={"Boardname"}
            placeholder="Board Name"
            onChange={(e: any) => {}}
            name={"Boardname"}
            id={"Boardname"}
          />
          <DrwaerLabel>
            Abbreviation
            <span>*</span>
          </DrwaerLabel>

          <InputField
            value={"Abbreviation"}
            placeholder="Abbreviation"
            onChange={(e: any) => {}}
            name={"Abbreviation"}
            id={"Abbreviatione"}
          />
          <DrwaerLabel>
            Email
            <span>*</span>
          </DrwaerLabel>

          <InputField
            value={"Email"}
            placeholder="Email"
            onChange={(e: any) => {}}
            name={"Email"}
            id={"Email"}
          />
          <DrwaerLabel>
            Abbreviation
            <span>*</span>
          </DrwaerLabel>

          <InputField
            value={"Address"}
            placeholder="Abbreviation"
            onChange={(e: any) => {}}
            name={"Address"}
            id={"Address"}
          />
        </DrwaerInnerContent>
        <DrawerFooter>
          <DrawerBorderBtn onClick={() => {}} text="Add Board" />
          <Transparent text="Cancel" onClick={() => {}} />
        </DrawerFooter>
      </DrawerView>
    </Container>
  );
};

export default BoardDrawer;
