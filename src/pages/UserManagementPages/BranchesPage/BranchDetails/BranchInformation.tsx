import React, { useState, useEffect } from "react";
import {
  H5Typography,
  H4SemiBold
} from "@components/Typography/Typography";
import { SubTitleWrapper } from "./style";
import { IconButton, EditIconButton } from "@components/Buttons";
import {
  ButttonWrapper,
  FormSectionWrapper,
  FormDevide,
  FormContainer,
  BiographyDiv
} from "@pages/UserManagementPages/StaffMembers/StaffDetails/style";
import { navigate } from "@reach/router";
import { Col, message, Row } from "antd";
import { BackArrow, DemoImage, EditIcon } from "@assets/index";
import FormDataView from "@components/ViewForm/FormDataField";
import { branchDetails } from "./helper";
import Loader from "@components/Loader";
import { BranchDetailsInterface } from "./type";
import { convertMobileNo, updateRawData } from "@utils/helpers";
import MapContainer from "@components/MapContainer/MapContainer";
import { SuitDetailsView } from "./SuitDetailsView";
import ProfileList from "@components/ProfileList";
interface BranchInformationProps {
  id?: any;
}

const BranchInformation: React.FC<BranchInformationProps> = ({ id }) => {
  const [loading, setLoading] = useState<boolean>(false);
  const [branchData, setBranchData] = useState<
    BranchDetailsInterface | undefined
  >();

  const getBranchDetails = async () => {
    setLoading(true);
    const res: any = await branchDetails(id);
    if (res.status === 200) {
      setLoading(false);
      setBranchData(res.data.data);
      updateRawData({ selectedBranch: res.data.data });
    } else {
      setLoading(false);
      message.error(res.error.message);
    }
  };
  useEffect(() => {
    getBranchDetails();
  }, []);
  return (
    <>
      <ButttonWrapper>
        <IconButton
          text={"Back to all branches"}
          testId="BackToRAH"
          onClick={() => {
            navigate("/usermanagement/branch-list");
          }}
          icon={BackArrow}
        />

        <EditIconButton
          text={"Edit"}
          icon={EditIcon}
          testId="EditProfile"
          onClick={() => {
            navigate(`/usermanagement/edit-branch/${id}`);
          }}
        />
      </ButttonWrapper>

      <Loader loading={loading} />
      {branchData && (
        <>
          <FormSectionWrapper>
            <H4SemiBold text="Branch details" />
          </FormSectionWrapper>
          <SubTitleWrapper>
            <H5Typography>Information about branch</H5Typography>
          </SubTitleWrapper>
          <FormDevide firstChild={true}>
            <Row>
              <Col className="gutter-row" span={8}>
                <FormDataView
                  heading={"Branch name"}
                  value={branchData.branch_name}
                />
              </Col>
              <Col className="gutter-row" span={8}>
                {branchData.branch_manager_users[0] && (
                  <>
                    <ProfileList
                      imgs={branchData.branch_manager_users.map(
                        i => i.profile_detail.profile_photo
                      )}
                      max={3}
                    />

                    {/* <FormDataView
                    image={
                      branchData.branch_manager_users[0].profile_detail
                        .profile_photo
                        ? branchData.branch_manager_users[0].profile_detail
                            .profile_photo
                        : DemoImage
                    }
                    heading={"Manager"}
                    value={
                      branchData.branch_manager_users[0].profile_detail
                        .full_name
                    }
                  /> */}
                  </>
                )}
              </Col>
              <Col className="gutter-row" span={8}>
                {branchData.branch_admin_users[0] && (
                  <FormDataView
                    image={
                      branchData.branch_admin_users[0].profile_detail
                        .profile_photo
                        ? branchData.branch_admin_users[0].profile_detail
                            .profile_photo
                        : DemoImage
                    }
                    heading={"Admin"}
                    value={
                      branchData.branch_admin_users[0].profile_detail.full_name
                    }
                  />
                )}
              </Col>
            </Row>
          </FormDevide>

          {/* contact details */}
          <SubTitleWrapper>
            <H5Typography>Contact details</H5Typography>
          </SubTitleWrapper>

          <FormDevide firstChild={true}>
            <Row>
              <Col className="gutter-row" span={8}>
                <FormDataView
                  heading={"Phone number"}
                  value={`${
                    branchData.phone ? convertMobileNo(branchData.phone) : "-"
                  }`}
                />
              </Col>
              <Col className="gutter-row" span={8}>
                <FormDataView
                  heading={"Fax number"}
                  value={`${
                    branchData.fax ? convertMobileNo(branchData.fax) : "-"
                  }`}
                />
              </Col>
              <Col className="gutter-row" span={8}>
                <FormDataView
                  heading={"Email"}
                  value={`${branchData.email ? branchData.email : "-"}`}
                />
              </Col>
            </Row>
          </FormDevide>

          {/* address details */}

          <SubTitleWrapper>
            <H5Typography>Address</H5Typography>
          </SubTitleWrapper>
          <FormDevide firstChild={true}>
            <Row>
              <Col className="gutter-row" span={8}>
                <FormDataView
                  heading={"Address #1"}
                  value={branchData.address1}
                />
              </Col>
              <Col className="gutter-row" span={8}>
                <FormDataView
                  heading={"Address #2"}
                  value={branchData.address2}
                />
              </Col>
              <Col className="gutter-row" span={8}>
                <FormDataView heading={"Unit"} value={branchData.unit} />
              </Col>
            </Row>
            <Row>
              <Col className="gutter-row" span={8}>
                <FormDataView
                  heading={"Province"}
                  value={branchData.province ? branchData.province : "-"}
                />
              </Col>
              <Col className="gutter-row" span={8}>
                <FormDataView
                  heading={"City"}
                  value={branchData.city ? branchData.city : "-"}
                />
              </Col>
              <Col className="gutter-row" span={8}>
                <FormDataView
                  heading={"Postal code"}
                  value={branchData.postal_code ? branchData.postal_code : "-"}
                />
              </Col>
            </Row>
            <Row>
              <MapContainer />
            </Row>
          </FormDevide>

          <SubTitleWrapper>
            <H5Typography>Notes</H5Typography>
          </SubTitleWrapper>
          <FormContainer>
            <FormDevide firstChild={false}>
              <BiographyDiv>{`${
                branchData.notes ? branchData.notes : "-"
              }`}</BiographyDiv>
            </FormDevide>
          </FormContainer>

          {/* suit Details */}

          <FormSectionWrapper>
            <H4SemiBold text="Suites" />
          </FormSectionWrapper>
          {branchData.branch_suite && (
            <SuitDetailsView managers={[]} suiteValue={branchData.branch_suite} />
          )}
        </>
      )}
    </>
  );
};

export default BranchInformation;
