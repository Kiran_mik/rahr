import React, { useState, useEffect } from "react";

import { navigate } from "@reach/router";
import { message, Table } from "antd";
import { branchMembers, columns, rowSelection } from "./helper";
import BranchMembersHeader from "./BranchMembersHeader";

import { TableWrap } from "./style";
import DataTable from "@components/DataTable/DataTable";

import { filterIt } from "@utils/helpers";
import { TableWrapper } from "@pages/UserManagementPages/RAHStaffListPage/style";
interface BranchMembersProps {
  id?: any;
}

const BranchMembers: React.FC<BranchMembersProps> = ({ id }) => {
  const [loading, setLoading] = useState<boolean>(false);

  const [dataSource, setDataSource] = useState([]);
  const [backupDataSource, setBackupDataSource] = useState([]);
  const [selectionType, setSelectionType] = useState<"checkbox">("checkbox");

  useEffect(() => {
    getBranchMembers();
  }, []);

  const getBranchMembers = async () => {
    setLoading(true);
    const res: any = await branchMembers(id);
    if (res.status === 200) {
      setLoading(false);
      const mappedData = res.data.data.data.map((i: any) => ({
        key: JSON.stringify(i),
        ...i,
      }));
      setBackupDataSource(mappedData);
      setDataSource(mappedData);
    } else {
      message.error(res.error.message);
    }
  };

  return (
    <>
      <BranchMembersHeader
        onEdit={() => {
          navigate(`/usermanagement/edit-branch/${id}`);
        }}
        onSearch={(e: any) => {
          let filterList: any = filterIt(
            backupDataSource || [],
            e.target.value
          );

          setDataSource(filterList);
        }}
      />
      <>
        <TableWrapper style={{ marginTop: "20px", width: "99%" }}>
          <DataTable
            loading={loading}
            rowSelection={{
              type: selectionType,
              ...rowSelection,
            }}
            columns={columns}
            dataSource={dataSource}
            pagination={false}
            infinity={true}
          />
        </TableWrapper>
      </>
    </>
  );
};

export default BranchMembers;
