import React from "react";
import { navigate } from "@reach/router";

import { SearchBar } from "@components/SerachBar";
import { IconButton, EditIconButton } from "@components/Buttons";
import { BackArrow, DemoImage, EditIcon, ThreeDots } from "@assets/index";
import { ButttonWrapper  } from "@pages/UserManagementPages/StaffMembers/StaffDetails/style";

function BranchMembersHeader(props:{onSearch:(e:React.ChangeEvent)=>void,onEdit:()=>void}) {
  const {onSearch,onEdit}=props;
    return (
    <ButttonWrapper>
      <IconButton
        text={"Back to all branches"}
        testId="BackToRAH"
        onClick={() => {
          navigate("/usermanagement/branch-list");
        }}
        icon={BackArrow}
      />
      <SearchBar
        className="ViewBranchDetailSearch"
        placeholder="Search by Username"
        onChange={onSearch}
      />

      <EditIconButton
        text={"Edit"}
        icon={EditIcon}
        testId="EditProfile"
        onClick={onEdit}
      />
    </ButttonWrapper>
  );
}

export default BranchMembersHeader;
