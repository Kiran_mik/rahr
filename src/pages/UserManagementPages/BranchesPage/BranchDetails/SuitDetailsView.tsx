import React from "react";
import { H5Typography } from "@components/Typography/Typography";
import FormDataView from "@components/ViewForm/FormDataField";
import { FormDevide } from "@pages/UserManagementPages/StaffMembers/StaffDetails/style";
import { Col, Row } from "antd";
import { SubTitleWrapper } from "./style";
import { DemoImage } from "@assets/index";
import { BranchManagerUser } from "./type";

interface SuitDetailsViewProps {
  suiteValue?: any;
  managers?: BranchManagerUser[];
}
export const SuitDetailsView: React.FC<SuitDetailsViewProps> = ({
  suiteValue,
  managers
}) => {
  console.log(suiteValue, "suiteValue");
  return (
    <>
      {suiteValue.map((suite: any) => {
        const managerName = (suite.branch_suite_manager_detail 
          && suite.branch_suite_manager_detail.profile_detail 
          && suite.branch_suite_manager_detail.profile_detail.full_name) 
          || "";
          const managerPicture = (suite.branch_suite_manager_detail 
            && suite.branch_suite_manager_detail.profile_detail 
            && suite.branch_suite_manager_detail.profile_detail.profile_photo) 
            || ""
        return (
          <>
            <SubTitleWrapper>
              <H5Typography>Suite {suite.suite}</H5Typography>
            </SubTitleWrapper>
            <FormDevide firstChild={true}>
              <Row>
                <Col className="gutter-row" span={8}>
                  <FormDataView heading="Suite" value={suite.suite} />
                </Col>
                <Col className="gutter-row" span={8}>
                  <FormDataView
                    image={managerPicture || DemoImage}
                    heading="Manager"
                    value={managerName}
                  />
                </Col>
                <Col className="gutter-row" span={8}>
                  <FormDataView
                    heading="RECO #"
                    value={suite.reco ? suite.reco : "-"}
                  />
                </Col>
              </Row>
              <Row>
                <Col className="gutter-row" span={8}>
                  <FormDataView
                    heading="Broker code"
                    value={
                      suite.broker_code ? suite.broker_code : "-"
                    }
                  />
                </Col>
                <Col className="gutter-row" span={8}>
                  <FormDataView
                    heading="Registered boards"
                    value={(suite.branch_suite_boards && suite.branch_suite_boards.length)
                      ? suite.branch_suite_boards.map((board:any)=>board.board_id).join(", ")
                      :"-"}
                  />
                </Col>
              </Row>
            </FormDevide>
          </>
        )
      }
      )}
    </>
  );
};
