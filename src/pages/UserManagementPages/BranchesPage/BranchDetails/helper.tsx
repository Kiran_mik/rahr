import React, { useState, useEffect } from "react";
import { DropdownStyle } from "@pages/UserManagementPages/RAHStaffListPage/style";
import { Menu } from "antd";
import { Link, navigate } from "@reach/router";
import { BackArrow, DemoImage, EditIcon, ThreeDots } from "@assets/index";
import { Dropdown, message } from "antd";

import {
  GetBranchDetails,
  GetBranchMembers,
} from "@services/UserManagement/BranchService";

export const branchDetails = async (branchId: any) => {
  const res: any = await GetBranchDetails(branchId);
  return res;
};
export interface DataType {
  key: React.Key;
  branchName?: string;
  branchManagerUsers?: string;
  branchAdminUsers?: string;
  staffMembersCount?: number;
  activeAgentCount?: number;
}
export const branchMembers = async (branchId: any) => {
  const res: any = await GetBranchMembers(branchId);
  return res;
};

export const menu = (userId: any) => {
  return (
    <Menu>
      <Menu.Item key="0">
        <Link
          to={`/usermanagement/edit-staff/${userId}`}
          style={{ color: "#17082D" }}
        >
          Update profile
        </Link>
      </Menu.Item>
      <Menu.Item key="1">Terminate user</Menu.Item>
    </Menu>
  );
};
export const columns = [
  {
    title: "User name",
    dataIndex: "first_name",
    key: "first_name",
    width: "40%",

    sorter: (a: any, b: any) => a.first_name.localeCompare(b.first_name),

    render: (first_name: string, userObject: any) => {
      return (
        <div style={{ fontWeight: 500, display: "flex", alignItems: "center" }}>
          {" "}
          <img
            alt="ProfileImage"
            style={{
              width: "24px",
              height: "24px",
              marginRight: "10px",
              borderRadius: "50%",
            }}
            src={
              userObject.profile_photo ? userObject.profile_photo : DemoImage
            }
          />
          <Link
            to={`/usermanagement/staff-detail/${userObject.id}`}
            style={{ color: "#4E1C95" }}
          >
            {userObject.first_name} {userObject.last_name}
          </Link>
          <div
            style={{
              width: "18px",
              height: "18px",
              marginLeft: "8px",
              backgroundColor: "#F58A07",
              borderRadius: "50%",
              color: "#FFF",
              textAlign: "center",
            }}
          >
            !
          </div>
        </div>
      );
    },
  },
  {
    title: "Position",
    dataIndex: "role_name",
    key: "role_name",
    width: "20%",
    sorter: (a: any, b: any) => a.role_name.localeCompare(b.role_name),
  },
  {
    title: "Department",
    dataIndex: "department_name",
    key: "department_name",

    width: "30%",
    sorter: (a: any, b: any) =>
      a.department_name.localeCompare(b.department_name),
    render: (department: any) => {
      return <div>{department}</div>;
    },
  },
  {
    title: "",
    dataIndex: "id",
    key: "id",
    width: "10%",

    render: (id: number, row: { id: number; index: number }) => {
      return (
        <DropdownStyle
          key={row.id + "" + row.index}
          style={{ cursor: "pointer" }}
        >
          <Dropdown overlay={menu(row.id)} trigger={["click"]}>
            <img
              src={ThreeDots}
              alt={ThreeDots}
              onClick={(e) => e.preventDefault()}
            />
          </Dropdown>
        </DropdownStyle>
      );
    },
  },
];
// rowselection object indicates the need for row selection
export const rowSelection = {
  onChange: (selectedRowKeys: any, selectedRows: any) => {},
  getCheckboxProps: (record: any) =>  {
     return ({
      disabled: record.name === "Disabled Users",
      name: record.id,
    })

  }
};
