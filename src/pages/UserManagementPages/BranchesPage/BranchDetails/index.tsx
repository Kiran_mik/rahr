import React, { FC, useEffect } from "react";
import { Link } from "@reach/router";
import { useSelector } from "react-redux";

import DashboardLayout from "@pages/UserManagementPages/StaffMembers/DashboardLayout";

import StaffHeader from "@components/StaffHeader";
import { H3 } from "@components/Typography/Typography";
import { Breadcrumb } from "antd";
import {
  BreadItem,
  UserBreadWrap,
} from "@pages/UserManagementPages/StaffMembers/StaffDetails/style";

import HorizontalTab from "@components/HorizontalTabs";
import { updateRawData } from "@utils/helpers";

import BranchInformation from "./BranchInformation";
import BranchMembers from "./BranchMembers";

const BranchDetail: FC<any> = ({ branchId }) => {
  const { selectedBranch = {} } = useSelector(
    (state: { rawData: { selectedBranch: any } }) => state.rawData
  );

  useEffect(() => {
    return () => {
      updateRawData({ selectedBranch: null });
    };
  }, []);

  const TabValues = [
    {
      tab: "Branch information",
      content: <BranchInformation id={branchId} />,
      key: 1,
    },
    { tab: "Branch members", content: <BranchMembers id={branchId} />, key: 2 },
  ];
  return (
    <>
      <DashboardLayout>
        <StaffHeader>
          <UserBreadWrap>
            <H3 text="Branch details" />
            <Breadcrumb separator=">">
              <Link to="/usermanagement/branch-list" data-testid="gouserList">
                <BreadItem data-testid="breadcrumb-user" firstNav={true}>
                  List of branches
                </BreadItem>
              </Link>
              <BreadItem firstNav={false}>
                {selectedBranch && selectedBranch.branch_name}
              </BreadItem>
            </Breadcrumb>
          </UserBreadWrap>
        </StaffHeader>

        <HorizontalTab value={TabValues} />
      </DashboardLayout>
    </>
  );
};

export default BranchDetail;
