
import styled from "styled-components";
import TableComponent from "@components/Table";
import { TableWrapper } from "@components/Table/style";
export const TabContainer = styled.div`

`;

export const TabContentWrapper = styled.div`
padding:32px;
background:#fff;
border-bottom-left-radius: 15px;
    border-bottom-right-radius: 15px;
    margin-bottom:20px;
`;

export const SubTitleWrapper = styled.div`
    padding-bottom: 16px;
    padding-top: 2px;
`;
 


export const TableWrap = styled(TableWrapper)`
.ant-table-content{
  border:none;
}
.ant-table-tbody > tr > td:nth-child(2) span{
  color:rgba(78,28,149,1);
}

.ant-table-thead th:nth-child(3){
min-width:110px ;
}
.ant-table{
  overflow:auto;
 max-height:400px;
   &::-webkit-scrollbar {
  width: 4px;
  height:4px;
  margin-top:20px
}
&::-webkit-scrollbar-track {
  box-shadow: inset 0 0 5px rgba(226, 226, 238, 1);
  border-radius: 10px;
}


&::-webkit-scrollbar-thumb {
  background: rgba(151, 146, 227, 1);
  border-radius: 10px;
}


&::-webkit-scrollbar-thumb:hover {
  background: rgba(151, 146, 227, 1);
}
}
.ant-table-body{
  
}
`;