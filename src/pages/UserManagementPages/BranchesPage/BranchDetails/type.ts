

    export interface Pivot {
        branch_id: number;
        user_id: number;
    }

    export interface ProfileDetail {
        id: number;
        full_name: string;
        profile_photo: string;
    }

    export interface BranchManagerUser {
        branch_id: number;
        user_id: number;
        id: number;
        email: string;
        profile_detail_id: number;
        pivot: Pivot;
        profile_detail: ProfileDetail;
    }

    export interface Pivot2 {
        branch_id: number;
        user_id: number;
    }

    export interface ProfileDetail2 {
        id: number;
        full_name: string;
        profile_photo: string;
    }

    export interface BranchAdminUser {
        branch_id: number;
        user_id: number;
        id: number;
        email: string;
        profile_detail_id: number;
        pivot: Pivot2;
        profile_detail: ProfileDetail2;
    }

    export interface BranchDetailsInterface {
        id: number;
        created_by: number;
        branch_name: string;
        email: string;
        RECO: string;
        broker_code: string;
        phone: string;
        fax: string;
        registerd_boards?: any;
        unit?: any;
        address1: string;
        address2?: any;
        province?: any;
        city?: any;
        postal_code?: any;
        latitude?: any;
        longitude?: any;
        notes?: any;
        branch_suite: string;
        created_at: Date;
        branch_manager_users: BranchManagerUser[];
        branch_admin_users: BranchAdminUser[];
    }



