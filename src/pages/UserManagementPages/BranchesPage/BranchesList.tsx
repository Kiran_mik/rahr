import React, { useState, useEffect } from "react";
import { Secondary } from "@components/Buttons";
import { SearchBar } from "@components/SerachBar";
import StaffHeader from "@components/StaffHeader";
import { H3 } from "@components/Typography/Typography";
import {
  InputAndButton,
  NavigateButton,
  TableWrapper
} from "../RAHStaffListPage/style";
import DashboardLayout from "../StaffMembers/DashboardLayout";
import { getColumns } from "./columns";
import { navigate } from "@reach/router";
import {
  BranchItem,
  getBranchListArray,
} from "./helper";
import DataTable from "@components/DataTable/DataTable";
interface DataType {
  key: React.Key;
  userObject?: any;
  branch_name?: string;
  branch_manager_users?: any;
  branch_admin_users?: any;
  staff_members_count?: number;
  agent_count?: number;
  id?: number;
  profile_photo: string;
}

const rowSelection = {
  onChange: (selectedRowKeys: React.Key[], selectedRows: DataType[]) => { },
  getCheckboxProps: (record: DataType) => ({
    disabled: record.branch_name === "Disabled Users",
    name: record.branch_name
  })
};

const BranchesList = (props: { path: string }) => {
  const [selectionType, setSelectionType] = useState < "checkbox" > ("checkbox");
  const [page, setPage] = useState < number > (1);
  const [dataSource, setDataSource] = useState < Array < BranchItem >> ([]);
  const [loading, setLoading] = useState(false);
  const [filter, setFilter] = useState("");
  const [currentSortOrder, setCurrentSortOrder] = useState({
    key: "",
    order: "asc"
  });

  useEffect(() => {
    getData(page, filter, currentSortOrder.key, currentSortOrder.order);
  }, [filter]);

  const onChange = (e: { target: { value: any } }) => {
    const currValue = e.target.value;
    setFilter(currValue);
  };

  const redirect = () => {
    navigate("/usermanagement/new-branch");
  };

  const getData = async (
    page: number,
    values?: string,
    sortKey?: string,
    sortOrder?: string,
    hideLoader = false
  ) => {
    setLoading(!hideLoader && true);
    //@ts-ignore
    const res = (await getBranchListArray(
      page,
      values,
      sortKey || "branch_name",
      sortOrder || "asc"
    )) as {
      list: Array<BranchItem>;
      lastPage: number;
    };
    setDataSource([]);
    setLoading(false);
    setDataSource(res.list && res.list.length ? res.list : dataSource);
  };

  const handlePageChange = async () => {
    //@ts-ignore
    const res = (await getBranchListArray(
      page + 1,
      filter,
      currentSortOrder.key,
      currentSortOrder.order
    )) as {
      list: Array<BranchItem>;
      lastPage: number;
    };
    setDataSource([...dataSource, ...res.list]);
    setPage(page => page + 1);
  };

  const onSort = async (key: string) => {
    const sortOrder =
      currentSortOrder.key === key || !currentSortOrder.key
        ? currentSortOrder.order === "asc"
          ? "desc"
          : "asc"
        : "asc";

    setLoading(true);
    //@ts-ignore
    const res = (await getBranchListArray(1, filter, key, sortOrder)) as {
      list: Array<BranchItem>;
      lastPage: number;
    };
    setDataSource([]);
    setPage(1);
    setLoading(false);
    setDataSource(res.list.length ? res.list : dataSource);
    setCurrentSortOrder({ key, order: sortOrder });
  };

  return (
    <>
      <DashboardLayout>
        <StaffHeader>
          <H3 text="Branches" />
        </StaffHeader>
        <div>
          <InputAndButton>
            <SearchBar
              className="SearchInput"
              placeholder="Search by branch name, manager or location"
              onChange={onChange}
            />
            <NavigateButton onClick={redirect}>
              <Secondary text="+ Add new branch" />
            </NavigateButton>
          </InputAndButton>
          <TableWrapper>
            {/* {dataSource && ( */}
            <DataTable
              loading={loading}
              rowSelection={{
                type: selectionType,
                ...rowSelection
              }}
              columns={getColumns(onSort)}
              //@ts-ignore
              dataSource={dataSource || []}
              pagination={false}
              infinity={true}
              //@ts-ignore
              onFetch={() => handlePageChange()}
            />
            {/* )} */}
          </TableWrapper>
        </div>
      </DashboardLayout>
    </>
  );
};
export default BranchesList;
