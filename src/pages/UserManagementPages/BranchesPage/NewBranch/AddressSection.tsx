import React from "react";
import { Select } from "antd";
import { FormLableTypography } from "@components/Typography/Typography";
import { FormInputFiled } from "@components/Input/Input";
import CustomError from "@components/CustomError";

import { FormSubTitle, FlexBox, Main } from "./style";
import { GoogleAutoComplete } from "@components/GoogleAutoComplete";
import { getAddressComps, getLocationByPlaceId } from "@utils/googlemaphealper";
const { Option }: any = Select;

interface AddressSectionProps {
  setFieldValue: (field: string, value: string) => void;
  errors: any;
  values: any;

  showerror: boolean;
  formerror: string;
}

function AddressSection(props: AddressSectionProps) {
  const { setFieldValue, errors, showerror, formerror, values } = props;

  const onPlaceSelect = async (placeId: string) => {
    const place:any = await getLocationByPlaceId(placeId);
    if (place) {
      const locObj = getAddressComps(place);
      if (locObj.city.length > 0) {
        // const coordinates = ({ lat: place.geometry.location.lat, lng: place.geometry.location.lng });
        // console.log("coordinates", coordinates);
        console.log("address", locObj);
        setFieldValue("address1", locObj.formattedAddress || '')
        setFieldValue("address2", locObj.addressLine_2 || '')
        setFieldValue("province", locObj.state || '')
        setFieldValue("city", locObj.city || '')
        setFieldValue("postal_code", locObj.postalCode || '')
        // setCoord([coordinates])
        // setCenter({ lat: place.geometry.location.lat, lng: place.geometry.location.lng })
      }
    }
  };
  return (
    <>
      <FormSubTitle>Address</FormSubTitle>
      <Main direction="row" justifyContent="space-around">
        <FlexBox direction="column">
          <FormLableTypography>
            Address line 1 <span>*</span>
          </FormLableTypography>
          {/* <FormInputFiled
            id="address1"
            name="address1"
            onChange={(e: any) => {
              setFieldValue("address1", e.target.value);
            }}
            error={props.errors.address1}
            value={props.values.address1}
          /> */}
           <GoogleAutoComplete
            apiKey={process.env.REACT_APP_GOOGLEAPI_KEY}
            defaultVal={''}
            id="address1"
            name="address1"
            placeholder='Input your location'
            onPlaceSelect={(place: any) => {
              console.log("api resp", place);
              if (place) onPlaceSelect(place.place_id);
            }}
          />
          <CustomError
            showerror={showerror}
            error={formerror}
            fieldName={"address1"}
          />
        </FlexBox>
        <FlexBox direction="column">
          <FormLableTypography>
            Address line 2 <span>*</span>
          </FormLableTypography>
          <FormInputFiled
            id="address2"
            name="address2"
            onChange={(e: any) => {
              setFieldValue("address2", e.target.value);
            }}
            error={props.errors.address2}
            value={props.values.address2}
          />
          <CustomError
            showerror={showerror}
            error={formerror}
            fieldName={"address2"}
          />
        </FlexBox>
      </Main>
      <Main direction="row" justifyContent="space-around">
        <FlexBox direction="column">
          <FormLableTypography>
            City <span>*</span>
          </FormLableTypography>
          <FormInputFiled
            id="city"
            name="city"
            onChange={(e: any) => {
              setFieldValue("city", e.target.value);
            }}
            error={props.errors.city}
            value={props.values.city}
          />
          <CustomError
            showerror={showerror}
            error={formerror}
            fieldName={"city"}
          />
        </FlexBox>
        <FlexBox direction="column">
          <FormLableTypography>
            Province <span>*</span>
          </FormLableTypography>
          <FormInputFiled
            id="province"
            name="province"
            onChange={(e: any) => {
              setFieldValue("province", e.target.value);
            }}
            error={props.errors.province}
            value={props.values.province}
          />
          <CustomError
            showerror={showerror}
            error={formerror}
            fieldName={"province"}
          />
        </FlexBox>
      </Main>
      <Main direction="row" justifyContent="space-around">
        <FlexBox direction="column">
          <FormLableTypography>
            Postal Code <span>*</span>
          </FormLableTypography>
          <FormInputFiled
            id="postal_code"
            name="postal_code"
            placeholder=""
            onChange={(e: any) => {
              setFieldValue("postal_code", e.target.value);
            }}
            error={props.errors.postal_code}
            value={props.values.postal_code}
          />
        </FlexBox>
      </Main>
    </>
  );
}

export default AddressSection;
