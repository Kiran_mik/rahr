import { CloseOutlined } from "@ant-design/icons";
import { Cross, DemoImage, EditIcon } from "@assets/index";
import Loader from "@components/Loader";
import { TableWrap, TableWrapper } from "./style";
import { Link } from "@reach/router";
import React, { useState, useEffect } from "react";
import { getEmployeeViaBranch } from "./helper";
import { roleInitialValues } from "@pages/UserManagementPages/DepartmentPages/DepartmentDetails/helper";
import EditRolesDrawer from "@pages/UserManagementPages/StaffMembers/EditStaff/EditRolesDrawer";
import { GetStaffPermission } from "@services/UserService";
const getCols = (onEdit: (roleId: string) => void) => {
  return [
    {
      title: "User name",
      dataIndex: "userObject",
      key: "userObject",

      sorter: (a: any, b: any) =>
        a.userObject.full_name.localeCompare(b.userObject.full_name),
      render: (userObject: any) => {
        return (
          <div
            style={{ fontWeight: 500, display: "flex", alignItems: "center" }}
          >
            {" "}
            <img
              alt="ProfileImage"
              style={{
                width: "24px",
                height: "24px",
                marginRight: "10px",
                borderRadius: "50%"
              }}
              src={
                userObject.profile_photo ? userObject.profile_photo : DemoImage
              }
            />
            <Link to={`/usermanagement/staff-detail/${userObject.id}`}>
              {userObject.full_name}
            </Link>
            <div
              style={{
                width: "18px",
                height: "18px",
                marginLeft: "8px",
                backgroundColor: "#F58A07",
                borderRadius: "50%",
                color: "#FFF",
                textAlign: "center"
              }}
            >
              !
            </div>
          </div>
        );
      }
    },
    {
      title: "Role",
      dataIndex: "role",
      key: "Role",
      sorter: (a: any, b: any) =>
        a.userObject.role.localeCompare(b.userObject.role)
    },
    {
      title: "",
      dataIndex: "Action",
      key: "Action",
      render: (userObject: any) => {
        return (
          <div>
            <img
              onClick={() => onEdit(userObject)}
              alt="EditIcon"
              style={{
                width: "15px",
                height: "15px",
                marginRight: "10px",
                cursor: "pointer"
              }}
              src={EditIcon}
            />
            <CloseOutlined />
          </div>
        );
      }
    }
  ];
};
const tableData = [
  {
    userObject: { full_name: "pavan", profile_photo: DemoImage },
    role: "branch manager"
  }
];

interface UserPermissionProps {
  tableValues: any;
  loading: boolean;
  onRoleUpdate?: (showLoader: boolean) => void;
}
const BranchEmployeeTable: React.FC<UserPermissionProps> = ({
  tableValues,
  loading,
  onRoleUpdate
}) => {
  const [editRoleObject, setEditRoleObject] = useState<any>(null);
  const columns = getCols(roleObject => {
    setEditRoleObject(roleObject);
  });

  return (
    <div style={{ paddingTop: "14px" }}>
      <TableWrapper>
        {!loading ? (
          <TableWrap
            tableData={tableValues}
            column={columns}
            tableSize="full"
          />
        ) : (
          <Loader loading={loading} />
        )}
      </TableWrapper>
      {editRoleObject && (
        <EditRolesDrawer
          onClose={() => setEditRoleObject(null)}
          visible={editRoleObject}
          roleId={editRoleObject.role_id}
          username={editRoleObject.role_name}
          tableValues={roleInitialValues.permissions}
          roleNames={editRoleObject.role_name}
          onRoleUpdate={() => {
            if (onRoleUpdate) {
              onRoleUpdate(false);
            }
          }}
        />
      )}
    </div>
  );
};
export default BranchEmployeeTable;
