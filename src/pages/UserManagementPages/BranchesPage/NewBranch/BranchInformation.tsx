import React, { useState, useEffect } from "react";
import { Form, Formik } from "formik";
import { message, Divider, Select } from "antd";
import { Link } from "@reach/router";
import { ConnectedFocusError } from "focus-formik-error";
import {
  ContainerBox,
  FlexBox,
  InnerContent,
} from "@pages/UserManagementPages/StaffMembers/NewStaff/style";
import { navigate } from "@reach/router";

import {
  formInitValues,
  AddBranchInfo,
  validationSchema,
  BranchMembersAdmins,
  getBranchInformation,
  transformBranchData,
  getAllBoards,
} from "./helper";
import Loader from "@components/Loader";
import InfoAboutBranchSection from "./InfoAboutBranchSection";
import ContactDetailsSection from "./ContactDetailsSection";
import AddressSection from "./AddressSection";
import SuitesSection from "./SuitesSection";
import BranchSuccessModal from "./BranchSuccessModal";
import { FormWrapper } from "./style";

const boards = new Array(26).fill("").map((i, index) => (index + 1) * 10);
export interface BranchInformationProps {
  branchId?: number;
}

const BranchInformation: React.FC<BranchInformationProps> = ({ branchId }) => {
  const [loading, setLoading] = useState<boolean>(false);
  const [managerList, setManagerList] = useState<any>([]);
  const [adminList, setAdminList] = useState<any>([]);
  const [formerror, setFormError] = useState<any>("");
  const [showerror, setShowError] = useState<boolean>(false);
  const [formValuesData, setFormValuesData] = useState<any>();
  const [modalvalue, setModalValue] = useState<any>(null);
  const [boardList, setBoardList] = useState([]);
  useEffect(() => {
    (async () => {
      getBranchMembersAdminsData();
      const boardsRes = await getAllBoards();
      setBoardList(boardsRes);

      if (branchId) {
        getBranchInformationData();
      }
    })();
  }, []);

  const onSubmitForm = async (value: any) => {
    const payload = { ...value, suites: value.suits, id: branchId };
    delete payload.suits;
    const res = await AddBranchInfo(payload, !!branchId);
    setShowError(false);
    if (res.status === 200) {
      message.success(res.message);
      if (!branchId) {
        setModalValue(res.data[0]);
      } else {
        navigate("/usermanagement/branch-list");
      }
    } else {
      message.error("Something went Wrong");
      setFormError(res.msg.error);
      setShowError(true);
    }
  };

  const getBranchInformationData = async () => {
    if (branchId) {
      const res: any = await getBranchInformation(branchId);
      if (res.status === 200) {
        const formData = transformBranchData(res.data.data);
        setFormValuesData(formData);
      } else {
        message.error(res.error.message);
      }
    }
  };

  const getBranchMembersAdminsData = async () => {
    setLoading(true);
    const res: any = await BranchMembersAdmins();
    if (res.status === 200) {
      setManagerList(res.data.data.managers);
      setAdminList(res.data.data.admins);
      setLoading(false);
    } else {
      setLoading(false);
      setModalValue(res.data.data.admins);

      message.error(res.error.message);
    }
  };

  const onCloseModal = () => {
    navigate(`/usermanagement/edit-branch/${modalvalue.id}`, {
      state: { moveToUser: true },
    });
    setModalValue(null);
  };
   return (
    <FormWrapper>
      <>
        {!loading ? (
          <Formik
            enableReinitialize={true}
            initialValues={formValuesData || formInitValues}
            onSubmit={(values, actions) => {
              onSubmitForm(values);
            }}
            validationSchema={validationSchema}
          >
            {(props) => {
              const { values, setFieldValue, errors } = props;
              const onSuiteDelete = (
                suit: any,
                arrayHelpers: any,
                index: number
              ) => {
                if (!suit.id) {
                  arrayHelpers.remove(index);
                  return;
                }
                const suits = values.suits;
                const newSutes = suits.map(
                  (suteItem: any, suteIndex: number) => {
                    if (suteItem.id === suit.id) {
                      suteItem.deleted = 1;
                    }
                    return suteItem;
                  }
                );
                setFieldValue("suits", newSutes);
              };

              return (
                <Form>
                  <ConnectedFocusError />
                  <ContainerBox>
                    <InnerContent>
                      <InfoAboutBranchSection
                        setFieldValue={setFieldValue}
                        //@ts-ignore
                        errors={props.errors}
                        values={props.values}
                        showerror={showerror}
                        formerror={formerror}
                        managerList={managerList}
                        adminList={adminList}
                      />
                      <ContactDetailsSection
                        setFieldValue={setFieldValue}
                        //@ts-ignore
                        errors={props.errors}
                        values={props.values}
                        showerror={showerror}
                        formerror={formerror}
                      />
                      <AddressSection
                        setFieldValue={setFieldValue}
                        //@ts-ignore
                        errors={props.errors}
                        values={props.values}
                        showerror={showerror}
                        formerror={formerror}
                      />

                      <Divider dashed className="customdivider" />
                      <SuitesSection
                        setFieldValue={setFieldValue}
                        //@ts-ignore
                        errors={props.errors}
                        values={props.values}
                        showerror={showerror}
                        formerror={formerror}
                        onSuiteDelete={onSuiteDelete}
                        managerList={managerList}
                        boards={boardList}
                      />
                    </InnerContent>
                  </ContainerBox>
                  <FlexBox className="footerbtn">
                    <button type="submit" className="submitbtn">
                      Submit and proceed
                    </button>
                    <Link
                      to={"/usermanagement/branch-list"}
                      className="cancelbtn"
                    >
                      Cancel
                    </Link>
                  </FlexBox>
                </Form>
              );
            }}
          </Formik>
        ) : (
          <Loader loading={loading} />
        )}
      </>
      {!!modalvalue && (
        <BranchSuccessModal
          modalvalue={modalvalue}
          onCloseModal={onCloseModal}
        />
      )}
    </FormWrapper>
  );
};
export default BranchInformation;
