import React from "react";
import ModalComponent from "@components/Modal/Modal";

import { DrawerPrimaryBtn } from "@components/Buttons";
import { Flex } from "@components/Flex";
import { H3 } from "@components/Typography/Typography";
import modalimg from "@assets/pana.jpg";

interface BranchSuccessModalProps {
  modalvalue: any;
  onCloseModal: () => void;
}

function BranchSuccessModal(props: BranchSuccessModalProps) {
  const { modalvalue, onCloseModal } = props;
  return (
    <>
      <ModalComponent
        visible={modalvalue}
        onOK={onCloseModal}
        onCancel={onCloseModal}
        //@ts-ignore
        modalimage={modalimg}
        title=""
        content=""
      >
        <div style={{ textAlign: "center", padding: "24px" }}>
          <img
            src={modalimg}
            alt="backgroundimg"
            style={{ marginBottom: "40px" }}
          />
          <H3
            text={`New branch “${modalvalue.branch_name}” has been created`}
            className="MainTitle"
          />
          <p
            className="modalpara"
            style={{
              fontFamily: "Poppins",
              color: "#50514F",
              fontSize: "14px",
            }}
          />
        </div>
        <Flex justifyContent="center">
          <DrawerPrimaryBtn text="Okay!" onClick={onCloseModal} />
        </Flex>
      </ModalComponent>
    </>
  );
}

export default BranchSuccessModal;
