import React from "react";
import { Select } from "antd";
import { FormLableTypography } from "@components/Typography/Typography";
import { FormInputFiled } from "@components/Input/Input";
import CustomError from "@components/CustomError";

import { FormSubTitle, FlexBox, Main } from "./style";
const { Option }: any = Select;

interface ContactDetailsSectionProps {
  setFieldValue: (field: string, value: string) => void;
  errors: any;
  values: any;

  showerror: boolean;
  formerror: string;
}

function ContactDetailsSection(props: ContactDetailsSectionProps) {
  const { setFieldValue, errors, showerror, formerror, values } = props;
  return (
    <>
      <FormSubTitle>Contact Details</FormSubTitle>
      <Main direction="row" justifyContent="space-around">
        <FlexBox direction="column">
          <FormLableTypography>
            Email <span>*</span>
          </FormLableTypography>
          <FormInputFiled
            id="email"
            name="email"
            placeholder="sample@mail.com"
            onChange={(e: any) => {
              if (e.target.value.toString().length < 35) {
                setFieldValue("email", e.target.value);
              }
              // setShowError(false);
            }}
            error={props.errors.email}
            value={props.values.email}
          />

          <CustomError
            showerror={showerror}
            error={formerror}
            fieldName={"email"}
          />
        </FlexBox>
        <FlexBox direction="column">
          <FormLableTypography>
            Phone <span>*</span>
          </FormLableTypography>

          <FormInputFiled
            id="phone"
            name="phone"
            placeholder="+1 ( _ _ _ ) _ _ _ _  - _ _ _ "
            onChange={(e: any) => {
              if (
                e.target.value.toString().length < 11 &&
                e.target.value.toString() != "0000000000"
              ) {
                setFieldValue("phone", e.target.value);
              }
            }}
            value={props.values.phone}
            error={props.errors.phone}
          />
          <CustomError
            showerror={showerror}
            error={formerror}
            fieldName={"phone"}
          />
        </FlexBox>
      </Main>
      <Main direction="row" justifyContent="space-around">
        <FlexBox direction="column">
          <FormLableTypography>
            Fax <span>*</span>
          </FormLableTypography>

          <FormInputFiled
            id="fax"
            name="fax"
            placeholder="+1 ( _ _ _ ) _ _ _ _  - _ _ _ "
            // addonBefore="http://"
            onChange={(e: any) => {
              if (e.target.value.toString().length < 11) {
                setFieldValue("fax", e.target.value);
              }
            }}
            error={props.errors.fax}
            value={props.values.fax}
          />
          <CustomError
            showerror={showerror}
            error={formerror}
            fieldName={"fax"}
          />
        </FlexBox>
      </Main>
    </>
  );
}

export default ContactDetailsSection;
