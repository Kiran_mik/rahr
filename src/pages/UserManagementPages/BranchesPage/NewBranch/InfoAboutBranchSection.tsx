import React from "react";
import { Select } from "antd";
import SelectInput from "@components/Select";
import { FormLableTypography } from "@components/Typography/Typography";
import { FormInputFiled } from "@components/Input/Input";
import CustomError from "@components/CustomError";
import TextAreaInput from "@components/Input/TextArea";

import { FormSubTitle, FlexBox, Main } from "./style";
const { Option }: any = Select;

interface InfoAboutBranchSectionProps {
  setFieldValue: (field: string, value: string) => void;
  errors: any;
  values: any;

  showerror: boolean;
  formerror: string;
  managerList: [];
  adminList: [];
}

function InfoAboutBranchSection(props: InfoAboutBranchSectionProps) {
  const {
    setFieldValue,
    managerList,
    errors,
    showerror,
    formerror,
    values,
    adminList,
  } = props;
  return (
    <>
      <FormSubTitle>Information about branch</FormSubTitle>
      <Main direction="row" justifyContent="space-around">
        <FlexBox direction="column">
          <FormLableTypography>
            Branch name <span>*</span>
          </FormLableTypography>
          <FormInputFiled
            id="branch_name"
            name="branch_name"
            placeholder="Start typing"
            onChange={(e: any) => {
              setFieldValue("branch_name", e.target.value);
            }}
            error={errors.branch_name}
            value={values.branch_name}
          />
          <CustomError
            showerror={showerror}
            error={formerror}
            fieldName={"branch_name"}
          />
        </FlexBox>
      </Main>
      <Main direction="row" justifyContent="space-around">
        <FlexBox direction="column">
          <FormLableTypography>
            Managers <span>*</span>
          </FormLableTypography>

          <SelectInput
            mode="multiple"
            id="manager_ids"
            allowClear
            placeholdertitle="Select from the list"
            onChange={(value: any) => {
              setFieldValue("manager_ids", value);
            }}
            error={props.errors.manager_ids}
            value={props.values.manager_ids}
          >
            {managerList &&
              managerList.map((manager: any) => (
                <Option key={manager.id} value={manager.id}>
                  {manager.username}
                </Option>
              ))}
          </SelectInput>
          <CustomError
            showerror={showerror}
            error={formerror}
            fieldName={"manager_ids"}
          />
        </FlexBox>
      </Main>
      <Main direction="row" justifyContent="space-around">
        <FlexBox direction="column">
          <FormLableTypography>
            Administrators <span>*</span>
          </FormLableTypography>
          <SelectInput
            id="admin_ids"
            mode="multiple"
            allowClear
            placeholdertitle="Select from the list"
            onChange={(value: any) => {
              setFieldValue("admin_ids", value);
            }}
            error={props.errors.admin_ids}
            value={props.values.admin_ids}
          >
            {adminList &&
              adminList.map((manager: any) => (
                <Option key={manager.id} value={manager.id}>
                  {manager.username}
                </Option>
              ))}
          </SelectInput>
          <CustomError
            showerror={showerror}
            error={formerror}
            fieldName={"admin_ids"}
          />
        </FlexBox>
      </Main>
      <Main direction="row" justifyContent="space-around">
        <FlexBox direction="column" style={{ marginBottom: "24px" }}>
          <FormLableTypography>
            Notes <span>*</span>
          </FormLableTypography>

          <TextAreaInput
            name="notes"
            id="notes"
            placeholder="Add notes about branch here"
            onChange={(e: any) => {
              setFieldValue("notes", e.target.value);
            }}
            error={props.errors.notes}
            value={props.values.notes}
          />
          <CustomError
            showerror={showerror}
            error={formerror}
            fieldName={"notes"}
          />
        </FlexBox>
      </Main>
    </>
  );
}

export default InfoAboutBranchSection;
