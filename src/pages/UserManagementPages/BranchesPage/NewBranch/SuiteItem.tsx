import React from "react";
import { Form, Formik, FieldArray, Field } from "formik";
import { Row, Col, Select } from "antd";
import { CloseOutlined } from "@ant-design/icons";

import SelectInput from "@components/Select";
import { FormLableTypography } from "@components/Typography/Typography";
import { FormInputFiled } from "@components/Input/Input";
import CustomError from "@components/CustomError";
import TextAreaInput from "@components/Input/TextArea";

import { FormSubTitle, FlexBox, Main } from "./style";
import { Link } from "@reach/router";
const { Option }: any = Select;

interface SuiteItemProps {
  index: number;
  onSuiteDelete: () => void;
  onChange: (field: string, value: string) => void;
  suit: any;
  boards: Array<any>;
  managerList: [];
}

function SuiteItem(props: SuiteItemProps) {
  const { onSuiteDelete, index, onChange, suit, boards, managerList } = props;
   return (
    <>
      <Row justify="space-between">
        <Col span={4}>
          {" "}
          <FormSubTitle>Suit {index + 1}</FormSubTitle>
        </Col>
        <Col
          span={6}
          style={{
            textAlign: "right",
          }}
        >
          <Link
            style={{
              color: "#4E1C95",
            }}
            onClick={onSuiteDelete}
            to=""
          >
            <CloseOutlined /> Remove office
          </Link>{" "}
        </Col>
      </Row>
      <Main direction="row" justifyContent="space-around">
        <FlexBox direction="column">
          <FormLableTypography>
            Suit<span>*</span>
          </FormLableTypography>
          <FormInputFiled
            name={`suits[${index}].suite`}
            id={`suits[${index}].suite`}
            onChange={(e: any) => {
              onChange(`suits[${index}].suite`, e.target.value);
            }}
            error={Error}
            value={suit.suite}
          />
        </FlexBox>
        <FlexBox direction="column">
          <FormLableTypography>
            Boards
            <span>*</span>
          </FormLableTypography>
          <SelectInput
            mode="multiple"
            name={`suits[${index}].boards`}
            id={`suits[${index}].boards`}
            placeholdertitle="Select from a list"
            onChange={(e: any) => {
            
              onChange(`suits[${index}].boards`, e);
            }}
            error={Error}
            value={suit.boards}
          >
            {boards.map((i) => {
              return (
                //@ts-ignore
                <Option key={i.id} value={i.id}>
                  {i.name}
                </Option>
              );
            })}
          </SelectInput>
        </FlexBox>
      </Main>
      <Main direction="row" justifyContent="space-around">
        <FlexBox direction="column">
          <FormLableTypography>
            Managers <span>*</span>
          </FormLableTypography>
          <SelectInput
            name={`suits[${index}].manager`}
            id={`suits[${index}].manager`}
            placeholdertitle="Select from a list"
            onChange={(e: any) => {
              onChange(`suits[${index}].manager`, e);
            }}
            error={Error}
            value={suit.manager}
          >
            {managerList.map((manager: any) => (
              <Option key={manager.id} value={manager.id}>
                {manager.username}
              </Option>
            ))}
          </SelectInput>
        </FlexBox>
      </Main>
      <Main direction="row" justifyContent="space-around">
        <FlexBox direction="column">
          <FormLableTypography>
            RECO # <span>*</span>
          </FormLableTypography>
          <FormInputFiled
            name={`suits[${index}].reco`}
            id={`suits[${index}].reco`}
            onChange={(e: any) => {
              onChange(`suits[${index}].reco`, e.target.value);
            }}
            error={Error}
            value={suit.reco}
          />
        </FlexBox>
        <FlexBox direction="column">
          <FormLableTypography>
            TREEB Broker code<span>*</span>
          </FormLableTypography>
          <FormInputFiled
            name={`suits[${index}].treeb_broker_code`}
            id={`suits[${index}].treeb_broker_code`}
            onChange={(e: any) => {
              onChange(`suits[${index}].treeb_broker_code`, e.target.value);
            }}
            error={Error}
            value={suit.treeb_broker_code}
          />
        </FlexBox>
      </Main>
    </>
  );
}

export default SuiteItem;
