import React from "react";
import { Form, Formik, FieldArray, Field } from "formik";
import { Row, Col, Select } from "antd";
import { CloseOutlined } from "@ant-design/icons";

import SelectInput from "@components/Select";
import { FormLableTypography } from "@components/Typography/Typography";
import { FormInputFiled } from "@components/Input/Input";
import CustomError from "@components/CustomError";
import TextAreaInput from "@components/Input/TextArea";

import { FormSubTitle, FlexBox, Main } from "./style";
import { Link } from "@reach/router";
import SuiteItem from "./SuiteItem";
const { Option }: any = Select;

interface SuitesSectionProps {
  setFieldValue: (field: string, value: string) => void;
  errors: any;
  values: any;

  showerror: boolean;
  formerror: string;
  boards: Array<any>;
  managerList: [];
  onSuiteDelete: (suite: any, arrayHelpers: any, index: number) => void;
}

function SuitesSection(props: SuitesSectionProps) {
  const {
    setFieldValue,
    errors,
    showerror,
    formerror,
    values,
    boards,
    onSuiteDelete,
    managerList,
  } = props;
  return (
    <>
      <FormSubTitle>Suites</FormSubTitle>
      <Row>
        <FieldArray
          name="suits"
          render={(arrayHelpers: any) => (
            <div style={{ width: "100%" }}>
              {values.suits &&
                values.suits.map((suit: any, index: number) => (
                  <>
                    {values.suits[index].deleted === 0 && (
                      <div key={index}>
                        <SuiteItem
                          index={index}
                          onSuiteDelete={() => {
                            onSuiteDelete(suit, arrayHelpers, index);
                          }}
                          onChange={(field, value) => {
                            setFieldValue(field, value);
                          }}
                          boards={boards}
                          managerList={managerList}
                          suit={suit}
                        />
                      </div>
                    )}
                  </>
                ))}
              <Link
                onClick={() =>
                  arrayHelpers.push({
                    suite: "",
                    boards: [],
                    manager: null,
                    reco: "",
                    treeb_broker_code: "",
                    deleted: 0,
                  })
                }
                to=""
                style={{ color: "#4E1C95" }}
              >
                + Add suite
              </Link>
            </div>
          )}
        />
      </Row>
    </>
  );
}

export default SuitesSection;
