import { DemoImage } from "@assets/index";
import { FlexBox } from "./style";
import Loader from "@components/Loader";
import SelectInput from "@components/Select";
import { FormLableTypography } from "@components/Typography/Typography";
import {
  FormSubTitle,
  FormTitle,
  FormWrapper
} from "@pages/UserManagementPages/StaffMembers/NewStaff/style";
import {  navigate } from "@reach/router";
import {  message, Row, Select } from "antd";
import { ConnectedFocusError } from "focus-formik-error";
import { Form, Formik } from "formik";
import React, { useState, useEffect } from "react";
import BranchEmployeeTable from "./BranchEmployeeTable";
import {
  addBranchEmployee,
  getEmployeeViaBranch,
  getSearchEmployee
} from "./helper";

export interface AddUsersProps {
  branchId?: any;
}
let timer: any = null;
const AddUsers: React.FC<AddUsersProps> = ({ branchId }: AddUsersProps) => {
  const [loading, setLoading] = useState<boolean>(false);
  const [tableLoading, setTableLoading] = useState<boolean>(false);
  const [employeeList, setEmployeeList] = useState<any>([]);
  const [employeedata, setEmployeeData] = useState<any>([]);

  const { Option }: any = Select;
  const getDataSearchEmployee = async (searchTerm: string) => {
    if (searchTerm.length) {
      const res: any = await getSearchEmployee(searchTerm);
      if (res.status === 200) {
        const employees = res.data.data;
        const filteredEmployees = employees.filter((i: any) => {
          return !employeedata.find((j: any) => j.id === i.id);
        });

        setEmployeeList(filteredEmployees);
      } else {
        // message.error(res.error.message);
      }
    }
  };
  const getEmployeeUsingBranchId = async (showLoader = true) => {
    if (branchId) {
      setTableLoading(showLoader && true);

      const res: any = await getEmployeeViaBranch(branchId);
      if (res.status === 200) {
        setEmployeeData(
          res.data.data.map(
            (row: {
              id: any;
              first_name: any;
              last_name: any;
              profile_photo: any;
              role_name: any;
            }) => ({
              id: row.id,
              userObject: {
                id: row.id,
                full_name: `${row.first_name} ${row.last_name}`,
                profile_photo: row.profile_photo
              },
              role: row.role_name,
              key: row.id,
              Action: { ...row }
            })
          )
        );
        setTableLoading(false);
      } else {
        setTableLoading(false);
      }
    }
  };
  const SubmitForm = async (value: any) => {
    const res = await addBranchEmployee(branchId, value.employee_ids);
    if (res.status === 200) {
      message.success(branchId ? "Branch Updated Sucessfully" : res.message);
      if (branchId) {
        navigate("/usermanagement/branch-list");
        return;
      }

      getEmployeeUsingBranchId();
    }
  };
  useEffect(() => {
    if (branchId) {
      getEmployeeUsingBranchId();
    }
  }, []);
  return (
    <FormWrapper>
      <FormTitle>Employees</FormTitle>
      <FormSubTitle>Add new employee</FormSubTitle>

      {!loading ? (
        <Formik
          initialValues={{ employee_ids: [] }}
          onSubmit={(values, actions) => {
            SubmitForm(values);
          }}
        >
          {props => {
            const { values, setFieldValue } = props;
            return (
              <Form>
                <Row justify="space-between">
                  <FlexBox direction="column" style={{ flex: "4 1 auto" }}>
                    <FormLableTypography>
                      Employee full name <span>*</span>
                    </FormLableTypography>

                    <SelectInput
                      mode="multiple"
                      id="employee_ids"
                      allowClear
                      onChange={(value: any) => {
                        setFieldValue("employee_ids", [
                          ...values.employee_ids,
                          parseInt(value)
                        ]);
                      }}
                      placeholdertitle="Select from the list"
                      onSearch={(value: string) => {
                        if (timer) {
                          clearTimeout(timer);
                        }
                        timer = setTimeout(() => {
                          getDataSearchEmployee(value);
                        }, 300);
                      }}
                    >
                      {employeeList.map((employee: any) => (
                        <>
                          <div
                            className="demo-option-label-item"
                            key={employee.id}
                          >
                            <span role="img" aria-label="China">
                              <img
                                alt="ProfileImage"
                                style={{
                                  width: "24px",
                                  height: "24px",
                                  marginRight: "10px",
                                  borderRadius: "50%"
                                }}
                                src={
                                  employee.profile_photo
                                    ? employee.profile_photo
                                    : DemoImage
                                }
                              />
                            </span>
                            {employee.first_name} {employee.last_name}
                          </div>
                        </>
                      ))}
                    </SelectInput>
                  </FlexBox>
                  <FlexBox style={{ alignSelf: "center", textAlign: "right" }}>
                    <button
                      type="submit"
                      className="submitbtn"
                      style={{ width: "193px" }}
                    >
                      Add to list
                    </button>
                  </FlexBox>
                </Row>
                <ConnectedFocusError />
                <BranchEmployeeTable
                  tableValues={employeedata}
                  loading={tableLoading}
                  onRoleUpdate={(showLoader: boolean) => {
                    getEmployeeUsingBranchId(showLoader);
                  }}
                />
              </Form>
            );
          }}
        </Formik>
      ) : (
        <Loader loading={loading} />
      )}
    </FormWrapper>
  );
};
export default AddUsers;
