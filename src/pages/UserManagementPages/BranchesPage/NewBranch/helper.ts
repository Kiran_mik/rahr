import services from "@services/index"
import { updateBranch } from "./../../../../services/UserManagement/BranchService"
import * as Yup from "yup"
import {
	addBranch,
	addEmployeeToBranch,
	getBranchInfo,
	GetBranchMembersAdmins,
	getEmployeeViaBranchId,
	searchEmployee,
} from "@services/UserManagement/BranchService"

export const getAllBoards = async () => {
	try {
		const res = await services.getTest('v1/boards')
		//@ts-ignore
		return res.data.data
	} catch (err) { return [] }
}
export const getRolesByDepId = async (depId: number) => {
	try {
		const res = await services.getTest(`v1/departments/role/${depId}`)
		//@ts-ignore
		return res.data.data
	} catch (err) {
		return []
	}
}
export const formInitValues = {
	branch_name: "",
	notes: "",
	email: "",
	phone: "",
	fax: "",
	address1: "",
	address2: "",
	city: "",
	province: "",
	postal_code: "",
	manager_ids: [],
	admin_ids: [],
	suits: [],
}

const phoneRegExp = /^[0-9]*\d$/

export const validationSchema = Yup.object({
	branch_name: Yup.string()
		.required("Please enter your Branch Name")
		.max(30, ""),
	notes: Yup.string().required("Please Add note"),
	email: Yup.string().email("Please enter valid email"),
	phone: Yup.string()
		.required("Phone number must be 10 digits.")
		.matches(phoneRegExp, "phone number is not valid")
		.min(10, "phone number length should be 10 digits")
		.max(10, "please enter vaild phone number"),
	fax: Yup.string()
		.required("Phone number must be 10 digits.")
		.matches(phoneRegExp, "fax is not valid")
		.min(10, "fax length should be 10 digits")
		.max(10, "please enter vaild fax"),
	address1: Yup.string().required("Please enter address1"),
	address2: Yup.string().required("Please enter address2"),
	city: Yup.string().required("Please enter city"),
	province: Yup.string().required("Please enter province"),
	postal_code: Yup.string().required("Please enter postal code"),
	manager_ids: Yup.array().required("Please select atleast one manager"),
	admin_ids: Yup.array().required("Please select atleast one Admin"),
	suits: Yup.array().of(
		Yup.object().shape({
			suite: Yup.string().required("Please enter Suit "),
			reco: Yup.string().required("Please select atleast one Reco "),
			manager: Yup.string().required("Please select atleast one Manager "),
			treeb_broker_code: Yup.string().required("Please enter TREEB Broker code "),
			boards: Yup.array().min(1,"Please select atleast one Boards "),

			// Rest of your amenities object properties
		})
	),
})

export const AddBranchInfo = async (data: any, isUpdate = false) => {
	const res: any = (await isUpdate) ? updateBranch(data) : addBranch(data)
	return res
}

export const BranchMembersAdmins = async () => {
	const res: any = await GetBranchMembersAdmins()
	return res
}
export const getSearchEmployee = async (searchTerm?: any) => {
	const res: any = await searchEmployee(searchTerm)
	return res
}
export const addBranchEmployee = async (branch_id?: number, employees?: [number]) => {
	const res: any = await addEmployeeToBranch(branch_id, employees)
	return res
}

export const getEmployeeViaBranch = async (branch_id: number) => {
	const res: any = await getEmployeeViaBranchId(branch_id)
	return res
}
export const getBranchInformation = async (branch_id: number) => {
	const res: any = await getBranchInfo(branch_id)
	return res
}


export const transformBranchData = (branchData: any) => {
	const branch = branchData || {};
	const formdata: any = {
		branch_name: branch.branch_name,
		notes: branch.notes,
		email: branch.email,
		phone: branch.phone,
		address1: branch.address1,
		address2: branch.address2,
		fax: branch.fax,
		city: branch.city,
		province: branch.province,
		postal_code: branch.postal_code,
		manager_ids: branch.branch_manager_users
			? branch.branch_manager_users.map((admin: any) => admin.id)
			: [],
		admin_ids: branch.branch_admin_users
			? branch.branch_admin_users.map((admin: any) => admin.id)
			: [],
		suits: branch.branch_suite
			? branch.branch_suite.map((i: any) => {
				i.deleted = 0;

				i.boards = i.branch_suite_boards ? i.branch_suite_boards.map((j: any) => parseInt(j.board_id))
					: []
				i.manager = i.branch_suite_manager_detail && i.branch_suite_manager_detail.id;
				i.treeb_broker_code = i.broker_code;

				delete i.broker_code;
				delete i.board_id;
				delete i.manager_id;
				delete i.branch_suite_boards
				return i;
			})
			: [],
	};
	return formdata
}