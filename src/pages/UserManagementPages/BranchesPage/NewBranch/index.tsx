import React, { FC, useState } from "react";
import { Layout, Descriptions } from "antd";
import { RouteComponentProps } from "@reach/router";
import DashboardLayout from "@pages/UserManagementPages/StaffMembers/DashboardLayout";
import StaffHeader from "@components/StaffHeader";
import { H3 } from "@components/Typography/Typography";
import VerticalTab from "@components/VerticalTabs";
import { TabDiv, TabTitle } from "./style";
import BranchInformation from "./BranchInformation";
import AddUsers from "./addUsers";
import { RoundedCircle, StatusIcon } from "@assets/index";

const getTabValues = (branchId: number) => {
  return [
    {
      tab: (
        <>
          <TabTitle>
            {!!branchId ? (
              <span
                style={{
                  height: 20,
                  width: 20,
                  backgroundColor: "#31AF91",
                  borderRadius: "50%",
                  display: "inline-block",
                  color: "white",
                  textAlign: "center",
                  fontWeight: "bold"
                }}
                className="tabicon"
              >
                &#10003;
              </span>
            ) : (
              <img src={RoundedCircle} alt="" className="tabicon" />
            )}
            <Descriptions title="Branch Information" layout="vertical" />
            <span className="SmallText">
              Small description of this step <br />
              and what to expect
            </span>
          </TabTitle>
        </>
      ),
      content: <BranchInformation branchId={branchId} />,
      key: 1,
      isDone: !!branchId
    },
    {
      tab: (
        <>
          {" "}
          <TabTitle>
            <img src={StatusIcon} alt="" className="tabicon" />
            <Descriptions title="Users" layout="vertical" />
            <span className="SmallText">
              Small description of this step <br />
              and what to expect
            </span>
          </TabTitle>
        </>
      ),
      content: <AddUsers branchId={branchId} />,
      key: 2,
      disabled: !branchId
    }
  ];
};

export interface SignupProps extends RouteComponentProps {
  title?: string;
  branchId?: any;
}
const AddbranchPage: FC<SignupProps> = props => {
  const { branchId, location } = props;
  const tabValues = getTabValues(parseInt(branchId));
  const activeKey =
    //@ts-ignore
    location && location.state && location.state.moveToUser ? "2" : "1";
  return (
    <>
      <DashboardLayout>
        <StaffHeader>
          <H3 text="New Branch" />
        </StaffHeader>
        <TabDiv>
          <VerticalTab
            className="tab-style-class"
            activeKey={activeKey}
            value={tabValues}
          />
        </TabDiv>
      </DashboardLayout>
    </>
  );
};

export default AddbranchPage;
