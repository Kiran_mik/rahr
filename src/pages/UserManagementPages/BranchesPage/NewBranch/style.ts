import styled from "styled-components";
import { Flex } from "@components/Flex";
import TableComponent from "@components/Table";
import { H5Typography } from "@components/Typography/Typography";


export const Main = styled(Flex)`
	   direction:"row" ;
	     margin: 0px -8px;
       `;

export const FormSubTitle = styled(H5Typography)`
  font-weight: 600;
  font-size: 16px;
  color: rgba(23, 8, 45, 1);
  margin-bottom: 16px;
`;

export const TabDiv = styled.div`
  .ant-tabs > div > .ant-tabs-nav .ant-tabs-nav-wrap {
    padding: 10 px;
  }
  .ant-tabs-card.ant-tabs-left > .ant-tabs-nav {
    width: 312px;
  }
  .ant-tabs > .ant-tabs-nav .ant-tabs-nav-wrap {
    padding: 10 px;
  }
  .ant-tabs-left
    > .ant-tabs-content-holder
    > .ant-tabs-content
    > .ant-tabs-tabpane {
    padding-left: 0px;
  }
  .ant-tabs-tab {
    border-radius: 10px !important;
    margin-right: 20px !important;
    margin-bottom: 10px !important;
    text-align: left !important;
    padding: 7px 7px 7px 42px !important;
  }
  .ant-tabs-tab-active {
    margin-right: 0px !important;
  }
  .ant-descriptions-title {
    font-size: 14px;
    color: #000;
    font-weight: 600;
    line-height: 22px;
    margin-bottom: 4px;
  }
  .ant-descriptions-header {
    margin-bottom: 0px;
  }
  .SmallText {
    color: #50514f;
    font-size: 12px;
    font-weight: 400;
  }
  .ant-tabs-nav .ant-tabs-tab-active {
    border: 1px solid #fff;
    border-top-right-radius: 0px !important;
    border-bottom-right-radius: 0px !important;
    border-left: 4px solid rgba(151, 146, 227, 1);
  }
`;

export const TabTitle = styled.div`
  .tabicon {
    position: absolute;
    top: 9px;
    left: 12px;
  }
`;

export const FormWrapper = styled.div`
    background: white;
    width: 100%;
    padding: 32px;
    border-radius: 0px;
    margin-bottom: 42px;

	.submitbtn{
		    padding: 12px 24px;
    background: #4E1C95;
    color: #fff;
    font-family: Poppins;
    font-size: 14px;
    line-height: 22px;
    border-radius: 8px;
    width: max-content;
	cursor:pointer;
	}
	.submitbtn:focus, .cancelbtn:focus{
		border-color: #40a9ff;
    border-right-width: 1px !important;
    outline: 0;
    box-shadow: 0 0 0 1px rgb(24 144 255 / 80%);

	}
	.cancelbtn{
		    background: transparent;
    font-size: 14px;
    align-items:center;
    display:flex;
    line-height: 22px;
    color: #4E1C95;
    font-family: Poppins;
    font-weight: 400;
    margin-left: 22px;
	cursor pointer;
      align-items: center;
}
.MainTitle{
	font-weight:600px;
}
.footerbtn{
	padding:0px;
	    margin-top: 0px;
    border-top: 1px solid #E2E2EE;
    padding-top: 24px;

}
textarea.ant-input{
	height:111px;
	border-radius:8px;
	border-color:#BDBDD3 !important;
}
.ant-input-textarea-show-count::after{
	float:left;
	color:rgba(123, 123, 151, 1);
	font-size:14px;
	line-height:22px;
	font-family:Poppins;
	font-weight:400;
}
	}
`;

export const TableWrap = styled(TableComponent)`
  .ant-table-content {
    border: none;
  }
`;
export const TableWrapper = styled.div`
  .ant-table-content {
    border: none;
  }
`;
export const FlexBox = styled(Flex)`
  flex: 1;
  padding: 0px 8px;
`;
