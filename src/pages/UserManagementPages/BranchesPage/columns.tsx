import {
  ThreeDots,
  UserImage,
  UserAvatar2,
  User3,
  DemoImage
} from "@assets/index";
import ProfileList from "@components/ProfileList";
import { Link } from "@reach/router";
import { Avatar, Dropdown, Menu } from "antd";
import React from "react";
import { DropdownStyle } from "../RAHStaffListPage/style";

export const menu = (branch_id: number) => (
  <Menu className="MenuItems" style={{ borderRadius: "8px" }}>
    <Menu.Item key="0">
      <Link
        style={{ color: "#17082d" }}
        to={`/usermanagement/edit-branch/${branch_id}`}
      >
        Update branch
      </Link>
    </Menu.Item>
    <Menu.Item key="1">Delete branch</Menu.Item>
  </Menu>
);

export const getColumns = (sorterFn: (key: string) => void) => {
  return [
    {
      title: "Branch Name",
      onHeaderCell: (column: any) => {
        return {
          onClick: () => {
            sorterFn("branch_name");
          }
        };
      },
      sorter: (a: any, b: any) => {},
      dataIndex: "userObject",
      key: "userObject",
      render: (row: {
        // userObject: any;
        id: number;
        index: number;
        branch_name: string;
      }) => {
        return (
          <>
            {/* {userObject && ( */}
            <div style={{ fontWeight: 500 }}>
              <Link
                style={{ color: "#4E1C95" }}
                to={`/usermanagement/branch-detail/${row.id}`}
              >
                {row.branch_name}
              </Link>
            </div>
            {/* )} */}
          </>
        );
      }
    },
    {
      title: "Managers",

      dataIndex: "branchManagerUsers",
      key: "branchManagerUsers",
      render: (branchManagerUsers: string[]) => {
        return (
          <>
            <ProfileList imgs={branchManagerUsers} max={3} />
          </>
        );
      }
    },
    {
      title: "Admins",

      dataIndex: "branchAdminUsers",
      key: "branchAdminUsers",
      render: (branchAdminUsers: string[]) => {
        return (
          <>
            <ProfileList imgs={branchAdminUsers} max={3} />
          </>
        );
      }
    },
    {
      title: "Staff Members",
      onHeaderCell: (column: any) => {
        return {
          onClick: () => {
            sorterFn("staffCount");
          }
        };
      },
      sorter: (a: any, b: any) => {},
      dataIndex: "staffMembersCount",
      key: "staffMembersCount"
    },
    {
      title: "Active Agents",
      onHeaderCell: (column: any) => {
        return {
          onClick: () => {
            sorterFn("agent_count");
          }
        };
      },
      sorter: (a: any, b: any) => {},
      dataIndex: "activeAgentCount",
      key: "activeAgentCount"
    },
    {
      title: "",
      dataIndex: "userObject",
      key: "userObject",
      render: (row: { id: number; index: number }) => {
        return (
          <DropdownStyle
            key={row.id + "" + row.index}
            style={{ cursor: "pointer" }}
          >
            <Dropdown overlay={menu(row.id)} trigger={["click"]}>
              <img
                src={ThreeDots}
                alt={ThreeDots}
                onClick={e => e.preventDefault()}
              />
            </Dropdown>
          </DropdownStyle>
        );
      }
    }
  ];
};
