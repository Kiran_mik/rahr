import { GetBranchList } from "@services/GetStaffList";

export interface BranchItem {
  key: React.Key;
  branch_name?: string;
  branch_manager_users?: any;
  branch_admin_users?: any;
  staff_members_count?: number;
  agent_count?: number;
  id?: number;
  profile_photo: string;
}

export const getBranchListArray = async(page: number, values: any, sortKey: string, sortOrder: string) => {
    const res: any = await GetBranchList(page, values, sortKey || 'branch_name', sortOrder || 'asc');
    if(res.status === 200) {
        const branchList = transformBranchListData( res.data.data.data || [] )
        return {list: branchList, lastPage: res.data.data.last_page};
    }
    return[];
}

export const transformBranchListData = (branchList: Array<BranchItem>) => {
    return branchList.map((row: BranchItem, index: number) => ({
        id: row.id,
        index: index,
        userObject: { branch_name: row.branch_name, id: row.id },
        branchManagerUsers: row.branch_manager_users
          ? row.branch_manager_users.map(
              (row: any) => row.profile_detail && row.profile_detail.profile_photo
            )
          : [],
        branchAdminUsers: row.branch_admin_users
          ? row.branch_admin_users.map(
              (row: any) => row.profile_detail && row.profile_detail.profile_photo
            )
          : [],
        staffMembersCount: row.staff_members_count,
        activeAgentCount: row.agent_count,
        key: row.id
    }))
}