import React, { FC, useState, useEffect } from "react";
import {
  FormWrapper,
  ContainerBox,
  InnerContent,
  FlexBox,
  RowContent,
  ButtonText,
  H4Title
} from "./style";
import { InputField } from "@components/Input/Input";
import { FormTitle, Main } from "../StaffMembers/NewStaff/style";
import { FormLableTypography, H3 } from "@components/Typography/Typography";
import { Formik } from "formik";
import { Divider, message, Select } from "antd";
import { Link, navigate } from "@reach/router";
import { Flex } from "@components/Flex";
import NewRolesDrawer from "./NewRoleDrawer";
import EditRolesDrawer from "../StaffMembers/EditStaff/EditRolesDrawer";
import {
  GetManagerList,
  GetStaffPermission,
  SendDepartments
} from "@services/UserService";
import modalimg from "@assets/pana.jpg";
import roleimg from "@assets/emptydivimg.svg";

import { CustomSelect } from "../StaffMembers/EditStaff/style";
import { validationSchema, formInitValues } from "./helper";
import ModalComponent from "@components/Modal/Modal";
import { DrawerPrimaryBtn } from "@components/Buttons";

const { Option } = Select;
const NewDepartment = () => {
  const [showNewRoleDrawer, setShowNewRoleDrawer] = useState<boolean>(false);
  const [showEditRoleDrawer, setShowEditRoleDrawer] = useState<boolean>(false);
  const [viewRole, setviewRole] = useState<boolean>(false);
  const [isModalVisible, setIsModalVisible] = useState(false);

  const [managerId, setmanagerId] = useState<any>();
  const [modalvalue, setModalValue] = useState<boolean>(false);
  const [allRoles, setAllRoles] = useState<any>([]);

  const [deptValue, setDeptValue] = useState<any>();
  const [roleName, setRoleName] = useState<string>("");
  const [roleId, setRoleId] = useState<any>();
  const [error, setError] = useState<boolean>(false);
  const [depterror, setDeptError] = useState<any>();
  const [Mngerror, setMngError] = useState<any>();
  var finalrole: any = [];
  // const [close, setClose] = useState<boolean>(false);
  const [rolefinalValues, setRoleFinalValues] = useState<any>([]);
  const [editRoleInfo, seEditRoleInfo] = useState<{
    roleid: string;
    edit: boolean;
  }>({
    roleid: "",
    edit: false
  });
  var roleInitialValues = {
    role_name: " ",
    permissions: [
      {
        name: "Module Name 1",
        module_name_id: 1,
        view: false,
        edit: false,
        delete: false,
        create: false
      },
      {
        name: "Module Name 2",
        module_name_id: 2,
        view: false,
        edit: false,
        delete: false,
        create: false
      },
      {
        name: "Module Name 3",
        module_name_id: 3,
        view: false,
        edit: false,
        delete: false,
        create: false
      }
    ]
  };
  const onClose = () => {
    setShowEditRoleDrawer(false);
    setShowNewRoleDrawer(false);
    // setClose(true);
    seEditRoleInfo({
      roleid: "",
      edit: false
    });
  };

  const showModal = () => {
    if (modalvalue === true) {
      setIsModalVisible(true);
    } else {
      setIsModalVisible(false);
    }
  };
  const closeModal = () => {
    setModalValue(false);
    navigate("/usermanagement/department-list");
  };
  useEffect(() => {
    showModal();
  }, [modalvalue]);

  const getRolePermissionDetails = async (role_id: any) => {
    const resRole: any = await GetStaffPermission(roleId);

    if (resRole.status === 200) {
      setAllRoles([...resRole.data.data]);
      return resRole;
    } else {
      message.error("something Went Wrong");
    }
  };
  useEffect(() => {
    if (showEditRoleDrawer) {
      getRolePermissionDetails(roleId);
    }
  }, [showEditRoleDrawer]);
  const onSubmit = async () => {
    const roleIds = rolefinalValues.map((x: { id: any }) => x.id);
    var { response, success } = await SendDepartments(
      deptValue,
      managerId,
      roleIds
    );
    if (success) {
      setModalValue(true);
    } else {
      const errorresponse = response as any;
      setError(true);
      // setMngError(errorresponse.error.error.manager_id[0]);
      // setDeptError(
      //   errorresponse.error.error.department_name &&
      //     errorresponse.error.error.department_name[0]
      // );
      setModalValue(false);
      return errorresponse.error.error;
    }
  };
  const onRoleSubmit = (values: any) => {
    setviewRole(true);
    setRoleFinalValues([...rolefinalValues, values]);
    setRoleName(values.role);
  };
  const [managerNames, setManagerNames] = useState<
    { value: string; key: string }[]
  >([]);
  useEffect(() => {
    // setAllRoles([...]);
  }, []);
  return (
    <>
      <Formik
        initialValues={formInitValues}
        onSubmit={(values, actions) => {
          // onSubmit();
        }}
        validationSchema={validationSchema}
      >
        {props => {
          const { values, setFieldValue } = props;
          return (
            <FormWrapper>
              <FormTitle>Department Details</FormTitle>
              <ContainerBox>
                <InnerContent>
                  <Main direction="row" justifyContent="space-around">
                    <FlexBox direction="column">
                      <FormLableTypography>
                        Department Name <span>*</span>
                      </FormLableTypography>
                      <InputField
                        id="dept_name"
                        name="dept_name"
                        placeholder="Enter Department Name"
                        onChange={(e: any) => {
                          setDeptValue(e.target.value);
                          setDeptError("");
                        }}
                      />
                      {depterror ? (
                        <p style={{ color: "red" }}>
                          {depterror && depterror
                            ? "Please enter department name"
                            : ""}
                        </p>
                      ) : (
                        ""
                      )}
                    </FlexBox>
                    <FlexBox direction="column">
                      <FormLableTypography>
                        Department Manager
                      </FormLableTypography>
                      <CustomSelect
                        // mode="multiple"
                        showSearch
                        style={{ width: "100%" }}
                        placeholder=""
                        optionFilterProp="children"
                        id="manager-name"
                        onSearch={async (value: any) => {
                          const resRole: any = await GetManagerList(value);
                          if (resRole.status === 200) {
                            let mgrNames = [];
                            for (let i = 0; i < resRole.data.data.length; i++) {
                              mgrNames.push({
                                value: resRole.data.data[i].username,
                                key: resRole.data.data[i].id
                              });
                            }
                            setManagerNames([...mgrNames]);
                          } else {
                          }
                        }}
                        onChange={(value: any, id: any) => {
                          setmanagerId(id.label);
                          setMngError("");
                        }}

                        // optionLabelProp="label"
                      >
                        {managerNames.map((item: any) => {
                          return (
                            <>
                              <Option value={item.value} label={item.key}>
                                {item.value}
                              </Option>
                            </>
                          );
                        })}
                      </CustomSelect>
                    </FlexBox>
                  </Main>
                  <Divider style={{ margin: "35px 0px" }} />
                  <H4Title text="Roles" />
                  {rolefinalValues.length === 0 ? (
                    <div style={{ textAlign: "center" }}>
                      <img
                        src={roleimg}
                        style={{
                          width: "280px",
                          marginBottom: "24px"
                        }}
                      />
                      <ButtonText>
                        There are no roles available at this moment, please
                        <button
                          onClick={() => {
                            setShowNewRoleDrawer(true);
                            roleInitialValues.permissions = [
                              {
                                name: "Module Name 1",
                                module_name_id: 1,
                                view: false,
                                edit: false,
                                delete: false,
                                create: false
                              },
                              {
                                name: "Module Name 2",
                                module_name_id: 2,
                                view: false,
                                edit: false,
                                delete: false,
                                create: false
                              },
                              {
                                name: "Module Name 3",
                                module_name_id: 3,
                                view: false,
                                edit: false,
                                delete: false,
                                create: false
                              }
                            ];
                          }}
                        >
                          add a new role
                        </button>
                      </ButtonText>
                    </div>
                  ) : (
                    <>
                      {viewRole &&
                        rolefinalValues.map((roleItem: any) => {
                          return (
                            <>
                              <RowContent>
                                <span>{roleItem.role}</span>
                                <Flex>
                                  <Link
                                    to=""
                                    style={{
                                      paddingRight: "24px",
                                      color: "rgba(78, 28, 149, 1)"
                                    }}
                                  >
                                    <span
                                      style={{
                                        color: "rgba(78, 28, 149, 1)"
                                      }}
                                      onClick={() => {
                                        setRoleId(roleItem.id);
                                        // setEditVisible(true);
                                        seEditRoleInfo({
                                          roleid: roleItem.id,
                                          edit: true
                                        });
                                        // getRolePermissionDetails(roleItem.id);
                                      }}
                                    >
                                      Edit
                                    </span>
                                  </Link>
                                  <Link
                                    to=""
                                    style={{ color: "rgba(78, 28, 149, 1)" }}
                                  >
                                    Delete
                                  </Link>
                                </Flex>
                              </RowContent>
                            </>
                          );
                        })}
                      <Link
                        to=""
                        style={{
                          color: "#4E1C95",
                          lineHeight: "22px",
                          marginTop: "32px",
                          alignItems: "center",
                          display: "inline-flex"
                        }}
                        onClick={() => {
                          setShowNewRoleDrawer(true);
                        }}
                      >
                        <span
                          style={{ paddingRight: "10px", fontSize: "20px" }}
                        >
                          +
                        </span>{" "}
                        Create New Role
                      </Link>
                    </>
                  )}

                  <FlexBox className="footerbtn">
                    <button
                      type="submit"
                      className="submitbtn"
                      onClick={onSubmit}
                    >
                      Create department
                    </button>
                    <Link
                      to={"/usermanagement/department-list"}
                      data-testid="gouserList"
                      className="cancelbtn"
                    >
                      Cancel
                    </Link>
                  </FlexBox>
                </InnerContent>
              </ContainerBox>
              {showNewRoleDrawer && (
                <NewRolesDrawer
                  onClose={onClose}
                  visible={showNewRoleDrawer}
                  tableValues={roleInitialValues.permissions}
                  onRoleSubmit={(values: any) => {
                    onRoleSubmit(values);
                  }}
                />
              )}
              {editRoleInfo.edit && allRoles && (
                <EditRolesDrawer
                  onClose={onClose}
                  visible={editRoleInfo.edit}
                  roleId={roleId}
                  username={deptValue}
                  tableValues={roleInitialValues.permissions}
                  roleNames={roleName}
                  onRoleUpdate={() => {
                    getRolePermissionDetails(roleId);
                  }}
                />
              )}
            </FormWrapper>
          );
        }}
      </Formik>
      {modalvalue === true ? (
        <ModalComponent
          visible={modalvalue}
          onOK={closeModal}
          onCancel={closeModal}
          //@ts-ignore
          modalimage={modalimg}
          title=""
          content=""
        >
          <div style={{ textAlign: "center", padding: "24px" }}>
            <img
              src={modalimg}
              alt="backgroundimg"
              style={{ marginBottom: "40px" }}
            />
            <H3
              text={`New department "${deptValue}" has been created`}
              className="MainTitle"
            />
            <p
              className="modalpara"
              style={{
                fontFamily: "Poppins",
                color: "#50514F",
                fontSize: "14px"
              }}
            />
          </div>
          <Flex justifyContent="center">
            <DrawerPrimaryBtn
              text="Back to Department List"
              onClick={closeModal}
            />
          </Flex>
        </ModalComponent>
      ) : (
        ""
      )}
    </>
  );
};

export default NewDepartment;
