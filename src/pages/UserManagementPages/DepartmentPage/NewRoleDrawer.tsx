import DrawerView from "@components/Drawer";
import React, { FC, useState, useEffect } from "react";
import { Card, Checkbox } from "antd";
import { SendRoleDetails } from "@services/UserService";
import Accordion from "@components/Accordion";
import {
  MainCard,
  DrawerFooter,
  AccordionContainer,
  DrwaerInnerContent,
  DrwaerLabel,
  InputField
} from "./style";
import { DrawerPrimaryBtn, Transparent } from "@components/Buttons/index";

interface DrawerProps {
  onClose: () => void;
  visible: boolean;
  title?: string;
  className?: string;
  children?: React.ReactNode;
  onRoleSubmit: (values: any) => void;
  tableValues?: any;
}
var roleInitialValues = {
  role_name: " ",
  permissions: [
    {
      name: "Module Name 1",
      module_name_id: 1,
      view: false,
      edit: false,
      delete: false,
      create: false
    },
    {
      name: "Module Name 2",
      module_name_id: 2,
      view: false,
      edit: false,
      delete: false,
      create: false
    },
    {
      name: "Module Name 3",
      module_name_id: 3,
      view: false,
      edit: false,
      delete: false,
      create: false
    }
  ]
};
const NewRoleDrawer: FC<DrawerProps> = ({
  onClose,
  visible,
  children,
  title,
  className,
  onRoleSubmit,
  tableValues
}) => {
  const [modalvalue, setModalValue] = useState<boolean>(false);
  const [roleName, setroleName] = useState<string>("");
  const [TableValue, setTableValue] = useState();
  const [idealValues, setIdealValues] = useState(tableValues);
  const [permissions, setPermissions] = useState<any>(tableValues);

  const [roleInfo, setRoleInfo] = useState<Array<any>>([]);
  const [error, setError] = useState<boolean>(false);
  const [rolenameError, setrolenameError] = useState<any>();
  const [close, setClose] = useState<boolean>(false);
  function onChange(e: any) {
    let id = Number(String(e.target.name).split("-")[1]) - 1;
    let value = String(e.target.name).split("-")[0];
    if (value == "view") permissions[id].view = e.target.checked;
    if (value == "edit") permissions[id].edit = e.target.checked;
    if (value == "delete") permissions[id].delete = e.target.checked;
    if (value == "create") permissions[id].create = e.target.checked;
    setPermissions([...permissions]);
  }
  useEffect(() => {
    tableValues.permissions = [
      {
        name: "Module Name 1",
        module_name_id: 1,
        view: false,
        edit: false,
        delete: false,
        create: false
      },
      {
        name: "Module Name 2",
        module_name_id: 2,
        view: false,
        edit: false,
        delete: false,
        create: false
      },
      {
        name: "Module Name 3",
        module_name_id: 3,
        view: false,
        edit: false,
        delete: false,
        create: false
      }
    ];
  }, []);
  const onCheck = async () => {
    var { response, success } = await SendRoleDetails(roleName, permissions);
    if (success) {
      onClose();

      roleInfo.push(...roleInfo, response.data);
      // setPermissions(tableValues.permissions);
      onRoleSubmit(response.data);
    } else {
      const errorresponse = response as any;
      setError(true);
      setrolenameError(errorresponse.error.errors.role_name[0]);
    }
  };

  return (
    <div>
      <DrawerView onClose={onClose} visible={visible} title={`Create new role`}>
        <DrwaerInnerContent>
          <DrwaerLabel>
            Role Name
            <span>*</span>
          </DrwaerLabel>
          <InputField
            placeholder="Branch admin"
            onChange={(e: any) => {
              setroleName(e.target.value);
              setError(false);
            }}
            name={"branch-admin"}
          />
          {error && rolenameError ? (
            <p style={{ color: "red" }}>Please enter role name</p>
          ) : (
            ""
          )}
          <AccordionContainer>
            <Accordion title="Role Permissions">
              <MainCard>
                <Card title="" bordered={false}>
                  {permissions &&
                    permissions.map((module: any) => {
                      return (
                        <Accordion
                          title={module.name}
                          key={module.module_name_id}
                        >
                          <Checkbox
                            name={"view-" + module.module_name_id}
                            checked={module.view}
                            onChange={onChange}
                          >
                            View
                          </Checkbox>
                          <Checkbox
                            name={"edit-" + module.module_name_id}
                            checked={module.edit}
                            onChange={onChange}
                          >
                            Edit
                          </Checkbox>
                          <Checkbox
                            name={"create-" + module.module_name_id}
                            checked={module.create}
                            onChange={onChange}
                          >
                            Create
                          </Checkbox>
                          <Checkbox
                            name={"delete-" + module.module_name_id}
                            checked={module.delete}
                            onChange={onChange}
                          >
                            Delete
                          </Checkbox>
                        </Accordion>
                      );
                    })}
                </Card>
              </MainCard>
            </Accordion>
          </AccordionContainer>
        </DrwaerInnerContent>
        <DrawerFooter>
          <DrawerPrimaryBtn
            testId="SubmitAndproceed"
            text="Submit and Proceed"
            onClick={() => {
              onCheck();
              setClose(true);
            }}
          />

          <Transparent
            text="Cancel"
            onClick={() => {
              onClose();
              setClose(true);
            }}
          />
        </DrawerFooter>
      </DrawerView>
    </div>
  );
};

export default NewRoleDrawer;
