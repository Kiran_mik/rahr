import * as Yup from "yup";


export const formInitValues = {
    dept_name: "",
    manager_name: "",

  };

  export const validationSchema = Yup.object({
		dept_name: Yup.string().required("Please enter your department name"),
		manager_name: Yup.string().notRequired(),
  })