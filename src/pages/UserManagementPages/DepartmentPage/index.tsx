import React, { FC } from "react";
import { RouteComponentProps } from "@reach/router";
import StaffHeader from "@components/StaffHeader";
import { H3 } from "@components/Typography/Typography";
import NewDepartment from "./NewDepartment";
import DashboardLayout from "@pages/UserManagementPages/StaffMembers/DashboardLayout";

export interface DeptProps extends RouteComponentProps {
  title?: string;
}
const index: FC<DeptProps> = () => {
  return (
    <DashboardLayout>
      <StaffHeader>
        <div>
          <H3 text="New Department" />
        </div>
      </StaffHeader>
      <NewDepartment />
    </DashboardLayout>
  );
};

export default index;
