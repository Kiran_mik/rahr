import styled from "styled-components";
import BreadcrumbItem from "antd/lib/breadcrumb/BreadcrumbItem";
import { Flex } from "@components/Flex";
import { FormLableTypography, H4SemiBold } from "@components/Typography/Typography";
import { Input } from "antd";
export const FormWrapper = styled.div`
    background: white;
    width: 100%;
    padding: 32px;
    border-radius: 10px;
    margin-bottom: 42px;
	.ant-select-selector{
		font-size: 14px;
    font-family: Poppins;
    padding: 11px 12px;
    border: 1px solid #BDBDD3;
    background: transparent;
    border-radius: 4px;
    color: #17082D;
    font-weight: 500;
   
    line-height: 22px;
	height:auto;
	}
 

.MainTitle{
	font-weight:600px;
}

textarea.ant-input{
	height:111px
}
.ant-input-textarea-show-count::after{
	float:left;
	color:rgba(123, 123, 151, 1);
	font-size:14px;
	line-height:22px;
	font-family:Poppins;
	font-weight:400;
}
	}
`
export const FlexBox = styled(Flex)`
        flex:1 ;
		padding:0px 8px
       `;
export const ButtonText = styled.p`
       color:#59596b;
       font-size:16px;
       line-height:22px;
       font-weight:400;
           margin-bottom: 8px;
       button{
         background:transparent;
         color:#4E1C95;
         text-decoration:underline;
         cursor:pointer;
       }
       `;

export const Main = styled(Flex)`
	   direction:"row" ;
	     margin: 0px -8px;
       `;
export const ContainerBox = styled.div`
	
	position: relative;
	.error {
		font-size: 14px;
		color: #eb5757;
	}
`;
export const InnerContent = styled.div`
	display: flex;
	flex-direction: column;
	overflow-y: auto;
	overflow-x: hidden;
	box-sizing: border-box;
	// padding-bottom: 24px;
.customdivider{
			margin:35px 0;
			border-color:#A2A2BA;
		}
	@media (max-width: 600px) {
		padding: 28px 16px;
	}
	@media (max-width: 359px) {
		padding: 24px 12px;
	}
	.error {
		font-size: 14px;
		color: #eb5757;
		margin-top: 4px;
	}
	.forminput{
		width:477px;
		height:46px;

	}
	.submitbtn{
        padding: 12px 24px;
background: #4E1C95;
color: #fff;
font-family: Poppins;
font-size: 14px;
line-height: 22px;
border-radius: 8px;
width: max-content;
cursor:pointer;
}

.cancelbtn{
        background: transparent;
font-size: 14px;
line-height: 22px;
color: #4E1C95;
font-family: Poppins;
font-weight: 400;
margin-left: 22px;
margin-top: 10px;
cursor pointer;
}

.footerbtn{
padding:0px;
    margin-top: 24px;
border-top: 1px solid #E2E2EE;
padding-top: 24px;

}

	  
`;
export const RowContent = styled.div`
display: flex;
flex-direction: row;
justify-content: space-between;
align-items: center;
padding: 8px 16px;
margin:8px 0px;
position: static;
width: 100%;
height: 38px;
left: 0px;
top: 0px;

background: #F5F5FF;
border-radius: 8px;

span{
    font-family: Poppins;
font-style: normal;
font-weight: normal;
font-size: 14px;
line-height: 22px;


color: #17082D;

}

`
export const AccordionContainer = styled.div`
position:static;
width:100%;
 max-height: calc(100vh - 339px);
min-height: calc(100vh - 339px);
background: #F5F5FF;
overflow: auto;
overflow-x: hidden;
margin-top:24px;
    border-radius: 8px;
.ant-collapse > .ant-collapse-item > .ant-collapse-header .ant-collapse-arrow {
    float:right;
        -webkit-text-emphasis: circle;
}
`
export const MainCard = styled.div`
width: 100%;
max-height: calc(100vh - 432px);
min-height: calc(100vh - 412px);
left: 16px;
top: 64px;
overflow: auto;
background: #fff;
    padding: 16px 17px;
border-radius: 8px;
    font-family: Poppins;

    &::-webkit-scrollbar {
  width: 5px;
}


&::-webkit-scrollbar-track {
  box-shadow: inset 0 0 5px grey;
  border-radius: 10px;
}


&::-webkit-scrollbar-thumb {
  background:  rgba(151, 146, 227, 1);
  border-radius: 10px;
}


&::-webkit-scrollbar-thumb:hover {
  background:  rgba(151, 146, 227, 1);
}
.ant-card{
      max-height: calc(100vh - 450px);
    min-height: calc(100vh - 450px);
    overflow:auto;

     &::-webkit-scrollbar {
  width: 5px;
}


&::-webkit-scrollbar-track {
  box-shadow: inset 0 0 5px grey;
  border-radius: 10px;
}


&::-webkit-scrollbar-thumb {
  background:  rgba(151, 146, 227, 1);
  border-radius: 10px;
}


&::-webkit-scrollbar-thumb:hover {
  background:  rgba(151, 146, 227, 1);
}
}
    .ant-collapse > .ant-collapse-item > .ant-collapse-header .ant-collapse-arrow {
 
         float: left;
    -webkit-text-emphasis: circle;
    background: rgba(189,189,211,1);
    padding: 2px 6px;
  margin: 3px 8px 3px 0px;
    border-radius: 4px;
    height: auto;
    font-size: 8px;
    
    align-items: center;
}
.ant-card-body{
    padding:0px;
}
.ant-collapse > .ant-collapse-item > .ant-collapse-header {
      padding: 0px 0px;
}
.ant-collapse > .ant-collapse-item > .ant-collapse-header {
    color: #17082D
}
.ant-collapse-ghost {
       background-color: transparent;
    border: 0;
    padding: 20px 0px;
    border-bottom: 1px dashed #A2A2BA;
    border-radius: 0px;
    font-size: 14px;
    font-weight: 600;
    font-family: Poppins
}

.ant-checkbox + span {
    padding-right: 8px;
    padding-left: 8px;
    font-weight: 400;
    color: #17082D;
    font-size: 14px;
}
.ant-collapse-item .ant-collapse-arrow > svg{
    transform: rotate(-90deg);
}
&::-webkit-scrollbar {
  width: 5px;
}


&::-webkit-scrollbar-track {
  box-shadow: inset 0 0 5px grey;
  border-radius: 10px;
}


&::-webkit-scrollbar-thumb {
  background:  rgba(151, 146, 227, 1);
  border-radius: 10px;
}


&::-webkit-scrollbar-thumb:hover {
  background:  rgba(151, 146, 227, 1);
}
`
export const DrwaerLabel = styled(FormLableTypography)`
 font-size:14px;

`


export const DrwaerInnerContent = styled.div`
   max-height: calc(100vh - 181px);
    overflow: auto;
    min-height: calc(100vh - 181px);
    padding:24px 0px;

`
export const DrawerFooter = styled.div`
display:flex;
border-top:1px solid #E2E2EE;
padding:24px 0px 18px 0px;
    -webkit-box-pack: justify;
    -webkit-justify-content: space-between;
    -ms-flex-pack: justify;
    justify-content: space-between;
`

export const ModalFooter = styled.div`
display:flex;
border-top:1px solid #DBDBED;
padding:29px 0px 0px 0px;
    justify-content: flex-start;
`
export const H4Title = styled(H4SemiBold)`
margin-bottom:16px;
`
export const InputField = styled(Input)`
  font-size: 12px;
    font-family: Poppins;
    padding: 11px 10px;
    border: 1px solid #AFADC8;
    background: transparent;
    border-radius:8px;
    color: #17082D;
    font-weight: 500;
    line-height:22px;
    &:hover{
       border: 1px solid #AFADC8;
    }
`