import React, { useState, useEffect } from "react";

import {
  IconButton,
  EditIconButton,
  DrawerPrimaryBtn,
  Transparent,
  AddBtn,
} from "@components/Buttons";
import { TableWrap, InputField, SelectSearch } from "./style";
import {
  ButttonWrapper,
  FormDevide,
  FormSectionWrapper,
} from "@pages/UserManagementPages/StaffMembers/StaffDetails/style";
import { Link, navigate } from "@reach/router";
import { Col, Dropdown, Input, message, Row, Select } from "antd";
import { FormWrapper } from "@pages/UserManagementPages/StaffMembers/NewStaff/style";
import { BackArrow, DemoImage, EditIcon } from "@assets/index";

import Loader from "@components/Loader";

import { Table } from "antd";
import {
  FormLableTypography,
  H4SemiBold,
  H4Typography,
} from "@components/Typography/Typography";
import FormDataView from "@components/ViewForm/FormDataField";
import { Divider } from "./style";
import { SearchBar } from "@components/SerachBar";
import { departmentDetails } from "./helper";
import { string } from "yup/lib/locale";
import {
  DepartmentInfoProps,
  DepartmentInformationInterface,
  StaffMembersInterface,
} from "./type";
import {
  CustomSelect,
  DrawerFooter,
  FlexBox,
} from "@pages/UserManagementPages/StaffMembers/EditStaff/style";
import { GetManagerList, SendRoleDetails } from "@services/UserService";
import {
  SendStaff,
  UpdateDeptDetails,
} from "@services/UserManagement/DepartmentService";
import { GetDeptStaffList } from "@services/GetStaffList";
const { Option } = Select;

const columns = [
  {
    title: "User name",
    dataIndex: "userObject",
    key: "userObject",

    sorter: (a: any, b: any) =>
      a.userObject.full_name.localeCompare(b.userObject.full_name),
    render: (userObject: any) => {
      return (
        <div style={{ fontWeight: 500, display: "flex", alignItems: "center" }}>
          {" "}
          <img
            alt="ProfileImage"
            style={{
              width: "24px",
              height: "24px",
              marginRight: "10px",
              borderRadius: "50%",
            }}
            src={
              userObject.profile_photo ? userObject.profile_photo : DemoImage
            }
          />
          <Link
            to={`/usermanagement/staff-detail/${userObject.id}`}
            style={{ color: "#4E1C95" }}
          >
            {userObject.full_name}
          </Link>
          <div
            style={{
              width: "18px",
              height: "18px",
              marginLeft: "8px",
              backgroundColor: "#F58A07",
              borderRadius: "50%",
              color: "#FFF",
              textAlign: "center",
            }}
          >
            !
          </div>
        </div>
      );
    },
  },
  {
    title: "Role",
    dataIndex: "role",
    key: "role",
    sorter: (a: any, b: { role: any }) => a.role.localeCompare(b.role),
    width: "30%",
  },
  {
    title: "Email",
    dataIndex: "email",
    key: "email",
    sorter: (a: any, b: any) => a.email.localeCompare(b.email),
    width: "30%",
  },
];

// rowselection object indicates the need for row selection
const rowSelection = {
  onChange: (selectedRowKeys: any, selectedRows: any) => {},
  getCheckboxProps: (record: any) => ({
    disabled: record.branchName === "Disabled Users",
    name: record.branchName,
  }),
};
var managerNames: any = [];

const DepartmentInfo: React.FC<DepartmentInfoProps> = ({ id }) => {
  const [loading, setLoading] = useState<boolean>(false);
  const [departmentInformation, setDepartmentInformation] = useState<
    DepartmentInformationInterface
  >();
  const [dataSource, setDataSource] = useState([]);
  const [backupDataSource, setBackupDataSource] = useState([]);
  const [selectionType, setSelectionType] = useState<"checkbox">("checkbox");
  const [edit, setEdit] = useState<boolean>(false);
  const [newDeptName, setNewdeptName] = useState<any>("");
  const [newsetManagerName, setManagerName] = useState<any>("");
  const [managerId, setmanagerId] = useState<any>();
  const [managerNames, setManagerNames] = useState<
    { value: string; key: string }[]
  >([]);
  const [staffNames, setStaffNames] = useState<
    { value: string; key: string }[]
  >([]);
  const [staffIds, setStaffIds] = useState<any>([]);
  function onChange(pagination: any, filter: any, sorter: any, extra: any) {}
  const onCheck = async () => {
    var res: any = await UpdateDeptDetails(id, newDeptName, managerId);

    if (res && res.success) {
      message.success(res.message);
      navigate(`/usermanagement/department-list`);
      // window.location.reload(false);
      setEdit(false);
    } else {
      message.error(res.error.message);
    }
  };
  const onStaff = async () => {
    var { response, success } = await SendStaff(id, staffIds);
    if (success) {
      message.success("Staff added successfully");
      getDepartmentDetails();
    } else {
      const errorresponse = response as any;
    }
  };
  const getDepartmentDetails = async () => {
    setLoading(true);
    const res: any = await departmentDetails(id);

    if (res.status === 200) {
      setDepartmentInformation({
        department_name: res.data.data[0].department_name,
        manager_user: res.data.data[0].manager_user
          ? res.data.data[0].manager_user.profile_detail
          : undefined,
      });
      if (res.data.data[0]) {
        setNewdeptName(res.data.data[0].department_name);
        if (res.data.data[0].manager_user) {
          setmanagerId(res.data.data[0].manager_user.id);
        }

        setDataSource(
          res.data.data[0].department_staff_members.map((row: any) => ({
            id: row.id,
            userObject: {
              id: row.id,
              full_name: row.profile_detail.full_name,
              profile_photo: row.profile_detail.profile_photo
                ? row.profile_detail.profile_photo
                : DemoImage,
            },
            role: row.user_role.role.role,
            email: row.email,
            key: row.id,
          }))
        );
        setBackupDataSource(
          res.data.data[0].department_staff_members.map((row: any) => ({
            id: row.id,
            userObject: {
              id: row.id,
              full_name: row.profile_detail.full_name,
              profile_photo: row.profile_detail.profile_photo
                ? row.profile_detail.profile_photo
                : DemoImage,
            },
            role: row.user_role.role.role,
            email: row.email,
            key: row.id,
          }))
        );
      }
      setLoading(false);
    } else {
      setLoading(false);
      // message.error(res.error.error.departmentId[0]);
    }
  };
  useEffect(() => {
    getDepartmentDetails();
  }, []);
  return (
    <>
      <ButttonWrapper>
        <IconButton
          text={"Back to all Departments"}
          testId="BackToRAH"
          onClick={() => {
            navigate("/usermanagement/department-list");
          }}
          icon={BackArrow}
        />

        <EditIconButton
          text={"Edit"}
          icon={EditIcon}
          testId="EditProfile"
          onClick={() => {
            setEdit(true);
          }}
        />
      </ButttonWrapper>

      <Loader loading={loading} />
      {departmentInformation && (
        <>
          {!edit ? (
            <>
              <FormSectionWrapper>
                <H4SemiBold text="Department information" />
              </FormSectionWrapper>

              <FormDevide firstChild={true}>
                <Row>
                  <Col span={8}>
                    <FormDataView
                      heading={"Department name"}
                      value={
                        departmentInformation &&
                        departmentInformation.department_name
                          ? departmentInformation.department_name
                          : "-"
                      }
                    />
                  </Col>
                  <Col span={8}>
                    <FormDataView
                      image={
                        departmentInformation &&
                        departmentInformation.manager_user
                          ? departmentInformation.manager_user.profile_photo
                          : DemoImage
                      }
                      heading={"Manager"}
                      value={
                        departmentInformation &&
                        departmentInformation.manager_user
                          ? departmentInformation.manager_user.full_name
                          : "-"
                      }
                    />
                  </Col>
                </Row>
              </FormDevide>
            </>
          ) : (
            <>
              <FormSectionWrapper>
                <H4SemiBold text="Department information" />
              </FormSectionWrapper>

              <FormDevide firstChild={true}>
                <Row>
                  <FlexBox
                    direction="column"
                    style={{ paddingLeft: "0px", marginBottom: "21px" }}
                  >
                    <FormLableTypography>
                      Department Name <span>*</span>
                    </FormLableTypography>
                    <InputField
                      id="dept_name"
                      name="dept_name"
                      placeholder={departmentInformation.department_name}
                      onChange={(e: any) => {
                        if (e.target.value.toString().length < 30) {
                          setNewdeptName(
                            e.target.value
                              ? e.target.value
                              : departmentInformation.department_name
                          );
                        }
                      }}
                      defaultValue={departmentInformation.department_name}
                      // value={departmentInformation.department_name}
                    />
                  </FlexBox>

                  <FlexBox
                    direction="column"
                    style={{ paddingRight: "0px", marginBottom: "21px" }}
                  >
                    <FormLableTypography>
                      Department Manager <span>*</span>
                    </FormLableTypography>
                    <CustomSelect
                      // mode="multiple"
                      showSearch
                      style={{ width: "100%" }}
                      placeholder="select manager for your department"
                      optionFilterProp="children"
                      id="manager-name"
                      onSearch={async (value: any) => {
                        setManagerName(value);
                        const resRole: any = await GetManagerList(value);

                        if (resRole.status === 200) {
                          const managers = resRole.data.data.map(
                            (item: any) => {
                              return {
                                value: item.username,
                                key: item.id,
                              };
                            }
                          );
                          setManagerNames(managers);
                        }
                      }}
                      defaultValue={
                        departmentInformation.manager_user &&
                        departmentInformation.manager_user.full_name
                      }
                      onChange={(value: any, id: any) => {
                        setmanagerId(
                          id.label
                            ? id.label
                            : departmentInformation.manager_user.id
                        );
                        //   setMngError("");
                      }}

                      // optionLabelProp="label"
                    >
                      {managerNames.map((item: any) => {
                        return (
                          <>
                            <Option value={item.value} label={item.key}>
                              {item.value}
                            </Option>
                          </>
                        );
                      })}
                    </CustomSelect>
                  </FlexBox>
                </Row>
              </FormDevide>
            </>
          )}

          <Divider style={{ margin: "14px 0px 11px 0px" }} />
          <FormDevide firstChild={true}>
            <FormSectionWrapper>
              <H4SemiBold text="Department staff" />
              {!edit ? (
                <SearchBar
                  className="ViewBranchDetailSearch"
                  placeholder="Search agent by name or role"
                  onChange={(e: any) => {
                    let filterList: any = backupDataSource
                      ? backupDataSource.filter(
                          (entry: any) =>
                            entry.userObject.full_name
                              .toLowerCase()
                              .includes(e.target.value) ||
                            entry.role.toLowerCase().includes(e.target.value)
                        )
                      : [];
                    setDataSource(filterList);
                  }}
                />
              ) : (
                <div style={{ display: "flex" }}>
                  <SelectSearch
                    style={{ marginRight: "10px" }}
                    // mode="multiple"
                    showSearch
                    placeholder="Find user from a list"
                    optionFilterProp="children"
                    id="staff-name"
                    onSearch={async (value: any) => {
                      const resRole: any = await GetDeptStaffList(value);
                      if (resRole) {
                        let staff = [];
                        for (let i = 0; i < resRole.data.data.length; i++) {
                          staff.push({
                            value:
                              resRole.data.data[i].first_name +
                              resRole.data.data[i].last_name,
                            key: resRole.data.data[i].id,
                          });
                        }
                        setStaffNames([...staff]);
                      }
                    }}
                    onChange={(value: any, id: any) => {
                      setmanagerId(id.label);
                      staffIds.push(id.label);
                    }}

                    // optionLabelProp="label"
                  >
                    {staffNames.map((item: any) => {
                      return (
                        <>
                          <Option value={item.value} label={item.key}>
                            {item.value}
                          </Option>
                        </>
                      );
                    })}
                  </SelectSearch>
                  <AddBtn
                    text={"Add"}
                    // icon={EditIcon}
                    testId="Add"
                    onClick={() => {
                      onStaff();
                    }}
                  />
                </div>
              )}
            </FormSectionWrapper>
            {dataSource.length > 0 && (
              <TableWrap style={{ marginTop: "4px" }}>
                <Table
                  // loading={loading}
                  rowSelection={{
                    type: selectionType,
                    ...rowSelection,
                  }}
                  columns={columns}
                  dataSource={dataSource}
                  pagination={false}
                  onChange={onChange}
                />
              </TableWrap>
            )}
          </FormDevide>
        </>
      )}
      {edit && (
        <DrawerFooter>
          <DrawerPrimaryBtn
            testId="SubmitAndproceed"
            text="Submit changes"
            onClick={() => {
              // showModal();
              onCheck();
            }}
          />

          <Transparent
            text="Cancel"
            onClick={() => {
              setEdit(false);
            }}
          />
        </DrawerFooter>
      )}
    </>
  );
};

export default DepartmentInfo;
