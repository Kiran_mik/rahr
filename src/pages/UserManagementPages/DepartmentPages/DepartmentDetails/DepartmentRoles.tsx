import React, { useState, useEffect } from "react";

import { IconButton, EditIconButton } from "@components/Buttons";
import { ButttonWrapper } from "@pages/UserManagementPages/StaffMembers/StaffDetails/style";
import { Link, navigate } from "@reach/router";
import { TableWrap } from "./style";
import { message, Row } from "antd";

import {
  BackArrow,
  DemoImage,
  EditIcon,
  FillEditIcon,
  BagIcon
} from "@assets/index";
// import { Bag, editIcon } from "@assets";
import Loader from "@components/Loader";
import { Table } from "antd";
import { departmentRoles, roleInitialValues } from "./helper";
import EditRolesDrawer from "@pages/UserManagementPages/StaffMembers/EditStaff/EditRolesDrawer";
import { GetStaffPermission } from "@services/UserService";
interface DepartmentRolesProps {
  id?: any;
}

// rowselection object indicates the need for row selection
const rowSelection = {
  onChange: (selectedRowKeys: any, selectedRows: any) => {},
  getCheckboxProps: (record: any) => ({
    disabled: record.branchName === "Disabled Users",
    name: record.branchName
  })
};

const DepartmentRoles: React.FC<DepartmentRolesProps> = ({ id }) => {
  const [loading, setLoading] = useState<boolean>(false);
  const [roleInformation, setrRoleInformation] = useState([]);

  const [backupRoleInformation, setBackupRoleInformation] = useState([]);
  const [selectionType, setSelectionType] = useState<"checkbox">("checkbox");
  const [edit, setEdit] = useState<boolean>(false);
  const [allRoles, setAllRoles] = useState<any>([]);
  const [roleId, setRoleid] = useState();
  const [roleName, setRoleName] = useState();

  const columns = [
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
      sorter: (a: any, b: any) => a.name.localeCompare(b.name),
      render: (name: string) => {
        return(
          <div style={{ fontWeight: 500}}>{name}</div>
        );
      }
    },
    {
      title: "# Users",
      dataIndex: "users",
      key: "users",
      sorter: (a: any, b: any) => a.users - b.users
    },
    {
      title: "Permission",
      dataIndex: "permissions",
      key: "permissions",
      render: (permission: any) => {
        const jsonPermission = JSON.parse(permission);
        return "permission";
      },
      sorter: (a: any, b: any) => a.permissions.localeCompare(b.permissions)
    },
    {
      title: "",
      dataIndex: "ImageIcon",
      key: "ImageIcon",
      sorter: (a: any, b: any) => a.permissions.localeCompare(b.ImageIcon),
      render: (ImageIcon: any) => {
        return (
          <div style={{ display: "Flex" }}>
            <Link
              to=""
              onClick={() => {
                ImageIcon.clickImage(ImageIcon.roleId);
                ImageIcon.SendName(ImageIcon.RoleName);
                setEdit(true);
              }}
            >
              {" "}
              <img
                src={FillEditIcon}
                alt={DemoImage}
                style={{ marginRight: "12px" }}
              />
            </Link>
            <img src={BagIcon} alt={DemoImage} />
          </div>
        );
      }
    }
  ];
  const getRolePermissionDetails = async (role_id: any) => {
    const resRole: any = await GetStaffPermission(role_id);

    if (resRole.status === 200) {
      setAllRoles([...resRole.data.data]);
      // setEditTableValue([...resRole.data.data]);
      return resRole;
    } else {
      // setLoading(false);

      message.error("something Went Wrong");
    }
  };
  function onChange(pagination: any, filter: any, sorter: any, extra: any) {}

  const getDepartmentRoles = async () => {
    setLoading(true);
    const res: any = await departmentRoles(id);
    if (res.status === 200) {
      setrRoleInformation(
        res.data.data[0].department_role.map((role: any) => ({
          name: role.roles[0].role,
          users: role.roles[0].user_roles_count
            ? role.roles[0].user_roles_count
            : 0,
          permissions: role.roles[0].module_permissions
            ? JSON.stringify(role.roles[0].module_permissions)
            : "-",
          ImageIcon: {
            clickImage: setRoleid,
            roleId: role.roles[0].id,
            SendName: setRoleName,
            RoleName: role.roles[0].name
          }
        }))
      );

      res.data.data[0].department_role.map((item: any) => {
        setRoleid(item.role_id);
        item.roles.map((roleItem: any) => {
          // setRoleName(roleItem.name);
        });
      });
      setLoading(false);
    } else {
      setLoading(false);
      message.error(res.error.error.departmentId[0]);
    }
  };

  useEffect(() => {
    getDepartmentRoles();
    roleInformation.map((item: any) => {
      // setRoleName(item.name);
    });
  }, []);
  const onClose = () => {
    setEdit(false);
  };

  return (
    <>
      <ButttonWrapper>
        <IconButton
          text={"Back to all Departments"}
          testId="BackToRAH"
          onClick={() => {
            navigate("/usermanagement/department-list");
          }}
          icon={BackArrow}
        />

        {/* <EditIconButton
          text={"Edit"}
          icon={EditIcon}
          testId="EditProfile"
          onClick={() => {
            setEdit(true);
          }}
        /> */}
      </ButttonWrapper>

      <Loader loading={loading} />
      {!loading && (
        <>
          <TableWrap style={{ marginTop: "20px" }}>
            <Table
              // loading={loading}
              rowSelection={{
                type: selectionType,
                ...rowSelection
              }}
              columns={columns}
              dataSource={roleInformation}
              pagination={false}
              onChange={onChange}
            />
          </TableWrap>
        </>
      )}
      {edit && (
        <EditRolesDrawer
          onClose={onClose}
          visible={edit}
          roleId={roleId}
          username={roleName}
          tableValues={roleInitialValues.permissions}
          roleNames={roleName}
          onRoleUpdate={() => {
            getRolePermissionDetails(roleId);
          }}
        />
      )}
    </>
  );
};

export default DepartmentRoles;
