import { getDepartmentDetails, getDepartmentRoles } from "@services/UserManagement/DepartmentService";

export const departmentDetails = async (departmentId:any) => {
    const res: any = await getDepartmentDetails(departmentId);
    return res;
  };

  export const departmentRoles = async (departmentId:any) => {
    const res: any = await getDepartmentRoles(departmentId);
    return res;
  };



 export  const roleInitialValues = {
    role_name: " ",
    permissions: [
      {
        name: "Module Name 1",
        module_name_id: 1,
        view: false,
        edit: false,
        delete: false,
        create: false
      },
      {
        name: "Module Name 2",
        module_name_id: 2,
        view: false,
        edit: false,
        delete: false,
        create: false
      },
      {
        name: "Module Name 3",
        module_name_id: 3,
        view: false,
        edit: false,
        delete: false,
        create: false
      }
    ]
  };