import React, { FC } from "react";
import { Link } from "@reach/router";
import DashboardLayout from "@pages/UserManagementPages/StaffMembers/DashboardLayout";

import StaffHeader from "@components/StaffHeader";
import { H3 } from "@components/Typography/Typography";
import { Breadcrumb } from "antd";
import {
  BreadItem,
  UserBreadWrap
} from "@pages/UserManagementPages/StaffMembers/StaffDetails/style";

import HorizontalTab from "@components/HorizontalTabs";
import DepartmentRoles from "./DepartmentRoles";
import DepartmentInfo from "./DepartmentInfo";

const DepartmentDetail: FC<any> = ({ departmentId }) => {
  const TabValues = [
    {
      tab: "Department information & members",
      content: <DepartmentInfo id={departmentId} />,
      key: 1
    },
    { tab: "Roles", content: <DepartmentRoles id={departmentId} />, key: 2 }
  ];
  return (
    <>
      <DashboardLayout>
        <StaffHeader>
          <UserBreadWrap>
            <H3 text="Department details" />
            <Breadcrumb separator=">">
              <Link
                to="/usermanagement/department-list"
                data-testid="gouserList"
              >
                <BreadItem data-testid="breadcrumb-user" firstNav={true}>
                  List of departments
                </BreadItem>
              </Link>
              <BreadItem firstNav={false}>Deal Processing</BreadItem>
            </Breadcrumb>
          </UserBreadWrap>
        </StaffHeader>

        <HorizontalTab value={TabValues} />
      </DashboardLayout>
    </>
  );
};

export default DepartmentDetail;
