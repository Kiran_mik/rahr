import styled from "styled-components";
import { TableWrapper } from "@components/Table/style";
import { Input, Select } from "antd";
import {
  SearchIcon
} from "@assets/index";
export const Divider = styled.div`
margin:10px 0;
  border-bottom:1px solid #C5C5D4;
`;

export const TableWrap = styled(TableWrapper)`
.ant-table-content{
  border:none;
}
.ant-table-tbody > tr > td:nth-child(2) span{
  color:rgba(78,28,149,1);
}

.ant-table-thead th:nth-child(3){
min-width:110px ;
}
.ant-table{
  overflow:auto;
 max-height:400px;
   &::-webkit-scrollbar {
  width: 4px;
  height:4px;
  margin-top:20px
}
&::-webkit-scrollbar-track {
  box-shadow: inset 0 0 5px rgba(226, 226, 238, 1);
  border-radius: 10px;
}


&::-webkit-scrollbar-thumb {
  background: rgba(151, 146, 227, 1);
  border-radius: 10px;
}


&::-webkit-scrollbar-thumb:hover {
  background: rgba(151, 146, 227, 1);
}
}
.ant-table-body{
  
}
`;

export const InputField = styled(Input)`
font-size: 14px;
  font-family: Poppins;
  width: 100%;
  padding: 11px 12px;
  border: 1px solid #BDBDD3!important;
  background: transparent;
  border-radius: 8px;
  color: #17082D;
  font-weight: 500;
  line-height: 22px;
  
`;


export const SelectSearch = styled(Select)`
 
.ant-select-selector{
      border: 1px solid #BDBDD3 !important;
    border-radius: 8px !important;
   width:320px !important;
    height: auto;
    line-height: 1.5;
    height: 32px;
    display: flex;
    align-items: center;
}
 .ant-select-arrow {
        top: 30%;
    margin-top: 0px;
    font-size: 10px;
    color: #000;
    right: 10px;
}
.anticon.anticon-down{
        background:url(${SearchIcon}) no-repeat;
        display: block;
    width: 14px;
    height: 17px;
    }
    .anticon.anticon-down svg{
        display:none;
    } 


.ant-select-selection-placeholder{
	    color: #7B7B97;
    font-weight: 400;
}

`