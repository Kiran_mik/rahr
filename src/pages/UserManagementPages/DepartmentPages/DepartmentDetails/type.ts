

export interface DepartmentInformationInterface {
    department_name?:string;
    manager_user?:any
}

export interface StaffMembersInterface {

    userObject?:any;
    role?: string,
    email?: string,
}


export interface DepartmentInfoProps {
    id?: any;
  }
  