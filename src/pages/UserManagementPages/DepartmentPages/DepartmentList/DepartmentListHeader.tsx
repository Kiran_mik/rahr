import { Secondary } from "@components/Buttons";
import { SearchBar } from "@components/SerachBar";
import {
  InputAndButton,
  NavigateButton
} from "@pages/UserManagementPages/RAHStaffListPage/style";
import React from "react";
interface DepartmentListHeaderProps {
  onChange: (e: any) => void;
  redirect: () => void;
}
function DepartmentListHeader(props: DepartmentListHeaderProps) {
  const { onChange, redirect } = props;
  return (
    <>
      <InputAndButton>
        <SearchBar
          className="SearchInput"
          placeholder="Search by department name, manager or location"
          onChange={onChange}
        />
        <NavigateButton onClick={redirect}>
          <Secondary text="+ Add new department" />
        </NavigateButton>
      </InputAndButton>
    </>
  );
}

export default DepartmentListHeader;
