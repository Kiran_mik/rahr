import React, { useState, useEffect } from "react";

import { TableWrapper } from "@pages/UserManagementPages/RAHStaffListPage/style";
import { navigate } from "@reach/router";
import { getColumns } from "./columns";
import DataTable from "@components/DataTable/DataTable";
import { DepartmentItems, getDepartmentListArray } from "./helper";
import DepartmentListHeader from "./DepartmentListHeader";
import { rowSelection } from "./helper";

const DepartmentTableList = () => {
  const [selectionType, setSelectionType] = useState<"checkbox">("checkbox");
  const [page, setPage] = useState<number>(1);
  const [dataSource, setDataSource] = useState<Array<DepartmentItems>>([]);
  const [loading, setLoading] = useState(false);
  const [filter, setFilter] = useState("");
  const [currentSortOrder, setCurrentSortOrder] = useState({
    key: "",
    order: "asc"
  });
  useEffect(() => {
    getData(page, filter, currentSortOrder.key, currentSortOrder.order);
  }, [filter]);

  const onChange = (e: { target: { value: any } }) => {
    const currValue = e.target.value;
    setFilter(currValue);
  };

  const redirect = () => {
    navigate("/usermanagement/new-department");
  };

  const getData = async (
    page: number,
    values?: string,
    sortKey?: string,
    sortOrder?: string,
    hideLoader = false
  ) => {
    setLoading(!hideLoader && true);
    const res = (await getDepartmentListArray(
      page,
      values,
      sortKey || "name",
      sortOrder || "asc"
    )) as {
      list: Array<DepartmentItems>;
      lastPage: number;
    };
    setDataSource([]);
    setLoading(false);
    setDataSource(res.list.length ? res.list : dataSource);
  };

  const handlePageChange = async () => {
    const res = (await getDepartmentListArray(
      page + 1,
      filter,
      currentSortOrder.key,
      currentSortOrder.order
    )) as {
      list: Array<DepartmentItems>;
      lastPage: number;
    };
    setDataSource([...dataSource, ...res.list]);
    setPage(page => page + 1);
  };

  const onSort = async (key: string) => {
    const sortOrder =
      currentSortOrder.key === key || !currentSortOrder.key
        ? currentSortOrder.order === "asc"
          ? "desc"
          : "asc"
        : "asc";

    setLoading(true);
    const res = (await getDepartmentListArray(1, filter, key, sortOrder)) as {
      list: Array<DepartmentItems>;
      lastPage: number;
    };
    setDataSource([]);
    setPage(1);
    setLoading(false);
    setDataSource(res.list.length ? res.list : dataSource);
    setCurrentSortOrder({ key, order: sortOrder });
  };

  return (
    <>
      <DepartmentListHeader onChange={onChange} redirect={redirect} />
      <TableWrapper>
        <DataTable
          loading={loading}
          rowSelection={{
            type: selectionType,
            ...rowSelection
          }}
          columns={getColumns(onSort)}
          //@ts-ignore
          dataSource={dataSource || []}
          pagination={false}
          infinity={true}
          //@ts-ignore
          onFetch={() => handlePageChange()}
        />
      </TableWrapper>
    </>
  );
};
export default DepartmentTableList;
