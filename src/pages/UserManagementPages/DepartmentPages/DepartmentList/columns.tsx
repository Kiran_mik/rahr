import React from "react";
import { Avatar, Dropdown, Menu } from "antd";
import { DropdownStyle } from "./style";
import { Link } from "@reach/router";
import { DemoImage, ThreeDots } from "@assets/index";

export const menu = (department_id: number) => (
  <Menu className="MenuItems">
    <Menu.Item key="0">
      <Link
        style={{ color: "#17082d" }}
        to={`/usermanagement/department-detail/${department_id}`}
      >
        Update department
      </Link>
    </Menu.Item>
    <Menu.Item key="1">Deactivate department</Menu.Item>
  </Menu>
);

export const getColumns = (sorterFn: (key: string) => void) => {
  return [
    {
      title: "Department Name",
      onHeaderCell: (column: any) => {
        return {
          onClick: () => {
            sorterFn("department_name");
          }
        };
      },
      dataIndex: "departmentNameAndId",
      key: "departmentNameAndId",
      sorter: (a: any, b: any) => {},
      render: (row: { department_name: string; department_id: number }) => {
        return (
          <Link to={`/usermanagement/department-detail/${row.department_id}`}>
            <div style={{ fontWeight: 500, color: "#4E1C95" }}>
              <span>
                {row.department_name ? row.department_name : "Unknown"}
              </span>
            </div>
          </Link>
        );
      }
    },
    {
      title: "Manager",
      onHeaderCell: (column: any) => {
        return {
          onClick: () => {
            sorterFn("manager");
          }
        };
      },
      dataIndex: "userObject",
      key: "userObject",
      sorter: (a: any, b: any) => {},
      render: (row: {
        full_name: string;
        profile_photo: string;
        user_id: number;
      }) => {
        return (
          <div style={{ display: "flex", alignItems: "center" }}>
            <Avatar src={row.profile_photo} alt={DemoImage} />
            <div style={{ marginLeft: 8 }}>
              <span>{row && row.full_name ? row.full_name : ""}</span>
            </div>
          </div>
        );
      }
    },
    {
      title: "Staff Members",
      onHeaderCell: (column: any) => {
        return {
          onClick: () => {
            sorterFn("staffCount");
          }
        };
      },
      dataIndex: "department_staff_members_count",
      key: "department_staff_members_count",
      sorter: (a: any, b: any) => {}
    },
    {
      title: "Number of roles",
      onHeaderCell: (column: any) => {
        return {
          onClick: () => {
            sorterFn("roleCount");
          }
        };
      },
      dataIndex: "department_number_of_roles_count",
      key: "department_number_of_roles_count",
      sorter: (a: any, b: any) => {}
    },
    {
      title: "",
      dataIndex: "departmentNameAndId",
      key: "departmentNameAndId",
      render: (row: { department_id: number; index: number }) => {
        return (
          <DropdownStyle
            key={row.department_id + row.index}
            style={{ cursor: "pointer" }}
          >
            <Dropdown overlay={menu(row.department_id)} trigger={["click"]}>
              <img
                src={ThreeDots}
                alt={ThreeDots}
                onClick={e => e.preventDefault()}
              />
            </Dropdown>
          </DropdownStyle>
        );
      }
    }
  ];
};
