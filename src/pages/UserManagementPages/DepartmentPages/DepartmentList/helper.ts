import { GetDepartmentList } from "@services/GetStaffList";

export interface DataType {
  key: React.Key;
  user_id?: number;
  full_name?: string;
  department_id?: number;
  department_name?: string;
  department_staff_members_count?: number;
  department_number_of_roles_count?: number;
  userObject?: any;
  departmentNameAndId?: any;
}

 export const rowSelection = {
  onChange: (selectedRowKeys: React.Key[], selectedRows: DataType[]) => {},
  getCheckboxProps: (record: DataType) => ({
    disabled: record.department_name === "Disabled User", // Column configuration not to be checked
    name: record.department_name
  })
};



export interface DepartmentItems {
  key: React.Key;
  user_id?: number;
  full_name?: string;
  department_id?: number;
  department_name?: string;
  department_staff_members_count?: number;
  department_number_of_roles_count?: number;
  userObject?: any;
  departmentNameAndId?: any;
  profile_photo?: string;
}

export const getDepartmentListArray = async (
  page: number,
  values?: any,
  sortKey?: string,
  sortOrder?: string
) => {
  const res: any = await GetDepartmentList(
    page,
    values,
    sortKey || "department_name",
    sortOrder || "asc"
  );
  if (res.status === 200) {
    const departmentList = transformDeptListData(res.data.data.data || []);
    return { list: departmentList, lastPage: res.data.data.lastPage };
  }
  return [];
};

export const transformDeptListData = (
  departmentList: Array<DepartmentItems>
) => {
  return departmentList.map((row: DepartmentItems, index: number) => ({
		user_id: row.user_id,
		full_name: row.full_name,
		department_id: row.department_id,
		department_name: row.department_name,
		department_staff_members_count: row.department_staff_members_count,
		department_number_of_roles_count: row.department_number_of_roles_count,
		userObject: {
			id: row.user_id,
			full_name: row.full_name,
			profile_photo: row.profile_photo,
		},
		departmentNameAndId: {
			department_name: row.department_name,
			department_id: row.department_id,
		},
		key: row.department_id,
  }))
};
