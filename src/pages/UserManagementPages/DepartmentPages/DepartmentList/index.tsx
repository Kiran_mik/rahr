import React, { FC } from "react";
import { RouteComponentProps } from "@reach/router";
import StaffHeader from "@components/StaffHeader";
import { H3 } from "@components/Typography/Typography";
import DepartmentTableList from "./DepartmentListTable";
import DashboardLayout from "@pages/UserManagementPages/StaffMembers/DashboardLayout";

export interface DepartmentProps extends RouteComponentProps {
  title?: string;
}

const DepartmentList: FC<DepartmentProps> = () => {
  return (
    <div>
      <DashboardLayout>
        <StaffHeader>
          <H3 text="Departments" />
        </StaffHeader>
        <DepartmentTableList />
      </DashboardLayout>
    </div>
  );
};
export default DepartmentList;
