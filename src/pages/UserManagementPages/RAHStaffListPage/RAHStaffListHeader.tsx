import { Secondary } from "@components/Buttons";
import { SearchBar } from "@components/SerachBar";
import React from "react";
import { InputAndButton, NavigateButton } from "./style";
interface RAHStaffListHeaderProps {
  onChange: (e: any) => void;
  redirect: () => void;
}

function RAHStaffListHeader(props: RAHStaffListHeaderProps) {
  const { onChange, redirect } = props;
  return (
    <InputAndButton>
      <SearchBar
        className="SearchInput"
        placeholder="Search by staff name, email, branch..."
        onChange={onChange}
      />
      <NavigateButton onClick={redirect}>
        <Secondary text="+ Add new staff" />
      </NavigateButton>
    </InputAndButton>
  );
}

export default RAHStaffListHeader;
