import React, { useState, useEffect, useRef } from "react";
import "antd/dist/antd.css";
import { VariableSizeGrid as Grid } from "react-window";
import ResizeObserver from "rc-resize-observer";
import classNames from "classnames";
import { Table } from "antd";
import { TableWrapper } from "./style";

interface DataType {
  key: React.Key;
  profile_photo?: string;
  userObject?: any;
  full_name?: string;
  last_name?: string;
  branch_name?: string;
  department_name?: string;
  role_name?: string;
}

export default function VirtualTable(props: any) {
  const { columns, scroll, onPageChange, page, lastPage, dataSource } = props;
  const [tableWidth, setTableWidth] = useState(0);

  const [selectionType, setSelectionType] = useState<"checkbox">("checkbox");
  function onChange(pagination: any, filters: any, sorter: any, extra: any) {}

  const rowSelection = {
    onChange: (selectedRowKeys: React.Key[], selectedRows: DataType[]) => {},
    getCheckboxProps: (record: DataType) => ({
      disabled: record.full_name === "Disabled User", // Column configuration not to be checked
      name: record.full_name
    })
  };

  // @ts-ignore
  const widthColumnCount = columns.filter(({ width }) => !width).length;
  // @ts-ignore
  const mergedColumns = columns.map(column => {
    if (column.width) {
      return column;
    }

    return { ...column, width: Math.floor(tableWidth / widthColumnCount) };
  });
  const gridRef = useRef();
  const [connectObject] = useState(() => {
    const obj = {};
    Object.defineProperty(obj, "scrollLeft", {
      get: () => null,
      set: scrollLeft => {
        if (gridRef.current) {
          // @ts-ignore
          gridRef.current.scrollTo({
            scrollLeft
          });
        }
      }
    });
    return obj;
  });

  const resetVirtualGrid = () => {
    // @ts-ignore
    gridRef.current.resetAfterIndices({
      columnIndex: 0,
      shouldForceUpdate: true
    });
  };

  useEffect(() => resetVirtualGrid, [tableWidth]);

  const renderVirtualList = (
    rawData: string | any[],
    { scrollbarSize, ref, onScroll }: any
  ) => {
    ref.current = connectObject;
    const totalHeight = rawData.length * 54;
    return (
      <Grid
        // @ts-ignore
        ref={gridRef}
        // rowSelection={{
        //   type: selectionType,
        //   ...rowSelection
        // }}
        className="virtual-grid"
        columnCount={mergedColumns.length}
        columnWidth={index => {
          const { width } = mergedColumns[index];
          return totalHeight > scroll.y && index === mergedColumns.length - 1
            ? width - scrollbarSize - 1
            : width;
        }}
        height={scroll.y}
        rowCount={rawData.length}
        rowHeight={() => 54}
        width={tableWidth}
        onScroll={({ scrollTop }) => {
          const scrollHeight = totalHeight - 500;
          const maxScrollHeight = totalHeight - 480;
          if (
            scrollTop >= scrollHeight &&
            scrollTop < maxScrollHeight &&
            page < lastPage
          ) {
            onPageChange(page + 1);
          }
        }}
      >
        {({ columnIndex, rowIndex, style }) => (
          <div
            className={classNames("virtual-table-cell", {
              "virtual-table-cell-last":
                columnIndex === mergedColumns.length - 1
            })}
            style={style}
          >
            {rawData[rowIndex][mergedColumns[columnIndex].dataIndex]}
          </div>
        )}
      </Grid>
    );
  };

  return (
    <ResizeObserver
      onResize={({ width }) => {
        setTableWidth(width);
      }}
    >
      <TableWrapper>
        <Table
          {...props}
          rowSelection={{
            type: selectionType,
            ...rowSelection
          }}
          onChange={onChange}
          className="virtual-table"
          columns={mergedColumns}
          pagination={false}
          // components={{
          //   body: renderVirtualList
          // }}
          dataSource={dataSource}
        />
      </TableWrapper>
    </ResizeObserver>
  );
} // Usage
