import React from "react";

import { Avatar, Dropdown, Menu } from "antd";
import { Link } from "@reach/router";
import { ThreeDots } from "@assets/index";
import demoImage from "@assets/demoImage.png";

import { DropdownStyle } from "./style";

export const menu = (userId: number) => (
  <Menu className="MenuItems" style={{ borderRadius: "8px" }}>
    <Menu.Item key="0" style={{ padding: "5px 12px" }}>
      <Link
        style={{ color: "#17082d" }}
        to={`/usermanagement/edit-staff/${userId}`}
      >
        Update profile
      </Link>
    </Menu.Item>
    <Menu.Item key="1">Terminate user</Menu.Item>
  </Menu>
);

export const getColumns = (sorterFn: (key: string) => void) => {
  return [
    {
      title: "Name",
      onHeaderCell: (column: any) => {
        return {
          onClick: () => {
            sorterFn("name");
          }
        };
      },
      sorter: (a: any, b: any) => {},
      dataIndex: "userObject",
      key: "userObject",

      render: (row: {
        full_name: string;
        profile_photo: typeof Image;
        id: number;
        index: number;
      }) => {
        return (
          <div style={{ display: "flex", alignItems: "center" }} key={row.id}>
            <Avatar
              src={row.profile_photo ? row.profile_photo : demoImage}
              alt={demoImage}
            />
            <Link to={`/usermanagement/staff-detail/${row.id}`}>
              <div style={{ marginLeft: 8, fontWeight: 500, color: "#4E1C95" }}>
                <span className="NameTitle">{row.full_name}</span>
              </div>
            </Link>
          </div>
        );
      }
    },
    {
      title: "Branch",
      dataIndex: "branch_name",
      key: "branch_name",
      sorter: (a: any, b: any) => {},

      onHeaderCell: (column: any) => {
        return {
          onClick: () => {
            sorterFn("branch");
          }
        };
      }
    },
    {
      title: "Department",
      dataIndex: "department_name",
      key: "department_name",
      onHeaderCell: (column: any) => {
        return {
          onClick: () => {
            sorterFn("department");
          }
        };
      },
      sorter: (a: any, b: any) => {}
    },
    {
      title: "Role",
      dataIndex: "role_name",
      key: "role_name",
      onHeaderCell: (column: any) => {
        return {
          onClick: () => {
            sorterFn("role_name");
          }
        };
      },
      sorter: (a: any, b: any) => {}
    },
    {
      title: "",
      dataIndex: "userObject_menu",
      key: "userObject_menu",
      render: (row: { id: number; index: number }) => {
        return (
          <DropdownStyle
            key={row.id + "" + row.index}
            style={{ cursor: "pointer" }}
          >
            <Dropdown overlay={menu(row.id)} trigger={["click"]}>
              <img
                src={ThreeDots}
                alt={ThreeDots}
                onClick={e => e.preventDefault()}
              />
            </Dropdown>
          </DropdownStyle>
        );
      }
    }
  ];
};
