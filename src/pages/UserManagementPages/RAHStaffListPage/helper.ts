import { GetStaffList } from '@services/GetStaffList';
export interface StaffItem{
    id: number;
    userObject: any;
    full_name: string;
    branch_name: string;
    department_name: String;
    role_name: string;
    key: any;
    profile_photo: string;
    email: string;
}

export interface DataType {
  key: React.Key;
  profile_photo?: string;
  userObject?: any;
  full_name?: string;
  branch_name?: string;
  department_name?: string;
  role_name?: string;
}

export const rowSelection = {
  onChange: (selectedRowKeys: React.Key[], selectedRows: DataType[]) => {},
  getCheckboxProps: (record: DataType) => ({
    disabled: record.full_name === "Disabled User", // Column configuration not to be checked
    name: record.full_name
  })
};

export const getStaffListArray=async(page:number, values:any,sortKey:string,sortOrder:string)=>{
  const res: any = await GetStaffList(page, values,sortKey || 'name',sortOrder||'asc');
  if (res.status === 200) {
    const staffList = transformStaffListData(res.data.data.data || [])
    return {list:staffList,lastPage:res.data.data.last_page};
  }
  return []
}

export const transformStaffListData=(staffList:Array<StaffItem>)=>{
return staffList.map((row:StaffItem,index:number)=>({
    id: row.id,
    index: index,
    full_name: row.full_name,
    userObject: {
      id: row.id,
      full_name: row.full_name,
      profile_photo: row.profile_photo
    },
    branch_name: row.branch_name,
    department_name: row.department_name,
    role_name: row.role_name,
    key: row.id,
    profile_photo: row.profile_photo,
    email: row.email,
    userObject_menu:row,
  }))
}