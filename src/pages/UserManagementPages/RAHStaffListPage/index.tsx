import React, { useState, useEffect, useCallback } from "react";
import DashboardLayout from "@pages/UserManagementPages/StaffMembers/DashboardLayout";
import StaffHeader from "@components/StaffHeader";
import { H3 } from "@components/Typography/Typography";
import DataTable from "@components/DataTable/DataTable";
import RAHStaffListHeader from "./RAHStaffListHeader";
import { InputAndButton, TableWrapper, NavigateButton } from "./style";
import { navigate } from "@reach/router";
import { getColumns } from "./columns";
import {
  getStaffListArray,
  StaffItem,
  transformStaffListData,
  rowSelection,
  DataType
} from "./helper";
import Table from "rc-table/lib/Table";

const RAHStaffList = (props: { path: string }) => {
  const [selectionType, setSelectionType] = useState < "checkbox" > ("checkbox");
  const [page, setPage] = useState < number > (1);
  const [dataSource, setDataSource] = useState < Array < StaffItem >> ([]);
  const [loading, setloading] = useState(false);
  const [filter, setfilter] = useState("");
  const [currentSortOrder, setCurrentSortOrder] = useState({
    key: "",
    order: "asc"
  });
  useEffect(() => {
    getData(page, filter, currentSortOrder.key, currentSortOrder.order);
  }, [filter]);

  const onChange = (e: { target: { value: any } }) => {
    const currValue = e.target.value;
    setfilter(currValue);
  };

  const redirect = () => {
    navigate("/usermanagement/new-staff");
  };

  const getData = async (
    page: number,
    values?: string,
    sortKey?: string,
    sortOrder?: string,
    hideLoader = false
  ) => {
    setloading(!hideLoader && true);
    const res = (await getStaffListArray(
      page,
      values,
      sortKey || "name",
      sortOrder || "asc"
    )) as {
      list: Array<StaffItem>;
      lastPage: number;
    };
    setDataSource([]);
    setloading(false);
    setDataSource(res.list && res.list.length ? res.list : []);
  };

  const handlePageChange = async () => {
    const res = (await getStaffListArray(
      page + 1,
      filter,
      currentSortOrder.key,
      currentSortOrder.order
    )) as {
      list: Array<StaffItem>;
      lastPage: number;
    };
    setDataSource([...dataSource, ...res.list]);
    setPage(page => page + 1);
  };

  const onSort = async (key: string) => {
    const sortOrder =
      currentSortOrder.key === key || !currentSortOrder.key
        ? currentSortOrder.order === "asc"
          ? "desc"
          : "asc"
        : "asc";

    setloading(true);
    const res = (await getStaffListArray(1, filter, key, sortOrder)) as {
      list: Array<StaffItem>;
      lastPage: number;
    };
    setDataSource([]);
    setPage(1);

    setloading(false);
    setDataSource(res.list && res.list.length ? res.list : dataSource);
    setCurrentSortOrder({ key, order: sortOrder });
  };

  return (
    <DashboardLayout>
      <StaffHeader>
        <H3 text="RAH Staff" />
      </StaffHeader>
      <RAHStaffListHeader onChange={onChange} redirect={redirect} />
      <TableWrapper test-Id="TableWrapperId">
        <DataTable
          loading={loading}
          rowSelection={{
            type: selectionType,
            ...rowSelection
          }}
          columns={getColumns(onSort)}
          //@ts-ignore
          dataSource={dataSource || []}
          pagination={false}
          infinity={true}
          //@ts-ignore
          onFetch={() => handlePageChange()}
        />
      </TableWrapper>
    </DashboardLayout>
  );
};
export default RAHStaffList;
