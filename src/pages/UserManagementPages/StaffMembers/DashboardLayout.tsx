import React, { FC } from "react";

import { Container } from "./style";

import { Layout } from "antd";
import { Content } from "antd/lib/layout/layout";

export interface DashboardLayoutProps {
  children?: React.ReactNode;
}

const DashboardLayout: FC<DashboardLayoutProps> = ({ children }) => {
  return (
    <Container>
      <Layout
        style={{
          flex: "auto",
          padding: "32px 58px",
          maxHeight: "100vh",
          minHeight: "100vh",
          overflow: "auto",
        }}
      >
        <Content className="site-layout-background">{children}</Content>
      </Layout>
    </Container>
  );
};

export default DashboardLayout;
