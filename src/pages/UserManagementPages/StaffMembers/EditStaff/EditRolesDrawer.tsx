import DrawerView from "@components/Drawer";
import React, { FC, useState, useEffect } from "react";
import { Card, Checkbox, message } from "antd";
import { GetStaffPermission, UpdateRoleDetails } from "@services/UserService";
import Accordion from "@components/Accordion";
import {
  Container,
  MainCard,
  DrawerFooter,
  AccordionContainer,
  DrwaerInnerContent,
  DrwaerLabel,
  ModalFooter
} from "./style";
import {
  DrawerPrimaryBtn,
  Transparent,
  DrawerBorderBtn
} from "@components/Buttons/index";
import ModalComponent from "@components/Modal/Modal";
import { Flex } from "@components/Flex";
import { DrawerIcon } from "@assets/index";
import { H5Typography } from "@components/Typography/Typography";
import { ExclamationCircle } from "@assets/index";

import { getRoleValue } from "./helper";
import { InputField } from "@components/Input/Input";
interface DrawerProps {
  onClose: () => void;
  visible: boolean;
  title?: string;
  className?: string;
  children?: React.ReactNode;
  tableValues?: any;
  roleId?: any;
  username?: any;
  profilePhoto?: any;
  roleNames?: any;
  onRoleUpdate?: any;
}

const EditRolesDrawer: FC<DrawerProps> = ({
  onClose,
  visible,
  children,
  title,
  className,
  tableValues,
  roleId,
  username,
  profilePhoto,
  roleNames,
  onRoleUpdate
}) => {
  const [roleData, setRoleData] = useState<any>([]);
  const [permissions, setPermissions] = useState<any>(getRoleValue(roleData));

  const [modalvalue, setModalValue] = useState<boolean>(false);
  const [roleName, setroleName] = useState<string>("");

  useEffect(() => {
    setroleName(roleNames);

    if (visible) {
      (async () => {
        await getRolePermissionDetails(roleId);
      })();
    }
  }, [roleId, visible, roleNames]);
  const getRolePermissionDetails = async (role_id: any) => {
    if (role_id.toString().length > 0) {
      const resRole: any = await GetStaffPermission(role_id);
      if (resRole.status === 200) {
        // @ts-ignore
        setRoleData(resRole.data.data);
        setPermissions(getRoleValue(resRole.data.data));
        return resRole;
      } else {
        message.error("something Went Wrong");
      }
    } else {
      setRoleData([...tableValues]);
    }
  };

  function onChange(e: any) {
    let id = Number(String(e.target.name).split("-")[1]) - 1;
    let value = String(e.target.name).split("-")[0];
    if (value == "view") permissions[id].view = e.target.checked;
    if (value == "edit") permissions[id].edit = e.target.checked;
    if (value == "delete") permissions[id].delete = e.target.checked;
    if (value == "create") permissions[id].create = e.target.checked;
    setPermissions([...permissions]);
  }
  // let perms = JSON.parse(JSON.stringify(tableValues));

  const onCheck = async () => {
    var res: any = await UpdateRoleDetails(roleId, roleName, permissions);

    if (res && res.success) {
      message.success(res.message);
      onRoleUpdate();
      closeModal();
      onClose();
    } else {
      message.error(res.error.message);
      closeModal();
    }
  };
  const restoreodefault = async () => {
    await getRolePermissionDetails(roleId);
  };
  const showModal = () => {
    setModalValue(true);
  };
  const closeModal = () => {
    setModalValue(false);
  };

  return (
    <Container>
      <DrawerView
        onClose={onClose}
        visible={visible}
        title={`Edit ${username !== undefined ? username : "Role"} permission`}
      >
        <DrwaerInnerContent>
          <DrwaerLabel>
            Role Name
            <span>*</span>
          </DrwaerLabel>

          <InputField
            value={roleName}
            placeholder="Branch admin"
            onChange={(e: any) => {
              setroleName(e.target.value);
            }}
            name={"role-name"}
            id={"role-name"}
          />
          <AccordionContainer>
            <Accordion title="Role Permissions">
              <MainCard>
                <Card title="" bordered={false}>
                  {permissions &&
                    permissions.map((module: any) => {
                      return (
                        <Accordion title={module.heading}>
                          <Checkbox
                            name={"view-" + module.module_name_id}
                            checked={module.view}
                            onChange={onChange}
                          >
                            View
                          </Checkbox>
                          <Checkbox
                            name={"edit-" + module.module_name_id}
                            checked={module.edit}
                            onChange={onChange}
                          >
                            Edit
                          </Checkbox>
                          <Checkbox
                            name={"create-" + module.module_name_id}
                            checked={module.create}
                            onChange={onChange}
                          >
                            Create
                          </Checkbox>
                          <Checkbox
                            name={"delete-" + module.module_name_id}
                            checked={module.delete}
                            onChange={onChange}
                          >
                            Delete
                          </Checkbox>
                        </Accordion>
                      );
                    })}
                </Card>
              </MainCard>
            </Accordion>
          </AccordionContainer>
        </DrwaerInnerContent>
        <DrawerFooter>
          <DrawerPrimaryBtn
            testId="SubmitAndproceed"
            text="Submit and Proceed"
            onClick={() => {
              {
                window.location.pathname.split("/")[2] === "new-department" ||
                window.location.pathname.split("/")[2] === "department-detail"
                  ? onCheck()
                  : showModal();
              }
            }}
          />

          <DrawerBorderBtn
            onClick={() => {
              restoreodefault();
            }}
            text="Restore to default"
          />
          <Transparent
            text="Cancel"
            onClick={() => {
              onClose();
            }}
          />
        </DrawerFooter>
      </DrawerView>
      {window.location.pathname.split("/")[2] === "new-department" ? (
        ""
      ) : (
        <>
          {" "}
          {modalvalue === true ? (
            <ModalComponent
              visible={modalvalue}
              onOK={closeModal}
              onCancel={closeModal}
            >
              <Flex>
                <div>
                  <img
                    src={ExclamationCircle}
                    alt="ExclamationCircle"
                    style={{ marginRight: "17px" }}
                  />
                </div>
                <div>
                  <H5Typography>
                    You are about to override permissions and settings
                  </H5Typography>
                  <div style={{ display: "flex", margin: "16px 0px 26px 0px" }}>
                    <img
                      src={profilePhoto ? profilePhoto : DrawerIcon}
                      alt="backgroundimg"
                      style={{
                        borderRadius: "50%",
                        marginRight: "12px",
                        height: "40px"
                      }}
                    />
                    <div style={{ textAlign: "left" }}>
                      <span
                        style={{
                          lineHeight: "22px",
                          fontSize: "14px",
                          fontFamily: "Poppins",
                          fontWeight: 600
                        }}
                      >
                        {username}
                      </span>
                      <br />
                      <span
                        style={{
                          fontFamily: "Poppins",
                          color: "#7B7B97",
                          fontSize: "12px"
                        }}
                      >
                        {roleName}
                      </span>
                    </div>
                  </div>
                  <p
                    className="modalpara"
                    style={{
                      color: "#17082D",
                      marginBottom: "24px"
                    }}
                  >
                    Are you sure you want to proceed?
                  </p>
                  <ModalFooter>
                    <DrawerPrimaryBtn text="Proceed anyway" onClick={onCheck} />
                    <Transparent text="Cancel" onClick={closeModal} />
                  </ModalFooter>
                </div>
              </Flex>
            </ModalComponent>
          ) : (
            ""
          )}
        </>
      )}
    </Container>
  );
};

export default EditRolesDrawer;
