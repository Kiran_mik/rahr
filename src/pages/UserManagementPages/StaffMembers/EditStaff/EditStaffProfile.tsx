import React, { FC, useState, useEffect } from "react";
import { navigate, Link, RouteComponentProps } from "@reach/router";
import StaffHeader from "@components/StaffHeader";
import { Breadcrumb, message } from "antd";
import { H3 } from "@components/Typography/Typography";
import Loader from "@components/Loader";
import { BreadItem } from "./style";
import { GetServiceData } from "@services/UserService";
import DashboardLayout from "@pages/UserManagementPages/StaffMembers/DashboardLayout";
import EditForm from "./EditStaffProfileForm";
export interface EditStaffProps extends RouteComponentProps {
  empId?: any;
}
const EditStaffProfile: FC<EditStaffProps> = ({ empId }) => {
  const [UserData, setUserData] = useState < any > ();
  const [loading, setLoading] = useState < boolean > (false);

  const getProfileDetails = async () => {
    setLoading(true);
    const res: any = await GetServiceData(empId);
    if (res.status === 200) {
      setLoading(false);
      setUserData(res.data.data);
    } else {
      setLoading(false);
      message.error(res.error.message);
    }
    return res;
  };

  useEffect(() => {
    getProfileDetails();
  }, []);
  return (
    <DashboardLayout>
      <StaffHeader>
        <div>
          <Breadcrumb separator=">">
            <Link to="/usermanagement/staff-list" data-testid="gouserList">
              <BreadItem data-testid="breadcrumb-user" firstNav={true}>
                List of staffs
              </BreadItem>
            </Link>
            <Link
              to={`/usermanagement/staff-detail/${empId}`}
              data-testid="gouserList"
            >
              <BreadItem data-testid="breadcrumb-user" firstNav={true}>
                Staff Profile
              </BreadItem>
            </Link>
            <BreadItem firstNav={false}>
              {UserData && UserData.username}
            </BreadItem>
          </Breadcrumb>
          <H3 text="Edit Staff" />
        </div>
      </StaffHeader>
      {UserData && <EditForm userdata={UserData} empId={empId} />}
    </DashboardLayout>
  );
};

export default EditStaffProfile;
