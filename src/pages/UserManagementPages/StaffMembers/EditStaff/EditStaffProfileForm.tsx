import React, { FC, useEffect, useState } from "react";
import { Form, Formik, validateYupSchema } from "formik";
import { DatePicker } from "antd";

import {
  ContainerBox,
  FlexBox,
  InnerContent,
  Main,
  FormWrapper,
  FormTitle,
  FormSubTitle,
  DateContent,
  CustomSelect
} from "./style";
import { ConnectedFocusError } from "focus-formik-error";

import { FormInputFiled, InputField } from "@components/Input/Input";
import { FormLableTypography, H3 } from "@components/Typography/Typography";
import { Divider, Input, Radio, Select, Row, Col } from "antd";
import DatePickerComponent from "@components/DatePicker";
import { formInitValues, validationSchema } from "./helper";
import SelectInput from "@components/Select";
import { FileInput } from "@components/FileInput";
import FormItem from "antd/lib/form/FormItem";
import TextAreaInput from "@components/Input/TextArea";
import {
  GetSupportDataStaff,
  UpdateStaffDetails
} from "@services/UserManagement/NewStaffForm";
import axios from "axios";
import moment from "moment";
import { Link, navigate } from "@reach/router";
import { Primary } from "@components/Buttons";

interface Formprops {
  userdata: any;
  empId: any;
}
const { Option } = Select;

const EditStaffProfileForm: FC<Formprops> = ({ userdata, empId }) => {
  const [imagename, setImagename] = useState("");
  const [showerror, setShowError] = useState<boolean>(false);
  const [showemailerror, setShowEmailError] = useState<boolean>(false);
  const [showphoneerror, setShowPhoneError] = useState<boolean>(false);
  const [changeImage, setChangeimage] = useState<boolean>(false);
  const [formerror, setFormError] = useState<any>("");
  const [branchLists, setBranchLists] = useState<
    { value: string; key: string }[]
  >([]);
  const [deptLists, setDeptLists] = useState<{ value: string; key: string }[]>(
    []
  );
  const [posLists, setPosLists] = useState<{ value: string; key: string }[]>(
    []
  );
  const [reportLists, setReportLists] = useState<
    { value: string; key: string }[]
  >([]);
  const photoUpload = async (values: any) => {
    let formData = new FormData();

    formData.append("imageFile", values.profile_photo);
    axios
      .post(
        "https://7ngsa15qzk.execute-api.ca-central-1.amazonaws.com/api/v1/image/upload",
        formData
      )
      .then(async (res: any) => {
        if (res.data.data.url) {
          sessionStorage.setItem("Upload", res.data.data.url);
          values.profile_photo = res.data.data.url;

          SubmitForm(values);
          setChangeimage(false);
        }
      });
  };
  const SupportData = async () => {
    const res: any = await GetSupportDataStaff();
    if (res) {
      let branches = [];
      for (let i = 0; i < res.data.data.branch_list.length; i++) {
        branches.push({
          value: res.data.data.branch_list[i].branch_name,

          key: res.data.data.branch_list[i].id
        });
      }
      setBranchLists([...branches]);
      let depts = [];

      for (let i = 0; i < res.data.data.department_list.length; i++) {
        depts.push({
          value: res.data.data.department_list[i].department_name,

          key: res.data.data.department_list[i].id
        });
      }
      setDeptLists([...depts]);
      let positions = [];
      for (let i = 0; i < res.data.data.position_list.length; i++) {
        positions.push({
          value: res.data.data.position_list[i].name,

          key: res.data.data.position_list[i].id
        });
      }
      setPosLists([...positions]);

      let report = [];
      for (let i = 0; i < res.data.data.report_to_list.length; i++) {
        report.push({
          value:
            res.data.data.report_to_list[i].first_name +
            " " +
            res.data.data.report_to_list[i].last_name,

          key: res.data.data.report_to_list[i].id
        });
      }
      setReportLists([...report]);
    }
  };
  useEffect(() => {
    SupportData();
  }, []);
  const redirect = () => {
    navigate(`/usermanagement`);
  };
  const SubmitForm = async (values: any) => {
    var res: any = await UpdateStaffDetails(empId, values);
    values.id = userdata.id;
    if (res && res.success) {
      // message.success(res.message);
      navigate("/usermanagement/staff-list");
    } else {
      // message.error(res.error.message);
      setShowError(true);
      setShowEmailError(true);
      setShowPhoneError(true);
      setFormError(res.data.error.error);
    }
  };
  let photourl = userdata.profile_photo;
  var url = String(photourl)
    .split("profile/")[1]
    .substr(10);

  return (
    <FormWrapper>
      <FormTitle>{userdata && userdata.username}</FormTitle>
      <FormSubTitle>Profile details</FormSubTitle>
      <Formik
        initialValues={userdata}
        onSubmit={(values, actions) => {
          if (changeImage === true) {
            photoUpload(values);
          } else {
            SubmitForm(values);
          }
        }}
        validationSchema={validationSchema}
      >
        {props => {
          const {
            values,
            setFieldValue,
            isSubmitting,
            isValid,
            errors
          } = props;
          return (
            <Form>
              <ConnectedFocusError />
              <ContainerBox>
                <InnerContent>
                  <Main direction="row" justifyContent="space-around">
                    <FlexBox direction="column">
                      <FormLableTypography>
                        First Name <span>*</span>
                      </FormLableTypography>
                      <FormInputFiled
                        id="first_name"
                        name="first_name"
                        placeholder={userdata.first_name}
                        onChange={(e: any) => {
                          if (e.target.value.toString().length < 30) {
                            setFieldValue(
                              "first_name",
                              e.target.value
                                ? e.target.value
                                : userdata.first_name
                            );
                          }
                        }}
                        defaultValue={
                          props.values.first_name
                            ? props.values.first_name
                            : userdata.first_name
                        }
                        // value={
                        //   props.values.first_name
                        //     ? props.values.first_name
                        //     : userdata.first_name
                        // }
                      />
                    </FlexBox>
                    <FlexBox direction="column">
                      <FormLableTypography>
                        Last Name <span>*</span>
                      </FormLableTypography>
                      <FormInputFiled
                        id="last_name"
                        name="last_name"
                        placeholder={userdata.first_name}
                        onChange={(e: any) => {
                          if (e.target.value.toString().length < 30) {
                            setFieldValue("last_name", e.target.value);
                          }
                        }}
                        defaultValue={
                          props.values.last_name
                            ? props.values.last_name
                            : userdata.last_name
                        }
                      />
                    </FlexBox>
                  </Main>
                  <Main direction="row" justifyContent="space-around">
                    <FlexBox direction="column">
                      <FormLableTypography>
                        Middle Name <span>*</span>
                      </FormLableTypography>

                      <FormInputFiled
                        id="middle_name"
                        name="middle_name"
                        placeholder=""
                        onChange={(e: any) => {
                          if (e.target.value.toString().length < 30) {
                            setFieldValue("middle_name", e.target.value);
                          }
                        }}
                        error={props.errors.middle_name}
                        defaultValue={
                          props.values.middle_name
                            ? props.values.middle_name
                            : userdata.middle_name
                        }
                      />
                    </FlexBox>
                    <FlexBox direction="column">
                      <FormLableTypography>
                        Date of Birth <span>*</span>
                      </FormLableTypography>
                      <DateContent>
                        <DatePicker
                          name="dob"
                          id="dob"
                          defaultValue={
                            moment(userdata.dob)
                              ? moment(userdata.dob)
                              : props.values.dob
                          }
                          format="D MMMM, YYYY"
                          onChange={(value: any) => {
                            setFieldValue(
                              "dob",
                              moment(value).format("YYYY-MM-DD")
                            );
                          }}
                          disabledDate={d =>
                            !d ||
                            d.isAfter(moment().format("MM/DD/YYYY")) ||
                            d.isSameOrBefore("1960-01-01")
                          }
                        />
                      </DateContent>
                    </FlexBox>
                  </Main>
                  <Main direction="row" justifyContent="space-around">
                    <FlexBox direction="column">
                      <FormLableTypography>
                        RAH Email <span>*</span>
                      </FormLableTypography>
                      <FormInputFiled
                        id="rah_email"
                        name="rah_email"
                        placeholder="email@rah.ca"
                        onChange={(e: any) => {
                          if (e.target.value.toString().length < 35) {
                            setFieldValue("rah_email", e.target.value);
                          }
                          setShowError(false);
                        }}
                        // error={props.errors.rah_email}
                        defaultValue={
                          props.values.rah_email
                            ? props.values.rah_email
                            : userdata.rah_email
                        }
                      />
                      {showerror ? (
                        <p style={{ color: "red" }}>
                          {formerror && formerror.rah_email
                            ? formerror.rah_email.toString()
                            : ""}
                        </p>
                      ) : (
                        ""
                      )}
                    </FlexBox>
                    <FlexBox direction="column">
                      <FormLableTypography>
                        Personal Email <span>*</span>
                      </FormLableTypography>

                      <FormInputFiled
                        id="email"
                        name="email"
                        placeholder="sample@mail.ca"
                        onChange={(e: any) => {
                          if (e.target.value.toString().length < 35) {
                            setFieldValue("email", e.target.value);
                          }
                          setShowEmailError(false);
                        }}
                        error={props.errors.email}
                        defaultValue={
                          props.values.email
                            ? props.values.email
                            : userdata.email
                        }
                      />

                      {showemailerror ? (
                        <p style={{ color: "red" }}>
                          {formerror && formerror.email
                            ? formerror.email.toString()
                            : ""}
                        </p>
                      ) : (
                        ""
                      )}
                    </FlexBox>
                  </Main>
                  <Main direction="row" justifyContent="space-around">
                    <FlexBox direction="column">
                      <FormLableTypography>
                        Phone Number <span>*</span>
                      </FormLableTypography>

                      <FormInputFiled
                        id="phone_number"
                        name="phone_number"
                        placeholder="+1 ( _ _ _ ) _ _ _ _  - _ _ _ "
                        onChange={(e: any) => {
                          if (
                            e.target.value.toString().length < 11 &&
                            e.target.value.toString() != "0000000000"
                          ) {
                            setFieldValue("phone_number", e.target.value);
                          }

                          setShowPhoneError(false);
                        }}
                        defaultValue={
                          props.values.phone_number
                            ? props.values.phone_number
                            : userdata.phone_number
                        }
                        // error={props.errors.phone_number}
                      />
                      {showphoneerror ? (
                        <p style={{ color: "red" }}>
                          {formerror && formerror.phone_number
                            ? formerror.phone_number.toString()
                            : ""}
                        </p>
                      ) : (
                        ""
                      )}
                    </FlexBox>
                    <FlexBox direction="column">
                      <FormLableTypography>
                        Contact Number <span>*</span>
                      </FormLableTypography>

                      <FormInputFiled
                        id="cell_phone_number"
                        name="cell_phone_number"
                        placeholder="+1 ( _ _ _ ) _ _ _ _  - _ _ _ "
                        // addonBefore="http://"
                        onChange={(e: any) => {
                          if (e.target.value.toString().length < 11) {
                            setFieldValue("cell_phone_number", e.target.value);
                          }
                        }}
                        error={props.errors.cell_phone_number}
                        defaultValue={
                          props.values.cell_phone_number
                            ? props.values.cell_phone_number
                            : userdata.cell_phone_number
                        }
                      />
                    </FlexBox>
                  </Main>
                  <Main
                    direction="row"
                    justifyContent="space-around"
                    style={{ marginBottom: "19px" }}
                  >
                    <FlexBox direction="column">
                      <FormLableTypography style={{ marginBottom: "13px" }}>
                        Does this member has a real estate license?
                      </FormLableTypography>
                      <Radio.Group
                        onChange={(e: any) => {
                          setFieldValue(
                            "is_real_estate_license",
                            e.target.value
                          );
                        }}
                        defaultValue={
                          userdata.is_real_estate_license === 0
                            ? 2
                            : userdata.is_real_estate_license
                        }
                        id="is_real_estate_license"
                      >
                        <Radio value={1}>Yes</Radio>
                        <Radio value={2}>No</Radio>
                      </Radio.Group>
                    </FlexBox>
                  </Main>
                  <Main direction="row" justifyContent="space-around">
                    <FlexBox direction="column">
                      <FormLableTypography>
                        Background education information
                      </FormLableTypography>
                      <FormInputFiled
                        id=" background_education_information"
                        name=" background_education_information "
                        placeholder="i.e. university degree"
                        onChange={(e: any) => {
                          setFieldValue(
                            "background_education_information",
                            e.target.value
                          );
                        }}
                        defaultValue={
                          props.values.background_education_information
                            ? props.values.background_education_information
                            : userdata.background_education_information
                        }
                      />
                    </FlexBox>
                  </Main>
                  <Main direction="row" justifyContent="space-around">
                    <FlexBox direction="column">
                      <FormLableTypography>
                        Languages Spoken
                      </FormLableTypography>

                      <CustomSelect
                        placeholder="Select from a list"
                        onChange={(e: any) => {
                          setFieldValue("language_spoken", e);
                        }}
                        id="language_spoken"
                        value={
                          props.values.language_spoken
                            ? props.values.language_spoken
                            : userdata.language_spoken
                        }
                        defaultValue={userdata.language_spoken}
                      >
                        <Option value="English">English</Option>
                      </CustomSelect>
                    </FlexBox>
                    <FlexBox direction="column">
                      <FormLableTypography>CRA Credentials</FormLableTypography>

                      <FormInputFiled
                        id="CRA_credential"
                        name="CRA_credential"
                        placeholder=""
                        onChange={(e: any) => {
                          setFieldValue("CRA_credential", e.target.value);
                        }}
                        defaultValue={
                          props.values.CRA_credential
                            ? props.values.CRA_credential
                            : userdata.CRA_credential
                        }
                      />
                    </FlexBox>
                  </Main>
                  <Main direction="row" justifyContent="space-around">
                    <FlexBox direction="column">
                      <FormLableTypography>Address #1</FormLableTypography>

                      <FormInputFiled
                        id="address1"
                        name="address1"
                        placeholder="Street, unit number"
                        onChange={(e: any) => {
                          setFieldValue("address1", e.target.value);
                        }}
                        defaultValue={
                          props.values.address1
                            ? props.values.address1
                            : userdata.address1
                        }
                      />
                    </FlexBox>
                    <FlexBox direction="column">
                      <FormLableTypography>Address #2</FormLableTypography>

                      <FormInputFiled
                        id="address2"
                        name="address2"
                        placeholder=""
                        onChange={(e: any) => {
                          setFieldValue("address2", e.target.value);
                        }}
                        defaultValue={
                          props.values.address2
                            ? props.values.address2
                            : userdata.address2
                        }
                      />
                    </FlexBox>
                  </Main>
                  <Main direction="row" justifyContent="space-around">
                    <FlexBox direction="column">
                      <FormLableTypography>City</FormLableTypography>

                      <FormInputFiled
                        id="city"
                        name="city"
                        placeholder=""
                        onChange={(e: any) => {
                          setFieldValue("city", e.target.value);
                        }}
                        defaultValue={
                          props.values.city ? props.values.city : userdata.city
                        }
                      />
                    </FlexBox>
                    <FlexBox direction="column">
                      <FormLableTypography>Postal Code</FormLableTypography>

                      <FormInputFiled
                        id="postal_code"
                        name="postal_code"
                        placeholder=""
                        onChange={(e: any) => {
                          setFieldValue("postal_code", e.target.value);
                        }}
                        defaultValue={
                          props.values.postal_code
                            ? props.values.postal_code
                            : userdata.postal_code
                        }
                      />
                    </FlexBox>
                  </Main>
                  <Main direction="row" justifyContent="space-around">
                    <FlexBox direction="column">
                      <FormLableTypography>
                        Profile Photo <span>*</span>
                      </FormLableTypography>

                      <FileInput
                        type="file"
                        name="profile_photo"
                        id="profile_photo"
                        onChange={(value: any) => {
                          let formData = new FormData();
                          setChangeimage(true);
                          formData.append("imageFile", value.target.files[0]);
                          setImagename(value.target.files[0].name);
                          setFieldValue("profile_photo", value.target.files[0]);
                        }}
                        error={props.errors.profile_photo}
                        imgname={imagename ? imagename : url}
                      />
                    </FlexBox>
                    <FlexBox direction="column">
                      <FormLableTypography>
                        When joined RAH <span>*</span>
                      </FormLableTypography>

                      <DateContent>
                        <DatePicker
                          name="rha_joind_date"
                          id="rha_joind_date"
                          defaultValue={
                            moment(userdata.rha_joind_date)
                              ? moment(userdata.rha_joind_date)
                              : props.values.rha_joind_date
                          }
                          format="D MMMM, YYYY"
                          onChange={(value: any) => {
                            setFieldValue(
                              "rha_joind_date",
                              moment(value).format("DD-MM-YYYY")
                            );
                          }}
                          disabledDate={d =>
                            !d ||
                            d.isAfter(moment().format("MM/DD/YYYY")) ||
                            d.isSameOrBefore("1960-01-01")
                          }
                        />
                      </DateContent>
                    </FlexBox>
                  </Main>
                  <Main direction="row" justifyContent="space-around">
                    <FlexBox direction="column">
                      <FormLableTypography>
                        Short biography <span>*</span>
                      </FormLableTypography>
                      <FormItem name="short_note" label="">
                        <TextAreaInput
                          name="short_note"
                          id="short_note"
                          error={props.errors.short_note}
                          onChange={(e: any) => {
                            setFieldValue("short_note", e.target.value);
                          }}
                          value={
                            props.values.short_note
                              ? props.values.short_note
                              : userdata.short_note
                          }
                          defaultValue={userdata.short_note}
                        />
                      </FormItem>
                    </FlexBox>
                  </Main>

                  <FormSubTitle>Branch and role details</FormSubTitle>
                  <Main direction="row" justifyContent="space-around">
                    <FlexBox direction="column">
                      <FormLableTypography>Branch</FormLableTypography>
                      <CustomSelect
                        placeholder="Start typing or select from list"
                        onChange={(e: any) => {
                          setFieldValue("user_branche_id", e);
                        }}
                        id="user_branche_id"
                        value={values.user_branche_id}
                        defaultValue={userdata.user_branche_id}
                      >
                        {branchLists.map((item: any) => {
                          return (
                            <>
                              <Option value={item.key}>{item.value}</Option>
                            </>
                          );
                        })}
                      </CustomSelect>
                    </FlexBox>
                    <FlexBox direction="column">
                      <FormLableTypography>
                        Department <span>*</span>
                      </FormLableTypography>
                      <FormItem name="user_department_id" label="">
                        <CustomSelect
                          placeholder="Start typing or select from list"
                          onChange={(e: any) => {
                            setFieldValue("user_department_id", e);
                          }}
                          id="user_department_id"
                          defaultValue={userdata.user_department_id}
                          value={values.user_department_id}
                        >
                          {deptLists.map((item: any) => {
                            return (
                              <>
                                <Option value={item.key}>{item.value}</Option>
                              </>
                            );
                          })}
                        </CustomSelect>
                      </FormItem>
                    </FlexBox>
                  </Main>
                  <Main direction="row" justifyContent="space-around">
                    <FlexBox direction="column">
                      <FormLableTypography>
                        Position <span style={{ color: "red" }}>*</span>
                      </FormLableTypography>

                      <FormItem name="user_role_id" label="">
                        <CustomSelect
                          placeholder="Start typing or select from list"
                          onChange={(e: any) => {
                            setFieldValue("user_role_id", e);
                          }}
                          id="user_role_id"
                          // value={
                          //   props.values.user_role_id
                          //     ? props.values.user_role_id
                          //     : userdata.user_role_id
                          // }

                          defaultValue={userdata.user_role_id}
                        >
                          {posLists.map((item: any) => {
                            return (
                              <>
                                <Option value={item.key}>{item.value}</Option>
                              </>
                            );
                          })}
                        </CustomSelect>
                      </FormItem>
                    </FlexBox>

                    <FlexBox direction="column">
                      <FormLableTypography>Will report to</FormLableTypography>
                      <CustomSelect
                        placeholder="Start typing or SelectInput from list"
                        onChange={(e: any) => {
                          setFieldValue("reporting_user_id", e);
                        }}
                        id="reporting_user_id"
                        value={values.reporting_user_id}
                      >
                        {reportLists.map((item: any) => {
                          return (
                            <>
                              <Option value={item.key}>{item.value}</Option>
                            </>
                          );
                        })}
                      </CustomSelect>
                    </FlexBox>
                  </Main>
                  <FlexBox className="footerbtn">
                    <Primary
                      text={"Save"}
                      type="submit"
                      className="submitbtn"
                    />

                    <Link
                      to={`/usermanagement/staff-detail/${empId}`}
                      data-testid="gouserList"
                      className="cancelbtn"
                    >
                      Cancel
                    </Link>
                  </FlexBox>
                </InnerContent>
              </ContainerBox>
            </Form>
          );
        }}
      </Formik>
    </FormWrapper>
  );
};
export default EditStaffProfileForm;
