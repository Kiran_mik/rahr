import * as Yup from "yup";


export const getRoleValue = (tableValues:any) => {
    let data = tableValues.map((tableRow: any) => ({
      id: tableRow.permissions
      ? tableRow.permissions.id
      : null ,
      name: tableRow.name,
      heading: tableRow.title,
      module_name_id:tableRow.id ,
      view: tableRow.permissions
        ? (tableRow.permissions.view === 0
          ? false
          : true)
        : false,
      edit: tableRow.permissions
        ? (tableRow.permissions.edit === 0
          ? false
          : true)
        : false,
      delete: tableRow.permissions
        ? (tableRow.permissions.delete === 0
          ? false
          : true)
        : false,
      create: tableRow.permissions
        ? (tableRow.permissions.create === 0
          ? false
          : true)
        : false
    }));
    return data;
  }; 
  
  export const formInitValues = {
    first_name: "",
    last_name: "",
    middle_name: "",
    dob: "",
    // rah_email: "",
    // email: "",
    // phone_number: "",
    // cell_phone_number: "",
    // profile_photo: "",
    // rha_joind_date: "",
    // user_role_id: "",
    // user_department_id: "",
    // short_note: "",
    // background_education_information:""
  };
  const phoneRegExp = /^[0-9]*\d$/
  
  export const validationSchema = Yup.object({
    // first_name: Yup.string().required("Please enter your full name"),
    // last_name: Yup.string().required("Please enter your last name"),
    // middle_name: Yup.string()
    //   .nullable()
    //   .required("Please enter your middle name"),
    //   user_role_id: Yup.string()
    // .required("Please select your position"),
    // user_department_id: Yup.string()
    // .required("Please select your department"),
    // short_note: Yup.string().nullable().required("Please enter short biography"),
    // dob: Yup.date().required("Please enter valid date"),
    // rha_joind_date: Yup.date().required("Please enter valid date"),
  
    // profile_photo: Yup.mixed()
    //   .nullable()
    //   .required("Please upload your photo"),
    // rah_email: Yup.string().email('Please enter valid and unique email')
    //   .nullable()
    //   .required("Please enter valid email"),
    // email: Yup.string().email('Please enter valid email')
    //   .nullable()
    //   .required("Please enter valid email"),
    //   phone_number: Yup.string()
  
    //   .required(
    //     "Phone number must be 10 digits."
    //   ).matches(phoneRegExp, 'phone number is not valid')
    //   .min(10, "phone number length should be 10 digits")
    //   .max(10, "please enter vaild phone number"),
    //   cell_phone_number: Yup.string()
    //   .required(
    //     "Contact number must be 10 digits."
    //   ).matches(phoneRegExp, 'contact number is not valid')
    //   .min(10, "contact number length should be 10 digits")
    //   .max(10, "contact number length should not exceed 10 digit"),
  });