import styled from "styled-components";
import { FormLableTypography } from "@components/Typography/Typography";
import BreadcrumbItem from "antd/lib/breadcrumb/BreadcrumbItem";
import { Flex } from "@components/Flex";
import {
  H3Typography,
  H5Typography,
  LabelTypography
} from "@components/Typography/Typography";
import {
  DatepickerIcon
} from "@assets/index";
import {
  DropdownIcon
} from "@assets/index";
import { Select } from "antd";

export const Container = styled.div`


`
export const AccordionContainer = styled.div`
position:static;
width:100%;
 max-height: calc(100vh - 331px);
min-height: calc(100vh - 331px);
background: #F5F5FF;
overflow: auto;
overflow-x: hidden;
margin-top:24px;
    border-radius: 8px;
.ant-collapse > .ant-collapse-item > .ant-collapse-header .ant-collapse-arrow {
    float:right;
        -webkit-text-emphasis: circle;
}
`
export const CustomSelect = styled(Select)`
 
.ant-select-selector{
    border-radius:8px !important;
     margin-bottom:0px !important;
border-color:#BDBDD3 !important;
}
 .ant-select-arrow {
    top: 43%;
    margin-top:0px;
	font-size:10px;
    color: #000;
    right:20px;
}
.anticon.anticon-down{
        background:url(${DropdownIcon}) no-repeat;
        display: block;
    width: 14px;
    height: 10px;
    }
    .anticon.anticon-down svg{
        display:none;
    } 
.ant-select-selector {
    width: 100%;
    height: auto !important;
  padding: 7px 12px !important;
    line-height: 22px;
}
input {
    padding: 12px 0px !important;
    height: auto !important;
}
.ant-select-single:not(.ant-select-customize-input) .ant-select-selector .ant-select-selection-search-input {
    height: 46px;
}
.ant-select-selection-placeholder{
	    color: #7B7B97;
    font-weight: 400;
}

`
export const DateContent = styled.div`
margin-bottom:18px;
.ant-picker{
font-size: 14px;
    font-family: Poppins;
    padding: 11px 12px;
    border: 1px solid #BDBDD3;
    background: transparent;
    border-radius: 4px;
    color: #17082D;
    font-weight: 500;
    line-height: 22px;
    width: 100%;
    font-family: Poppins;
    
}
.ant-picker-suffix{
        color: #17082D;
}
.anticon.anticon-calendar svg{
    display:none;
}
.anticon.anticon-calendar{
        background:url(${DatepickerIcon}) no-repeat;
        display: block;
    width: 18px;
    height: 18px;
    }
input{
    &::placeholder{
        color:#7B7B97;
  font-weight:400;
    }
}
`
export const MainCard = styled.div`
width: 100%;
max-height: calc(100vh - 432px);
min-height: calc(100vh - 412px);
left: 16px;
top: 64px;
overflow: auto;
background: #fff;
    padding: 16px 17px;
border-radius: 8px;
    font-family: Poppins;

    &::-webkit-scrollbar {
  width: 5px;
}


&::-webkit-scrollbar-track {
  box-shadow: inset 0 0 5px grey;
  border-radius: 10px;
}


&::-webkit-scrollbar-thumb {
  background:  rgba(151, 146, 227, 1);
  border-radius: 10px;
}


&::-webkit-scrollbar-thumb:hover {
  background:  rgba(151, 146, 227, 1);
}
.ant-card{
      max-height: calc(100vh - 450px);
    min-height: calc(100vh - 450px);
    overflow:auto;

     &::-webkit-scrollbar {
  width: 5px;
}


&::-webkit-scrollbar-track {
  box-shadow: inset 0 0 5px grey;
  border-radius: 10px;
}


&::-webkit-scrollbar-thumb {
  background:  rgba(151, 146, 227, 1);
  border-radius: 10px;
}


&::-webkit-scrollbar-thumb:hover {
  background:  rgba(151, 146, 227, 1);
}
}
    .ant-collapse > .ant-collapse-item > .ant-collapse-header .ant-collapse-arrow {
 
         float: left;
    -webkit-text-emphasis: circle;
    background: rgba(189,189,211,1);
    padding: 2px 6px;
  margin: 3px 8px 3px 0px;
    border-radius: 4px;
    height: auto;
    font-size: 8px;
    
    align-items: center;
}
.ant-card-body{
    padding:0px;
}
.ant-collapse > .ant-collapse-item > .ant-collapse-header {
      padding: 0px 0px;
}
.ant-collapse > .ant-collapse-item > .ant-collapse-header {
    color: #17082D
}
.ant-collapse-ghost {
       background-color: transparent;
    border: 0;
    padding: 20px 0px;
    border-bottom: 1px dashed #A2A2BA;
    border-radius: 0px;
    font-size: 14px;
    font-weight: 600;
    font-family: Poppins
}

.ant-checkbox + span {
    padding-right: 8px;
    padding-left: 8px;
    font-weight: 400;
    color: #17082D;
    font-size: 14px;
}
.ant-collapse-item .ant-collapse-arrow > svg{
    transform: rotate(-90deg);
}
&::-webkit-scrollbar {
  width: 5px;
}


&::-webkit-scrollbar-track {
  box-shadow: inset 0 0 5px grey;
  border-radius: 10px;
}


&::-webkit-scrollbar-thumb {
  background:  rgba(151, 146, 227, 1);
  border-radius: 10px;
}


&::-webkit-scrollbar-thumb:hover {
  background:  rgba(151, 146, 227, 1);
}
`
export const DrwaerLabel = styled(FormLableTypography)`
 font-size:14px;

`


export const DrwaerInnerContent = styled.div`
   max-height: calc(100vh - 181px);
    overflow: auto;
    min-height: calc(100vh - 181px);
    padding:24px 0px;

`
export const DrawerFooter = styled.div`

display:flex;
border-top:1px solid #E2E2EE;
padding:24px 0px 18px 0px;
    justify-content: space-between;
			display: flex;
			border-top: 1px solid #e2e2ee;
			padding: 24px 0px 18px 0px;
      -webkit-box-pack: justify;
    -webkit-justify-content: space-between;
    -ms-flex-pack: justify;
    justify-content: space-between;
		`


export const ModalFooter = styled.div`
display:flex;
border-top:1px solid #DBDBED;
padding:29px 0px 0px 0px;
    justify-content: flex-start;
`
export const BreadItem = styled(BreadcrumbItem) <{ firstNav: boolean }>`
  color:${props => props.firstNav ? "#17082D" : "#9792E3"};
`

export const FormTitle = styled(H3Typography)`
       font-weight:600;
	   margin-bottom:24px;
	   color:rgba(23, 8, 45, 1);
       `;

export const FormSubTitle = styled(H5Typography)`
       font-weight:600;
	   font-size:16px;
	   color:rgba(23, 8, 45, 1);
	   margin-bottom:16px;
       `;

export const FlexBox = styled(Flex)`
        flex:1 ;
		padding:0px 8px
       `;
export const Main = styled(Flex)`
	   direction:"row" ;
	     margin: 0px -8px;
       `;
export const ContainerBox = styled.div`
	
	position: relative;
	.error {
		font-size: 14px;
		color: #eb5757;
	}
`;
export const InnerContent = styled.div`
	display: flex;
	flex-direction: column;
	overflow-y: auto;
	overflow-x: hidden;
	box-sizing: border-box;
	padding-bottom: 24px;
.customdivider{
			margin:35px 0;
			border-color:#A2A2BA;
		}
	@media (max-width: 600px) {
		padding: 28px 16px;
	}
	@media (max-width: 359px) {
		padding: 24px 12px;
	}
	.error {
		font-size: 14px;
		color: #eb5757;
		margin-top: 4px;
	}
	.forminput{
		width:477px;
		height:46px;

	}

	.uploadFile {
		width: 30%;
		background-color: white;
		border: 1px solid grey;
		color: grey;
		font-size: 16px;
		line-height: 23px;
		overflow: hidden;
		padding: 10px 10px 4px 10px;
		position: relative;
		resize: none;
	  
		[type="file"] {
		  cursor: pointer !important;
		  display: block;
		  font-size: 999px;
		  filter: alpha(opacity=0);
		  min-height: 100%;
		  min-width: 100%;
		  opacity: 0;
		  position: absolute;
		  right: 0px;
		  text-align: right;
		  top: 0px;
		  z-index: 1;
		}
		
	  }
	  
`;
export const Formdiv = styled.div`
display:flex;
`
export const FormWrapper = styled.div`
    background: white;
    width: 100%;
    padding: 32px;
    border-radius: 10px;
    margin-bottom: 42px;
	.ant-select-selector{
		font-size: 14px;
    font-family: Poppins;
    padding: 11px 12px;
    border: 1px solid #BDBDD3;
    background: transparent;
    border-radius: 4px;
    color: #17082D;
    font-weight: 500;

    line-height: 22px;
	height:auto;
	}
 
	.submitbtn{
		    padding: 12px 24px;
    background: #4E1C95;
    color: #fff;
    font-family: Poppins;
    font-size: 14px;
    line-height: 22px;
    border-radius: 8px;
    width: max-content;
	cursor:pointer;
	}
	
	.cancelbtn{
		    background: transparent;
    font-size: 14px;
    line-height: 22px;
    color: #4E1C95;
    font-family: Poppins;
    font-weight: 400;
    margin-left: 22px;
    margin-top: 10px;
	cursor pointer;
}
.MainTitle{
	font-weight:600px;
}
.footerbtn{
	padding:0px;
	    margin-top: 24px;
    border-top: 1px solid #E2E2EE;
    padding-top: 24px;

}
textarea.ant-input{
	height:111px;
  border-radius:8px;
border-color:#BDBDD3 !important;
  
}
.ant-input-textarea-show-count::after{
	opacity:0;
}
	}
`
