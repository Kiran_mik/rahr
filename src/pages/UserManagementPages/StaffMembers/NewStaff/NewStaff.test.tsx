import React from "react";
import { render } from "@testing-library/react";
import "@testing-library/jest-dom";
import "@testing-library/jest-dom/extend-expect";
import { Provider } from "react-redux";
import store from "@store/index";
import NewStaffForm from "./NewStaffForm";

const TestUserData = {
  id: 1,
  username: "Joe Rogan",
  email: "atul.nandeshwar+1@torinit.ca",
  profile_detail_id: 1,
  profile_info_id: null,
  academy_id: null,
  vendor_id: null,
  vendor_product_id: null,
  analytics_id: null,
  billing_commission_id: null,
  form_id: null,
  email_verified_at: null,
  created_at: "2021-10-20T07:35:36.000000Z",
  first_name: "test",
  middle_name: "middle",
  last_name: "lastname",
  dob: "2021-10-01",
  language_spoken: "",
  phone_number: "12312123",
  cell_phone_number: null,
  driver_license: null,
  profile_photo: "",
  is_photo_upload_rahr_social_media: 0,
  address1: "",
  address2: null,
  province: "dcfc",
  city: "",
  postal_code: "",
  website_url: null,
  how_did_you_hear_about_rahr: null,
  detail_hear_about_rahr: null,
  is_real_estate_license: null,
  background_education_information: null,
  CRA_credential: null,
  rha_joind_date: "2021-10-20 09:35:36",
  short_note: null,
  reporting_user_id: null,
  rah_email: ""
};
describe("New Staff details test cases", () => {
  window.matchMedia =
    window.matchMedia ||
    function() {
      return {
        matches: false,
        addListener: function() {},
        removeListener: function() {}
      };
    };

  test("should display a blank  form", async () => {
    const result = render(
      <Provider store={store}>
        <NewStaffForm />
      </Provider>
    );
    const FirstName = await result.container.querySelector("#first_name");
    const LastName = await result.container.querySelector("#last_name");
    const MiddleName = await result.container.querySelector("#middle_name");
    // const DOB = await result.container.querySelector("#dob");
    const RAH_Email = await result.container.querySelector("#rah_email");
    const Email = await result.container.querySelector("#email");

    expect(FirstName).toHaveTextContent("");
    expect(LastName).toHaveTextContent("");
    expect(MiddleName).toHaveTextContent("");
    // expect(DOB).toHaveTextContent("");
    expect(RAH_Email).toHaveTextContent("");
    expect(Email).toHaveTextContent("");
  });
  // test("should submit form", async () => {
  //   const linkElement = await SubmitForm(TestUserData);
  //   expect(linkElement).toHaveTextContent("");
  // }, 30000);
});
