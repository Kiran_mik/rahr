import React, { useState, useEffect, useRef } from "react";
import { Formik, Form, useFormik, validateYupSchema } from "formik";
import FocusError from "@components/FocusError/FocusError";
import { validationSchema, formInitValues } from "./helper";
import { FormInputFiled } from "@components/Input/Input";
import { FileInput } from "@components/FileInput/index";
import { navigate } from "@reach/router";
import { ConnectedFocusError } from "focus-formik-error";
import {
  ContainerBox,
  FlexBox,
  InnerContent,
  Main,
  FormWrapper,
  FormTitle,
  FormSubTitle
} from "./style";
import modalimg from "@assets/pana.jpg";
import {
  SendStaffDetails,
  GetSupportDataStaff
} from "@services/UserManagement/NewStaffForm";
import { FormLableTypography, H3 } from "@components/Typography/Typography";
import DatePickerComponent from "@components/DatePicker/index";
import { DrawerPrimaryBtn } from "@components/Buttons/index";
import { Divider, Radio, Select } from "antd";
import TextAreaInput from "@components/Input/TextArea";
import ModalComponent from "@components/Modal/Modal";
import FormItem from "antd/lib/form/FormItem";
import SelectInput from "@components/Select";
import { Flex } from "@components/Flex";
import axios from "axios";
import { GoogleAutoComplete } from "@components/GoogleAutoComplete";
import { getAddressComps, getLocationByPlaceId } from "@utils/googlemaphealper";
interface MyFormValues {
  firstName: string;
}
const { Option } = Select;

export const MyApp: React.FC<{}> = (props) => {
  const [imgUpload, setimgupload] = useState<any>(false);
  const [formerror, setFormError] = useState<any>("");
  const [imagename, setImagename] = useState("");
  const [showerror, setShowError] = useState<boolean>(false);
  const [showemailerror, setShowEmailError] = useState<boolean>(false);
  const [showphoneerror, setShowPhoneError] = useState<boolean>(false);
  const [imgData, setimgData] = useState<any>(false);

  const [isModalVisible, setIsModalVisible] = useState(false);
  const [modalvalue, setModalValue] = useState<boolean>(false);
  const [formValues, setformValues] = useState<any>("");
  const [branchLists, setBranchLists] = useState<
    { value: string; key: string }[]
  >([]);
  const [deptLists, setDeptLists] = useState<{ value: string; key: string }[]>(
    []
  );
  const [posLists, setPosLists] = useState<{ value: string; key: string }[]>(
    []
  );
  const [reportLists, setReportLists] = useState<
    { value: string; key: string }[]
  >([]);
  const SupportData = async () => {
    const res: any = await GetSupportDataStaff();
    if (res) {
      let branches = [];
      for (let i = 0; i < res.data.data.branch_list.length; i++) {
        branches.push({
          value: res.data.data.branch_list[i].branch_name,

          key: res.data.data.branch_list[i].id
        });
      }
      setBranchLists([...branches]);
      let depts = [];

      for (let i = 0; i < res.data.data.department_list.length; i++) {
        depts.push({
          value: res.data.data.department_list[i].department_name,

          key: res.data.data.department_list[i].id
        });
      }
      setDeptLists([...depts]);
      let positions = [];

      for (let i = 0; i < res.data.data.position_list.length; i++) {
        positions.push({
          value: res.data.data.position_list[i].name,

          key: res.data.data.position_list[i].id
        });
      }
      setPosLists([...positions]);

      let report = [];

      for (let i = 0; i < res.data.data.report_to_list.length; i++) {
        report.push({
          value:
            res.data.data.report_to_list[i].first_name +
            " " +
            res.data.data.report_to_list[i].last_name,

          key: res.data.data.report_to_list[i].id
        });
      }
      setReportLists([...report]);
    }
  };

  const showModal = () => {
    if (modalvalue === true) {
      setIsModalVisible(true);
    } else {
      setIsModalVisible(false);
    }
  };
  const closeModal = () => {
    setModalValue(false);
    navigate("/usermanagement/staff-list");
  };
  useEffect(() => {
    showModal();
  }, [modalvalue]);

  const redirect = () => {
    navigate("/usermanagement/staff-list");
  };

  const photoUpload = async (values: any) => {
    let formData = new FormData();
    formData.append("imageFile", values.profile_photo);
    axios
      .post(
        "https://7ngsa15qzk.execute-api.ca-central-1.amazonaws.com/api/v1/image/upload",
        formData
      )
      .then(async (res: any) => {
        if (res.data.data.url) {
          sessionStorage.setItem("Upload", res.data.data.url);
          values.profile_photo = res.data.data.url;
          SubmitForm(values);
        }
      });
  };

  const SubmitForm = async (values: any) => {
    setformValues(values);

    var { response, success } = await SendStaffDetails(values);
    if (success) {
      setModalValue(true);
    } else {
      const errorresponse = response as any;
      values.profile_photo = sessionStorage.getItem("Upload");
      setFormError(errorresponse.error.error);

      setShowError(true);
      setShowEmailError(true);
      setShowPhoneError(true);
      setModalValue(false);
      setimgData(true);
    }
    return response;
  };
  useEffect(() => {
    window.onunload = function () {
      sessionStorage.removeItem("Upload");
    };
    SupportData();
  }, []);



  return (
    <FormWrapper>
      <FormTitle>New member information</FormTitle>
      <FormSubTitle>Profile details</FormSubTitle>
      <Formik
        initialValues={formInitValues}
        onSubmit={(values, actions) => {
          if (imgData === false) {
            photoUpload(values);
          } else {
            SubmitForm(values);
          }
        }}
        validationSchema={validationSchema}
      >
        {props => {
          const { values, setFieldValue } = props;
          
          const onPlaceSelect = async (placeId: string) => {
            const place = await getLocationByPlaceId(placeId);
            if (place) {
              const locObj = getAddressComps(place);
              if (locObj.city.length > 0) {
                // const coordinates = ({ lat: place.geometry.location.lat, lng: place.geometry.location.lng });
                // console.log("coordinates", coordinates);
                console.log("address", locObj);
                setFieldValue("address1", locObj.formattedAddress || '')
                setFieldValue("address2", locObj.addressLine_2 || '')
                setFieldValue("province", locObj.state || '')
                setFieldValue("city", locObj.city || '')
                setFieldValue("postal_code", locObj.postalCode || '')
                // setCoord([coordinates])
                // setCenter({ lat: place.geometry.location.lat, lng: place.geometry.location.lng })
              }
            }
          };
          return (
            <Form>
              <ConnectedFocusError />
              <ContainerBox>
                <InnerContent>
                  <Main direction="row" justifyContent="space-around">
                    <FlexBox direction="column">
                      <FormLableTypography>
                        First Name <span>*</span>
                      </FormLableTypography>
                      <FormInputFiled
                        id="first_name"
                        name="first_name"
                        data-testid="first_name"
                        placeholder=""
                        onChange={(e: any) => {
                          if (e.target.value.toString().length < 30) {
                            setFieldValue("first_name", e.target.value);
                          }
                          setformValues(e.target.value);
                        }}
                        error={props.errors.first_name}
                        value={props.values.first_name}
                      />
                    </FlexBox>
                    <FlexBox direction="column">
                      <FormLableTypography>
                        Last Name <span>*</span>
                      </FormLableTypography>
                      <FormInputFiled
                        id="last_name"
                        name="last_name"
                        placeholder=""
                        data-testid="last_name"
                        onChange={(e: any) => {
                          if (e.target.value.toString().length < 30) {
                            setFieldValue("last_name", e.target.value);
                          }
                        }}
                        error={props.errors.last_name}
                        value={props.values.last_name}
                      />
                    </FlexBox>
                  </Main>
                  <Main direction="row" justifyContent="space-around">
                    <FlexBox direction="column">
                      <FormLableTypography>
                        Middle Name <span>*</span>
                      </FormLableTypography>

                      <FormInputFiled
                        id="middle_name"
                        name="middle_name"
                        placeholder=""
                        onChange={(e: any) => {
                          if (e.target.value.toString().length < 30) {
                            setFieldValue("middle_name", e.target.value);
                          }
                        }}
                        error={props.errors.middle_name}
                        value={props.values.middle_name}
                      />
                    </FlexBox>
                    <FlexBox direction="column">
                      <FormLableTypography>
                        Date of Birth <span>*</span>
                      </FormLableTypography>

                      <DatePickerComponent
                        name="dob"
                        id="dob"
                        error={props.errors.dob}
                        onChange={(value: any) => {
                          setFieldValue("dob", value);
                        }}
                        value={props.values.dob}
                      />
                    </FlexBox>
                  </Main>
                  <Main direction="row" justifyContent="space-around">
                    <FlexBox direction="column">
                      <FormLableTypography>
                        RAH Email <span>*</span>
                      </FormLableTypography>
                      <FormInputFiled
                        id="rah_email"
                        name="rah_email"
                        placeholder="email@rah.ca"
                        onChange={(e: any) => {
                          if (e.target.value.toString().length < 35) {
                            setFieldValue("rah_email", e.target.value);
                          }
                          setShowError(false);
                        }}
                        error={props.errors.rah_email}
                        value={props.values.rah_email}
                      />
                      {showerror ? (
                        <p style={{ color: "red", marginTop:"-13px", marginBottom:"18px" }}>
                          {formerror && formerror.rah_email
                            ? formerror.rah_email.toString()
                            : ""}
                        </p>
                      ) : (
                        ""
                      )}
                    </FlexBox>
                    <FlexBox direction="column">
                      <FormLableTypography>
                        Personal Email <span>*</span>
                      </FormLableTypography>

                      <FormInputFiled
                        id="email"
                        name="email"
                        placeholder="sample@mail.ca"
                        onChange={(e: any) => {
                          if (e.target.value.toString().length < 35) {
                            setFieldValue("email", e.target.value);
                          }
                          setShowEmailError(false);
                        }}
                        error={props.errors.email}
                        value={props.values.email}
                      />

                      {showemailerror ? (
                        <p style={{ color: "red", marginTop:"-13px", marginBottom:"18px" }}>
                          {formerror && formerror.email
                            ? formerror.email.toString()
                            : ""}
                        </p>
                      ) : (
                        ""
                      )}
                    </FlexBox>
                  </Main>
                  <Main direction="row" justifyContent="space-around">
                    <FlexBox direction="column">
                      <FormLableTypography>
                        Phone Number <span>*</span>
                      </FormLableTypography>

                      <FormInputFiled
                        id="phone_number"
                        name="phone_number"
                        placeholder="+1 ( _ _ _ ) _ _ _ - _ _ _ _ "
                        onChange={(e: any) => {
                          if (
                            e.target.value.toString().length < 11 &&
                            // e.target.value.toString() != "0000000000" &&
                            e.target.value.substr(0, 1) !== "0"
                          ) {
                            setFieldValue("phone_number", e.target.value);
                          }
                          setShowPhoneError(false);
                        }}
                        value={props.values.phone_number}
                        error={props.errors.phone_number}
                      />
                      {showphoneerror ? (
                        <p style={{ color: "red", marginTop:"-13px", marginBottom:"18px" }}>
                          {formerror && formerror.phone_number
                            ? formerror.phone_number.toString()
                            : ""}
                        </p>
                      ) : (
                        ""
                      )}
                    </FlexBox>
                    <FlexBox direction="column">
                      <FormLableTypography>
                        Contact Number <span>*</span>
                      </FormLableTypography>

                      <FormInputFiled
                        id="cell_phone_number"
                        name="cell_phone_number"
                        placeholder="+1 ( _ _ _ ) _ _ _ - _ _ _ _ "
                        // addonBefore="http://"
                        onChange={(e: any) => {
                          if (
                            e.target.value.toString().length < 11 &&
                            // e.target.value.toString() != "0000000000" &&
                            e.target.value.substr(0, 1) !== "0"
                          ) {
                            setFieldValue("cell_phone_number", e.target.value);
                          }
                          // setFieldValue("cell_phone_number", e.target.value);
                        }}
                        error={props.errors.cell_phone_number}
                        value={props.values.cell_phone_number}
                      />
                    </FlexBox>
                  </Main>
                  <Main
                    direction="row"
                    justifyContent="space-around"
                    style={{ marginBottom: "19px" }}
                  >
                    <FlexBox direction="column">
                      <FormLableTypography style={{ marginBottom: "13px" }}>
                        Does this member has a real estate license?
                      </FormLableTypography>
                      <Radio.Group
                        onChange={(e: any) => {
                          // onRadioChange(e.target.value);

                          setFieldValue(
                            "is_real_estate_license",
                            e.target.value
                          );
                        }}
                        defaultValue={2}
                        id="is_real_estate_license"
                      >
                        <Radio value={1}>Yes</Radio>
                        <Radio value={2}>No</Radio>
                      </Radio.Group>
                    </FlexBox>
                  </Main>
                  <Main direction="row" justifyContent="space-around">
                    <FlexBox direction="column">
                      <FormLableTypography>
                        Background education information
                      </FormLableTypography>
                      <FormInputFiled
                        id=" background_education_information"
                        name=" background_education_information "
                        placeholder="i.e. university degree"
                        onChange={(e: any) => {
                          setFieldValue(
                            "background_education_information",
                            e.target.value
                          );
                        }}
                      />
                    </FlexBox>
                  </Main>
                  <Main direction="row" justifyContent="space-around">
                    <FlexBox direction="column">
                      <FormLableTypography>
                        Languages Spoken
                      </FormLableTypography>

                      <SelectInput
                        placeholdertitle="Select from a list"
                        onChange={(e: any) => {
                          setFieldValue("language_spoken", e);
                        }}
                        id="language_spoken"
                      >
                        <Option value="English">English</Option>
                      </SelectInput>
                    </FlexBox>
                    <FlexBox direction="column">
                      <FormLableTypography>CRA Credentials</FormLableTypography>

                      <FormInputFiled
                        id="CRA_credential"
                        name="CRA_credential"
                        placeholder=""
                        onChange={(e: any) => {
                          setFieldValue("CRA_credential", e.target.value);
                        }}
                      />
                    </FlexBox>
                  </Main>
                  <Main direction="row" justifyContent="space-around">
                    <FlexBox direction="column">
                      <FormLableTypography>Address #1</FormLableTypography>

                      {/* <FormInputFiled
                        id="address1"
                        name="address1"
                        placeholder="Street, unit number"
                        onChange={(e: any) => {
                          setFieldValue("address1", e.target.value);
                        }}
                      /> */}
                      <GoogleAutoComplete
                        apiKey={process.env.REACT_APP_GOOGLEAPI_KEY}
                        defaultVal={''}
                        id="address1"
                        name="address1"
                        placeholder="Street, unit number"
                        onPlaceSelect={(place: any) => {
                          console.log("api resp", place);
                          if (place) onPlaceSelect(place.place_id);
                        }}
                      />
                    </FlexBox>
                    <FlexBox direction="column">
                      <FormLableTypography>Address #2</FormLableTypography>

                      <FormInputFiled
                        id="address2"
                        name="address2"
                        placeholder=""
                        onChange={(e: any) => {
                          setFieldValue("address2", e.target.value);
                        }}
                        //@ts-ignore
                        value={props.values.address2}
                      />
                    </FlexBox>
                  </Main>
                  <Main direction="row" justifyContent="space-around">
                    <FlexBox direction="column">
                      <FormLableTypography>City</FormLableTypography>

                      <FormInputFiled
                        id="city"
                        name="city"
                        placeholder=""
                        onChange={(e: any) => {
                          setFieldValue("city", e.target.value);
                        }}
                        //@ts-ignore
                        value={props.values.city}
                      />
                    </FlexBox>
                    <FlexBox direction="column">
                      <FormLableTypography>Postal Code</FormLableTypography>

                      <FormInputFiled
                        id="postal_code"
                        name="postal_code"
                        placeholder=""
                        onChange={(e: any) => {
                          setFieldValue("postal_code", e.target.value);
                        }}
                        //@ts-ignore
                        value={props.values.postal_code}
                      />
                    </FlexBox>
                  </Main>
                  <Main direction="row" justifyContent="space-around">
                    <FlexBox direction="column">
                      <FormLableTypography>
                        Profile Photo <span>*</span>
                      </FormLableTypography>

                      <FileInput
                        type="file"
                        name="profile_photo"
                        id="profile_photo"
                        onChange={(value: any) => {
                          let formData = new FormData();
                          formData.append("imageFile", value.target.files[0]);
                          setImagename(value.target.files[0].name);
                          setFieldValue("profile_photo", value.target.files[0]);
                        }}
                        error={props.errors.profile_photo}
                        imgname={imagename}
                      />
                    </FlexBox>
                    <FlexBox direction="column">
                      <FormLableTypography>
                        When joined RAH <span>*</span>
                      </FormLableTypography>

                      <DatePickerComponent
                        name="rha_joind_date"
                        id="rha_joind_date"
                        error={props.errors.rha_joind_date}
                        onChange={(value: any) => {
                          setFieldValue("rha_joind_date", value);
                        }}
                        value={props.values.rha_joind_date}
                      />
                    </FlexBox>
                  </Main>
                  <Main direction="row" justifyContent="space-around">
                    <FlexBox direction="column">
                      <FormLableTypography>
                        Short biography <span>*</span>
                      </FormLableTypography>
                      <FormItem name="short_note" label="">
                        <TextAreaInput
                          name="short_note"
                          id="short_note"
                          error={props.errors.short_note}
                          onChange={(e: any) => {
                            setFieldValue("short_note", e.target.value);
                          }}
                          value={props.values.short_note}
                        />
                      </FormItem>
                    </FlexBox>
                  </Main>
                  <Divider dashed className="customdivider" />
                  <FormSubTitle>Branch and role details</FormSubTitle>
                  <Main direction="row" justifyContent="space-around">
                    <FlexBox direction="column">
                      <FormLableTypography>Branch</FormLableTypography>
                      <SelectInput
                        className="bottom-0"
                        placeholdertitle="Start typing or select from list"
                        onChange={(e: any) => {
                          setFieldValue("user_branche_id", e);
                        }}
                        id="user_branche_id"
                        onFocus
                      >
                        {branchLists.map((item: any) => {
                          return (
                            <>
                              <Option value={item.key}>{item.value}</Option>
                            </>
                          );
                        })}
                      </SelectInput>
                    </FlexBox>
                    <FlexBox direction="column">
                      <FormLableTypography>
                        Department <span>*</span>
                      </FormLableTypography>
                      <FormItem name="user_department_id" label="">
                        <SelectInput
                          className="bottom-0"
                          placeholdertitle="Start typing or select from list"
                          name="user_department_id"
                          onChange={(e: any) => {
                            setFieldValue("user_department_id", e);
                          }}
                          id="user_department_id"
                          value={props.values.user_department_id}
                          error={props.errors.user_department_id}
                        >
                          {/* deptLists */}
                          {deptLists.map((item: any) => {
                            return (
                              <>
                                <Option value={item.key}>{item.value}</Option>
                              </>
                            );
                          })}
                        </SelectInput>
                      </FormItem>
                    </FlexBox>
                  </Main>
                  <Main direction="row" justifyContent="space-around">
                    <FlexBox direction="column">
                      <FormLableTypography>
                        Position <span style={{ color: "red" }}>*</span>
                      </FormLableTypography>

                      <FormItem name="user_role_id" label="">
                        <SelectInput
                          className="bottom-0"
                          placeholdertitle="Start typing or select from list"
                          name="user_role_id"
                          onChange={(e: any) => {
                            setFieldValue("user_role_id", e);
                          }}
                          id="user_role_id"
                          value={props.values.user_role_id}
                          error={props.errors.user_role_id}
                        >
                          {posLists.map((item: any) => {
                            return (
                              <>
                                <Option value={item.key}>{item.value}</Option>
                              </>
                            );
                          })}
                        </SelectInput>
                      </FormItem>
                    </FlexBox>
                    <FlexBox direction="column">
                      <FormLableTypography>Will report to</FormLableTypography>
                      <SelectInput
                        className="bottom-0"
                        placeholdertitle="Start typing or SelectInput from list"
                        onChange={(e: any) => {
                          setFieldValue("reporting_user_id", e);
                        }}
                        id="reporting_user_id"
                        value={props.values.reporting_user_id}
                        error={props.errors.reporting_user_id}
                      >
                        {reportLists.map((item: any) => {
                          return (
                            <>
                              <Option value={item.key}>{item.value}</Option>
                            </>
                          );
                        })}
                      </SelectInput>
                    </FlexBox>
                  </Main>
                  <FlexBox className="footerbtn">
                    <button type="submit" className="submitbtn">
                      Send invite
                    </button>
                    <button className="cancelbtn" onClick={redirect}>
                      Cancel
                    </button>
                  </FlexBox>
                </InnerContent>
              </ContainerBox>
              {modalvalue === true ? (
                <ModalComponent
                  visible={modalvalue}
                  onOK={closeModal}
                  onCancel={closeModal}
                  modalimage={modalimg}
                  title=""
                  content=""
                >
                  <div style={{ textAlign: "center", padding: "24px" }}>
                    <img
                      src={modalimg}
                      alt="backgroundimg"
                      style={{ marginBottom: "40px" }}
                    />
                    <H3
                      text={`New user "${props.values.first_name +
                        " " +
                        props.values.last_name} " has been created`}
                      className="MainTitle"
                    />
                    <p
                      className="modalpara"
                      style={{
                        fontFamily: "Poppins",
                        color: "#50514F",
                        fontSize: "14px"
                      }}
                    />
                  </div>
                  <Flex justifyContent="center">
                    <DrawerPrimaryBtn
                      text="Back to Staff List"
                      onClick={closeModal}
                    />
                  </Flex>
                </ModalComponent>
              ) : (
                ""
              )}
            </Form>
          );
        }}
      </Formik>
    </FormWrapper>
  );
};

export default MyApp;
