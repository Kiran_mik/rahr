import * as Yup from "yup";
import { SendStaffDetails } from "@services/UserManagement/NewStaffForm";

export const formInitValues = {
  first_name: "",
  last_name: "",
  middle_name: "",
  dob: "",
  rah_email: "",
  email: "",
  phone_number: "",
  cell_phone_number: "",
  profile_photo: "",
  rha_joind_date: "",
  user_role_id: "",
  user_department_id: "",
  user_branche_id: "",
  short_note: "",
  background_education_information: "",
  reporting_user_id: "",
};

const phoneRegExp = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;

export const validationSchema = Yup.object({
  first_name: Yup.string()
    .required("Please enter your first name")
    .max(30, ""),
  last_name: Yup.string().required("Please enter your last name"),
  middle_name: Yup.string()
    .nullable()
    .required("Please enter your middle name"),
  user_role_id: Yup.string().required("Please select your position"),
  user_department_id: Yup.string().required("Please select your department"),
  short_note: Yup.string()
    .nullable()
    .required("Please enter your short biography"),
  dob: Yup.date().required("Please enter a valid date"),
  rha_joind_date: Yup.date().required("Please enter a valid date"),
  profile_photo: Yup.mixed()
    .nullable()
    .required("Please upload your photo"),
  rah_email: Yup.string()
    .email("Please enter a valid and unique email")
    .nullable()
    .required("Please enter an email address"),
  email: Yup.string()
    .email("Please enter a valid email")
    .nullable()
    .required("Please enter an email address"),
  phone_number: Yup.string()
    .required("Phone number cannot be empty")
    .matches(phoneRegExp, "Phone number is not valid")
    .min(10, "Phone number should be of 10 digits")
    .max(10, "Please enter a valid phone number"),
  cell_phone_number: Yup.string()
    .required("Contact number cannot be empty")
    .matches(phoneRegExp, "Contact number is not valid")
    .min(10, "Contact number should be of 10 digits")
    .max(10, "Contact number cannot exceed 10 digits"),
});
export const SubmitForm = async (values: any) => {
  const res = await SendStaffDetails(values);
  return res;
};
