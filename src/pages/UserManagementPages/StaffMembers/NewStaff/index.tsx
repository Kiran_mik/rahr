import React, { FC } from "react";
import { Layout } from "antd";
import { navigate, RouteComponentProps } from "@reach/router";
import DashboardLayout from "@pages/UserManagementPages/StaffMembers/DashboardLayout";
import NewStaffForm from "./NewStaffForm";
import { Container } from "./style";
import StaffHeader from "@components/StaffHeader";
import { H3 } from "@components/Typography/Typography";
export interface SignupProps extends RouteComponentProps {
  title?: string;
}
const { Content } = Layout;
const index: FC<SignupProps> = () => {
  return (
    <>
      <DashboardLayout>
        <StaffHeader>
          <H3 text="Create user profile" />
        </StaffHeader>
        <NewStaffForm />
      </DashboardLayout>
    </>
  );
};

export default index;
