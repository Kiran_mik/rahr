import styled from "styled-components";
import { Flex } from "@components/Flex";
import SelectInput from "@components/Select";
import {
	H3Typography,
	H5Typography,
	LabelTypography
} from "@components/Typography/Typography";
export const Container = styled.div`

`;
export const FormTitle = styled(H3Typography)`
       font-weight:600;
	   margin-bottom:24px;
	   color:rgba(23, 8, 45, 1);
       `;

export const FormSubTitle = styled(H5Typography)`
       font-weight:600;
	   font-size:16px;
	   color:rgba(23, 8, 45, 1);
	   margin-bottom:16px;
       `;

export const FlexBox = styled(Flex)`
        flex:1 ;
		padding:0px 8px;
		position:relative
       `;
export const Main = styled(Flex)`
	   direction:"row" ;
	     margin: 0px -8px;
       `;
export const ContainerBox = styled.div`
	
	position: relative;
	.error {
		font-size: 14px;
		color: #eb5757;
	}
	.bottom-0{
	margin-bottom:0px;
}
`;
export const InnerContent = styled.div`
	display: flex;
	flex-direction: column;
	overflow-y: auto;
	overflow-x: hidden;
	box-sizing: border-box;
	padding-bottom: 24px;
.customdivider{
			margin:35px 0;
			border-color:#A2A2BA;
		}
	@media (max-width: 600px) {
		padding: 28px 16px;
	}
	@media (max-width: 359px) {
		padding: 24px 12px;
	}
	.error {
		font-size: 14px;
		color: #eb5757;
		margin-top: 4px;
	}
	.forminput{
		width:477px;
		height:46px;

	}

	.uploadFile {
		width: 30%;
		background-color: white;
		border: 1px solid grey;
		color: grey;
		font-size: 16px;
		line-height: 23px;
		overflow: hidden;
		padding: 10px 10px 4px 10px;
		position: relative;
		resize: none;
	  
		[type="file"] {
		  cursor: pointer !important;
		  display: block;
		  font-size: 999px;
		  filter: alpha(opacity=0);
		  min-height: 100%;
		  min-width: 100%;
		  opacity: 0;
		  position: absolute;
		  right: 0px;
		  text-align: right;
		  top: 0px;
		  z-index: 1;
		}
		
	  }
	  
`;
export const Formdiv = styled.div`
display:flex;
`
export const FormWrapper = styled.div`
    background: white;
    width: 100%;
    padding: 32px;
    border-radius: 10px;
    margin-bottom: 42px;
	.ant-select-selector{
		font-size: 14px;
    font-family: Poppins;
    padding: 11px 12px;
    border: 1px solid #BDBDD3;
    background: transparent;
    border-radius: 4px;
    color: #17082D;
    font-weight: 500;
    line-height: 22px;
	height:auto;
	}
 
	.submitbtn{
		    padding: 12px 24px;
    background: #4E1C95;
    color: #fff;
    font-family: Poppins;
    font-size: 14px;
    line-height: 22px;
    border-radius: 8px;
    width: max-content;
	cursor:pointer;
	}
	.submitbtn:focus, .cancelbtn:focus{
		border-color: #40a9ff;
    border-right-width: 1px !important;
    outline: 0;
    box-shadow: 0 0 0 1px rgb(24 144 255 / 80%);

	}
	.cancelbtn{
		    background: transparent;
    font-size: 14px;
    line-height: 22px;
    color: #4E1C95;
    font-family: Poppins;
    font-weight: 400;
    margin-left: 22px;
	cursor pointer;
}
.MainTitle{
	font-weight:600px;
}
.footerbtn{
	padding:0px;
	    margin-top: 24px;
    border-top: 1px solid #E2E2EE;
    padding-top: 24px;

}
textarea.ant-input{
	height:111px;
	border-radius:8px;
	border-color:#BDBDD3 !important;
}
.ant-input-textarea-show-count::after{
	float:left;
	color:rgba(123, 123, 151, 1);
	font-size:14px;
	line-height:22px;
	font-family:Poppins;
	font-weight:400;
}
	}
`
