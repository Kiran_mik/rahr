import React from "react";
import {
  act,
  render,
  screen,
  fireEvent,
  waitForElement
} from "@testing-library/react";
import "@testing-library/jest-dom";
import "@testing-library/jest-dom/extend-expect";
import StaffDetails from ".";
import { Provider } from "react-redux";
import store from "@store/index";
import UserDetails from "./UserDetails";
import FormDataField from "./FormDataField";
import { getProfile } from "./helper";
import SelectInput from "@components/Select";
import { GetServiceData } from "@services/UserService";
import axiosMock from "axios";
const TestUserData = {
  id: 1,
  username: "Joe Rogan",
  email: "atul.nandeshwar+1@torinit.ca",
  profile_detail_id: 1,
  profile_info_id: null,
  academy_id: null,
  vendor_id: null,
  vendor_product_id: null,
  analytics_id: null,
  billing_commission_id: null,
  form_id: null,
  email_verified_at: null,
  created_at: "2021-10-20T07:35:36.000000Z",
  first_name: "test",
  middle_name: "middle",
  last_name: "lastname",
  dob: "2021-10-01",
  // gender: 1,
  // SIN_number: "123",
  language_spoken: "",
  phone_number: "12312123",
  cell_phone_number: null,
  driver_license: null,
  profile_photo: "",
  is_photo_upload_rahr_social_media: 0,
  address1: "",
  address2: null,
  province: "dcfc",
  city: "",
  postal_code: "",
  website_url: null,
  how_did_you_hear_about_rahr: null,
  detail_hear_about_rahr: null,
  is_real_estate_license: null,
  background_education_information: null,
  CRA_credential: null,
  rha_joind_date: "2021-10-20 09:35:36",
  short_note: null,
  reporting_user_id: null,
  rah_email: ""
};
describe("Staff details test cases", () => {
  window.matchMedia =
    window.matchMedia ||
    function() {
      return {
        matches: false,
        addListener: function() {},
        removeListener: function() {}
      };
    };
  test("StaffDetails should render properly", async () => {
    const container = render(
      <Provider store={store}>
        <StaffDetails />
      </Provider>
    );
    const loader = screen.getByTestId("loader");
    expect(loader).toBeInTheDocument();
    const linkElement = await screen.getByTestId("breadcrumb-user");
    const goTouserList = screen.getByTestId("gouserList");
    const UserDetails = screen.queryByText(/User details/i);
    // expect(container.firstChild.classList.contains("ProfileName")).toBe(true);
    const erroralert = screen.queryByText(/something Went Wrong/i);
    expect(linkElement).toBeInTheDocument();
    expect(erroralert).not.toBeInTheDocument();
    expect(UserDetails).toBeInTheDocument();
    expect(goTouserList).toHaveAttribute("href", "/usermanagement/staff-list");
  });
  test("Full Name Should Display Properly", () => {
    render(
      <Provider store={store}>
        <FormDataField
          heading={"Full Name"}
          value={`${TestUserData.first_name} ${TestUserData.middle_name} ${
            TestUserData.last_name
          }`}
        />
      </Provider>
    );
    const FieldData = screen.getByTestId("FieldData");
    const FieldHeading = screen.getByTestId("FieldHeading");
    expect(FieldHeading).toHaveTextContent("Full Name");
    expect(FieldData).toHaveTextContent("test middle lastname");
  });
});

function UserDetailsComponent() {
  return (
    <Provider store={store}>
      <UserDetails userData={TestUserData} />
    </Provider>
  );
}
describe("user details test cases", () => {
  beforeEach(() => {
    return render(<UserDetailsComponent />);
  });

  it("User details render properly", async () => {
    // const res: any = await getProfile(5);

    const ProfileDetailsText = screen.queryByText(/Profile details/i);

    const FieldData = screen.getByText("Full Name");
    const FieldValue = screen.getByText("test middle lastname");
    expect(FieldData).toBeInTheDocument();
    expect(FieldValue).toBeInTheDocument();
    //expect(FieldData).toHaveTextContent("test middle lastname");
  });
});
describe("Staff details Apis", () => {
  it("get profile details api should run", async () => {
    // const res: any = await getProfile(5);
    act(async () => {
      render(
        <Provider store={store}>
          <StaffDetails />
        </Provider>
      );
    });
    const loader = screen.getByTestId("loader");
    expect(loader).toBeInTheDocument();
    //expect(getProfile).toHaveBeenCalledTimes(1);
    //expect(jest.mock(getProfile)).toHaveBeenCalledWith();
    // fetchPosts.mockResolvedValueOnce(getProfile);
    //expect(res.status).toBe(200);
  }, 7000);
  const profilePhoto = screen.getByTestId("profilePhoto");

  expect(ProfileDetailsText).toBeInTheDocument();
  expect(profilePhoto).toBeInTheDocument();
  expect(FieldData).toBeInTheDocument();
  expect(FieldValue).toBeInTheDocument();
});
it("Edit Permission popup should open", async () => {
  // const res: any = await getProfile(5);
  fireEvent.click(screen.getByTestId("EditPermission"));
  const RoleName = screen.queryByText(/Role Name/i);
  fireEvent.click(screen.getByTestId("SubmitAndproceed"));
  const PermissionPopup = await screen.queryByText(
    /You are about to override user permissions and settings/i
  );

  expect(RoleName).toBeInTheDocument();
  expect(PermissionPopup).toBeInTheDocument();
});

it("Click Back Button", async () => {
  fireEvent.click(screen.getByTestId("BackToRAH"));
});
it("Click EditProfile button", async () => {
  fireEvent.click(screen.getByTestId("EditProfile"));
});
describe("Staff details Apis", () => {
  it("get profile details api should run", async () => {
    // const res: any = await getProfile(5);
    act(async () => {
      render(
        <Provider store={store}>
          <StaffDetails />
        </Provider>
      );
    });
    const loader = screen.getByTestId("loader");
    expect(loader).toBeInTheDocument();
  });
});
