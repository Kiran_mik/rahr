import React, { useState } from "react";

import { FlexBox, FormWrapper } from "../NewStaff/style";
import { H5Typography } from "@components/Typography/Typography";

import { IconButton, EditIconButton } from "@components/Buttons";
import {
  ButttonWrapper,
  FormSectionWrapper,
  TilteWrapper,
  FormFieldProfile,
  FormFieldHeading,
  ProfilePhoto,
  FormDevide,
  FormContainer,
  ReportingDiv,
  BiographyDiv,
  H3Title
} from "./style";
import { BackArrow, DemoImage, EditIcon, ReportIcon } from "@assets/index";
import PhotoBG from "@assets/photo-bg.png";
import UserPermission from "./UserPermissions";
import { navigate } from "@reach/router";
import EditRolesDrawer from "../EditStaff/EditRolesDrawer";
import moment from "moment";
import { Col, Row } from "antd";

import FormDataView from "@components/ViewForm/FormDataField";
import { convertMobileNo } from "@utils/helpers";
interface UserDetailsProps {
  userData: any;
  tableValue: any;
  getProfileDetails: any;
  onRoleUpdate: any;
  empId: any;
}

const UserDetails: React.FC<UserDetailsProps> = ({
  userData,
  tableValue,
  onRoleUpdate,
  getProfileDetails,
  empId
}) => {
  const [visible, setVisible] = useState(false);
  const showDrawer = () => {
    setVisible(true);
  };
  const onClose = () => {
    setVisible(false);
    ///getProfileDetails();
  };

  return (
    <FormWrapper>
      <ButttonWrapper>
        <IconButton
          text={"Back to RAH members"}
          testId="BackToRAH"
          onClick={() => {
            navigate("/usermanagement/staff-list");
          }}
          icon={BackArrow}
        />

        <EditIconButton
          text={"Edit"}
          icon={EditIcon}
          testId="EditProfile"
          onClick={() => {
            navigate(`/usermanagement/edit-staff/${empId}`);
          }}
        />
      </ButttonWrapper>
      <FormSectionWrapper>
        <H5Typography>Profile details</H5Typography>
      </FormSectionWrapper>
      <FormContainer>
        <FormDevide firstChild={true}>
          <Row>
            <Col className="gutter-row" span={20}>
              <Row gutter={16}>
                <Col className="gutter-row" span={8}>
                  <FormDataView
                    heading={"Full Name"}
                    value={`${userData.first_name} ${userData.last_name}`}
                  />
                </Col>
                <Col className="gutter-row" span={8}>
                  <FormDataView
                    heading={"Date Of Birth"}
                    value={`${moment(userData.dob).format("D MMMM, YYYY")}`}
                  />
                </Col>
                <Col className="gutter-row" span={8}>
                  <FormDataView
                    heading={"Date joined RAH"}
                    value={`${userData &&
                      moment(userData.rha_joind_date).format("D MMMM, YYYY")}`}
                  />
                </Col>
              </Row>
              <Row gutter={16}>
                <Col className="gutter-row" span={8}>
                  <FormDataView
                    heading={"Has a real estate license?"}
                    value={userData.is_real_estate_license === 0 ? "No" : "yes"}
                  />
                </Col>
                <Col className="gutter-row" span={8}>
                  <FormDataView
                    heading={"Languages"}
                    // value={`${
                    //   Array.isArray(userData.language_spoken) &&
                    //   userData.language_spoken
                    //     ? userData.language_spoken.join(",")
                    //     : "-"
                    // }`}
                    value={`${
                      userData.language_spoken ? userData.language_spoken : "-"
                    }`}
                  />
                </Col>
                <Col className="gutter-row" span={8}>
                  <FormDataView
                    heading={"CRA credentials"}
                    value={`${
                      userData.CRA_credential ? userData.CRA_credential : "-"
                    }`}
                  />
                </Col>
              </Row>
              <Row>
                {" "}
                <Col className="gutter-row" span={8}>
                  <FormDataView
                    heading={"Background education"}
                    // value={`${
                    //   Array.isArray(
                    //     userData.background_education_information
                    //   ) && userData.background_education_information
                    //     ? userData.background_education_information.join(",")
                    //     : "-"
                    // }`}
                    value={`${
                      userData.background_education_information
                        ? userData.background_education_information
                        : "-"
                    }`}
                  />
                </Col>
              </Row>
            </Col>
            <Col className="gutter-row" span={4}>
              <FormFieldProfile>
                <FormFieldHeading>{"Profile Photo"}</FormFieldHeading>
                <ProfilePhoto
                  data-testid="profilePhoto"
                  src={
                    userData.profile_photo ? userData.profile_photo : DemoImage
                  }
                />
              </FormFieldProfile>
            </Col>
          </Row>
        </FormDevide>
      </FormContainer>
      <FormSectionWrapper>
        <H5Typography>Contact & address Details</H5Typography>
      </FormSectionWrapper>
      <FormContainer>
        <FormDevide firstChild={true}>
          <Row>
            <Col className="gutter-row" span={8}>
              <FormDataView
                data-testid="user-phone"
                heading={"Phone number"}
                value={`${
                  userData.phone_number
                    ? convertMobileNo(userData.phone_number)
                    : "-"
                }`}
              />
            </Col>
            <Col className="gutter-row" span={8}>
              <FormDataView
                heading={"Contact number"}
                value={`${
                  userData.cell_phone_number
                    ? convertMobileNo(userData.cell_phone_number)
                    : "-"
                }`}
              />
            </Col>
            <Col className="gutter-row" span={8}>
              <FormDataView
                heading={"Personal Email"}
                value={`${userData.email ? userData.email : "-"}`}
              />
            </Col>
          </Row>
          <Row>
            <Col className="gutter-row" span={8}>
              <FormDataView
                heading={"RAH Email"}
                value={`${userData.rah_email ? userData.rah_email : "-"}`}
              />
            </Col>
            <Col className="gutter-row" span={8}>
              <FormDataView
                heading={"Address"}
                value={
                  userData.address1 ||
                  userData.address2 ||
                  userData.city ||
                  userData.postal_code
                    ? `${userData.address1 ? userData.address1 + "," : ""}${
                        userData.address2 ? userData.address2 + "," : ""
                      } ${userData.city ? userData.city + "," : ""}${
                        userData.postal_code ? userData.postal_code : ""
                      }`
                    : "-"
                }
              />
            </Col>
          </Row>
        </FormDevide>
      </FormContainer>
      <FormSectionWrapper>
        <H5Typography>RAH Details</H5Typography>
      </FormSectionWrapper>
      <FormContainer>
        <FormDevide firstChild={true}>
          <Row>
            <Col className="gutter-row" span={8}>
              <FormDataView
                heading={"Branch"}
                value={`${userData.branch_name ? userData.branch_name : "-"}`}
              />
            </Col>
            <Col className="gutter-row" span={8}>
              <FormDataView
                heading={"Department"}
                value={`${
                  userData.department_name ? userData.department_name : "-"
                }`}
              />
            </Col>
            <Col className="gutter-row" span={8}>
              <FormDataView
                heading={"Role"}
                value={`${userData.role_name ? userData.role_name : "-"}`}
              />
            </Col>
          </Row>
        </FormDevide>
      </FormContainer>

      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center"
        }}
      >
        <TilteWrapper>
          <H3Title>User permission</H3Title>
        </TilteWrapper>
        <EditIconButton
          text={"Edit"}
          testId="EditPermission"
          icon={EditIcon}
          onClick={showDrawer}
        />
      </div>
      <FormContainer>
        <div style={{ width: "100%" }}>
          <UserPermission empId={userData.id} tableValues={tableValue} />
        </div>
      </FormContainer>
      <FormSectionWrapper>
        <H5Typography>Reporting to</H5Typography>
      </FormSectionWrapper>
      <FormContainer>
        <FormDevide firstChild={false}>
          <ReportingDiv>
            <FlexBox style={{ alignItems: "center" }}>
              <img
                alt="ProfileImage"
                style={{
                  width: "24px",
                  height: "24px",
                  marginRight: "10px",
                  borderRadius: "50%"
                }}
                src={userData.profile_photo || DemoImage}
              />
              <BiographyDiv style={{ marginBottom: "0px" }}>
                {userData.reporting_user_name
                  ? userData.reporting_user_name
                  : "-"}
              </BiographyDiv>

              <img
                alt="ReportIcon"
                style={{ width: "10px", height: "10px", marginLeft: "10px" }}
                src={ReportIcon}
              />
            </FlexBox>
          </ReportingDiv>
        </FormDevide>
      </FormContainer>
      <FormSectionWrapper>
        <H5Typography>Short biography</H5Typography>
      </FormSectionWrapper>

      <FormDevide firstChild={false}>
        <BiographyDiv>{`${
          userData.short_note ? userData.short_note : "-"
        }`}</BiographyDiv>
        {/* <BiographyDiv>
          {
            " She is a multilingual licensed Chartered Professional Accountant (CPA, CA) and Real Estate Broker, who started her career at PricewaterhouseCoopers after she obtained a Bachelor of Commerce Degree, Marketing Major from University of Toronto. Julia authored and co-authored articles in PROFIT and Business Credit magazines and has been recognized with various awards in her field. She has served as Treasurer, on the Board of Directors of “Orphan’s Hope Kids to Canada”, a Canadian adoption charity."
          }
        </BiographyDiv>
        <BiographyDiv>
          {
            "Through her strong leadership, she has gained the trust, respect, and support of many in the industry. She is excited to join RAHR, as her personal values and outlook of the real estate industry closely align with those of our fast-growing company."
          }
        </BiographyDiv> */}
      </FormDevide>

      {visible && (
        <EditRolesDrawer
          onClose={onClose}
          visible={visible}
          tableValues={tableValue}
          roleId={userData.role_id || userData.user_role_id}
          username={userData.username}
          profilePhoto={userData.profile_photo}
          roleNames={userData.role_name}
          onRoleUpdate={onRoleUpdate}
        />
      )}
    </FormWrapper>
  );
};

export default UserDetails;
