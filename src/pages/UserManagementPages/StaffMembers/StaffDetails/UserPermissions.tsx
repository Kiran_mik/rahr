import TableComponent from "@components/Table";
import React from "react";
import { Cross, Right } from "@assets/index";
import { Flex } from "@components/Flex";

const columns = [
  {
    title: "Module name",
    dataIndex: "moduleName",
    key: "Module name",
    width: "100%",
    render: (data: any) => (
      <div style={{ display: "flex", alignItems: "center" }}>
        {data.flag && (
          <span
            style={{
              width: "18px",
              height: "18px",
              marginRight: "10px",
              backgroundColor: "#F58A07",
              borderRadius: "50%",
              color: "#FFF",
              textAlign: "center"
            }}
          >
            !
          </span>
        )}
        {data.flag}
        {data.name}
      </div>
    )
  },
  {
    title: "View",
    dataIndex: "View",
    key: "View",
    render: (access: number) => (
      <div style={{ display: "flex", alignItems: "center" }}>
        <div
          style={{
            width: "53px",
            height: "24px",
            backgroundColor: "#9792E3",
            borderRadius: "8px",
            color: "#FFFFFF",
            textAlign: "center",
            fontSize: "12px",
            display: "flex",
            alignItems: "center",
            justifyContent: "center"
          }}
        >
          view
        </div>
        <span
          style={{
            display: Flex,
            justifyItems: "center",
            marginLeft: "10px "
          }}
        >
          {access === 0 ? (
            <img src={Cross} alt="Cross" />
          ) : (
            <img src={Right} alt="Right" />
          )}
        </span>
      </div>
    )
  },
  {
    title: "Edit",
    dataIndex: "Edit",
    key: "Edit",
    render: (access: number) => (
      <div style={{ display: "flex", alignItems: "center" }}>
        <div
          style={{
            width: "53px",
            height: "24px",
            backgroundColor: "#9792E3",
            borderRadius: "8px",
            color: "#FFFFFF",
            textAlign: "center",
            fontSize: "12px",
            display: "flex",
            alignItems: "center",
            justifyContent: "center"
          }}
        >
          view
        </div>
        <span
          style={{
            display: Flex,
            justifyItems: "center",
            marginLeft: "10px "
          }}
        >
          {access === 0 ? (
            <img src={Cross} alt="Cross" />
          ) : (
            <img src={Right} alt="Right" />
          )}
        </span>
      </div>
    )
  },
  {
    title: "Delete",
    dataIndex: "Delete",
    key: "Delete",
    render: (access: number) => (
      <div style={{ display: "flex", alignItems: "center" }}>
        <div
          style={{
            width: "53px",
            height: "24px",
            backgroundColor: "#9792E3",
            borderRadius: "8px",
            color: "#FFFFFF",
            textAlign: "center",
            fontSize: "12px",
            display: "flex",
            alignItems: "center",
            justifyContent: "center"
          }}
        >
          view
        </div>
        <span
          style={{
            display: Flex,
            justifyItems: "center",
            marginLeft: "10px "
          }}
        >
          {access === 0 ? <img src={Cross} /> : <img src={Right} />}
        </span>
      </div>
    )
  },
  {
    title: "Create",
    dataIndex: "Create",
    key: "Create",
    render: (access: number) => (
      <div style={{ display: "flex", alignItems: "center" }}>
        <div
          style={{
            width: "53px",
            height: "24px",
            backgroundColor: "#9792E3",
            borderRadius: "8px",
            color: "#FFFFFF",
            textAlign: "center",
            fontSize: "12px",
            display: "flex",
            alignItems: "center",
            justifyContent: "center"
          }}
        >
          view
        </div>
        <span
          style={{
            display: Flex,
            justifyItems: "center",
            marginLeft: "10px "
          }}
        >
          {access === 0 ? <img src={Cross} /> : <img src={Right} />}
        </span>
      </div>
    )
  }
];

interface UserPermissionProps {
  empId: any;
  tableValues: any;
}
export const UserPermission: React.FC<UserPermissionProps> = ({
  empId,
  tableValues
}) => {
  return (
    <>
      {tableValues && (
        <TableComponent
          tableData={tableValues.map((row: any) => ({
            key: row.id,
            moduleName: {
              name: row.title,
              flag: row.permissions
                ? Object.values(row.permissions).includes(0)
                  ? true
                  : false
                : false
            },
            View: row.permissions ? row.permissions.view : 0,
            Edit: row.permissions ? row.permissions.edit : 0,
            Delete: row.permissions ? row.permissions.delete : 0,
            Create: row.permissions ? row.permissions.create : 0
          }))}
          column={columns}
          tableSize="full"
        />
      )}
    </>
  );
};

export default UserPermission;
