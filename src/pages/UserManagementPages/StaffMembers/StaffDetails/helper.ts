import { GetServiceData } from '@services/UserService';

export const getProfile = async (empId:any) => {
    const res: any = await GetServiceData(empId);
    return res;
  };

export const convertMobileNo = (mobilleNo:any) => {
     return `+1 (${mobilleNo.slice(0,3)}) ${mobilleNo.slice(3,6)}-${mobilleNo.slice(6,10)}`
  };


