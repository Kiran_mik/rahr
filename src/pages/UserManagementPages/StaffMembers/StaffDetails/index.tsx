import React, { FC, useEffect, useState } from "react";
import { Link, RouteComponentProps } from "@reach/router";
import DashboardLayout from "@pages/UserManagementPages/StaffMembers/DashboardLayout";

import { BreadItem, UserBreadWrap } from "./style";
import StaffHeader from "@components/StaffHeader";
import { H3 } from "@components/Typography/Typography";
import UserDetails from "./UserDetails";
import Loader from "@components/Loader";
import { getProfile } from "./helper";
import { Breadcrumb, message } from "antd";
import AlertComponent from "@components/Alert";
import { GetStaffPermission } from "@services/UserService";
export interface StaffDetailsProps extends RouteComponentProps {
  empId?: any;
}
const StaffDetails: FC<StaffDetailsProps> = ({ empId }) => {
  const [loading, setLoading] = useState < boolean > (false);

  const [UserData, setUserData] = useState < any > ();
  const [tableValue, setTableValue] = useState < any > ();

  useEffect(() => {
    getProfileDetails();
  }, []);
  const getProfileDetails = async () => {
    setLoading(true);
    const res: any = await getProfile(empId);

    if (res.status === 200) {
      getRolePermissionDetails(res.data.data.user_role_id);
      setLoading(false);
      setUserData(res.data);
    } else {
      setLoading(false);
      message.error(res.error.message);
    }
  };
  const getRolePermissionDetails = async (role_id: any) => {
    const resRole: any = await GetStaffPermission(role_id);

    if (resRole.status === 200) {
      setTableValue([...resRole.data.data]);
    } else {
      // setLoading(false);

      message.error("something Went Wrong");
    }
  };
  return (
    <DashboardLayout>
      <StaffHeader>
        <UserBreadWrap>
          <H3 text="User details" />
          <Breadcrumb separator=">">
            <Link to="/usermanagement/staff-list" data-testid="gouserList">
              <BreadItem data-testid="breadcrumb-user" firstNav={true}>
                List of staffs
              </BreadItem>
            </Link>
            <BreadItem firstNav={false}>
              {UserData && UserData.data.username}
            </BreadItem>
          </Breadcrumb>
        </UserBreadWrap>
      </StaffHeader>
      <Loader loading={loading} />
      {UserData && (
        <UserDetails
          userData={UserData.data}
          tableValue={tableValue}
          getProfileDetails={getProfileDetails}
          empId={empId}
          onRoleUpdate={() => {
            getRolePermissionDetails(UserData.data.user_role_id);
          }}
        />
      )}
    </DashboardLayout>
  );
};

export default StaffDetails;
