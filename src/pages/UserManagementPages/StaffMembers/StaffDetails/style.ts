
import BreadcrumbItem from "antd/lib/breadcrumb/BreadcrumbItem";
import styled from "styled-components";
import { H3Typography } from "@components/Typography/Typography";
export const Breadcrumbs = styled.ul`
  list-style: none;
  padding: 0;
  margin-bottom:0px;
  & > li:after {
    font-size:12px;
    content: ">";
    padding: 0 8px;
  }
`;
export const UserBreadWrap = styled.div`
.ant-breadcrumb{
  font-size:12px;
  line-height:12px;
  margin-top:4px;
}
`;

export const Crumb = styled.li`
  display: inline-block;
  font-size:12px;
  line-height:12px;
  color:#17082D;
  font-family:Poppins;
  &:first-of-type a {
    color:#17082D;
  }
  &:last-of-type:after {
    content: "";
    padding: 0;
  }

  a {
    color: #9792E3;
    text-decoration: none;
    &:hover,
    &:active {
      text-decoration: underline;
    }
  }
`;


export const ButttonWrapper = styled.div`
  display:flex;
  justify-content: space-between;
`;
export const FormSectionWrapper = styled.div`
  padding: 24px 0 14px ;
  display:flex;
  justify-content: space-between;
`;
export const TilteWrapper = styled.div`
  padding: 24px 0 20px ;
  display:flex;
  justify-content: space-between;
`;


export const FormFieldProfile = styled.div`
 margin-bottom:21px;
`;
export const FormDevide = styled.div<{ firstChild: boolean }>`
 margin-bottom:${props => props.firstChild ? "0" : "10px "};
`;

export const FormContainer = styled.div`
 padding-bottom:13px;
margin-bottom:10px;
 border-bottom:1px dashed #A2A2BA;
`;


export const FieldBundleWrapper = styled.div`
 display:flex;
`;

export const ViewLabel = styled.div`
width: "53px",
height: "24px",
background-color: "#9792E3",
border-radius: "8px",
color: "#FFFFFF",
text-align: "center",
font-size: "12px"
`;
export const ProfilePhoto = styled.img`
vertical-align: middle;
width: 80px;
height:80px;
object-fit:cover;
border-radius: 50%;
  
`;
export const FormFieldHeading = styled.div`
 font-size: 12px;
    line-height: 20px;
    padding-bottom: 4px;
    color: #7B7B97;
    font-family: Poppins;
    font-weight: 400;
  
`;
export const ReportingDiv = styled.div`
display: flex;
justify-content: space-between;
align-items: center;
margin: 10px 0
  
`;
export const BiographyDiv = styled.p`
font-size: 14px;
    font-weight: 400;
    color: #17082D;
    margin-bottom: 32px;
    font-family: Poppins;
  &:last-child{
    margin-bottom:0px;
  }
`;
export const H3Title = styled(H3Typography)`
font-size:20px;
  }
`;


export const BreadItem = styled(BreadcrumbItem) <{ firstNav: boolean }>`
  color:${props => props.firstNav ? "#17082D" : "#9792E3"};
`
