import services from "@services/index"

export const signInUser = async (email: any, password: any) => {
	try {
		const param = { email: email, password: password }
		const response = await services.post("v1/login", param)
 
		return response
	} catch (err) {
		return { data: null, sucess: false,error:err.error }
	}
}

export const ForgotPassword = async (emailData:{email: string}) => {
	
	try {
		const response = await services.post("v1/forgot-password", emailData)
		return response
	} catch (err) {
		return { data: null, sucess: false ,msg: err.error.errors['email'][0]}
	}
}

export const resetPassword = async (token: string ,password:string) => {
	
	const param = { token: token, password: password }
	try {
		const response = await services.post("v1/set-password", param)
		return response
	} catch (err) {
		
		return { data: null, sucess: false,error: err.error}
	}
}
export const validateToken = async (token: any) => {
	
	try {
		const response:any = await services.getTest(`v1/validate-password-reset-token?token=${token}`)
		return { data: response.data, sucess: true}
	} catch (err) {
		
		return { data: null, sucess: false,error: err.error}
	}
}


