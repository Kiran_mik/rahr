import services from "@services/index";
import { isNamedExports } from "typescript";

export const GetStaffList = async (
  page: number,
  searchterm?: any,
  sortKey?: string,
  sortOrder?: string
) => {
  try {
    const response = await services.getTest(
      `v1/staffs?page=${page}${searchterm ? "&search=" + searchterm : ""}${
        sortKey ? `&sortBy=${sortKey}&order=${sortOrder}` : ""
      }`
    );
    return response;
  } catch (err) {
    return { data: null, success: false };
  }
};

export const GetBranchList = async (page: number, searchterm?: any, sortKey?: string, sortOrder?: string) => {
	try {
		const response = await services.getTest(
			`v1/branches?page=${page}${searchterm ? "&search=" + searchterm : ""}${
				sortKey ? `&sortBy=${sortKey}&order=${sortOrder}` : ""
			}`
		)

		return response
	} catch (err) {
		return { data: null, success: false }
	}
}

export const GetDepartmentList = async (page: number, searchterm?: any, sortKey?: string, sortOrder?: string) => {
	try {
		const response = await services.getTest(
			`v1/departments?page=${page}${searchterm ? "&search=" + searchterm : ""}${
				sortKey ? `&sortBy=${sortKey}&order=${sortOrder}` : ""
			}`
		)
		return response
	} catch (err) {
		return { data: null, success: false }
	}
}

export const GetDeptStaffList = async (name?: string) => {
  try {
    const response = await services.getTest(`v1/departments/staff-list`);
    return response;
  } catch (err) {
    return { data: null, sucess: false };
  }
};
// api/v1/staffs/support-data
