import services from "@services/index";

export const GetBranchDetails = async (branchId?: any) => {
	try {
		const response = await services.getTest(`v1/branches/${branchId}`)
		return response
	} catch (err) {
		return { data: null, success: false ,error: err.error}
	}
}

export const GetBranchMembers = async (branchId?: any) => {
	try {
		const response = await services.getTest(`v1/branches/members/${branchId}`)
		return response
	} catch (err) {
		return { data: null, success: false,error: err.error }
	}
}
export const GetBranchMembersAdmins = async () => {
	try {
		const response = await services.getTest(`v1/branches/managers-admins`)
		return response
	} catch (err) {
		return { data: null, success: false,error: err.error }
	}
}
export const addBranch = async (data?: any) => {
	try {
		const response = await services.post("v1/branches", data)
		return response
	} catch (err) {
		return { data: null, sucess: false ,msg: err.error}
	}
}

export const updateBranch = async (data?: any) => {
	try {
		const response = await services.put(`v1/branches/${data.id}`, data)
		return response
	} catch (err) {
		return { data: null, sucess: false, msg: err.error }
	}
}
export const getBranchInfo = async (branch_id: number) => {
	try {
		const response = await services.getTest(`v1/branches/${branch_id}`)
		return response
	} catch (err) {
		return { data: null, sucess: false ,msg: err.error}
	}
}

export const searchEmployee = async (searchTerm?: any) => {
	try {
		const response = await services.getTest(`v1/branches/search-employees/${searchTerm}`)
		return response
	} catch (err) {
		return { data: null, sucess: false ,msg: err.error}
	}
}

export const addEmployeeToBranch = async (branch_id?: number,employees?:[number]) => {
	try {
		const response = await services.post(`v1/branches/employees`,{branch_id:branch_id,employee_ids:employees})
		return response
	} catch (err) {
						//@ts-ignore
						return { data: null, sucess: false, msg: err.error }
					}
}
export const getEmployeeViaBranchId = async (branch_id?: number) => {
	try {
		const response = await services.getTest(`v1/branches/employees/${branch_id}`)
		return response
	} catch (err) {
		return { data: null, sucess: false, msg: err.error }
	}
}

