import services from "@services/index";

export const getDepartmentDetails = async (departmentId?: any) => {
	try {
		const response = await services.getTest(`v1/departments/${departmentId}`)
		return response
	} catch (err) {
		return { data: null, success: false ,error: err.error}
	}
}


export const getDepartmentRoles = async (departmentId?: any) => {
	try {
		const response = await services.getTest(`v1/departments/${departmentId}/roles`)
		return response
	} catch (err) {
		return { data: null, success: false ,error: err.error}
	}
}

export const UpdateDeptDetails = async (departmentId: any,department_name:any,manager_id?:any)=>{

	try{
	
		const data = {
			"id": departmentId,
			"department_name":department_name,
			"manager_id":manager_id
		
		  }
	const response =await services.put(`v1/departments/${departmentId}`,data)
	
	return response
	
	}catch (err) {
		return { data: null, sucess: false,error: err.error}
	}}
	
	export const SendStaff = async (id:any,staffId:any) => {
		try {
			const data = {
				"staff_id": staffId,
				"id":id
			  }
			const response = await services.post(`v1/departments/staff/${id}`,data)
			return {response: response, success:true}
			
		} catch (err) {
			return {response:err
			,sucess: false }
		}
	}

export const GetDepartmentList = async (searchterm?: any) => {
	try {
		const response = await services.getTest(`v1/departments${searchterm ? '?search='+searchterm:''}`)
		return response
	} catch (err) {
		return { data: null, success: false }
	}
}
