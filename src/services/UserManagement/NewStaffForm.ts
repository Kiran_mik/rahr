import services from "@services/index"
import axios from "axios"
import { AnyObject } from "yup/lib/object"
import { AnyMessageParams } from "yup/lib/types"

export const SendStaffDetails = async (values:any) => {
    try {
		const param = {
			"first_name": values.first_name,
			"middle_name": values.middle_name,
			"last_name": values.last_name,
			"dob": values.dob,
			"rah_email": values.rah_email,
			"email": values.email,
			"phone_number": values.phone_number,
			"cell_phone_number":values.cell_phone_number,
			"is_real_estate_license": values.is_real_estate_license,
			"background_education_information": values.background_education_information,
			"language_spoken": values.language_spoken,
			"CRA_credential": values.CRA_credential,
			"address1":values.address1,
			"address2": values.address2,
			"city": values.city,
			"postal_code": values.postal_code,
			"profile_photo": values.profile_photo,
			"rha_joind_date": values.rha_joind_date,
			"short_note": values.short_note,
			"reporting_user_id":values.reporting_user_id ,
			"user_branche_id": values.user_branche_id,
			"user_department_id":values.user_department_id,
			"user_role_id": values.user_role_id
			
			}
		const response = await services.post("v1/staffs", param)
		return {response: response, success:true}
	} catch (err) {
		return {response:err
		,sucess: false }
	}
}

export const UpdateStaffDetails = async (staffId:any,values:any)=>{
	try{
		const param = {
			"first_name": values.first_name,
			"middle_name": values.middle_name,
			"last_name": values.last_name,
			"dob": values.dob,
			"rah_email": values.rah_email,
			"email": values.email,
			"phone_number": values.phone_number,
			"cell_phone_number":values.cell_phone_number,
			"is_real_estate_license": values.is_real_estate_license,
			"background_education_information": values.background_education_information,
			"language_spoken": values.language_spoken,
			"CRA_credential": values.CRA_credential,
			"address1":values.address1,
			"address2": values.address2,
			"city": values.city,
			"postal_code": values.postal_code,
			"profile_photo": values.profile_photo,
			"rha_joind_date": values.rha_joind_date,
			"short_note": values.short_note,
			"reporting_user_id":values.reporting_user_id,
			"user_branche_id":values.user_branche_id,
			"user_department_id":values.user_department_id,
			"user_role_id": values.user_role_id,
			"id":values.id
			}
	const response =await services.put(`v1/staffs/${staffId}`,param)
	
	return response
	
	}catch (err) {
		return { data: err, sucess: false}
	}}
	export const GetSupportDataStaff = async (name?:string) => {
		try {
		
			const response = await services.getTest(`v1/staffs/support-data`)
			return response
		} catch (err) {
			return { data: null, sucess: false }
		}
	}



export const ImageUpload = async (values:any) => {
		let formData = new FormData();
		formData.append("imageFile", values);

	axios
	.post(
	  "https://7ngsa15qzk.execute-api.ca-central-1.amazonaws.com/api/v1/image/upload",
	  formData
	)
	.then((res: any) => {
	  if (res.data.data.url) {
	  } else {}
	})
	.catch((err: any) => {
	  return err.error;
	});

  
	
}
