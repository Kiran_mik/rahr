import { AGENT_ONBOARDING_BASE_URL } from './../pages/AgentOnboardingPages/constants';
import axios from "axios";
import services from "@services/index"
import { userActions } from "@store/userProfileReducer";
import store from "@store/index";


export const getDocuSignLink = async (url: string, documentURL: string) => {
	try {

		if (!documentURL) {
			const res = await services.get(url) as { data: { data: any } }
			return res.data.data;
		}
	} catch (error) {
		return error;
	}
}

export const getAllBracnhes=async()=>{
	
}

export const checkDocuSign = async (data: any,docuSignURL?:string) => {
	try {

		const res = await services.post(`${AGENT_ONBOARDING_BASE_URL}${docuSignURL || '/review-agreements/docusign'}`, { ...data }) as { data: { data: any } }
		return res;
	} catch (error) {
		return error;

	}
}
export const uploadImage = (image: any) => {
	return new Promise((resolve, reject) => {
		if (typeof image === 'string') {
			return resolve(image);
		}
		let formData = new FormData();

		formData.append("imageFile", image);
		axios
			.post(
				(process.env.REACT_APP_DOCUMENT_S3+"image/upload") || "",
				formData
			)
			.then(async (res: any) => {
				if (res.data.data.url) {
					return resolve(res.data.data.url)

				}
			}).catch(err => {
				return reject(err)
			});

	})
}

export const uploadFile = (doc: any,type:string) => {
	return new Promise((resolve, reject) => {
		if (typeof doc === 'string') {
			return resolve(doc);
		}
		let formData = new FormData();

		formData.append("file", doc);
		formData.append("type", type);
		axios
			.post(
				(process.env.REACT_APP_DOCUMENT_S3+"file/upload") || "",
				formData
			)
			.then(async (res: any) => {
				if (res.data.data.url) {
					return resolve(res.data.data.url)

				}
			}).catch(err => {
				return reject(err)
			});

	})
}


export const GetServiceData = async (profileId: string) => {

	try {
		const response = await services.getTest(`v1/staffs/${profileId}`)
		return response
	} catch (err) {
		//@ts-ignore
		return { data: null, sucess: false, error: err.error }
	}
}
export const GetStaffPermission = async (profileId: string) => {
	try {
		const response = await services.getTest(`v1/staff/role-permissions/${profileId}`)
		return response
	} catch (err) {
		return { data: null, sucess: false }
	}
}
export const GetManagerList = async (name?: string) => {
	try {
		const response = await services.getTest(`v1/departments/managers/${name}`)
		return response
	} catch (err) {
		return { data: null, sucess: false }
	}
}


export const UpdateRoleDetails = async (roleId: any, roleName: string, permissions: any) => {

	try {

		const data = {
			"role_name": roleName,
			"role_id": roleId,
			"permissions": permissions
		}
		const response = await services.put(`v1/staff/role-permissions/${roleId}`, data)

		return response

	} catch (err) {
		return { data: null, sucess: false }
	}
}

export const SendRoleDetails = async (roleName: string, permissions: any) => {
	try {
		const data = {
			"role_name": roleName,
			"permissions": permissions
		}
		const response = await services.post("v1/staff/role-permissions", data)
		return { response: response, success: true }

	} catch (err) {
		return {
			response: err
			, sucess: false
		}
	}
}
export const SendDepartments = async (deptName: string, managerId: any, deptRoles: any) => {
	try {
		const data = {
			department_name: deptName,
			manager_id: managerId,
			department_roles: deptRoles,
		}
		const response = await services.post("v1/departments", data)
		return { response: response, success: true }

	} catch (err) {
		return {
			response: err
			, sucess: false
		}
	}
}


export const getUserInfo = async () => {
    try {
        const res = await services.get('v1/user-info') as { data: { data: any } }
        return res.data.data;
    } catch (error) {
        return error
    }
}

export const updateUserStore = async () => {
    const res = await getUserInfo();
    const { userInfo = { name: {} }, join_as_a_parked_agent } = res;
     store.dispatch(
        userActions.setUser({
			...userInfo,
            role: userInfo.role,
            currentOnboardingStep: res.currentOnboardingStep,
            ...userInfo.name,
            onBoardingStepsStatus: res.currentOnboardingStep,
			join_as_a_parked_agent:join_as_a_parked_agent
        })
    );
    return true
}