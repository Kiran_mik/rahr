import { userActions } from "@store/userProfileReducer/index";
import { authActions } from "@store/authReducer";
import axios from "axios";
import { getBaseURL } from "@constants/index";
import Cookies from "js-cookie";
import store from "@store/index";

type RequestType = null | undefined | string;
class Serivce {
  get = async (url: string, basePath?: string) => {
    return new Promise((resolve, reject) => {
      const BASE_URL = basePath
        ? basePath
        : url === "v1/user-info"
          ? process.env.REACT_APP_BASE_URL_original
          : getBaseURL();

      try {
        axios
          .get(`${BASE_URL}${url}`, {
            headers: {
              "Content-Type": "application/json;charset=UTF-8",
              "Access-Control-Allow-Origin": "*",
              "Access-Control-Allow-Headers": "X-Requested-With,content-type",
              "Access-Control-Allow-Methods":
                "GET, POST, OPTIONS, PUT, PATCH, DELETE",
              Authorization: `Bearer ${this.getAuthToken()}`,
            },
          })
          .then((res) => {
            return resolve({ data: res.data, status: res.status });
          })
          .catch((err) => {
            if (err.response && err.response.status === 401) {
              store.dispatch(userActions.setUser(null));

              store.dispatch(authActions.setAuth(null));

              Cookies.remove("token");
              window.location.href = "/";
              return;
            }
            if (err.isAxiosError && err.response) {
              const errResponse = err.response;
              return reject({
                status: errResponse.status,
                error: errResponse.data,
                //message:  errResponse.data.message
                //message: errResponse?.data?.message || errResponse?.message,
              });
            }
            if (err) {
              return reject({
                status: 400,
                error: { message: err.message },
              });
            }
          });
      } catch (err) {
        return reject(err);
      }
    });
  };

  getAuthToken = () => {
    return Cookies.get("token") ? Cookies.get("token") : "";
  };

  getTest = async (url: string, basePath?: string) => {
    return new Promise((resolve, reject) => {
      const BASE_URL = basePath
        ? basePath
        : url === "v1/user-info"
          ? process.env.REACT_APP_BASE_URL_original
          : getBaseURL();

      try {
        axios
          .get(`${BASE_URL}${url}`, {
            headers: {
              "Content-Type": "application/json;charset=UTF-8",
              "Access-Control-Allow-Origin": "*",
              "Access-Control-Allow-Headers": "X-Requested-With,content-type",
              "Access-Control-Allow-Methods":
                "GET, POST, OPTIONS, PUT, PATCH, DELETE",
              Authorization: `Bearer ${this.getAuthToken()}`,
            },
          })
          .then((res) => {
            return resolve({ data: res.data, status: res.status });
          })
          .catch((err) => {
            if (err.response && err.response.status === 401) {
              store.dispatch(userActions.setUser(null));

              store.dispatch(authActions.setAuth(null));

              Cookies.remove("token");
              window.location.href = "/";
              return;
            }
            if (err.isAxiosError && err.response) {
              const errResponse = err.response;
              return reject({
                status: errResponse.status,
                error: errResponse.data,
                //message:  errResponse.data.message
                //message: errResponse?.data?.message || errResponse?.message,
              });
            }
            if (err) {
              return reject({
                status: 400,
                error: { message: err.message },
              });
            }
          });
      } catch (err) {
        return reject(err);
      }
    });
  };
  post = (url: string, params: {}, basePath?: string) => {
    return new Promise((resolve, reject) => {
      const BASE_URL = basePath
        ? basePath
        : url === "v1/user-info"
          ? process.env.REACT_APP_BASE_URL_original
          : getBaseURL();

      try {
        axios
          .post(
            `${BASE_URL}${url}`,
            { ...params },
            {
              headers: {
                "Content-Type": "application/json;charset=UTF-8",
                "Access-Control-Allow-Origin": "*",
                Accept: "application/json",
                "Access-Control-Allow-Headers": "X-Requested-With,content-type",
                "Access-Control-Allow-Methods":
                  "GET, POST, OPTIONS, PUT, PATCH, DELETE",
                Authorization: `Bearer ${this.getAuthToken()}`,
              },
            }
          )
          .then((res: { data: any; status: number }) => {
            return resolve({
              ...res.data,
              status: res.status,
              success: res.status === 200,
            });
          })
          .catch((err) => {
            if (err.response && err.response.status === 401) {
              store.dispatch(userActions.setUser(null));

              store.dispatch(authActions.setAuth(null));
              Cookies.remove("token");
              window.location.href = "/";
              return;
            }
            if (err.isAxiosError && err.response) {
              //.eslint
              const errResponse: any = err.response;
              return reject({
                status: errResponse.status,
                error: errResponse.data,
                // message: errResponse?.data?.message.message
                //   ? errResponse?.data?.message.message
                //   : errResponse?.data?.message,
              });
            }
            return reject({
              status: 400,
              error: { message: err.message },
            });
          });
      } catch (err) {
        return reject(err);
      }
    });
  };

  put = (url: string, params: {}, type?: RequestType) => {
    return new Promise((resolve, reject) => {
      const BASE_URL =
        url === "v1/user-info"
          ? process.env.REACT_APP_BASE_URL_original
          : getBaseURL();

      try {
        axios
          .put(
            `${BASE_URL}${url}`,
            { ...params },
            {
              headers: {
                "Content-Type": "application/json;charset=UTF-8",
                "Access-Control-Allow-Origin": "*",
                Accept: "application/json",
                "Access-Control-Allow-Headers": "X-Requested-With,content-type",
                "Access-Control-Allow-Methods":
                  "GET, POST, OPTIONS, PUT, PATCH, DELETE",
                Authorization: `Bearer ${this.getAuthToken()}`,
              },
            }
          )
          .then((res: { data: any; status: number }) => {
            return resolve({
              ...res.data,
              status: res.status,
              success: res.status === 200,
            });
          })
          .catch((err) => {
            if (err.isAxiosError && err.response) {
              //.eslint
              const errResponse: any = err.response;
              return reject({
                status: errResponse.status,
                error: errResponse.data,
                // message: errResponse?.data?.message.message
                //   ? errResponse?.data?.message.message
                //   : errResponse?.data?.message,
              });
            }
            return reject({
              status: 400,
              error: { message: err.message },
            });
          });
      } catch (err) {
        return reject(err);
      }
    });
  };
}

export default new Serivce();
