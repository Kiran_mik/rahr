import services from ".";

export const getMasterStatuses = async (statusType:string) => {
    try {
      const res = await services.get(
        `v1/master-statuses?type=${statusType}`,
        process.env.REACT_APP_BASE_URL_GLOBAL_SETTINGS
      );
      //@ts-ignore
      return res.data.data;
    } catch (err) {
      return [];
    }
  };
  