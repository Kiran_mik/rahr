import React, { Component } from "react";
import { Collapse } from "antd";
import { Accordian, AccordianWrapper, ClockIcon, CheckedIcon } from "./style";
const { Panel } = Collapse;

const AccoridanPanel = (props: any) => {
  let {
    title,
    children,
    accordianKey,
    defaultActiveKey,
    status,
    disabled,
    onClick,
  } = props;

  const getIcon = () => {
    return status == 1 ? <CheckedIcon /> : <ClockIcon />;
  };

  return (
    <AccordianWrapper>
      <Accordian
        bordered={false}
        defaultActiveKey={[defaultActiveKey]}
        expandIcon={({ isActive }) => getIcon()}
        className="site-collapse-custom-collapse"
        activeKey={defaultActiveKey}
        onChange={onClick}
      >
        <Panel
          disabled={disabled}
          header={<span className="title-header">{title}</span>}
          key={accordianKey}
          className="site-collapse-custom-panel"
        >
          {children}
        </Panel>
      </Accordian>
    </AccordianWrapper>
  );
};

export default AccoridanPanel;
