import styled from "styled-components";
import { Collapse } from "antd";
import { ClockCircleFilled, CheckCircleFilled } from "@ant-design/icons";

export const AccordianWrapper = styled.div`
  .ant-collapse-borderless {
    background-color: #fff;
    border: 0;
  }

  .ant-collapse-borderless
    > .ant-collapse-item:last-child
    .ant-collapse-header {
    border-radius: 8px !important;
  }

  .ant-collapse-borderless > .ant-collapse-item:last-child,
  .ant-collapse-borderless
    > .ant-collapse-item:last-child
    .ant-collapse-header {
    border-radius: 8px !important;
  }

  .ant-collapse-content {
    background-color: #fff !important;
  }
`;

export const Accordian = styled(Collapse)`
  .site-collapse-custom-panel {
    overflow: hidden;
    background: #fcfcff;
    border: 1px solid #9792e3;
    border-radius: 8px;
    margin-bottom: 20px;

    .title-header {
      font-size: 16px;
      line-height: 24px;
      color: #17082d;
      font-weight: 500;
      font-family: Poppins;
    }
  }
  .site-collapse-custom-collapse .site-collapse-custom-panel {
    background: rgba(255, 255, 255, 0.04);
    border: 0px;
  }
`;

export const ClockIcon = styled(ClockCircleFilled)`
  font-size: 15px !important;
  color: #b7b7bc;
`;

export const CheckedIcon = styled(CheckCircleFilled)`
  font-size: 15px !important;
  color: #31af91;
`;
