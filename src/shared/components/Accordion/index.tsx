import React, { FC } from "react";
import { Collapse } from "antd";

const { Panel } = Collapse;
export interface AccordionProps {
  className?: string;
  children?: React.ReactNode;

  title?: string;
}
const Accordion: FC<AccordionProps> = ({ children, title, className }) => {
  return (
    <div>
      <Collapse ghost defaultActiveKey={["1"]}>
        <Panel header={title} key="1">
          {children}
        </Panel>
      </Collapse>
    </div>
  );
};

export default Accordion;
