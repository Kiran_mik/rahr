import { H3 } from "@components/Typography/Typography";
import React, { FC } from "react";
import { ContentWrapper } from "./style";
import { Col, Row } from "antd";

interface ConfirmationScreenProps {
  children: React.ReactNode;
  subHeader: string;
  rightImage: string;
}

const ConfirmationScreen: FC<ConfirmationScreenProps> = ({
  children,
  subHeader,
  rightImage,
}) => {
  return (
    <div>
      <ContentWrapper className="bordernone">
        <Row gutter={16}>
          <Col span={13}>
            <H3 className="font-500 space-bottom-16" text={subHeader} />
            {children}
          </Col>
          <Col span={11} style={{ textAlign: "center" }}>
            <img src={rightImage} alt="" width="70%" />
          </Col>
        </Row>
      </ContentWrapper>
    </div>
  );
};

export default ConfirmationScreen;
