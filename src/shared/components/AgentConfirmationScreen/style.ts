import styled from "styled-components";

export const ContentWrapper = styled.div`
  border-bottom: 1px dashed #a2a2ba;
  padding: 33px 0px;
  width: 100%;
  &.bordernone {
    border-bottom: none;
  }
  &.space-bottom-0 {
    padding-bottom: 0px;
  }
  .borderbtn {
    border: 1px solid rgb(78, 28, 149);
    border-color: #4e1c95;
    padding: 3px 10px;
    font-size: 14px;
    border-radius: 8px;
  }
`;

export const FormHeader = styled.div`
  display: flex;
  padding: 24px 48px;
  border-bottom: 1px solid #e2e2ee;
`;