import React, { FC } from "react";
import { Alert, Collapse } from "antd";

export interface AlertProps {
  message?: string;
  type?: "success" | "info" | "warning" | "error" | undefined;
}
const AlertComponent: FC<AlertProps> = ({ message, type }) => {
  return <Alert message={message} type={type} showIcon closable />;
};

export default AlertComponent;
