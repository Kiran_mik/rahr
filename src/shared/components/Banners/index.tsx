import React, { FC, lazy, Suspense } from "react";
// import Box from "@material-ui/core/Box";
import { BannerProps } from "./types";
import { BannerH1, Container } from "./style";
import { Flex } from "@components/Flex";

const BreadCrumb: FC<BannerProps> = ({ title, image }) => {
  return (
    <Container image={image}>
      <div style={{ height: "100%", backgroundColor: "rgba(19, 31, 48, 0.5)" }}>
        <Flex justifyContent={"space-evenly"}>
          <BannerH1>{title}</BannerH1>
        </Flex>
      </div>
    </Container>
  );
};
export default BreadCrumb;
