import styled from "styled-components";
import { BannerProps } from "./types";
import login from "@assets/login.png";

export const Container = styled.div<BannerProps>`

   background-image: ${(props: any) =>
    props.image ? "url(" + props.image + ")" : ""};
   background-size: contain;
   background-position: right bottom;
   background-repeat: no-repeat;
   min-height:calc(100vh - 80px);
     max-height:calc(100vh - 80px);
position:relative;
   width:100%;
    background-color: #faf4f8;
    &:before{
            background-color: #152a44;
    content: "";
    margin: 0 auto;
    position: absolute;
    
    bottom: 0px;
    left: 0px;

    width: 100%;
    height: 5%;
    z-index: 1;
    
    }
}

@media (max-width: 600px) {
  display:none;
}
`;
export const BannerH1 = styled.h1`
  color: #fff;
    font-size: 50px;
    font-weight: 700;
    font-family: Roboto;
    width: 294px;
    line-height: 52px;
    padding-top: 70px;
    @media(max-width:1400px){
    font-size: 43px;
    font-family: Roboto;
    width: 294px;
    line-height: 43px;
    padding-top: 34px;
    }
`;
