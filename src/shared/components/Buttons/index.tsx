import React, { FC } from "react";
import { Spin } from "antd";
import {
  PrimaryButton,
  SecondaryButton,
  PrimaryHalfButton,
  TransparentButton,
  DrawerBorderButton,
  DrawerPrimaryButton,
  PrimaryIconButton,
  SecondaryIconButton,
  AddButton,
  UpdateButton,
  BorderButton,
} from "./style";
export interface ButtonProps {
  text?: string | React.ReactNode;
  onClick?: any;
  className?: any;
  icon?: any;
  testId?: string;
  style?: any;
  type?: string;
  isLoading?: boolean;
  isDisable?: boolean;
}
export const Primary: FC<ButtonProps> = ({
  text,
  onClick,
  type,
  testId,
  style,
  className = "",
  isLoading,
  isDisable,
}) => {
  return (
    <PrimaryButton
      style={style}
      //@ts-ignore
      type={type}
      onClick={(e) => !isLoading && onClick && onClick(e)}
      data-testid={testId && testId}
      className={className}
      disabled={isDisable || isLoading}
    >
      {isLoading && (
        <div style={{ position: "absolute", left: "50%" }}>
          <Spin />
        </div>
      )}
      {text}
    </PrimaryButton>
  );
};

export const Secondary: FC<ButtonProps> = ({ text }) => {
  return <SecondaryButton>{text}</SecondaryButton>;
};
export const Transparent: FC<ButtonProps> = ({
  text,
  onClick,
  className,
  style,
}) => {
  return (
    <TransparentButton style={style} onClick={onClick} className={className}>
      {text}
    </TransparentButton>
  );
};
export const PrimaryHalf: FC<ButtonProps> = ({ text, onClick }) => {
  return <PrimaryHalfButton onClick={onClick}>{text}</PrimaryHalfButton>;
};

export const DrawerPrimaryBtn: FC<ButtonProps> = ({
  text,
  onClick,
  type,
  testId,
  style,
  className = "",
  isLoading,
  isDisable,
}) => {
  return (
    <DrawerPrimaryButton
      style={style}
      //@ts-ignore
      type={type}
      onClick={(e) => !isLoading && onClick && onClick(e)}
      data-testid={testId && testId}
      className={className}
      disabled={isDisable || isLoading}
    >
      {isLoading ? <Spin /> : text}
    </DrawerPrimaryButton>
  );
};
export const DrawerBorderBtn: FC<ButtonProps> = ({ text, onClick }) => {
  return <DrawerBorderButton onClick={onClick}>{text}</DrawerBorderButton>;
};
export const IconButton: FC<ButtonProps> = ({
  text,
  icon,
  onClick,
  testId,
  className,
}) => {
  return (
    <PrimaryIconButton onClick={onClick} data-testid={testId}>
      {icon && <img style={{ marginRight: "10px" }} src={icon} />} {text}
    </PrimaryIconButton>
  );
};

export const EditIconButton: FC<ButtonProps> = ({
  text,
  icon,
  onClick,
  testId,
  style,
}) => {
  return (
    <SecondaryIconButton style={style} onClick={onClick} data-testid={testId}>
      <img style={{ marginRight: "10px" }} src={icon} />
      {text}
    </SecondaryIconButton>
  );
};

export const AddBtn: FC<ButtonProps> = ({ text, icon, onClick, testId }) => {
  return (
    <AddButton onClick={onClick} data-testid={testId}>
      {text}
    </AddButton>
  );
};

export const UpdateBtn: FC<ButtonProps> = ({ text, icon, onClick, testId }) => {
  return (
    <UpdateButton onClick={onClick} data-testid={testId}>
      {text}
    </UpdateButton>
  );
};

export const BorderBtn: FC<ButtonProps> = ({
  text,
  icon,
  style,
  onClick,
  testId,
  isLoading,
}) => {
  return (
    <BorderButton
      style={{ cursor: "pointer", ...style }}
      onClick={onClick}
      data-testid={testId}
    >
      {isLoading ? (
        <Spin   />
      ) : (
        <>
          {typeof icon === "string" ? (
            <img style={{ marginRight: "10px" }} src={icon} />
          ) : (
            icon
          )}
          {text}
        </>
      )}
    </BorderButton>
  );
};
