import styled from "styled-components";

export const PrimaryButton = styled.button`

	width: 100%;
    background-color: #4E1C95;
    background: #4E1C95;
    border-color: #4E1C95;
    height: auto;
    padding: 8px 12px;
    font-size: 16px;
    line-height: 32px;
    font-weight: 600;
    border-radius: 8px;
    letter-spacing: 0px;
    color: #fff;
    font-family: Poppins;
	cursor:pointer;
    position:relative;
    .ant-spin-dot-item{
        // background-color:white;
    }
	&.halfBtn {
		min-width: 49%;
	}
	&.inviteBtn {
		width: 200px !important;
		@media (max-width: 600px) {
			width: 100% !important;
		}
	}
	&:disabled {
		background: #F5F5FF !important;
        color: #A2A2BA;
		cursor: default;
	}
    &.addbtn{
            font-size: 14px;
    line-height: 22px;
    font-weight: 400;
    padding: 12px 24px;
    width: max-content;
    }
    &.maxWidth{
        width:max-content;
        padding:36px 4px; 
    }
    &.font-400{
        font-weight:400;
    }
    &.purple{
        background:rgba(151, 146, 227, 1)
    }
`;

export const SecondaryButton = styled.button`
width:auto;

	background: #f58a07;
	border-radius: 8px;
	text-align: center;
	text-transform: capitalize;
	color: #ffffff;
	font-family: Poppins;
	padding: 8px 27px;
	font-size:16px;
	font-weight:500;
	line-height:32px;
	border: none;
	outline: none;
	cursor: pointer;
`;
export const TransparentButton = styled.button`

		    background: transparent;
    font-size: 14px;
    line-height: 22px;
    color: #4E1C95;
    font-family: Poppins;
    font-weight: 400;
	cursor pointer;
padding:12px 24px;
&:focus{
outline: 1px solid #4E1C95;
}
`;

export const PrimaryHalfButton = styled.button`
width:auto;
	background: #4E1C95;
	border-radius: 8px;
	text-align: center;
	text-transform: capitalize;
	color: #ffffff;
	font-family: Poppins;
	padding: 8px 27px;
	font-size:16px;
	font-weight:500;
	line-height:32px;
	border: none;
	outline: none;
	cursor: pointer;
`;
export const PrimaryIconButton = styled.button`
	background: #fff;
    border: 1px solid #BDBDD3;
    border-radius: 8px;
    text-align: center;
    padding: 5px 16px 5px 16px;
    color: #17082D;
    font-family: Poppins;
    display: flex;
    align-items: center;
	cursor:pointer;
    line-height:22px !important;
`;

export const SecondaryIconButton = styled.button`
	background: #fff;
    border: 1px solid #BDBDD3;
    border-radius: 8px;
    text-align: center;
    padding: 5px 27px 5px 27px;
    color: #17082D;
    font-family: Poppins;
    display: flex;
    align-items: center;
	cursor:pointer;
    line-height:22px !important;
`;

export const DrawerBorderButton = styled.button`
	background: transparent;
    border-radius: 8px;
    text-align: center;
    text-transform: capitalize;
    color: #17082D;
    font-family: Poppins;
    padding: 12px 24px;
    font-size: 14px;
    font-weight: 400;
    line-height: 22px;
    border: none;
    outline: none;
	border:1px solid #BDBDD3;
    cursor: pointer;
`;

export const DrawerPrimaryButton = styled.button`
	background: #4E1C95;
    border-radius: 8px;
    text-align: center;
    text-transform: capitalize;
    color: #ffffff;
    font-family: Poppins;
    padding: 12px 24px;
    font-size: 14px;
    font-weight: 400;
    line-height: 22px;
    border: none;
    outline: none;
    cursor: pointer;
    &:disabled {
		background: #F5F5FF;
        color: #A2A2BA;
		cursor: default;
	}
`;
export const AddButton = styled.button`
	background: rgba(78, 28, 149, 1);
    border-radius: 8px;
    color: #ffffff;
    font-family: Poppins;
    padding: 5px 44px;
    font-size: 14px;
    font-weight: 400;
    line-height: 22px;
    border: none;
    outline: none;
    cursor: pointer;
`;

export const UpdateButton = styled.button`
	background: rgba(151, 146, 227, 1);
    border-radius: 8px;
    color: #ffffff;
    font-family: Poppins;
    padding: 4px 36px;
    font-size: 14px;
    font-weight: 400;
    line-height: 32px;
    border: none;
    outline: none;
    cursor: pointer;
`;

export const BorderButton = styled.button`
	color:#17082D;
    border: 1px solid #BDBDD3;
    padding:13px 24px;
    color:#17082D;
    font-sie:14px;
    line-height:20px;
    font-weight:600;
    border-radius:10px;
    background:transparent;
`;