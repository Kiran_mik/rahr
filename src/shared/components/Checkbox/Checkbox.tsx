import React from "react";
import { Checkbox } from "antd";
import { Block, Flex } from "@components/Flex";
import Error from "@components/Error";

export interface CheckboxProps {
  name?: string;
  onChange?: any;
  defaultChecked?: any;
  checked?: any;
  disabled?: any;
  id?: string;
  error?: string;
  style?: any;
}
const CheckboxComponent: React.FC<CheckboxProps> = ({
  error,
  id,
  name,
  onChange,
  checked,
  style,
}) => {
  return (
    <Flex direction="column">
      <Flex>
        <Checkbox
          checked={checked}
          name={id}
          id={id}
          onChange={onChange}
          style={style}
        />
        <span style={{marginLeft:8}}> {name}</span>
      </Flex>

      <span>{error ? <Error error={error} /> : ""}</span>
    </Flex>
  );
};

export default CheckboxComponent;
