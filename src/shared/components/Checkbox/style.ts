import styled from "styled-components";
import { Checkbox } from 'antd';

export const CheckboxPrimary = styled(Checkbox)`
    padding: 3px;
`;