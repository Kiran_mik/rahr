
import React from "react";
import { Container } from "./style";

export interface EmptyDivPros {
  name?: string;
  image?: any;
  redirects?: any;
  description?: string;
}
export const CommentBox = (props: EmptyDivPros): JSX.Element => {
  const { name, image, description } = props;
  return (
    <Container>
    <div className="image">
<img src={image} alt="" />
    </div>
    <div className="content">
<h5>{name}</h5>
<p>{description}</p>
    </div>
    </Container>
  );
};
export default CommentBox;