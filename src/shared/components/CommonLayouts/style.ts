
import styled from "styled-components";
import BreadcrumbItem from "antd/lib/breadcrumb/BreadcrumbItem";
export const FormWrapper = styled.div`
    background: white;
    width: 100%;
    padding: 32px;
    border-radius: 10px;
    margin-bottom: 42px;
	.ant-select-selector{
		font-size: 14px;
    font-family: Poppins;
    padding: 11px 12px;
    border: 1px solid #BDBDD3;
    background: transparent;
    border-radius: 4px;
    color: #17082D;
    font-weight: 500;
    line-height: 22px;
	height:auto;
	}
 
	.submitbtn{
		    padding: 12px 24px;
    background: #4E1C95;
    color: #fff;
    font-family: Poppins;
    font-size: 14px;
    line-height: 22px;
    border-radius: 8px;
    width: max-content;
	cursor:pointer;
  font-weight:500;
	}
	.submitbtn:focus, .cancelbtn:focus{
		border-color: #40a9ff;
    border-right-width: 1px !important;
    outline: 0;
    box-shadow: 0 0 0 1px rgb(24 144 255 / 80%);

	}
	.cancelbtn{
		    background: transparent;
    font-size: 14px;
    line-height: 22px;
    color: #4E1C95;
    font-family: Poppins;
    font-weight: 400;
    margin-left: 22px;
	cursor pointer;
}
.MainTitle{
	font-weight:600px;
}
.footerbtn{
	padding:0px;
	    margin-top: 18px;
    border-top: 1px solid #E2E2EE;
    padding-top: 24px;

}
textarea.ant-input{
	height:111px;
	border-radius:8px;
	border-color:#BDBDD3 !important;
}
.ant-input-textarea-show-count::after{
	float:left;
	color:rgba(123, 123, 151, 1);
	font-size:14px;
	line-height:22px;
	font-family:Poppins;
	font-weight:400;
}
.squareRadioBtn .ant-radio-wrapper{
  border: 1px solid #AFADC8;
    padding: 8px 15px;
    margin-bottom: 18px;
    margin-right:15px;
    border-radius:4px;
}
.hidecounter.ant-input-textarea-show-count::after{
  display:none
}
	}
`

export const BreadItem = styled(BreadcrumbItem) <{ firstNav: boolean }>`
  color:${props => props.firstNav ? "#17082D" : "#9792E3"};
`

export const UserBreadWrap = styled.div`
.ant-breadcrumb{
  font-size:12px;
  line-height:12px;
  margin-top:4px;
}
`;