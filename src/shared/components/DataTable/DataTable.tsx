import React from "react";
import PropTypes from "prop-types";
import Table from "antd/lib/table";
import { TableContainer, EmptyContainer } from "./style";

const scrollConfig = {
  y: 650,
};

interface DataTableType {
  debug?: boolean;
  infinity?: boolean;
  dataSource: Array<any>;
  scroll?: { y: number; x: number };
  onFetch?: () => Promise<any>;
  rowSelection?: any;
  loading?: boolean;
  pagination?: boolean;
  columns: any;
  props?: any;
  renderEmpty?: () => React.ReactNode;
  style?:React.CSSProperties
}

const DataTable = (p: DataTableType) => {
  const {
    debug = false,
    infinity = false,
    dataSource = [],
    scroll = scrollConfig,
    pagination = false,
    onFetch = () => null,
    renderEmpty,
    style,
    ...props
     
  } = p;
  return (
    <TableContainer
      showCustomEmpty={!props.loading && !!renderEmpty}
      //@ts-ignore
      onScroll={(e: {
        target: {
          scrollHeight: number;
          scrollTop: number;
          clientHeight: number;
        };
      }) => {
        if (
          infinity &&
          !props.loading &&
          e.target.scrollHeight &&
          e.target.scrollTop
        ) {
          const bottom =
            Math.round(e.target.scrollHeight - e.target.scrollTop) ===
            e.target.clientHeight;

          if (bottom) {
            onFetch();
          }
        }
      }}
    >
      <Table
        dataSource={dataSource}
        scroll={scroll}
        pagination={false}
        {...props}
        showSorterTooltip={false}
        style={style}
      />

      {renderEmpty && !props.loading && !dataSource.length && (
        <EmptyContainer>{renderEmpty()}</EmptyContainer>
      )}
    </TableContainer>
  );
};

DataTable.propTypes = {
  lastId: PropTypes.any,
  dataSource: PropTypes.any,
  debug: PropTypes.bool,
  infinity: PropTypes.bool,
  loadingIndicator: PropTypes.bool,
  onFetch: PropTypes.any,
  scroll: PropTypes.any,
};

export default DataTable;
