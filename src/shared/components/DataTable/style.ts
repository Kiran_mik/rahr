import styled from "styled-components";

export const TableContainer = styled.div`
  ${(props: { showCustomEmpty: boolean }) => {
    return (
      props.showCustomEmpty &&
      `
        position: relative;

.ant-empty{
    height:200px;
}
.ant-empty-image{
    display:none;
}
.ant-empty-description{
    display:none;
}
`
    );
  }}
`;
export const EmptyContainer = styled.div`
  top: 0;
  position: absolute;
  display: flex;
  width: 100%;
  justify-content: center;
`;
