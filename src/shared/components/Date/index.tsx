import React from "react";
import moment from "moment";

interface DateProps {
  date: string;
  isDisabled?: boolean;
  format?:string
}
function Date(props: DateProps) {
  const { date, isDisabled,format } = props;
  return (
    <div
      style={{
        fontSize: 14,
        fontWeight: 400,
        color: isDisabled ? "black" : "#A2A2BA",
      }}
    >
      {moment(date)
        .format(format || "MMM. DD, YYYY LT")
        .concat(" EST")}
    </div>
  );
}

export default Date;
