import { getDateAsFormat, ObjectStr } from "@utils/helpers";
import { DatePicker } from "antd";
import { ErrorMessage } from "formik";
import moment from "moment";
import React, { FC } from "react";
import { Content } from "./style";

const MonthPicker = DatePicker.MonthPicker;
interface FormValues {
  name: string;
  id: string;
  error?: any;
  onChange?: (value: any) => void;
  onBlur?: (event: React.ChangeEvent<any>, value: any) => void;
  value?: any;
  defaultValue?: any;
  className?: any;
  placeholder?: any;
  picker?: any;
  format?: string;
}

const PICKER_VIEW_MAP: ObjectStr = {
  year: "YYYY",
};
const DatePickerComponent: FC<FormValues> = ({
  error,
  id,
  name,
  onChange,
  value,
  defaultValue,
  className,
  placeholder,
  picker,
  format,
}) => {
  const dateFormat = PICKER_VIEW_MAP[picker] || "DD/MM/YYYY";
  const formatedValue = value && value!=='Invalid date'
    ? moment(moment(value, "YYYY-MM-DD").format(dateFormat), dateFormat)
    : null;

  const options = formatedValue
    ? {
        value: formatedValue,
      }
    : {};
  return (
    <Content className="bottom-0">
      <DatePicker
        onChange={(e: any) => {
          if (onChange) {
            onChange(e ? moment(e._d).format("YYYY-MM-DD") : "");
          }
        }}
        placeholder={placeholder ? placeholder : dateFormat}
        disabledDate={(d) =>
          !d ||
          d.isAfter(moment().format(dateFormat)) ||
          d.isSameOrBefore("1960-01-01")
        }
        {...options}
        format={format || dateFormat}
        picker={picker}
      />

      <span className="errormsg" style={{ color: "red" }}>
        {error ? <ErrorMessage name={name} /> : ""}
      </span>
    </Content>
  );
};

export default DatePickerComponent;
