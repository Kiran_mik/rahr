import styled from "styled-components";
import {
    DatepickerIcon
} from "@assets/index";

export const Content = styled.div`
margin-bottom:18px;
&.bottom-0{
    margin-bottom:0px;
}
.ant-picker{
font-size: 14px;
    font-family: Poppins;
    padding: 11px 12px;
    border: 1px solid #BDBDD3;
    background: transparent;
    border-radius: 8px;
    color: #17082D;
    font-weight: 500;
    line-height: 22px;
    width: 100%;
    font-family: Poppins;
    
}
.ant-picker-suffix{
        color: #17082D;
}
.anticon.anticon-calendar svg{
    display:none;
}
.anticon.anticon-calendar{
        background:url(${DatepickerIcon}) no-repeat;
        display: block;
    width: 18px;
    height: 18px;
    }
input{
    &::placeholder{
        color:#7B7B97;
  font-weight:400;
    }
}
`