import Error from "@components/Error";
import { Spin } from "antd";
import React from "react";
import { Container } from "./style";

export interface PropContent {
  text?: React.ReactNode;
  image?: any;
  link?: string;
  linktext?: string;
  docURL?: string | null;
  isLoading?: boolean;
  error?: string;
}

export const DocuSign = (props: PropContent): JSX.Element => {
  const { text, error, docURL, link, image, isLoading, linktext } = props;
  return (
    <>
      {isLoading && <Spin />}
      {!isLoading && (
        <Container>
          <img src={image} alt="" />
          <p>{text}</p>
          <a href={docURL || link} target={"_blank"}>
            {docURL ? "View Form" : linktext}
          </a>
        </Container>
      )}
      {!!error && <Error error={error}/>}
    </>
  );
};
export default DocuSign;
