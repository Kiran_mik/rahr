import React, { useEffect, useState } from "react";
import { Upload,  Button } from 'antd';
import { Link } from "@reach/router";
import { DocumentUploadWrapper } from "./style"
import DocumentImg from "@assets/icon-form.svg";
import { ErrorMessage } from "formik";

interface DocumentUploadProps {
	type?: string;
	id?: string;
	placeholder?: string;
	className?: string;
	value?: Array<any>;
	name: string;
	onChange?: (value: any) => void;
	onBlur?: (event: React.ChangeEvent<any>, value: any) => void;
	components?: any;
	error?: any;
	addonBefore?: any;
	dataTestid?: any;
	max?: any;
	docURL?: string,
	label?: string
	multiple?: boolean
}

function DocumentUpload(props: DocumentUploadProps) {
	const {
		type,
		id,
		name,
		value,
		className,
		placeholder,
		error,
		dataTestid,
		onChange,
		docURL,
		label,
		multiple
	} = props;
	console.log(docURL);
	const [fileList, setFileList] = useState<Array<any>>([]);
	const DocumentUploadProps = {
		onRemove: (file: any) => {
			const index = [...fileList].indexOf(file);
			const newFileList = fileList.slice();
			newFileList.splice(index, 1);
			setFileList(newFileList)
		},
		beforeUpload: (file: any) => {
			if (multiple) {
				setFileList([...fileList, file])
			} else {
				setFileList([file])
			}
			return false;
		},
		fileList,
		multiple: multiple,
	};
	useEffect(() => {
		onChange && onChange(multiple
			? fileList
			: (fileList.length ?
				fileList[0]
				: null));
	}, [fileList]);
	useEffect(() => {
		setFileList(value || []);
	}, [value]);

	return (
		<>
		<DocumentUploadWrapper>
			<Upload {...DocumentUploadProps}>
				<Button className="buttonWrapper">
					<img src={DocumentImg} alt="" style={{ marginRight: "20px" }} /> Drag and drop or browse <Link to="" style={{ marginLeft: "24px" }}>Upload</Link></Button>
			</Upload>
			<span className="errormsg">
				{error ? <ErrorMessage name={name} /> : " "}
			</span>
			{!!docURL && !error && (typeof docURL === 'string') && (
				<a href={docURL} target={"_blank"}>
					{label}
				</a>
			)}
		</DocumentUploadWrapper>
		
			
		</>
	);
}

export default DocumentUpload;
