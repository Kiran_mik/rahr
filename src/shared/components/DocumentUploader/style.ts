import styled from "styled-components";
import { Input } from "antd";
import Atachment from "@assets/attachment.svg";


export const DocumentUploadWrapper = styled.div`
margin-bottom:24px;
.errormsg{
  color:red;
    display: block;
    margin-top:1px;
}
a{
  
        background: rgba(245, 138, 7, 1);
    padding: 4px 12px;
    font-size: 12px;
    line-height: 12px;
    color: #fff;
    border-radius: 5px;
    
}

.buttonWrapper{
  padding: 16px 20px;
    height: auto;
        text-align: left;
    border-radius:4px;
    border: 1px dashed #A2A2BA;
    width:100%;
}
.ant-upload.ant-upload-select{
  width:100%
}
`

