import React, { useState } from "react";
import { Button, Space, Radio } from "antd";
import { DrawerProps } from "antd/es/drawer";
import { CustomDrawer } from "./style";
export interface DrawerViewProps {
  onClose: React.ReactEventHandler;
  onOpen?: React.ReactEventHandler;
  className?: string;
  children?: React.ReactNode;
  direction?: "bottom" | "left" | "right" | "top";
  HeaderComponent?: () => void;
  FooterComopnent?: () => void;
  variant?: "permanent" | "persistent" | "temporary";
  visible?: boolean;
  title?: string;
}

export const DrawerView: React.FC<DrawerViewProps> = props => {
  const { onClose, onOpen, children, visible, title } = props;

  return (
    <>
      <CustomDrawer
        title={title}
        placement="right"
        width={575}
        onClose={onClose}
        visible={visible}
        className="main"
      >
        {children}
      </CustomDrawer>
    </>
  );
};

export default DrawerView;
