import styled from "styled-components";
import { Drawer } from "antd";



export const CustomDrawer = styled(Drawer)`
 font-family: Poppins;
.ant-drawer-title {
    color: rgba(0, 0, 0, 0.85);
    font-weight: 600;
        font-family: Poppins;
        line-height:32px;
    font-size: 24px;
}
.ant-drawer-header{
        padding: 32px 0px 24px 0px;
        margin:0px 29px 0px 32px;
       1px solid #E9E9E9;
}
.ant-drawer-close {
    top: 32px;
    right: 0px;
    z-index: 10;
    color: #17082D;
    font-weight: 700;
    font-size: 22px;
    padding:0px;
}
.ant-drawer-body {
    padding: 0px 32px;
}
`