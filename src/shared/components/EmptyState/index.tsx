import { Link } from "@reach/router";
import React from "react";
import { Container } from "./style";
export interface EmptyDivPros {
  text?: React.ReactNode;
  image?: any;
  redirects?: any;
  linktext?: string;
}
export const EmptyDiv = (props: EmptyDivPros): JSX.Element => {
  const { text, image, linktext } = props;
  return (
    <Container>
      <img src={image} alt="" width="280px" />
      <p>
        {text}
      </p>
    </Container>
  );
};
export default EmptyDiv;
