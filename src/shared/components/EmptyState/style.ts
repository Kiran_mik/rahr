import styled from "styled-components";


export const Container = styled.div`
    text-align: center;
    display: flex;
    flex-direction: column;
    margin:24px 0px;
    img{
            display: block;
    margin: auto;
        margin-bottom:12px;
    }
    p{
        font-size:14px;
        line-height:22px;
        color:#b0b0b0;
        font-weight:400
    }
    a{
        color:rgba(78, 28, 149, 1)
    }
`
