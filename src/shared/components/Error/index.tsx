import React from "react";

function Error(props: { error: string }) {
  const { error } = props;
  return <span style={{ color: "red", fontFamily: "Poppins" }}>{error}</span>;
}

export default Error;
