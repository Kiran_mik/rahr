import styled from "styled-components";
import { Input } from "antd";
import { Field } from "formik";
import Atachment from "@assets/attachment.svg";


export const InputFormdiv = styled.div`
 margin-bottom:18px;
}
.errormsg{
  color:red;
    display: block;
    margin-top:1px;
}
`
export const InputFormWrapper = styled.div`
border: 1px dashed #E9ECF3;
    padding: 8px 10px;
    border-radius: 8px;
   display:block;
   align-items:center;
    position:relative;
    height:46px;
input{
   opacity:0;
  width:100%;
  z-index: 4;
    position: relative;
      
}
input[type = file]::-webkit-file-upload-button{
   visibility: hidden;
  //  display:none;
}
&::after{
  content: url(${Atachment});
  position:absolute;
  right:10px;
  top:7px;
}
.file-placeholder{
      position: absolute;
    left: 12px;
    color:#7B7B97;
    margin-top:2px;
}

`
export const InputForm = styled(Field)`
   
}
`

