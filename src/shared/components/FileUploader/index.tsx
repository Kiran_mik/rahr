import Form from "antd/lib/form/Form";
import React, { FC, useState, useEffect } from "react";
import { Upload, message } from "antd";
import { DownloadOutlined, InboxOutlined, UploadOutlined } from "@ant-design/icons";
import { PhotoCOntainer, FileContainer, UploadButton, DownloadButton } from "./style";
import Atachment from "@assets/attachment.svg";
import DocumentImg from "@assets/icon-form.svg";
import { ErrorMessage } from "formik";
import { ImageUpload } from "@services/UserManagement/NewStaffForm";
import axios from "axios";
interface FormValues {
  name: string;
  id: string;
  error: any;
  onChange?: (value: any) => void;
  onBlur?: (event: React.ChangeEvent<any>, value: any) => void;
  value: any;
  onSuccess?: any;
  onSubmit?: any;
}
const FileUpload: FC<FormValues> = ({
  error,
  id,
  name,
  onChange,
  value,
  onSuccess,
  onSubmit,
}) => {
  const { Dragger } = Upload;
  const [errormsg, setmsg] = useState<any>(false);
  const [userimg, setImg] = useState<any>("");
  const [imgresp, setImgresp] = useState<any>();

  const props = {
    name: "file",
    multiple: false,
    action:
      "https://7ngsa15qzk.execute-api.ca-central-1.amazonaws.com/api/v1/image/upload",
    onChange(info: any) {
      const { status } = info.file;
      if (info.fileList.length < 1) {
        setmsg(true);
      }
      if (status !== "uploading") {
      }
      if (status === "done") {
        message.success(`${info.file.name} file uploaded successfully.`);
        setImg(info);
        // var imgresp = info.file;
        setImgresp(info.file);
      } else if (status === "error") {
        message.error(
          `${info.file.name
          } file upload failed.Please upload images less than 15KB`
        );
      }
    },
    value: userimg,
    maxCount: 1,
    onDrop(e: any) { },
  };
  const onSubmitPhoto = async () => {
    var res = await ImageUpload(userimg);
    return res;
  };
  useEffect(() => {
    onChange && onChange(userimg);
    // onSubmitPhoto();
  }, [userimg]);
  const handleSubmit = (event: any) => {
    event.preventDefault();

    let formData = new FormData();
    // add one or more of your files in FormData
    // again, the original file is located at the `originFileObj` key
    formData.append("Imgfile", userimg.fileList[0].originFileObj);

    axios
      .post(
        "https://7ngsa15qzk.execute-api.ca-central-1.amazonaws.com/api/v1/image/upload",
        formData
      )
      .then((res: any) => { })
      .catch((err: any) => { });
  };
  return (
    <>
      <PhotoCOntainer
        onChange={(e: any) => {
          return onChange && onChange(userimg);
        }}
        onSubmit={handleSubmit}
      >
        <Dragger {...props}>
          <input placeholder="Please upload Photo" />
          <span>
            <img src={Atachment} alt="attachment" />
          </span>
        </Dragger>
        <br />
      </PhotoCOntainer>
      <span style={{ color: "red", paddingBottom: "10px" }}>
        {error || errormsg ? <ErrorMessage name={name} /> : ""}
      </span>
    </>
  );
};

export default FileUpload;


interface FileUploadBoxProps extends FormValues {
  templateUrl?: string;
}

export const FileUploadBox: FC<FileUploadBoxProps> = ({
  error,
  id,
  name,
  onChange,
  value,
  onSuccess,
  onSubmit,
  templateUrl
}) => {
  console.log("value fileupload", name, '===', value);

  const { Dragger } = Upload;
  const [errormsg, setmsg] = useState<any>(false);
  const [userimg, setImg] = useState<any>("");
  const [imgresp, setImgresp] = useState<any>();
  const [loading, setLoading] = useState<boolean>(false);

  const props = {
    name: "file",
    multiple: false,
    showUploadList: false,
    action:
      "https://7ngsa15qzk.execute-api.ca-central-1.amazonaws.com/api/v1/file/upload",
    onChange(info: any) {
      setLoading(true);
      const { status } = info.file;
      if (info.fileList.length < 1) {
        setmsg(true);
      }
      if (status !== "uploading") {
      }
      if (status === "done") {
        console.log("file == ", info);
        message.success(`${info.file.name} file uploaded successfully.`);
        setImg(info.file.name);
        setLoading(false);
      } else if (status === "error") {
        message.error(
          `${info.file.name
          } file upload failed.Please upload images less than 15KB`
        );
        setLoading(false);
      }
    },
    value: userimg,
    maxCount: 1,
    onDrop(e: any) { },
  };
  const onSubmitPhoto = async () => {
    var res = await ImageUpload(userimg);
    return res;
  };
  useEffect(() => {
    if (userimg !== "") {
      onChange && onChange(userimg);
    }
    // onSubmitPhoto();
  }, [userimg]);
  const handleSubmit = (event: any) => {
    event.preventDefault();

    let formData = new FormData();
    // add one or more of your files in FormData
    // again, the original file is located at the `originFileObj` key
    formData.append("Imgfile", userimg.fileList[0].originFileObj);

    axios
      .post(
        "https://7ngsa15qzk.execute-api.ca-central-1.amazonaws.com/api/v1/file/upload",
        formData
      )
      .then((res: any) => { })
      .catch((err: any) => { });
  };
  return (
    <>
      <FileContainer
        onChange={(e: any) => {
          return onChange && onChange(userimg);
        }}
        onSubmit={handleSubmit}
      >
        <Dragger {...props}>
          <img src={DocumentImg} alt="file" />
          <span className="label">{loading ? "Uploading..." : (value ? value : "Drag and drop or browse")}</span>
          <UploadButton icon={<UploadOutlined />} >Upload</UploadButton>
        </Dragger>
        {templateUrl ? <DownloadButton icon={<DownloadOutlined />} >Download template</DownloadButton> : null}

      </FileContainer>
      <span style={{ color: "red", paddingBottom: "10px" }}>
        {error || errormsg ? <ErrorMessage name={name} /> : ""}
      </span>
    </>
  );
};
