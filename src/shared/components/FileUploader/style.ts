import styled from "styled-components";
import { Button } from 'antd';
import { FolderViewOutlined } from "@ant-design/icons";

export const PhotoCOntainer = styled.div`
position: relative;
    display: inline-flex;
    align-items: center;
    width: 100%;
        font-family: Poppins;
.ant-upload.ant-upload-drag {
    position: relative;
    width: 100%;
    height: 100%;
    text-align: center;
    padding: 7px 10px;
    background: transparent;
    border: 1px dashed rgba(233, 236, 243, 1);
    border-radius: 4px;
    cursor: pointer;
    transition: border-color 0.3s;
    display: block;
}
.ant-upload-btn{
    padding:0px !important;
}
.ant-upload-list-text .ant-upload-span > *{
   flex: auto;
}
.ant-upload-list-item-card-actions{
    text-align:right;
}
span{
    width:100%
}
 .ant-upload-drag-container {
    text-align: left;
        display: flex !important;
    img{
    float:right;
}
}
input{
    
     &::placeholder{
  color:#7B7B97;
  font-weight:400;
}

`

export const FileContainer = styled.div`
    position: relative;
    display: inline-flex;
    align-items: center;
    width: auto;
    font-family: Poppins;
    border: 1px dashed #BDBDD3;
    transition: border-color 0.3s;
    display: -webkit-inline-box;
    display: -webkit-inline-flex;
    display: -ms-inline-flexbox;
    display: inline-flex;
    width: -webkit-max-content;
    width: -moz-max-content;
    width: max-content;
    padding: 5px 15px;
    border-radius: 8px;
    -webkit-align-items: center;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;

    .ant-upload.ant-upload-drag {
        position: relative;
        width: 100%;
        height: 100%;
        text-align: center;
        padding: 7px 5px;
        background: transparent;
        cursor: pointer;
        display: block;
        border: none;
        &:hover{
            border: none;
        }
    }
    .ant-upload-btn{
        padding:0px !important;
    }
    .ant-upload-list-text .ant-upload-span > *{
        flex: auto;
    }
    .ant-upload-list-item-card-actions{
        text-align:right;
    }
    span.label{
        min-width: 10px;
        padding: 5px 15px;
    }
    .ant-upload-drag-container {
        text-align: left;
        display: flex !important;
        img{
            float:right;
        }
    }
    input{
        &::placeholder{
            color:#7B7B97;
            font-weight:400;
    }
`;

export const UploadButton = styled(Button)`
    background: #4E1C95;
    width: auto;
    padding: 5px 10px;
    color: #FFF;
    border-radius: 8px;
    margin: 0px 0px;
    &:hover{
        background: #4E1C95;
        color: #FFF;
        border: 1px inset #4E1C95;
    }
    & span{
        padding: 0px;
    }
    & .ant-btn:hover{
        background: #4E1C95;
        color: #FFF;
    }
    & .ant-btn:active{
        background: #4E1C95;
        color: #FFF;
    }
    & .ant-btn::focus{
        background: #4E1C95;
        color: #FFF;
    }
`;


export const DownloadButton = styled(Button)`
    background: #FFFFFF;
    width: auto;
    padding: 5px 10px;
    color: #17082D;
    border-radius: 8px;
    margin: 0px 0px;
    border: 1px solid #BDBDD3;
    &:hover{
        background: #FFFFFF;
        color: #17082D;
        border: 1px solid #BDBDD3;
    }
    & span{
        padding: 0px;
    }
    & .ant-btn:hover{
        background: #FFFFFF;
        color: #17082D;
    }
`
