import { Primary, Transparent } from "@components/Buttons";
import { Flex } from "@components/Flex";
import React, { useState } from "react";
import { FilterOuter, PrimaryButton, TransparentButton } from "./style";

interface SearchFieldProps {
    type?: string;
    id?: string;
    placeholder?: string;
    className?: string;
    onChange?: any;
    style?: any;
    setFilterval?: any
}

const FilterComponent = (props: SearchFieldProps): JSX.Element => {
    const { setFilterval } = props;
    const [type, setType] = useState('all')
    const handleChange = (val: string) => {
        setType(val)
        setFilterval(val)
    }

    return (
        <FilterOuter>
            <Flex className="footerbtn">
                {/* <PrimaryButton
                text="All"
                // className="submitbtn"
                style={{ marginRight: "20px" }}
            /> */}
                {type === 'all' ?
                    <PrimaryButton text="All" style={{ marginRight: "20px" }} /> :
                    <TransparentButton text="All" style={{ marginRight: "20px" }} onClick={() => handleChange('all')} />
                }
                {type === 'active' ?
                    <PrimaryButton text="Active" style={{ marginRight: "20px" }} /> :
                    <TransparentButton text="Active" style={{ marginRight: "20px" }} onClick={() => handleChange('active')} />
                }
                {type === 'inactive' ?
                    <PrimaryButton text="Inactive" style={{ marginRight: "20px" }} /> :
                    <TransparentButton text="Inactive" style={{ marginRight: "20px" }} onClick={() => handleChange('inactive')} />
                }
                {type === 'parked' ?
                    <PrimaryButton text="Parked" style={{ marginRight: "20px" }} /> :
                    <TransparentButton text="Parked" style={{ marginRight: "20px" }} onClick={() => handleChange('parked')} />
                }
                {type === 'pending' ?
                    <PrimaryButton text="Pending" style={{ marginRight: "20px" }} /> :
                    <TransparentButton text="Pending" style={{ marginRight: "20px" }} onClick={() => handleChange('pending')} />
                }
            </Flex>
        </FilterOuter>
    );
};

export default FilterComponent;
