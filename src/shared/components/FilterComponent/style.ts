import styled from "styled-components";
import { Input } from "antd";
import { SearchIcon } from "@assets/index";
import { Primary, Transparent } from "@components/Buttons";

export const FormInput = styled(Input)`
  font-size: 12px;
  font-family: Poppins;
  line-height: 12px;
  background: transparent;
  color: #17082d;
  font-weight: 500;
  border: none;
  padding-left: 0px;
  &:focus {
    border: none;
    outline: none;
    box-shadow: none;
  }
 

  &::placeholder {
    color: rgba(183, 183, 188, 1);
    font-weight: 400;
    font-size: 12px;
  }
`;

export const PrimaryButton = styled(Primary)`
  font-size: 14px;
  min-width: 48px;
  border-radius: 10px;
  line-height:22px;
  padding: 3px 15px;
`;

export const TransparentButton = styled(Transparent)`
font-size: 14px;
border-radius: 10px;
`;

export const FilterOuter = styled.div`
  padding: 8px 25px;
  background:#fff;
  box-shadow: 0px 3px 10px rgba(0, 0, 0, 0.03);
  border-radius: 10px;
`;

