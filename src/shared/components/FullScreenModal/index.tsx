import React from "react";
import { FullScreenContainer, FullScreenContent } from "./style";

function FullScreenModal(props: any) {
  return (
    <FullScreenContainer>
      <FullScreenContent style={props.style}>
        {props.children}
      </FullScreenContent>
    </FullScreenContainer>
  );
}

export default FullScreenModal;
