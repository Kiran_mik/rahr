import React, { useState } from "react";
import { GoogleAutoComplete } from "@components/GoogleAutoComplete";
import { FormLableTypography } from "@components/Typography/Typography";
import { InputField } from "@components/Input/Input";
import { getAddressComps, getLocationByPlaceId } from "@utils/googlemaphealper";
import MapContainer from "@components/MapContainer/MapContainer";


interface LocationType {
    formattedAddress: string;
    addressLine_1: string;
    addressLine_2: string;
    city: string;
    state: string;
    country: string;
    postalCode: string;
}
interface LatLongType {
    [index: number]: {
        lat: number;
        lng: number
    }
}


// const log = logger('ContactDetails');

const ContactDetails: React.FC = () => {

    const [loc, setLoc] = useState<LocationType>({
        formattedAddress: '',
        addressLine_1: '',
        addressLine_2: '',
        city: '',
        state: '',
        country: '',
        postalCode: '',
    });
    const [coord, setCoord] = useState<LatLongType>([
        {
            lat: 43.5931073,
            lng: -79.6422539
        }
    ])

    const onPlaceSelect = async (placeId: string) => {
        const place:any = await getLocationByPlaceId(placeId);
        if (place) {
            const locObj = getAddressComps(place);
            if (locObj.city.length > 0) {
                const latlong = ({ lat: place.geometry.location.lat(), lng: place.geometry.location.lng() });
                console.log("latlong", latlong);
                console.log("address", locObj);
                setLoc(locObj);
                setCoord([latlong])
            }
        }
    };

    const multiCoord = [
        {
            lat: 49.2771219,
            lng: -123.0780013
        },
        {
            lat: 43.6500109,
            lng: -79.38733640000001
        },
        {
            lat: 49.1730057,
            lng: -123.1379091
        },
        {
            lat: 43.5899794,
            lng: -79.72337689999999
        }
    ]

    return (
        <div>
            <div>
                <FormLableTypography>
                    Address Line 1<span>*</span>
                </FormLableTypography>
                <GoogleAutoComplete
                    apiKey={process.env.REACT_APP_GOOGLEAPI_KEY}
                    defaultVal={''}
                    id="address1"
                    name="address1"
                    placeholder='Input your location'
                    onPlaceSelect={(place: any) => {
                        console.log("api resp", place);
                        if (place) onPlaceSelect(place.place_id);
                        else {
                            const emptPlace = {
                                formattedAddress: '',
                                addressLine_1: '',
                                addressLine_2: '',
                                city: '',
                                state: '',
                                country: '',
                                postalCode: '',
                            };
                            setLoc(emptPlace);
                        }
                    }}
                />
                {/* TODO: need to separate the following fields to other component */}
                <FormLableTypography>
                    Address Line 2<span>*</span>
                </FormLableTypography>
                <InputField
                    placeholder='Suite,unit number,etc'
                    name='AddressLine2'
                    value={loc.addressLine_2}
                    className={'autoComplete'}
                />
                <FormLableTypography>
                    Province<span>*</span>
                </FormLableTypography>
                <InputField
                    placeholder='Province'
                    name='AddressLine2'
                    value={loc.state}
                    className={'autoComplete'}
                />
                <FormLableTypography>
                    City<span>*</span>
                </FormLableTypography>
                <InputField
                    placeholder='City'
                    name='AddressLine2'
                    value={loc.city}
                    className={'autoComplete'}
                />
                <FormLableTypography>
                    Postral code<span>*</span>
                </FormLableTypography>
                <InputField
                    placeholder='--- ---'
                    name='Postralcode'
                    value={loc.postalCode}
                    className={'autoComplete'}
                />
            </div>
            Latitude :{coord[0].lat}
            <br />
            Longitude :{coord[0].lng}
            <div>
                <MapContainer width={'900'} coord={multiCoord} zoom={2} />
            </div>

        </div>
    );
};

export default (ContactDetails);

