import React, { useState, useEffect } from 'react';
import usePlacesService from 'react-google-autocomplete/lib/usePlacesAutocompleteService';
import { InputField } from "@components/Input/Input";
import Vector from "@assets/icons/vector.svg";
import {Ul, Li, Dropdown, DropdownInput, Img} from './style'
export interface GoogleAutoCompleteProps {
    apiKey?: string;
    placeholder?: string;
    defaultVal?: string;
    id?: string;
    name?: string;
    value?:string
    onPlaceSelect: (place: any) => void;
    error?: any;
}

export const GoogleAutoComplete: React.FC<GoogleAutoCompleteProps> = (props) => {
    const { apiKey, placeholder, defaultVal, onPlaceSelect, id, name, error } = props;

    const [val, setVal] = useState<string>('');
    const [showSuggestion, setsuggestion] = useState(false);

    useEffect(() => {
        if (defaultVal) setVal(defaultVal);
    }, [defaultVal]);

    const { placePredictions, getPlacePredictions } = usePlacesService({
        apiKey: apiKey,
    });

    return (
        <Dropdown>
            <DropdownInput>
                <InputField
                    placeholder={placeholder}
                    name={name || ''}
                    value={val}
                    id={id}
                    onChange={(evt) => {
                        if (evt.target.value.length === 0 || evt.target.value === undefined) {
                            setVal(evt.target.value);
                            setsuggestion(false);
                            onPlaceSelect({});
                        } else {
                            getPlacePredictions({
                                input: evt.target.value,
                                componentRestrictions: { country: 'ca' },
                            });
                            setVal(evt.target.value);
                            setsuggestion(true);
                        }
                    }}
                    error={error}
                />
            </DropdownInput>
            {showSuggestion && (
                <Ul>
                    {placePredictions.map((place) => {
                        return (
                                
                                <Li
                                    id={place}
                                    onClick={(e: any) => {
                                        setVal(place.description)
                                        onPlaceSelect(place);
                                        setsuggestion(false);
                                    }}
                                >
                                    <Img src={Vector} alt="" />
                                    {place.description}
                                </Li>

                        );
                    })}
                </Ul>
            )}
        </Dropdown>
    );
};


// {
//     placePredictions.map((place: any) => {
//         return (
//             <>
//                 <Option
//                     id={place}
//                     onClick={(e: any) => {
//                         setVal(place.description)
//                         onPlaceSelect(place);
//                         setsuggestion(false);
//                     }}
//                     value={place.description}
//                 >{place.description}
//                 </Option>
//             </>
//         );
//     })
// }