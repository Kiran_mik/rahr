import React, { FC } from "react";
import { Tabs } from "antd";
import { TabsProps } from "@components/VerticalTabs";
import { CustomTab } from "./style";
const { TabPane } = Tabs;

const HorizontalTab: FC<TabsProps> = ({ children, tab, className, value }) => {
  return (
    <CustomTab type="card">
      {value.map((user: any) => {
        return (
          <TabPane tab={user.tab} key={user.key}>
            {user.content}
          </TabPane>
        );
      })}
    </CustomTab>
  );
};

export default HorizontalTab;
