import styled from "styled-components";
import { Tabs } from "antd";

export const TabContentWrapper = styled.div`
 
`
export const CustomTab = styled(Tabs)`
 .ant-tabs-content{
     background:#fff;
     padding:32px;
     border-bottom-left-radius:15px;
       border-bottom-right-radius:15px;
       margin-bottom:20px;
        margin: 0px 1px;
 }
 .ant-tabs-nav{
     margin: 0px 1px;
 }
 .ant-tabs-nav .ant-tabs-tab + .ant-tabs-tab{
     margin-left:8px !important;
     
        
 }
  .ant-tabs-tab{
      border:none !important;
      border:1px solid #fafafa !important;
      backgrund:rgba(255, 255, 255, 0.5) !imprtant;
      width:50%;
  }

 .ant-tabs-tab.ant-tabs-tab-active .ant-tabs-tab-btn {
    color: #50514fa3;
    font-weight: 500;
 }
 .ant-tabs-nav-list{
         width: 100%;
    display: flex;
    flex-direction: row;
 }
 .ant-tabs-tab{
     padding: 12px 24px !important;
    line-height: 22px;
    font-size: 14px;
    color:#17082D;
    font-weight:500;
       display: flex;
       
           border-top-left-radius: 10px !important;
             border-top-right-radius: 10px !important;
             
 }
   .ant-tabs-tab-active{
      border:1px solid #fff !important;
  }
`