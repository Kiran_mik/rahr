import React, { useState } from "react";
import { Autofill } from "./style";
interface InputFieldProps {
  type?: string;
  id?: string;
  placeholder?: string;
  className?: string;
  onChange?: any;
  optionValues?: any;
}

export const AutofillInput = (props: InputFieldProps): JSX.Element => {
  const { type, id, placeholder, className, onChange, optionValues } = props;
  const options = optionValues;
  return (
    <>
      <Autofill
        style={{ width: "100%" }}
        options={options}
        placeholder={placeholder}
        filterOption={(inputValue, option) =>
          option!.value.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1
        }
      />
    </>
  );
};
