import React from "react";
import ReactDOM from "react-dom";
import { FormInputFiled } from "./Input";
import { render, screen } from "@testing-library/react";

describe("Input componant test cases", () => {
  window.matchMedia =
    window.matchMedia ||
    function() {
      return {
        matches: false,
        addListener: function() {},
        removeListener: function() {}
      };
    };
  test("Input should render properly", async () => {
    const container = render(<FormInputFiled name="Input" value="Hello" />);

    const UserDetails = screen.queryByText(/Hello/i);
    const InputName = screen.queryByText(/Input/i);
    expect(UserDetails).toHaveTextContent("Hello");
    expect(InputName).toHaveTextContent("Input");

    expect(container).toBeInTheDocument();
  });
});
