import { ErrorMessage, Field } from "formik";
import React from "react";
import { FormInput, InputForm, InputFormWrapper } from "./style";
import { Input as AntInput, Form } from "antd";

interface InputFieldProps {
  type?: string;
  id?: string;
  placeholder?: string;
  className?: string;
  value?: string;
  name: string;
  onChange?: (event: React.ChangeEvent<any>, value: any) => void;
  onBlur?: (event: React.ChangeEvent<any>, value: any) => void;
  components?: any;
  error?: any;
  addonBefore?: any;
  dataTestid?: any;
  max?: any;
  defaultValue?: any;
  prefix?: string;
  refProps?: any;
  disabled?: boolean;
  style?:any
}

export const InputField = (props: InputFieldProps): JSX.Element => {
  const {
    type,
    id,
    name,
    value,
    className,
    style,
    placeholder,
    addonBefore,
    dataTestid,
    defaultValue,
    onChange,
    error,
    prefix,
    refProps,
    disabled,
  } = props;
  const FieldComonent = type !== "password" ? FormInput : AntInput.Password;
  return (
    <InputFormWrapper className={className}>
      <FieldComonent
        ref={refProps ? refProps : null}
        placeholder={placeholder}
        type={type}
        id={id}
        name={name}
        prefix={prefix}
        style={style}
        className={className}
        value={value}
        defaultValue={defaultValue}
        disabled={disabled}
        onChange={(e) => {
          if (onChange) {
            onChange(e, e.target.value);
          }
        }}
      />

      <span>
        {error ? (
          <span style={{ color: "red", fontFamily: "Poppins" }}>{error}</span>
        ) : (
          ""
        )}
      </span>
    </InputFormWrapper>
  );
};

export const FormInputFiled = (props: InputFieldProps): JSX.Element => {
  const {
    type,
    id,
    name,
    value,
    className,
    placeholder,
    components,
    error,
    dataTestid,
    onChange,
    defaultValue,
    max,
  } = props;
  return (
    <InputFormWrapper>
      <InputForm
        placeholder={placeholder}
        type={type}
        id={id}
        name={name}
        className={className}
        data-testid={dataTestid}
        //@ts-ignore
        onChange={onChange}
        value={value}
        defaultValue={defaultValue}
      />
      <span className="errormsg">
        {error ? <ErrorMessage name={name} /> : ""}
      </span>
    </InputFormWrapper>
  );
};

export const Input = (props: InputFieldProps): JSX.Element => {
  const {
    type,
    id,
    name,
    value,
    className,
    placeholder,
    error,
    dataTestid,
    onChange,
    defaultValue,
  } = props;
  return (
    <InputFormWrapper>
      <InputForm
        placeholder={placeholder}
        type={type}
        id={id}
        name={name}
        className={className}
      />
      <span className="errormsg">
        {error ? <ErrorMessage name={name} /> : ""}
      </span>
    </InputFormWrapper>
  );
};
