import React, { FC } from "react";
import { Input } from "antd";

import { ErrorMessage } from "formik";
import { TextAreaWrapper } from "./style"
const { TextArea } = Input;
export interface TextAreaProps {
  name?: any;
  id?: string;
  error?: any;
  value?: string;
  onChange?: any;
  defaultValue?: any;
  placeholder?: string;
  className?: any;
  style?:React.CSSProperties
  row?:number
  showCount?:boolean,
  containerStyle?:React.CSSProperties
}
const TextAreaInput: FC<TextAreaProps> = ({
  name,
  id,
  error,
  value,
  onChange,
  defaultValue,
  placeholder,
  className,
  style,
  row ,
  showCount=true,
  containerStyle
}) => {
  return (
    <TextAreaWrapper style={containerStyle}>
      <TextArea
        showCount={showCount===undefined ? true:showCount}
        maxLength={500}
        onChange={onChange}
        name={name}
        defaultValue={defaultValue}
        placeholder={placeholder}
        value={value}
        className={className}
        rows={row || 5 }
        size="large"
        style={{borderRadius:10,...style}}
      />
      <div style={{ color: "red", textAlign: "center" }}>
        <span className="errormsg">
          {error ? <ErrorMessage name={name} /> : ""}
        </span>
      </div>
    </TextAreaWrapper>
  );
};

export default TextAreaInput;
