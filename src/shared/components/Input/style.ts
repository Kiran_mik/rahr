import styled from "styled-components";
import { Input } from "antd";
import { Field } from "formik";
import { AutoComplete } from "antd";



export const FormInput = styled(Input)`
     font-size: 12px;
    font-family: Poppins;
    line-height: 16px;
    padding: 11px 10px;
    border: 1px solid #AFADC8;
    background: transparent;
    border-radius:8px;
    color: #17082D;
    font-weight: 500;
    line-height:22px;
    &:hover{
       border: 1px solid #AFADC8;
    }
    .errormsg{
      color : red !important;
        font-family: Poppins; 
   }
    `
export const InputFormWrapper = styled.div`
 margin-bottom: 18px;
 width:100%;
   .errormsg{
     color:red;
       font-family: Poppins; 
  }
  &.bottom-0{
    margin-bottom:0px;
  }
  .ant-input[disabled]{
    opacity:0.8
  }
`
export const InputForm = styled(Field)`
    font-size: 14px;
    font-family: Poppins;
     width:100%;
    padding: 11px 12px;
    border: 1px solid #BDBDD3 !important;
    background: transparent;
    border-radius: 8px;
    color: #17082D;
    font-weight: 500;
    line-height: 22px;
    &::placeholder{
  color:#7B7B97;
  font-weight:400;
  span{
    color:red;
    padding-bottom:10px
  }
&:active{
  border: 1px solid #BDBDD3;
  }
}
`
export const LabelBlock = styled.div`
  margin-bottom: 4px;
  color: #17082d;
  font-size: 14px;
  min-height: 22px;
`;

export const RequiredBlock = styled.div`
  position: absolute;
  width: 100%;
  margin-left: -10px;

  margin-top: -10px;
`;

export const Required = styled.span`
  color: #fa3c44;
`;

export const Container = styled.div`
.ant-input-group-addon{
  border-radius : 8px 0px 0px 8px;
}
.ant-input-affix-wrapper{
  border-radius: ${(props: { addonBefore: any }) => {
    return props.addonBefore ? "0px 8px 8px 0px" : "8px";
  }} !important;
}
`
export const CustomInput = styled(Input)`
  
.ant-input{
    padding: 12px !important;
    border-radius: ${(props: { addonBefore: any }) => {
    return props.addonBefore ? "0px 8px 8px 0px" : "8px";
  }} !important;
  }

 
&:focus{
    border: 1px solid #AFADC8;
    outline: none;
}
&::placeholder{
  color:rgba(175, 173, 200, 1);
  font-weight:400;
}
`

export const Autofill = styled(AutoComplete)`
    .ant-select-selector {
    border: 1px solid rgba(189, 189, 211, 1) !important;
    border-radius: 8px !important;
    height: auto !important;
    padding: 7px 12px !important;
}
input{
      padding: 12px 0px !important;
    height: auto !important;
    color:#17082D;
}
`
export const TextAreaWrapper = styled.div`
  
  .rounded textarea{
    border-radius:10px;
height: 108px;
    width: 100%;
  
   }
`

