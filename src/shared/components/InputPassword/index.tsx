import React from "react";
import { Input } from "antd";
import { PasswordInput } from "./style";

interface PasswordFieldProps {
  type?: string;
  id?: string;
  placeholder?: string;
  className?: string;
  name?: string;
}

export const PasswordField = (props: PasswordFieldProps): JSX.Element => {
  const { type, id, className, placeholder, name } = props;
  return (
    <PasswordInput>
      <Input.Password
        placeholder={placeholder}
        type={type}
        id={id}
        className={className}
        name={name}
      />
    </PasswordInput>
  );
};
