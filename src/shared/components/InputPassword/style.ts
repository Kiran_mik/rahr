import styled from "styled-components";
import { Input } from "antd";

export const PasswordInput = styled.div`
.ant-input-affix-wrapper{
     border: 1px solid #AFADC8;
    background: transparent;
    border-radius:4px;
    line-height: 0px;
    padding: 0px 0px !important;
   
&:focus{
 box-shadow:none  ;
    border: 1px solid #AFADC8;
    outline: none;
}
.ant-input-suffix {
    margin-left: 4px;
    margin-right: 10px;
}
  }
  .ant-input-password-icon {
    color: rgba(226, 230, 238, 1);
    width: 11px;
}
input{
    font-family: Poppins;
  
     padding: 11px 10px !important;
    color: #17082D;
    font-weight: 500;
    font-size: 12px !important;
    font-family: Poppins !important;
    line-height:22px;
    &::placeholder{
  color:rgba(175, 173, 200, 1);
  font-weight:400;
}
}
`