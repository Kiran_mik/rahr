import React, { useEffect } from "react";
import { Map, GoogleApiWrapper, Circle, Marker } from "google-maps-react";

export interface MapProps {
  google: any;
  initCenter?: any;
  locationsList?: any;
  circleRadius?: number;
  zoom?: number;
  width?: any;
  coord?: any;
  center?: any
}

// const log = logger('MapContainer');

const MapContainer: React.FC<MapProps> = (props) => {
  const { width, google, coord, zoom, center } = props
  let makewidth = width + "px";
  const mapStyles = {
    width: makewidth,
    height: "227px",
    position: "relative",
  };
  return (
    <div style={{ position: "relative", height: "227px" }} id="mapContainer">
      <Map
        google={google}
        style={mapStyles}
        //@ts-ignore
        zoom={zoom || 10}
        initialCenter={{
          lat: 43.5931073,
          lng: -79.6422539,
        }}
        center={center}
      >
        {coord && coord.length >= 0 &&
          (coord.map((each: any) => {
            return <Marker
              //@ts-ignore
              title={'The marker`s title will appear as a tooltip.'}
              name={'SOMA'}
              position={{ lat: each.lat, lng: each.lng }}
              key={each.lat + "_" + each.lng}
            />
          })
          )
        }
      </Map>
    </div>
  );
};

export default GoogleApiWrapper({
  apiKey: process.env.REACT_APP_GOOGLEAPI_KEY || ''
  // apiKey: "",
})(MapContainer);

// {coord.length > 0 &&
//   (coord.map((each: any) => {
//     return <Marker
//       // @ts-ignore
//       name={'Your position'}
//       position={{ lat: each.lat, lng: each.lng }}
//       icon={{
//         url: "http://cdn.onlinewebfonts.com/svg/img_177193.svg",
//         anchor: new google.maps.Point(32, 32),
//         scaledSize: new google.maps.Size(64, 64)
//       }} />
//   })
//   )
// }

