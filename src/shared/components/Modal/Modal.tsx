import React, { FC } from "react";
import { Modal, Button } from "antd";
import modalimg from "@assets/pana.jpg";
import { ModalWrapper, Title } from "./style";
import { PrimaryHalf } from "../../../shared/components/Buttons";
interface Values {
  visible?: boolean;
  onOK?: any;
  onCancel?: any;
  title?: string;
  modalimage?: any;
  content?: any;
  children?: any;
  modalWidth?: any;
}
const ModalComponent: FC<Values> = ({
  visible,
  onOK,
  onCancel,
  title,

  children,
  modalWidth
}) => {
  return (
    <ModalWrapper
      title={title}
      centered
      visible={visible}
      onOk={onOK}
      onCancel={onCancel}
      width={modalWidth}
    >
      {children}
    </ModalWrapper>
  );
};
export default ModalComponent;
