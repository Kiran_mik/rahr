import styled from "styled-components";
import { H3Typography } from "@components/Typography/Typography";
import { Modal } from "antd";
export const ModalWrapper = styled(Modal)`
width:100%
.ant-modal{
    text-align:center !important;
} 
.ant-modal-content {
    position: relative;
    background-color: #fff;
    background-clip: padding-box;
    border: 0;
    border-radius: 24px;
    padding: 32px;
}
.ant-modal-body{
    padding:0px;
}
.modalpara{
    font-size:14px;
    line-height:22px;
    color:#50514F;
    font-family:Poppins;
    font-weight:400;
}
.ant-modal-close-icon{
    display:none;
}
.ant-modal-footer{
    display:none
}
`

export const Title = styled(H3Typography)`
 font-family:Poppins;
 margin-bottom:8px;
`