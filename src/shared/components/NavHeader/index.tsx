import React, { FC } from "react";
import { CustomHeader, Icons } from "./style";
import NotificationIcon from "@assets/icon-alerts.svg";
import AlertSettingIcon from "@assets/icon-setting.svg";
import { Flex } from "@components/Flex";
import { Avatar } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { LogoutOutlined } from "@ant-design/icons";
import { authActions } from "@store/authReducer";
import Cookies from "js-cookie";
import { Menu, Dropdown } from "antd";
import { userActions } from "@store/userProfileReducer";
import { navigate } from "@reach/router";
import services from "@services/index";
interface StaffHeaderProps {
  children?: React.ReactNode;
  loggedUser?: string;
}

const NavHeader: FC<StaffHeaderProps> = ({ children, loggedUser }) => {
  const dispatch = useDispatch();

  const user = useSelector((state: { user: any }) => state.user);
  const { firstName = " ", lastName = " ", email } = user;
  const token = services.getAuthToken();
  let userInitials = "";
  if (user && token) {
    userInitials = firstName[0].toUpperCase() + lastName[0].toUpperCase();
  }
  const menu = (
    <Menu>
      <Menu.Item>
        <button
          onClick={() => {
            dispatch(userActions.setUser(null));
            dispatch(authActions.setAuth(null));
            Cookies.remove("token");
            navigate("/");
          }}
          style={{ background: "transparent", cursor: "pointer" }}
        >
          <LogoutOutlined style={{ marginRight: "5px" }} /> Logout
        </button>
      </Menu.Item>
    </Menu>
  );
  return (
    <CustomHeader>
      {children}
      {user && !!user.role && token && (
        <Icons>
          <Flex style={{ alignSelf: "center", marginRight: "56px" }}>
            <img
              src={NotificationIcon}
              alt=""
              style={{ marginRight: "32px" }}
            />
            <img src={AlertSettingIcon} alt="" />
          </Flex>
          <Flex className="ProfileName">
            <Dropdown overlay={menu}>
              <a
                className="ant-dropdown-link"
                onClick={(e) => e.preventDefault()}
                style={{ color: "#F8F8FC" }}
              >
                <Avatar className="userimage">{userInitials}</Avatar>{" "}
                {email || firstName}
              </a>
            </Dropdown>
          </Flex>
        </Icons>
      )}
    </CustomHeader>
  );
};

export default NavHeader;
