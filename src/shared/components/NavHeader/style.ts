import styled from "styled-components";
export const CustomHeader = styled.div`
background-color: #382561;
display:flex;
height:80px;
align-items: center;
width : 100%;
padding: 14px 103px 16px 103px;

@media(max-width:600px){
    padding: 14px 0px 16px 14px;
}
`;
export const Heading = styled.div`
    display: flex;
    justify-content: space-between;
    padding-bottom:15px;
    align-items:center;
`;

export const Icons = styled.div`
    display: flex;
    justify-content: space-around;
    .ProfileName{
            font-size: 12px;
    line-height: 12px;
    font-weight: 400;
    font-family: Poppins;
    align-self: center;
    display: flex;
    justify-content: center;
    align-items: center;
    self-align:center
    }
    .userimage{
       width: 36px;
    height: 36px;
    line-height: 36px;
    font-size: 10px;
    background-color: white;
    color: black;
    margin-right: 10px;
    font-weight: 400;
    font-family: Poppins;
        align-self: center
    }
    
`;
