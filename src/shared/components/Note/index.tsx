import { Alert } from 'antd'
import React from 'react'
import { Notewrapper } from './style';

interface NoteProps {
	id?: string;
	description?: React.ReactNode;
	className?: string;
	message?: string;
	noteIcon?: typeof Image;
}

export const Note = (props: NoteProps): JSX.Element => {
	const { description } = props;
	const { message } = props;
	return (
		<Notewrapper>
			<Alert
				message={message}
				description={description}
				type="info"
				showIcon
			/>
		</Notewrapper>
	)
}


