import React, { useState } from "react";
import { Container, FolderEyeIcon } from "./style";
import DownloadIcon from "@assets/icons/download-icon.svg";
import { getDocUrl } from "@pages/GlobalSettingsPages/OnboardingPage/helper";
import PDFViewer from "@components/PDFViewer";

interface OnboardingFormButtonProps {
  text?: React.ReactNode;
  image?: any;
  buttonText?: string;
  formDetails: {
    id: string;
    docusign_id?: string;
    form_link?: string;
    form_name: string;
  };
}

export default function OnboardingFormButton(props: OnboardingFormButtonProps) {
  const { text, image, buttonText, formDetails } = props;
  const [showPDF, setShowPDF] = useState <
    false | { form_name: string; form_url: string }
    > ();
  const onView = async () => {
    if (formDetails.docusign_id) {
      const res = await getDocUrl(formDetails);
      setShowPDF(res);
    } else {
      window.open(formDetails.form_link, "_blank");
    }
  };

  const onDownload = async () => {
    if (formDetails.docusign_id) {
      const res = await getDocUrl(formDetails);
      console.log("onDownload ~ res", res);
      let url = res.form_url;
      // window.open(url, "_blank");
      const newWindow = window.open(url, "_blank");
      if (newWindow == null || typeof newWindow == "undefined") {
        //if pop-up blocker is enabled, pdf will open in same window.
        document.location.href = url;
      } else {
        newWindow.focus();
      }
    } else {
      window.open(formDetails.form_link, "_blank");
    }
  };

  return (
    <Container>
      <img src={image} alt="" />
      <div className="text-plain">{text}</div>
      <div className="folder-btn" onClick={onView}>
        <FolderEyeIcon />
        {buttonText}
      </div>
      <div className="download-btn" onClick={onDownload}>
        <img
          src={DownloadIcon}
          alt=""
          style={{ marginRight: "-1px" }}
          width={15}
        />
      </div>
      {showPDF && (
        <PDFViewer
          title={showPDF.form_name}
          pdfURL={showPDF.form_url}
          onClose={() => setShowPDF(false)}
        />
      )}
    </Container>
  );
}
