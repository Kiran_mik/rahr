import styled from "styled-components";
import { FolderViewOutlined } from "@ant-design/icons";

export const Container = styled.div`
  background: rgba(249, 250, 251, 1);
  display: inline-flex;
  width: max-content;
  padding: 12px 16px;
  border: 1px solid #e9ecf3;
  border-radius: 4px;
  align-items: center;
  .text-plain {
    max-width: 246px;
    margin-bottom: 0px;
    padding-right: 32px;
    // white-space: nowrap;
    // overflow: hidden;
  }
  img {
    margin-right: 14px;
  }
  .folder-btn {
    background-color: #4e1c95;
    background: #4e1c95;
    border-color: #4e1c95;
    height: auto;
    padding: 0px 15px;
    font-size: 14px;
    line-height: 32px;
    font-weight: 600;
    border-radius: 8px;
    letter-spacing: 0px;
    color: #fff;
    font-family: Poppins;
    cursor: pointer;
    display: flex;
    justify-content: space-evenly;
    align-content: center;
  }
  .download-btn {
    background-color: #fff;
    display: flex;
    justify-content: center;
    align-content: center;
    width: 32px;
    height: 32px;
    margin-left: 10px;
    border: 1px solid #bdbdd3;
    box-sizing: border-box;
    border-radius: 8px;
  }
`;

export const FolderEyeIcon = styled(FolderViewOutlined)`
  font-size: 17px;
  margin: 7px 5px 0px 0px;
  padding: 0px;
`;
