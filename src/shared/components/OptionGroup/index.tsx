import React, { FC } from "react";
import { Select } from "antd";
const { Option, OptGroup } = Select;

export interface SelectProps {
    onChange?: any;
    onFocus?: any;
    onBlur?: any;
    onSearch?: any;
    options?: any;
    children?: any;
    name?: any;
    id?: string;
    value?: any;
    error?: any;
    placeholdertitle?: any;
    defaultValue?: any;
    mode?: "multiple" | "tags" | undefined;
    allowClear?: boolean;
    onKeyUp?: any;
    className?: any;
}

const OptionsGroup: FC<SelectProps> = ({
    onChange,
    onFocus,
    onBlur,
    onSearch,
    options,
    children,
    name,
    error,
    value,
    id,
    placeholdertitle,
    defaultValue,
    mode,
    allowClear,
    onKeyUp,
    className,
}) => {
    return (
        <Select defaultValue="Find user from list" style={{ width: 200 }} onChange={onChange}>
            <OptGroup label="ASSIGN TO ME">
                <Option value="jack">Jack</Option>
            </OptGroup>
            <OptGroup label="FIND A USER">
                <Option value="Yiminghe">yiminghe</Option>
                <Option value="lucy">Lucy1</Option>
                <Option value="lucy">Lucy2</Option>
                <Option value="lucy">Lucy3</Option>
                <Option value="lucy">Lucy4</Option>
            </OptGroup>
        </Select>
    );
};


export default OptionsGroup;
