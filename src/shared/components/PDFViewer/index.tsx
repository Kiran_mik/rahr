import { CloseOutlined } from "@ant-design/icons";
import { Flex } from "@components/Flex";
import { H2 } from "@components/Typography/Typography";
import React from "react";
import { PDFContent, PDFHeader, PDFViewerContainer, PDFIFrame } from "./style";

interface PDFViewerProps{
    onClose?:()=>void
    pdfURL:string
    title:string
}

function PDFViewer(props:PDFViewerProps) {
    const {onClose,pdfURL,title}=props;
  return (
    <PDFViewerContainer>
      <PDFContent>
        <PDFHeader>
          <CloseOutlined onClick={onClose} style={{ fontSize: 16 }} />
          <Flex left={10}>
            <H2
              text={title}
            />
          </Flex>
        </PDFHeader>
        <PDFIFrame src={pdfURL} />
      </PDFContent>
    </PDFViewerContainer>
  );
}

export default PDFViewer;
