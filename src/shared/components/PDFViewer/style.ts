import styled from "styled-components";

export const PDFViewerContainer = styled.div`
  position: fixed; /* Sit on top of the page content */
  display: block; /* Hidden by default */
  width: 100%; /* Full width (cover the whole page) */
  height: 100%; /* Full height (cover the whole page) */
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(0, 0, 0, 0.5); /* Black background with opacity */
  z-index: 2; /* Specify a stack order in case you're using a different order for other elements */
  cursor: pointer; /* Add a pointer on hover */
`;

export const PDFContent = styled.div`
  background-color: white;
  width: 100%;
  height: 100%;
  margin-left: 100px;
  padding: 10px;
`;

export const PDFHeader = styled.div`
  display: flex;
  align-items: center;
  border-bottom: 1px solid #E9E9E9;
  padding-bottom:10px;
  width: 92%;
  margin-left:10px;
`;


export const PDFIFrame=styled.iframe`
width: 92%;
margin-top:20px;
height: 90%;

`