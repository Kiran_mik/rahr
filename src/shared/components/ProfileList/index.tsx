import React from "react";
import { DemoImage } from "@assets/index";
import { Flex } from "@components/Flex";
import { NumberCircleContainer, NumberLabel, ProfileCircleImg } from "./style";

interface ProfileListProps {
  imgs: Array<string>;
  max?: number;
}
function ProfileList(props: ProfileListProps) {
  const { imgs, max = 3 } = props;
  const splitedArray = imgs.slice(0, max);
  const diff = imgs.length - splitedArray.length;

  return (
    <Flex>
      {splitedArray.map((img: any) => (
        <ProfileCircleImg src={img || DemoImage} />
      ))}
      {!!diff && (
        <NumberCircleContainer>
          <NumberLabel>+ {diff}</NumberLabel>
        </NumberCircleContainer>
      )}
    </Flex>
  );
}

export default ProfileList;
