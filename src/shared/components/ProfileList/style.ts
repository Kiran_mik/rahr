import styled from "styled-components";

export const ProfileCircleImg=styled.img`
width: 24px;
height: 24px;
margin-right: -5px;
border-radius: 50%;
object-fit: cover;
`


export const NumberCircleContainer=styled.div`
width: 24px;
height: 24px;
margin-right: -5px;
border-radius: 50%;
background-color: #F2FFF7;
cursor:pointer;
text-align: center;
`

export const NumberLabel=styled.span`
color: #187D79;
font-weight:600;
font-size:12px;
`