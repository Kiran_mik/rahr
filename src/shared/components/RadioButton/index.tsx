import React, { FC, useState } from "react";
import { Radio } from "antd";
import { RadioBtnWrapper } from "./style";
export interface Props {
  title?: string;
  onToggle?: any;
  value?: number;
}
const RadioBtn: FC<Props> = ({ title, onToggle, value }) => {
  return (
    <RadioBtnWrapper>
      <Radio.Group onChange={onToggle} value={value}>
        <Radio value={value}>{title}</Radio>
      </Radio.Group>
    </RadioBtnWrapper>
  );
};

export default RadioBtn;
