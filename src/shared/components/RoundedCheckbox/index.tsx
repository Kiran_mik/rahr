import React from "react";
import { Checkbox } from "antd";
import { CheckBoxWrapper } from "./style";


export interface CheckboxProps {
  name?: string;
  onChange?: any;
  defaultChecked?: any;
  checked?: any;
  disabled?: any;
  id?: string;
  style?: any;
}
const RoundedCheckbox: React.FC<CheckboxProps> = ({
  id,
  name,
  onChange,
  checked,
  style,
}) => {
  return (
    <CheckBoxWrapper checked={checked}>
      <Checkbox checked={checked} name={name} id={id} onChange={onChange} style={style}>
        {name}
      </Checkbox>
    </CheckBoxWrapper>
  );
};

export default RoundedCheckbox;
