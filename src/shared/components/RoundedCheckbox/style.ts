import { CheckedIcon } from './../AccordianComponent/style';
import styled from "styled-components";
import { Checkbox } from 'antd';
import closeIcon from '@assets/closeIcon.svg'
import checkIcon from '@assets/Check.svg'
export const CheckboxPrimary = styled(Checkbox)`
    padding: 3px;
`;
export const CheckBoxWrapper = styled.div <{ checked: boolean }>`
 
 border-radius:100px;
    display:flex;
    margin-bottom:15px;
.ant-checkbox-wrapper{
     max-width: max-content;
margin-right: 10px;
    color: #000;
    background:${props => props.checked ? "#4E1C95" : "#E9ECF3"};
    color:${props => props.checked ? "#fff" : "#000"};
    padding: 4px 10px;
    font-size: 12px;
    border-radius: 100px;
    display: flex;
    flex-direction: row-reverse;
}
.ant-checkbox:hover{
    border:none;
}
 .ant-checkbox-inner{
    background:transparent;
    border-color:transparent;
    border:none !important
}
 .ant-checkbox-inner::after{
content:url(${closeIcon});
    background: transparent;
    border-color: transparent;
    transform: unset;
    opacity: 1;
    top: 0px;
    position:relative;
    transition:none;
}
.ant-checkbox-checked::after{
    border:none;
}
.ant-checkbox-checked .ant-checkbox-inner::after {
   content:url(${checkIcon});
}
.ant-checkbox-checked .ant-checkbox-inner{
    background:transparent;
    border-color:transparent;
}

`;