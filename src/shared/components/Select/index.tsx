import React, { FC, useEffect, useState } from "react";
import {
  CustomInput,
  InputFormWrapper,
  InputFormWrapperForMultiCheckBox,
} from "./style";
import { ErrorMessage } from "formik";
import { TreeSelect } from "antd";
import { DropdownIcon } from "@assets/index";
import { SearchOutlined } from "@ant-design/icons";

const { SHOW_PARENT } = TreeSelect;

const { Option } = CustomInput;
export interface SelectProps {
  onChange?: any;
  onFocus?: any;
  onBlur?: any;
  onSearch?: any;
  options?: any;
  children?: any;
  name?: any;
  id?: string;
  value?: any;
  error?: any;
  placeholdertitle?: any;
  defaultValue?: any;
  mode?: "multiple" | "tags" | undefined;
  allowClear?: boolean;
  onKeyUp?: any;
  className?: any;
}

const SelectInput: FC<SelectProps> = ({
  onChange,
  onFocus,
  onBlur,
  onSearch,
  options,
  children,
  name,
  error,
  value,
  id,
  placeholdertitle,
  defaultValue,
  mode,
  allowClear,
  onKeyUp,
  className,
}) => {
  return (
    <InputFormWrapper className={className}>
      <CustomInput
        mode={mode}
        showSearch
        id={id}
        style={{ width: "100%" }}
        placeholder={placeholdertitle}
        optionFilterProp="children"
        onChange={onChange}
        onFocus={onFocus}
        onBlur={onBlur}
        onSearch={onSearch}
        options={options}
        value={value}
      >
        {children}
      </CustomInput>

      <span className="errormsg" style={{ color: "red" }}>
        {error ? <ErrorMessage name={name} /> : ""}
      </span>
    </InputFormWrapper>
  );
};

export const SelectInputWithCheckBox: FC<SelectProps> = ({
  onChange,
  onFocus,
  onBlur,
  onSearch,
  options,
  children,
  name,
  error,
  value,
  id,
  placeholdertitle,
  defaultValue,
  mode,
  allowClear,
  onKeyUp,
  className,
}) => {
  const [toggleIcon, setToggleIcon] = useState(false);

  return (
    <InputFormWrapperForMultiCheckBox>
      <TreeSelect
        value={value}
        onChange={onChange}
        onFocus={() => {
          setToggleIcon(!toggleIcon);
        }}
        onBlur={() => {
          setToggleIcon(false);
        }}
        showCheckedStrategy={SHOW_PARENT}
        placeholder={placeholdertitle}
        treeData={options}
        treeCheckable={true}
        showArrow={true}
        treeNodeFilterProp="title"
        showSearch
        suffixIcon={() =>
          toggleIcon ? <SearchOutlined /> : <img src={DropdownIcon} />
        }
        style={{ minWidth: "100%",width:'auto' }}
      />
      <span className="errormsg" style={{ color: "red" }}>
        {error ? <ErrorMessage name={name} /> : ""}
      </span>
    </InputFormWrapperForMultiCheckBox>
  );
};

export default SelectInput;
