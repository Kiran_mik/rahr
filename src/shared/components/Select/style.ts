import styled from "styled-components";
import { Select } from "antd";
import { DropdownIcon } from "@assets/index";

export const InputFormWrapperForMultiCheckBox = styled.div`
  .ant-select-selector {
    padding: 6px 0px !important;
    height: auto !important ;
    border:1px solid #AFADC8 !important;
    border-radius: 8px !important;   
  }

  .ant-select-arrow {
    top: 43%;
    display:block;
    margin-top: 0px;
    font-size: 10px;
    color: #000;
    font-weight:bold;
    right: 20px;
  }
`;

export const InputFormWrapper = styled.div`
  margin-bottom: 18px;
  &.bottom-0 {
    margin-bottom: 0px;
  }
  width: 100%;
  .errormsg {
    color: red;
    font-family: Poppins;
  }

  .ant-select-multiple .ant-select-selection-item {
    font-size: 14px !important;
    line-height: 22px;
    border-radius: 8px;
    background: #dbdbed !important;
  }
  .ant-select-multiple input {
    padding: 0px 0px !important;
  }
  .ant-select-multiple .ant-select-selection-item-remove > .anticon svg {
    color: #000;
  }
`;
export const CustomInput = styled(Select)`
  .ant-select-selector {
    border-radius: 8px !important;
    margin-bottom: 0px;
    border-color: #bdbdd3 !important;
  }
  .ant-select-arrow {
    top: 43%;
    margin-top: 0px;
    font-size: 10px;
    color: #000;
    right: 20px;
  }
  input {
    padding: 12px 0px !important;
    height: auto !important;
  }
  .ant-select-multiple .ant-select-selection-search-input {
    width: 100%;
    padding: 0px 0px !important;
    min-width: 100%;
  }

  .anticon.anticon-down {
    background: url(${DropdownIcon}) no-repeat;
    display: block;
    width: 14px;
    height: 10px;
  }
  .anticon.anticon-down svg {
    display: none;
  }
  .ant-select-selector {
    width: 100%;
    height: auto !important;
    padding: 7px 12px !important;
    line-height: 22px;
  }

  .ant-select-single:not(.ant-select-customize-input)
    .ant-select-selector
    .ant-select-selection-search-input {
    height: 44px;
  }
  .ant-select-selection-placeholder {
    color: #7b7b97;
    font-weight: 400;
  }
`;
