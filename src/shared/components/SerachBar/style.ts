import styled from "styled-components";
import { Input } from "antd";
import { SearchIcon } from "@assets/index";
export const FormInput = styled(Input)`
  font-size: 12px;
  font-family: Poppins;
  line-height: 12px;
  background: transparent;
  color: #17082d;
  font-weight: 500;
  border: none;
  padding-left: 0px;
  &:focus {
    border: none;
    outline: none;
    box-shadow: none;
  }
 

  &::placeholder {
    color: rgba(183, 183, 188, 1);
    font-weight: 400;
    font-size: 12px;
  }
`;
export const SearchbarWrapper = styled.div`
  font-size: 12px;
    font-family: Poppins;
    line-height: 12px;
    padding: 8px 18px 8px 16px;
    border: none;
    background: transparent;
    border-radius: 4px;
    color: #17082D;
    width: 350px;
    background: #FFFFFF;
    align-items: center;
    display: flex;
    &.ViewBranchDetailSearch{
    padding: 2px 12px;
    border: 1px solid #BDBDD3;
    border-radius: 8px;
    width: 320px;
    }
&::after{
  content:url(${SearchIcon}) }};
}
`
export const SearchbarWrapperOutline = styled.div`
font-size: 12px;
font-family: Poppins;
line-height: 16px;
padding: 8px 18px 8px 16px;
border: 1px solid #AFADC8;
background: transparent;
border-radius:4px;
color: #17082D;
font-weight: 500;
align-items: center;
display: flex;
&:hover{
   border: 1px solid #AFADC8;
}
&::after{
  content:url(${SearchIcon}) }};
}
`

