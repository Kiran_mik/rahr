import React, { FC } from "react";
import Banner from "@components/Banners";
import { Flex } from "@components/Flex";
import login from "@assets/login.png";
import { SignupContainer, FormBox, SignupForm } from "./style";
export interface SignUpWrapperProps {
  children?: React.ReactNode;
  bannerImage?: string;
  className?: string;
}

const SignUpWrapper: FC<SignUpWrapperProps> = ({
  children,
  bannerImage,
  className,
}) => {
  return (
    <>
      <SignupContainer>
        <Flex style={{ flex: "43%" }} className="LeftCol">
          <Banner
            title="Start your journey now!"
            image={bannerImage || login}
          />
        </Flex>
        <Flex
          justifyContent={"center"}
          className={`RightCol`}
          style={{ alignItems: "center" }}
        >
          <SignupForm className={className}>
            <FormBox>{children}</FormBox>
          </SignupForm>
        </Flex>
      </SignupContainer>
    </>
  );
};
export default SignUpWrapper;
