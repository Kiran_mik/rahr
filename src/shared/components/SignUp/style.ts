import styled from "styled-components";


export const SignupContainer = styled.div`
  display:flex;

  .create-password-outer{
    padding: 40px 0 0 !important;
  }
  .ant-form-item{
margin-bottom:20px;
  }
  .LeftCol{
    flex: 43%;
     @media(max-width:600px){
        display:none;
    }
  }
   .RightCol{
   flex: 55%;
    background: #ebeff4;

    @media(max-width:600px){
        flex: 100%;
    }
  }
    .ant-input{
       font-size: 14px;
    font-family: Poppins;
     width:100%;
    padding: 11px 12px;
    border: 1px solid #BDBDD3 !important;
    background: transparent;
    border-radius: 4px;
    color: #17082D;
    font-weight: 500;
    line-height: 22px;
    }
   .ant-input:placeholder-shown {
    text-overflow: ellipsis;
    color: #7B7B97;
    font-weight: 400;
    font-family: Poppins;
}
.ant-input-password{
    border: 1px solid #AFADC8;
    background: transparent;
    border-radius:4px;
    padding: 0px 0px !important;
    padding-right:10px !important;
}
.ant-input-password input{
    font-family: Poppins;
     padding: 11px 10px !important;
    color: #17082D;
    font-weight: 500;
    border:none !important;
    font-size: 12px !important;
    font-family: Poppins !important;
     line-height:22px;
}
 .ant-input-password .ant-input-password-icon {
    color: #17082D;
    width: 11px;

}
.ant-input.dynamic-error{
  border :1px solid #F5222D !important;
}
.dynamic-error-lable .ant-form-item-extra{
  color:#ff4d4f !important;   
}
`;
export const FormBox = styled.div`
  width: 409px;
  background: #ffffff;
  box-shadow: 0px 4px 4px rgba(151, 146, 227, 0.03);
 padding: 40px 48px;
  border-radius: 15px;
  justify-content: center;
  box-sizing: border-box;
  position:relative;
  display:block;
margin:0 auto;
  @media (max-width: 768px) {
    width: 100%;
    border-radius: 0px;
`;
export const AlertBox = styled.div`
width: 460px;

  @media (min-width: 1024px) and (max-width: 1399px) {
    width: 450px;
  }
    @media (max-width: 768px) {
      width: 100%;
    }
    @media (min-width: 768px) and (max-width: 1023px) {
      width: 100%;
      margin:0 auto 24px;
    }
`;
export const SignupForm = styled.div`
  position: relative;
  width:100%;
  display: block;
  padding: 50px 0;
  display:block;
  max-height:calc(100vh - 80px);
  overflow-y:auto;
  @media (max-width: 600px) {
    padding:0px
`;
