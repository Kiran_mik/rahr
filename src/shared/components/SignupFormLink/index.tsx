import React from "react";
import { Container } from "./style";
import { Link } from "@reach/router";
export interface PropContent {
    text?: React.ReactNode;
    image?: any;
    redirects?: any;
    linktext?: string;
}
export const SignupFormLink = (props: PropContent): JSX.Element => {
    const { text, image, redirects, linktext } = props;
    return (
        <Container>
            <img src={image} alt="" />
            <p>
                {text}
            </p>
            <Link to={redirects}>{linktext}</Link>
        </Container>
    );
};
export default SignupFormLink;