import styled from "styled-components";

export const Container = styled.div`
    background: rgba(249, 250, 251, 1);
       display: inline-flex;
    width: max-content;
    padding: 16px 24px;
    border: 1px solid #E9ECF3;;
    border-radius:4px;
        align-items: center;
    p{
        margin-bottom:0px;
        margin-right:24px;
    }
    img{
        margin-right:14px
    }
    a{
        background: rgba(245, 138, 7, 1);
    padding: 4px 12px;
    font-size: 12px;
    line-height: 12px;
    color: #fff;
    border-radius: 5px;
    }
`