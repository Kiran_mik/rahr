import React from "react";
import { Flex } from "@components/Flex";
import { Link } from "@reach/router"; 
import { SOCIAL_MEDIA_OPTIONS } from "@constants/index";

function SocialMediaSection() {
  return (
    <Flex justifyContent="space-between" flex={1}>
      {Object.values(SOCIAL_MEDIA_OPTIONS).map((i: any) => {
        return (
          <a key={i.id} href={i.url} target={'_blank'}>
            <img src={i.img} />
          </a>
        );
      })}
    </Flex>
  );
}

export default SocialMediaSection;
