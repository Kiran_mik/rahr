import React from 'react'
import { SquareWrapper } from './style';
import { Radio } from 'antd';

export interface RadioProps {
	text?: React.ReactNode;
	title?: any;
	value?: any;
	onChange?: any;
	name?: any;
	id?: any;
	defaultValue?: any;
	optionArray?: any
}

function SquareRadioButton(props: RadioProps) {
	const { onChange, value, id, name, optionArray, defaultValue } = props;
	return (
		<SquareWrapper>
			<Radio.Group onChange={onChange} defaultValue={defaultValue} value={value} name={name} id={id}>
				{
					optionArray && optionArray.length ? optionArray.map((radioItem: any) => {
						return <Radio value={radioItem.value}>{radioItem.label}</Radio>
					}) : null

				}
			</Radio.Group>
		</SquareWrapper>
	)
}

export default SquareRadioButton;

// Define optional array like this
// let optionArray = [
// 	{
// 		label: "Male",
// 		value: 1
// 	},
// 	{
// 		label: "Female",
// 		value: 2
// 	},
// ]

//Component Calling formate
{/* <SquareRadioButton value={Number(values.gender)}
	onChange={handleChange}
	id={"gender"}
	name={"gender"}
	optionArray={optionArray}
></SquareRadioButton> */}


