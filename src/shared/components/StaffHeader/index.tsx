import React, { FC } from "react";
import { Heading, Icons } from "./style";
import { NotificationIcon, AlertSettingIcon } from "@assets/index";
import { Flex } from "@components/Flex";
import { Avatar } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { LogoutOutlined } from "@ant-design/icons";
import { authActions } from "@store/authReducer";
import Cookies from "js-cookie";
import { Menu, Dropdown } from "antd";
import { userActions } from "@store/userProfileReducer";
import { navigate } from "@reach/router";
interface StaffHeaderProps {
  children?: React.ReactNode;
  loggedUser?: string;
}

const StaffHeader: FC<StaffHeaderProps> = ({ children, loggedUser }) => {
  const dispatch = useDispatch();

  const user = useSelector(
    (state: { user: { username: string } }) => state.user
  );
  const { username } = user;
  const menu = (
    <Menu>
      <Menu.Item>
        <button
          onClick={() => {
            dispatch(userActions.setUser(null));
            dispatch(authActions.setAuth(null));
            Cookies.remove("token");
            navigate("/");
          }}
          style={{ background: "transparent", cursor: "pointer" }}
        >
          <LogoutOutlined style={{ marginRight: "5px" }} /> Logout
        </button>
      </Menu.Item>
    </Menu>
  );
  return (
    <Heading>
      {children}
      <Icons>
        <Flex style={{ alignSelf: "center", marginRight: "47px" }}>
          <img src={NotificationIcon} alt="" style={{ marginRight: "10px" }} />
          <img src={AlertSettingIcon} alt="" />
        </Flex>
        <Flex className="ProfileName">
          <Dropdown overlay={menu}>
            <a
              className="ant-dropdown-link"
              onClick={(e) => e.preventDefault()}
              style={{ color: "#50514F" }}
            >
              <Avatar className="userimage">SA</Avatar> {username}
            </a>
          </Dropdown>
        </Flex>
      </Icons>
    </Heading>
  );
};

export default StaffHeader;
