import styled from "styled-components";

export const Heading = styled.div`
    display: flex;
    justify-content: space-between;
    padding-bottom:15px;
    align-items:center;
`;

export const Icons = styled.div`
    display: flex;
    justify-content: space-around;
    .ProfileName{
            font-size: 12px;
    line-height: 12px;
    font-weight: 400;
    font-family: Poppins;
    align-self: center;
    display: flex;
    justify-content: center;
    align-items: center;
    self-align:center
    }
    .userimage{
       width: 36px;
    height: 36px;
    line-height: 36px;
    font-size: 10px;
    background-color: white;
    color: black;
    margin-right: 10px;
    font-weight: 400;
    font-family: Poppins;
        align-self: center
    }
    
`;
