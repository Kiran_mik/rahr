import styled from "styled-components";

export const TableWrapper = styled.div<{ tableSize?: string }>`
 .ant-table {
    padding-bottom: 27px;
    border-radius: 16px;
    font-family: Poppins;
}
.ant-table-content{
    border: 1px solid;
    color: #E2E6EE;
    border-radius: 15px;
}
.ant-table-thead > tr > th{
      background: #E2E6EE;
}
.ant-table-container table > thead > tr:first-child th:first-child {
    border-top-left-radius: 12px;
     border-bottom-left-radius: 12px;
     
}
.ant-table-container table > thead > tr:first-child th:last-child {
    border-top-right-radius: 12px;
     border-bottom-right-radius: 12px;
}
.ant-table-tbody > tr > td {
    border-bottom: none;
    transition: background 0.3s;
}
table tr td.ant-table-selection-column {
    padding-right: 12px;
    padding-left: 12px;
    text-align: center;
    padding: 12px;
}
.ant-table-thead > tr > th {
    color: rgba(23, 8, 45, 1);
    font-weight: 600;
    font-family: Poppins;
}
.ant-table-thead > tr > th:not(:last-child):not(.ant-table-selection-column):not(.ant-table-row-expand-icon-cell):not([colspan])::before {
background-color:transparent;
}
.ant-table-tbody > tr > td{
      font-size: 12px;
    line-height: 20px;
    color: #17082D;
    font-family: Poppins;
    padding: 13px 16px;
}
.ant-pagination-item-active {
    background: #fff;
    border-color: transparent;
    font-size: 12px;
    font-weight: 600;
    color: rgba(0, 0, 0, 0.65);
}
 .ant-pagination-item-link{
    border:none
}
.ant-table-thead th.ant-table-column-has-sorters:hover {
    background: #E2E6EE;
}
.ant-table-column-sorters{
     justify-content: flex-start;
}
.ant-table-column-sorter{
       color: rgba(23,8,45,1);
           justify-content: flex-start;
}
.ant-table-column-title {
    flex: initial;
    margin-right:9px
}
.ant-table-tbody > tr > td:nth-child(2){
    color:rgba(78, 28, 149, 1);
}
`;


