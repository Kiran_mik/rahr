import React from "react";
import { Tooltip, Button } from "antd";
import QuestionMark from "@assets/question-mark.svg";
import { TooltipWrapper } from "./style";

interface TooltipFieldProps {
  text?: React.ReactNode;
}

export const TooltipText = (props: TooltipFieldProps): JSX.Element => {
  const { text } = props;
  return (
    <TooltipWrapper>
      <Tooltip placement="right" title={text} className="tooltip-style">
        <img
          src={QuestionMark}
          alt="Tooltip Icon"
          style={{ maxWidth: "max-content" }}
        />
      </Tooltip>
    </TooltipWrapper>
  );
};
