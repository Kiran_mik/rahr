import React from "react";
import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import "@testing-library/jest-dom/extend-expect";
import { FormLabel, FormLabelBold, H1, H2, H3, H5, Label } from "./Typography";

describe("Typegraphy test cases", () => {
  window.matchMedia =
    window.matchMedia ||
    function() {
      return {
        matches: false,
        addListener: function() {},
        removeListener: function() {}
      };
    };
  test("H1 should render properly", () => {
    const { getByRole } = render(<H1 text={"test H1"} />);
    expect(getByRole("heading", { level: 1 })).toBeInTheDocument();
  });

  test("H2 should render properly", () => {
    const { getByRole } = render(<H2 text={"test H2"} />);
    expect(getByRole("heading", { level: 2 })).toBeInTheDocument();
  });
  test("H3 should render properly", () => {
    const { getByRole } = render(<H3 text={"test H3"} />);
    expect(getByRole("heading", { level: 3 })).toBeInTheDocument();
  });
  test("H5 should render properly", () => {
    const { getByRole } = render(<H5 text={"test H5"} />);
    expect(getByRole("heading", { level: 5 })).toBeInTheDocument();
  });
  test("Label should render properly", () => {
    const { getByText } = render(<Label text={"test label"} />);

    expect(getByText("test label")).toBeInTheDocument();
  });
  test("FormLabel should render properly", () => {
    const { getByText } = render(<FormLabel text={"test form label"} />);

    expect(getByText("test form label")).toBeInTheDocument();
  });
  test("FormLabelBold should render properly", () => {
    const { getByText } = render(
      <FormLabelBold text={"test FormLabelBold "} />
    );

    expect(getByText("test FormLabelBold")).toBeInTheDocument();
  });
});
