import React, { FC } from "react";
import styled from "styled-components";

export const H1Typography = styled.h1`
  font-size: 30px;
  line-height: 1.2;
  color: #000000;
  font-family: Poppins;
  margin: 0;
  &.font-600 {
    font-weight: 600;
  }
`;

export const H2Typography = styled.h2`
  font-size: 24px;
  line-height: 1.2;
  color: #000000;
  font-family: Poppins;
  margin: 0;
  font-weight: 600;
  &.spaceBottom-24 {
    margin-bottom: 24px;
  }
`;

export const H3Typography = styled.h3`
  font-size: 20px;
  line-height: 28px;
  color: #17082d;
  font-family: Poppins;
  font-weight: 600;
  margin: 0;
  &.font-500 {
    font-weight: 500;
  }
  &.space-bottom-16 {
    margin-bottom: 16px;
  }
  .iconmark {
    color: red;
  }
`;
export const H4Typography = styled.h4`
  font-size: 16px;
  line-height: 24px;
  font-family: Poppins;
  font-weight: 600;
  margin: 0;
`;
export const H4SemiBoldTypography = styled.h4`
  font-size: 20px;
  line-height: 28px;
  font-family: Poppins;
  font-weight: 600;
  margin: 0;
`;
export const H5Typography = styled.h5`
  font-size: 16px;
  line-height: 24px;
  color: #17082d;
  font-weight: 600;
  font-family: Poppins;
  &.font-500{
    font-weight:500;s
  }
  &.spaceBottom-16{
    margin-bottom:16px;
  }
  span{
    color:red;
  }
`;
// Form label

export const LabelTypography = styled.label`
  font-size: 14px !important;
  line-height: 16px;
  color: #17082d;
  font-family: Poppins;
  margin: 0;
  margin-bottom: 4px;
  font-weight: 400;
`;

export const FormLableTypography = styled.label`
  font-size: 14px !important;
  line-height: 16px;
  color: #17082d;
  font-family: Poppins;
  margin: 0;
  margin-bottom: 4px;
  font-weight: 400;
  display: block;
  span {
    color: red;
  }
`;

export const FormLableBoldTypography = styled.label`
  font-size: 14px !important;
  line-height: 20px;
  color: #17082d;
  font-family: Poppins;
  display: block;
  margin-bottom: 4px;
  font-weight: 600;
  span {
    color: red;
  }
  &.spaceBottom-16 {
    margin-bottom: 16px;
  }
`;

// Form Value
export const FormValueTypography = styled.span`
  font-size: 14px !important;
  line-height: 20px;
  color: #17082d;
  font-family: Poppins;
  margin: 0;
  margin-bottom: 4px;
  font-weight: 500;
  &.font-400 {
    font-weight: 400;
  }
`;

//Description
export const DescriptionPara = styled.p`
  margin-bottom: 38px;
  line-height: 22px;
  font-size: 14px;
  color: #59596b;
  font-family: Poppins;
  &.bottom-16 {
    margin-bottom: 16px;
  }
  &.bottom-0 {
    margin-bottom: 0px;
  }
`;

export const ParagraphDetails = styled.p`
  margin-bottom: 38px;
  line-height: 22px;
  font-size: 14px;
  color: #17082d;
  font-family: Poppins;
`;

export const SmallGrayText = styled.span`
  margin-bottom: 4px;
  line-height: 20px;
  font-size: 14px;
  color: #7b7b97;
  font-family: Poppins;
`;
interface TypographyProps {
  text?: string | JSX.Element;
  className?: string;
  dataTestid?: any;
  style?: React.CSSProperties;
  mandatoryIcon?: any;
  onClick?:()=>void
}

export const H1: FC<TypographyProps> = ({ text, style, className }) => {
  return (
    <H1Typography style={style} className={className} data-testid="H1-test">
      {text}
    </H1Typography>
  );
};

export const H2: FC<TypographyProps> = ({ text, className, style }) => {
  return (
    <H2Typography style={style} className={className} data-testid="H2-test">
      {text}
    </H2Typography>
  );
};

export const H3: FC<TypographyProps> = ({
  text,
  className,
  style,
  mandatoryIcon,
}) => {
  return (
    <H3Typography style={style} className={className} data-testid="H3-test">
      {text} <span className="iconmark">{mandatoryIcon}</span>
    </H3Typography>
  );
};
export const H4: FC<TypographyProps> = ({ text,onClick, className, style }) => {
  return (
    <H4Typography onClick={onClick} style={style} className={className} data-testid="H4-test">
      {text}
    </H4Typography>
  );
};

export const H4SemiBold: FC<TypographyProps> = ({ text, className }) => {
  return (
    <H4SemiBoldTypography className={className} data-testid="H4-test">
      {text}
    </H4SemiBoldTypography>
  );
};
export const H5: FC<TypographyProps> = ({ text, className }) => {
  return (
    <H5Typography className={className} data-testid="H5-test">
      {text}
    </H5Typography>
  );
};
export const Label: FC<TypographyProps> = ({ text, className }) => {
  return <LabelTypography className={className}>{text}</LabelTypography>;
};

export const FormLabel: FC<TypographyProps> = ({ text, className }) => {
  return (
    <FormLableTypography className={className}>{text}</FormLableTypography>
  );
};

export const FormValue: FC<TypographyProps> = ({ text, className }) => {
  return (
    <FormValueTypography className={className} data-testid="FieldData">
      {text}
    </FormValueTypography>
  );
};
export const FormLabelBold: FC<TypographyProps> = ({ text, className }) => {
  return (
    <FormLableBoldTypography className={className}>
      {text}
    </FormLableBoldTypography>
  );
};

export const Description: FC<TypographyProps> = ({ text, className,style }) => {
  return (
    <DescriptionPara style={style} className={className} data-testid="Description-test">
      {text}
    </DescriptionPara>
  );
};

export const Paragraph: FC<TypographyProps> = ({ text, className }) => {
  return (
    <ParagraphDetails className={className} data-testid="ParagraphDetails-test">
      {text}
    </ParagraphDetails>
  );
};

export const SmallText: FC<TypographyProps> = ({ text, className }) => {
  return (
    <SmallGrayText className={className} data-testid="SmallGrayText-test">
      {text}
    </SmallGrayText>
  );
};
