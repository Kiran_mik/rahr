import React, { FC, useEffect, useState } from "react";
import { Tabs } from "antd";

const { TabPane } = Tabs;

export interface TabsProps {
  className?: string;
  children?: React.ReactNode;
  tab?: string;
  value?: any;
  title?: string;
  activeKey?: string;
}

const VerticalTab: FC<TabsProps> = ({
  children,
  tab,
  activeKey,
  className,
  value
}) => {
  const [activeKeyState, setActiveKeyState] = useState(activeKey);
  useEffect(() => {
    setActiveKeyState(activeKey);
  }, [activeKey]);
  return (
    <Tabs
      type="card"
      onTabClick={e => {
        setActiveKeyState(e);
      }}
      activeKey={activeKeyState}
      tabPosition="left"
    >
      {value.map((user: any) => {
        return (
          <TabPane tab={user.tab} key={user.key} disabled={user.disabled}>
            {user.content}
          </TabPane>
        );
      })}
    </Tabs>
  );
};

export default VerticalTab;
