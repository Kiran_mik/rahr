import { FormValue } from "@components/Typography/Typography";
import React from "react";
import { FormFieldHeading, FormFieldWrapper } from "./style";

interface FormDataProps {
  heading: string;
  value: any;
  image?: any;
}
const FormDataView: React.FC<FormDataProps> = ({ heading, value, image }) => {
  return (
    <FormFieldWrapper>
      <FormFieldHeading data-testid="FieldHeading">{heading}</FormFieldHeading>
      {image && (
        <img
          alt="ProfileImage"
          style={{
            width: "24px",
            height: "24px",
            marginRight: "10px",
            borderRadius: "50%"
          }}
          src={image}
        />
      )}

      <FormValue text={value ? value : "-"} />
    </FormFieldWrapper>
  );
};
export default FormDataView;
