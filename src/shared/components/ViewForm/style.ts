import { Breadcrumb } from "antd";
import BreadcrumbItem from "antd/lib/breadcrumb/BreadcrumbItem";
import styled from "styled-components";
import { H3Typography } from "@components/Typography/Typography";



export const FormFieldWrapper = styled.div`
 margin-bottom:21px;
`;
export const FormFieldHeading = styled.div`
 font-size: 12px;
    line-height: 20px;
    padding-bottom: 4px;
    color: #7B7B97;
    font-family: Poppins;
    font-weight: 400;
  
`;