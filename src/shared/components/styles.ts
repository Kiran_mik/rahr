import styled from "styled-components";

export const TableWrapper = styled.div`
.ant-table table{
  // table-layout:unset !important;
}
.ant-table-cell-scrollbar{
  display:none;
}
  .ant-table {
    padding: 24px;
    border-radius: 16px;
    font-family: Poppins;
  }
  .ant-table-thead > tr > th {
    background: #e2e6ee;

  }
  .ant-table-container table > thead > tr:first-child th:first-child {
    border-top-left-radius: 12px;
    border-bottom-left-radius: 12px;
        padding: 8px 16px;
  }
  .ant-table-container table > thead > tr:first-child th:last-child{
    border-top-right-radius: 12px;
    border-bottom-right-radius: 12px;
  }

   thead th:nth-last-child(2){
    border-top-right-radius: 12px;
    border-bottom-right-radius: 12px;
  }

  .ant-table-tbody > tr > td:last-child{
    width:60px;
  }
   
 .ant-table-cell.ant-table-cell-scrollbar{
   display:none;
 }
  .ant-table-tbody > tr > td {
    border-bottom: 1px dashed #a2a2ba;
    transition: background 0.3s;
    min-width:150px;
    width:150px;
   
  }
  table tr td.ant-table-selection-column {

    text-align: center;
   
  }
  .ant-table-thead > tr > th {
    color: rgba(23, 8, 45, 1);
    font-weight: 600;
    font-family: Poppins;
  }
  .ant-table-thead
    > tr
    > th:not(:last-child):not(.ant-table-selection-column):not(.ant-table-row-expand-icon-cell):not([colspan])::before {
    background-color: transparent;
  }
  .ant-table-tbody > tr > td {
    font-size: 12px;
    line-height: 20px;
    color: #59596b;
    font-family: Poppins;
    padding: 8px 16px;
  }
 
  .ant-pagination-item-active {
    background: #fff;
    border-color: transparent;
    font-size: 12px;
    font-weight: 600;
    color: rgba(0, 0, 0, 0.65);
  }
  .ant-pagination-item-link {
    border: none;
  }
  .ant-table-thead th.ant-table-column-has-sorters:hover {
    background: #e2e6ee;
  }
  .ant-table-column-sorters {
    justify-content: flex-start;
  }
  .ant-table-column-sorter {
    color: rgba(23, 8, 45, 1);
    justify-content: flex-start;
  }
  .ant-table-column-title {
    flex: initial;
    margin-right: 9px;
  }
  .ant-table-tbody > tr > td:nth-child(2) {
    color: rgba(78, 28, 149, 1);
  }

.NameTitle{
  white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    max-width: 110px;
    display: block;
}
.ant-table-body{
   &::-webkit-scrollbar {
  width: 4px;
   height:4px;
}
&::-webkit-scrollbar-track {
  box-shadow: inset 0 0 5px rgba(226, 226, 238, 1);
  border-radius: 10px;
}


&::-webkit-scrollbar-thumb {
  background: rgba(151, 146, 227, 1);
  border-radius: 10px;
}


&::-webkit-scrollbar-thumb:hover {
  background: rgba(151, 146, 227, 1);
}
}
.ant-table-cell-scrollbar{
  box-shadow:none;
      
}

`;


export const DrwaerInnerContent = styled.div`
   max-height: calc(100vh - 181px);
    overflow: auto;
    min-height: calc(100vh - 181px);
    padding:24px 0px;
.squareRadioBtn .ant-radio-wrapper{
    border: 1px solid #AFADC8;
      padding: 8px 15px;
      margin-bottom: 18px;
      margin-right:15px;
      border-radius:4px;
  }
`
export const DrawerFooter = styled.div`
display:flex;
border-top:1px solid #E2E2EE;
padding:24px 0px 18px 0px;
    -webkit-box-pack: justify;
    -webkit-justify-content: space-between;
    -ms-flex-pack: justify;
    justify-content: space-between;
`

export const ModalFooter = styled.div`
display:flex;
border-top:1px solid #DBDBED;
padding:29px 0px 0px 0px;
    justify-content: flex-start;
`