import { createActions, createReducer } from "reduxsauce"
export interface AgentOnBoardingSteps {
    currentStep: number,
    contactDetails: Object,
    personalInfo: Object,
    employmentInfo: Object,
    precDetails: Object
    charityEnrollments:Object,
    agreements:Object
}
const INITIAL_STATE: AgentOnBoardingSteps = {
    currentStep: 0,
    contactDetails: {},
    personalInfo: {},
    employmentInfo: {},
    precDetails: {},
    charityEnrollments:{},
    agreements:{}
}

const { Types, Creators } = createActions({
    updateForm: ['formType', 'details'],
    setStep: ['step']

})


export const updateForm = (state = INITIAL_STATE, action: { formType: string, details: any }) => ({
    ...state,
    [action.formType]: action.details
})

export const setStep = (state = INITIAL_STATE, action: { step: number }) => {
    return ({
        ...state,
        currentStep: action.step
    })
}

const HANDLERS = {
    [Types.UPDATE_FORM]: updateForm,
    [Types.SET_STEP]: setStep
}


const agentOnboardingReducer = createReducer(INITIAL_STATE, HANDLERS)
export const agentOnboardingActions = Creators

export default agentOnboardingReducer
