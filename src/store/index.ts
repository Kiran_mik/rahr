import { combineReducers, compose, createStore } from "redux";
import authReducer from "./authReducer";
import staffReducer from "./staffReducer";
import userProfileReducer from "./userProfileReducer";
import { persistReducer, persistStore } from "redux-persist";
import storage from "redux-persist/lib/storage";
import rawDataReducer from "./rawDataReducer";
import agentOnboardingReducer from "./agentOnboardingReducer";

const reducer = combineReducers({
  auth: authReducer,
  user: userProfileReducer,
  stff: staffReducer,
  rawData: rawDataReducer,
  agentOnboarding: agentOnboardingReducer,
});
//@ts-ignore
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const persistConfig = {
  key: "root",
  blacklist: ["stff", "rawData", "agentOnboarding"],
  storage,
};

const rootReducer = persistReducer(persistConfig, reducer);

const store = createStore(rootReducer, {}, composeEnhancers());
//@ts-ignore
export const persistor = persistStore(store);

export default store;
