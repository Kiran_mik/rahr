import Moment from 'moment';

export const formatNumberWithComma = (number: string | number) => {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

export const dateFormat = (date: string, formatter = "DD-MM-YYYY") => {
    return Moment(date).format(formatter);
}