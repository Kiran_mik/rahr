import moment from "moment";
import store from "@store/index";
import { rawDataActions } from "@store/rawDataReducer/index";
import { ROLE_MAPING } from "@constants/userConstants";
export interface ObjectStr {
  [key: string]: any;
}

export const getDateAsFormat = (dateString: any, format: string) => {
  let date = moment(dateString, "MMYYYY");
  return date.format(format);
};

export const removeQueryParams = () => {
  window.history.pushState({}, document.title, window.location.pathname);
};
export const getQueryParams = (key: string) => {
  const urlParams = new URLSearchParams(window.location.search);
  return urlParams.get(key);
};

export const addKeysBeforeLoading = () => {
  const path = window.location;
  const search = path.search;
  const pathName = path.pathname;
  if (search) {
    const urlParams = new URLSearchParams(window.location.search);
    updateRawData({ agreement_env_id: urlParams.get("envelope_id") });
  }
};

export const getDocuSignURL = (formName: string, baseURL?: string) => {
  const url = `${baseURL ||
    "v1/agents/onboardings/review-agreements/docusign"}?form=${formName}`;
  return url;
};

export const getErrors = (res: any) => {
  const errors = Object.keys(res).reduce((acc: any, currentValue: any) => {
    acc[currentValue] = res[currentValue][0];
    return acc;
  }, {});
  return errors;
};
export const sum = (a: number, b: number): number => a + b;

export const convertMobileNo = (mobilleNo: any) => {
  return `+1 (${mobilleNo.slice(0, 3)}) ${mobilleNo.slice(
    3,
    6
  )}-${mobilleNo.slice(6, 10)}`;
};

export const updateRawData = (data: any) => {
  store.dispatch(rawDataActions.updateRawData(data));
};

export const isFormValid = (
  errors: {},
  values: {},
  ignore?: string | Array<string>
) => {
  const isValid =
    Object.keys(errors).length === 0 &&
    !Object.keys(values).some((i: string) => {
      if (ignore && ignore.includes(i)) {
        return false;
      }
      //@ts-ignore
      return !values[i];
    });
  return isValid;
};


export const ROLE_ROUTE_MAP: { [key: number]: string } = {
  [ROLE_MAPING.AGENT]: "/agent-onboard/contact-details",
  [ROLE_MAPING.ADMIN]: "/usermanagement/staff-list",
};

export const filterIt = (arr: Array<any>, searchKey: string) => {
  return arr.filter(function(obj) {
    return Object.keys(obj).some(function(key) {
      return obj[key].includes && obj[key].includes(searchKey);
    });
  });
};

export const isObjectEmpty = (object:Object) => {
  return Object.keys(object).length === 0 && object.constructor === Object
}